/**
 * Cinema Seat Allocation
 *
 * A cinema has n rows of seats, numbered from 1 to n and there are ten seats in each row, labelled
 * from 1 to 10 as shown in the figure above.
 *
 * Given the array reservedSeats containing the numbers of seats already reserved, for example,
 * reservedSeats[i] = [3,8] means the seat located in row 3 and labelled with 8 is already reserved.
 *
 * Return the maximum number of four-person groups you can assign on the cinema seats. A four-person
 * group occupies four adjacent seats in one single row. Seats across an aisle (such as [3,3] and
 * [3,4]) are not considered to be adjacent, but there is an exceptional case on which an aisle
 * split a four-person group, in that case, the aisle split a four-person group in the middle, which
 * means to have two people on each side.
 *
 * Example 1:
 *
 * Input: n = 3, reservedSeats = [[1,2],[1,3],[1,8],[2,6],[3,1],[3,10]]
 * Output: 4
 * Explanation: The figure above shows the optimal allocation for four groups, where seats mark with
 * blue are already reserved and contiguous seats mark with orange are for one group.
 * Example 2:
 *
 * Input: n = 2, reservedSeats = [[2,1],[1,8],[2,6]]
 * Output: 2
 * Example 3:
 *
 * Input: n = 4, reservedSeats = [[4,3],[1,4],[4,6],[1,7]]
 * Output: 4
 *
 * Constraints:
 *
 * 1 <= n <= 10^9
 * 1 <= reservedSeats.length <= min(10*n, 10^4)
 * reservedSeats[i].length == 2
 * 1 <= reservedSeats[i][0] <= n
 * 1 <= reservedSeats[i][1] <= 10
 * All reservedSeats[i] are distinct.
 */
/**
 * Optimize below solution.
 */
class Solution {
public:
    int maxNumberOfFamilies(int n, vector<vector<int>>& reservedSeats) {
        int ans = 2 * n;
        sort(reservedSeats.begin(), reservedSeats.end());
        int i = 0, N = reservedSeats.size();
        while (i < N) {
            int seats{0};
            do {
                seats |= 1 << (reservedSeats[i][1] - 1);
                ++i;
            } while (i < N && reservedSeats[i][0] == reservedSeats[i - 1][0]);

            auto p1 = 0 == (seats & 0b000011110);
            auto p2 = 0 == (seats & 0b111100000);
            auto p3 = 0 == (seats & 0b001111000);
            if (p1 && p2) {
                ;
            }
            else if (p1 || p2 || p3) {
                --ans;
            }
            else {
                ans -= 2;
            }
        }

        return ans;
    }
};

/**
 * Because of OOM, we process one row at a time.
 */
class Solution {
public:
    struct Limiter
    {
        Limiter(int v = 0) : val{v} {}
        bool in_range(int L, int H) {
            return L < val && val < H;
        }
        int val;
    };

    int maxNumberOfFamilies(int n, vector<vector<int>>& reservedSeats) {
        int ans = 2 * n;
        array<int, 6> const ranges = { 1, 6, 5, 10, 3, 8 };
        sort(reservedSeats.begin(), reservedSeats.end());
        int i = 0, N = reservedSeats.size();
        while (i < N) {
            array<bool, 3> p4{true, true, true};
            do {
                Limiter L{reservedSeats[i][1]};
                for (int j = 0; j < 6; j += 2) {
                    if (L.in_range(ranges[j], ranges[j + 1])) {
                        p4[j >> 1] = false;
                    }
                }
                ++i;
            } while (i < N && reservedSeats[i][0] == reservedSeats[i - 1][0]);

            if (p4[0] && p4[1]) {
                ;
            }
            else if (p4[0] || p4[1] || p4[2]) {
                ans -= 1;
            }
            else {
                ans -= 2;
            }
        }

        return ans;
    }
};

/**
 * Original solution. But encountered out of memory issue.
 */
class Solution {
public:
    int maxNumberOfFamilies(int n, vector<vector<int>>& reservedSeats) {
        vector<vector<bool>> row_4pg(n, vector<bool>(3, true));
        for (auto const& rs : reservedSeats) {
            auto r = rs[0], c = rs[1];
            if (1 < c && c < 6) {
                row_4pg[r - 1][0] = false;
            }
            if (5 < c && c < 10) {
                row_4pg[r - 1][1] = false;
            }
            if (3 < c && c < 8) {
                row_4pg[r - 1][2] = false;
            }
        }

        int ans = 0;
        for (auto const& r : row_4pg) {
            if (r[0] && r[1]) {
                ans += 2;
            }
            else if (r[0] || r[1] || r[2]) {
                ans += 1;
            }
        }

        return ans;
    }
};
