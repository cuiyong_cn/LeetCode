/**
 * Occurrences After Bigram
 *
 * Given words first and second, consider occurrences in some text of the form
 * "first second third", where second comes immediately after first, and third
 * comes immediately after second.
 *
 * For each such occurrence, add "third" to the answer, and return the answer.
 *
 * Example 1:
 *
 * Input: text = "alice is a good girl she is a good student", first = "a", second = "good"
 * Output: ["girl","student"]
 *
 * Example 2:
 *
 * Input: text = "we will we will rock you", first = "we", second = "will"
 * Output: ["we","rock"]
 *
 * Note:
 *
 *  1. 1 <= text.length <= 1000
 *  2. text consists of space separated words, where each word consists of lowercase English letters.
 *  3. 1 <= first.length, second.length <= 10
 *  4. first and second consist of lowercase English letters.
 */
/**
 * Another intresting solution.
 */
class Solution
{
public:
    vector<string> findOcurrences(string raw_text, string first, string second)
    {
        vector<string> res;
        auto bigram = " " + first + " " + second + " ", text = " " + raw_text;
        auto p = text.find(bigram);
        while (p != string::npos) {
            auto p1 = p + bigram.size(), p2 = p1;
            while (p2 < text.size() && text[p2] != ' ') ++p2;
            res.push_back(text.substr(p1, p2 - p1));
            p = text.find(bigram, p + 1);
        }
        return res;
    }
};
/**
 * My original version. Beats 100%. But if we extend the problem,
 * this method has to add extra state case. So this is not good
 * enough though.
 */
class Solution
{
public:
    vector<string> findOcurrences(string text, string first, string second)
    {
        stringstream ss(text);
        vector<string> ans;
        string word;
        int match = 1;

        while (!ss.eof()) {
            ss >> word;
            switch (match) {
            case 1:
                if (word.compare(first) == 0) match = 2;
                break;
            case 2:
                if (word.compare(second) == 0) match = 3;
                else if (word.compare(first) == 0) match = 2;
                else match = 1;
                break;
            case 3:
                ans.push_back(word);
                if (word.compare(first) == 0) match = 2;
                else match = 1;
                break;
            default:
                break;
            }
        }
        return ans;
    }
};

