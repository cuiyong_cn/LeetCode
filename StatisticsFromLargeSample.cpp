/**
 * Statistics from a Large Sample
 *
 * We sampled integers between 0 and 255, and stored the results in an array count:  count[k] is the number of integers
 * we sampled equal to k.
 *
 * Return the minimum, maximum, mean, median, and mode of the sample respectively, as an array of floating point
 * numbers.  The mode is guaranteed to be unique.
 *
 * (Recall that the median of a sample is:
 *
 * The middle element, if the elements of the sample were sorted and the number of elements is odd;
 * The average of the middle two elements, if the elements of the sample were sorted and the number of elements is even.)
 *
 * Example 1:
 *
 * Input: count = [0,1,3,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
 * Output: [1.00000,3.00000,2.37500,2.50000,3.00000]
 * Example 2:
 *
 * Input: count = [0,4,3,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
 * Output: [1.00000,4.00000,2.18182,2.00000,1.00000]
 *
 * Constraints:
 *
 * count.length == 256
 * 1 <= sum(count) <= 10^9
 * The mode of the sample that count represents is unique.
 * Answers within 10^-5 of the true value will be accepted as correct.
 */
/**
 * Original solution.
 */
class Solution {
public:
    vector<double> sampleStats(vector<int>& count) {
        auto gtZero = [](auto val) {
            return val > 0;
        };

        double min_it = std::distance(count.begin(), std::find_if(count.begin(), count.end(), gtZero));
        double max_it = 255 - std::distance(count.rbegin(), std::find_if(count.rbegin(), count.rend(), gtZero));
        auto [m1, m2] = getMedianIndex(std::accumulate(count.begin(), count.end(), 0));

        double sum = 0;
        double median = 0;
        double mode = 0;
        int mode_count = 0;
        int start = 0;

        for (size_t i = 0; i < count.size(); ++i) {
            int end = start + count[i];
            if (start < m1 && m1 <= end) {
                median += i;
            }
            if (start < m2 && m2 <= end) {
                median += i;
            }
            if (count[i] > mode_count) {
                mode_count = count[i];
                mode = i;
            }
            sum += i * count[i];
            start = end;
        }

        return {min_it, max_it, sum / start, median / 2, mode};
    }
private:
    std::pair<int, int> getMedianIndex(int total)
    {
        int half = total > 1 ? (total / 2) : 1;

        if (total % 2) {
            return {half, half};
        }

        return {half, half + 1};
    }
};
