/**
 * Reconstruct Original Digits from English
 *
 * Given a non-empty string containing an out-of-order English representation of digits 0-9, output the digits in ascending order.
 *
 * Note:
 * Input contains only lowercase English letters.
 * Input is guaranteed to be valid and can be transformed to its original digits. That means invalid inputs such as "abc" or "zerone" are not permitted.
 * Input length is less than 50,000.
 * Example 1:
 * Input: "owoztneoer"
 *
 * Output: "012"
 * Example 2:
 * Input: "fviefuro"
 *
 * Output: "45"
 */
/**
 * Same idea, but different implementation. Weirdly, this one is slower than the original.
 */
class Solution {
public:
    string originalDigits(string s) {
        std::unordered_map<char, int> char_map = {
            {'z', 0}, {'w', 2}, {'u', 4}, {'x', 6}, {'g', 8},
            {'f', 5}, {'s', 7}, {'h', 3}, {'i', 9}, {'o', 1}
        };
        std::array<int, 10> digit_count = { 0, };

        for (auto c : s) {
            auto iter = char_map.find(c);
            if (iter != char_map.end()) {
                ++digit_count[iter->second];
            }
        }

        digit_count[7] -= digit_count[6];
        digit_count[5] -= digit_count[4];
        digit_count[3] -= digit_count[8];
        digit_count[9] -= digit_count[5] + digit_count[6] + digit_count[8];
        digit_count[1] -= digit_count[0] + digit_count[2] + digit_count[4];

        std::string ans;
        for (int i = 0; i < digit_count.size(); ++i) {
            ans += std::string(digit_count[i], '0' + i);
        }

        return ans;
    }
};

/**
 * Rewritten the java version in c++
 */
class Solution {
public:
    string originalDigits(string s) {
        int count[10] = { 0, };

        for (auto c : s){
            if (c == 'z') count[0]++;
            if (c == 'w') count[2]++;
            if (c == 'x') count[6]++;
            if (c == 's') count[7]++; //7-6
            if (c == 'g') count[8]++;
            if (c == 'u') count[4]++;
            if (c == 'f') count[5]++; //5-4
            if (c == 'h') count[3]++; //3-8
            if (c == 'i') count[9]++; //9-8-5-6
            if (c == 'o') count[1]++; //1-0-2-4
        }

        count[7] -= count[6];
        count[5] -= count[4];
        count[3] -= count[8];
        count[9] = count[9] - count[8] - count[5] - count[6];
        count[1] = count[1] - count[0] - count[2] - count[4];

        std::string ans;
        for (int i = 0; i <= 9; i++){
            ans += std::string(count[i], '0' + i);
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    string originalDigits(string s) {
        std::array<int, 26> char_count = { 0, };
        for (auto c : s) {
            ++char_count[c - 'a'];
        }

        std::vector<std::string> dmap = {
            "z0zero", "w2two", "u4four", "o1one", "r3three",
            "f5five", "x6six", "s7seven", "g8eight", "i9nine"
        };

        std::vector<std::string> dstr(10);
        string ans;

        for (const auto& d : dmap) {
            int count = char_count[d[0] - 'a'];
            dstr[d[1] - '0'] = std::string(count, d[1]);
            for (int i = 2; i < d.length(); ++i) {
                char_count[d[i] - 'a'] -= count;
            }
        }

        for (const auto& ds : dstr) {
            ans += ds;
        }

        return ans;
    }
};
