/**
 * Dota2 Senate
 *
 * In the world of Dota2, there are two parties: the Radiant and the Dire.
 *
 * The Dota2 senate consists of senators coming from two parties. Now the Senate wants to decide on
 * a change in the Dota2 game. The voting for this change is a round-based procedure. In each round,
 * each senator can exercise one of the two rights:
 *
 * Ban one senator's right: A senator can make another senator lose all his rights in this and all
 * the following rounds.  Announce the victory: If this senator found the senators who still have
 * rights to vote are all from the same party, he can announce the victory and decide on the change
 * in the game.  Given a string senate representing each senator's party belonging. The character
 * 'R' and 'D' represent the Radiant party and the Dire party. Then if there are n senators, the
 * size of the given string will be n.
 *
 * The round-based procedure starts from the first senator to the last senator in the given order.
 * This procedure will last until the end of voting. All the senators who have lost their rights
 * will be skipped during the procedure.
 *
 * Suppose every senator is smart enough and will play the best strategy for his own party. Predict
 * which party will finally announce the victory and change the Dota2 game. The output should be
 * "Radiant" or "Dire".
 *
 * Example 1:
 *
 * Input: senate = "RD"
 * Output: "Radiant"
 * Explanation:
 * The first senator comes from Radiant and he can just ban the next senator's right in round 1.
 * And the second senator can't exercise any rights anymore since his right has been banned.  And in
 * round 2, the first senator can just announce the victory since he is the only guy in the senate
 * who can vote.
 * Example 2:
 *
 * Input: senate = "RDD"
 * Output: "Dire"
 * Explanation:
 * The first senator comes from Radiant and he can just ban the next senator's right in round 1.
 * And the second senator can't exercise any rights anymore since his right has been banned.
 * And the third senator comes from Dire and he can ban the first senator's right in round 1.
 * And in round 2, the third senator can just announce the victory since he is the only guy in the
 * senate who can vote.
 *
 * Constraints:
 *
 * n == senate.length
 * 1 <= n <= 104
 * senate[i] is either 'R' or 'D'
 */
/**
 * Same idea, using queue to simulate the process.
 */
class Solution {
public:
    string predictPartyVictory(string senate) {
        queue<int> q1;
        queue<int> q2;
        int n = senate.length();
        for(int i = 0; i < n; i++) {
            auto& q = 'R' == senate[i] ? q1 : q2;
            q.push(i);
        }

        while (!q1.empty() && !q2.empty()) {
            auto r_index = q1.front(); q1.pop();
            auto d_index = q2.front(); q2.pop();
            if (r_index < d_index) {
                q1.push(r_index + n);
            }
            else {
                q2.push(d_index + n);
            }
        }

        return q1.size() > q2.size() ? "Radiant" : "Dire";
    }
};

/**
 * More short and intuitive solution.
 */
class Solution {
public:
    string predictPartyVictory(string senate) {
        int count = 0, len = 0;

        while (senate.size() != len) {
            string s;
            len = senate.size();
            for (int i = 0; i < len; i++) {
                if (senate[i] == 'R') {
                    if (count++ >= 0) {
                        s += 'R';
                    }
                }
                else {
                    if (count-- <= 0) {
                        s += 'D';
                    }
                }
            }
            swap(s, senate);
        }

        return 'R' == senate[0] ? "Radiant" : "Dire";
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    string predictPartyVictory(string senate) {
        int dire_senators = 0;
        int radiant_senators = 0;
        for (auto s : senate) {
            if ('R' == s) {
                ++radiant_senators;
            }
            else {
                ++dire_senators;
            }
        }

        int ban_radiant = 0;
        int ban_dire = 0;
        int i = 0;
        while (i < senate.length()) {
            if ('R' == senate[i]) {
                if (ban_radiant > 0) {
                    --ban_radiant;
                    senate[i] = '#';
                }
                else if (0 == dire_senators) {
                    return "Radiant";
                }
                else {
                    --dire_senators;
                    ++ban_dire;
                }
            }
            else if ('D' == senate[i]) {
                if (ban_dire > 0) {
                    --ban_dire;
                    senate[i] = '#';
                }
                else if (0 == radiant_senators) {
                    return "Dire";
                }
                else {
                    --radiant_senators;
                    ++ban_radiant;
                }
            }

            if (i == (senate.length() - 1)) {
                i = 0;
            }
            else {
                ++i;
            }
        }

        return "";
    }
};
