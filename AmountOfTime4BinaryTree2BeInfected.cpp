/**
 * Amount of Time for Binary Tree to Be Infected
 *
 * You are given the root of a binary tree with unique values, and an integer start. At minute 0, an
 * infection starts from the node with value start.
 *
 * Each minute, a node becomes infected if:
 *
 * The node is currently uninfected.
 * The node is adjacent to an infected node.
 * Return the number of minutes needed for the entire tree to be infected.
 *
 * Example 1:
 *
 * Input: root = [1,5,3,null,4,10,6,9,2], start = 3
 * Output: 4
 * Explanation: The following nodes are infected during:
 * - Minute 0: Node 3
 * - Minute 1: Nodes 1, 10 and 6
 * - Minute 2: Node 5
 * - Minute 3: Node 4
 * - Minute 4: Nodes 9 and 2
 * It takes 4 minutes for the whole tree to be infected so we return 4.
 * Example 2:
 *
 *
 * Input: root = [1], start = 1
 * Output: 0
 * Explanation: At minute 0, the only node in the tree is infected so we return 0.
 *
 * Constraints:
 *
 * The number of nodes in the tree is in the range [1, 105].
 * 1 <= Node.val <= 105
 * Each node has a unique value.
 * A node with a value of start exists in the tree.
 */
/**
 * DFS solution.
 */
class Solution {
public:
    int dist = 0;
    pair<int, int> dfs(TreeNode* n, int start) {
        if (n == nullptr)
            return {0, 0};
        auto [d1, inf_d1] = dfs(n->left, start);
        auto [d2, inf_d2] = dfs(n->right, start);
        if (inf_d1 != inf_d2)
            dist = max(dist, inf_d1 > 0 ? d2 + inf_d1 : d1 + inf_d2);
        return { max(d1, d2) + 1, max(inf_d1, inf_d2) + (n->val == start || inf_d1 != inf_d2) };
    }
    int amountOfTime(TreeNode* root, int start) {
        auto [depth, inf_depth] = dfs(root, start);
        return max(dist, depth - inf_depth);
    }
};

/**
 * Original solution.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
int value(TreeNode const* node)
{
    return node ? node->val : -1;
}

void build_graph(TreeNode const* node, int parent, unordered_map<int, tuple<int, int, int>>& graph)
{
    if (nullptr == node) {
        return;
    }

    graph[node->val] = tuple(parent, value(node->left), value(node->right));
    build_graph(node->left, node->val, graph);
    build_graph(node->right, node->val, graph);
}

class Solution {
public:
    int amountOfTime(TreeNode* root, int start) {
        auto graph = unordered_map<int, tuple<int, int, int>>{};
        build_graph(root, -1, graph);

        auto infected = unordered_set<int>{};
        auto dq = deque<int>{start};
        auto ans = -1;
        while (!dq.empty()) {
            ++ans;
            auto size = dq.size();
            for (auto i = size_t{0}; i < size; ++i) {
                auto node = dq.front();
                dq.pop_front();
                if (infected.count(node)) {
                    continue;
                }
                infected.insert(node);
                auto [parent, left, right] = graph[node];
                for (auto n : {parent, left, right}) {
                    if (n > 0 && !infected.count(n)) {
                        dq.push_back(n);
                    }
                }
            }
        }
        return ans;
    }
};
