/**
 * Split Two Strings to Make Palindrome
 *
 * You are given two strings a and b of the same length. Choose an index and split both strings at
 * the same index, splitting a into two strings: aprefix and asuffix where a = aprefix + asuffix,
 * and splitting b into two strings: bprefix and bsuffix where b = bprefix + bsuffix. Check if
 * aprefix + bsuffix or bprefix + asuffix forms a palindrome.
 *
 * When you split a string s into sprefix and ssuffix, either ssuffix or sprefix is allowed to be
 * empty. For example, if s = "abc", then "" + "abc", "a" + "bc", "ab" + "c" , and "abc" + "" are
 * valid splits.
 *
 * Return true if it is possible to form a palindrome string, otherwise return false.
 *
 * Notice that x + y denotes the concatenation of strings x and y.
 *
 * Example 1:
 *
 * Input: a = "x", b = "y"
 * Output: true
 * Explaination: If either a or b are palindromes the answer is true since you can split in the following way:
 * aprefix = "", asuffix = "x"
 * bprefix = "", bsuffix = "y"
 * Then, aprefix + bsuffix = "" + "y" = "y", which is a palindrome.
 * Example 2:
 *
 * Input: a = "xbdef", b = "xecab"
 * Output: false
 * Example 3:
 *
 * Input: a = "ulacfd", b = "jizalu"
 * Output: true
 * Explaination: Split them at index 3:
 * aprefix = "ula", asuffix = "cfd"
 * bprefix = "jiz", bsuffix = "alu"
 * Then, aprefix + bsuffix = "ula" + "alu" = "ulaalu", which is a palindrome.
 *
 * Constraints:
 *
 * 1 <= a.length, b.length <= 105
 * a.length == b.length
 * a and b consist of lowercase English letters
 */
/**
 * Shorten it.
 */
class Solution {
public:
    bool checkPalindromeFormation(string a, string b) {
        return split_palindrome(a, b) || split_palindrome(b, a);
    }

private:
    bool split_palindrome(string const& a, string const& b)
    {
        size_t const N = a.length();
        int i = 0;
        for (int j = N - 1; i < N; ++i, --j) {
            if (a[i] != b[j]) {
                break;
            }
        }

        return is_palindrome({b.c_str() + i, N - 2 * i})
            || is_palindrome({a.c_str() + i, N - 2 * i});
    }

    bool is_palindrome(string_view s)
    {
        int const N = s.length();
        for (int i = 0, j = N - 1; i <= j; ++i, --j) {
            if (s[i] != s[j]) {
                return false;
            }
        }
        return true;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool checkPalindromeFormation(string a, string b) {
        return split_palindrome(a, b) || split_palindrome(b, a);
    }

private:
    bool split_palindrome(string const& a, string const& b)
    {
        size_t const N = a.length();
        int i = 0;
        for (int j = N - 1; i < N; ++i, --j) {
            if (a[i] != b[j]) {
                break;
            }
        }

        for (int j = i; j >= 0; --j) {
            if (is_palindrome({b.c_str() + j, N - 2 * j})) {
                return true;
            }
        }
        for (int j = N - i; j < N; ++j) {
            if (is_palindrome({a.c_str() + (N - j), N - 2 * (N - j)})) {
                return true;
            }
        }
        return false;
    }

    bool is_palindrome(string_view s)
    {
        int const N = s.length();
        for (int i = 0, j = N - 1; i <= j; ++i, --j) {
            if (s[i] != s[j]) {
                return false;
            }
        }
        return true;
    }
};
