/**
 * Minimum Path Sum
 *
 * Given a m x n grid filled with non-negative numbers, find a path from top left to bottom right which minimizes the sum of all numbers along its path.
 *
 * Note: You can only move either down or right at any point in time.
 *
 * Example:
 *
 * Input:
 * [
 *   [1,3,1],
 *   [1,5,1],
 *   [4,2,1]
 * ]
 * Output: 7
 * Explanation: Because the path 1→3→1→1→1 minimizes the sum.
 */
/**
 * A more clean solution.
 */
class Solution {
public:
    int minPathSum(vector<vector<int>>& grid) {
        int m = grid.size();
        int n = grid[0].size();
        vector<int> cur(m, grid[0][0]);
        for (int i = 1; i < m; i++)
            cur[i] = cur[i - 1] + grid[i][0];

        for (int j = 1; j < n; j++) {
            cur[0] += grid[0][j];
            for (int i = 1; i < m; i++)
                cur[i] = min(cur[i - 1], cur[i]) + grid[i][j];
        }

        return cur[m - 1];
    }
};

/**
 * Avoid cache thrashing. This is actually a DP problem.
 */
class Solution {
public:
    int minPathSum(vector<vector<int>>& grid) {
        if (grid.empty()) return 0;

        int m = grid.size(), n = grid[0].size();
        for (int row = m - 1; row >= 0; --row) {
            for (int col = n - 1; col >= 0; --col) {
                int cur = grid[row][col];
                int rv = -1, dv = -1;
                if ((col + 1) < n) rv = grid[row][col + 1];
                if ((row + 1) < m) dv = grid[row + 1][col];
                if (rv >= 0 && dv >= 0) {
                    rv = min(rv, dv);
                }
                else {
                    rv = max(max(rv, dv), 0);
                }
                grid[row][col] = cur + rv;
            }
        }

        return grid[0][0];
    }
};

/**
 * More efficient solution.
 */
class Solution {
public:
    int minPathSum(vector<vector<int>>& grid) {
        if (grid.empty()) return 0;

        int m = grid.size(), n = grid[0].size();
        for (int col = n - 1; col >= 0; --col) {
            for (int row = m - 1; row >= 0; --row) {
                int cur = grid[row][col];
                int rv = -1, dv = -1;
                if ((col + 1) < n) rv = grid[row][col + 1];
                if ((row + 1) < m) dv = grid[row + 1][col];
                if (rv >= 0 && dv >= 0) {
                    rv = min(rv, dv);
                }
                else {
                    rv = max(max(rv, dv), 0);
                }
                grid[row][col] = cur + rv;
            }
        }

        return grid[0][0];
    }
};

/**
 * Passed solution, but not correct.
 */
class Solution {
public:
    int minPathSum(vector<vector<int>>& grid) {
        if (grid.empty()) return 0;

        m = grid.size();
        n = grid[0].size();
        return dfs(grid, 0, 0);
    }

private:
    int dfs(vector<vector<int>>& grid, int x, int y) {
        int len = -1;
        int rl = 0, dl = 0;
        if (x < m && y < n) {
            auto iter = memo.find(x * 10000 + y);
            if (iter == memo.end()) {
                len = grid[x][y];
                int rl = dfs(grid, x, y + 1);
                int dl = dfs(grid, x + 1, y);
                if (rl >= 0 && dl >= 0) {
                    len += min(rl, dl);
                }
                else {
                    len += max(0, max(rl, dl));
                }
                memo[x * 10000 + y] = len;
            }
            else {
                len = iter->second;
            }
        }
        return len;
    }

private:
    int m;
    int n;
    unordered_map<int, int> memo;
};

/**
 * original solution. recursive method but TLE
 */
class Solution {
public:
    int minPathSum(vector<vector<int>>& grid) {
        if (grid.empty()) return 0;

        m = grid.size();
        n = grid[0].size();
        return dfs(grid, 0, 0);
    }
private:
    int dfs(vector<vector<int>>& grid, int x, int y) {
        int len = 0;
        int rl = 0, dl = 0;
        if (x < m && y < n) {
            len = grid[x][y];
            int rl = dfs(grid, x, y + 1);
            int dl = dfs(grid, x + 1, y);
            if (rl > 0 && dl > 0) {
                len += min(rl, dl);
            }
            else {
                len += rl + dl;
            }
        }
        return len;
    }

private:
    int m;
    int n;
};
