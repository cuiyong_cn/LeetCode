/**
 * Long Pressed Name
 *
 * Your friend is typing his name into a keyboard. Sometimes, when typing a character c, the key
 * might get long pressed, and the character will be typed 1 or more times.
 *
 * You examine the typed characters of the keyboard. Return True if it is possible that it was your
 * friends name, with some characters (possibly none) being long pressed.
 *
 * Example 1:
 *
 * Input: name = "alex", typed = "aaleex"
 * Output: true
 * Explanation: 'a' and 'e' in 'alex' were long pressed.
 * Example 2:
 *
 * Input: name = "saeed", typed = "ssaaedd"
 * Output: false
 * Explanation: 'e' must have been pressed twice, but it wasn't in the typed output.
 * Example 3:
 *
 * Input: name = "leelee", typed = "lleeelee"
 * Output: true
 * Example 4:
 *
 * Input: name = "laiden", typed = "laiden"
 * Output: true
 * Explanation: It's not necessary to long press any character.
 *
 * Constraints:
 *
 * 1 <= name.length <= 1000
 * 1 <= typed.length <= 1000
 * name and typed contain only lowercase English letters.
 */
/**
 * make it shorter based on typed.
 */
class Solution {
public:
    bool isLongPressedName(string name, string typed) {
        int i = 0;
        int const N = name.length();
        char prev = 0;
        for (auto const c : typed) {
            if (i < N && c == name[i]) {
                ++i;
            }
            else if (c != prev) {
                return false;
            }
            prev = c;
        }

        return N == i;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool isLongPressedName(string name, string typed) {
        auto prev = name.front();
        auto i = 0;
        for (auto c : name) {
            if (c != prev) {
                while (i < typed.length() && prev == typed[i]) {
                    ++i;
                }
            }

            if (i == typed.length() || c != typed[i]) {
                return false;
            }

            prev = c; ++i;
        }

        while (i < typed.length()) {
            if (prev != typed[i]) {
                return false;
            }
            ++i;
        }

        return true;
    }
};
