/**
 * Lowest Common Ancestor of a Binary Tree
 *
 * Given a binary tree, find the lowest common ancestor (LCA) of two given nodes in the tree.
 *
 * According to the definition of LCA on Wikipedia: “The lowest common ancestor is defined between two nodes p and q as
 * the lowest node in T that has both p and q as descendants (where we allow a node to be a descendant of itself).”
 *
 * Example 1:
 *
 * Input: root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 1
 * Output: 3
 * Explanation: The LCA of nodes 5 and 1 is 3.
 * Example 2:
 *
 * Input: root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 4
 * Output: 5
 * Explanation: The LCA of nodes 5 and 4 is 5, since a node can be a descendant of itself according to the LCA definition.
 * Example 3:
 *
 * Input: root = [1,2], p = 1, q = 2
 * Output: 1
 *
 * Constraints:
 *
 * The number of nodes in the tree is in the range [2, 105].
 * -109 <= Node.val <= 109
 * All Node.val are unique.
 * p != q
 * p and q will exist in the tree.
 */
/**
 * without backtracking.
 */
class Solution {

    // Three static flags to keep track of post-order traversal.

    // Both left and right traversal pending for a node.
    // Indicates the nodes children are yet to be traversed.
    private static int BOTH_PENDING = 2;

    // Left traversal done.
    private static int LEFT_DONE = 1;

    // Both left and right traversal done for a node.
    // Indicates the node can be popped off the stack.
    private static int BOTH_DONE = 0;

    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {

        Stack<Pair<TreeNode, Integer>> stack = new Stack<Pair<TreeNode, Integer>>();

        // Initialize the stack with the root node.
        stack.push(new Pair<TreeNode, Integer>(root, Solution.BOTH_PENDING));

        // This flag is set when either one of p or q is found.
        boolean one_node_found = false;

        // This is used to keep track of the LCA.
        TreeNode LCA = null;

        // Child node
        TreeNode child_node = null;

        // We do a post order traversal of the binary tree using stack
        while (!stack.isEmpty()) {

            Pair<TreeNode, Integer> top = stack.peek();
            TreeNode parent_node = top.getKey();
            int parent_state = top.getValue();

            // If the parent_state is not equal to BOTH_DONE,
            // this means the parent_node can't be popped off yet.
            if (parent_state != Solution.BOTH_DONE) {

                // If both child traversals are pending
                if (parent_state == Solution.BOTH_PENDING) {

                    // Check if the current parent_node is either p or q.
                    if (parent_node == p || parent_node == q) {

                        // If one_node_found was set already, this means we have found
                        // both the nodes.
                        if (one_node_found) {
                            return LCA;
                        } else {
                            // Otherwise, set one_node_found to True,
                            // to mark one of p and q is found.
                            one_node_found = true;

                            // Save the current top element of stack as the LCA.
                            LCA = stack.peek().getKey();
                        }
                    }

                    // If both pending, traverse the left child first
                    child_node = parent_node.left;
                } else {
                    // traverse right child
                    child_node = parent_node.right;
                }

                // Update the node state at the top of the stack
                // Since we have visited one more child.
                stack.pop();
                stack.push(new Pair<TreeNode, Integer>(parent_node, parent_state - 1));

                // Add the child node to the stack for traversal.
                if (child_node != null) {
                    stack.push(new Pair<TreeNode, Integer>(child_node, Solution.BOTH_PENDING));
                }
            } else {

                // If the parent_state of the node is both done,
                // the top node could be popped off the stack.
                // Update the LCA node to be the next top node.
                if (LCA == stack.pop().getKey() && one_node_found) {
                    LCA = stack.peek().getKey();
                }

            }
        }

        return null;
    }
}

/**
 * Iterative solution.
 */
class Solution {
public:
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        deque<TreeNode*> qs{root};
        unordered_map<TreeNode*, TreeNode*> parent;
        parent[root] = nullptr;

        while (parent.find(p) == parent.end() || parent.find(q) == parent.end()) {
            auto node = qs.front();
            qs.pop_front();

            if (node->left) {
                parent[node->left] = node;
                qs.push_back(node->left);
            }
            if (node->right) {
                parent[node->right] = node;
                qs.push_back(node->right);
            }
        }

        unordered_set<TreeNode*> ancestors;

        while (p) {
            ancestors.insert(p);
            p = parent[p];
        }

        while (ancestors.find(q) == ancestors.end()) {
            q = parent[q];
        }

        return q;
    }

private:
};

/**
 * More efficient dfs solution.
 */
class Solution {
public:
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        dfs(root, p, q);

        return ans;
    }

private:
    bool dfs(TreeNode* node, TreeNode* p, TreeNode* q)
    {
        if (!node) {
            return false;
        }

        int left = dfs(node->left, p, q);
        int right = dfs(node->right, p, q);
        int mid = p == node || q == node;

        if ((left + right + mid) >= 2) {
            ans = node;
        }

        return (left + right + mid) > 0;
    }

    TreeNode* ans;
};

/**
 * Original solution. To be honest, I was suprised that this solution passed. Somewhat I explect
 * this gonna be a TLE solution.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        if (p == root || q == root) {
            return root;
        }

        TreeNode* ans = nullptr;
        if (NodeWrapper(p).is_decendant_of(root->left)) {
            if (NodeWrapper(q).is_decendant_of(root->right)) {
                ans = root;
            }
            else {
                ans = lowestCommonAncestor(root->left, p, q);
            }
        }
        else {
            if (NodeWrapper(q).is_decendant_of(root->left)) {
                ans = root;
            }
            else {
                ans = lowestCommonAncestor(root->right, p, q);
            }
        }

        return ans;
    }

private:
    class NodeWrapper
    {
    public:
        NodeWrapper(TreeNode* n) : node{n}
        {

        }

        bool is_decendant_of(TreeNode* t) const
        {
            if (!t) {
                return false;
            }

            return (node == t) || is_decendant_of(t->left) || is_decendant_of(t->right);
        }

    private:
        TreeNode* node;
    };
};
