/**
 * Valid Sudoku
 *
 * Determine if a 9x9 Sudoku board is valid. Only the filled cells need to be validated according to the following rules:
 *     Each row must contain the digits 1-9 without repetition.
 *     Each column must contain the digits 1-9 without repetition.
 *     Each of the 9 3x3 sub-boxes of the grid must contain the digits 1-9 without repetition.
 *
 * A partially filled sudoku which is valid.
 *
 * The Sudoku board could be partially filled, where empty cells are filled with the character '.'.
 *
 * Example 1:
 *
 * Input:
 * [
 *   ["5","3",".",".","7",".",".",".","."],
 *   ["6",".",".","1","9","5",".",".","."],
 *   [".","9","8",".",".",".",".","6","."],
 *   ["8",".",".",".","6",".",".",".","3"],
 *   ["4",".",".","8",".","3",".",".","1"],
 *   ["7",".",".",".","2",".",".",".","6"],
 *   [".","6",".",".",".",".","2","8","."],
 *   [".",".",".","4","1","9",".",".","5"],
 *   [".",".",".",".","8",".",".","7","9"]
 * ]
 * Output: true
 *
 * Example 2:
 *
 * Input:
 * [
 *   ["8","3",".",".","7",".",".",".","."],
 *   ["6",".",".","1","9","5",".",".","."],
 *   [".","9","8",".",".",".",".","6","."],
 *   ["8",".",".",".","6",".",".",".","3"],
 *   ["4",".",".","8",".","3",".",".","1"],
 *   ["7",".",".",".","2",".",".",".","6"],
 *   [".","6",".",".",".",".","2","8","."],
 *   [".",".",".","4","1","9",".",".","5"],
 *   [".",".",".",".","8",".",".","7","9"]
 * ]
 * Output: false
 * Explanation: Same as Example 1, except with the 5 in the top left corner being
 *     modified to 8. Since there are two 8's in the top left 3x3 sub-box, it is invalid.
 *
 * Note:
 *     A Sudoku board (partially filled) could be valid but is not necessarily solvable.
 *     Only the filled cells need to be validated according to the mentioned rules.
 *     The given board contain only digits 1-9 and the character '.'.
 *     The given board size is always 9x9.
 */
/**
 * More readable one.
 */
class Solution {
public:
    bool isValidSudoku(vector<vector<char>>& board) {
        unordered_set<string> seen;

        for (size_t r = 0; r < 9; ++r) {
            for (size_t c = 0; c <9; ++c) {
                if ('.' == board[r][c]) continue;

                auto n = board[r][c] - '1';
                if (!seen.insert(to_string(n) + " in row " + to_string(r)).second
                   || !seen.insert(to_string(n) + " in col " + to_string(c)).second
                   || !seen.insert(to_string(n) + " in block " + to_string(r/3*3 + c/3)).second) {
                    return false;
                }
            }
        }

        return true;
    }
};

/**
 * Improved one.
 */
class Solution {
public:
    bool isValidSudoku(vector<vector<char>>& board) {
        vector<int> row_count(9, 0);
        vector<int> col_count(9, 0);
        vector<int> sub_count(9, 0);
        for (size_t r = 0; r < 9; ++r) {
            for (size_t c = 0; c <9; ++c) {
                if ('.' == board[r][c]) continue;

                auto digit = 1 << (board[r][c] - '1');
                auto sub = r / 3 * 3 + c / 3;
                if ((row_count[r] & digit)
                       || (col_count[c] & digit)
                       || (sub_count[sub] & digit)) {
                    return false;
                }
                row_count[r] |= digit;
                col_count[c] |= digit;
                sub_count[sub] |= digit;
            }
        }

        return true;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool isValidSudoku(vector<vector<char>>& board) {
        vector<vector<int>> row_count(9, vector<int>(9, 0));
        vector<vector<int>> col_count(9, vector<int>(9, 0));
        vector<vector<int>> sub_count(9, vector<int>(9, 0));
        for (size_t r = 0; r < 9; ++r) {
            for (size_t c = 0; c <9; ++c) {
                if ('.' == board[r][c]) continue;

                auto digit = board[r][c] - '1';
                if (row_count[r][digit]) {
                    return false;
                }
                row_count[r][digit] = 1;

                if (col_count[c][digit]) {
                    return false;
                }
                col_count[c][digit] = 1;

                auto sub = r / 3 * 3 + c / 3;
                if (sub_count[sub][digit]) {
                    return false;
                }
                sub_count[sub][digit] = 1;
            }
        }

        return true;
    }
};
