/**
 * Maximum Population Year
 *
 * You are given a 2D integer array logs where each logs[i] = [birthi, deathi] indicates the birth
 * and death years of the ith person.
 *
 * The population of some year x is the number of people alive during that year. The ith person is
 * counted in year x's population if x is in the inclusive range [birthi, deathi - 1]. Note that the
 * person is not counted in the year that they die.
 *
 * Return the earliest year with the maximum population.
 *
 * Example 1:
 *
 * Input: logs = [[1993,1999],[2000,2010]]
 * Output: 1993
 * Explanation: The maximum population is 1, and 1993 is the earliest year with this population.
 * Example 2:
 *
 * Input: logs = [[1950,1961],[1960,1971],[1970,1981]]
 * Output: 1960
 * Explanation:
 * The maximum population is 2, and it had happened in years 1960 and 1970.
 * The earlier year between them is 1960.
 *
 * Constraints:
 *
 * 1 <= logs.length <= 100
 * 1950 <= birthi < deathi <= 2050
 */
/**
 * Line sweep algo.
 */
class Solution {
public:
    int maximumPopulation(vector<vector<int>>& logs) {
        vector<int> population(2051, 0);

        for (auto const& log : logs) {
            ++population[log[0]];
            --population[log[1]];
        }

        int ans = 0;
        int cur_population = 0;
        int max_population = 0;
        for (int y = 1950; y < 2051; ++y) {
            cur_population += population[y];
            if (cur_population > max_population) {
                max_population = cur_population;
                ans = y;
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int maximumPopulation(vector<vector<int>>& logs) {
        int ans = 0;
        int max_population = 0;
        for (int y = 1950; y < 2051; ++y) {
            auto cnt = count_if(logs.begin(), logs.end(),
                               [y](auto const& log) {
                                   return log[0] <= y && y < log[1];
                               });
            if (cnt > max_population) {
                max_population = cnt;
                ans = y;
            }
        }

        return ans;
    }
};
