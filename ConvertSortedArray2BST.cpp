/**
 * Convert Sorted Array to Binary Search Tree
 *
 * Given an array where elements are sorted in ascending order, convert it to a height balanced BST.
 *
 * For this problem, a height-balanced binary tree is defined as a binary tree in which the depth of the two subtrees of every node never differ by more than 1.
 *
 * Example:
 *
 * Given the sorted array: [-10,-3,0,5,9],
 *
 * One possible answer is: [0,-3,9,-10,null,5], which represents the following height balanced BST:
 *
 *       0
 *      / \
 *    -3   9
 *    /   /
 *  -10  5
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/**
 * Original solution. Using divide and conquer method
 */
class Solution {
public:
    TreeNode* dfs(vector<int>& nums, int s, int e) {
        if (s > e) return nullptr;

        int mid = (s + e) >> 1;
        TreeNode* root = new TreeNode(nums[mid]);
        root->left = dfs(nums, s, mid - 1);
        root->right = dfs(nums, mid + 1, e);
        return root;
    }
    TreeNode* sortedArrayToBST(vector<int>& nums) {
        return dfs(nums, 0, nums.size() - 1);
    }
};

/**
 * BFS method.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    struct MyNode {
        MyNode(TreeNode* n, int l, int r) : node{n}, lb{l}, rb{r} {}
        TreeNode* node;
        int lb;
        int rb;
    };

    TreeNode* sortedArrayToBST(vector<int>& nums) {
        if (true == nums.empty()) return nullptr;

        int mid = (nums.size() - 1) >> 1;
        TreeNode* root = new TreeNode(nums[mid]);
        deque<MyNode> q;
        q.push_back(MyNode(root, 0, nums.size() - 1));

        while (false == q.empty()) {
            MyNode t = q.front();
            q.pop_front();

            int l = t.lb;
            int r = t.rb;
            int mid = (l + r) >> 1;
            if (l != mid) {
                TreeNode* left = new TreeNode(nums[(l + mid - 1) >> 1]);
                t.node->left = left;
                q.push_back(MyNode(left, l, mid - 1));
            }
            if (r != mid) {
                TreeNode* right = new TreeNode(nums[(r + mid + 1) >> 1]);
                t.node->right = right;
                q.push_back(MyNode(right, mid + 1, r));
            }
        }

        return root;
    }
};
