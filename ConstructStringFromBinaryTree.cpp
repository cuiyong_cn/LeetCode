/**
 * Construct String from Binary Tree
 *
 * You need to construct a string consists of parenthesis and integers from a binary tree with the preorder traversing way.
 *
 * The null node needs to be represented by empty parenthesis pair "()". And you need to omit all the empty parenthesis
 * pairs that don't affect the one-to-one mapping relationship between the string and the original binary tree.
 *
 * Example 1:
 *
 * Input: Binary tree: [1,2,3,4]
 *        1
 *      /   \
 *     2     3
 *    /
 *   4
 *
 * Output: "1(2(4))(3)"
 *
 * Explanation: Originallay it needs to be "1(2(4)())(3()())",
 * but you need to omit all the unnecessary empty parenthesis pairs.
 * And it will be "1(2(4))(3)".
 *
 * Example 2:
 *
 * Input: Binary tree: [1,2,3,null,4]
 *        1
 *      /   \
 *     2     3
 *      \
 *       4
 *
 * Output: "1(2()(4))(3)"
 *
 * Explanation: Almost the same as the first example,
 * except we can't omit the first parenthesis pair to break the one-to-one mapping relationship between the input and the output.
 */
/**
 * Most efficient one.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    void preorder(TreeNode* t, stringstream& ss) {
        if (nullptr != t) {
            ss << t->val;
            if (nullptr != t->left) {
                ss << "(";
                preorder(t->left, ss);
                ss << ")";
            }
            else if (nullptr != t->right) {
                ss << "()";
            }
            if (nullptr != t->right) {
                ss << "(";
                preorder(t->right, ss);
                ss << ")";
            }
        }
    }

    string tree2str(TreeNode* t) {
        stringstream ss;
        preorder(t, ss);

        return ss.str();
    }
};

/**
 * More compact version. We can also using iterative version.
 */
class Solution {
public:
    string tree2str(TreeNode* t) {
        stringstream ss;
        if (nullptr != t) {
            ss << t->val;
            if (nullptr != t->left) {
                ss << "(" << tree2str(t->left) << ")";
            }
            else if (nullptr != t->right) {
                ss << "()";
            }
            if (nullptr != t->right) {
                ss << "(" << tree2str(t->right) << ")";
            }
        }

        return ss.str();
    }
};

/**
 * Original solution.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    void preorder(TreeNode* t, stringstream& ss) {
        if (nullptr == t) {
            ss << "()";
        }
        else {
            ss << "(" << t->val;
            if (nullptr != t->right) {
                preorder(t->left, ss);
                preorder(t->right, ss);
            }
            else if (nullptr != t->left) {
                preorder(t->left, ss);
            }
            ss << ")";
        }
    }

    string tree2str(TreeNode* t) {
        stringstream ss;
        if (nullptr != t) {
            ss << t->val;
            if (nullptr != t->right) {
                preorder(t->left, ss);
                preorder(t->right, ss);
            }
            else if (nullptr != t->left) {
                preorder(t->left, ss);
            }
        }

        return ss.str();
    }
};
