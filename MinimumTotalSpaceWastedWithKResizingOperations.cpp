/**
 * Minimum Total Space Wasted With K Resizing Operations
 *
 * You are currently designing a dynamic array. You are given a 0-indexed integer array nums, where
 * nums[i] is the number of elements that will be in the array at time i. In addition, you are given
 * an integer k, the maximum number of times you can resize the array (to any size).
 *
 * The size of the array at time t, sizet, must be at least nums[t] because there needs to be enough
 * space in the array to hold all the elements. The space wasted at time t is defined as sizet -
 * nums[t], and the total space wasted is the sum of the space wasted across every time t where 0 <=
 * t < nums.length.
 *
 * Return the minimum total space wasted if you can resize the array at most k times.
 *
 * Note: The array can have any size at the start and does not count towards the number of resizing
 * operations.
 *
 * Example 1:
 *
 * Input: nums = [10,20], k = 0
 * Output: 10
 * Explanation: size = [20,20].
 * We can set the initial size to be 20.
 * The total wasted space is (20 - 10) + (20 - 20) = 10.
 * Example 2:
 *
 * Input: nums = [10,20,30], k = 1
 * Output: 10
 * Explanation: size = [20,20,30].
 * We can set the initial size to be 20 and resize to 30 at time 2.
 * The total wasted space is (20 - 10) + (20 - 20) + (30 - 30) = 10.
 * Example 3:
 *
 * Input: nums = [10,20,15,30,20], k = 2
 * Output: 15
 * Explanation: size = [10,20,20,30,30].
 * We can set the initial size to 10, resize to 20 at time 1, and resize to 30 at time 3.
 * The total wasted space is (10 - 10) + (20 - 20) + (20 - 15) + (30 - 30) + (30 - 20) = 15.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 200
 * 1 <= nums[i] <= 106
 * 0 <= k <= nums.length - 1
 */
/**
 * I have no clue resolving this problem. So here we go a solution from Discussion.
 * Some notes:
 * 1. Let dp(i, k) denote the minimum space wasted if we can resize k times of nums[i..n-1].
 * 2. The idea is that when we do use a resize option to adapt all elements in nums[i..j],
 *    we need to pick the size equal to the maximum number in nums[i..j].
 *    so the total space wasted is max(nums[i...j]) * (j-i+1) - sum(nums[i..j)
 *
 * Another way to look at this problem is:
 * we are split this array into K intervals. And for each interval,
 * the wasted space is max(nums[i..j]) * (j - i + 1) - sum(nums[i..j])
 */
class Solution {
public:
    int INF = 200 * 1e6;
    int memo[200][200] = {};

    int minSpaceWastedKResizing(vector<int> &nums, int k) {
        memset(memo, -1, sizeof(memo));
        return dp(nums, 0, k);
    }

    int dp(vector<int> const& nums, int i, int k) {
        int const N = nums.size();
        if (i == N) {
            return 0;
        }

        if (k < 0) {
            return INF;
        }

        if (memo[i][k] != -1) {
            return memo[i][k];
        }

        int ans = INF;
        int maxNum = nums[i];
        int totalSum = 0;
        for (int j = i; j < N; ++j) {
            maxNum = max(maxNum, nums[j]);
            totalSum += nums[j];
            int wasted = maxNum * (j - i + 1) - totalSum;
            ans = min(ans, dp(nums, j + 1, k - 1) + wasted);
        }

        return memo[i][k] = ans;
    }
};
