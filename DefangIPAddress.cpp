/**
 * Given a valid (IPv4) IP address, return a defanged version of that IP address.
 * A defanged IP address replaces every period "." with "[.]".
 *
 * Input: address = "1.1.1.1"
 * Output: "1[.]1[.]1[.]1"
 *
 * Input: address = "255.100.50.0"
 * Output: "255[.]100[.]50[.]0"
 *
 * Constraints:
 *
 * The given address is a valid IPv4 address.
 *
 */

/**
 * Found a really cool python solution
 *
 * def defangIPaddr(self, address):
 *     return ("[.]".join(address.split('.')))
 */

/**
 * C++ one liner solution
 *
 * std::regex_replace(address, "[.]", "[.]");
 */
class Solution {
public:
    string defangIPaddr(string address) {
        std::string defangedAddr;
        for (auto c : address) {
            if (c == '.') {
                defangedAddr += "[.]";
            }
            else {
               defangedAddr += c; 
            }
        }
        
        return defangedAddr;
    }
};
