/**
 * Vowels of All Substrings
 *
 * Given a string word, return the sum of the number of vowels ('a', 'e', 'i', 'o', and 'u') in
 * every substring of word.
 *
 * A substring is a contiguous (non-empty) sequence of characters within a string.
 *
 * Note: Due to the large constraints, the answer may not fit in a signed 32-bit integer. Please be
 * careful during the calculations.
 *
 * Example 1:
 *
 * Input: word = "aba"
 * Output: 6
 * Explanation:
 * All possible substrings are: "a", "ab", "aba", "b", "ba", and "a".
 * - "b" has 0 vowels in it
 * - "a", "ab", "ba", and "a" have 1 vowel each
 * - "aba" has 2 vowels in it
 * Hence, the total sum of vowels = 0 + 1 + 1 + 1 + 1 + 2 = 6.
 * Example 2:
 *
 * Input: word = "abc"
 * Output: 3
 * Explanation:
 * All possible substrings are: "a", "ab", "abc", "b", "bc", and "c".
 * - "a", "ab", and "abc" have 1 vowel each
 * - "b", "bc", and "c" have 0 vowels each
 * Hence, the total sum of vowels = 1 + 1 + 1 + 0 + 0 + 0 = 3.
 * Example 3:
 *
 * Input: word = "ltcd"
 * Output: 0
 * Explanation: There are no vowels in any substring of "ltcd".
 *
 * Constraints:
 *
 * 1 <= word.length <= 105
 * word consists of lowercase English letters.
 */
/**
 * See comments.
 */
class Solution {
public:
    long long countVowels(string word) {
        auto vowel_set = unordered_set<char>{'a', 'e', 'i', 'o', 'u'};
        long long const N = word.length();
        long long ans = 0;

        for (long long i = 0; i < N; ++i) {
            if (vowel_set.count(word[i])) {
                /**
                 * There are 3 cases
                 * 1. substr ends with i =  (i + 1)
                 * 2. substr start with i = (N - i)
                 * 3. substr contains with i = i * (N - i - 1)
                 * case 1 and caes 2 has a duplicate, thus case1 + case2 = N
                 */
                ans += N + (i * (N - i - 1)); // => (i + 1) * (N - 1)
            }
        }

        return ans;
    }
};

/**
 * Original solution. Brute force.
 */
class Solution {
public:
    long long countVowels(string word) {
        auto vowel_set = unordered_set<char>{'a', 'e', 'i', 'o', 'u'};
        int const N = word.length();
        vector<int> vowels_to(N + 1, 0);

        for (int i = 0; i < N; ++i) {
            vowels_to[i + 1] = vowels_to[i] + vowel_set.count(word[i]);
        }

        long long ans = 0;;
        for (int i = 0; i < N; ++i) {
            for (int L = 1; (i + L) <= N; ++L) {
                auto end = i + L;
                ans += vowels_to[i + L] - vowels_to[i];
            }
        }

        return ans;
    }
};
