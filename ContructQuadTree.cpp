/**
 * Construct Quad Tree
 *
 * We want to use quad trees to store an N x N boolean grid. Each cell in the grid can
 * only be true or false. The root node represents the whole grid. For each node,
 * it will be subdivided into four children nodes until the values in the region it
 * represents are all the same.
 * 
 * Each node has another two boolean attributes : isLeaf and val. isLeaf is true if and
 * only if the node is a leaf node. The val attribute for a leaf node contains the value
 * of the region it represents.
 * 
 * Your task is to use a quad tree to represent a given grid. The following example may
 * help you understand the problem better:
 * 
 * Given the 8 x 8 grid below, we want to construct the corresponding quad tree:
 * 
 * It can be divided according to the definition above:
 * 
 * The corresponding quad tree should be as following, where each node is represented as
 * a (isLeaf, val) pair.
 * 
 * For the non-leaf nodes, val can be arbitrary, so it is represented as *.
 * 
 * Note:
 * 
 *     N is less than 1000 and guaranteened to be a power of 2.
 *     If you want to know more about the quad tree, you can refer to its wiki.
 */
/*
// Definition for a QuadTree node.
class Node {
public:
    bool val;
    bool isLeaf;
    Node* topLeft;
    Node* topRight;
    Node* bottomLeft;
    Node* bottomRight;

    Node() {}

    Node(bool _val, bool _isLeaf, Node* _topLeft, Node* _topRight, Node* _bottomLeft, Node* _bottomRight) {
        val = _val;
        isLeaf = _isLeaf;
        topLeft = _topLeft;
        topRight = _topRight;
        bottomLeft = _bottomLeft;
        bottomRight = _bottomRight;
    }
};
*/

/**
 * My original version. Simple and Intuitive.
 */
class Solution {
    struct Point
    {
        int x;
        int y;
        Point(int x_, int y_) : x{x_}, y{y_} {}
    };

private:
    bool allSame(vector<vector<int>>& g, Point tl, Point br) {
        int x1 = tl.x;
        int x2 = br.x;
        int y1 = tl.y;
        int y2 = br.y;
        int pat = g[x1][y1];
        for (int i = x1; i < x2; ++i) {
            for (int j = y1; j < y2; ++j) {
                if (g[i][j] != pat) {
                    return false;
                }
            }
        }
        
        return true;
    }
    
    Node* quadTree(vector<vector<int>>& g, Point tl, Point br) {
        if (true == allSame(g, tl, br)) {
            bool val = g[tl.x][tl.y] ? true : false;
            return new Node(val, true, nullptr, nullptr, nullptr, nullptr);
        }

        int midx = (br.x + tl.x) / 2;
        int midy = (br.y + tl.y) / 2;
        Node* tL = quadTree(g, tl, {midx, midy});
        Node* tR = quadTree(g, {tl.x, midy}, {midx, br.y});
        Node* bL = quadTree(g, {midx, tl.y}, {br.x, midy});
        Node* bR = quadTree(g, {midx, midy}, br);
        return new Node(false, false, tL, tR, bL, bR);
    }
public:
    Node* construct(vector<vector<int>>& grid) {
        int N = grid.size();
        return quadTree(grid, {0, 0}, {N, N});
    }
};

/**
 * Same idea, but the param is just simple type.
 */
class Solution
{
private:
    Node *constructHelper(vector<vector<int>> &grid, int len, int x, int y)
    {
        int flag = grid[x][y];
        for (int i = x; i < x + len; i++)
            for (int j = y; j < y + len; j++)
                //if not a leaf, go deeper
                if (flag != grid[i][j])
                {
                    Node *tl = constructHelper(grid, len / 2, x, y);
                    Node *tr = constructHelper(grid, len / 2, x, y + len / 2);
                    Node *bl = constructHelper(grid, len / 2, x + len / 2, y);
                    Node *br = constructHelper(grid, len / 2, x + len / 2, y + len / 2);
                    return new Node(true, false, tl, tr, bl, br);
                }
        return new Node(grid[x][y] == 1, true, nullptr, nullptr, nullptr, nullptr);
    }

public:
    Node *construct(vector<vector<int>> &grid)
    {
        int n = grid.size();
        return (n == 0) ? nullptr : constructHelper(grid, n, 0, 0);
    }
};

/**
 * Use the split and merge approach
 */
class Solution {
private:
    Node* buildNode(vector<vector<int>>& grid, int x, int y, int length) {
        if (length == 1) {
            return new Node(grid[x][y] == 1, true, nullptr, nullptr, nullptr, nullptr);
        }
        
        int newLength = length / 2;
        Node* topLeft = buildNode(grid, x, y, newLength);
        Node* topRight = buildNode(grid, x, y + newLength, newLength);
        Node* botLeft = buildNode(grid, x + newLength, y, newLength);
        Node* botRight = buildNode(grid, x + newLength, y + newLength, newLength);
        
        bool isAllLeaf = topLeft->isLeaf && topRight->isLeaf && botRight->isLeaf && botLeft->isLeaf;
        if (true == isAllLeaf) {
            bool isValAllSame = topLeft->val && topRight->val && botLeft->val && botRight->val;
            if (false == isValAllSame) {
                isValAllSame = false == (topLeft->val || topRight->val || botLeft->val || botRight->val);
            }
            if (true == isValAllSame) {
                bool val = topLeft->val;
                delete topLeft;
                delete topRight;
                delete botLeft;
                delete botRight;
                return new Node(val, true, nullptr, nullptr, nullptr, nullptr);
            }
        }

        return new Node(true, false, topLeft, topRight, botLeft, botRight);
    }
public:
    Node* construct(vector<vector<int>>& grid) {
        int N = grid.size();
        if (N == 0) {
            return nullptr;
        }
        return buildNode(grid, 0, 0, N);
    }
};
