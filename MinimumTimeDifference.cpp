/**
 * Minimum Time Difference
 *
 * Given a list of 24-hour clock time points in "Hour:Minutes" format, find the minimum minutes difference between any
 * two time points in the list.
 *
 * Example 1:
 *
 * Input: ["23:59","00:00"]
 * Output: 1
 *
 * Note:
 *     The number of time points in the given list is at least 2 and won't exceed 20000.
 *     The input time is legal and ranges from 00:00 to 23:59.
 */
/**
 * Improved one. Using bucket sort
 */
class Solution {
public:
    int findMinDifference(vector<string>& timePoints) {
        vector<bool> mark(24 * 60, false);

        for (auto& time : timePoints) {
            int h = stoi(time.substr(0, 2));
            int m = stoi(time.substr(3, 2));

            if (mark[h * 60 + m]) return 0;
            mark[h * 60 + m] = true;
        }

        auto it1 = find(mark.begin(), mark.end(), true);
        auto it2 = find(mark.rbegin(), mark.rend(), true);
        int first = distance(mark.begin(), it1);
        int last = 24 * 60 - distance(mark.rbegin(), it2) - 1;
        int ans = first + 24 * 60 - last;
        int prev = first;

        for (int i = first + 1; i <= last; ++i) {
            if (mark[i]) {
                ans = min(ans, i - prev);
                prev = i;
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int findMinDifference(vector<string>& timePoints) {
        vector<int> times(timePoints.size(), 0);
        for (int i = 0; i < timePoints.size(); ++i) {
            string ts = timePoints[i];
            transform(ts.begin(), ts.end(), ts.begin(), [](char& c) {
                return ':' == c ? ' ' : c;
            });
            stringstream ss(ts);
            int h = 0, m = 0;
            ss >> h >> m;
            times[i] = h * 60 + m;
        }

        sort(times.begin(), times.end());

        int ans = times[0] + 24 * 60 - times[times.size() - 1];

        for (int i = 1; i < times.size(); ++i) {
            ans = min(ans, times[i] - times[i - 1]);
        }

        return ans;
    }
};
