/**
 * Three Divisors
 *
 * Given an integer n, return true if n has exactly three positive divisors. Otherwise, return false.
 *
 * An integer m is a divisor of n if there exists an integer k such that n = k * m.
 *
 * Example 1:
 *
 * Input: n = 2
 * Output: false
 * Explantion: 2 has only two divisors: 1 and 2.
 * Example 2:
 *
 * Input: n = 4
 * Output: true
 * Explantion: 4 has three divisors: 1, 2, and 4.
 *
 * Constraints:
 *
 * 1 <= n <= 104
 */
/**
 * Original solution.
 */
bool is_prime(int n)
{
    if (n < 2) {
        return false;
    }

    if (n <= 3) {
        return true;
    }

    if (n % 2 == 0 || n % 3 == 0) {
        return false;
    }

    for (int i = 5; i * i <= n; i += 6) {
        if (n % i == 0 || n % (i + 2) == 0) {
            return false;
        }
    }

    return true;
}

class Solution {
public:
    bool isThree(int n) {
        if (n < 4) {
            return false;
        }

        int const sr = sqrt(n);
        if ((sr * sr) != n) {
            return false;
        }

        return is_prime(sr);
    }
};
