/**
 * Keyboard Row
 *
 * Given a List of words, return the words that can be typed using letters of
 * alphabet on only one row's of American keyboard like the image below.
 *
 * Example:
 *
 * Input: ["Hello", "Alaska", "Dad", "Peace"]
 * Output: ["Alaska", "Dad"]
 *
 * Note:
 *
 *     You may use one character in the keyboard more than once.
 *     You may assume the input string will only contain letters of alphabet.
 */
/**
 * My original version
 */
class Solution
{
private:
    map<char, int> kbmap;
public:
    Solution()
    {
        kbmap = { {'Q', 1}, {'q', 1}, {'W', 1}, {'w', 1}, {'E', 1}, {'e', 1}, {'R', 1}, {'r', 1},
            {'T', 1}, {'t', 1}, {'Y', 1}, {'y', 1}, {'U', 1}, {'u', 1}, {'I', 1}, {'i', 1},
            {'O', 1}, {'o', 1}, {'P', 1}, {'p', 1}, {'A', 2}, {'a', 2}, {'S', 2}, {'s', 2},
            {'D', 2}, {'d', 2}, {'F', 2}, {'f', 2}, {'G', 2}, {'g', 2}, {'H', 2}, {'h', 2},
            {'J', 2}, {'j', 2}, {'K', 2}, {'k', 2}, {'L', 2}, {'l', 2}, {'Z', 3}, {'z', 3},
            {'X', 3}, {'x', 3}, {'C', 3}, {'c', 3}, {'V', 3}, {'v', 3}, {'B', 3}, {'b', 3},
            {'N', 3}, {'n', 3}, {'M', 3}, {'m', 3}
        };
    }
    vector<string> findWords(vector<string>& words)
    {
        vector<string> ans;
        for (auto& s : words) {
            bool oneRow = true;
            for (int i = 1; i < s.size(); ++i) {
                if (kbmap[s[i]] != kbmap[s[i - 1]]) {
                    oneRow = false;
                    break;
                }
            }
            if (oneRow) {
                ans.push_back(s);
            }
        }
        return ans;
    }
};

/**
 * Another idea is simple. Define a mask flag for each row. In my implementation,
 * I define the flag for Row 1 ("QWERTYUIOP") as 1 (001), Row 2 ("ASDFGHJKL") as
 * 2 (010), Row 3 ("ZXCVBNM") as 4 (100).
 * For each word, us 7 (111) as base, do AND operation on each character.
 * If all chars from same row, the final result will be one of (1, 2, 4).
 * If any character from other row, the final result will be 0.
 */
class Solution
{
public:
    vector<string> findWords(vector<string>& words)
    {
        vector<int> dict(26);
        vector<string> rows = {"QWERTYUIOP", "ASDFGHJKL", "ZXCVBNM"};
        for (int i = 0; i < rows.size(); i++) {
            for (auto c : rows[i]) dict[c - 'A'] = 1 << i;
        }
        vector<string> res;
        for (auto w : words) {
            int r = 7;
            for (char c : w) {
                r &= dict[toupper(c) - 'A'];
                if (r == 0) break;
            }
            if (r) res.push_back(w);
        }
        return res;
    }
};

/**
 * Three hash table method
 */
class Solution
{
public:
    vector findWords(vector& words)
    {
        std::unordered_set  dict1 = { 'q', 'Q', 'w', 'W', 'e', 'E', 'r', 'R', 't', 'T', 'y', 'Y', 'u', 'U', 'i', 'I', 'o', 'O', 'p', 'P' };
        std::unordered_set  dict2 = { 'a', 'A', 's', 'S', 'd', 'D', 'f', 'F', 'g', 'G', 'h', 'H', 'j', 'J', 'k', 'K', 'l', 'L'};
        std::unordered_set  dict3 = { 'z', 'Z', 'x', 'X', 'c', 'C', 'v', 'V', 'b', 'B', 'n', 'N', 'm', 'M'};

        vector res;

        for (auto &word : words) {
            bool d1 = true, d2 = true, d3 = true;

            for (auto & ch : word) {
                if (d1) {
                    auto it = dict1.find(ch);
                    if (it == dict1.end()) d1 = false;
                }

                if (d2) {
                    auto it = dict2.find(ch);
                    if (it == dict2.end()) d2 = false;
                }

                if (d3) {
                    auto it = dict3.find(ch);
                    if (it == dict3.end()) d3 = false;
                }
            }

            if (d1 || d2 || d3) res.push_back(word);
        }

        return res;
    }
};

/**
 * set method
 */
class Solution
{
public:
    vector<string> findWords(vector<string>& words)
    {
        unordered_set<char> row1 {'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'};
        unordered_set<char> row2 {'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l'};
        unordered_set<char> row3 { 'z', 'x', 'c', 'v', 'b', 'n', 'm'};
        vector<unordered_set<char>> rows {row1, row2, row3};

        vector<string> validWords;
        for (int i = 0; i < words.size(); ++i) {
            int row = 0;

            for (int k = 0; k < 3; ++k) {
                if (rows[k].count((char)tolower(words[i][0])) > 0) row = k;
            }

            validWords.push_back(words[i]);
            for (int j = 1; j < words[i].size(); ++j) {
                if (rows[row].count((char)tolower(words[i][j])) == 0) {
                    validWords.pop_back();
                    break;
                }
            }

        }
        return validWords;
    }
};

class Solution
{
public:

    vector<string> findWords(vector<string>& words)
    {
        vector<string> res;

        for (auto str : words) {
            bool r1 = str.find_first_of("QWERTYUIOPqwertyuiop") == string::npos ? false : true;
            bool r2 = str.find_first_of("ASDFGHJKLasdfghjkl") == string::npos ? false : true;
            bool r3 = str.find_first_of("ZXCVBNMzxcvbnm") == string::npos ? false : true;

            if (r1 + r2 + r3 == 1)
                res.push_back(str);
        }

        return res;
    }

};
