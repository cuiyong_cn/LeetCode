/**
 * Sum Multiples
 *
 * Given a positive integer n, find the sum of all integers in the range [1, n]
 * inclusive that are divisible by 3, 5, or 7.
 *
 * Return an integer denoting the sum of all numbers in the given range satisfying the constraint.
 *
 * Example 1:
 *
 * Input: n = 7
 * Output: 21
 * Explanation: Numbers in the range [1, 7] that are divisible by 3, 5, or 7 are 3, 5, 6, 7. The sum of these numbers is 21.
 * Example 2:
 *
 * Input: n = 10
 * Output: 40
 * Explanation: Numbers in the range [1, 10] that are divisible by 3, 5, or 7 are 3, 5, 6, 7, 9, 10. The sum of these numbers is 40.
 * Example 3:
 *
 * Input: n = 9
 * Output: 30
 * Explanation: Numbers in the range [1, 9] that are divisible by 3, 5, or 7 are 3, 5, 6, 7, 9. The sum of these numbers is 30.
 *
 * Constraints:
 *
 * 1 <= n <= 103
 */
/**
 * O(1) solution.
 */
int sum_of_multiples(int n, int k)
{
    auto x = n / k;
    return k * (x * (x + 1) / 2);
}

class Solution {
public:
    int sumOfMultiples(int n) {
        return sum_of_multiples(n, 3)
            + sum_of_multiples(n, 5)
            + sum_of_multiples(n, 7)
            - sum_of_multiples(n, 3 * 5)
            - sum_of_multiples(n, 3 * 7)
            - sum_of_multiples(n, 5 * 7)
            + sum_of_multiples(n, 3 * 5 * 7);
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int sumOfMultiples(int n) {
        auto ans = 0;
        for (auto i = 3; i <= n; ++i) {
            for (auto d : {3, 5, 7}) {
                if (0 == (i % d)) {
                    ans += i;
                    break;
                }
            }
        }
        return ans;
    }
};
