/**
 * Unique Binary Search Trees
 *
 * Given n, how many structurally unique BST's (binary search trees) that store values 1 ... n?
 *
 * Example:
 *
 * Input: 3
 * Output: 5
 * Explanation:
 * Given n = 3, there are a total of 5 unique BST's:
 *
 *    1         3     3      2      1
 *     \       /     /      / \      \
 *      3     2     1      1   3      2
 *     /     /       \                 \
 *    2     1         2                 3
 */
/**
 * Improved one
 */
class Solution {
public:
    int numTrees(int n) {
        vector<int> ans(n + 1, 1);

        for (int i = 2; i <= n; ++i) {
            for (int j = 1; j <= i; ++j) {
                ans[i] += ans[j - 1] * ans[i - j];
            }
        }

        return ans[n];
    }
};

/**
 * Original solution. The idea is:
 * n = 1, ans = 1
 * n = 2, ans = 2
 * n = 3, things get interesting.
 *      if root = 1, then left subtree should be null, right subtree be formed with 2, 3 which can be 2 kinds, total
 *      kinds are 1 * 2 = 2
 *      if root = 2, then left subtree should 1, and right subtree should 3, and total kinds are 1 * 1 = 1
 *      if root = 3, same with first one, total kinds are 1 * 2 = 2
 *      sum the above together we get 2 + 1 + 2 = 5
 * n = 4, we do the same things
 *      if root = 1, left null, right can be formed with 3 numbers, can be 5 variaties
 *      if root = 2, left be 1, right can be formed with 2 numbers, can be 2 variaties
 *      ....
 * we do this recursive stuff to get the result
 */
class Solution {
public:
    int numTrees(int n) {
        vector<int> ans{1, 1, 2, 5, 14, 42};

        if (n >= ans.size()) {
            for (int i = ans.size(); i <= n; ++i) {
                int num = 0;
                for (int j = 1; j <= i; ++j) {
                    num += ans[j - 1] * ans[i - j];
                }
                ans.push_back(num);
            }
        }

        return ans[n];
    }
};
