/**
 * Element Appearing More Than 25% In Sorted Array
 *
 * Given an integer array sorted in non-decreasing order, there is exactly one integer in the array
 * that occurs more than 25% of the time, return that integer.
 *
 * Example 1:
 *
 * Input: arr = [1,2,2,6,6,6,6,7,10]
 * Output: 6
 * Example 2:
 *
 * Input: arr = [1,1]
 * Output: 1
 *
 * Constraints:
 *
 * 1 <= arr.length <= 104
 * 0 <= arr[i] <= 105
 */
/**
 * Faster version. Divide the array into 4 parts.
 */
class Solution {
public:
    int findSpecialInteger(vector<int>& arr) {
        int const n = arr.size();
        int f4_in_1 = n / 4;
        for (auto i : {n/4, n/2, n*3/4}) {
            auto p = equal_range(arr.begin(), arr.end(), arr[i]);
            if ((p.second - p.first) > f4_in_1) {
                return arr[i];
            }
        }

        return 0;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int findSpecialInteger(vector<int>& arr) {
        int const F4_1 = arr.size() / 4;
        unordered_map<int, int> num_cnt;
        for (auto n : arr) {
            ++num_cnt[n];
        }

        for (auto const& nc : num_cnt) {
            if (nc.second > F4_1) {
                return nc.first;
            }
        }

        return -1;
    }
};
