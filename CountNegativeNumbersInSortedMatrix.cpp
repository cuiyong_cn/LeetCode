/**
 * Count Negative Numbers in a Sorted Matrix
 *
 * Given a m x n matrix grid which is sorted in non-increasing order both row-wise and column-wise,
 * return the number of negative numbers in grid.
 *
 * Example 1:
 *
 * Input: grid = [[4,3,2,-1],[3,2,1,-1],[1,1,-1,-2],[-1,-1,-2,-3]]
 * Output: 8
 * Explanation: There are 8 negatives number in the matrix.
 * Example 2:
 *
 * Input: grid = [[3,2],[1,0]]
 * Output: 0
 * Example 3:
 *
 * Input: grid = [[1,-1],[-1,-1]]
 * Output: 3
 * Example 4:
 *
 * Input: grid = [[-1]]
 * Output: 1
 *
 * Constraints:
 *
 * m == grid.length
 * n == grid[i].length
 * 1 <= m, n <= 100
 * -100 <= grid[i][j] <= 100
 *
 * Follow up: Could you find an O(n + m) solution?
 */
/**
 * Using binary search to speed it up again.
 */
class Solution {
public:
    int countNegatives(vector<vector<int>>& grid) {
        int ans = 0;
        int idx = grid[0].size();
        for (auto const& row : grid) {
            auto it = upper_bound(row.begin(), row.begin() + idx, 0,
                                 [](auto const& a, auto const& b) {
                                     return b < a;
                                 });
            idx = distance(row.begin(), it);
            ans += row.size() - idx;
        }

        return ans;
    }
};

/**
 * More efficient one.
 */
class Solution {
public:
    int countNegatives(vector<vector<int>>& grid) {
        int ans = 0, neg_cnt = 0;
        int idx = grid[0].size() - 1;
        for (auto const& row : grid) {
            for (; idx >= 0; --idx) {
                if (row[idx] >= 0) {
                    break;
                }
                ++neg_cnt;
            }

            ans += neg_cnt;
        }

        return ans;
    }
};

/**
 * Brute force solution.
 */
class Solution {
public:
    int countNegatives(vector<vector<int>>& grid) {
        int ans = 0;
        for (auto const& row : grid) {
            for (int i = row.size() - 1; i >= 0; --i) {
                if (row[i] >= 0) {
                    break;
                }
                ++ans;
            }
        }

        return ans;
    }
};
