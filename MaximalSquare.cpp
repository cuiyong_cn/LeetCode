/**
 * Maximal Square
 *
 * Given an m x n binary matrix filled with 0's and 1's, find the largest square containing only 1's
 * and return its area.
 *
 * Example 1:
 *
 * Input: matrix = [["1","0","1","0","0"],["1","0","1","1","1"],["1","1","1","1","1"],["1","0","0","1","0"]]
 * Output: 4
 * Example 2:
 *
 * Input: matrix = [["0","1"],["1","0"]]
 * Output: 1
 * Example 3:
 *
 * Input: matrix = [["0"]]
 * Output: 0
 *
 * Constraints:
 *
 * m == matrix.length
 * n == matrix[i].length
 * 1 <= m, n <= 300
 * matrix[i][j] is '0' or '1'.
 */
/**
 * Reduce space usage.
 */
class Solution {
public:
    int maximalSquare(vector<vector<char>>& matrix) {
        int const m = matrix.size();
        int const n = matrix.front().size();

        vector<int> dp(n + 1, 0);
        int max_len = 0;
        int prev = 0;
        for (int i = 1; i <= m; ++i) {
            for (int j = 1; j <= n; ++j) {
                int temp = dp[j];
                if ('1' == matrix[i - 1][j - 1]) {
                    dp[j] = min(min(dp[j - 1], prev), dp[j]) + 1;
                    max_len = max(max_len, dp[j]);
                }
                else {
                    dp[j] = 0;
                }
                prev = temp;
            }
        }

        return max_len * max_len;
    }
};

/**
 * DP solution.
 */
class Solution {
public:
    int maximalSquare(vector<vector<char>>& matrix) {
        int const m = matrix.size();
        int const n = matrix.front().size();

        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
        int max_len = 0;
        for (int i = 1; i <= m; ++i) {
            for (int j = 1; j <= n; ++j) {
                if ('1' == matrix[i - 1][j - 1]) {
                    dp[i][j] = min(min(dp[i][j - 1], dp[i - 1][j]), dp[i - 1][j - 1]) + 1;
                    max_len = max(max_len, dp[i][j]);
                }
            }
        }

        return max_len * max_len;
    }
};

/**
 * Using binary search to speed up.
 */
class Solution {
public:
    int maximalSquare(vector<vector<char>>& matrix) {
        int const m = matrix.size();
        int const n = matrix.front().size();

        vector<vector<int>> prefix_sum(m + 1, vector<int>(n + 1, 0));
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                prefix_sum[i + 1][j + 1] = prefix_sum[i + 1][j] + ('1' == matrix[i][j] ? 1 : 0);
            }
        }

        for (int i = 2; i <= m; ++i) {
            for (int j = 1; j <= n; ++j) {
                prefix_sum[i][j] += prefix_sum[i - 1][j];
            }
        }

        int left = 1;
        int right = min(m, n);

        while (left <= right) {
            int mid = (left + right) / 2;
            int expect_area = mid * mid;
            bool can_be_formed = false;
            for (int i = 0; i <= (m - mid); ++i) {
                for (int j = 0; j <= (n - mid); ++j) {
                    if ('0' == matrix[i][j]) {
                        continue;
                    }

                    int rbx = i + mid;
                    int rby = j + mid;
                    int area = prefix_sum[rbx][rby] - prefix_sum[i][rby] - prefix_sum[rbx][j] + prefix_sum[i][j];

                    if (area == expect_area) {
                        can_be_formed = true;
                        goto update;
                    }
                }
            }
        update:
            if (can_be_formed) {
                left = mid + 1;
            }
            else {
                right = mid - 1;
            }
        }

        return right * right;
    }
};

/**
 * Original solution. Prefix sum.
 */
class Solution {
public:
    int maximalSquare(vector<vector<char>>& matrix) {
        int const m = matrix.size();
        int const n = matrix.front().size();

        vector<vector<int>> prefix_sum(m + 1, vector<int>(n + 1, 0));
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                prefix_sum[i + 1][j + 1] = prefix_sum[i + 1][j] + ('1' == matrix[i][j] ? 1 : 0);
            }
        }

        for (int i = 2; i <= m; ++i) {
            for (int j = 1; j <= n; ++j) {
                prefix_sum[i][j] += prefix_sum[i - 1][j];
            }
        }

        for (int L = min(m, n); L > 0; --L) {
            int expect_area = L * L;
            for (int i = 0; i < m; ++i) {
                if ((i + L) > m) {
                    break;
                }

                for (int j = 0; j < n; ++j) {
                    if ((j + L) > n) {
                        break;
                    }

                    if ('0' == matrix[i][j]) {
                        continue;
                    }

                    int rbx = i + L;
                    int rby = j + L;
                    int area = prefix_sum[rbx][rby] - prefix_sum[i][rby] - prefix_sum[rbx][j] + prefix_sum[i][j];
                    if (area == expect_area) {
                        return area;
                    }
                }
            }
        }

        return 0;
    }
};
