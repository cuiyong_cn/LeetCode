/**
 * Sum of Beauty of All Substrings
 *
 * The beauty of a string is the difference in frequencies between the most frequent and least
 * frequent characters.
 *
 * For example, the beauty of "abaacc" is 3 - 1 = 2.
 * Given a string s, return the sum of beauty of all of its substrings.
 *
 * Example 1:
 *
 * Input: s = "aabcb"
 * Output: 5
 * Explanation: The substrings with non-zero beauty are ["aab","aabc","aabcb","abcb","bcb"], each with beauty equal to 1.
 * Example 2:
 *
 * Input: s = "aabcbaa"
 * Output: 17
 *
 * Constraints:
 *
 * 1 <= s.length <= 500
 * s consists of only lowercase English letters.
 */
/**
 * Brute force. This approach is more efficient.
 */
class Solution {
public:
    int beautySum(string s) {
        int res = 0;
        for (auto i = 0; i < s.size(); ++i) {
            int cnt[26] = {}, max_f = 0, min_f = 0;
            for (auto j = i; j < s.size(); ++j) {
                int idx = s[j] - 'a';
                max_f = max(max_f, ++cnt[idx]);
                if (min_f >= cnt[idx] - 1) {
                    min_f = cnt[idx];
                    for (int k = 0; k < 26; ++k)
                        min_f = min(min_f, cnt[k] == 0 ? INT_MAX : cnt[k]);
                }
                res += max_f - min_f;
            }
        }
        return res;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int beautySum(string s) {
        int const char_cnt = 26;
        int const len = s.length();
        vector<vector<int>> ch_prefix_sum(len + 1, vector<int>(char_cnt, 0));

        for (int i = 0; i < len; ++i) {
            ch_prefix_sum[i + 1] = ch_prefix_sum[i];
            ++ch_prefix_sum[i + 1][s[i] - 'a'];
        }

        int ans = 0;

        for (int L = 3; L <= len; ++L) {
            int const R = len - L;
            for (int i = 0; i <= R; ++i) {
                int most = 0;
                int least = 501;
                int const k = i + L;
                for (int j = 0; j < char_cnt; ++j) {
                    auto freq = ch_prefix_sum[k][j] - ch_prefix_sum[i][j];
                    if (0 == freq) {
                        continue;
                    }

                    if (freq > most) {
                        most = freq;
                    }

                    if (freq < least) {
                        least = freq;
                    }
                }

                auto beauty = most - least;
                ans += beauty;
            }
        }

        return ans;
    }
};
