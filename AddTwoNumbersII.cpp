/** Add Two Numbers II
 *
 * You are given two non-empty linked lists representing two non-negative integers. The most significant digit comes
 * first and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.
 *
 * You may assume the two numbers do not contain any leading zero, except the number 0 itself.
 *
 * Follow up: What if you cannot modify the input lists? In other words, reversing the lists is not allowed.
 *
 * Example:
 *
 * Input: (7 -> 2 -> 4 -> 3) + (5 -> 6 -> 4) Output: 7 -> 8 -> 0 -> 7
 */
/**
 * Original solution. The idea is:
 * 3->4->5      4->3->5
 * "345"   +   "435" ->  0->8->7 -> 7->8->0
 *
 * My first solution is convert into int number, add them together. Converto to string and then form the listnode.
 * Then I find if the listnode is long enough that int and long int can not hold the number. Then the method will fail.
 * Therefore comes the above solution.
 *
 * one tricky point is the last carry bit manipulation.
 *
 * Also we can reverse the orignal two list node, then form the new one.
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        ListNode* head = nullptr;
        ListNode* tail = nullptr;
        string num1 = getNumString(l1);
        string num2 = getNumString(l2);
        if (num1.length() < num2.length()) swap(num1, num2);

        int diff = num1.length() - num2.length();
        int carry = 0;
        for (int i = num1.length() - 1; i >= 0; --i) {
            int n = num1[i] - '0' + carry;
            if (i >= diff) n += num2[i - diff] - '0';

            carry = n > 9 ? 1 : 0;
            n = n > 9 ? n - 10 : n;
            ListNode* node = new ListNode(n);
            if (nullptr == head) {
                head = node;
            }
            else {
                tail->next = node;
            }
            tail = node;
        }

        if (carry == 1) tail->next = new ListNode(1);

        return reverseList(head);
    }

    string getNumString(ListNode* node)
    {
        stringstream ss;
        while (node) {
            ss << node->val;
            node = node->next;
        }

        return ss.str();
    }

    ListNode* reverseList(ListNode* head) {
        ListNode* prev = nullptr;
        ListNode* cur = head;
        while (nullptr != cur) {
            ListNode* next = cur->next;
            cur->next = prev;
            prev = cur;
            cur = next;
        }
        return prev;
    }
};

/**
 * Same idea, but not reverse the List
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        ListNode* head = nullptr;
        ListNode* tail = nullptr;
        string num1 = getNumString(l1);
        string num2 = getNumString(l2);
        if (num1.length() < num2.length()) swap(num1, num2);

        int diff = num1.length() - num2.length();
        int carry = 0;
        for (int i = num1.length(); i >= 0; --i) {
            int n = num1[i] - '0' + carry;
            if (i >= diff) n += num2[i - diff] - '0';

            carry = n > 9 ? 1 : 0;
            n = n > 9 ? n - 10 : n;
            num1[i] = n + '0';
        }

        if (carry == 1) num1 = "1" + num1;

        return formList(num1);
    }

    string getNumString(ListNode* node)
    {
        stringstream ss;
        while (node) {
            ss << node->val;
            node = node->next;
        }

        return ss.str();
    }

    ListNode* formList(const string& num) {
        ListNode* head = new ListNode(num[0] - '0');
        ListNode* tail = head;
        for (int i = 1; i < num.length(); ++i) {
            tail->next = new ListNode(num[i] - '0');
            tail = tail->next;
        }
        return head;
    }
};
