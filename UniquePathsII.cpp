/**
 * Unique Paths II
 *
 * A robot is located at the top-left corner of a m x n grid (marked 'Start' in the diagram below).
 *
 * The robot can only move either down or right at any point in time. The robot is trying to reach
 * the bottom-right corner of the grid (marked 'Finish' in the diagram below).
 *
 * Now consider if some obstacles are added to the grids. How many unique paths would there be?
 *
 * An obstacle and space is marked as 1 and 0 respectively in the grid.
 *
 * Example 1:
 *
 * Input: obstacleGrid = [[0,0,0],[0,1,0],[0,0,0]]
 * Output: 2
 * Explanation: There is one obstacle in the middle of the 3x3 grid above.
 * There are two ways to reach the bottom-right corner:
 * 1. Right -> Right -> Down -> Down
 * 2. Down -> Down -> Right -> Right
 * Example 2:
 *
 * Input: obstacleGrid = [[0,1],[0,0]]
 * Output: 1
 *
 * Constraints:
 *
 * m == obstacleGrid.length
 * n == obstacleGrid[i].length
 * 1 <= m, n <= 100
 * obstacleGrid[i][j] is 0 or 1.
 */
/**
 * More short one.
 */
class Solution {
public:
    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
        if (1 == obstacleGrid[0][0]) {
            return 0;
        }

        int M = obstacleGrid.size();
        int N = obstacleGrid[0].size();

        vector<vector<int>> dp(M + 1, vector<int>(N + 1, 0));
        dp[0][1] = 1;

        for (int r = 1; r <= M; ++r) {
            for (int c = 1; c <= N; ++c) {
                if (0 == obstacleGrid[r - 1][c - 1]) {
                    dp[r][c] = dp[r - 1][c] + dp[r][c - 1];
                }
            }
        }

        return dp.back().back();
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
        if (1 == obstacleGrid[0][0]) {
            return 0;
        }

        int M = obstacleGrid.size();
        int N = obstacleGrid[0].size();

        vector<vector<int>> dp(M, vector<int>(N, 0));
        dp[0][0] = 1;

        // first row
        for (int c = 1; c < N; ++c) {
            if (1 == obstacleGrid[0][c]) {
                break;
            }
            dp[0][c] = dp[0][c - 1];
        }
        // first column
        for (int r = 1; r < M; ++r) {
            if (1 == obstacleGrid[r][0]) {
                break;
            }
            dp[r][0] = dp[r - 1][0];
        }

        // start from [1, 1]
        for (int r = 1; r < M; ++r) {
            for (int c = 1; c < N; ++c) {
                if (0 == obstacleGrid[r][c]) {
                    dp[r][c] = dp[r - 1][c] + dp[r][c - 1];
                }
            }
        }

        return dp.back().back();
    }
};
