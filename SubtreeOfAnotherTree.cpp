/**
 * Subtree of Another Tree
 *
 * Given two non-empty binary trees s and t, check whether tree t has exactly the same structure and node values with a
 * subtree of s. A subtree of s is a tree consists of a node in s and all of this node's descendants. The tree s could
 * also be considered as a subtree of itself.
 *
 * Example 1:
 * Given tree s:
 *
 *      3
 *     / \
 *    4   5
 *   / \
 *  1   2
 * Given tree t:
 *    4
 *   / \
 *  1   2
 * Return true, because t has the same structure and node values with a subtree of s.
 *
 * Example 2:
 * Given tree s:
 *
 *      3
 *     / \
 *    4   5
 *   / \
 *  1   2
 *     /
 *    0
 * Given tree t:
 *    4
 *   / \
 *  1   2
 * Return false.
 */
/**
 * Another great solution using preorder traversal of the binary tree.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    bool isSubtree(TreeNode* s, TreeNode* t) {
        auto pres = toPreorderString(s);
        auto pret = toPreorderString(t);

        return pres.find(pret) != string::npos;
    }

private:
    std::string toPreorderString(TreeNode* node)
    {
        if (!node) {
            return "null";
        }

        return "#" + std::to_string(node->val)
            + toPreorderString(node->left)
            + toPreorderString(node->right);
    }
};

/**
 * Improve the below solution.
 */

class Solution {
public:
    bool isSubtree(TreeNode* s, TreeNode* t) {
        getDepth(s, getDepth(t, -1));

        for (auto node : nodes) {
            if (isIdentical(node, t)) {
                return true;
            }
        }

        return false;
    }

private:
    bool isIdentical(TreeNode* t1, TreeNode* t2)
    {
        if (!t1 || !t2) {
            return !t1 && !t2;
        }

        return t1->val == t2->val
            && isIdentical(t1->left, t2->left)
            && isIdentical(t1->right, t2->right);
    }

    int getDepth(TreeNode* r, int d) {
        if (!r) {
            return -1;
        }

        int depth = max(getDepth(r->left, d), getDepth(r->right, d)) + 1;

        if (depth == d) {
            nodes.push_back(r);
        }

        return depth;
    }

    vector<TreeNode*> nodes;
};

/**
 * make it more compact
 */
class Solution {
public:
    bool isSubtree(TreeNode* s, TreeNode* t) {
        if (s) {
            return isIdentical(s, t) || isSubtree(s->left, t) || isSubtree(s->right, t);
        }

        return false;
    }

private:
    bool isIdentical(TreeNode* t1, TreeNode* t2)
    {
        if (!t1 || !t2) {
            return !t1 && !t2;
        }

        return t1->val == t2->val
            && isIdentical(t1->left, t2->left)
            && isIdentical(t1->right, t2->right);
    }
};

/**
 * Original solution.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    bool isSubtree(TreeNode* s, TreeNode* t) {
        if (s) {
            if (s->val == t->val) {
                if (isIdentical(s->left, t->left) && isIdentical(s->right, t->right)) {
                    return true;
                }
            }

            return isSubtree(s->left, t) || isSubtree(s->right, t);
        }

        return false;
    }

private:
    bool isIdentical(TreeNode* t1, TreeNode* t2)
    {
        if (!t1 || !t2) {
            if (!t1 && !t2) {
                return true;
            }

            return false;
        }

        if (t1->val == t2->val) {
            if (isIdentical(t1->left, t2->left) && isIdentical(t1->right, t2->right)) {
                return true;
            }
        }

        return false;
    }
};
