/**
 * Change Minimum Characters to Satisfy One of Three Conditions
 *
 * You are given two strings a and b that consist of lowercase letters. In one operation, you can
 * change any character in a or b to any lowercase letter.
 *
 * Your goal is to satisfy one of the following three conditions:
 *
 * Every letter in a is strictly less than every letter in b in the alphabet.
 * Every letter in b is strictly less than every letter in a in the alphabet.
 * Both a and b consist of only one distinct letter.
 * Return the minimum number of operations needed to achieve your goal.
 *
 * Example 1:
 *
 * Input: a = "aba", b = "caa"
 * Output: 2
 * Explanation: Consider the best way to make each condition true:
 * 1) Change b to "ccc" in 2 operations, then every letter in a is less than every letter in b.
 * 2) Change a to "bbb" and b to "aaa" in 3 operations, then every letter in b is less than every letter in a.
 * 3) Change a to "aaa" and b to "aaa" in 2 operations, then a and b consist of one distinct letter.
 * The best way was done in 2 operations (either condition 1 or condition 3).
 * Example 2:
 *
 * Input: a = "dabadd", b = "cda"
 * Output: 3
 * Explanation: The best way is to make condition 1 true by changing b to "eee".
 *
 * Constraints:
 *
 * 1 <= a.length, b.length <= 105
 * a and b consist only of lowercase letters.
 */
/**
 * My original attempt failed. The naive approach like below will not work:
 *
 * Find the lowerest value of b, then count the element in a that >= that value.
 *
 * This approach not working because we limits ourselves to only change a at a time.
 * But actally we can change a or b at a time.
 *
 * So the real solution from the discussion.
 */
class Solution {
public:
    int minCharacters(string const& a, string const& b) {
        int const LA = a.length();
        int const LB = b.length();
        int ans = LA + LB;
        auto cnt_a = count_char(a);
        auto cnt_b = count_char(b);

        for (int i = 0; i < cnt_a.size(); ++i) {
            ans= min(ans, LA + LB - cnt_a[i] - cnt_b[i]); // condition 3

            if (i > 0) {
                cnt_a[i] += cnt_a[i - 1];
                cnt_b[i] += cnt_b[i - 1];
            }

            if (i < 25) {
                ans = min(ans, LA - cnt_a[i] + cnt_b[i]); // condition 1
                ans = min(ans, LB - cnt_b[i] + cnt_a[i]); // condition 2
            }
        }

        return ans;
    }

private:
    vector<int> count_char(string const& a)
    {
        vector<int> cnt(26, 0);
        for_each(a.begin(), a.end(), [&cnt](auto const& c) { ++cnt[c - 'a']; });
        return cnt;
    }
};
