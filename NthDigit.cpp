/**
 * Nth Digit
 *
 * Given an integer n, return the nth digit of the infinite integer sequence [1, 2, 3, 4, 5, 6, 7,
 * 8, 9, 10, 11, ...].
 *
 * Example 1:
 *
 * Input: n = 3
 * Output: 3
 * Example 2:
 *
 * Input: n = 11
 * Output: 0
 * Explanation: The 11th digit of the sequence 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, ... is a 0, which
 * is part of the number 10.
 *
 * Constraints:
 *
 * 1 <= n <= 231 - 1
 */
/**
 * Increase the steps to make it faster.
 */
class Solution {
public:
    int findNthDigit(int n) {
        int base = 9;
        long digits = 1;
        while ((n - base * digits) > 0) {
            n -= base * digits;
            base *= 10;
            ++digits;
        }

        int index = (n - 1) % digits;
        int offset = (n - 1) / digits;
        long start = pow(10, digits - 1);
        return to_string(start + offset)[index] - '0';
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int findNthDigit(int n) {
        int i = 1;
        while (n > 0) {
            n -= digit_cnt(i);
            ++i;
        }

        n += digit_cnt(i - 1);
        string str = to_string(i - 1);
        return str[n - 1] - '0';
    }

private:
    int digit_cnt(int i)
    {
        int cnt = 0;
        if (i < 10) {
            cnt = 1;
        }
        else if (i < 100) {
            cnt = 2;
        }
        else if (i < 1000) {
            cnt = 3;
        }
        else if (i < 10000) {
            cnt = 4;
        }
        else if (i < 100000) {
            cnt = 5;
        }
        else if (i < 1000000) {
            cnt = 6;
        }
        else if (i < 10000000) {
            cnt = 7;
        }
        else if (i < 100000000) {
            cnt = 8;
        }
        else if (i < 1000000000) {
            cnt = 9;
        }
        else {
            cnt = 10;
        }

        return cnt;
    }
};
