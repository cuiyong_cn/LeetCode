/**
 * Duplicate Zeros
 *
 * Given a fixed length array arr of integers, duplicate each occurrence of zero,
 * shifting the remaining elements to the right.
 *
 * Note that elements beyond the length of the original array are not written.
 *
 * Do the above modifications to the input array in place, do not return anything from
 * your function.
 *
 * Example 1:
 *
 * Input: [1,0,2,3,0,4,5,0]
 * Output: null
 * Explanation: After calling your function, the input array is modified to: [1,0,0,2,3,0,0,4]
 *
 * Example 2:
 *
 * Input: [1,2,3]
 * Output: null
 * Explanation: After calling your function, the input array is modified to: [1,2,3]
 *
 * Note:
 *
 *     1 <= arr.length <= 10000
 *     0 <= arr[i] <= 9
 */
/**
 * More efficient one.
 */
class Solution {
public:
    void duplicateZeros(vector<int>& arr) {
        vector<int> copy = arr;
        int i = 0, j = 0;
        while (i < arr.size()) {
            arr[i] = copy[j];
            if (0 == copy[j]) {
                if (i < arr.size() - 1) {
                    arr[i + 1] = 0;
                    ++i;
                }
            }
            ++i; ++j;
        }
    }
};
/**
 * Not very efficient
 */
class Solution {
public:
    void duplicateZeros(vector<int>& arr) {
        for (int i = 0; i < arr.size(); ++i) {
            if (0 == arr[i]) {
                int start = i + 1;
                for (int j = arr.size() - 1; j > start; --j) {
                    swap(arr[j], arr[j - 1]);
                }
                arr[i + 1] = 0;
                ++i;
            }
        }
    }
};
