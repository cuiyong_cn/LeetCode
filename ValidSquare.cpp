/**
 * Valid Square
 *
 * Given the coordinates of four points in 2D space p1, p2, p3 and p4, return true if the four
 * points construct a square.
 *
 * The coordinate of a point pi is represented as [xi, yi]. The input is not given in any order.
 * A valid square has four equal sides with positive length and four equal angles (90-degree angles).
 *
 * Example 1:
 *
 * Input: p1 = [0,0], p2 = [1,1], p3 = [1,0], p4 = [0,1]
 * Output: true
 * Example 2:
 *
 * Input: p1 = [0,0], p2 = [1,1], p3 = [1,0], p4 = [0,12]
 * Output: false
 * Example 3:
 *
 * Input: p1 = [1,0], p2 = [-1,0], p3 = [0,1], p4 = [0,-1]
 * Output: true
 *
 * Constraints:
 *
 * p1.length == p2.length == p3.length == p4.length == 2
 * -104 <= xi, yi <= 104
 */
/**
 * Original solution.
 * At first, I sort the points. Then compare the 4 side lenths, and diagonal.
 */
class Solution {
public:
    bool validSquare(vector<int>& p1, vector<int>& p2, vector<int>& p3, vector<int>& p4) {
        vector<vector<int>> points{p1, p2, p3, p4};
        map<int, int> sidelen_count;
        for (int i = 0; i < 4; ++i) {
            for (int j = i + 1; j < 4; ++j) {
                if (points[i] == points[j]) {
                    return false;
                }

                int dx = points[i][0] - points[j][0];
                int dy = points[i][1] - points[j][1];
                ++sidelen_count[dx * dx + dy * dy];
            }
        }

        if (sidelen_count.size() != 2) {
            return false;
        }

        int count = 0;
        for (auto sc : sidelen_count) {
            count += sc.second;
        }

        return 6 == count;
    }
};
