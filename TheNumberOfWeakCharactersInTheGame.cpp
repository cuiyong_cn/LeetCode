/**
 * The Number of Weak Characters in the Game
 *
 * You are playing a game that contains multiple characters, and each of the characters has two main
 * properties: attack and defense. You are given a 2D integer array properties where properties[i] =
 * [attacki, defensei] represents the properties of the ith character in the game.
 *
 * A character is said to be weak if any other character has both attack and defense levels strictly
 * greater than this character's attack and defense levels. More formally, a character i is said to
 * be weak if there exists another character j where attackj > attacki and defensej > defensei.
 *
 * Return the number of weak characters.
 *
 * Example 1:
 *
 * Input: properties = [[5,5],[6,3],[3,6]]
 * Output: 0
 * Explanation: No character has strictly greater attack and defense than the other.
 * Example 2:
 *
 * Input: properties = [[2,2],[3,3]]
 * Output: 1
 * Explanation: The first character is weak because the second character has a strictly greater attack and defense.
 * Example 3:
 *
 * Input: properties = [[1,5],[10,4],[4,3]]
 * Output: 1
 * Explanation: The third character is weak because the second character has a strictly greater attack and defense.
 *
 * Constraints:
 *
 * 2 <= properties.length <= 105
 * properties[i].length == 2
 * 1 <= attacki, defensei <= 105
 */
/**
 * Improve again. Finnaly passed.
 */
class Solution {
public:
    int numberOfWeakCharacters(vector<vector<int>>& properties) {
        vector<int> max_defense(100002, 0);
        for (auto const& p : properties) {
            auto& def = max_defense[p[0]];
            def = max(def, p[1]);
        }

        for (int i = 100000; i > 0; --i) {
            max_defense[i - 1] = max(max_defense[i - 1], max_defense[i]);
        }

        int ans = 0;
        for (auto const& p : properties) {
            if (p[1] < max_defense[p[0] + 1]) {
                ++ans;
            }
        }

        return ans;
    }
};

/**
 * Improve again. Still TLE.
 */
class Solution {
public:
    int numberOfWeakCharacters(vector<vector<int>>& properties) {
        map<int, int> strongest;
        for (auto const& p : properties) {
            auto defense = strongest[p[0]];
            strongest[p[0]] = max(defense, p[1]);
        }

        int ans = 0;
        for (auto const& p : properties) {
            for (auto sit = strongest.rbegin(); sit != strongest.rend() && p[0] < sit->first; ++sit) {
                if (p[1] < sit->second) {
                    ++ans;
                    break;
                }
            }
        }

        return ans;
    }
};

/**
 * Improve. But still TLE.
 */
class Solution {
public:
    int numberOfWeakCharacters(vector<vector<int>>& properties) {
        sort(properties.begin(), properties.end());

        unordered_map<int, int> attack_cnt;
        for (auto const& p : properties) {
            ++attack_cnt[p[0]];
        }

        int ans = 0;
        for (auto const& p : properties) {
            int i = properties.size() - 1;
            while (i > 0 && p[0] < properties[i][0]) {
                if (p[1] < properties[i][1]) {
                    ++ans;
                    break;
                }
                else {
                    i -= attack_cnt[properties[i][0]];
                }
            }
        }

        return ans;
    }
};

/**
 * Original solution. TLE.
 */
class Solution {
public:
    int numberOfWeakCharacters(vector<vector<int>>& properties) {
        map<int, vector<int>> same_attack_group;
        for (auto const& p : properties) {
            same_attack_group[p[0]].emplace_back(p[1]);
        }

        for (auto& sag : same_attack_group) {
            sort(sag.second.begin(), sag.second.end());
        }

        auto ans = 0;
        for (auto const& p : properties) {
            auto attack_gt = upper_bound(same_attack_group.begin(), same_attack_group.end(), p[0],
                                        [](auto const& b1, auto const& b2) {
                                            return b1 < b2.first;
                                        });
            for (; attack_gt != same_attack_group.end(); ++attack_gt) {
                auto defense_gt = upper_bound(attack_gt->second.begin(), attack_gt->second.end(), p[1],
                                             [](auto const& b1, auto const& b2) {
                                                 return b1 < b2;
                                             });
                if (defense_gt != attack_gt->second.end()) {
                    ++ans;
                }
            }
        }

        return ans;
    }
};
