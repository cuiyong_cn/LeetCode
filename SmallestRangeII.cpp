/**
 * Smallest Range II
 *
 * You are given an integer array nums and an integer k.
 *
 * For each index i where 0 <= i < nums.length, change nums[i] to be either nums[i] + k or nums[i] - k.
 *
 * The score of nums is the difference between the maximum and minimum elements in nums.
 *
 * Return the minimum score of nums after changing the values at each index.
 *
 * Example 1:
 *
 * Input: nums = [1], k = 0
 * Output: 0
 * Explanation: The score is max(nums) - min(nums) = 1 - 1 = 0.
 * Example 2:
 *
 * Input: nums = [0,10], k = 2
 * Output: 6
 * Explanation: Change nums to be [2, 8]. The score is max(nums) - min(nums) = 8 - 2 = 6.
 * Example 3:
 *
 * Input: nums = [1,3,6], k = 3
 * Output: 3
 * Explanation: Change nums to be [4, 6, 3]. The score is max(nums) - min(nums) = 6 - 3 = 3.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 104
 * 0 <= nums[i] <= 104
 * 0 <= k <= 104
 */
/**
 * Original solution.
 * We can formalize the above concept: if A[i] < A[j], we don't need to consider when A[i] goes down
 * while A[j] goes up. This is because the interval (A[i] + K, A[j] - K) is a subset of (A[i] - K,
 * A[j] + K) (here, (a, b) for a > b denotes (b, a) instead.)
 *
 * That means that it is never worse to choose (up, down) instead of (down, up). We can prove this
 * claim that one interval is a subset of another, by showing both A[i] + K and A[j] - K are between
 * A[i] - K and A[j] + K.
 *
 * For sorted A, say A[i] is the largest i that goes up. Then A[0] + K, A[i] + K, A[i+1] - K,
 * A[A.length - 1] - K are the only relevant values for calculating the answer: every other value is
 * between one of these extremal values.
 */
class Solution {
public:
    int smallestRangeII(vector<int>& nums, int k) {
        sort(nums.begin(), nums.end());

        int ans = nums.back() - nums.front();
        int const N = nums.size();
        int const U = nums.front() + k;
        int const D = nums.back() - k;

        for (int i = 0; i < (N - 1); ++i) {
            int a = nums[i];
            int b = nums[i + 1];
            int high = max(D, a + k);
            int low = min(U, b - k);
            ans = min(ans, high - low);
        }

        return ans;
    }
};
