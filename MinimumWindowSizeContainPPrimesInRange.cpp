/**
 * Minimum window size containing atleast P primes in every window of given range
 *
 * Given three integers X, Y and P, the task is to find the minimum window size K such that every window in the range
 * [X, Y] of this size have atleast P prime numbers.
 *
 * Examples:
 *     Input: X = 2, Y = 8, P = 2
 *     Output: 4
 *     Explanation:
 *     In the range [2, 8], window size of 4 contains atleast 2 primes in each window.
 *     Possible Windows –
 *     {2, 3, 4, 5} – No of Primes = 3
 *     {3, 4, 5, 6} – No of Primes = 2
 *     {4, 5, 6, 7} – No of Primes = 2
 *     {5, 6, 7, 8} – No of Primes = 2
 *
 *     Input: X = 12, Y = 42, P = 3
 *     Output: 14
 *     Explanation:
 *     In the range [12, 42], window size of 14 contains atleast 3 primes in each window.
 */
// Function to check that a number is a prime or not in O(sqrt(N))
bool isPrime(int N)
{
    if (N < 2) return false;
    if (N < 4) return true;
    if ((N & 1) == 0) return false;
    if (N % 3 == 0) return false;

    int curr = 5, s = sqrt(N);

    // Loop to check if any number number is divisible by any other number or not
    while (curr <= s) {
        if (N % curr == 0) return false;
        curr += 2;
        if (N % curr == 0) return false;
        curr += 4;
    }

    return true;
}

// Function to check whether window size satisfies condition or not
bool check(int s, int p, int prefix_sum[], int n)
{
    bool satisfies = true;

    // Loop to check each window of size have atleast P primes
    for (int i = 0; i < n; i++) {
        if (i + s - 1 >= n) break;

        // Checking condition using prefix sum
        if (prefix_sum[i + s - 1] - (i - 1 >= 0 ? prefix_sum[i - 1] : 0) < p)
            satisfies = false;
    }

    return satisfies;
}

// Function to find the minimum window size possible for the given range in X and Y
int minimumWindowSize(int x, int y, int p)
{
    // Prefix array
    int prefix_sum[y - x + 1] = { 0 };

    // Mark those numbers which are primes as 1
    for (int i = x; i <= y; i++) {
        if (isPrime(i))
            prefix_sum[i - x] = 1;
    }

    // Convert to prefix sum
    for (int i = 1; i < y - x + 1; i++)
        prefix_sum[i] +=
              prefix_sum[i - 1];

    // Applying binary search over window size
    int low = 1, high = y - x + 1;
    int mid;
    while (high - low > 1) {
        mid = (low + high) / 2;

        // Check whether mid satisfies the condition or not
        if (check(mid, p, prefix_sum, y - x + 1)) {
            // If satisfies search in first half
            high = mid;
        }
        // Else search in second half
        else {
            low = mid;
        }
    }

    if (check(low, p, prefix_sum, y - x + 1)) return low;

    return high;
}
