/**
 * Check Completeness of a Binary Tree
 *
 * Given a binary tree, determine if it is a complete binary tree.
 *
 * Definition of a complete binary tree from Wikipedia: In a complete binary tree every level, except possibly the last,
 * is completely filled, and all nodes in the last level are as far left as possible. It can have between 1 and 2h nodes
 * inclusive at the last level h.
 *
 * Example 1:
 *
 * Input: [1,2,3,4,5,6] Output: true Explanation: Every level before the last is full (ie. levels with node-values {1}
 * and {2, 3}), and all nodes in the last level ({4, 5, 6}) are as far left as possible.
 *
 * Example 2:
 *
 * Input: [1,2,3,4,5,null,7] Output: false Explanation: The node with value 7 isn't as far left as possible.
 *
 * Note:
 *     The tree will have between 1 and 100 nodes.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/**
 * And more efficient one. Return as early as possible.
 */
class Solution
{
public:
    bool isCompleteTree(TreeNode* root) {
        deque<TreeNode*> q;
        q.push_back(root);

        bool isComplete = true;
        bool end = false;

        while (false == q.empty()) {
            TreeNode* cur = q.front(); q.pop_front();
            if (cur) {
                if (end) {
                    isComplete = false;
                    break;
                }
                q.push_back(cur->left);
                q.push_back(cur->right);
            }
            else {
                end = true;
            }
        }

        return isComplete;
    }
};

/**
 * After reading the discussion. Original solution stinks. Clean solutioin.
 */
class Solution
{
public:
    bool isCompleteTree(TreeNode* root) {
        vector<TreeNode*> bfs;
        bfs.push_back(root);
        int i = 0;
        while (i < bfs.size() && bfs[i]) {
            bfs.push_back(bfs[i]->left);
            bfs.push_back(bfs[i]->right);
            i++;
        }

        while (i < bfs.size() && !bfs[i]) i++;
        return i == bfs.size();
    }
};

/**
 * Original solution. Basic idea is:
 * If you check a nullptr node, then its previous nodes have to be all non-null, or its not complete.
 * If a level's non-null nodes are not equal to the supposed nodes count, it shoule be the last level. If we encount
 * this situation twice, then it is not complete.
 */
class Solution {
public:
    bool isCompleteTree(TreeNode* root) {
        int nodes = 1;
        int lastLvl = 0;
        deque<TreeNode*> q;
        q.push_back(root);

        while (false == q.empty()) {
            int nodeCount = q.size();
            nodes = nodes << 1;
            int index = 0;
            int nonNullNodes = 0;
            for (int i = 0; i < nodeCount; ++i) {
                TreeNode* n = q.front(); q.pop_front();
                ++index;
                if (nullptr != n->left) {
                    ++nonNullNodes;
                    if (nonNullNodes != index) {
                        return false;
                    }
                    q.push_back(n->left);
                }
                ++index;
                if (nullptr != n->right) {
                    ++nonNullNodes;
                    if (nonNullNodes != index) {
                        return false;
                    }
                    q.push_back(n->right);
                }
            }

            if (nonNullNodes != nodes && nonNullNodes > 0) {
                if (lastLvl > 0) return false;
                ++lastLvl;
            }
        }
        return true;
    }
};
