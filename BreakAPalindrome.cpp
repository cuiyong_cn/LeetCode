/**
 * Break a Palindrome
 *
 * Given a palindromic string palindrome, replace exactly one character by any lowercase English letter so that the
 * string becomes the lexicographically smallest possible string that isn't a palindrome.
 *
 * After doing so, return the final string.  If there is no way to do so, return the empty string.
 *
 * Example 1:
 *
 * Input: palindrome = "abccba"
 * Output: "aaccba"
 * Example 2:
 *
 * Input: palindrome = "a"
 * Output: ""
 *
 * Constraints:
 *
 * 1 <= palindrome.length <= 1000
 * palindrome consists of only lowercase English letters.
 */
/**
 * Slightly improve.
 */
class Solution {
public:
    string breakPalindrome(string palindrome) {
        if (1 == palindrome.length()) {
            return "";
        }

        int i = 0;
        int half_way = palindrome.length() / 2;
        for (; i < half_way; ++i) {
            if (palindrome[i] > 'a') {
                palindrome[i] = 'a';
                break;
            }
        }

        if (i == half_way) {
            palindrome.back() = 'b';
        }

        return palindrome;
    }
};
/**
 * Original solution.
 */
class Solution {
public:
    string breakPalindrome(string palindrome) {
        if (1 == palindrome.length()) {
            return "";
        }

        int i = 0;
        for (; i < palindrome.length(); ++i) {
            if (palindrome[i] > 'a') {
                if (i != (palindrome.length() / 2)) {
                    palindrome[i] = 'a';
                    break;
                }
            }
        }

        if (i == palindrome.length()) {
            palindrome.back() = 'b';
        }

        return palindrome;
    }
};
