/**
 * Total Hamming Distance
 *
 * The Hamming distance between two integers is the number of positions at which the corresponding bits are different.
 *
 * Now your job is to find the total Hamming distance between all pairs of the given numbers.
 *
 * Example:
 *
 * Input: 4, 14, 2
 *
 * Output: 6
 *
 * Explanation: In binary representation, the 4 is 0100, 14 is 1110, and 2 is 0010 (just
 * showing the four bits relevant in this case). So the answer will be:
 * HammingDistance(4, 14) + HammingDistance(4, 2) + HammingDistance(14, 2) = 2 + 2 + 2 = 6.
 *
 * Note:
 *     Elements of the given array are in the range of 0 to 10^9
 *     Length of the array will not exceed 10^4.
 */
/**
 * Improve the below solution.
 */
class Solution
{
public:
    int totalHammingDistance(vector<int>& nums)
    {
        ios_base::sync_with_stdio(0);
        cin.tie(0);
        cout.tie(0);

        int n = nums.size();
        int ans = 0;
        int a[32] = {0};
        for (int i = 0; i < n ; i++)
            for (int j = 0; j < 32; j++)
                a[j] += nums[i] & (1 << j);

        for (int i = 0; i < 32; i++) ans += (a[i]) * (n - a[i]);

        return ans;
    }
};

/**
 * TLE solution. But code is simple and clear.
 */
class Solution
{
public:
    int totalHammingDistance(vector<int>& nums)
    {
        int ans = 0;

        for (size_t i = 0; i < nums.size(); ++i) {
            for (size_t j = i + 1; j < nums.size(); ++j) {
                std::bitset<sizeof(int) * 8> bs(nums[i] ^ nums[j]);
                ans += bs.count();
            }
        }

        return ans;
    }
};

