/**
 * Concatenation of Consecutive Binary Numbers
 *
 * Given an integer n, return the decimal value of the binary string formed by concatenating the binary representations
 * of 1 to n in order, modulo 109 + 7.
 *
 * Example 1:
 *
 * Input: n = 1
 * Output: 1
 * Explanation: "1" in binary corresponds to the decimal value 1.
 * Example 2:
 *
 * Input: n = 3
 * Output: 27
 * Explanation: In binary, 1, 2, and 3 corresponds to "1", "10", and "11".
 * After concatenating them, we have "11011", which corresponds to the decimal value 27.
 * Example 3:
 *
 * Input: n = 12
 * Output: 505379714
 * Explanation: The concatenation results in "1101110010111011110001001101010111100".
 * The decimal value of that is 118505380540.
 * After modulo 109 + 7, the result is 505379714.
 *
 * Constraints:
 *
 * 1 <= n <= 105
 */
/**
 * Improve the method of find the binary length of i.
 */
class Solution {
public:
    Solution() : mod{1'000'000'000 + 7}
    {

    }

    int concatenatedBinary(int n) {
        long ans = 0;
        int len = 0;
        for (int i = 1; i <= n; ++i) {
            if (0 == (i & (i - 1))) {
                // i is power of 2
                ++len;
            }

            ans = (ans << len) + i;
            ans = ans % mod;
        }

        return ans;
    }

private:
    const int mod;
};
/**
 * Revise below solution.
 */
class Solution {
public:
    Solution() : mod{1'000'000'000 + 7}
    {

    }

    int concatenatedBinary(int n) {
        long ans = 0;
        for (int i = 1; i <= n; ++i) {
            ans = (ans << binary_length(i)) + i;
            ans = ans % mod;
        }

        return ans;
    }

private:
    int binary_length(unsigned long n)
    {
        int len = 0;
        do {
            ++len;
        } while (n >>= 1);

        return len;
    }

    const int mod;
};
/**
 * Original solution. TLE of course.
 */
class Solution {
public:
    Solution() : mod{1'000'000'000 + 7}
    {

    }

    int concatenatedBinary(int n) {
        string ans = "1";

        for (int i = 2; i <= n; ++i) {
            ans += to_binary_string(i);
            ans = to_binary_string(bitset<64>(ans).to_ulong() % mod);
        }

        return bitset<32>(ans).to_ulong();
    }

private:
    string to_binary_string(unsigned long n)
    {
        char bArray[(sizeof(unsigned long) * 8) + 1];

        unsigned index  = sizeof(unsigned long) * 8;

        char temp = 0;
        bArray[index] = '\0';

        do {
            bArray[--index] = (n & 1) + '0';
        } while (n >>= 1);

        return string(bArray + index);
    }

    const int mod;
};

