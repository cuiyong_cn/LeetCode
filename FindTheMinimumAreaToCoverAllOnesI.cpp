/**
 * Find the Minimum Area to Cover All Ones I
 *
 * You are given a 2D binary array grid. Find a rectangle with horizontal and vertical sides with
 * the smallest area, such that all the 1's in grid lie inside this rectangle.
 *
 * Return the minimum possible area of the rectangle.
 *
 * Example 1:
 *
 * Input: grid = [[0,1,0],[1,0,1]]
 *
 * Output: 6
 *
 * Explanation:
 *
 * The smallest rectangle has a height of 2 and a width of 3, so it has an area of 2 * 3 = 6.
 *
 * Example 2:
 *
 * Input: grid = [[1,0],[0,0]]
 *
 * Output: 1
 *
 * Explanation:
 *
 * The smallest rectangle has both height and width 1, so its area is 1 * 1 = 1.
 *
 * Constraints:
 *     1 <= grid.length, grid[i].length <= 1000
 *     grid[i][j] is either 0 or 1.
 *     The input is generated such that there is at least one 1 in grid.
 */
/**
 * Original solution.
 */
class Solution {
public:
    int minimumArea(vector<vector<int>>& grid) {
        auto min_x = 2000;
        auto min_y = min_x;
        auto max_x = 0;
        auto max_y = 0;

        auto const xend = grid.size();
        auto const yend = grid[0].size();

        for (auto i = 0uz; i < xend; ++i) {
            for (auto j = 0uz; j < yend; ++j) {
                if (1 == grid[i][j]) {
                    min_x = min<int>(min_x, i);
                    min_y = min<int>(min_y, j);
                    max_x = max<int>(max_x, i);
                    max_y = max<int>(max_y, j);
                }
            }
        }

        return (max_x - min_x + 1) * (max_y - min_y + 1);
    }
};
