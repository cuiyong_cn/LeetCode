/**
 * Word Pattern
 *
 * Given a pattern and a string s, find if s follows the same pattern.
 *
 * Here follow means a full match, such that there is a bijection between a letter in pattern and a
 * non-empty word in s.
 *
 * Example 1:
 *
 * Input: pattern = "abba", s = "dog cat cat dog"
 * Output: true
 * Example 2:
 *
 * Input: pattern = "abba", s = "dog cat cat fish"
 * Output: false
 * Example 3:
 *
 * Input: pattern = "aaaa", s = "dog cat cat dog"
 * Output: false
 * Example 4:
 *
 * Input: pattern = "abba", s = "dog dog dog dog"
 * Output: false
 *
 * Constraints:
 *
 * 1 <= pattern.length <= 300
 * pattern contains only lower-case English letters.
 * 1 <= s.length <= 3000
 * s contains only lower-case English letters and spaces ' '.
 * s does not contain any leading or trailing spaces.
 * All the words in s are separated by a single space.
 */
/**
 * A lot shorter one.
 */
class Solution {
public:
    bool wordPattern(string pattern, string str) {
        using Word = std::string;
        using Letter = char;

        unordered_map<Letter, int> p2i;
        unordered_map<Word, int> w2i;

        istringstream iss(str);
        int i;
        for (i = 0; !iss.eof(); ++i) {
            Word word; iss >> word;
            auto letter = pattern[i];
            if (i == pattern.length() || p2i[letter] != w2i[word]) {
                return false;
            }

            p2i[letter] = i + 1;
            w2i[word] = i + 1;
        }

        return i == pattern.length();
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool wordPattern(string pattern, string s) {
        using Word = std::string;
        using Letter = char;

        unordered_map<Letter, Word> letter_word;
        unordered_map<Word, Letter> word_letter;

        istringstream iss{s};
        int i = 0;
        while (i < pattern.length() && !iss.eof()) {
            Word word; iss >> word;
            auto letter = pattern[i++];
            if (letter_word.count(letter)) {
                if (letter_word[letter] != word) {
                    return false;
                }
            }
            else {
                if (word_letter.count(word)) {
                    return false;
                }
                letter_word[letter] = word;
                word_letter[word] = letter;
            }
        }

        if (i < pattern.length() || !iss.eof()) {
            return false;
        }

        return true;
    }
};
