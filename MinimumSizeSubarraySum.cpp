/**
 * Minimum Size Subarray Sum
 *
 * Given an array of positive integers nums and a positive integer target, return the minimal length
 * of a contiguous subarray [numsl, numsl+1, ..., numsr-1, numsr] of which the sum is greater than
 * or equal to target. If there is no such subarray, return 0 instead.
 *
 * Example 1:
 *
 * Input: target = 7, nums = [2,3,1,2,4,3]
 * Output: 2
 * Explanation: The subarray [4,3] has the minimal length under the problem constraint.
 * Example 2:
 *
 * Input: target = 4, nums = [1,4,4]
 * Output: 1
 * Example 3:
 *
 * Input: target = 11, nums = [1,1,1,1,1,1,1,1]
 * Output: 0
 *
 * Constraints:
 *
 * 1 <= target <= 109
 * 1 <= nums.length <= 105
 * 1 <= nums[i] <= 105
 *
 * Follow up: If you have figured out the O(n) solution, try coding another solution of which the
 * time complexity is O(n log(n)).
 */
/**
 * Make binary search shorter.
 */
class Solution {
public:
    int minSubArrayLen(int s, vector<int>& nums)
    {
        int n = nums.size();
        int ans = numeric_limits<int>::max();
        vector<int> sums(n + 1, 0);
        partial_sum(begin(nums), end(nums), begin(sums) + 1);

        for (int i = 1; i <= n; i++) {
            int to_find = s + sums[i - 1];
            auto bound = lower_bound(sums.begin(), sums.end(), to_find);
            if (bound != sums.end()) {
                ans = min<int>(ans, distance(begin(sums) + i - 1, bound));
            }
        }

        return numeric_limits<int>::max() == ans ? 0 : ans;
    }
};

/**
 * Binary search solution.
 */
class Solution {
public:
    int minSubArrayLen(int target, vector<int>& nums) {
        int left = 1;
        int right = nums.size();

        vector<int> prefix_sum(right + 1, 0);
        partial_sum(begin(nums), end(nums), begin(prefix_sum) + 1);

        while (left <= right) {
            int mid = (left + right) / 2;
            int end = prefix_sum.size() - mid;
            bool possible = false;
            for (int i = 0; i < end; ++i) {
                int sum = prefix_sum[i + mid] - prefix_sum[i];
                if (sum >= target) {
                    possible = true;
                    break;
                }
            }

            if (possible) {
                right = mid - 1;
            }
            else {
                left = mid + 1;
            }
        }

        return right == nums.size() ? 0 : right + 1;
    }
};

/**
 * Original solution. O(n) solution.
 */
class Solution {
public:
    int minSubArrayLen(int target, vector<int>& nums) {
        int min_len = numeric_limits<int>::max();
        int sum = 0;
        int left = 0;

        for (size_t i = 0; i < nums.size(); ++i) {
            sum += nums[i];
            if (sum >= target) {
                for (; left <= i && sum >= target; ++left) {
                    sum -= nums[left];
                }
                min_len = min<int>(min_len, i - left + 2);
            }
        }

        return numeric_limits<int>::max() == min_len ? 0 : min_len;
    }
};
