/**
 * Customers Who Never Order
 *
 * Suppose that a website contains two tables, the Customers table and the Orders table. Write a SQL query to find all
 * customers who never order anything.
 *
 * Table: Customers.
 *
 * +----+-------+
 * | Id | Name  |
 * +----+-------+
 * | 1  | Joe   |
 * | 2  | Henry |
 * | 3  | Sam   |
 * | 4  | Max   |
 * +----+-------+
 *
 * Table: Orders.
 *
 * +----+------------+
 * | Id | CustomerId |
 * +----+------------+
 * | 1  | 3          |
 * | 2  | 1          |
 * +----+------------+
 *
 * Using the above tables as example, return the following:
 *
 * +-----------+
 * | Customers |
 * +-----------+
 * | Henry     |
 * | Max       |
 * +-----------+
 */
/**
 * Left join version
 */
SELECT a.Name as Customers
  FROM Customers a
  LEFT JOIN Orders b
    On a.Id = b.CustomerId
 WHERE a.CustomerId is NULL;

/**
 * Original solution.
 */
# Write your MySQL query statement below
SELECT a.name as customers
  FROM customers a
 WHERE a.id not in
 (
    SELECT b.customerid FROM orders b
 );
