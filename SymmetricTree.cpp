/**
 * Symmetric Tree
 *
 * Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).
 *
 * For example, this binary tree [1,2,2,3,4,4,3] is symmetric:
 *
 *     1
 *    / \
 *   2   2
 *  / \ / \
 * 3  4 4  3
 *
 *
 * But the following [1,2,2,null,3,null,3] is not:
 *
 *     1
 *    / \
 *   2   2
 *    \   \
 *    3    3
 *
 * Follow up: Solve it both recursively and iteratively.
 */
/**
 * Iterative one.
 */
class Solution {
public:
    bool isSymmetric(TreeNode* root) {
        deque<std::pair<TreeNode*, TreeNode*>> q;

        if (root) {
            q.push_back({root->left, root->right});
        }

        while (!q.empty()) {
            auto [node1, node2] = q.front();
            q.pop_front();
            if (node1 && node2) {
                if (node1->val == node2->val) {
                    q.push_back({node1->left, node2->right});
                    q.push_back({node1->right, node2->left});
                }
                else {
                    return false;
                }
            }
            else if (node1 || node2) {
                return false;
            }
        }

        return true;
    }
};

/**
 * Original solution. recursive one.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    bool isSymmetric(TreeNode* root) {
        return is_symmetric(root, root);
    }

private:
    bool is_symmetric(TreeNode* node1, TreeNode* node2)
    {
        if (node1 && node2) {
            if (node1->val == node2->val) {
                if (is_symmetric(node1->left, node2->right) && is_symmetric(node1->right, node2->left)) {
                    return true;
                }

                return false;
            }

            return false;
        }

        return !node1 && !node2;
    }
};
