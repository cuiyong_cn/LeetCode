/**
 * Koko Eating Bananas
 *
 * Koko loves to eat bananas.  There are N piles of bananas, the i-th pile has piles[i] bananas.  The guards have gone
 * and will come back in H hours.
 *
 * Koko can decide her bananas-per-hour eating speed of K.  Each hour, she chooses some pile of bananas, and eats K
 * bananas from that pile.  If the pile has less than K bananas, she eats all of them instead, and won't eat any more
 * bananas during this hour.
 *
 * Koko likes to eat slowly, but still wants to finish eating all the bananas before the guards come back.
 *
 * Return the minimum integer K such that she can eat all the bananas within H hours.
 *
 * Example 1:
 *
 * Input: piles = [3,6,7,11], H = 8 Output: 4
 *
 * Example 2:
 *
 * Input: piles = [30,11,23,4,20], H = 5 Output: 30
 *
 * Example 3:
 *
 * Input: piles = [30,11,23,4,20], H = 6 Output: 23
 *
 * Note:
 *     1 <= piles.length <= 10^4
 *     piles.length <= H <= 10^9
 *     1 <= piles[i] <= 10^9
 */
/**
 * So the direction is right, but modulus operation is really expensive.
 * eliminate the modulus
 */
class Solution {
public:
    int minEatingSpeed(vector<int>& piles, int H) {
        int l = 1, r = 1000000000;
        while (l < r) {
            int m = (l + r) / 2, total = 0;
            for (int p : piles) total += (p + m - 1) / m;
            if (total > H) l = m + 1; else r = m;
        }

        return l;
    }
};

/**
 * Using binary search to speed things up. And this one passed.
 */
class Solution {
public:
    int minEatingSpeed(vector<int>& piles, int H) {
        sort(piles.begin(), piles.end());
        int K = 0;
        int baseH = piles.size();
        if (baseH == H) {
            return piles.empty() ? 0 : piles.back();
        }

        int si = piles.size() - 1 - (H - baseH);
        si = si < 0 ? 0 : si;
        int left = piles.back() / (H - baseH + 1) + (piles.back() % (H - baseH + 1) ? 1 : 0);
        int right = piles.back();

        while (left < right) {
            int mk = (left + right) / 2;
            int costs = 0;

            for (auto b : piles) {
                costs += b / mk + (b % mk ? 1 : 0);
            }

            if (costs <= H) {
                right = mk;
                K = mk;
            }
            else {
                left = mk + 1;
            }
        }

        return K;
    }
};

/**
 * Original solution. TLE
 */
class Solution {
public:
    int minEatingSpeed(vector<int>& piles, int H) {
        sort(piles.begin(), piles.end());
        int K = 0;
        int baseH = piles.size();
        if (baseH == H) {
            return piles.empty() ? 0 : piles.back();
        }

        int si = piles.size() - 1 - (H - baseH);
        si = si < 0 ? 0 : si;
        int mk = piles.back() / (H - baseH + 1) + (piles.back() % (H - baseH + 1) ? 1 : 0);

        for (int i = mk; ; ++i) {
            int feasible = true;
            int costs = 0;
            for (auto b : piles) {
                costs += b / i + (b % i ? 1 : 0);
                if (costs > H) {
                    feasible = false;
                    break;
                }
            }

            if (feasible) {
                K = i;
                break;
            }
        }

        return K;
    }
};
