/**
 * Top K Frequent Words
 *
 * Given a non-empty list of words, return the k most frequent elements.
 *
 * Your answer should be sorted by frequency from highest to lowest. If two words have the same frequency, then the word
 * with the lower alphabetical order comes first.
 *
 * Example 1:
 *
 * Input: ["i", "love", "leetcode", "i", "love", "coding"], k = 2
 * Output: ["i", "love"]
 * Explanation: "i" and "love" are the two most frequent words.
 *     Note that "i" comes before "love" due to a lower alphabetical order.
 *
 * Example 2:
 *
 * Input: ["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"], k = 4
 * Output: ["the", "is", "sunny", "day"]
 * Explanation: "the", "is", "sunny" and "day" are the four most frequent words,
 *     with the number of occurrence being 4, 3, 2 and 1 respectively.
 *
 * Note:
 *     You may assume k is always valid, 1 ≤ k ≤ number of unique elements.
 *     Input words contain only lowercase letters.
 *
 * Follow up:
 *     Try to solve it in O(n log k) time and O(n) extra space.
 */
/**
 * Original solution.
 * Also we can use priority_queue to do this
 */
class Solution {
public:
    struct StrFreq
    {
        string word;
        int count;
    };

    vector<string> topKFrequent(vector<string>& words, int k) {
        unordered_map<string, int> strFreq;
        vector<StrFreq> vecStrFreq;

        for_each(words.begin(), words.end(), [&strFreq](auto& w) {
            ++strFreq[w];
        });

        for_each(strFreq.begin(), strFreq.end(), [&vecStrFreq](auto& p) {
            vecStrFreq.push_back({p.first, p.second});
        });

        sort(vecStrFreq.begin(), vecStrFreq.end(), [](auto& p1, auto&p2) {
            if (p1.count > p2.count) {
                return true;
            }
            else if (p1.count == p2.count) {
                if (p1.word < p2.word) {
                    return true;
                }
            }

            return false;
        });

        vector<string> ans;
        for (int i = 0; i < k; ++i) {
            ans.push_back(vecStrFreq[i].word);
        }

        return ans;
    }
};
