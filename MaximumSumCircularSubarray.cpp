/**
 * Maximum Sum Circular Subarray
 *
 * Given a circular integer array nums of length n, return the maximum possible sum of a non-empty
 * subarray of nums.
 *
 * A circular array means the end of the array connects to the beginning of the array. Formally, the
 * next element of nums[i] is nums[(i + 1) % n] and the previous element of nums[i] is nums[(i - 1 +
 * n) % n].
 *
 * A subarray may only include each element of the fixed buffer nums at most once. Formally, for a
 * subarray nums[i], nums[i + 1], ..., nums[j], there does not exist i <= k1, k2 <= j with k1 % n ==
 * k2 % n.
 *
 * Example 1:
 *
 * Input: nums = [1,-2,3,-2]
 * Output: 3
 * Explanation: Subarray [3] has maximum sum 3
 * Example 2:
 *
 * Input: nums = [5,-3,5]
 * Output: 10
 * Explanation: Subarray [5,5] has maximum sum 5 + 5 = 10
 * Example 3:
 *
 * Input: nums = [3,-1,2,-1]
 * Output: 4
 * Explanation: Subarray [2,-1,3] has maximum sum 2 + (-1) + 3 = 4
 * Example 4:
 *
 * Input: nums = [3,-2,2,-3]
 * Output: 3
 * Explanation: Subarray [3] and [3,-2,2] both have maximum sum 3
 * Example 5:
 *
 * Input: nums = [-2,-3,-1]
 * Output: -1
 * Explanation: Subarray [-1] has maximum sum -1
 *
 * Constraints:
 *
 * n == nums.length
 * 1 <= n <= 3 * 104
 * -3 * 104 <= nums[i] <= 3 * 104
 */
/**
 * One pass.
 */
class Solution {
public:
    int maxSubarraySumCircular(vector<int>& A) {
        int total = 0, maxSum = A[0], curMax = 0, minSum = A[0], curMin = 0;
        for (int& a : A) {
            curMax = max(curMax + a, a);
            maxSum = max(maxSum, curMax);
            curMin = min(curMin + a, a);
            minSum = min(minSum, curMin);
            total += a;
        }
        return maxSum > 0 ? max(maxSum, total - minSum) : maxSum;
    }
};

/**
 * Still using kadane algo.
 * Circular array A[j]+ A[j + 1] + A[A.size() - 1] + A[0] + A[1]+ A[i], with j >= i + 2 can be
 * viewed as (A[0] + A[1] + ... + A[A.size() - 1]) - (A[i + 1] + A[i + 2] + ... + A[j - 1])
 */
int kadane(vector<int> const& A, int i , int j, int sign)
{
    if (j < i) {
        return 0;
    }

    int ans = numeric_limits<int>::min();
    int cur = ans;
    for (int k = i; k <= j; ++k) {
        cur = sign * A[k] + max(cur, 0);
        ans = max(ans, cur);
    }

    return ans;
}

class Solution {
public:
    int maxSubarraySumCircular(vector<int>& A) {
        int const N = A.size();
        int S = accumulate(A.begin(), A.end(), 0);

        int ans1 = kadane(A, 0, N - 1, 1);
        int ans2 = S + kadane(A, 1, N - 1, -1);
        int ans3 = S + kadane(A, 0, N - 2, -1);
        return max(ans1, max(ans2, ans3));
    }
};

/**
 * Using prefix sum. Circular Array A can be viewed as non-circular A + A
 */
class Solution {
public:
    int maxSubarraySumCircular(vector<int>& A) {
        int const N = A.size();

        // Compute P[j] = B[0] + B[1] + ... + B[j-1]
        // for fixed array B = A+A
        vector<int> P(2 * N + 1, 0);
        for (int i = 0; i < 2*N; ++i) {
            P[i + 1] = P[i] + A[i % N];
        }

        // Want largest P[j] - P[i] with 1 <= j-i <= N
        // For each j, want smallest P[i] with i >= j-N
        int ans = A[0];
        // de: i's, increasing by P[i]
        deque<int> deq = {0};
        for (int j = 1; j <= 2*N; ++j) {
            // If the smallest i is too small, remove it.
            if (deq.front() < (j - N)) {
                deq.pop_front();
            }

            // The optimal i is deque[0], for cand. answer P[j] - P[i].
            ans = max(ans, P[j] - P[deq.front()]);

            // Remove any i1's with P[i2] <= P[i1].
            while (!deq.empty() && P[j] <= P[deq.back()]) {
                deq.pop_back();
            }

            deq.push_back(j);
        }

        return ans;
    }
};

/**
 * Original solution. Refer to kadane's algorithm
 */
class Solution {
public:
    int maxSubarraySumCircular(vector<int>& A) {
        int const N = A.size();

        int ans = A[0];
        int cur = A[0];
        for (int i = 1; i < N; ++i) {
            cur = A[i] + max(cur, 0);
            ans = max(ans, cur);
        }

        // ans is the answer for 1-interval subarrays.
        // Now, let's consider all 2-interval subarrays.
        // For each i, we want to know
        // the maximum of sum(A[j:]) with j >= i+2

        // rightsums[i] = A[i] + A[i+1] + ... + A[N-1]
        vector<int> rightsums(N, 0);
        rightsums.back() = A.back();
        for (int i = N-2; i >= 0; --i) {
            rightsums[i] = rightsums[i+1] + A[i];
        }

        // maxright[i] = max_{j >= i} rightsums[j]
        vector<int> maxright(N, 0);
        maxright.back() = A.back();
        for (int i = N-2; i >= 0; --i) {
            maxright[i] = max(maxright[i+1], rightsums[i]);
        }

        int leftsum = 0;
        for (int i = 0; i < N-2; ++i) {
            leftsum += A[i];
            ans = max(ans, leftsum + maxright[i+2]);
        }

        return ans;
    }
};
