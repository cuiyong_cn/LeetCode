/**
 * Generate Random Point in a Circle
 *
 * Given the radius and the position of the center of a circle, implement the function randPoint
 * which generates a uniform random point inside the circle.
 *
 * Implement the Solution class:
 *
 * Solution(double radius, double x_center, double y_center) initializes the object with the radius
 * of the circle radius and the position of the center (x_center, y_center).  randPoint() returns a
 * random point inside the circle. A point on the circumference of the circle is considered to be in
 * the circle. The answer is returned as an array [x, y].
 *
 * Example 1:
 *
 * Input
 * ["Solution", "randPoint", "randPoint", "randPoint"]
 * [[1.0, 0.0, 0.0], [], [], []]
 * Output
 * [null, [-0.02493, -0.38077], [0.82314, 0.38945], [0.36572, 0.17248]]
 *
 * Explanation
 * Solution solution = new Solution(1.0, 0.0, 0.0);
 * solution.randPoint(); // return [-0.02493, -0.38077]
 * solution.randPoint(); // return [0.82314, 0.38945]
 * solution.randPoint(); // return [0.36572, 0.17248]
 *
 * Constraints:
 *
 * 0 < radius <= 108
 * -107 <= x_center, y_center <= 107
 * At most 3 * 104 calls will be made to randPoint.
 */
/**
 * Correct one. The reason is that. Look at the picture of the circle.
 * If r changes to r + delta for some theta angle. The area of the fan shape is
 * not increas in linear. As the area's formular is theta * Pi * r^2 / 2
 * So that when we chose r, we have to sqrt it.
 */
class Solution {
public:
    Solution(double radius, double x_center, double y_center)
        : radius{radius}, xc{x_center}, yc{y_center}, rd{}, rand_engine{rd()},  uniform_rand_double{0, std::nextafter(1, std::numeric_limits<double>::max())} {
    }
    
    vector<double> randPoint() {
        const double PI = 3.14159265358979732384626433832795;
        double theta = 2 * PI * uniform_rand_double(rand_engine);
        double r = sqrt(uniform_rand_double(rand_engine));
        return {xc + r * radius * cos(theta), yc + r * radius * sin(theta)};
    }

private:
    double radius;
    double xc;
    double yc;
    std::random_device rd;
    std::mt19937 rand_engine;
    std::uniform_real_distribution<double> uniform_rand_double;
};

/**
 * Original solution. This is not correct.
 */
class Solution {
public:
    Solution(double radius, double x_center, double y_center)
        : xc{x_center}, yc{y_center}, rd{}, rand_engine{rd()},  uniform_rand_double{-radius, std::nextafter(radius, std::numeric_limits<double>::max())} {
    }
    
    vector<double> randPoint() {
        return {uniform_rand_double(rand_engine) + xc, uniform_rand_double(rand_engine) + yc};
    }

private:
    double xc;
    double yc;
    std::random_device rd;
    std::mt19937 rand_engine;
    std::uniform_real_distribution<double> uniform_rand_double;
};

/**
 * Your Solution object will be instantiated and called as such:
 * Solution* obj = new Solution(radius, x_center, y_center);
 * vector<double> param_1 = obj->randPoint();
 */
