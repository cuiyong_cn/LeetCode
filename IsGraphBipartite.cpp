/**
 * Is Graph Bipartite?
 *
 * Given an undirected graph, return true if and only if it is bipartite.
 *
 * Recall that a graph is bipartite if we can split it's set of nodes into two independent subsets A and B such that every edge in the graph has one node in A and another node in B.
 *
 * The graph is given in the following form: graph[i] is a list of indexes j for which the edge between nodes i and j exists.  Each node is an integer between 0 and graph.length - 1.  There are no self edges or parallel edges: graph[i] does not contain i, and it doesn't contain any element twice.
 *
 * Example 1:
 * Input: [[1,3], [0,2], [1,3], [0,2]]
 * Output: true
 * Explanation:
 * The graph looks like this:
 * 0----1
 * |    |
 * |    |
 * 3----2
 * We can divide the vertices into two groups: {0, 2} and {1, 3}.
 * Example 2:
 * Input: [[1,2,3], [0,2], [0,1,3], [0,2]]
 * Output: false
 * Explanation:
 * The graph looks like this:
 * 0----1
 * | \  |
 * |  \ |
 * 3----2
 * We cannot find a way to divide the set of nodes into two independent subsets.
 *
 *
 * Note:
 *
 * graph will have length in range [1, 100].
 * graph[i] will contain integers in range [0, graph.length - 1].
 * graph[i] will not contain i or duplicate values.
 * The graph is undirected: if any element j is in graph[i], then i will be in graph[j].
 */
/**
 * And here is the bfs version.
 */
class Solution
{
public:
    bool isBipartite(vector<vector<int>>& graph)
    {
        vector<int> color(graph.size(), 0); // 0: uncolored; 1: color A; -1: color B

        queue<int> q;

        for (int i = 0; i < graph.size(); i++) {
            if (color[i]) {
                continue;
            }

            color[i] = 1;
            q.push(i);

            while (!q.empty()) {
                int cur = q.front();
                q.pop();

                for (auto neighbor : graph[cur]) {
                    if (0 == color[neighbor]) {
                        color[neighbor] = -color[cur];
                        q.push(neighbor);
                    }
                    else if (color[neighbor] == color[cur]) {
                        return false;
                    }
                }
            }
        }

        return true;
    }
};

/**
 * Oh, boy. My solution sucks. So here is the version from discussion, I rewrite it in c++
 */
class Solution
{
public:
    bool isBipartite(vector<vector<int>>& graph)
    {
        vector<int> memo(graph.size(), 0);

        for (int i = 0; i < graph.size(); ++i) {
            // in case the graph is a disconnected graph, check all
            if (0 == memo[i] && !dfs(graph, memo, 1, i)) {
                return false;
            }
        }

        return true;
    }

private:
    bool dfs(const vector<vector<int>>& graph, vector<int>& memo, int color, int v)
    {
        if (memo[v] != 0) {
            return memo[v] == color;
        }

        memo[v] = color;
        for (auto t : graph[v]) {
            if (!dfs(graph, memo, -color, t)) {
                return false;
            }
        }

        return true;
    }
};

/**
 * Improve the below solution.
 */
class Solution
{
public:
    bool isBipartite(vector<vector<int>>& graph)
    {
        vector<int> memo(graph.size());

        return dfs(graph, 0, memo);
    }

private:
    bool dfs(const vector<vector<int>>& graph, int v, vector<int>& memo)
    {
        if (v >= graph.size()) {
            return true;
        }

        bool must_in_1 = false;
        bool must_in_2 = false;

        if (0 == memo[v]) {
            for (auto t : graph[v]) {
                if (1 == memo[t]) {
                    must_in_2 = true;
                    break;
                }
                if (2 == memo[t]) {
                    must_in_1 = true;
                    break;
                }
            }
        } else if (1 == memo[v]) {
            must_in_1 = true;
        } else if (2 == memo[v]) {
            must_in_2 = true;
        }

        if (must_in_1) {
            for (auto t : graph[v]) {
                if (1 == memo[t]) {
                    return false;
                }
                memo[t] = 2;
            }
        } else if (must_in_2) {
            for (auto t : graph[v]) {
                if (2 == memo[t]) {
                    return false;
                }
                memo[t] = 1;
            }
        } else {
            memo[v] = 1;
            for (auto t : graph[v]) {
                memo[t] = 2;
            }
            if (dfs(graph, v + 1, memo)) {
                return true;
            }
            memo[v] = 2;
            for (auto t : graph[v]) {
                memo[t] = 1;
            }
        }

        return dfs(graph, v + 1, memo);
    }
};
/**
 * Original solution.
 */
class Solution
{
public:
    bool isBipartite(vector<vector<int>>& graph)
    {
        vector<int> memo(graph.size());

        return dfs(graph, 0, memo);
    }

private:
    bool dfs(const vector<vector<int>>& graph, int v, vector<int>& memo)
    {
        if (v >= graph.size()) {
            return true;
        }

        if (0 == memo[v]) {
            bool must_in_1 = false;
            bool must_in_2 = false;
            for (auto t : graph[v]) {
                if (1 == memo[t]) {
                    must_in_2 = true;
                } else if (2 == memo[t]) {
                    must_in_1 = true;
                }

                if (must_in_1 && must_in_2) {
                    return false;
                }
            }

            if (must_in_1) {
                for (auto t : graph[v]) {
                    memo[t] = 2;
                }
                return dfs(graph, v + 1, memo);
            } else if (must_in_2) {
                for (auto t : graph[v]) {
                    memo[t] = 1;
                }
                return dfs(graph, v + 1, memo);
            } else {
                memo[v] = 1;
                for (auto t : graph[v]) {
                    memo[t] = 2;
                }
                if (dfs(graph, v + 1, memo)) {
                    return true;
                }
                memo[v] = 2;
                for (auto t : graph[v]) {
                    memo[t] = 1;
                }
                return dfs(graph, v + 1, memo);
            }
        } else if (1 == memo[v]) {
            for (auto t : graph[v]) {
                if (1 == memo[t]) {
                    return false;
                } else {
                    memo[t] = 2;
                }
            }
            return dfs(graph, v + 1, memo);
        } else if (2 == memo[v]) {
            for (auto t : graph[v]) {
                if (2 == memo[t]) {
                    return false;
                } else {
                    memo[t] = 1;
                }
            }
            return dfs(graph, v + 1, memo);
        }

        return true;
    }
};

