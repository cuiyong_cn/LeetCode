/**
 * Isomorphic Strings
 *
 * Given two strings s and t, determine if they are isomorphic.
 *
 * Two strings s and t are isomorphic if the characters in s can be replaced to get t.
 *
 * All occurrences of a character must be replaced with another character while preserving the order
 * of characters. No two characters may map to the same character, but a character may map to
 * itself.
 *
 * Example 1:
 *
 * Input: s = "egg", t = "add"
 * Output: true
 * Example 2:
 *
 * Input: s = "foo", t = "bar"
 * Output: false
 * Example 3:
 *
 * Input: s = "paper", t = "title"
 * Output: true
 *
 * Constraints:
 *
 * 1 <= s.length <= 5 * 104
 * t.length == s.length
 * s and t consist of any valid ascii character.
 */
/**
 * More efficient one.
 */
class Solution {
public:
    bool isIsomorphic(string s, string t) {
        char map_s[128] = { 0 };
        char map_t[128] = { 0 };
        int len = s.size();

        for (int i = 0; i < len; ++i) {
            if (map_s[s[i]] != map_t[t[i]]) {
                return false;
            }

            map_s[s[i]] = i+1;
            map_t[t[i]] = i+1;
        }

        return true;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool isIsomorphic(string s, string t) {
        unordered_map<char, char> stmap;
        unordered_map<char, char> tsmap;

        for (size_t i = 0; i < s.size(); ++i) {
            auto st = stmap.find(s[i]);
            if (st != stmap.end()) {
                if (t[i] != st->second) {
                    return false;
                }

                auto ts = tsmap.find(t[i]);
                if (ts == tsmap.end()) {
                    return false;
                }

                if (s[i] != ts->second) {
                    return false;
                }
            }
            else {
                auto ts = tsmap.find(t[i]);
                if (ts != tsmap.end()) {
                    return false;
                }

                stmap[s[i]] = t[i];
                tsmap[t[i]] = s[i];
            }
        }

        return true;
    }
};
