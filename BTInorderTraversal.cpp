/**
 * Binary Tree Inorder Traversal
 *
 * Given a binary tree, return the inorder traversal of its nodes' values.
 *
 * Example:
 *
 * Input: [1,null,2,3]
 *    1
 *     \
 *      2
 *     /
 *    3
 *
 * Output: [1,3,2]
 *
 * Follow up: Recursive solution is trivial, could you do it iteratively?
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/**
 * The trival recursive version
 */
class Solution {
private:
    void dfs(TreeNode* root, vector<int>& v) {
        if (nullptr == root) return;

        dfs(root->left, v);
        v.push_back(root->val);
        dfs(root->right, v);
    }
public:
    vector<int> inorderTraversal(TreeNode* root) {
        vector<int> ans;
        dfs(root, ans);
        return ans;
    }
};

/**
 * Iterative using Stack
 */
class Solution {
public:
    vector<int> inorderTraversal(TreeNode* root) {
        vector<int> ans;
        stack<TreeNode*> stk;
        TreeNode* cur = root;
        while (false == stk.empty() || nullptr != cur) {
            while (nullptr != cur) {
                stk.push(cur);
                cur = cur->left;
            }
            cur = stk.top();
            stk.pop();
            ans.push_back(cur->val);
            cur = cur->right;
        }
        return ans;
    }
};

/**
 * Morris solution. Not modifying the orignal tree.
 */
class Solution {
public:
    vector<int> inorderTraversal(TreeNode* root) {
        vector<int> ans;
        TreeNode* cur = root;
        while (nullptr != cur) {
            if (nullptr != cur->left) {
                TreeNode* pre = cur->left;
                while (nullptr != pre->right && pre->right != cur) {
                    pre = pre->right;
                }
                if (nullptr == pre->right) {
                    pre->right = cur;
                    cur = cur->left;
                }
                else {
                    pre->right = nullptr;
                    ans.push_back(cur->val);
                    cur = cur->right;
                }
            }
            else {
                ans.push_back(cur->val);
                cur = cur->right;
            }
        }
        return ans;
    }
};
