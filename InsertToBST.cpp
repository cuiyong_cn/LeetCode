/**
 * Given the root node of a binary search tree (BST) and a value to be inserted
 * into the tree, insert the value into the BST. Return the root node of the BST
 * after the insertion. It is guaranteed that the new value does not exist in
 * the original BST.
 * 
 * Note that there may exist multiple valid ways for the insertion, as long as
 * the tree remains a BST after insertion. You can return any of them.
 * 
 * For example, 
 * 
 * Given the tree:
 *         4
 *        / \
 *       2   7
 *      / \
 *     1   3
 * And the value to insert: 5
 * 
 * You can return this binary search tree:
 * 
 *          4
 *        /   \
 *       2     7
 *      / \   /
 *     1   3 5
 * 
 * This tree is also valid:
 * 
 *          5
 *        /   \
 *       2     7
 *      / \   
 *     1   3
 *          \
 *           4
 * 
 */

/**
 *  The recursive method. Clean, short, clear.
 *  My solution is iterative, long but worked.
 *  I think this one is more appropriate, especially
 *  memory is cheap nowadays.
 *
 * 	TreeNode* insertIntoBST(TreeNode *node, int val) {
 *      if (!node) {
 *      	TreeNode *newNode = new TreeNode(val);
 *      	return newNode;
 *      }
 *      if (val < node->val) {
 *      	node->left = insertIntoBST(node->left, val);
 *      }
 *      else {
 *      	node->right = insertIntoBST(node->right, val);
 *      }
 *      return node;
 *  }
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* insertIntoBST(TreeNode* root, int val) {
        if (!root) return new TreeNode{val};

        TreeNode* cur = root;
        deque<TreeNode*> stack;
        while (cur) {
            stack.push_back(cur);
            if (cur->val == val) {
                break;
            }
            else if (cur->val < val) {
                cur = cur->right;
            }
            else {
                cur = cur->left;
            }
        }

        if (!cur) {
            cur = stack.back();
            if (cur->val < val) {
                cur->right = new TreeNode{val};
            }
            else {
                cur->left = new TreeNode{val};
            }
        }

        return root;
    }
};
