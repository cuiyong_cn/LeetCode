/**
 * Minimum Number of Operations to Sort a Binary Tree by Level
 *
 * You are given the root of a binary tree with unique values.
 *
 * In one operation, you can choose any two nodes at the same level and swap their values.
 *
 * Return the minimum number of operations needed to make the values at each level sorted in a
 * strictly increasing order.
 *
 * The level of a node is the number of edges along the path between it and the root node.
 *
 * Example 1:
 *
 * Input: root = [1,4,3,7,6,8,5,null,null,null,null,9,null,10]
 * Output: 3
 * Explanation:
 * - Swap 4 and 3. The 2nd level becomes [3,4].
 * - Swap 7 and 5. The 3rd level becomes [5,6,8,7].
 * - Swap 8 and 7. The 3rd level becomes [5,6,7,8].
 * We used 3 operations so return 3.
 * It can be proven that 3 is the minimum number of operations needed.
 * Example 2:
 *
 * Input: root = [1,3,2,7,6,5,4]
 * Output: 3
 * Explanation:
 * - Swap 3 and 2. The 2nd level becomes [2,3].
 * - Swap 7 and 4. The 3rd level becomes [4,6,5,7].
 * - Swap 6 and 5. The 3rd level becomes [4,5,6,7].
 * We used 3 operations so return 3.
 * It can be proven that 3 is the minimum number of operations needed.
 * Example 3:
 *
 * Input: root = [1,2,3,4,5,6]
 * Output: 0
 * Explanation: Each level is already sorted in increasing order so return 0.
 *
 * Constraints:
 *
 * The number of nodes in the tree is in the range [1, 105].
 * 1 <= Node.val <= 105
 * All the values of the tree are unique.
 */
/**
 * Original solution.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
auto next_level(vector<TreeNode*> const& nodes)
{
    auto level_nodes = vector<TreeNode*>{};
    for (auto n : nodes) {
        if (n->left) {
            level_nodes.push_back(n->left);
        }
        if (n->right) {
            level_nodes.push_back(n->right);
        }
    }
    return level_nodes;
}

auto level_array(vector<TreeNode*> const& nodes)
{
    auto arr = vector<int>(nodes.size(), 0);
    for (auto n = nodes.size(), i = 0 * n; i < n; ++i) {
        arr[i] = nodes[i]->val;
    }
    return arr;
}

class Solution {
public:
    int minimumOperations(TreeNode* root) {
        auto ans = 0;
        auto level_nodes = vector<TreeNode*>{root};
        while (!level_nodes.empty()) {
            auto arr = level_array(level_nodes);
            auto val_idx = unordered_map<int, int>{};
            for (auto i = 0; i < arr.size(); ++i) {
                val_idx[arr[i]] = i;
            }
            auto sorted = arr;
            sort(begin(sorted), end(sorted));
            for (auto n = arr.size(), i = 0 * n; i < n; ++i) {
                if (arr[i] != sorted[i]) {
                    ++ans;
                    auto tidx = val_idx[sorted[i]];
                    swap(arr[i], arr[tidx]);
                    val_idx[arr[i]] = i;
                    val_idx[arr[tidx]] = tidx;
                }
            }
            level_nodes = next_level(level_nodes);
        }
        return ans;
    }
};
