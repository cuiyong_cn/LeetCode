/**
 * Find the Maximum Length of a Good Subsequence I
 *
 * You are given an integer array nums and a non-negative integer k. A sequence of integers seq is
 * called good if there are at most k indices i in the range [0, seq.length - 2] such that seq[i] !=
 * seq[i + 1].
 *
 * Return the maximum possible length of a good subsequence of nums.
 *
 * Example 1:
 *
 * Input: nums = [1,2,1,1,3], k = 2
 *
 * Output: 4
 *
 * Explanation:
 *
 * The maximum length subsequence is [1,2,1,1,3].
 *
 * Example 2:
 *
 * Input: nums = [1,2,3,4,5,1], k = 0
 *
 * Output: 2
 *
 * Explanation:
 *
 * The maximum length subsequence is [1,2,3,4,5,1].
 *
 * Constraints:
 *     1 <= nums.length <= 500
 *     1 <= nums[i] <= 109
 *     0 <= k <= min(nums.length, 25)
 */
/**
 * Original solution.
 */
class Solution {
public:
    int maximumLength(vector<int>& nums, int k) {
        auto dp = vector<unordered_map<int, int>>(k + 1); // longest good subsequence with at most i different neighbours, and ending with a.
        auto ans = vector<int>(k + 1); // longest good subsequence with at most i different neighbours

        for (auto const& a : nums) {
            for (auto i = k; i >= 0; --i) {
                // When we met a, there are 2 ways to increaes the sequence
                //    1 - We append a into a previous sequence that ended with a --> dp[i][a] + 1
                //    2 - We append a into a previous sequence that ended with other value, ans[i - 1] + 1
                dp[i][a] = max(dp[i][a] + 1, (i > 0 ? ans[i - 1] + 1 : 0));
                ans[i] = max(ans[i], dp[i][a]);
            }
        }

        return ans[k];
    }
};
