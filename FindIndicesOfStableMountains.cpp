/**
 * Find Indices of Stable Mountains
 *
 * There are n mountains in a row, and each mountain has a height. You are given an integer array
 * height where height[i] represents the height of mountain i, and an integer threshold.
 *
 * A mountain is called stable if the mountain just before it (if it exists) has a height strictly
 * greater than threshold. Note that mountain 0 is not stable.
 *
 * Return an array containing the indices of all stable mountains in any order.
 *
 * Example 1:
 *
 * Input: height = [1,2,3,4,5], threshold = 2
 *
 * Output: [3,4]
 *
 * Explanation:
 *     Mountain 3 is stable because height[2] == 3 is greater than threshold == 2.
 *     Mountain 4 is stable because height[3] == 4 is greater than threshold == 2.
 *
 * Example 2:
 *
 * Input: height = [10,1,10,1,10], threshold = 3
 *
 * Output: [1,3]
 *
 * Example 3:
 *
 * Input: height = [10,1,10,1,10], threshold = 10
 *
 * Output: []
 *
 * Constraints:
 *     2 <= n == height.length <= 100
 *     1 <= height[i] <= 100
 *     1 <= threshold <= 100
 */
/**
 * Original solution.
 */
template<typename Iter>
class RngIter
{
public:
    RngIter(Iter i, int s) : iter{i}, size{s} {}

    pair<int, Iter> operator*()
    {
        return {i, iter};
    }

    RngIter& operator++()
    {
        ++iter;
        ++i;
        return *this;
    }

    RngIter& operator++(int)
    {
        return operator++();
    }

    RngIter& begin()
    {
        return *this;
    }

    RngIter& end()
    {
        return *this;
    }

    bool operator!=(RngIter const& other)
    {
        return i != size;
    }

private:
    Iter iter;
    int i = 0;
    int size = 0;
};

template<typename Rng>
auto enumerate(Rng&& rng)
{
    return RngIter{rng.begin(), static_cast<int>(rng.size())};
}

class Solution {
public:
    vector<int> stableMountains(vector<int>& height, int threshold) {
        auto ans = vector<int>{};

        for (auto pre = 0; auto [i, iter] : enumerate(height)) {
            if (pre > threshold) {
                ans.push_back(i);
            }

            pre = *iter;
        }

        return ans;
    }
};
