/**
 * Advantage Shuffle
 * Given two arrays A and B of equal size, the advantage of A with respect to B
 * is the number of indices i for which A[i] > B[i].
 *
 * Return any permutation of A that maximizes its advantage with respect to B.
 *
 * Example 1:
 *
 * Input: A = [2,7,11,15], B = [1,10,4,11]
 * Output: [2,11,7,15]
 * Example 2:
 *
 * Input: A = [12,24,8,32], B = [13,25,32,11]
 * Output: [24,32,8,12]
 *
 * Note:
 *
 * 1 <= A.length = B.length <= 10000
 * 0 <= A[i] <= 10^9
 * 0 <= B[i] <= 10^9
 */
/**
 * Compact and moderate fast solution.
 */
class Solution {
public:
    vector<int> advantageCount(vector<int>& A, vector<int>& B) {
        multiset<int> s(begin(A), end(A));
        for (auto i = 0; i < B.size(); ++i) {
            auto p = *s.rbegin() <= B[i] ? s.begin() : s.upper_bound(B[i]);
            A[i] = *p;
            s.erase(p);
        }
        return A;
    }
};

/**
 * Original solution.
 */
vector<int> sequence(int s, int e)
{
    vector<int> ans(e - s + 1, 0);
    generate(ans.begin(), ans.end(), [&s, e]() {
        return s++;
    });

    return ans;
}

class Solution {
public:
    vector<int> advantageCount(vector<int>& A, vector<int>& B) {
        auto sa = sequence(0, A.size() - 1);
        auto sb = sa;

        sort(sa.begin(), sa.end(), [&A](int a, int b) {
            return A[a] <= A[b];
        });

        sort(sb.begin(), sb.end(), [&B](int a, int b) {
            return B[a] <= B[b];
        });

        vector<int> ans(A.size(), 0);
        int left = 0, right = sa.size() - 1;
        for (int i = sb.size() - 1; i >= 0; --i) {
            auto a = sa[right], b = sb[i];
            if (A[a] > B[b]) {
                ans[b] = A[a];
                --right;
            }
            else {
                ans[b] = A[sa[left]];
                ++left;
            }
        }

        return ans;
    }
};
