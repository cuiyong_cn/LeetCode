/**
 * Find Two Non-overlapping Sub-arrays Each With Target Sum
 *
 * You are given an array of integers arr and an integer target.
 *
 * You have to find two non-overlapping sub-arrays of arr each with a sum equal target. There can be
 * multiple answers so you have to find an answer where the sum of the lengths of the two sub-arrays
 * is minimum.
 *
 * Return the minimum sum of the lengths of the two required sub-arrays, or return -1 if you cannot
 * find such two sub-arrays.
 *
 * Example 1:
 *
 * Input: arr = [3,2,2,4,3], target = 3
 * Output: 2
 * Explanation: Only two sub-arrays have sum = 3 ([3] and [3]). The sum of their lengths is 2.
 * Example 2:
 *
 * Input: arr = [7,3,4,7], target = 7
 * Output: 2
 * Explanation: Although we have three non-overlapping sub-arrays of sum = 7 ([7], [3,4] and [7]), but we will choose the first and third sub-arrays as the sum of their lengths is 2.
 * Example 3:
 *
 * Input: arr = [4,3,2,6,2,3,4], target = 6
 * Output: -1
 * Explanation: We have only one sub-array of sum = 6.
 *
 * Constraints:
 *
 * 1 <= arr.length <= 105
 * 1 <= arr[i] <= 1000
 * 1 <= target <= 108
 */
/**
 * Sliding window combine with DP solution.
 */
class Solution {
public:
    int minSumOfLengths(vector<int>& arr, int target) {
        int l=0,r=0,csum=0,ans=INT_MAX;
        vector<int> dp(arr.size(),INT_MAX);
        for(;r<arr.size();r++){
            csum+=arr[r];
            while(csum>target)
                csum-=arr[l++];
            if(csum==target){
                dp[r]=r-l+1;
                if(l-1>=0 && dp[l-1]!=INT_MAX)
                    ans=min(ans,dp[r]+dp[l-1]);
            }
            if(r-1>=0)
                dp[r]=min(dp[r-1],dp[r]);
        }
        
        return ans==INT_MAX?-1:ans;
    }
};

/**
 * General solution.
 */
class Solution {
public:
    int dp[100005][3];  //if asking for n subarrays, change 3 to n+1
    unordered_map<int,int> sm;  // keep track of (prefixsum : index)  
    
    int minSumOfLengths(vector<int>& arr, int tar) {
        int n = arr.size(), cursum = 0;
        sm[0] = 0;
        memset(dp, 127, sizeof(dp));  //initialize to INF
        for (int i = 0; i < 100005; i++) dp[i][0] = 0;  //if we doesn't find a subarray, len = 0
        
        for (int i = 1; i <= n; i++) {
            int d = -1;  //initialize to -1
            cursum += arr[i-1];
            sm[cursum] = i;
            if (sm.count(cursum - tar)) d = sm[cursum-tar];
            
            for (int j = 1; j <= 2; j++) {  // if asking for n subarrays, change 2 to n
                dp[i][j] = min(dp[i][j], dp[i-1][j]);  //dp[i][j] must <= dp[i-1][j]
                if (d != -1) dp[i][j] = min(dp[i][j],dp[d][j-1] + i - d);
            }
        }
        if (dp[n][2] > 1e9) return -1;   // if asking for n subarrays, change 2 to n
        return dp[n][2];   // if asking for n subarrays, change 2 to n
    }
};

/**
 * Passed solution from discussion.
 */
class Solution {
public:
    int minSumOfLengths(vector<int>& arr, int target) {
        auto left = minLen(arr, target);
        reverse(arr.begin(), arr.end());
        auto right = minLen(arr, target);

        int min_val = arr.size() + 1;
        int const N = arr.size();
        for (int i = 0; i < N - 1; ++i) {
            min_val = min(min_val, left[i] + right[N-1-i-1]);
        }
        return min_val == arr.size() + 1 ? -1 : min_val;
    }

    // at each position, i, find the min length of array with target sum
    vector<int> minLen(vector<int> &arr, int target) {
        int const N = arr.size();
        auto sum = 0;
        unordered_map<int, int> idx;
        vector<int> ans(N, N + 1);
        idx[0] = -1;
        for(int i = 0; i < N; ++i) {
            sum += arr[i];
            int len = idx.find(sum - target) != idx.end() ? i - idx[sum - target] : N + 1;
            idx[sum] = i;
            ans[i] = i == 0 ? len : min(ans[i-1], len);
        }

        return ans;
    }
};

/**
 * Optimize a little.
 */
class Solution {
public:
    int minSumOfLengths(vector<int>& arr, int target) {
        int const N = arr.size();
        vector<int> prefix_sum(N + 1, 0);
        partial_sum(arr.begin(), arr.end(), prefix_sum.begin() + 1);
        vector<pair<int, int>> prefix(N, {10'001, 10'001});

        for (int i = 0; i < N; ++i) {
            for (int L = 1; L <= N && (i + L) <= N; ++L) {
                auto sum = prefix_sum[i + L] - prefix_sum[i];
                if (sum > target) {
                    break;
                }
                if (sum == target) {
                    prefix[i].second =  min(prefix[i].second, L);
                    prefix[i + L - 1].first = min(prefix[i + L - 1].first, L);
                }
            }
        }

        for (int i = 1; i < N; ++i) {
            prefix[i].first = min(prefix[i - 1].first, prefix[i].first);
        }
        for (int i = N - 2; i >= 0; --i) {
            prefix[i].second = min(prefix[i + 1].second, prefix[i].second);
        }

        int ans = 10'001;
        for (int i = 1; i < (N - 1); ++i) {
            ans = min(ans, prefix[i - 1].first + prefix[i].second);
        }

        return 10'001 == ans ? -1 : ans;
    }
};

/**
 * Original solution. TLE.
 */
class Solution {
public:
    int minSumOfLengths(vector<int>& arr, int target) {
        int const N = arr.size();
        vector<int> prefix_sum(N + 1, 0);
        partial_sum(arr.begin(), arr.end(), prefix_sum.begin() + 1);
        vector<pair<int, int>> prefix(N, {10'001, 10'001});

        for (int i = 0; i < N; ++i) {
            for (int L = 1; L <= N && (i + L) <= N; ++L) {
                auto sum = prefix_sum[i + L] - prefix_sum[i];
                if (sum == target) {
                    prefix[i].second =  min(prefix[i].second, L);
                    prefix[i + L - 1].first = min(prefix[i + L - 1].first, L);
                }
            }
        }

        for (int i = 1; i < N; ++i) {
            prefix[i].first = min(prefix[i - 1].first, prefix[i].first);
        }
        for (int i = N - 2; i >= 0; --i) {
            prefix[i].second = min(prefix[i + 1].second, prefix[i].second);
        }

        int ans = 10'001;
        for (int i = 1; i < (N - 1); ++i) {
            ans = min(ans, prefix[i - 1].first + prefix[i].second);
        }

        return 10'001 == ans ? -1 : ans;
    }
};
