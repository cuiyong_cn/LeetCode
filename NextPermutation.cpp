/**
 * Next Permutation
 *
 * Implement next permutation, which rearranges numbers into the lexicographically next greater
 * permutation of numbers.
 *
 * If such an arrangement is not possible, it must rearrange it as the lowest possible order (i.e.,
 * sorted in ascending order).
 *
 * The replacement must be in place and use only constant extra memory.
 *
 * Example 1:
 *
 * Input: nums = [1,2,3]
 * Output: [1,3,2]
 * Example 2:
 *
 * Input: nums = [3,2,1]
 * Output: [1,2,3]
 * Example 3:
 *
 * Input: nums = [1,1,5]
 * Output: [1,5,1]
 * Example 4:
 *
 * Input: nums = [1]
 * Output: [1]
 *
 * Constraints:
 *
 * 1 <= nums.length <= 100
 * 0 <= nums[i] <= 100
 */
/**
 * Get rid of the extra space and sort.
 */
class Solution {
public:
    void nextPermutation(vector<int>& nums) {
        int back = nums.size() - 2;
        for (; back >= 0; --back) {
            if (nums[back + 1] > nums[back]) {
                break;
            }
        }

        if (back >= 0) {
            for (int i = nums.size() - 1; i > back; --i) {
                if (nums[i] > nums[back]) {
                    swap(nums[i], nums[back]);
                    break;
                }
            }
        }

        reverse(begin(nums) + (back + 1), end(nums));
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    void nextPermutation(vector<int>& nums) {
        array<int, 101> num_pos;
        num_pos.fill(-1);

        num_pos[nums.back()] = nums.size() - 1;

        for (int back = nums.size() - 2; back >= 0; --back) {
            int i = nums[back] + 1;
            for (; i < num_pos.size(); ++i) {
                if (num_pos[i] > 0) {
                    break;
                }
            }

            if (i < num_pos.size()) {
                swap(nums[back], nums[num_pos[i]]);
                sort(begin(nums) + back + 1, end(nums));
                return;
            }
            num_pos[nums[back]] = back;
        }

        reverse(begin(nums), end(nums));
    }
};
