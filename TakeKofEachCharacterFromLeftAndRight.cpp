/**
 * Take K of Each Character From Left and Right
 *
 * You are given a string s consisting of the characters 'a', 'b', and 'c' and a non-negative
 * integer k. Each minute, you may take either the leftmost character of s, or the rightmost
 * character of s.
 *
 * Return the minimum number of minutes needed for you to take at least k of each character, or
 * return -1 if it is not possible to take k of each character.
 *
 * Example 1:
 *
 * Input: s = "aabaaaacaabc", k = 2
 * Output: 8
 * Explanation:
 * Take three characters from the left of s. You now have two 'a' characters, and one 'b' character.
 * Take five characters from the right of s. You now have four 'a' characters, two 'b' characters, and two 'c' characters.
 * A total of 3 + 5 = 8 minutes is needed.
 * It can be proven that 8 is the minimum number of minutes needed.
 * Example 2:
 *
 * Input: s = "a", k = 1
 * Output: -1
 * Explanation: It is not possible to take one 'b' or 'c' so return -1.
 *
 * Constraints:
 *
 * 1 <= s.length <= 105
 * s consists of only the letters 'a', 'b', and 'c'.
 * 0 <= k <= s.length
 */
/**
 * One pass.
 */

auto less_than(int k)
{
    return [k](auto cnt) { return cnt < k; };
}

class Solution {
public:
    int takeCharacters(string s, int k) {
        if (0 == k) {
            return 0;
        }

        auto const n = int(s.length());
        auto cnt = vector<int>(3, 0);
        for (auto c : s) {
            ++cnt[c - 'a'];
        }

        if (any_of(begin(cnt), end(cnt), less_than(k))) {
            return -1;
        }

        auto ans = n;
        for (auto i = n - 1, j = i; i >= 0; --i) {
            --cnt[s[i] - 'a'];
            while (any_of(begin(cnt), end(cnt), less_than(k))) {
                ++cnt[s[j] - 'a'];
                --j;
            }
            ans = min(ans, i - 1 + n - j);
        }

        return ans;
    }
};

/**
 * Original solution. It is valid solution but TLE.
 */
class Solution {
public:
    int takeCharacters(string s, int k) {
        if (0 == k) {
            return 0;
        }

        auto const n = int(s.length());
        auto prefix_cnt = vector<vector<int>>(3, vector<int>(n, 0));
        auto suffix_cnt = prefix_cnt;

        for (auto i = 0; i < n; ++i) {
            ++prefix_cnt[s[i] - 'a'][i];
        }

        for (auto i = n - 1; i >= 0; --i) {
            ++suffix_cnt[s[i] - 'a'][i];
        }

        for (auto i = 0; i < 3; ++i) {
            partial_sum(begin(prefix_cnt[i]), end(prefix_cnt[i]), begin(prefix_cnt[i]));
            partial_sum(rbegin(suffix_cnt[i]), rend(suffix_cnt[i]), rbegin(suffix_cnt[i]));
        }

        auto ans = n + 1;
        for (auto i = 0; i < n; ++i) {
            auto cnt = 0;
            for (auto n = 0; n < 3; ++n) {
                cnt += prefix_cnt[n][i] >= k;
            }
            if (3 == cnt) {
                ans = min(ans, i + 1);
                break;
            }
        }

        for (auto i = n - 1; i >= 0; --i) {
            auto cnt = 0;
            for (auto n = 0; n < 3; ++n) {
                cnt += suffix_cnt[n][i] >= k;
            }
            if (3 == cnt) {
                ans = min(ans, n - i);
                break;
            }
        }

        for (auto i = 0; i < n; ++i) {
            for (auto j = n - 1; j > i; --j) {
                auto cnt = 0;
                for (auto n = 0; n < 3; ++n) {
                    cnt += (prefix_cnt[n][i] + suffix_cnt[n][j]) >= k;
                }
                if (3 == cnt) {
                    ans = min(ans, i + 1 + n - j);
                    break;
                }
            }
        }

        return (n + 1) == ans ? -1 : ans;
    }
};
