/**
 * Word Subsets
 *
 * We are given two arrays A and B of words.  Each word is a string of lowercase letters.
 * 
 * Now, say that word b is a subset of word a if every letter in b occurs in a, including multiplicity.  For example, "wrr" is a subset of "warrior", but is not a subset of "world".
 * 
 * Now say a word a from A is universal if for every b in B, b is a subset of a. 
 * 
 * Return a list of all universal words in A.  You can return the words in any order.
 * 
 *  
 * 
 * Example 1:
 * 
 * Input: A = ["amazon","apple","facebook","google","leetcode"], B = ["e","o"]
 * Output: ["facebook","google","leetcode"]
 * Example 2:
 * 
 * Input: A = ["amazon","apple","facebook","google","leetcode"], B = ["l","e"]
 * Output: ["apple","google","leetcode"]
 * Example 3:
 * 
 * Input: A = ["amazon","apple","facebook","google","leetcode"], B = ["e","oo"]
 * Output: ["facebook","google"]
 * Example 4:
 * 
 * Input: A = ["amazon","apple","facebook","google","leetcode"], B = ["lo","eo"]
 * Output: ["google","leetcode"]
 * Example 5:
 * 
 * Input: A = ["amazon","apple","facebook","google","leetcode"], B = ["ec","oc","ceo"]
 * Output: ["facebook","leetcode"]
 *  
 * 
 * Note:
 * 
 * 1 <= A.length, B.length <= 10000
 * 1 <= A[i].length, B[i].length <= 10
 * A[i] and B[i] consist only of lowercase letters.
 * All words in A[i] are unique: there isn't i != j with A[i] == A[j].
 */
/**
 * More readble one.
 */
class Solution {
public:
    vector<string> wordSubsets(vector<string>& A, vector<string>& B) {
        int max_letters[26] = { 0, };
        
        for (auto& word : B) {
            int letters[26] = { 0, };
            
            for (auto c : word) {
                ++letters[c - 'a'];
            }
            
            for (size_t i = 0; i < 26; ++i) {
                max_letters[i] = std::max(max_letters[i], letters[i]);
            }
        }

        vector<string> ans;

        for (auto& word : A) {
            if (is_universal(word, max_letters)) {
                ans.push_back(word);
            }
        }
        
        return ans;
    }

private:
    bool is_universal(const string& word, int (&max_letters)[26])
    {
        int letters[26] = { 0, };

        for (auto c : word) {
            ++letters[c - 'a'];
        }
        
        for (size_t i = 0; i < 26; ++i) {
            if (max_letters[i] > letters[i]) {
                return false;
            }
        }
        
        return true;
    }
};

/**
 * Original solution.
 */
template<typename T, size_t N>
bool is_universal(const T (&a)[N], const T (&b)[N])
{
    for (size_t i = 0; i < N; ++i) {
        if (a[i] > b[i]) {
            return false;
        }
    }
    
    return true;
}

class Solution {
public:
    vector<string> wordSubsets(vector<string>& A, vector<string>& B) {
        int max_letters[26] = { 0, };
        
        for (auto& word : B) {
            int letters[26] = { 0, };
            
            for (auto c : word) {
                ++letters[c - 'a'];
            }
            
            for (size_t i = 0; i < 26; ++i) {
                max_letters[i] = std::max(max_letters[i], letters[i]);
            }
        }

        vector<string> ans;

        for (auto& word : A) {
            int letters[26] = { 0, };

            for (auto c : word) {
                ++letters[c - 'a'];
            }
            
            if (is_universal(max_letters, letters)) {
                ans.push_back(word);
            }
        }
        
        return ans;
    }
};
