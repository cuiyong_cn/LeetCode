/**
 * Minimum Remove to Make Valid Parentheses
 *
 * Given a string s of '(' , ')' and lowercase English characters.
 *
 * Your task is to remove the minimum number of parentheses ( '(' or ')', in any positions
 * ) so that the resulting parentheses string is valid and return any valid string.
 *
 * Formally, a parentheses string is valid if and only if:
 *
 *   It is the empty string, contains only lowercase characters, or
 *   It can be written as AB (A concatenated with B), where A and B are valid strings, or
 *   It can be written as (A), where A is a valid string.
 *
 * Example 1:
 *
 * Input: s = "lee(t(c)o)de)"
 * Output: "lee(t(c)o)de"
 * Explanation: "lee(t(co)de)" , "lee(t(c)ode)" would also be accepted.
 *
 * Example 2:
 *
 * Input: s = "a)b(c)d"
 * Output: "ab(c)d"
 *
 * Example 3:
 *
 * Input: s = "))(("
 * Output: ""
 * Explanation: An empty string is also valid.
 *
 * Example 4:
 *
 * Input: s = "(a(b(c)d)"
 * Output: "a(b(c)d)"
 *
 * Constraints:
 *
 *     1 <= s.length <= 10^5
 *     s[i] is one of  '(' , ')' and lowercase English letters.
 */
/**
 * Both memory friendily and efficient.
 */
class Solution
{
public:
    string minRemoveToMakeValid(string s) {
    stack<int> st;
    for (auto i = 0; i < s.size(); ++i) {
        if (s[i] == '(') st.push(i);
        if (s[i] == ')') {
            if (!st.empty()) st.pop();
            else s[i] = '*';
        }
    }

    while (!st.empty()) {
        s[st.top()] = '*';
        st.pop();
    }

    s.erase(remove(s.begin(), s.end(), '*'), s.end());

    return s;
    }
};
/**
 * Improved version. We mark the unpair the parenthese with '*', and remove them all at
 * once, this version still not very efficient, but it is memory friendily.
 */
class Solution {
private:
    int findMatchedPair(string& s, int start)
    {
        int index = -1;
        int count = 0;
        int i = start;
        while (i < s.size()) {
            if ('(' == s[i]) {
                ++count;
            }
            else if (')' == s[i]) {
                if (0 == count) {
                    index = i;
                    break;
                }
                else {
                    --count;
                }
            }

            ++i;
        }
        return index;
    }
public:
    string minRemoveToMakeValid(string s) {
        int i = 0;
        while (i < s.size()) {
            if ('(' == s[i]) {
                int idx = findMatchedPair(s, i + 1);
                if (idx > i) {
                    i = idx;
                }
                else {
                    s[i] = '*';
                }
            }
            else if (')' == s[i]) {
                s[i] = '*';
            }
            ++i;
        }

        s.erase(remove(s.begin(), s.end(), '*'), s.end());
        return s;
    }
};
/**
 * My original version. But it is not very efficient for the string pattern like this:
 *          "))(((((((((((((((((((((((((((((((((((("
 * It has to go over the string several times.
 * So we need another method to record the unmatched pair.
 * Also this method consume a lot of memory.
 */
class Solution {
private:
    int findMatchedPair(string& s, int start)
    {
        int index = -1;
        int count = 0;
        int i = start;
        while (i < s.size()) {
            if ('(' == s[i]) {
                ++count;
            }
            else if (')' == s[i]) {
                if (0 == count) {
                    index = i;
                    break;
                }
                else {
                    --count;
                }
            }

            ++i;
        }
        return index;
    }
public:
    string minRemoveToMakeValid(string s) {
        string ans;
        int i = 0;
        while (i < s.size()) {
            if ('(' == s[i]) {
                int idx = findMatchedPair(s, i + 1);
                if (idx > i) {
                    ans = ans + s.substr(i, idx - i + 1);
                    i = idx;
                }
            }
            else if (')' != s[i]) {
                ans = ans + s[i];
            }
            ++i;
        }
        return ans;
    }
};
