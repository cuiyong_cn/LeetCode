/**
 * Find the K-th Character in String Game I
 *
 * Alice and Bob are playing a game. Initially, Alice has a string word = "a".
 *
 * You are given a positive integer k.
 *
 * Now Bob will ask Alice to perform the following operation forever:
 *     Generate a new string by changing each character in word to its next character in the English
 *     alphabet, and append it to the original word.
 *
 * For example, performing the operation on "c" generates "cd" and performing the operation on "zb" generates "zbac".
 * Return the value of the kth character in word, after enough operations have been done for word to have at least k characters.
 * Note that the character 'z' can be changed to 'a' in the operation.
 *
 * Example 1:
 *
 * Input: k = 5
 *
 * Output: "b"
 *
 * Explanation:
 *
 * Initially, word = "a". We need to do the operation three times:
 *     Generated string is "b", word becomes "ab".
 *     Generated string is "bc", word becomes "abbc".
 *     Generated string is "bccd", word becomes "abbcbccd".
 *
 * Example 2:
 *
 * Input: k = 10
 *
 * Output: "c"
 *
 * Constraints:
 *     1 <= k <= 500
 */
/**
 * Based on bleow solution. Simplify.
 */
class Solution {
public:
    char kthCharacter(int k) {
        return 'a' + (bitset<sizeof(int) * 8>(k - 1).count() % 26);
    }
};

/**
 * Observe the pattern.
 */
class Solution {
public:
    char kthCharacter(int k) {
        auto len = 1;

        while (len < k) {
            len += len;
        }

        auto block = len / 2;
        auto incr = 0;

        while (k > 1) {
            ++incr;
            k -= block;
            while (k <= block) {
                block /= 2;
            }
        }

        return 'a' + (incr % 26);
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    char kthCharacter(int k) {
        auto str = string{"a"};

        while (str.length() < k) {
            auto gen = str;

            for (auto& c : gen) {
                c = ((c - 'a' + 1) % 26) + 'a';
            }

            str += gen;
        }

        return str[k - 1];
    }
};
