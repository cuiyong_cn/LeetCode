/**
 * Count the Number of Special Characters I
 *
 * You are given a string word. A letter is called special if it appears both in lowercase and
 * uppercase in word.
 *
 * Return the number of special letters in word.
 *
 * Example 1:
 *
 * Input: word = "aaAbcBC"
 *
 * Output: 3
 *
 * Explanation:
 *
 * The special characters in word are 'a', 'b', and 'c'.
 *
 * Example 2:
 *
 * Input: word = "abc"
 *
 * Output: 0
 *
 * Explanation:
 *
 * No character in word appears in uppercase.
 *
 * Example 3:
 *
 * Input: word = "abBCab"
 *
 * Output: 1
 *
 * Explanation:
 *
 * The only special character in word is 'b'.
 *
 * Constraints:
 *     1 <= word.length <= 50
 *     word consists of only lowercase and uppercase English letters.
 */
/**
 * Original solution.
 */
class Solution {
public:
    int numberOfSpecialChars(string word) {
        auto upper_seen = vector<bool>(26, false);
        auto lower_seen = upper_seen;

        for (auto c : word) {
            if (islower(c)) {
                lower_seen[c - 'a'] = true;
            }
            else {
                upper_seen[c - 'A'] = true;
            }
        }

        auto ans = 0;

        for (auto i = 0; i < 26; ++i) {
            if (lower_seen[i] && upper_seen[i]) {
                ++ans;
            }
        }

        return ans;
    }
};
