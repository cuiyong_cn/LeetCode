/**
 * Compare Strings by Frequency of the Smallest Character
 *
 * Let's define a function f(s) over a non-empty string s, which calculates the frequency
 * of the smallest character in s. For example, if s = "dcce" then f(s) = 2 because the
 * smallest character is "c" and its frequency is 2.
 *
 * Now, given string arrays queries and words, return an integer array answer, where
 * each answer[i] is the number of words such that f(queries[i]) < f(W), where W is a
 * word in words.
 *
 * Example 1:
 *
 * Input: queries = ["cbd"], words = ["zaaaz"]
 * Output: [1]
 * Explanation: On the first query we have f("cbd") = 1, f("zaaaz") = 3 so
 * f("cbd") < f("zaaaz").
 *
 * Example 2:
 *
 * Input: queries = ["bbb","cc"], words = ["a","aa","aaa","aaaa"]
 * Output: [1,2]
 * Explanation: On the first query only f("bbb") < f("aaaa"). On the second query both f("aaa") and f("aaaa") are both > f("cc").
 *
 * Constraints:
 *
 *     1 <= queries.length <= 2000
 *     1 <= words.length <= 2000
 *     1 <= queries[i].length, words[i].length <= 10
 *     queries[i][j], words[i][j] are English lowercase letters.
 */
/**
 * More efficient one. O(n) approach
 */
class Solution
{
public:
    vector<int> numSmallerByFrequency(vector<string>& queries, vector<string>& words)
    {
        auto f = [](string & s) {
            auto min_ch = *min_element(begin(s), end(s));
            return accumulate(begin(s), end(s), 0, [min_ch](int s, char ch) {
                return s += ch == min_ch ? 1 : 0;
            });
        };
        vector<int> cnt(11, 0), res;
        for (auto& w : words)
            for (int i = f(w) - 1; i >= 0; --i) ++cnt[i];
        for (auto& q : queries) res.push_back(cnt[f(q)]);
        return res;
    }
};

/**
 * My original version. simple and straight forward.
 */
class Solution
{
private:
    int freqOfSmallestChar(string& str)
    {
        int ans = 1;
        char smallestChar = str[0];
        for (int i = 1; i < str.size(); ++i) {
            if (str[i] < smallestChar) {
                ans = 1;
                smallestChar = str[i];
            } else if (str[i] == smallestChar) {
                ++ans;
            }
        }
        return ans;
    }

    int numLessThan(vector<int>& freq, int num)
    {
        int ans = 0;
        for (auto& n : freq) {
            if (num < n) {
                ++ans;
            }
        }
        return ans;
    }
public:
    vector<int> numSmallerByFrequency(vector<string>& queries, vector<string>& words)
    {
        vector<int> freqOfQuery(queries.size(), 0);
        vector<int> freqOfWords(words.size(), 0);
        vector<int> ans(queries.size(), 0);

        for (int i = 0; i < queries.size(); ++i) {
            freqOfQuery[i] = freqOfSmallestChar(queries[i]);
        }
        for (int i = 0; i < words.size(); ++i) {
            freqOfWords[i] = freqOfSmallestChar(words[i]);
        }

        for (int i = 0; i < queries.size(); ++i) {
            ans[i] = numLessThan(freqOfWords, freqOfQuery[i]);
        }
        return ans;
    }
};

