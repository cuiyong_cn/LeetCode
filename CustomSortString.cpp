/**
 * Custom Sort String
 *
 * S and T are strings composed of lowercase letters. In S, no letter occurs
 * more than once.
 *
 * S was sorted in some custom order previously. We want to permute the characters
 * of T so that they match the order that S was sorted. More specifically,
 * if x occurs before y in S, then x should occur before y in the returned string.
 *
 * Return any permutation of T (as a string) that satisfies this property.
 *
 * Example :
 * Input:
 * S = "cba"
 * T = "abcd"
 * Output: "cbad"
 * Explanation:
 * "a", "b", "c" appear in S, so the order of "a", "b", "c" should be "c", "b", and "a".
 * Since "d" does not appear in S, it can be at any position in T. "dcba", "cdba",
 * "cbda" are also valid outputs.
 *
 * Note:
 *
 *     S has length at most 26, and no character is repeated in S.
 *     T has length at most 200.
 *     S and T consist of lowercase letters only.
 */
/**
 * My original version
 */
class Solution {
public:
    string customSortString(string S, string T) {
        set<char> sc;
        string s1;
        string s2;
        for (auto& c : S) {
            sc.insert(c);
        }

        for (auto& c : T) {
            if (sc.count(c)) s1 += c;
            else s2 += c;
        }
        int index = 0;
        for (auto& c : S) {
            while (1) {
                size_t pos = s1.find(c, index);
                if (pos != string::npos) {
                    swap(s1[index], s1[pos]);
                    ++index;
                }
                else {
                    break;
                }
            }
        }
        return s1 + s2;
    }
};

class Solution {
  public:
    string customSortString(string S, string T) {
        sort(T.begin(), T.end(),
             [&](char a, char b) { return S.find(a) < S.find(b); });
        return T;
    }
};

class Solution {
  public:
    string customSortString(string S, string T) {
        unordered_map<char, int> dir;
        for (int i = 0; i < S.size(); i++) {
            dir[S[i]] = i + 1;
        }
        sort(T.begin(), T.end(),
             [&](char a, char b) { return dir[a] < dir[b]; });
        return T;
    }
};

class Solution {
  public:
    string customSortString(string S, string T) {
        unordered_map<char, int> dir;
        int i = 0;
        transform(S.begin(), S.end(), inserter(dir, dir.end()),
                  [&](char &a) { return make_pair(a, ++i); });
        sort(T.begin(), T.end(),
             [&](char a, char b) { return dir[a] < dir[b]; });
        return T;
    }
};
