/**
 * Count the Digits That Divide a Number
 *
 * Given an integer num, return the number of digits in num that divide num.
 *
 * An integer val divides nums if nums % val == 0.
 */
/**
 * Original solution.
 */
class Solution {
public:
    int countDigits(int num) {
        auto ans = 0;
        auto n = num;
        while (n > 0) {
            auto div = n % 10;
            ans += 0 == (num % div);
            n = n / 10;
        }
        return ans;
    }
};
