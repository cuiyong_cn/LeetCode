/**
 * Binary Tree Paths
 *
 * Given a binary tree, return all root-to-leaf paths.
 *
 * Note: A leaf is a node with no children.
 *
 * Example:
 *
 * Input:
 *
 *    1
 *  /   \
 * 2     3
 *  \
 *   5
 *
 * Output: ["1->2->5", "1->3"]
 *
 * Explanation: All root-to-leaf paths are: 1->2->5, 1->3
 */
/**
 * Original dfs solution. We can also solve this using the bfs. With each node attaching a path to it.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<string> binaryTreePaths(TreeNode* root) {
        if (root) {
            dfs(root, "");
        }

        return ans;
    }

private:
    inline bool isLeaf(TreeNode* node) {
        return nullptr == node->left && nullptr == node->right;
    }
    void dfs(TreeNode* node, string path)
    {
        if (nullptr == node) return;

        if (isLeaf(node)) {
            ans.push_back(path + to_string(node->val));
        }
        else {
            if (node->left) {
                dfs(node->left, path + to_string(node->val) + "->");
            }
            if (node->right) {
                dfs(node->right, path + to_string(node->val) + "->");
            }
        }
    }

private:
    vector<string> ans;
};
