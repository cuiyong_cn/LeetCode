/**
 * Longest Word in Dictionary through Deleting
 *
 * Given a string and a string dictionary, find the longest string in the dictionary that can be formed by deleting some
 * characters of the given string. If there are more than one possible results, return the longest word with the
 * smallest lexicographical order. If there is no possible result, return the empty string.
 *
 * Example 1:
 *
 * Input:
 * s = "abpcplea", d = ["ale","apple","monkey","plea"]
 *
 * Output:
 * "apple"
 *
 * Example 2:
 *
 * Input:
 * s = "abpcplea", d = ["a","b","c"]
 *
 * Output:
 * "a"
 *
 * Note:
 *     All the strings in the input will only contain lower-case letters.
 *     The size of the dictionary won't exceed 1,000.
 *     The length of all the strings in the input won't exceed 1,000.
 */
/**
 * Original solution. Actually we can remove the sort process. And just iterate the dictionary.
 * If a match is found, we test to see if we need to update the current matched one.
 */
class Solution {
public:
    string findLongestWord(string s, vector<string>& d) {
        auto longest_then_lexico = [](auto& word1, auto& word2) {
            if (word1.length() > word2.length()) {
                return true;
            }
            else if (word1.length() == word2.length()) {
                return word1.compare(word2) <= 0;
            }

            return false;
        };

        sort(std::begin(d), std::end(d), longest_then_lexico);

        for (auto& w : d) {
            if (substr_match(s, w)) {
                return w;
            }
        }

        return "";
    }

private:
    bool substr_match(const string& s, const string& word)
    {
        bool match = true;
        size_t pos = 0;
        for (auto c : word) {
            pos = s.find(c, pos);
            if (pos == string::npos) {
                match = false;
                break;
            }
            pos += 1;
        }

        return match;
    }
};
