/**
 * Find the Sum of Encrypted Integers
 *
 * You are given an integer array nums containing positive integers. We define a function encrypt
 * such that encrypt(x) replaces every digit in x with the largest digit in x. For example,
 * encrypt(523) = 555 and encrypt(213) = 333.
 *
 * Return the sum of encrypted elements.
 *
 * Example 1:
 *
 * Input: nums = [1,2,3]
 *
 * Output: 6
 *
 * Explanation: The encrypted elements are [1,2,3]. The sum of encrypted elements is 1 + 2 + 3 == 6.
 *
 * Example 2:
 *
 * Input: nums = [10,21,31]
 *
 * Output: 66
 *
 * Explanation: The encrypted elements are [11,22,33]. The sum of encrypted elements is 11 + 22 + 33 == 66.
 *
 * Constraints:
 *     1 <= nums.length <= 50
 *     1 <= nums[i] <= 1000
 */
/**
 * Original solution.
 */
auto biggest_digit(int n)
{
    auto bd = 0;
    auto digits = 0;

    while (n > 0) {
        bd = max(bd, n % 10);
        ++digits;
        n /= 10;
    }

    return pair(bd, digits);
}

auto encrypt(int n)
{
    auto [bd, digits] = biggest_digit(n);
    auto ans = 0;

    while (digits > 0) {
        ans = ans * 10 + bd;
        --digits;
    }

    return ans;
}

class Solution {
public:
    int sumOfEncryptedInt(vector<int>& nums) {
        auto ans = 0;

        for (auto n : nums) {
            ans += encrypt(n);
        }

        return ans;
    }
};
