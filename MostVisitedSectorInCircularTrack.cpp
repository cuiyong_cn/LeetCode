/**
 * Most Visited Sector in a Circular Track
 *
 * Given an integer n and an integer array rounds. We have a circular track which consists of n
 * sectors labeled from 1 to n. A marathon will be held on this track, the marathon consists of m
 * rounds. The ith round starts at sector rounds[i - 1] and ends at sector rounds[i]. For example,
 * round 1 starts at sector rounds[0] and ends at sector rounds[1]
 *
 * Return an array of the most visited sectors sorted in ascending order.
 *
 * Notice that you circulate the track in ascending order of sector numbers in the counter-clockwise
 * direction (See the first example).
 *
 * Example 1:
 *
 * Input: n = 4, rounds = [1,3,1,2]
 * Output: [1,2]
 * Explanation: The marathon starts at sector 1. The order of the visited sectors is as follows:
 * 1 --> 2 --> 3 (end of round 1) --> 4 --> 1 (end of round 2) --> 2 (end of round 3 and the marathon)
 * We can see that both sectors 1 and 2 are visited twice and they are the most visited sectors. Sectors 3 and 4 are visited only once.
 * Example 2:
 *
 * Input: n = 2, rounds = [2,1,2,1,2,1,2,1,2]
 * Output: [2]
 * Example 3:
 *
 * Input: n = 7, rounds = [1,3,5,7]
 * Output: [1,2,3,4,5,6,7]
 *
 * Constraints:
 *
 * 2 <= n <= 100
 * 1 <= m <= 100
 * rounds.length == m + 1
 * 1 <= rounds[i] <= n
 * rounds[i] != rounds[i + 1] for 0 <= i < m
 */
/**
 * Same as below using iota.
 */
class Solution {
public:
    vector<int> mostVisited(int n, vector<int>& rounds) {
        vector<int> ans(rounds.front() <= rounds.back() ?
                        rounds.back() - rounds.front() + 1 : rounds.back() + n - rounds.front() + 1, 0);
        if (rounds.front() <= rounds.back()) {
            iota(ans.begin(), ans.end(), rounds.front());
        }
        else {
            iota(ans.begin(), ans.begin() + rounds.back(), 1);
            iota(ans.begin() + rounds.back(), ans.end(), rounds.front());
        }

        return ans;
    }
};

/**
 * O(n) solution. much simpler and shorter.
 */
class Solution {
public:
    vector<int> mostVisited(int n, vector<int>& rounds) {
        vector<int> ans;

        auto sequence = [](int n) { return [n]() mutable { return n++; }; };
        if (rounds.front() <= rounds.back()) {
            generate_n(back_inserter(ans), rounds.back() - rounds.front() + 1, sequence(rounds.front()));
        }
        else {
            generate_n(back_inserter(ans), rounds.back(), sequence(1));
            generate_n(back_inserter(ans), n - rounds.front() + 1, sequence(rounds.front()));
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<int> mostVisited(int n, vector<int>& rounds) {
        vector<int> sector_cnt(n + 1, 0);
        int const M = rounds.size();
        for (int i = 1; i < M; ++i) {
            int ending_sector = rounds[i];
            for (int j = rounds[i - 1]; j != ending_sector; j = j == n ? 1 : j + 1) {
                ++sector_cnt[j];
            }
        }
        ++sector_cnt[rounds.back()];

        vector<int> ans;
        auto const mx_visit_cnt = *max_element(sector_cnt.begin(), sector_cnt.end());
        for (int i = 1; i <= n; ++i) {
            if (mx_visit_cnt == sector_cnt[i]) {
                ans.push_back(i);
            }
        }

        return ans;
    }
};
