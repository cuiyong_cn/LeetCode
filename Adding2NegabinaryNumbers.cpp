/**
 * Adding Two Negabinary Numbers
 *
 * Given two numbers arr1 and arr2 in base -2, return the result of adding them together.
 *
 * Each number is given in array format:  as an array of 0s and 1s, from most significant bit to
 * least significant bit.  For example, arr = [1,1,0,1] represents the number (-2)^3 + (-2)^2 +
 * (-2)^0 = -3.  A number arr in array, format is also guaranteed to have no leading zeros: either
 * arr == [0] or arr[0] == 1.
 *
 * Return the result of adding arr1 and arr2 in the same format: as an array of 0s and 1s with no
 * leading zeros.
 *
 * Example 1:
 *
 * Input: arr1 = [1,1,1,1,1], arr2 = [1,0,1]
 * Output: [1,0,0,0,0]
 * Explanation: arr1 represents 11, arr2 represents 5, the output represents 16.
 * Example 2:
 *
 * Input: arr1 = [0], arr2 = [0]
 * Output: [0]
 * Example 3:
 *
 * Input: arr1 = [0], arr2 = [1]
 * Output: [1]
 *
 * Constraints:
 *
 * 1 <= arr1.length, arr2.length <= 1000
 * arr1[i] and arr2[i] are 0 or 1
 * arr1 and arr2 have no leading zeros
 */
/**
 * Shorter one. But I think "carry >> 1" for negative number
 * is not portable.
 */
class Solution {
public:
    vector<int> addNegabinary(vector<int>& A, vector<int>& B) {
        vector<int> res;
        int carry = 0, i = A.size() - 1, j = B.size() - 1;
        while (i >= 0 || j >= 0 || carry) {
            if (i >= 0) carry += A[i--];
            if (j >= 0) carry += B[j--];
            res.push_back(carry & 1);
            carry = -(carry >> 1);
        }
        while (res.size() > 1 && res.back() == 0)
            res.pop_back();
        reverse(res.begin(), res.end());
        return res;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<int> addNegabinary(vector<int>& arr1, vector<int>& arr2) {
        int carry = 0;
        vector<int> ans;
        for (int i = arr1.size() - 1, j = arr2.size() - 1; i >= 0 || j >= 0; --i, --j) {
            int sum = (i >= 0 ? arr1[i] : 0) + (j >= 0 ? arr2[j] : 0) - carry;
            if (sum > 1) {
                ans.push_back(sum - 2);
                carry = 1;
            }
            else if (sum < 0) {
                ans.push_back(1);
                carry = -1;
            }
            else {
                ans.push_back(sum);
                carry = 0;
            }
        }

        if (1 == carry) {
            ans.push_back(1);
            ans.push_back(1);
        }

        while (ans.size() > 1 && 0 == ans.back()) {
            ans.pop_back();
        }

        reverse(ans.begin(), ans.end());

        return ans;
    }
};
