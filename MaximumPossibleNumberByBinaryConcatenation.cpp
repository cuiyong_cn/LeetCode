/**
 * Maximum Possible Number by Binary Concatenation
 *
 * You are given an array of integers nums of size 3.
 *
 * Return the maximum possible number whose binary representation can be formed by concatenating the
 * binary representation of all elements in nums in some order.
 *
 * Note that the binary representation of any number does not contain leading zeros.
 *
 * Example 1:
 *
 * Input: nums = [1,2,3]
 *
 * Output: 30
 *
 * Explanation:
 *
 * Concatenate the numbers in the order [3, 1, 2] to get the result "11110", which is the binary representation of 30.
 *
 * Example 2:
 *
 * Input: nums = [2,8,16]
 *
 * Output: 1296
 *
 * Explanation:
 *
 * Concatenate the numbers in the order [2, 8, 16] to get the result "10100010000", which is the binary representation of 1296.
 *
 * Constraints:
 *     nums.length == 3
 *     1 <= nums[i] <= 127
 */
/**
 * Original solution.
 */
auto bit_set(int b, int i)
{
    return (b & (1 << i)) != 0;
}

auto bin_rep_concat(int a, int b)
{
    constexpr int bits_int = sizeof(int) * 8;
    auto shf = bits_int - 1;

    while (!bit_set(b, shf)) {
        --shf;
    }

    return (a << (shf + 1)) | b;
}

class Solution {
public:
    int maxGoodNumber(vector<int>& nums) {
        auto idx = array<int, 3>{ 0, 1, 2 };
        auto ans = 0;;

        do {
            auto value = 0;

            for (auto i : idx) {
                value = bin_rep_concat(value, nums[i]);
            }

            ans = max(ans, value);
        } while (next_permutation(idx.begin(), idx.end()));

        return ans;
    }
};
