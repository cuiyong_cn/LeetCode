/**
 * Multiply Strings
 *
 * Given two non-negative integers num1 and num2 represented as strings, return the product of num1
 * and num2, also represented as a string.
 *
 * Note: You must not use any built-in BigInteger library or convert the inputs to integer directly.
 *
 * Example 1:
 *
 * Input: num1 = "2", num2 = "3"
 * Output: "6"
 * Example 2:
 *
 * Input: num1 = "123", num2 = "456"
 * Output: "56088"
 *
 * Constraints:
 *
 * 1 <= num1.length, num2.length <= 200
 * num1 and num2 consist of digits only.
 * Both num1 and num2 do not contain any leading zero, except the number 0 itself.
 */
/**
 * More efficient one. See detailed explaination at below URL
 * https://leetcode.com/problems/multiply-strings/discuss/17605/Easiest-JAVA-Solution-with-Graph-Explanation
 */
class Solution {
public:
    string multiply(string num1, string num2) {
        int m = num1.size(), n = num2.size();
        string ans(m + n, '0');
        for (int i = m - 1; i >= 0; i--) {
            for (int j = n - 1; j >= 0; j--) {
                int sum = (num1[i] - '0') * (num2[j] - '0') + (ans[i + j + 1] - '0');
                // below two lines are the key point here.
                ans[i + j + 1] = sum % 10 + '0';
                ans[i + j] += sum / 10;
            }
        }
        for (int i = 0; i < m + n; i++) {
            if (ans[i] != '0') {
                return ans.substr(i);
            }
        }

        return "0";
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    string multiply(string num1, string num2) {
        if ("0" == num1 || "0" == num2) {
            return "0";
        }

        string ans;
        for (int i = 0; i < num2.size(); ++i) {
            auto base = num1 + string(num2.size() - 1 - i, '0');
            auto times = num2[i] - '0';
            string sub;
            for (int k = 0; k < times; ++k) {
                sub = add(sub, base);
            }
            ans = add(ans, sub);
        }

        return ans;
    }

private:
    string add(string const& s1, string const& s2)
    {
        deque<char> ans;
        int carry = 0;

        for (int i = s1.size() - 1, j = s2.size() - 1; i >= 0 || j >= 0; --i, --j) {
            int num = (i >= 0 ? (s1[i] - '0') : 0)
                + (j >= 0 ? (s2[j] - '0') : 0) + carry;
            carry = num > 9;
            num %= 10;
            ans.push_front('0' + num);
        }

        if (carry > 0) {
            ans.push_front('1');
        }

        return string(begin(ans), end(ans));
    }
};
