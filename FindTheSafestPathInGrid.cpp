/**
 * Find the Safest Path in a Grid
 *
 * You are given a 0-indexed 2D matrix grid of size n x n, where (r, c) represents:
 *
 *     A cell containing a thief if grid[r][c] = 1
 *     An empty cell if grid[r][c] = 0
 *
 * You are initially positioned at cell (0, 0). In one move, you can move to any adjacent cell in
 * the grid, including cells containing thieves.
 *
 * The safeness factor of a path on the grid is defined as the minimum manhattan distance from any
 * cell in the path to any thief in the grid.
 *
 * Return the maximum safeness factor of all paths leading to cell (n - 1, n - 1).
 *
 * An adjacent cell of cell (r, c), is one of the cells (r, c + 1), (r, c - 1), (r + 1, c) and (r -
 * 1, c) if it exists.
 *
 * The Manhattan distance between two cells (a, b) and (x, y) is equal to |a - x| + |b - y|, where
 * |val| denotes the absolute value of val.
 *
 * Example 1:
 *
 * Input: grid = [[1,0,0],[0,0,0],[0,0,1]]
 * Output: 0
 * Explanation: All paths from (0, 0) to (n - 1, n - 1) go through the thieves in cells (0, 0) and (n - 1, n - 1).
 *
 * Example 2:
 *
 * Input: grid = [[0,0,1],[0,0,0],[0,0,0]]
 * Output: 2
 * Explanation: The path depicted in the picture above has a safeness factor of 2 since:
 * - The closest cell of the path to the thief at cell (0, 2) is cell (0, 0). The distance between them is | 0 - 0 | + | 0 - 2 | = 2.
 * It can be shown that there are no other paths with a higher safeness factor.
 *
 * Example 3:
 *
 * Input: grid = [[0,0,0,1],[0,0,0,0],[0,0,0,0],[1,0,0,0]]
 * Output: 2
 * Explanation: The path depicted in the picture above has a safeness factor of 2 since:
 * - The closest cell of the path to the thief at cell (0, 3) is cell (1, 2). The distance between them is | 0 - 1 | + | 3 - 2 | = 2.
 * - The closest cell of the path to the thief at cell (3, 0) is cell (3, 2). The distance between them is | 3 - 3 | + | 0 - 2 | = 2.
 * It can be shown that there are no other paths with a higher safeness factor.
 *
 * Constraints:
 *
 *     1 <= grid.length == n <= 400
 *     grid[i].length == n
 *     grid[i][j] is either 0 or 1.
 *     There is at least one thief in the grid.
 */
/**
 * Original solution.
 */
class Solution {
public:
    int maximumSafenessFactor(vector<vector<int>>& grid) {
        using Position = pair<int, int>;
        int const n = grid.size();
        auto thief_posts = vector<Position>{};

        for (auto i = 0; i < n; ++i) {
            for (auto j = 0; j < n; ++j) {
                if (1 == grid[i][j]) {
                    thief_posts.emplace_back(i, j);
                }
            }
        }

        auto md_to_thief = vector<vector<int>>(n, vector<int>(n, 2 * n));
        auto bfs_queue = deque<Position>{thief_posts.begin(), thief_posts.end()};
        auto within_map = [n](int x, int y) {
            return 0 <= x && x < n && 0 <= y && y < n;
        };

        auto d = 0;

        while (!bfs_queue.empty()) {
            auto const qsize = bfs_queue.size();

            for (auto i = 0; i < qsize; ++i) {
                auto [x, y] = bfs_queue.front();
                bfs_queue.pop_front();

                if (!within_map(x, y) || md_to_thief[x][y] <= d) {
                    continue;
                }

                md_to_thief[x][y] = d;

                bfs_queue.emplace_back(x, y - 1);
                bfs_queue.emplace_back(x, y + 1);
                bfs_queue.emplace_back(x - 1, y);
                bfs_queue.emplace_back(x + 1, y);
            }

            ++d;
        }

        auto ans = 0;
        auto left = 1;
        auto right = min(md_to_thief[0][0], md_to_thief.back().back());

        while (left <= right) {
            auto mid = left + (right - left) / 2;
            auto succeed = false;
            auto visited = vector<vector<bool>>(n, vector<bool>(n, false));
            auto bfs_queue = deque<Position>{{0, 0}};

            while (!bfs_queue.empty()) {
                auto const qsize = bfs_queue.size();

                for (auto i = 0; i < qsize; ++i) {
                    auto [x, y] = bfs_queue.front();
                    bfs_queue.pop_front();

                    if (!within_map(x, y) || visited[x][y]) {
                        continue;
                    }

                    visited[x][y] = true;

                    if ((n - 1) == x && (n - 1) == y) {
                        succeed = true;
                        break;
                    }

                    if (md_to_thief[x][y] >= mid) {
                        bfs_queue.emplace_back(x, y - 1);
                        bfs_queue.emplace_back(x, y + 1);
                        bfs_queue.emplace_back(x - 1, y);
                        bfs_queue.emplace_back(x + 1, y);
                    }
                }
            }

            if (succeed) {
                ans = mid;
                left = mid + 1;
            }
            else {
                right = mid - 1;
            }
        }

        return ans;
    }
};
