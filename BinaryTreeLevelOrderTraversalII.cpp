/**
 * Binary Tree Level Order Traversal II
 *
 * Given a binary tree, return the bottom-up level order traversal of its nodes' values. (ie, from left to right, level
 * by level from leaf to root).
 *
 * For example:
 * Given binary tree [3,9,20,null,null,15,7],
 *
 *     3
 *    / \
 *   9  20
 *     /  \
 *    15   7
 *
 * return its bottom-up level order traversal as:
 *
 * [
 *   [15,7],
 *   [9,20],
 *   [3]
 * ]
 */
/**
 * Original solution.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> levelOrderBottom(TreeNode* root) {
        if (nullptr == root) return {};

        vector< vector<int> > ans;
        deque<TreeNode*> q{root};

        while (false == q.empty()) {
            size_t  qsize = q.size();
            vector<int> nums;
            for (size_t i = 0; i < qsize; ++i) {
                TreeNode* node = q.front();
                q.pop_front();
                nums.push_back(node->val);
                if (node->left) q.push_back(node->left);
                if (node->right) q.push_back(node->right);
            }
            ans.push_back(nums);
        }

        std::reverse(ans.begin(), ans.end());

        return ans;
    }
};
