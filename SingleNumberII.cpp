/**
 * Single Number II
 *
 * Given an integer array nums where every element appears three times except for one, which appears
 * exactly once. Find the single element and return it.
 *
 * You must implement a solution with a linear runtime complexity and use only constant extra space.
 *
 * Example 1:
 *
 * Input: nums = [2,2,3,2]
 * Output: 3
 * Example 2:
 *
 * Input: nums = [0,1,0,1,0,1,99]
 * Output: 99
 *
 * Constraints:
 *
 * 1 <= nums.length <= 3 * 104
 * -231 <= nums[i] <= 231 - 1
 * Each element in nums appears exactly three times except for one element which appears once.
 */
/**
 * time O(32 * n) space O(1)
 */
class Solution {
public:
    int singleNumber(vector<int>& nums) {
        array<int, sizeof(int) * 8> bits_cnt = { 0, };

        for (auto n : nums) {
            for (int i = 0; i < bits_cnt.size(); ++i) {
                if (n & (1U << i)) {
                    ++bits_cnt[i];
                }
            }
        }

        int ans = 0;
        for (int i = 0; i < bits_cnt.size(); ++i) {
            if (bits_cnt[i] % 3) {
                ans |= 1U << i;
            }
        }

        return ans;
    }
};

/**
 * Uisng unordered_map
 */
class Solution {
public:
    int singleNumber(vector<int>& nums) {
        unordered_map<int, int> num_cnt;

        for (auto n : nums) {
            ++num_cnt[n];
            if (3 == num_cnt[n]) {
                num_cnt.erase(n);
            }
        }

        return num_cnt.begin()->first;
    }
};

/**
 * Origianal solution. Time O(nlogn), space O(1)
 */
class Solution {
public:
    int singleNumber(vector<int>& nums) {
        sort(begin(nums), end(nums));

        int len = 0;
        int prev = nums.front();
        for (auto n : nums) {
            if (n != prev) {
                if (3 != len) {
                    return prev;
                }
                len = 0;
            }
            ++len;
            prev = n;
        }

        return nums.back();
    }
};
