/**
 * Maximum Length of Subarray With Positive Product
 *
 * Given an array of integers nums, find the maximum length of a subarray where the product of all
 * its elements is positive.
 *
 * A subarray of an array is a consecutive sequence of zero or more values taken out of that array.
 *
 * Return the maximum length of a subarray with positive product.
 *
 * Example 1:
 *
 * Input: nums = [1,-2,-3,4]
 * Output: 4
 * Explanation: The array nums already has a positive product of 24.
 * Example 2:
 *
 * Input: nums = [0,1,-2,-3,-4]
 * Output: 3
 * Explanation: The longest subarray with positive product is [1,-2,-3] which has a product of 6.
 * Notice that we cannot include 0 in the subarray since that'll make the product 0 which is not positive.
 * Example 3:
 *
 * Input: nums = [-1,-2,-3,0,1]
 * Output: 2
 * Explanation: The longest subarray with positive product is [-1,-2] or [-2,-3].
 *
 * Constraints:
 *
 * 1 <= nums.length <= 105
 * -109 <= nums[i] <= 109
 */
/**
 * Same idea, but shorter. also less efficient than original.
 */
class Solution {
public:
    int getMaxLen(vector<int>& nums) {
        int positive_cnt = 0;
        int negative_cnt = 0;
        int ans = 0;

        for (auto n : nums) {
            if (n > 0) {
                ++positive_cnt;
                negative_cnt += negative_cnt > 0;
            }
            else if (n < 0) {
                swap(positive_cnt, negative_cnt);
                ++negative_cnt;
                positive_cnt += positive_cnt > 0;
            }
            else {
                positive_cnt = 0;
                negative_cnt = 0;
            }

            ans = max(ans, positive_cnt);
        }

        return ans;
    }
};

/**
 * Original solution. O(n) solution.
 */
bool is_odd_value(int n)
{
    return 0 == (n & 1);
}

class Solution {
public:
    int getMaxLen(vector<int>& nums) {
        int const N = nums.size();
        int ans = 0;

        for (int i = 0; i < N; ++i) {
            int j = i;
            int neg_cnt = 0;
            int first_neg_idx = -1;
            int last_neg_idx = -1;
            while (j < N) {
                if (nums[j] > 0) {
                    ;
                }
                else if (nums[j] < 0) {
                    ++neg_cnt;
                    if (first_neg_idx < 0) {
                        first_neg_idx = j;
                    }
                    last_neg_idx = j;
                }
                else {
                    break;
                }

                ++j;
            }

            if (is_odd_value(neg_cnt)) {
                ans = max(ans, j - i);
            }
            else {
                ans = max(ans, max(j - first_neg_idx - 1, last_neg_idx - i));
            }

            i = j;
        }

        return ans;
    }
};
