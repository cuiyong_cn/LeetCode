/**
 * Longest Well-Performing Interval
 *
 * We are given hours, a list of the number of hours worked per day for a given employee.
 *
 * A day is considered to be a tiring day if and only if the number of hours worked is (strictly)
 * greater than 8.
 *
 * A well-performing interval is an interval of days for which the number of tiring days is strictly
 * larger than the number of non-tiring days.
 *
 * Return the length of the longest well-performing interval.
 *
 * Example 1:
 *
 * Input: hours = [9,9,6,0,6,6,9]
 * Output: 3
 * Explanation: The longest well-performing interval is [9,9,6].
 * Example 2:
 *
 * Input: hours = [6,6,6]
 * Output: 0
 *
 * Constraints:
 *
 * 1 <= hours.length <= 104
 * 0 <= hours[i] <= 16
 */
/**
 * Passed one. Using hashmap.
 */
class Solution {
public:
    int longestWPI(vector<int>& hours) {
        int ans = 0, score = 0;
        int const days = hours.size();
        unordered_map<int, int> seen;

        for (int i = 0; i < days; ++i) {
            score += hours[i] > 8 ? 1 : -1;
            if (score > 0) {
                ans = i + 1;
            } else {
                if (0 == seen.count(score)) {
                    seen[score] = i;
                }

                if (seen.count(score - 1)) {
                    ans = max(ans, i - seen[score - 1]);
                }
            }
        }
        return ans;
    }
};

/**
 * Original solution. Passed all test cases, but TLE.
 */
class Solution {
public:
    int longestWPI(vector<int>& hours) {
        int const Days = hours.size();
        vector<int> tiring_day(Days + 1, 0);
        for (int i = 0; i < Days; ++i) {
            tiring_day[i + 1] = tiring_day[i] + (hours[i] > 8 ? 1 : -1);
        }

        int ans = 0;
        for (int j = Days; j > 0; --j) {
            if (ans >= j) {
                break;
            }

            for (int i = 0; i < j; ++i) {
                if ((tiring_day[j] - tiring_day[i]) > 0) {
                    ans = max(ans, j - i);
                    break;
                }
            }
        }

        return ans;
    }
};
