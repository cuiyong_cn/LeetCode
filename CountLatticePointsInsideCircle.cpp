/**
 * Count Lattice Points Inside a Circle
 *
 * Given a 2D integer array circles where circles[i] = [xi, yi, ri] represents the center (xi, yi)
 * and radius ri of the ith circle drawn on a grid, return the number of lattice points that are
 * present inside at least one circle.
 *
 * Note:
 *
 * A lattice point is a point with integer coordinates.
 * Points that lie on the circumference of a circle are also considered to be inside it.
 *
 * Example 1:
 *
 * Input: circles = [[2,2,1]]
 * Output: 5
 * Explanation:
 * The figure above shows the given circle.
 * The lattice points present inside the circle are (1, 2), (2, 1), (2, 2), (2, 3), and (3, 2) and
 * are shown in green.
 * Other points such as (1, 1) and (1, 3), which are shown in red, are not considered inside the circle.
 * Hence, the number of lattice points present inside at least one circle is 5.
 * Example 2:
 *
 * Input: circles = [[2,2,2],[3,4,1]]
 * Output: 16
 * Explanation:
 * The figure above shows the given circles.
 * There are exactly 16 lattice points which are present inside at least one circle.
 * Some of them are (0, 2), (2, 0), (2, 4), (3, 2), and (4, 4).
 *
 * Constraints:
 *
 * 1 <= circles.length <= 200
 * circles[i].length == 3
 * 1 <= xi, yi <= 100
 * 1 <= ri <= min(xi, yi)
 */
/**
 * Improve expressiveness
 */
auto square(int x)
{
    return x * x;
}

auto in_any_circle(vector<vector<int>> const& circles, int x, int y)
{
    for (auto const& c : circles) {
        auto x2 = square(x - c[0]);
        auto y2 = square(y - c[1]);
        auto r2 = square(c[2]);
        if ((x2 + y2) <= r2) {
            return 1;
        }
    }
    return 0;
}

class Solution {
public:
    int countLatticePoints(vector<vector<int>>& circles) {
        auto ans = 0;
        for (auto x = 0; x <= 200; ++x) {
            for (auto y = 0; y <= 200; ++y) {
                ans += in_any_circle(circles, x, y);
            }
        }
        return ans;
    }
};

/**
 * Since the range is limited. We can scan all the possible points.
 */
auto square(int x)
{
    return x * x;
}

class Solution {
public:
    int countLatticePoints(vector<vector<int>>& circles) {
        auto ans = 0;
        for (auto x = 0; x <= 200; ++x) {
            for (auto y = 0; y <= 200; ++y) {
                for (auto const& c : circles) {
                    auto x2 = square(x - c[0]);
                    auto y2 = square(y - c[1]);
                    auto r2 = square(c[2]);
                    if ((x2 + y2) <= r2) {
                        ++ans;
                        break;
                    }
                }
            }
        }
        return ans;
    }
};

/**
 * Original solution
 */
class Solution {
public:
    int countLatticePoints(vector<vector<int>>& circles) {
        auto points = set<pair<int, int>>{};
        for (auto const& c : circles) {
            auto x = c[0];
            auto y = c[1];
            auto r = c[2];
            auto r2 = r * r;
            for (auto xi = x - r, xend = x + r; xi <= xend; ++xi) {
                auto dx = xi - x;
                for (auto yi = y - r, yend = y + r; yi <= yend; ++yi) {
                    auto dy = yi - y;
                    if ((dx * dx + dy * dy) <= r2) {
                        points.emplace(xi, yi);
                    }
                }
            }
        }

        return points.size();
    }
};
