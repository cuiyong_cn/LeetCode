/**
 * Accounts Merge
 *
 * Given a list accounts, each element accounts[i] is a list of strings, where the first element accounts[i][0] is a
 * name, and the rest of the elements are emails representing emails of the account.
 *
 * Now, we would like to merge these accounts. Two accounts definitely belong to the same person if there is some email
 * that is common to both accounts. Note that even if two accounts have the same name, they may belong to different
 * people as people could have the same name. A person can have any number of accounts initially, but all of their
 * accounts definitely have the same name.
 *
 * After merging the accounts, return the accounts in the following format: the first element of each account is the
 * name, and the rest of the elements are emails in sorted order. The accounts themselves can be returned in any order.
 *
 * Example 1:
 *
 * Input:
 * accounts = [["John", "johnsmith@mail.com", "john00@mail.com"], ["John", "johnnybravo@mail.com"], ["John",
 * "johnsmith@mail.com", "john_newyork@mail.com"], ["Mary", "mary@mail.com"]] Output: [["John", 'john00@mail.com',
 * 'john_newyork@mail.com', 'johnsmith@mail.com'],  ["John", "johnnybravo@mail.com"], ["Mary", "mary@mail.com"]]
 *
 * Explanation:
 * The first and third John's are the same person as they have the common email "johnsmith@mail.com".
 * The second John and Mary are different people as none of their email addresses are used by other accounts.
 * We could return these lists in any order, for example the answer [['Mary', 'mary@mail.com'], ['John', 'johnnybravo@mail.com'],
 * ['John', 'john00@mail.com', 'john_newyork@mail.com', 'johnsmith@mail.com']] would still be accepted.
 *
 * Note:
 * The length of accounts will be in the range [1, 1000].
 * The length of accounts[i] will be in the range [1, 10].
 * The length of accounts[i][j] will be in the range [1, 30].
 */
/**
 * Original solution. Using graph and memo. The walking through the graph part
 * we can use the queue based method instead of recursion based. That may improve
 * the efficiency.
 */
class Solution {
public:
    vector<vector<string>> accountsMerge(vector<vector<string>>& accounts) {
        unordered_map<string, set<string>> graph;

        for (auto& emails : accounts) {
            for (size_t i = 1; i < emails.size(); ++i) {
                for (size_t j = i + 1; j < emails.size(); ++j) {
                    graph[emails[i]].insert(emails[j]);
                    graph[emails[j]].insert(emails[i]);
                }
            }
        }

        unordered_set<string> seen;
        vector<vector<string>> ans;
        for (auto& emails : accounts) {
            if (emails.size() > 1 && 0 == seen.count(emails[1])) {
                vector<string> acc{emails[0]};
                get_all_email_accounts(graph, seen, emails[1], acc);
                std::sort(std::begin(acc), std::end(acc));
                ans.push_back(acc);
            }
        }

        return ans;
    }

private:
    void get_all_email_accounts(unordered_map<string, set<string>>& g,
                                unordered_set<string>& seen, const string& email, vector<string>& acc)
    {
        if (seen.count(email)) return;

        seen.insert(email);
        acc.push_back(email);

        for (auto& e : g[email]) {
            get_all_email_accounts(g, seen, e, acc);
        }
    }
};
