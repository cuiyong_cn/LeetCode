/**
 * Greatest Common Divisor of Strings
 *
 * For strings S and T, we say "T divides S" if and only if S = T + ... + T  (T concatenated with itself 1 or more times)
 *
 * Return the largest string X such that X divides str1 and X divides str2.
 *
 * Example 1:
 *
 * Input: str1 = "ABCABC", str2 = "ABC"
 * Output: "ABC"
 * Example 2:
 *
 * Input: str1 = "ABABAB", str2 = "ABAB"
 * Output: "AB"
 * Example 3:
 *
 * Input: str1 = "LEET", str2 = "CODE"
 * Output: ""
 *
 *
 * Note:
 *
 * 1 <= str1.length <= 1000
 * 1 <= str2.length <= 1000
 * str1[i] and str2[i] are English uppercase letters.
 */
/**
 * Refactored one.
 */
class Solution {
public:
    inline bool isMultipleOf(string& str, string& base, int times) {
        string concat;
        while (times > 0) {
            concat += base;
            --times;
        }

        return concat == str;
    }

    string gcdOfStrings(string& str1, string& str2) {
        int len = min(str1.length(), str2.length());
        string ans;
        for (int i = len; i > 0; --i) {
            if (str1.length() % i != 0 || str2.length() % i != 0) continue;

            string base1 = str1.substr(0, i);
            string base2 = str2.substr(0, i);
            if (base1 != base2) continue;

            if (isMultipleOf(str1, base1, str1.length() / i)
               && isMultipleOf(str2, base2, str2.length() / i)) {
                ans = base1;
                break;
            }
        }

        return ans;
    }
};

/**
 * Original solution. Simple and intuitive. But not very elegant.
 */
class Solution {
public:
    string gcdOfStrings(string str1, string str2) {
        int len = min(str1.length(), str2.length());
        string ans;
        for (int i = len; i > 0; --i) {
            if (str1.length() % i != 0 || str2.length() % i != 0) continue;

            int times = str1.length() / i;
            string base1 = str1.substr(0, i);
            string base2 = str2.substr(0, i);
            if (base1 != base2) continue;

            string concat1;
            while (times > 0) {
                concat1 += base1;
                --times;
            }

            if (concat1 != str1) continue;

            times = str2.length() / i;
            string concat2;
            while (times > 0) {
                concat2 += base2;
                --times;
            }
            if (concat2 != str2) continue;

            ans = base1;
            break;
        }

        return ans;
    }
};
