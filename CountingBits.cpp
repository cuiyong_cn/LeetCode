/**
 * Counting Bits
 *
 * Given a non negative integer number num. For every numbers i in the range 0 ≤ i ≤ num
 * calculate the number of 1's in their binary representation and return them as an array.
 *
 * Example 1:
 *
 * Input: 2
 * Output: [0,1,1]
 *
 * Example 2:
 *
 * Input: 5
 * Output: [0,1,1,2,1,2]
 *
 * Follow up:
 *
 *     It is very easy to come up with a solution with run time O(n*sizeof(integer)).
 *     But can you do it in linear time O(n) /possibly in a single pass?
 *     Space complexity should be O(n).
 *     Can you do it like a boss? Do it without using any builtin function like
 *     __builtin_popcount in c++ or in any other language.
 */
/**
 * shortest one
 */
class Solution {
public:
    vector<int> countBits(int num) {
        vector<int> ret(num+1, 0);
        /**
         * i & (i - 1) means take off the right most bit 1. For example
         *
         * i = 3 => 11  then  i - 1 => 10, i & (i - 1) => 10
         * i = 4 => 100 then  i - 1 => 11, i & (i - 1) => 000
         */
        for (int i = 1; i <= num; ++i)
            ret[i] = ret[i&(i-1)] + 1;
        return ret;
    }
};

/**
 * My solution. It's ok, simple to digest, but not so efficient.
 */
class Solution {
public:
    vector<int> countBits(int num) {
        const int lt[16] = { 0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4 };
        vector<int> ans;
        ans.reserve(num + 1);
        for (int i = 0; i < num + 1; ++i) {
            if (i < 16) {
                ans.push_back(lt[i]);
            }
            else if (i < 256) {
                ans.push_back(lt[i & 0x0f] + lt[i>>4]);
            }
            else if (i < 4096) {
                ans.push_back(lt[i & 0x0f] + lt[(i>>4) & 0x0f] + lt[i>>8]);
            }
            else if (i < 65536) {
                ans.push_back(lt[i & 0x0f] + lt[(i>>4) & 0x0f] + lt[(i>>8) & 0x0f] + lt[i>>12]);
            }
            else if (i < 1048576) {
                ans.push_back(lt[i & 0x0f] + lt[(i>>4) & 0x0f] + lt[(i>>8) & 0x0f] + lt[(i>>12) & 0x0f] + lt[i>>16]);
            }
        }

        return ans;
    }
};

/**
 * A little improve. Take advantage of previous results
 */
class Solution {
public:
    vector<int> countBits(int num) {
        const int lt[16] = { 0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4 };
        vector<int> ans;
        ans.reserve(num + 1);
        for (int i = 0; i < num + 1; ++i) {
            if (i < 16) {
                ans.push_back(lt[i]);
            }
            else if (i < 256) {
                ans.push_back(lt[i & 0x0f] + lt[i>>4]);
            }
            else if (i < 4096) {
                ans.push_back(ans[i & 0xff] + lt[i>>8]);
            }
            else if (i < 65536) {
                ans.push_back(ans[i & 0x0fff] + lt[i>>12]);
            }
            else if (i < 1048576) {
                ans.push_back(ans[i & 0xffff] + lt[i>>16]);
            }
        }

        return ans;
    }
};
