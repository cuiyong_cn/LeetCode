/**
 * Most Common Word
 *Given a paragraph and a list of banned words, return the most frequent word that is not in the list of banned words.
 It is guaranteed there is at least one word that isn't banned, and that the answer is unique.
 *
 * Words in the list of banned words are given in lowercase, and free of punctuation.  Words in the paragraph are not
 * case sensitive.  The answer is in lowercase.
 *
 * Example:
 *
 * Input:
 * paragraph = "Bob hit a ball, the hit BALL flew far after it was hit."
 * banned = ["hit"]
 * Output: "ball"
 * Explanation:
 * "hit" occurs 3 times, but it is a banned word.
 * "ball" occurs twice (and no other word does), so it is the most frequent non-banned word in the paragraph.
 * Note that words in the paragraph are not case sensitive,
 * that punctuation is ignored (even if adjacent to words, such as "ball,"),
 * and that "hit" isn't the answer even though it occurs more because it is banned.
 *
 * Note:
 *
 * 1 <= paragraph.length <= 1000.
 * 0 <= banned.length <= 100.
 * 1 <= banned[i].length <= 10.
 * The answer is unique, and written in lowercase (even if its occurrences in paragraph may have uppercase symbols, and
 * even if it is a proper noun.)
 * paragraph only consists of letters, spaces, or the punctuation symbols !?',;.
 * There are no hyphens or hyphenated words.
 * Words only consist of letters, never apostrophes or other punctuation symbols.
 */
/**
 * Original solution.
 * Improve the below code using:
 *  1. convert all uppercase to lower and punctuations to ' '
 *  2. using stringstream to extract every word and build frequency map
 *  3. find the answer
 */
class Solution {
public:
    string mostCommonWord(string paragraph, vector<string>& banned) {
        set<string> banned_set(banned.begin(), banned.end());
        map<string, int> words_freq;

        string punctuations = " !?',;.";
        string word;

        auto lower = [](auto c) {
            return tolower(c);
        };

        for (auto ch : paragraph) {
            if (punctuations.find(ch) != string::npos) {
                if (!word.empty()) {
                    transform(word.begin(), word.end(), word.begin(), lower);

                    if (!banned_set.count(word)) {
                        ++words_freq[word];
                    }
                    word.clear();
                }
            }
            else {
                word += ch;
            }
        }

        if (!word.empty()) {
            transform(word.begin(), word.end(), word.begin(), lower);

            if (!banned_set.count(word)) {
                ++words_freq[word];
            }
            word.clear();
        }

        int max_freq = 0;
        string ans;
        for (const auto& p : words_freq) {
            if (p.second > max_freq) {
                max_freq = p.second;
                ans = p.first;
            }
        }

        return ans;
    }
};
