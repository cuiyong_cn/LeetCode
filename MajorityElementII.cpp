/**
 * Majority Element II
 *
 * Given an integer array of size n, find all elements that appear more than ⌊ n/3 ⌋ times.
 *
 * Follow-up: Could you solve the problem in linear time and in O(1) space?
 *
 * Example 1:
 *
 * Input: nums = [3,2,3]
 * Output: [3]
 * Example 2:
 *
 * Input: nums = [1]
 * Output: [1]
 * Example 3:
 *
 * Input: nums = [1,2]
 * Output: [1,2]
 *
 * Constraints:
 *
 * 1 <= nums.length <= 5 * 104
 * -109 <= nums[i] <= 109
 */
/**
 * Boyer Moore Majority vote. Not really intuitive. I don't like this one.
 */
class Solution {
public:
    vector<int> majorityElement(vector<int>& nums) {
        int cnt1 = 0, cnt2 = 0, a=0, b=1;

        for(auto n: nums){
            if (a==n){
                cnt1++;
            }
            else if (b==n){
                cnt2++;
            }
            else if (cnt1==0){
                a = n;
                cnt1 = 1;
            }
            else if (cnt2 == 0){
                b = n;
                cnt2 = 1;
            }
            else{
                cnt1--;
                cnt2--;
            }
        }

        cnt1 = cnt2 = 0;
        for(auto n: nums){
            if (n==a)   cnt1++;
            else if (n==b)  cnt2++;
        }

        vector<int> res;
        if (cnt1 > nums.size()/3)   res.push_back(a);
        if (cnt2 > nums.size()/3)   res.push_back(b);
        return res;
    }
};

/**
 * Reduce space to O(1) using sort
 */
class Solution {
public:
    vector<int> majorityElement(vector<int>& nums) {
        const int one_third = nums.size() / 3;
        vector<int> ans;
        
        sort(begin(nums), end(nums));
        int cnt = 1;

        for (int i = 1; i < nums.size(); ++i) {
            if (nums[i] != nums[i - 1]) {
                if (cnt > one_third) {
                    ans.emplace_back(nums[i - 1]);
                }
                cnt = 1;
            }
            else {
                ++cnt;
            }
        }

        if (cnt > one_third) {
            ans.emplace_back(nums.back());
        }

        return ans;
    }
};

/**
 * Original solution using map.
 */
class Solution {
public:
    vector<int> majorityElement(vector<int>& nums) {
        unordered_map<int, int> num_cnt;
        for (auto n : nums) {
            ++num_cnt[n];
        }

        const int one_third = nums.size() / 3;
        vector<int> ans;
        for (auto& [num, cnt] : num_cnt) {
            if (cnt > one_third) {
                ans.emplace_back(num);
            }
        }

        return ans;
    }
};
