/**
 * Minimum Substring Partition of Equal Character Frequency
 *
 * Given a string s, you need to partition it into one or more balanced substrings
 * For example, if s == "ababcc" then ("abab", "c", "c"), ("ab", "abc", "c"), and ("ababcc") are
 * all valid partitions, but ("a", "bab", "cc"), ("aba", "bc", "c"), and ("ab", "abcc") are not. The
 * unbalanced substrings are bolded.
 *
 * Return the minimum number of substrings that you can partition s into.
 *
 * Note: A balanced string is a string where each character in the string occurs the same number of times.
 *
 * Example 1:
 *
 * Input: s = "fabccddg"
 *
 * Output: 3
 *
 * Explanation:
 *
 * We can partition the string s into 3 substrings in one of the following ways: ("fab, "ccdd", "g"), or ("fabc", "cd", "dg").
 *
 * Example 2:
 *
 * Input: s = "abababaccddb"
 *
 * Output: 2
 *
 * Explanation:
 *
 * We can partition the string s into 2 substrings like so: ("abab", "abaccddb").
 *
 * Constraints:
 *     1 <= s.length <= 1000
 *     s consists only of English lowercase letters.
 */
/**
 * Passed one.
 */
class Solution {
public:
    int minimumSubstringsInPartition(string s) {
        int const len_s = s.length();
        auto constexpr max_int = numeric_limits<int>::max();
        auto dp = vector<int>(len_s + 1, max_int);

        dp[0] = 0;

        for (auto i = 0; i < len_s; ++i) {
            auto cnt_ch = array<int, 26>{ 0, };
            auto max_cnt = 0;
            auto unique = 0;

            for (auto j = i; j >= 0; --j) {
                auto cnt = ++cnt_ch[s[j] - 'a'];
                unique += 1 == cnt;
                max_cnt = max(max_cnt, cnt);

                // we have a balanced subpart
                if ((i - j + 1) == (unique * max_cnt)) {
                    dp[i + 1] = min(dp[j] + 1, dp[i + 1]);
                }
            }
        }

        return dp.back();
    }
};

/**
 * Original solution. This should work, but TLE.
 * Need some optimization for efficient checking of balanced string.
 */
#include <ranges>

bool is_balanced(string_view sv)
{
    auto cnt = array<int, 26>{ 0, };

    for (auto c : sv) {
        ++cnt[c - 'a'];
    }

    auto positive = [](auto i) { return i > 0; };
    auto freq = cnt | std::ranges::views::filter(positive);
    auto same = [f = freq.front()](auto v) { return v == f; };

    return std::ranges::all_of(freq, same);
}

class Solution {
public:
    int minimumSubstringsInPartition(string s) {
        int const len_s = s.length();
        auto constexpr max_int = numeric_limits<int>::max();
        auto dp = vector<int>(len_s, max_int);

        dp[0] = 1;

        for (auto i = 1; i < len_s; ++i) {
            if (is_balanced(string_view(s.data(), i + 1))) {
                dp[i] = 1;
            }

            for (auto j = 0; j < i; ++j) {
                auto part = string_view(s.data() + j + 1, i - j);
                auto balanced = is_balanced(part);
                dp[i] = balanced ? min(dp[i], dp[j] + 1) : dp[i];
            }
        }

        return dp.back();
    }
};
