/**
 * Find All Possible Recipes from Given Supplies
 *
 * You have information about n different recipes. You are given a string array recipes and a 2D
 * string array ingredients. The ith recipe has the name recipes[i], and you can create it if you
 * have all the needed ingredients from ingredients[i]. Ingredients to a recipe may need to be
 * created from other recipes, i.e., ingredients[i] may contain a string that is in recipes.
 *
 * You are also given a string array supplies containing all the ingredients that you initially
 * have, and you have an infinite supply of all of them.
 *
 * Return a list of all the recipes that you can create. You may return the answer in any order.
 *
 * Note that two recipes may contain each other in their ingredients.
 *
 * Example 1:
 *
 * Input: recipes = ["bread"], ingredients = [["yeast","flour"]], supplies = ["yeast","flour","corn"]
 * Output: ["bread"]
 * Explanation:
 * We can create "bread" since we have the ingredients "yeast" and "flour".
 * Example 2:
 *
 * Input: recipes = ["bread","sandwich"], ingredients = [["yeast","flour"],["bread","meat"]], supplies = ["yeast","flour","meat"]
 * Output: ["bread","sandwich"]
 * Explanation:
 * We can create "bread" since we have the ingredients "yeast" and "flour".
 * We can create "sandwich" since we have the ingredient "meat" and can create the ingredient "bread".
 * Example 3:
 *
 * Input: recipes = ["bread","sandwich","burger"], ingredients = [["yeast","flour"],["bread","meat"],["sandwich","meat","bread"]], supplies = ["yeast","flour","meat"]
 * Output: ["bread","sandwich","burger"]
 * Explanation:
 * We can create "bread" since we have the ingredients "yeast" and "flour".
 * We can create "sandwich" since we have the ingredient "meat" and can create the ingredient "bread".
 * We can create "burger" since we have the ingredient "meat" and can create the ingredients "bread" and "sandwich".
 *
 * Constraints:
 *
 * n == recipes.length == ingredients.length
 * 1 <= n <= 100
 * 1 <= ingredients[i].length, supplies.length <= 100
 * 1 <= recipes[i].length, ingredients[i][j].length, supplies[k].length <= 10
 * recipes[i], ingredients[i][j], and supplies[k] consist only of lowercase English letters.
 * All the values of recipes and supplies combined are unique.
 * Each ingredients[i] does not contain any duplicate values.
 */
/**
 * Original solution.
 * Of course we could use topological sorting. But that is too heavy for this one I think.
 */
using RecipeBook = unordered_map<string, vector<string>>;

RecipeBook build_recipe_book(vector<string> const& recipes,  vector<vector<string>> const& ingredients)
{
    auto book = RecipeBook{};
    auto rsize = static_cast<int>(recipes.size());
    for (auto i = int{0}; i < rsize; ++i) {
        book[recipes[i]] = ingredients[i];
    }

    return book;
}

bool can_make_recipe
    ( string const& r,
      RecipeBook const& book,
      unordered_set<string>& supplies,
      unordered_set<string>& chain)
{
    if (chain.count(r) || !book.count(r)) {
        return false;
    }

    if (supplies.count(r)) {
        return true;
    }

    chain.insert(r);

    for (auto const& ing : book.at(r)) {
        if (!supplies.count(ing)) {
            if (!can_make_recipe(ing, book, supplies, chain)) {
                return false;
            }
        }
    }
    chain.erase(r);

    supplies.insert(r);

    return true;
}

class Solution {
public:
    vector<string> findAllRecipes(vector<string>& recipes, vector<vector<string>>& ingredients, vector<string>& supplies) {
        auto recipe_book = build_recipe_book(recipes, ingredients);
        auto supply_set = unordered_set<string>{begin(supplies), end(supplies)};
        auto ans = vector<string>{};

        for (auto const& r : recipes) {
            auto chain = unordered_set<string>{};
            if (can_make_recipe(r, recipe_book, supply_set, chain)) {
                ans.emplace_back(r);
            }
        }

        return ans;
    }
};
