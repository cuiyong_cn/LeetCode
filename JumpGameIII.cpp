/**
 * Jump Game III
 *
 * Given an array of non-negative integers arr, you are initially positioned at start index of the
 * array. When you are at index i, you can jump to i + arr[i] or i - arr[i], check if you can reach
 * to any index with value 0.
 *
 * Notice that you can not jump outside of the array at any time.
 *
 * Example 1:
 *
 * Input: arr = [4,2,3,0,3,1,2], start = 5
 * Output: true
 * Explanation:
 * All possible ways to reach at index 3 with value 0 are:
 * index 5 -> index 4 -> index 1 -> index 3
 * index 5 -> index 6 -> index 4 -> index 1 -> index 3
 * Example 2:
 *
 * Input: arr = [4,2,3,0,3,1,2], start = 0
 * Output: true
 * Explanation:
 * One possible way to reach at index 3 with value 0 is:
 * index 0 -> index 4 -> index 1 -> index 3
 * Example 3:
 *
 * Input: arr = [3,0,2,1,2], start = 2
 * Output: false
 * Explanation: There is no way to reach at index 1 with value 0.
 *
 * Constraints:
 *
 * 1 <= arr.length <= 5 * 104
 * 0 <= arr[i] < arr.length
 * 0 <= start < arr.length
 */
/**
 * Original solution.
 */
class Solution {
public:
    bool canReach(vector<int>& arr, int start) {
        int const asize = arr.size();
        vector<bool> been_there(asize, false);

        deque<int> q = {start};
        while (!q.empty()) {
            auto const N = q.size();
            for (size_t i = 0; i < N; ++i) {
                auto idx = q.front(); q.pop_front();
                if (idx < 0 || idx >= asize || been_there[idx]) {
                    continue;
                }

                been_there[idx] = true;

                auto jmp = arr[idx];
                if (0 == jmp) {
                    return true;
                }

                q.push_back(idx + jmp);
                q.push_back(idx - jmp);
            }
        }

        return false;
    }
};
