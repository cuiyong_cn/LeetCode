/**
 * Satisfiability of Equality Equations
 *
 * Given an array equations of strings that represent relationships between variables, each string equations[i] has
 * length 4 and takes one of two different forms: "a==b" or "a!=b".  Here, a and b are lowercase letters (not
 * necessarily different) that represent one-letter variable names.
 *
 * Return true if and only if it is possible to assign integers to variable names so as to satisfy all the given
 * equations.
 *
 * Example 1:
 *
 * Input: ["a==b","b!=a"]
 * Output: false
 * Explanation: If we assign say, a = 1 and b = 1, then the first equation is satisfied, but not the second.  There is
 * no way to assign the variables to satisfy both equations.
 * Example 2:
 *
 * Input: ["b==a","a==b"]
 * Output: true
 * Explanation: We could assign a = 1 and b = 1 to satisfy both equations.
 * Example 3:
 *
 * Input: ["a==b","b==c","a==c"]
 * Output: true
 * Example 4:
 *
 * Input: ["a==b","b!=c","c==a"]
 * Output: false
 * Example 5:
 *
 * Input: ["c==c","b==d","x!=z"]
 * Output: true
 *
 * Note:
 *
 * 1 <= equations.length <= 500
 * equations[i].length == 4
 * equations[i][0] and equations[i][3] are lowercase letters
 * equations[i][1] is either '=' or '!'
 * equations[i][2] is '='
 */
/**
 * Actually, I tried the union find method to solve this one.
 * But there must be point I've been missing.
 * Correct uf solution.
 */
class Solution {
public:
    int uf[26];
    bool equationsPossible(vector<string>& equations) {
        for (int i = 0; i < 26; ++i) {
            uf[i] = i;
        }

        for (string e : equations) {
            if ('=' == e[1]) {
                uf[find(e[0] - 'a')] = find(e[3] - 'a');
            }
        }

        for (string e : equations) {
            if ('!' == e[1] && find(e[0] - 'a') == find(e[3] - 'a')) {
                return false;
            }
        }

        return true;
    }

    int find(int x) {
        if (x != uf[x]) {
            uf[x] = find(uf[x]);
        }

        return uf[x];
    }
};

/**
 * Original solution. I spent quite a lot time on this one.
 * Really messed my mind.
 */
class Solution {
public:
    bool equationsPossible(vector<string>& equations) {
        vector<vector<bool>> eq_map(26, vector<bool>(26, false));
        vector<string> ne_equations;

        for (const auto& e : equations) {
            if ('=' == e[1]) {
                eq_map[e[0] - 'a'][e[3] - 'a'] = true;
                eq_map[e[3] - 'a'][e[0] - 'a'] = true;
            }
            else {
                ne_equations.push_back(e);
            }
        }

        map<char, string> equal;
        for (const auto& ne : ne_equations) {
            if (!equal.count(ne[0])) {
                equal[ne[0]] = find_equal(eq_map, ne[0]);
            }

            if (string::npos != equal[ne[0]].find(ne[3])) {
                return false;
            }

            if (!equal.count(ne[3])) {
                equal[ne[3]] = find_equal(eq_map, ne[3]);
            }

            if (string::npos != equal[ne[3]].find(ne[0])) {
                return false;
            }
        }

        return true;
    }

private:
    string find_equal(const vector<vector<bool>>& eq_map, char c)
    {
        set<char> eq_chars;
        vector<bool> visited(26, false);
        dfs(eq_map, c, visited, eq_chars);

        return string(eq_chars.begin(), eq_chars.end());
    }

    void dfs(const vector<vector<bool>>& eq_map, char c, vector<bool>& visited, set<char>& eq_chars)
    {
        if (!visited[c - 'a']) {
            visited[c - 'a'] = true;
            eq_chars.insert(c);
            const auto& row = eq_map[c - 'a'];
            for (int i = 0; i < row.size(); ++i) {
                if (row[i]) {
                    dfs(eq_map, 'a' + i, visited, eq_chars);
                }
            }
        }
    }
};
