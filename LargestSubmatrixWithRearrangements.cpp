/**
 * Largest Submatrix With Rearrangements
 *
 * You are given a binary matrix matrix of size m x n, and you are allowed to rearrange the columns
 * of the matrix in any order.
 *
 * Return the area of the largest submatrix within matrix where every element of the submatrix is 1
 * after reordering the columns optimally.
 *
 * Example 1:
 *
 * Input: matrix = [[0,0,1],[1,1,1],[1,0,1]]
 * Output: 4
 * Explanation: You can rearrange the columns as shown above.
 * The largest submatrix of 1s, in bold, has an area of 4.
 * Example 2:
 *
 * Input: matrix = [[1,0,1,0,1]]
 * Output: 3
 * Explanation: You can rearrange the columns as shown above.
 * The largest submatrix of 1s, in bold, has an area of 3.
 * Example 3:
 *
 * Input: matrix = [[1,1,0],[1,0,1]]
 * Output: 2
 * Explanation: Notice that you must rearrange entire columns, and there is no way to make a submatrix of 1s larger than an area of 2.
 *
 * Constraints:
 *
 * m == matrix.length
 * n == matrix[i].length
 * 1 <= m * n <= 105
 * matrix[i][j] is either 0 or 1
 */
/**
 * Optimize a little.
 */
class Solution {
public:
    int largestSubmatrix(vector<vector<int>>& matrix) {
        int const M = matrix.size();
        int const N = matrix[0].size();

        for (int c = 0; c < N; ++c) {
            for (int r = 1; r < M; ++r) {
                if (1 == matrix[r][c]) {
                    matrix[r][c] += matrix[r - 1][c];
                }
            }
        }

        int ans = 0;
        for (auto& row : matrix) {
            sort(row.begin(), row.end(), greater<int>());
            for (int width = 0; width < N; ++width) {
                ans = max(ans, row[width] * (width + 1));
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int largestSubmatrix(vector<vector<int>>& matrix) {
        int const M = matrix.size();
        int const N = matrix[0].size();

        for (int c = 0; c < N; ++c) {
            for (int r = 0; r < M; ++r) {
                if (1 == matrix[r][c]) {
                    if (r > 0) {
                        matrix[r][c] += matrix[r - 1][c];
                    }
                }
            }
        }

        int ans = 0;
        for (auto& row : matrix) {
            sort(row.begin(), row.end(), greater<int>());
            auto height = numeric_limits<int>::max();
            for (int width = 0; width < N; ++width) {
                height = min(height, row[width]);
                ans = max(ans, height * (width + 1));
            }
        }

        return ans;
    }
};
