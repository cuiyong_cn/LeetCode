/**
 * Buddy Strings
 *
 * Given two strings s and goal, return true if you can swap two letters in s so the result is equal
 * to goal, otherwise, return false.
 *
 * Swapping letters is defined as taking two indices i and j (0-indexed) such that i != j and
 * swapping the characters at s[i] and s[j].
 *
 * For example, swapping at indices 0 and 2 in "abcd" results in "cbad".
 *
 * Example 1:
 *
 * Input: s = "ab", goal = "ba"
 * Output: true
 * Explanation: You can swap s[0] = 'a' and s[1] = 'b' to get "ba", which is equal to goal.
 * Example 2:
 *
 * Input: s = "ab", goal = "ab"
 * Output: false
 * Explanation: The only letters you can swap are s[0] = 'a' and s[1] = 'b', which results in "ba" != goal.
 * Example 3:
 *
 * Input: s = "aa", goal = "aa"
 * Output: true
 * Explanation: You can swap s[0] = 'a' and s[1] = 'a' to get "aa", which is equal to goal.
 * Example 4:
 *
 * Input: s = "aaaaaaabc", goal = "aaaaaaacb"
 * Output: true
 *
 * Constraints:
 *
 * 1 <= s.length, goal.length <= 2 * 104
 * s and goal consist of lowercase letters.
 */
/**
 * Original solution.
 */
class Solution {
public:
    bool buddyStrings(string s, string goal) {
        if (s.length() != goal.length()) {
            return false;
        }

        vector<int> diffs;
        vector<int> char_cnt(26, 0);
        for (size_t i = 0; i < s.length(); ++i) {
            if (s[i] != goal[i]) {
                diffs.push_back(i);
            }
            else {
                ++char_cnt[s[i] - 'a'];
            }
        }

        if (diffs.empty()) {
            return find_if(char_cnt.begin(), char_cnt.end(),
                       [](auto cnt) {
                           return cnt > 1;
                       }) != char_cnt.end();
        }

        if (diffs.size() == 2) {
            return s[diffs[0]] == goal[diffs[1]] && s[diffs[1]] == goal[diffs[0]];
        }

        return false;
    }
};
