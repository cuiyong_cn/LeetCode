/**
 * Rotate Array
 *
 * Given an array, rotate the array to the right by k steps, where k is non-negative.
 *
 * Example 1:
 *
 * Input: nums = [1,2,3,4,5,6,7], k = 3
 * Output: [5,6,7,1,2,3,4]
 * Explanation:
 * rotate 1 steps to the right: [7,1,2,3,4,5,6]
 * rotate 2 steps to the right: [6,7,1,2,3,4,5]
 * rotate 3 steps to the right: [5,6,7,1,2,3,4]
 * Example 2:
 *
 * Input: nums = [-1,-100,3,99], k = 2
 * Output: [3,99,-1,-100]
 * Explanation:
 * rotate 1 steps to the right: [99,-1,-100,3]
 * rotate 2 steps to the right: [3,99,-1,-100]
 *
 * Constraints:
 *
 * 1 <= nums.length <= 105
 * -231 <= nums[i] <= 231 - 1
 * 0 <= k <= 105
 *
 * Follow up:
 *
 * Try to come up with as many solutions as you can. There are at least three different ways to
 * solve this problem.
 * Could you do it in-place with O(1) extra space?
 */
/**
 * Using reverse.
 */
class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        k = k % nums.size();
        if (0 == k) {
            return;
        }

        reverse(begin(nums), end(nums));
        reverse(begin(nums), begin(nums) + k);
        reverse(begin(nums) + k, end(nums));
    }
};

/**
 * Manually implement the rotate
 */
class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        k = k % nums.size();
        if (0 == k) {
            return;
        }

        int next = 0;
        int N = nums.size();
        int start = 0;
        for (int i = 0; i < N; ++i) {
            next = (next + k) % N;
            if (start == next) {
                next = ++start;
            }
            else {
                swap(nums[start], nums[next]);
            }
        }
    }
};

/**
 * Using std algorithm.
 */
class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        k = k % nums.size();
        std::rotate(begin(nums), begin(nums) + (nums.size() - k), end(nums));
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        vector<int> nums_copy{nums};
        k = k % nums_copy.size();
        copy(begin(nums_copy) + (nums_copy.size() - k), end(nums_copy), begin(nums));
        copy(begin(nums_copy), begin(nums_copy) + (nums_copy.size() - k), begin(nums) + k);
    }
};
