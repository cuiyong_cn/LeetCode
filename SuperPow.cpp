/**
 * Super Pow
 *
 * Your task is to calculate ab mod 1337 where a is a positive integer and b is an extremely large
 * positive integer given in the form of an array.
 *
 * Example 1:
 *
 * Input: a = 2, b = [3]
 * Output: 8
 * Example 2:
 *
 * Input: a = 2, b = [1,0]
 * Output: 1024
 * Example 3:
 *
 * Input: a = 1, b = [4,3,3,8,5,2]
 * Output: 1
 * Example 4:
 *
 * Input: a = 2147483647, b = [2,0,0]
 * Output: 1198
 *
 * Constraints:
 *
 * 1 <= a <= 231 - 1
 * 1 <= b.length <= 2000
 * 0 <= b[i] <= 9
 * b doesn't contain leading zeros.
 */
/**
 * Half the vetor is too time consume. So we change the strategy.
 * Handle one digit at a time.
 */
class Solution {
    const int base = 1337;
    int powmod(int a, int k)
    {
        a %= base;
        int result = 1;
        for (int i = 0; i < k; ++i)
            result = (result * a) % base;
        return result;
    }
public:
    int superPow(int a, vector<int>& b) {
        if (b.empty()) return 1;
        int last_digit = b.back();
        b.pop_back();
        return powmod(superPow(a, b), 10) * powmod(a, last_digit) % base;
    }
};

/**
 * Original solution. Every time we half the power.
 * Basic idea: (a * b) % k = (a % k) * (b % k) % k
 */
class Solution {
public:
    int superPow(int a, vector<int>& b) {
        if (1 == a) {
            return 1;
        }

        if (1 == b.size()) {
            if (1 == b.back()) {
                return a;
            }
            if (0 == b.back()) {
                return 1;
            }
        }

        auto h1 = half(b);
        auto r1 = superPow(a, h1) % 1337;
        auto r2 = r1;
        if (b.back() & 1) {
            r2 = r2 * (a % 1337) % 1337;
        }

        return r1 * r2 % 1337;
    }

private:
    vector<int> half(vector<int> const& b) {
        int den = 0;
        vector<int> ans;
        for (auto n : b) {
            den = den * 10 + n;
            auto res = den / 2;
            ans.push_back(res);
            den = den - res * 2;
        }

        auto it = find_if(begin(ans), end(ans),
                         [](auto n) {
                             return 0 != n;
                         });
        return vector<int>(it, end(ans));
    }
};
