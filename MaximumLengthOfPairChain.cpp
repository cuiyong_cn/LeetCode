/**
 * Maximum Length of Pair Chain
 *
 * You are given n pairs of numbers. In every pair, the first number is always smaller than the second number.
 *
 * Now, we define a pair (c, d) can follow another pair (a, b) if and only if b < c. Chain of pairs can be formed in
 * this fashion.
 *
 * Given a set of pairs, find the length longest chain which can be formed. You needn't use up all the given pairs. You
 * can select pairs in any order.
 *
 * Example 1:
 *
 * Input: [[1,2], [2,3], [3,4]]
 * Output: 2
 * Explanation: The longest chain is [1,2] -> [3,4]
 *
 * Note:
 *     The number of given pairs will be in the range [1, 1000].
 */
/**
 * original solution.
 */
class Solution {
public:
    int findLongestChain(vector<vector<int>>& pairs) {
        if (pairs.size() < 2) return pairs.size();

        sort(pairs.begin(), pairs.end(),
            [](auto& a, auto& b) {
                if (a.empty()) return false;
                if (b.empty()) return true;
                return a[1] < b[1];
            });

        int ans = 1;
        int prev = pairs[0][1];
        for (int i = 1; i < pairs.size(); ++i) {
            if (pairs[i][0] > prev) {
                ++ans;
                prev = pairs[i][1];
            }
        }

        return ans;
    }
};

/**
 * dp solution.
 */
class Solution {
public:
    int findLongestChain(vector<vector<int>>& pairs) {
        if (pairs.size() < 2) return pairs.size();

        sort(pairs.begin(), pairs.end(),
            [](const auto& a, const auto& b) {
                if (a.empty()) return false;
                if (b.empty()) return true;
                return a[1] < b[1];
            });

        vector<int> dp(pairs.size(), 1);
        for (int i = 1; i < pairs.size(); ++i) {
            for (int j = 0; j < i; ++j) {
                if (pairs[j][1] < pairs[i][0]) {
                    dp[i] = max(dp[i], dp[j] + 1);
                }
            }
        }

        return *max_element(dp.begin(), dp.end());
    }
};
