/**
 * Permutations II
 *
 * Given a collection of numbers that might contain duplicates, return all possible unique permutations.
 *
 * Example:
 *
 * Input: [1,1,2]
 * Output:
 * [
 *   [1,1,2],
 *   [1,2,1],
 *   [2,1,1]
 * ]
 */
/**
 * Original solution.
 */
class Solution {
public:
    vector<vector<int>> permuteUnique(vector<int>& nums) {
        unordered_map<int, int> counter;

        for (const auto n : nums) {
            ++counter[n];
        }

        vector<vector<int>> ans;
        vector<int> comb;
        backtrack(ans, counter, comb, nums.size());
        return ans;
    }

private:
    void backtrack(vector<vector<int>>& ans, unordered_map<int, int>& counter, vector<int>& comb, int N)
    {
        if (comb.size() == N) {
            ans.push_back(comb);
            return;
        }

        for (auto& pair : counter) {
            auto& [key, value] = pair;
            if (value > 0) {
                value -= 1;
                comb.push_back(key);

                backtrack(ans, counter, comb, N);

                comb.pop_back();
                value += 1;
            }
        }
    }
};
