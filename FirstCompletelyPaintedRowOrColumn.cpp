/**
 * First Completely Painted Row or Column
 *
 * You are given a 0-indexed integer array arr, and an m x n integer matrix
 * mat. arr and mat both contain all the integers in the range [1, m * n].
 *
 * Go through each index i in arr starting from index 0 and paint the cell in mat containing the integer arr[i].
 *
 * Return the smallest index i at which either a row or a column will be completely painted in mat.
 *
 * Example 1:
 *
 * image explanation for example 1
 * Input: arr = [1,3,4,2], mat = [[1,4],[2,3]]
 * Output: 2
 * Explanation: The moves are shown in order, and both the first row and second column of the matrix become fully painted at arr[2].
 * Example 2:
 *
 * image explanation for example 2
 * Input: arr = [2,8,7,4,1,3,5,6,9], mat = [[3,2,5],[1,4,6],[8,7,9]]
 * Output: 3
 * Explanation: The second column becomes fully painted at arr[3].
 *
 * Constraints:
 *
 * m == mat.length
 * n = mat[i].length
 * arr.length == m * n
 * 1 <= m, n <= 105
 * 1 <= m * n <= 105
 * 1 <= arr[i], mat[r][c] <= m * n
 * All the integers of arr are unique.
 * All the integers of mat are unique.
 */
/**
 * Original solution.
 */
class Solution {
public:
    int firstCompleteIndex(vector<int>& arr, vector<vector<int>>& mat) {
        auto const m = mat.size();
        auto const n = mat[0].size();
        auto const total = m * n;
        auto cell_map = unordered_map<int, pair<int, int>>{};
        auto cell_painted_row = vector<int>(m + 1, 0);
        auto cell_painted_col = vector<int>(n + 1, 0);
        for (auto i = 0; i < m; ++i) {
            for (auto j = 0; j < n; ++j) {
                cell_map[mat[i][j]] = {i, j};
            }
        }

        for (auto i = 0; i < total; ++i) {
            auto [row, col] = cell_map[arr[i]];
            auto painted_row = ++cell_painted_row[row];
            auto painted_col = ++cell_painted_col[col];
            if (n == painted_row || m == painted_col) {
                return i;
            }
        }

        return 0;
    }
};
