/**
 * Largest 1-Bordered Square
 *
 * Given a 2D grid of 0s and 1s, return the number of elements in the largest square subgrid that has all 1s on its
 * border, or 0 if such a subgrid doesn't exist in the grid.
 *
 * Example 1:
 *
 * Input: grid = [[1,1,1],[1,0,1],[1,1,1]]
 * Output: 9
 * Example 2:
 *
 * Input: grid = [[1,1,0,0]]
 * Output: 1
 *
 * Constraints:
 *
 * 1 <= grid.length <= 100
 * 1 <= grid[0].length <= 100
 * grid[i][j] is 0 or 1
 */
/**
 * I think this is the same with the original. Just using another
 * info to check the four side of the square. And seems not efficient as
 * the original one.
 */
class Solution {
public:
    int largest1BorderedSquare(vector<vector<int>>& A) {
        int m = A.size(), n = A[0].size();
        vector<vector<int>> left(m, vector<int>(n)), top(m, vector<int>(n));
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                left[i][j] = A[i][j] + (j && A[i][j] ? left[i][j - 1] : 0);
                top[i][j] = A[i][j] + (i && A[i][j] ? top[i - 1][j] : 0);
            }
        }

        for (int L = min(m, n); L > 0; --L) {
            for (int i = 0; i < m - L + 1; ++i) {
                for (int j = 0; j < n - L + 1; ++j) {
                    if (std::min(
                                    {
                                        top[i + L - 1][j], // left
                                        top[i + L - 1][j + L - 1], // right
                                        left[i][j + L - 1], // up
                                        left[i + L - 1][j + L - 1] // bottom
                                    }
                                ) >= L) {
                        return L * L;
                    }
                }
            }
        }

        return 0;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int largest1BorderedSquare(vector<vector<int>>& grid) {
        int R = grid.size();
        int C = grid[0].size();
        int MR = std::min(R, C) - 1;

        vector<vector<int>> row_sum(R + 1, vector<int>(C + 1, 0));
        vector<vector<int>> col_sum(R + 1, vector<int>(C + 1, 0));

        for (size_t r = 0; r < grid.size(); ++r) {
            std::partial_sum(grid[r].begin(), grid[r].end(), row_sum[r + 1].begin() + 1);
        }

        for (size_t c = 0; c < grid[0].size(); ++c) {
            int prev = 0;
            for (size_t r = 0; r < grid.size(); ++r) {
                col_sum[r + 1][c + 1] = grid[r][c] + prev;
                prev = col_sum[r + 1][c + 1];
            }
        }

        for (size_t mr = MR; mr >= 0; --mr) {
            for (size_t r = 1; (r + mr) <= R; ++r) {
                for (size_t c = 1; (c + mr) <= C; ++c) {
                    int up_ones = row_sum[r][c + mr] - row_sum[r][c - 1];
                    if (up_ones != (mr + 1)) {
                        continue;
                    }

                    int down_ones = row_sum[r + mr][c + mr] - row_sum[r + mr][c - 1];
                    if (down_ones != (mr + 1)) {
                        continue;
                    }

                    int left_ones = col_sum[r + mr][c] - col_sum[r - 1][c];
                    if (left_ones != (mr + 1)) {
                        continue;
                    }

                    int right_ones = col_sum[r + mr][c + mr] - col_sum[r - 1][c + mr];
                    if (right_ones != (mr + 1)) {
                        continue;
                    }

                    return (mr + 1) * (mr + 1);
                }
            }
        }

        return 0;
    }
};
