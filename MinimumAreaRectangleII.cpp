/**
 *  Minimum Area Rectangle II
 *
 * Given a set of points in the xy-plane, determine the minimum area of any rectangle formed from these points, with
 * sides not necessarily parallel to the x and y axes.
 *
 * If there isn't any rectangle, return 0.
 *
 * Example 1:
 *
 * Input: [[1,2],[2,1],[1,0],[0,1]]
 * Output: 2.00000
 * Explanation: The minimum area rectangle occurs at [1,2],[2,1],[1,0],[0,1], with an area of 2.
 *
 * Example 2:
 *
 * Input: [[0,1],[2,1],[1,1],[1,0],[2,0]]
 * Output: 1.00000
 * Explanation: The minimum area rectangle occurs at [1,0],[1,1],[2,1],[2,0], with an area of 1.
 *
 * Example 3:
 *
 * Input: [[0,3],[1,2],[3,1],[1,3],[2,1]]
 * Output: 0
 * Explanation: There is no possible rectangle to form from these points.
 *
 * Example 4:
 *
 * Input: [[3,1],[1,1],[0,1],[2,1],[3,3],[3,2],[0,2],[2,3]]
 * Output: 2.00000
 * Explanation: The minimum area rectangle occurs at [2,1],[2,3],[3,3],[3,1], with an area of 2.
 *
 * Note:
 *     1 <= points.length <= 50
 *     0 <= points[i][0] <= 40000
 *     0 <= points[i][1] <= 40000
 *     All points are distinct.
 *     Answers within 10^-5 of the actual value will be accepted as correct.
 */
/**
 * Solution from leetcode
 * 1. For each triangle, let's try to find the 4th point and whether it is a rectangle.
 */
class Solution {
public:
    double minAreaFreeRect(vector<vector<int>>& points) {
        auto pack = [](const vector<int> &p) {return (uint64_t(p[0])<<32) | p[1];};

        unordered_set<int64_t> s;
        for (const auto &p : points) {
            s.insert(pack(p));
        }

        double best = -1;
        // take every 3 points and see if there is a corner
        for (int a_index=0; a_index<points.size(); ++a_index) {
            const auto &a = points[a_index];
            for (int b_index=a_index+1; b_index<points.size(); ++b_index) {
                const auto &b = points[b_index];
                for (int c_index=b_index+1; c_index<points.size(); ++c_index) {
                    const auto &c = points[c_index];
                    // build vectors ab and ac
                    const int ab_x = b[0]-a[0];
                    const int ab_y = b[1]-a[1];

                    const int ac_x = c[0]-a[0];
                    const int ac_y = c[1]-a[1];

                    // vectors ab and ac are orthogonal if the scalar product is zero
                    if (ab_x*ac_x+ab_y*ac_y != 0) {
                        // not orthogonal: ab and ac cannot form sides of a rectangle
                        continue;
                    }

                    // point d is sum of vectors ab and ac
                    const int d_x  = a[0] + ab_x + ac_x;
                    const int d_y  = a[1] + ab_y + ac_y;

                    // does d exist?
                    if (s.find(pack({d_x, d_y})) == s.end()) {
                        continue;
                    }

                    // area is length of vector ab * length of vector ac
                    const double area = sqrt((double(ab_x)*ab_x+ab_y*ab_y)*(ac_x*ac_x+ac_y*ac_y));
                    if (area>0.000000001) { // ignore empty rects
                        if (best<0 || area<best) {
                            best = area;
                        }
                    }
                }
            }
        }

        return best < 0 ? 0 : best;
    }
};

/**
 * 2. Iterate Centers
 */
class Solution {
private:
    // square of segment(p1, p2) length
    long lenSq(vector<int>& p1, vector<int>& p2) {
      return (p1[0] - p2[0]) * (p1[0] - p2[0]) + (p1[1] - p2[1]) * (p1[1] - p2[1]);
    }

public:
    double minAreaFreeRect(vector<vector<int>>& pts)
    {
      // PtGroups: (len^2, 2*xc, 2*yc)-> {{x1,y1},...}
      // where segment (x1,y)-(x2,y2) has length len and center (xc,yc)
      unordered_map<string, vector<vector<int>>> PtGroups; // group by center and length

      for (int j = 1; j < pts.size(); j++)
        for (int i = 0; i < j; i++)
        {
          int xc2 = pts[i][0]+pts[j][0];
          int yc2 = pts[i][1]+pts[j][1];
          long distSq = lenSq(pts[i], pts[j]);
          string key = to_string(distSq) + " " + to_string(xc2) + " " + to_string(yc2);
          PtGroups[key].push_back(pts[i]); // just need one end point
        }

      long minAreaSq = LONG_MAX;
      for (auto& p : PtGroups) {
        stringstream key(p.first);
        string tmp; key >> tmp;
        long distSq = stol(tmp);

        for (int j = 1; j < p.second.size(); j++)
          for (int i = 0; i < j; i++)
          {
            auto p1 = p.second[i];
            auto p2 = p.second[j];
            minAreaSq = min(minAreaSq, lenSq(p1,p2)*(distSq - lenSq(p1,p2))); // using Pythagoras's Theorem
          }
      }

      return minAreaSq == LONG_MAX? 0 : sqrt(minAreaSq);
    }
};

/**
 * Original solution. Finally passed. But a little dumb. The key to this question are two things:
 *  1. give 4 points, how to quickly decide if they can form a rect
 *  2. If they can form a rect, how to quickly calculate its area
 *
 * The below method is not the best one but works.
 */
class Solution {
public:
    double minAreaFreeRect(vector<vector<int>>& points) {
        ans = 1610000000.0;
        rectPoints = { -1, -1, -1, -1 };

        dfs(0, 0, points);

        return (1610000000.0 - ans) < 0.0001 ? 0.0 : ans;
    }

private:
    void dfs(int idx, int num, const vector<vector<int>>& points)
    {
        if (4 == num) {
            double area = 0;
            if (canFormRect(points, area)) {
                ans = min(ans, area);
            }
        }
        else {
            if (idx < points.size()) {
                rectPoints[num] = idx;
                dfs(idx + 1, num + 1, points);
                dfs(idx + 1, num, points);
            }
        }
    }

    bool canFormRect(const vector<vector<int>>& points, double& area)
    {
        auto& p = rectPoints;
        int p1x = points[p[0]][0], p1y = points[p[0]][1],
            p2x = points[p[1]][0], p2y = points[p[1]][1],
            p3x = points[p[2]][0], p3y = points[p[2]][1],
            p4x = points[p[3]][0], p4y = points[p[3]][1];

        if (canFormTriagle(p1x, p1y, p2x, p2y, p3x, p3y, area)) {
            if (canFormTriagle(p2x, p2y, p3x, p3y, p4x, p4y, area)) {
                long d12 = pow(p1x - p2x, 2) + pow(p1y - p2y, 2);
                long d34 = pow(p3x - p4x, 2) + pow(p3y - p4y, 2);

                if (d12 == d34) {
                    /*
                    if (ans) {
                        cout << p1x << ", " << p1y << std::endl;
                        cout << p2x << ", " << p2y << std::endl;
                        cout << p3x << ", " << p3y << std::endl;
                        cout << p4x << ", " << p4y << std::endl;
                        cout << "----------------" << area << endl;
                    }
                    */
                    return true;
                }
            }
        }

        return false;
    }

    bool canFormTriagle(int& p1x, int& p1y, int& p2x, int& p2y, int& p3x, int& p3y, double& area)
    {
        long d12 = pow(p1x - p2x, 2) + pow(p1y - p2y, 2);
        long d23 = pow(p2x - p3x, 2) + pow(p2y - p3y, 2);
        long d13 = pow(p1x - p3x, 2) + pow(p1y - p3y, 2);

        bool ans = false;
        if ((d12 + d23) == d13) {
            area = sqrt(d12) * sqrt(d23);
            swap(p1x, p2x);
            swap(p1y, p2y);
            ans = true;
        }
        else if ((d12 + d13) == d23) {
            area = sqrt(d12) * sqrt(d13);
            ans = true;
        }
        else if ((d13 + d23) == d12) {
            area = sqrt(d13) * sqrt(d23);
            swap(p1x, p3x);
            swap(p1y, p3y);
            ans = true;
        }

        return ans;
    }

private:
    double ans;
    vector<int> rectPoints;
};
