/**
 * Minimum Length of Anagram Concatenation
 *
 * You are given a string s, which is known to be a concatenation of anagrams of some string t.
 *
 * Return the minimum possible length of the string t.
 *
 * An anagram is formed by rearranging the letters of a string. For example, "aab", "aba", and,
 * "baa" are anagrams of "aab".
 *
 * Example 1:
 *
 * Input: s = "abba"
 *
 * Output: 2
 *
 * Explanation:
 *
 * One possible string t could be "ba".
 *
 * Example 2:
 *
 * Input: s = "cdef"
 *
 * Output: 4
 *
 * Explanation:
 *
 * One possible string t could be "cdef", notice that t can be equal to s.
 *
 * Constraints:
 *     1 <= s.length <= 105
 *     s consist only of lowercase English letters.
 */
/**
 * Original solution.
 */
auto count_char_first(char const* s, int len)
{
    auto cnt_ch = array<int, 26>{ 0, };

    for (auto i = 0; i < len; ++i) {
        ++cnt_ch[*(s + i) - 'a'];
    }

    return cnt_ch;
}

class Solution {
    bool ok(string const& s, int len) {
        int const len_s = s.length();
        auto const expected = count_char_first(s.data(), len);

        for (auto i = len; i < len_s; i += len) {
            auto cnt_ch = count_char_first(s.data() + i, len);
            if (cnt_ch != expected) {
                return false;
            }
        }

        return true;
    }

public:
    int minAnagramLength(string s) {
        int const len_s = s.size();
        for (auto i = 1; i <= len_s; ++i) {
            if (0 == (len_s % i)) {
                if (ok(s, i)) {
                    return i;
                }
            }
        }

        return len_s;
    }
};
