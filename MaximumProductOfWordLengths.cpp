/**
 * Maximum Product of Word Lengths
 *
 * Given a string array words, find the maximum value of length(word[i]) * length(word[j]) where the two words do not
 * share common letters. You may assume that each word will contain only lower case letters. If no such two words exist,
 * return 0.
 *
 * Example 1:
 *
 * Input: ["abcw","baz","foo","bar","xtfn","abcdef"]
 * Output: 16
 * Explanation: The two words can be "abcw", "xtfn".
 *
 * Example 2:
 *
 * Input: ["a","ab","abc","d","cd","bcd","abcd"]
 * Output: 4
 * Explanation: The two words can be "ab", "cd".
 *
 * Example 3:
 *
 * Input: ["a","aa","aaa","aaaa"]
 * Output: 0
 * Explanation: No such pair of words.
 */
/**
 * Further improvement.
 */
class Solution {
public:
    int maxProduct(vector<string>& words) {
        unordered_map<int,int> maxlen;
        int ans = 0;
        for (string word : words) {
            int mask = 0;
            for (char c : word) mask |= 1 << (c - 'a');

            maxlen[mask] = max(maxlen[mask], (int) word.size());

            for (auto maskAndLen : maxlen) {
                if (!(mask & maskAndLen.first)) {
                    int prod = word.length() * maskAndLen.second;
                    ans = max(ans, prod);
                }
            }
        }

        return ans;
    }
};

/**
 * Improved version.
 */
class Solution {
public:
    int maxProduct(vector<string>& words) {
        vector<int> mask(words.size());
        int ans = 0;

        for (int i=0; i<words.size(); ++i) {
            for (char c : words[i]) mask[i] |= 1 << (c - 'a');
            for (int j=0; j < i; ++j) {
                if (!(mask[i] & mask[j])) {
                    int prod = words[i].length() * words[j].length();
                    ans = max(ans, prod);
                }
            }
        }

        return ans;
    }
};

/**
 * Original solution. Brute force.
 */
class Solution {
public:
    int maxProduct(vector<string>& words) {
        int N = words.size();
        int ans = 0;

        for (int i = 0; i < N; ++i) {
            for (int j = i + 1; j < N; ++j) {
                if (false == haveCommonLetters(words[i], words[j])) {
                    int prod = words[i].length() * words[j].length();
                    ans = std::max(ans, prod);
                }
            }
        }

        return ans;
    }

private:
    bool haveCommonLetters(const string& s1, const string& s2)
    {
        vector<int> letters(26, 0);
        for (auto c : s1) ++letters[c - 'a'];
        for (auto c : s2) {
            if (letters[c - 'a'] > 0) return true;
        }

        return false;
    }
};
