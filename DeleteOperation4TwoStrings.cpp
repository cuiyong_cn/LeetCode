/**
 * Delete Operation for Two Strings
 *
 * Given two words word1 and word2, find the minimum number of steps required to make word1 and word2 the same, where in
 * each step you can delete one character in either string.
 *
 * Example 1:
 *
 * Input: "sea", "eat"
 * Output: 2
 * Explanation: You need one step to make "sea" to "ea" and another step to make "eat" to "ea".
 *
 * Note:
 *     The length of given words won't exceed 500.
 *     Characters in given words can only be lower-case letters.
 */
/**
 * Improved one.
 */
class Solution {
public:
    int minDistance(string word1, string word2) {
        vector<vector<int>> dp(word1.size() + 1, vector<int>(word2.size() + 1, 0));

        for (size_t i = 0; i <= word1.size(); ++i) {
            for (size_t j = 0; j <= word2.size(); ++j) {
                if (0 == i || 0 == j) {
                    dp[i][j] = i + j;
                }
                else if (word1[i - 1] == word2[j - 1]) {
                    dp[i][j] = dp[i - 1][j - 1];
                }
                else {
                    dp[i][j] = 1 + std::min(dp[i - 1][j], dp[i][j - 1]);
                }
            }
        }

        return dp[word1.size()][word2.size()];
    }
};

/**
 * Original solution. DP one.
 */
class Solution {
public:
    int minDistance(string word1, string word2) {
        if (word1.empty()) return word2.size();
        if (word2.empty()) return word1.size();

        // strange: at first, I use vector of vector, but when I replace with
        // array one. The time increase?? I thought array usually allocate
        // on the stack?!
        //
        // Finally I got this: if sum length of word1 and word2 is small.
        // allocate is faster than allocate 500 * 500 bytes on stack
        std::array<std::array<int, 500>, 500> dp{0};
        dp[0][0] = word1[0] == word2[0] ? 0 : 2;

        for (size_t i = 0; i < word1.size(); ++i) {
            for (size_t j = 0; j < word2.size(); ++j) {
                if (word1[i] == word2[j]) {
                    if (i > 0 && j > 0) {
                        dp[i][j] = dp[i - 1][j - 1];
                    }
                    else if (i > 0) {
                        dp[i][j] = i;
                    }
                    else if (j > 0) {
                        dp[i][j] = j;
                    }
                }
                else {
                    int d1 = 1001, d2 = 1001, d3 = 1001;
                    if (i > 0 && j > 0) {
                        d1 = std::min(d1, 2 + dp[i - 1][j - 1]);
                    }
                    if (j > 0) {
                        d2 = std::min(d2, 1 + dp[i][j - 1]);
                    }
                    if (i > 0) {
                        d3 = std::min(d3, 1 + dp[i - 1][j]);
                    }

                    if (i > 0 || j > 0) {
                        dp[i][j] = std::min(d1, std::min(d2, d3));
                    }
                }
            }
        }

        return dp[word1.size() - 1][word2.size() - 1];
    }
};
