/**
 * Find Pivot Index
 *
 * Given an array of integers nums, write a method that returns the "pivot" index of this array.
 *
 * We define the pivot index as the index where the sum of all the numbers to the left of the index is equal to the sum
 * of all the numbers to the right of the index.
 *
 * If no such index exists, we should return -1. If there are multiple pivot indexes, you should return the left-most
 * pivot index.
 *
 *  Example 1:
 *
 *  Input: nums = [1,7,3,6,5,6] Output: 3 Explanation: The sum of the numbers to the left of index 3 (nums[3] = 6) is
 *  equal to the sum of numbers to the right of index 3.  Also, 3 is the first index where this occurs.  Example 2:
 *
 *  Input: nums = [1,2,3] Output: -1 Explanation: There is no index that satisfies the conditions in the problem
 *  statement.
 *
 *   Constraints:
 *   The length of nums will be in the range [0, 10000].  Each element nums[i] will be an integer in the range [-1000,
 *   1000].
 */
/**
 * Same idea with out using the addition space.
 */
class Solution {
public:
    int pivotIndex(vector<int>& nums) {
        int total = accumulate(nums.begin(), nums.end(), 0);
        int sum = 0;
        for (int i = 0; i < nums.size(); sum += nums[i++]) {
            if ((sum * 2) == (total - nums[i])) {
                return i;
            }
        }

        return -1;
    }
};
/**
 * original solution using pre sum.
 */
class Solution {
public:
    int pivotIndex(vector<int>& nums) {
        if (nums.empty()) {
            return -1;
        }

        auto pre_sum = nums;
        partial_sum(pre_sum.begin(), pre_sum.end(), pre_sum.begin());
        if (0 == (pre_sum.back() - pre_sum[0])) {
            return 0;
        }

        for (int i = 1; i < pre_sum.size(); ++i) {
            int sum_left = pre_sum[i - 1];
            int sum_right = pre_sum.back() - pre_sum[i];
            if (sum_left == sum_right) {
                return i;
            }
        }

        return -1;
    }
};
