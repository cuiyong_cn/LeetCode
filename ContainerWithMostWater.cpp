/**
 * Container With Most Water
 *
 * Given n non-negative integers a1, a2, ..., an , where each represents a point at coordinate (i, ai). n vertical lines
 * are drawn such that the two endpoints of line i is at (i, ai) and (i, 0). Find two lines, which together with x-axis
 * forms a container, such that the container contains the most water.
 *
 * Note: You may not slant the container and n is at least 2.
 *
 * The above vertical lines are represented by array [1,8,6,2,5,4,8,3,7]. In this case, the max area of water (blue
 * section) the container can contain is 49.
 *
 * Example:
 *
 * Input: [1,8,6,2,5,4,8,3,7]
 * Output: 49
 */
/**
 * Improve the below solution. Solution from leetcode, from discussion I think this approach is not correct.
 */
class Solution {
public:
    int maxArea(vector<int>& height) {
        int width = height.size() - 1;
        int area = 0;
        int l = 0, r = width;
        int lval = height[l], rval = height[r];

        while (l < r) {
            area = max(area, width * min(lval, rval));
            --width;
            if (lval < rval) {
                ++l;
                lval = height[l];
            }
            else {
                --r;
                rval = height[r];
            }
        }

        return area;
    }
};

/**
 * Original solution. Brute force. slide every width window to find out the max one. Again the fate of brute force - TLE
 */
class Solution {
public:
    int maxArea(vector<int>& height) {
        int area = 0;
        int maxWidth = height.size();
        for (int w = 1; w < maxWidth; ++w) {
            for (int i = 0; i < (maxWidth - w); ++i) {
                area = max(area, w * min(height[i], height[i + w]));
            }
        }

        return area;
    }
};
