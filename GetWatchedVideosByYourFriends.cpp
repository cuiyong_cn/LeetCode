/**
 * Get Watched Videos by Your Friends
 *
 * There are n people, each person has a unique id between 0 and n-1. Given the
 * arrays watchedVideos and friends, where watchedVideos[i] and friends[i]
 * contain the list of watched videos and the list of friends respectively for
 * the person with id = i.
 *
 * Level 1 of videos are all watched videos by your friends, level 2 of videos
 * are all watched videos by the friends of your friends and so on. In general,
 * the level k of videos are all watched videos by people with the shortest path
 * exactly equal to k with you. Given your id and the level of videos, return
 * the list of videos ordered by their frequencies (increasing). For videos with
 * the same frequency order them alphabetically from least to greatest.
 *
 * Example 1:
 *
 * Input: watchedVideos = [["A","B"],["C"],["B","C"],["D"]],
 *              friends = [[1,2],[0,3],[0,3],[1,2]], id = 0, level = 1
 * Output: ["B","C"]
 *
 * Explanation:
 * You have id = 0 (green color in the figure) and your friends are (yellow
 * color in the figure):
 * Person with id = 1 -> watchedVideos = ["C"]
 * Person with id = 2 -> watchedVideos = ["B","C"]
 * The frequencies of watchedVideos by your friends are:
 * B -> 1
 * C -> 2
 *
 * Example 2:
 *
 * Input: watchedVideos = [["A","B"],["C"],["B","C"],["D"]],
 *              friends = [[1,2],[0,3],[0,3],[1,2]], id = 0, level = 2
 * Output: ["D"]
 *
 * Explanation:
 * You have id = 0 (green color in the figure) and the only friend of your
 * friends is the person with id = 3 (yellow color in the figure).
 *
 * Constraints:
 *
 * n == watchedVideos.length == friends.length
 * 2 <= n <= 100
 * 1 <= watchedVideos[i].length <= 100
 * 1 <= watchedVideos[i][j].length <= 8
 * 0 <= friends[i].length < n
 * 0 <= friends[i][j] < n
 * 0 <= id < n
 * 1 <= level < n
 * if friends[i] contains j, then friends[j] contains i
 */
/**
 * Improve the below code.
 */
class Solution {
public:
    vector<string> watchedVideosByFriends(vector<vector<string>>& watchedVideos, vector<vector<int>>& friends, int id, int level) {
        vector<bool> visited(friends.size(), false);
        std::vector<int> level_friends = {id};

        visited[id] = true;

        for (int lv = 1; lv <= level; ++lv) {
            std::vector<int> next_level_friends;
            for (auto fid : level_friends) {
                for (auto f : friends[fid]) {
                    if (!visited[f]) {
                        visited[f] = true;
                        next_level_friends.emplace_back(f);
                    }
                }
            }

            swap(level_friends, next_level_friends);
        }

        std::unordered_map<std::string, int> videos_wathched_by_friends;
        for (auto const fid : level_friends) {
            for (auto const& m : watchedVideos[fid]) {
                ++videos_wathched_by_friends[m];
            }
        }

        std::vector<std::pair<std::string, int>> vwbf(videos_wathched_by_friends.begin(), videos_wathched_by_friends.end());
        sort(vwbf.begin(), vwbf.end(),
             [](auto a, auto b) {
                 if (a.second < b.second) {
                     return true;
                 }
                 if (a.second > b.second) {
                     return false;
                 }

                 return a.first < b.first;
             });

        vector<std::string> ans;
        for (auto const& v : vwbf) {
            ans.emplace_back(v.first);
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<string> watchedVideosByFriends(vector<vector<string>>& watchedVideos, vector<vector<int>>& friends, int id, int level) {
        vector<bool> visited(friends.size(), false);
        std::unordered_map<int, int> level_friends;

        visited[id] = true;
        for (auto f : friends[id]) {
            level_friends[f] = 1;
            visited[f] = true;
        }

        int lvl = 2;
        while (lvl++ <= level) {
            std::unordered_map<int, int> next_level_friends;
            for (auto lf : level_friends) {
                auto [fid, lv] = lf;

                for (auto f : friends[fid]) {
                    if (!visited[f]) {
                        visited[f] = true;
                        next_level_friends[f] = lv + 1;
                    }
                }
            }

            swap(level_friends, next_level_friends);
        }

        std::unordered_map<std::string, int> videos_wathched_by_friends;
        for (auto const lf : level_friends) {
            for (auto const& m : watchedVideos[lf.first]) {
                ++videos_wathched_by_friends[m];
            }
        }

        std::vector<std::pair<std::string, int>> vwbf(
                videos_wathched_by_friends.begin(),
                videos_wathched_by_friends.end());
        sort(vwbf.begin(), vwbf.end(),
             [](auto a, auto b) {
                 if (a.second < b.second) {
                     return true;
                 }
                 if (a.second > b.second) {
                     return false;
                 }

                 return a.first < b.first;
             });

        vector<std::string> ans;
        for (auto const& v : vwbf) {
            ans.emplace_back(v.first);
        }

        return ans;
    }
};
