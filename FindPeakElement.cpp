/**
 * Find Peak Element
 *
 * A peak element is an element that is strictly greater than its neighbors.
 *
 * Given an integer array nums, find a peak element, and return its index. If the array contains
 * multiple peaks, return the index to any of the peaks.
 *
 * You may imagine that nums[-1] = nums[n] = -∞.
 *
 * Example 1:
 *
 * Input: nums = [1,2,3,1]
 * Output: 2
 * Explanation: 3 is a peak element and your function should return the index number 2.
 * Example 2:
 *
 * Input: nums = [1,2,1,3,5,6,4]
 * Output: 5
 * Explanation: Your function can return either index number 1 where the peak element is 2, or index
 * number 5 where the peak element is 6.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 1000
 * -231 <= nums[i] <= 231 - 1
 * nums[i] != nums[i + 1] for all valid i.
 */
/**
 * Binary search, iterative version.
 */
class Solution {
public:
    int findPeakElement(vector<int>& nums) {
        int left = 0, right = nums.size() - 1;
        while (left < right) {
            auto mid = (left + right) >> 1;
            if (nums[mid] > nums[mid + 1]) {
                right = mid;
            }
            else {
                left = mid + 1;
            }
        }

        return left;
    }
};

/**
 * From leetcode. Using binary search.
 */
class Solution {
public:
    int findPeakElement(vector<int>& nums) {
        return dfs(nums, 0, nums.size() - 1);
    }

private:
    int dfs(vector<int> const& nums, int left, int right)
    {
        if (left == right) {
            return left;
        }

        auto mid = (left + right) >> 1;
        if (nums[mid] > nums[mid + 1]) {
            return dfs(nums, left, mid);
        }

        return dfs(nums, mid + 1, right);
    }
};

/**
 * It turns out, lots of code is redundant.
 */
class Solution {
public:
    int findPeakElement(vector<int>& nums) {
        int end = nums.size() - 1;
        for (int i = 0; i < end; ++i) {
            if (nums[i] > nums[i + 1]) {
                return i;
            }
        }

        return end;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int findPeakElement(vector<int>& nums) {
        if (1 == nums.size()) {
            return 0;
        }

        int left = std::numeric_limits<int>::min();
        int end = nums.size() - 1;
        for (int i = 0; i < end; ++i) {
            if (left < nums[i] && nums[i] > nums[i + 1]) {
                return i;
            }

            left = nums[i];
        }

        return end;
    }
};
