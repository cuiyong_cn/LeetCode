/** Flatten Nested List Iterator
 *
 * Given a nested list of integers, implement an iterator to flatten it.
 *
 * Each element is either an integer, or a list -- whose elements may also be integers or other lists.
 *
 * Example 1:
 *
 * Input: [[1,1],2,[1,1]] Output: [1,1,2,1,1] Explanation: By calling next repeatedly until hasNext returns false, the
 * order of elements returned by next should be: [1,1,2,1,1].
 *
 * Example 2:
 *
 * Input: [1,[4,[6]]] Output: [1,4,6] Explanation: By calling next repeatedly until hasNext returns false, the order of
 * elements returned by next should be: [1,4,6].
 */
/**
 * original solution
 */
/**
 * // This is the interface that allows for creating nested lists.
 * // You should not implement it, or speculate about its implementation
 * class NestedInteger {
 *   public:
 *     // Return true if this NestedInteger holds a single integer, rather than a nested list.
 *     bool isInteger() const;
 *
 *     // Return the single integer that this NestedInteger holds, if it holds a single integer
 *     // The result is undefined if this NestedInteger holds a nested list
 *     int getInteger() const;
 *
 *     // Return the nested list that this NestedInteger holds, if it holds a nested list
 *     // The result is undefined if this NestedInteger holds a single integer
 *     const vector<NestedInteger> &getList() const;
 * };
 */
class NestedIterator {
public:
    typedef vector<NestedInteger>::const_iterator citer;

    NestedIterator(vector<NestedInteger> &nestedList) :
        cur{nestedList.begin()}, end{nestedList.end()} {

    }

    int next() {
        int ans = cur->getInteger();
        ++cur;
        return ans;
    }

    bool hasNext() {
        int ret = true;
        while (1) {
            if (cur != end) {
                if (cur->isInteger()) {
                    break;
                }
                else {
                    auto& newList = cur->getList();
                    ++cur;
                    stk.push({cur, end});
                    cur = newList.begin();
                    end = newList.end();
                }
            }
            else {
                if (false == stk.empty()) {
                    cur = stk.top().first;
                    end = stk.top().second;
                    stk.pop();
                }
                else {
                    ret = false;
                    break;
                }
            }
        }

        return ret;
    }
private:
    stack<pair<citer, citer>> stk;
    citer cur;
    citer end;
};

/**
 * Your NestedIterator object will be instantiated and called as such:
 * NestedIterator i(nestedList);
 * while (i.hasNext()) cout << i.next();
 */
