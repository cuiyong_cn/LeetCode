/**
 * Maximum Sum of Two Non-Overlapping Subarrays
 *
 * Given an array A of non-negative integers, return the maximum sum of elements in two non-overlapping (contiguous)
 * subarrays, which have lengths L and M.  (For clarification, the L-length subarray could occur before or after the
 * M-length subarray.)
 *
 * Formally, return the largest V for which V = (A[i] + A[i+1] + ... + A[i+L-1]) + (A[j] + A[j+1] + ... + A[j+M-1]) and either:
 *
 *     0 <= i < i + L - 1 < j < j + M - 1 < A.length, or
 *     0 <= j < j + M - 1 < i < i + L - 1 < A.length.
 *
 * Example 1:
 *
 * Input: A = [0,6,5,2,2,5,1,9,4], L = 1, M = 2
 * Output: 20
 * Explanation: One choice of subarrays is [9] with length 1, and [6,5] with length 2.
 *
 * Example 2:
 *
 * Input: A = [3,8,1,3,2,1,8,9,0], L = 3, M = 2
 * Output: 29
 * Explanation: One choice of subarrays is [3,8,1] with length 3, and [8,9] with length 2.
 *
 * Example 3:
 *
 * Input: A = [2,1,5,6,0,9,5,0,3,8], L = 4, M = 3
 * Output: 31
 * Explanation: One choice of subarrays is [5,6,0,9] with length 4, and [3,8] with length 3.
 *
 * Note:
 *
 *     L >= 1
 *     M >= 1
 *     L + M <= A.length <= 1000
 *     0 <= A[i] <= 1000
 */
/**
 * Improved one.
 *      Lmax: max sum of continuous L elements before the last M elements.
 *      Mmax: max sum of continuous M elements before the last L elements.
 */
class Solution {
public:
    int maxSumTwoNoOverlap(vector<int>& A, int L, int M) {
        for (int i = 1; i < A.size(); ++i) {
            A[i] += A[i - 1];
        }

        int res = A[L + M - 1], Lmax = A[L - 1], Mmax = A[M - 1];
        for (int i = L + M; i < A.size(); ++i) {
            Lmax = max(Lmax, A[i - M] - A[i - L - M]);
            Mmax = max(Mmax, A[i - L] - A[i - L - M]);
            res = max(res, max(Lmax + A[i] - A[i - M], Mmax + A[i] - A[i - L]));
        }
        return res;
    }
};

/**
 * Original solution. Walk through all the possible combination.
 *      We use prefix sum to make calculate the subarray fast.
 *
 * But this solution has a major drawback:
 *      It does a lot of repeated useless work.
 */
class Solution {
public:
    int maxSumTwoNoOverlap(vector<int>& A, int L, int M) {
        for (int i = 1; i < A.size(); ++i) {
            A[i] += A[i - 1];
        }
        int vecLen = A.size();
        int maxSum = numeric_limits<int>::min();
        for (int i = 0; i < (vecLen - L + 1); ++i) {
            int lSum = A[i + L - 1] - (i > 0 ? A[i - 1] : 0);
            /** M length subarray before L length subarray */
            if (i >= M) {
                /** 
                 * here is the weak point. Every time we recalculate the a lot of sum of L length subarray.
                 * We should find a way to memorize the maximum sum of L length subarray on the left of i
                 **/
                for (int j = 0; j < (i - M + 1); ++j) {
                    int rSum = A[j + M - 1] - (j > 0 ? A[j - 1] : 0);
                    maxSum = max(maxSum, lSum + rSum);
                }
            }
            /** L length subarray before M length subarray */
            if ((vecLen - i - L) > M) {
                /** refer to the above comment */
                for (int j = i + L; j < (vecLen - M + 1); ++j) {
                    int rSum = A[j + M - 1] - A[j - 1];
                    maxSum = max(maxSum, lSum + rSum);
                }
            }
        }
        return maxSum;
    }
};

/**
 * Another intereting solution from leetcode user votrubac.
 *      Note: max(res, left[i] + right[i])
 *
 *      The var i in above statement is treated as a split point.
 *      left[i] means "max sum of L length subarray on the left of i"
 *      right[i] means "max sum of M length subarray on the right of i"
 */
class Solution
{
public:
    int maxTwoNoOverlap(vector<int>& A, int L, int M, int sz, int res = 0) {
        vector<int> left(sz + 1), right(sz + 1);
        for (int i = 0, j = sz - 1, s_r = 0, s_l = 0; i < sz; ++i, --j) {
            s_l += A[i], s_r += A[j];
            left[i + 1] = max(left[i], s_l);
            right[j] = max(right[j + 1], s_r);
            if (i + 1 >= L) s_l -= A[i + 1 - L];
            if (i + 1 >= M) s_r -= A[j + M - 1];
        }

        for (auto i = 0; i < A.size(); ++i) {
            res = max(res, left[i] + right[i]);
        }
        return res;
    }

    int maxSumTwoNoOverlap(vector<int>& A, int L, int M) {
        return max(maxTwoNoOverlap(A, L, M, A.size()), maxTwoNoOverlap(A, M, L, A.size()));
    }
};
