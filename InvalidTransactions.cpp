/**
 * Invalid Transactions
 *
 * A transaction is possibly invalid if:
 *
 * the amount exceeds $1000, or;
 * if it occurs within (and including) 60 minutes of another transaction with the same name in a different city.
 * You are given an array of strings transaction where transactions[i] consists of comma-separated
 * values representing the name, time (in minutes), amount, and city of the transaction.
 *
 * Return a list of transactions that are possibly invalid. You may return the answer in any order.
 *
 * Example 1:
 *
 * Input: transactions = ["alice,20,800,mtv","alice,50,100,beijing"]
 * Output: ["alice,20,800,mtv","alice,50,100,beijing"]
 * Explanation: The first transaction is invalid because the second transaction occurs within a
 * difference of 60 minutes, have the same name and is in a different city. Similarly the second one
 * is invalid too.
 * Example 2:
 *
 * Input: transactions = ["alice,20,800,mtv","alice,50,1200,mtv"]
 * Output: ["alice,50,1200,mtv"]
 * Example 3:
 *
 * Input: transactions = ["alice,20,800,mtv","bob,50,1200,mtv"]
 * Output: ["bob,50,1200,mtv"]
 *
 * Constraints:
 *
 * transactions.length <= 1000
 * Each transactions[i] takes the form "{name},{time},{amount},{city}"
 * Each {name} and {city} consist of lowercase English letters, and have lengths between 1 and 10.
 * Each {time} consist of digits, and represent an integer between 0 and 1000.
 * Each {amount} consist of digits, and represent an integer between 0 and 2000.
 */
/**
 * Original solution. Optimization ideas:
 *
 * 1. we can group the same person's transaction together
 * 2. sorted by time
 * 3. can at most determine two transactions at a time
 */
class Solution {
    struct Transaction {
        string name;
        int time;
        int amount;
        string city;
    };

public:
    vector<string> invalidTransactions(vector<string>& transactions) {
        vector<string> ans;
        vector<Transaction> tr;

        for (auto const& t : transactions) {
            auto p1 = t.find(',');
            auto p2 = t.find(',', p1 + 1);
            auto p3 = t.find(',', p2 + 1);
            tr.push_back({t.substr(0, p1),
                            stoi(t.substr(p1 + 1, p2 - p1 - 1)),
                            stoi(t.substr(p2 + 1, p3 - p2 - 1)),
                            t.substr(p3 + 1)});
        }

        for (int i = 0; i < tr.size(); ++i) {
            auto valid = true;
            if (tr[i].amount > 1000) {
                valid = false;
            }
            else {
                for (int j = 0; j < tr.size(); ++j) {
                    if (i == j) {
                        continue;
                    }

                    if (within_60_min(tr[i].time, tr[j].time) && tr[i].name == tr[j].name && tr[i].city != tr[j].city) {
                        valid = false;
                        break;
                    }
                }
            }

            if (!valid) {
                ans.emplace_back(transactions[i]);
            }
        }

        return ans;
    }

private:
    bool within_60_min(int t1, int t2)
    {
        return (t1 <= t2 && t2 <= (t1 + 60)) || (t2 <= t1 && t1 <= (t2 + 60));
    }
};
