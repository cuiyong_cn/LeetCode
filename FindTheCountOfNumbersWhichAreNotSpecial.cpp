/**
 * Find the Count of Numbers Which Are Not Special
 *
 * You are given 2 positive integers l and r. For any number x, all positive divisors of x except x
 * are called the proper divisors of x.
 *
 * A number is called special if it has exactly 2 proper divisors. For example:
 *     The number 4 is special because it has proper divisors 1 and 2.
 *     The number 6 is not special because it has proper divisors 1, 2, and 3.
 *
 * Return the count of numbers in the range [l, r] that are not special.
 *
 * Example 1:
 *
 * Input: l = 5, r = 7
 *
 * Output: 3
 *
 * Explanation:
 *
 * There are no special numbers in the range [5, 7].
 *
 * Example 2:
 *
 * Input: l = 4, r = 16
 *
 * Output: 11
 *
 * Explanation:
 *
 * The special numbers in the range [4, 16] are 4 and 9.
 *
 * Constraints:
 *     1 <= l <= r <= 109
 */
/**
 * Since the size of the prime is less, we check if the square of the prime lies within the range [l, r]
 */
auto const& get_primes()
{
    static unordered_set<int> s_primes;

    if (s_primes.empty()) {
        int const end = sqrt(1'000'000'000);
        auto is_prime = vector<bool>(end, true);

        for (auto i = 2; i <= end; ++i) {
            if (is_prime[i]) {
                for (auto j = i * i; j <= end; j += i) {
                    is_prime[j] = false;
                }
            }
        }

        for (auto i = 2; i <= end; ++i) {
            if (is_prime[i]) {
                s_primes.insert(i);
            }
        }
    }

    return s_primes;
}

class Solution {
public:
    int nonSpecialCount(int l, int r) {
        auto specials = 0;
        auto const& primes = get_primes();

        for (auto p : primes) {
            auto sq = p * p;

            if (l <= sq && sq <= r) {
                ++specials;
            }
        }

        return (r - l + 1) - specials;
    }
};

/**
 * Original soltuion. TLE.
 */
auto const& get_primes()
{
    static unordered_set<int> s_primes;

    if (s_primes.empty()) {
        int const end = sqrt(1'000'000'000);
        auto is_prime = vector<bool>(end, true);

        for (auto i = 2; i <= end; ++i) {
            if (is_prime[i]) {
                for (auto j = i * i; j <= end; j += i) {
                    is_prime[j] = false;
                }
            }
        }

        for (auto i = 2; i <= end; ++i) {
            if (is_prime[i]) {
                s_primes.insert(i);
            }
        }
    }

    return s_primes;
}

class Solution {
public:
    int nonSpecialCount(int l, int r) {
        auto specials = 0;
        auto const& primes = get_primes();

        for (auto i = l; i <= r; ++i) {
            int const st = sqrt(i);
            if (primes.count(st) && ((st * st) == i)) {
                ++specials;
            }
        }

        return (r - l + 1) - specials;
    }
};
