/**
 * Short Encoding of Words
 *
 * Given a list of words, we may encode it by writing a reference string S and a list of indexes A.
 *
 * For example, if the list of words is ["time", "me", "bell"], we can write it as S = "time#bell#" and indexes = [0, 2,
 * 5].  Then for each index, we will recover the word by reading from the reference string from that index until we
 * reach a "#" character.
 *
 * What is the length of the shortest reference string S possible that encodes the given words?
 *
 * Example:
 *
 * Input: words = ["time", "me", "bell"]
 * Output: 10
 * Explanation: S = "time#bell#" and indexes = [0, 2, 5].
 *
 * Note:
 *     1 <= words.length <= 2000.
 *     1 <= words[i].length <= 7.
 *     Each word has only lowercase letters.
 */
/**
 * Improve the below solution.
 * We can solve this using Trie structure. See the last one.
 */
class Solution {
public:
    int minimumLengthEncoding(vector<string>& words) {
        unordered_set<string> encSet(words.begin(), words.end());

        for (auto& w : words) {
            for (size_t i = 1; i < w.length(); ++i) {
                encSet.erase(w.substr(i));
            }
        }

        int ans = 0;
        for (auto& w : encSet) {
            ans += w.length() + 1;
        }

        return ans;
    }
};

/**
 * Original solution. The idea is:
 *  1. Having two words a and b, the longer words might include the shorter one.
 *
 * So we sort the words by words's length and build a map using all the possible subsequence.
 */
class Solution {
public:
    int minimumLengthEncoding(vector<string>& words) {
        sort(words.begin(), words.end(), [](auto& w1, auto& w2) {
            return w1.length() > w2.length();
        });

        unordered_map<string, int> encMap;

        for (auto& w : words) {
            auto it = encMap.find(w);
            if (it == encMap.end()) {
                encMap[w] = 1;
                for (size_t i = 1; i < w.length(); ++i) {
                    encMap[w.substr(i)] = 0;
                }
            }
        }

        int ans = 0;
        for (auto& it : encMap) {
            if (1 == it.second) ans += it.first.length() + 1;
        }

        return ans;
    }
};

/**
 * Trie approach.
 */
class Solution {
    public int minimumLengthEncoding(String[] words) {
        TrieNode trie = new TrieNode();
        Map<TrieNode, Integer> nodes = new HashMap();

        for (int i = 0; i < words.length; ++i) {
            String word = words[i];
            TrieNode cur = trie;
            for (int j = word.length() - 1; j >= 0; --j)
                cur = cur.get(word.charAt(j));
            nodes.put(cur, i);
        }

        int ans = 0;
        for (TrieNode node: nodes.keySet()) {
            if (node.count == 0)
                ans += words[nodes.get(node)].length() + 1;
        }
        return ans;

    }
}

class TrieNode {
    TrieNode[] children;
    int count;
    TrieNode() {
        children = new TrieNode[26];
        count = 0;
    }
    public TrieNode get(char c) {
        if (children[c-'a'] == null) {
            children[c-'a'] = new TrieNode();
            count++;
        }
        return children[c - 'a'];
    }
}
