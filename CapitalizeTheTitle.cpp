/**
 * Capitalize the Title
 *
 * You are given a string title consisting of one or more words separated by a single space, where
 * each word consists of English letters. Capitalize the string by changing the capitalization of
 * each word such that:
 *
 * If the length of the word is 1 or 2 letters, change all letters to lowercase.
 * Otherwise, change the first letter to uppercase and the remaining letters to lowercase.
 * Return the capitalized title.
 *
 * Example 1:
 *
 * Input: title = "capiTalIze tHe titLe"
 * Output: "Capitalize The Title"
 * Explanation:
 * Since all the words have a length of at least 3, the first letter of each word is uppercase, and the remaining letters are lowercase.
 * Example 2:
 *
 * Input: title = "First leTTeR of EACH Word"
 * Output: "First Letter of Each Word"
 * Explanation:
 * The word "of" has length 2, so it is all lowercase.
 * The remaining words have a length of at least 3, so the first letter of each remaining word is uppercase, and the remaining letters are lowercase.
 * Example 3:
 *
 * Input: title = "i lOve leetcode"
 * Output: "i Love Leetcode"
 * Explanation:
 * The word "i" has length 1, so it is lowercase.
 * The remaining words have a length of at least 3, so the first letter of each remaining word is uppercase, and the remaining letters are lowercase.
 *
 * Constraints:
 *
 * 1 <= title.length <= 100
 * title consists of words separated by a single space without any leading or trailing spaces.
 * Each word consists of uppercase and lowercase English letters and is non-empty.
 */
/**
 * Two pointer.
 */
class Solution {
public:
    string capitalizeTitle(string title) {
        for (auto i = 0*title.size(), j = i; i <= title.size(); ++i) {
            if (i == title.size() || ' ' == title[i]) {
                if ((i - j) > 2) {
                    title[j] = toupper(title[j]);
                }
                j = i + 1;
            }
            else {
                title[i] = tolower(title[i]);
            }
        }

        return title;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    string capitalizeTitle(string title) {
        auto b = title.find_first_not_of(' ');
        while (b < title.length()) {
            auto e = title.find_first_of(' ', b + 1);
            if (string::npos == e) {
                e = title.length();
            }

            auto wb = begin(title); advance(wb, b);
            auto we = begin(title); advance(we, e);
            transform(wb, we, wb, ::tolower);
            if ((e - b) > 2) {
                title[b] = toupper(title[b]);
            }
            b = e + 1;
        }

        return title;
    }
};
