/**
 * Delete Columns to Make Sorted II
 *
 * You are given an array of n strings strs, all of the same length.
 *
 * We may choose any deletion indices, and we delete all the characters in those indices for each string.
 *
 * For example, if we have strs = ["abcdef","uvwxyz"] and deletion indices {0, 2, 3}, then the final
 * array after deletions is ["bef", "vyz"].
 *
 * Suppose we chose a set of deletion indices answer such that after deletions, the final array has
 * its elements in lexicographic order (i.e., strs[0] <= strs[1] <= strs[2] <= ... <= strs[n - 1]).
 * Return the minimum possible value of answer.length.
 *
 * Example 1:
 *
 * Input: strs = ["ca","bb","ac"]
 * Output: 1
 * Explanation:
 * After deleting the first column, strs = ["a", "b", "c"].
 * Now strs is in lexicographic order (ie. strs[0] <= strs[1] <= strs[2]).
 * We require at least 1 deletion since initially strs was not in lexicographic order, so the answer is 1.
 * Example 2:
 *
 * Input: strs = ["xc","yb","za"]
 * Output: 0
 * Explanation:
 * strs is already in lexicographic order, so we do not need to delete anything.
 * Note that the rows of strs are not necessarily in lexicographic order:
 * i.e., it is NOT necessarily true that (strs[0][0] <= strs[0][1] <= ...)
 * Example 3:
 *
 * Input: strs = ["zyx","wvu","tsr"]
 * Output: 3
 * Explanation: We have to delete every column.
 *
 * Constraints:
 *
 * n == strs.length
 * 1 <= n <= 100
 * 1 <= strs[i].length <= 100
 * strs[i] consists of lowercase English letters.
 */
/**
 * using vector instead of unordered_set.
 * we can further optimize this solution using below idea:
 * As we check column by column, some words are definitely sorted, means any characters before
 * current one are definitely sorted, so if we check i, i + 1 word, and find the current column
 * character violate the rule, it does not matter cos it definitely sorted, we can quickly skip it.
 */
class Solution {
public:
    int minDeletionSize(vector<string>& strs) {
        int const N = strs.size();
        int const str_len = strs[0].length();
        vector<bool> indice_del(str_len, false);

        for (int i = 0; i < str_len; ++i) {
            int j = 1;
            for (; j < N; ++j) {
                if (strs[j - 1][i] > strs[j][i]) {
                    int k = i - 1;
                    for (; k >= 0; --k) {
                        if (indice_del[k]) {
                            continue;
                        }
                        if (strs[j - 1][k] < strs[j][k]) {
                            break;
                        }
                    }

                    if (-1 == k) {
                        indice_del[i] = true;
                        break;
                    }
                }
            }
        }

        return count(indice_del.begin(), indice_del.end(), true);
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int minDeletionSize(vector<string>& strs) {
        unordered_set<int> del_indices;
        int const N = strs.size();
        int const str_len = strs[0].length();
        for (int i = 0; i < str_len; ++i) {
            int j = 1;
            for (; j < N; ++j) {
                if (strs[j - 1][i] > strs[j][i]) {
                    int k = i - 1;
                    for (; k >= 0; --k) {
                        if (del_indices.count(k)) {
                            continue;
                        }
                        if (strs[j - 1][k] < strs[j][k]) {
                            break;
                        }
                    }

                    if (-1 == k) {
                        del_indices.insert(i);
                        break;
                    }
                }
            }
        }

        return del_indices.size();
    }
};
