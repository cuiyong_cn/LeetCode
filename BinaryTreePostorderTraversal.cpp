/**
 * Binary Tree Postorder Traversal
 *
 * Given the root of a binary tree, return the postorder traversal of its nodes' values.
 *
 * Example 1:
 *
 * Input: root = [1,null,2,3]
 * Output: [3,2,1]
 * Example 2:
 *
 * Input: root = []
 * Output: []
 * Example 3:
 *
 * Input: root = [1]
 * Output: [1]
 * Example 4:
 *
 * Input: root = [1,2]
 * Output: [2,1]
 * Example 5:
 *
 * Input: root = [1,null,2]
 * Output: [2,1]
 *
 * Constraints:
 *
 * The number of the nodes in the tree is in the range [0, 100].
 * -100 <= Node.val <= 100
 *
 * Follow up: Recursive solution is trivial, could you do it iteratively?
 */
/**
 * Another iterative solution.
 */
class Solution {
public:
    vector<int> postorderTraversal(TreeNode* root) {
        deque<int> ans;
        vector<TreeNode*> stk;

        while (root || !stk.empty()) {
            if (root) {
                stk.emplace_back(root);
                ans.push_front(root->val);
                root = root->right;
            }
            else {
                root = stk.back()->left; stk.pop_back();
            }
        }

        return {begin(ans), end(ans)};
    }
};

/**
 * Iterative version.
 */
class Solution {
public:
    vector<int> postorderTraversal(TreeNode* root) {
        vector<int> ans;
        vector<pair<TreeNode*, bool>> stk;

        while (root || !stk.empty()) {
            while (root) {
                stk.emplace_back(root, false);
                root = root->left;
            }

            auto [node, complete] = stk.back(); stk.pop_back();
            if (complete) {
                ans.emplace_back(node->val);
            }
            else {
                stk.emplace_back(node, true);
                root = node->right;
            }
        }

        return ans;
    }
};

/**
 * Origianal solution. Recursive solution.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> postorderTraversal(TreeNode* root) {
        vector<int> ans;
        post_order_travel(root, ans);
        return ans;
    }

private:
    void post_order_travel(TreeNode* root, vector<int>& values)
    {
        if (nullptr == root) {
            return;
        }

        post_order_travel(root->left, values);
        post_order_travel(root->right, values);
        values.emplace_back(root->val);
    }
};
