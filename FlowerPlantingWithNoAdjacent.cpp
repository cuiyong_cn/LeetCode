/**
 * Flower Planting With No Adjacent
 *
 * You have N gardens, labelled 1 to N.  In each garden, you want to plant one of 4 types of flowers.
 *
 * paths[i] = [x, y] describes the existence of a bidirectional path from garden x to garden y.
 *
 * Also, there is no garden that has more than 3 paths coming into or leaving it.
 *
 * Your task is to choose a flower type for each garden such that, for any two gardens connected by a path, they have
 * different types of flowers.
 *
 * Return any such a choice as an array answer, where answer[i] is the type of flower planted in the (i+1)-th garden.
 * The flower types are denoted 1, 2, 3, or 4.  It is guaranteed an answer exists.
 *
 * Example 1:
 *
 * Input: N = 3, paths = [[1,2],[2,3],[3,1]]
 * Output: [1,2,3]
 * Example 2:
 *
 * Input: N = 4, paths = [[1,2],[3,4]]
 * Output: [1,2,1,2]
 * Example 3:
 *
 * Input: N = 4, paths = [[1,2],[2,3],[3,4],[4,1],[1,3],[2,4]]
 * Output: [1,2,3,4]
 *
 * Note:
 *
 * 1 <= N <= 10000
 * 0 <= paths.size <= 20000
 * No garden has 4 or more paths coming into or leaving it.
 * It is guaranteed an answer exists.
 */
/**
 * Original solution.
 */
class Solution {
public:
    struct Garden
    {
        Garden(int fl = 0) : flower{fl} {}
        int flower;
        vector<Garden*> adjacents;
    };

public:
    vector<int> gardenNoAdj(int N, vector<vector<int>>& paths) {
        vector<Garden> gardens(N + 1);

        for (auto& path : paths) {
            gardens[path[0]].adjacents.push_back(&gardens[path[1]]);
            gardens[path[1]].adjacents.push_back(&gardens[path[0]]);
        }

        for (auto& g : gardens) {
            int candidates = getCandidates(g);
            int flower = 1;
            while ((1 << flower) & candidates) {
                ++flower;
            }

            g.flower = flower;
        }

        vector<int> ans(N);
        for (size_t i = 1; i < gardens.size(); ++i) {
            ans[i - 1] = gardens[i].flower;
        }

        return ans;
    }

private:
    int getCandidates(const Garden& g)
    {
        int mask = 0;
        for (auto ga : g.adjacents) {
            mask |= 1 << ga->flower;
        }

        return mask;
    }
};
