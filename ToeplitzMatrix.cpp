/**
 * Toeplitz Matrix
 *
 * A matrix is Toeplitz if every diagonal from top-left to bottom-right has the same element.
 *
 * Now given an M x N matrix, return True if and only if the matrix is Toeplitz.
 *
 * Example 1:
 *
 * Input:
 * matrix = [
 *   [1,2,3,4],
 *   [5,1,2,3],
 *   [9,5,1,2]
 * ]
 * Output: True
 * Explanation:
 * In the above grid, the diagonals are:
 * "[9]", "[5, 5]", "[1, 1, 1]", "[2, 2, 2]", "[3, 3]", "[4]".
 * In each diagonal all elements are the same, so the answer is True.
 *
 * Example 2:
 *
 * Input:
 * matrix = [
 *   [1,2],
 *   [2,2]
 * ]
 * Output: False
 * Explanation:
 * The diagonal "[1, 2]" has different elements.
 *
 * Note:
 *
 *     matrix will be a 2D array of integers.
 *     matrix will have a number of rows and columns in range [1, 20].
 *     matrix[i][j] will be integers in range [0, 99].
 *
 * Follow up:
 *
 *     What if the matrix is stored on disk, and the memory is limited such that you can only load at most one row of the matrix into the memory at once?
 *     What if the matrix is so large that you can only load up a partial row into the memory at once?
 */
/**
 * For the follow up
 *
 * 1. What if the matrix is stored on disk, and the memory is limited such that
 * you can only load at most one row of the matrix into the memory at once?
 *
 * Compare half of 1 row with half of the next/previous row.
 *
 * 2. What if the matrix is so large that you can only load up a partial row
 * into the memory at once?
 *
 * The second method in below is probably good memthod for this one.
 *
 * Hash 2 rows (so only 1 element needs to be loaded at a time) and compare the results,
 * excluding the appropriate beginning or ending element.
 */
class Solution {
public:
    bool isToeplitzMatrix(vector<vector<int>>& matrix) {
        int rows = matrix.size();
        int cols = matrix[0].size();

        for (int i = 1; i < rows; ++i) {
            for (int j = 1; j < cols; ++j) {
                if (matrix[i][j] != matrix[i - 1][j - 1]) return false;
            }
        }
        return true;
    }
};

/**
 * We ask what feature makes two coordinates (r1, c1) and (r2, c2) belong to the same diagonal?
 *
 * It turns out two coordinates are on the same diagonal if and only if r1 - c1 == r2 - c2.
 *
 * Actually the Toeplitz Matrix is totally determined by the first row and first column.
 *
 * Based on this, we can use the map method.
 */
class Solution {
public:
    bool isToeplitzMatrix(vector<vector<int>>& matrix) {
        map<int, int> tmmap;
        int rows = matrix.size();
        int cols = matrix[0].size();

        for (int i = 0; i < cols; ++i) {
            tmmap[0 - i] = matrix[0][i];
        }
        for (int j = 0; j < rows; ++j) {
            tmmap[j - 0] = matrix[j][0];
        }

        for (int i = 1; i < rows; ++i) {
            for (int j = 1; j < cols; ++j) {
                if (matrix[i][j] != tmmap[i - j]) return false;
            }
        }

        return true;
    }
};
