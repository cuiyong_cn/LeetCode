/**
 * Greatest English Letter in Upper and Lower Case
 *
 * Given a string of English letters s, return the greatest English letter which occurs as both a
 * lowercase and uppercase letter in s. The returned letter should be in uppercase. If no such
 * letter exists, return an empty string.
 *
 * An English letter b is greater than another letter a if b appears after a in the English alphabet.
 *
 * Example 1:
 *
 * Input: s = "lEeTcOdE"
 * Output: "E"
 * Explanation:
 * The letter 'E' is the only letter to appear in both lower and upper case.
 * Example 2:
 *
 * Input: s = "arRAzFif"
 * Output: "R"
 * Explanation:
 * The letter 'R' is the greatest letter to appear in both lower and upper case.
 * Note that 'A' and 'F' also appear in both lower and upper case, but 'R' is greater than 'F' or 'A'.
 * Example 3:
 *
 * Input: s = "AbCdEfGhIjK"
 * Output: ""
 * Explanation:
 * There is no letter that appears in both lower and upper case.
 *
 * Constraints:
 *
 * 1 <= s.length <= 1000
 * s consists of lowercase and uppercase English letters.
 */
/**
 * Original solution.
 */
class Solution {
public:
    string greatestLetter(string s) {
        auto cnt = array<int, 26>{ 0, };
        for (auto c : s) {
            auto [idx, val] = invoke([](auto c) {
                return isupper(c) ? make_pair(c - 'A', 2) : make_pair(c - 'a', 1);
            }, c);
            cnt[idx] |= val;
        }

        for (auto i = 25; i >= 0; --i) {
            if (3 == cnt[i]) {
                return string(1, 'A' + i);
            }
        }
        return "";
    }
};
