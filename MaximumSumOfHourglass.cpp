/**
 * Maximum Sum of an Hourglass
 *
 * You are given an m x n integer matrix grid.
 *
 * We define an hourglass as a part of the matrix with the following form:
 *
 *
 * Return the maximum sum of the elements of an hourglass.
 *
 * Note that an hourglass cannot be rotated and must be entirely contained within the matrix.
 *
 * Example 1:
 *
 * Input: grid = [[6,2,1,3],[4,2,1,5],[9,2,8,7],[4,1,2,9]]
 * Output: 30
 * Explanation: The cells shown above represent the hourglass with the maximum sum: 6 + 2 + 1 + 2 + 9 + 2 + 8 = 30.
 * Example 2:
 *
 * Input: grid = [[1,2,3],[4,5,6],[7,8,9]]
 * Output: 35
 * Explanation: There is only one hourglass in the matrix, with the sum: 1 + 2 + 3 + 5 + 7 + 8 + 9 = 35.
 *
 * Constraints:
 *
 * m == grid.length
 * n == grid[i].length
 * 3 <= m, n <= 150
 * 0 <= grid[i][j] <= 106
 */
/**
 * Prefix sum not needed.
 */
class Solution {
public:
    int maxSum(vector<vector<int>>& grid) {
        auto ans = 0;
        auto const m = grid.size();
        auto const n = grid[0].size();
        for (auto i = 0; (i + 2) < m; ++i) {
            for (auto j = 0; (j + 2) < n; ++j) {
                auto top = grid[i][j] + grid[i][j + 1] + grid[i][j + 2];
                auto bot = grid[i + 2][j] + grid[i + 2][j + 1] + grid[i + 2][j + 2];
                ans = max(ans, top + bot + grid[i + 1][j + 1]);
            }
        }
        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int maxSum(vector<vector<int>>& grid) {
        for (auto& row : grid) {
            partial_sum(begin(row), end(row), begin(row));
        }

        auto ans = 0;
        auto const m = grid.size();
        auto const n = grid[0].size();
        for (auto i = 0; (i + 2) < m; ++i) {
            for (auto j = 0; (j + 2) < n; ++j) {
                auto top = grid[i][j + 2] - (j > 0 ? grid[i][j - 1] : 0);
                auto bot = grid[i + 2][j + 2] - ((j > 0 ? grid[i + 2][j - 1] : 0));
                auto mid = grid[i + 1][j + 1] - grid[i + 1][j];
                ans = max(ans, top + bot + mid);
            }
        }
        return ans;
    }
};
