/**
 * Count Complete Subarrays in an Array
 *
 * You are given an array nums consisting of positive integers.
 *
 * We call a subarray of an array complete if the following condition is satisfied:
 *
 * The number of distinct elements in the subarray is equal to the number of distinct elements in the whole array.
 * Return the number of complete subarrays.
 *
 * A subarray is a contiguous non-empty part of an array.
 *
 * Example 1:
 *
 * Input: nums = [1,3,1,2,2]
 * Output: 4
 * Explanation: The complete subarrays are the following: [1,3,1,2], [1,3,1,2,2], [3,1,2] and [3,1,2,2].
 * Example 2:
 *
 * Input: nums = [5,5,5,5]
 * Output: 10
 * Explanation: The array consists only of the integer 5, so any subarray is complete. The number of subarrays that we can choose is 10.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 1000
 * 1 <= nums[i] <= 2000
 */
/**
 * O(n) solution.
 */
template<typename T>
auto distinct_count(vector<T> const& con)
{
    return set<T>{begin(con), end(con)}.size();
}

class Solution {
public:
    int countCompleteSubarrays(vector<int>& nums) {
        auto dcnt = distinct_count(nums);
        int  const n = nums.size();
        auto ans = 0;
        auto num_cnt = unordered_map<int, int>{};

        for (auto i = 0, j = 0; i < n; ++i) {
            dcnt -= 1 == (++num_cnt[nums[i]]);

            while (0 == dcnt) {
                dcnt += 0 == (--num_cnt[nums[j++]]);
            }

            ans += j;
        }

        return ans;
    }
};

/**
 * Original solution.
 */
template<typename T>
auto distinct_count(vector<T> const& con)
{
    return set<T>{begin(con), end(con)}.size();
}

class Solution {
public:
    int countCompleteSubarrays(vector<int>& nums) {
        auto const dcnt = distinct_count(nums);
        int  const n = nums.size();
        auto ans = 0;

        for (auto len = dcnt; len <= n; ++len) {
            auto num_cnt = unordered_map<int, int>{};

            for (auto i = 0, j = 0, L = 1; j < n; ++j, ++L) {
                ++num_cnt[nums[j]];

                if (L > len) {
                    auto cnt = --num_cnt[nums[i]];
                    if (0 == cnt) {
                        num_cnt.erase(nums[i]);
                    }
                    ++i;
                }
                ans += L >= len ? num_cnt.size() == dcnt : 0;
            }
        }

        return ans;
    }
};
