/**
 * Jump Game II
 *
 * Given an array of non-negative integers nums, you are initially positioned at the first index of
 * the array.
 *
 * Each element in the array represents your maximum jump length at that position.
 *
 * Your goal is to reach the last index in the minimum number of jumps.
 *
 * You can assume that you can always reach the last index.
 *
 * Example 1:
 *
 * Input: nums = [2,3,1,1,4]
 * Output: 2
 * Explanation: The minimum number of jumps to reach the last index is 2. Jump 1 step from index 0
 * to 1, then 3 steps to the last index.
 * Example 2:
 *
 * Input: nums = [2,3,0,1,4]
 * Output: 2
 *
 * Constraints:
 *
 * 1 <= nums.length <= 104
 * 0 <= nums[i] <= 1000
 */
/**
 * Simplify below solution.
 */
class Solution {
public:
    int jump(vector<int> const& nums) {
        int jumps = 0, curEnd = 0, curFarthest = 0;
        for (int i = 0; i < (nums.size() - 1); i++) {
            curFarthest = max(curFarthest, i + nums[i]);
            if (i == curEnd) {
                jumps++;
                curEnd = curFarthest;
            }
        }

        return jumps;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int jump(vector<int>& nums) {
        int pos = 0;
        int jumps = 0;
        while (pos < (nums.size() - 1)) {
            ++jumps;
            auto max_jump_to = pos + nums[pos];
            if (max_jump_to >= (nums.size() - 1)) {
                break;
            }

            auto can_jump_to = max_jump_to;
            auto next_pos = pos;
            for (int i = pos; i <= max_jump_to; ++i) {
                auto jump_to = i + nums[i];
                if (jump_to > can_jump_to) {
                    can_jump_to = jump_to;
                    next_pos = i;
                }
            }
            pos = next_pos;
        }

        return jumps;
    }
};
