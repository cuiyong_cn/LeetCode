/**
 * Majority Element
 *
 * Given an array of size n, find the majority element. The majority element is the element that appears more than n/2 times.
 *
 * You may assume that the array is non-empty and the majority element always exist in the array.
 *
 * Example 1:
 *
 * Input: [3,2,3]
 * Output: 3
 *
 * Example 2:
 *
 * Input: [2,2,1,1,1,2,2]
 * Output: 2
 */
/**
 * Improved version.
 */
class Solution {
public:
    int majorityElement(vector<int>& nums) {
        sort(nums.begin(), nums.end());

        return nums[nums.size() >> 1];
    }
};
/**
 * Original solution. A little bit flaw: we don't need to traverse the array again.
 */
class Solution {
public:
    int majorityElement(vector<int>& nums) {
        sort(nums.begin(), nums.end());
        int maxLen = 1;
        int prev = 0;
        int idx = 0;
        for (int i = 1; i < nums.size(); ++i) {
            if (nums[i] != nums[i - 1]) {
                int len = i - prev;
                if (len > maxLen) {
                    maxLen = len;
                    idx = i - 1;
                }
                prev = i;
            }
        }
        int len = nums.size() - prev;
        if (len > maxLen) {
            idx = prev;
        }

        return nums[idx];
    }
};
