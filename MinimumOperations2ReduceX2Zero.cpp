/**
 * Minimum Operations to Reduce X to Zero
 *
 * You are given an integer array nums and an integer x. In one operation, you can either remove the
 * leftmost or the rightmost element from the array nums and subtract its value from x. Note that
 * this modifies the array for future operations.
 *
 * Return the minimum number of operations to reduce x to exactly 0 if it is possible, otherwise, return -1.
 *
 * Example 1:
 *
 * Input: nums = [1,1,4,2,3], x = 5
 * Output: 2
 * Explanation: The optimal solution is to remove the last two elements to reduce x to zero.
 * Example 2:
 *
 * Input: nums = [5,6,7,8,9], x = 4
 * Output: -1
 * Example 3:
 *
 * Input: nums = [3,2,20,1,1,3], x = 10
 * Output: 5
 * Explanation: The optimal solution is to remove the last three elements and the first two elements (5 operations in total) to reduce x to zero.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 105
 * 1 <= nums[i] <= 104
 * 1 <= x <= 109
 */
/**
 * Hash map solution.
 */
class Solution {
public:
    int minOperations(vector<int>& nums, int x) {
        unordered_map<int, int> left = { {0, -1 } };

        int const N = nums.size();
        int const n_max = N;
        int res = n_max;
        for (auto l = 0, sum = 0; l < N && sum <= x; ++l) {
            sum += nums[l];
            left[sum] = l;
        }

        for (int r = nums.size() - 1, sum = 0; r >= 0 && sum <= x; --r) {
            auto it = left.find(x - sum);
            if (it != end(left) && r >= it->second) {
                res = min(res, N - r + it->second);
            }
            sum += nums[r];
        }

        return n_max == res ? -1 : res;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int minOperations(vector<int>& nums, int x) {
        int const N = nums.size();
        int const complement_x = accumulate(nums.begin(), nums.end(), -x);
        if (complement_x < 0) {
            return -1;
        }

        if (complement_x == 0) {
            return N;
        }

        int i = 0, j = 0, sum = 0, ans = -1;
        while (j < N) {
            if (sum >= complement_x) {
                if (complement_x == sum) {
                    ans = max(ans, j - i);
                }
                sum -= nums[i++];
            }
            else {
                sum += nums[j++];
            }
        }

        while (i < N && sum > complement_x) {
            sum -= nums[i++];
        }

        if (complement_x == sum) {
            ans = max(ans, j - i);
        }

        return ans >= 0 ? N - ans : ans;
    }
};
