/**
 * Remove Stones to Minimize the Total
 *
 * You are given a 0-indexed integer array piles, where piles[i] represents the number of stones in
 * the ith pile, and an integer k. You should apply the following operation exactly k times:
 *
 * Choose any piles[i] and remove floor(piles[i] / 2) stones from it.  Notice that you can apply the
 * operation on the same pile more than once.
 *
 * Return the minimum possible total number of stones remaining after applying the k operations.
 *
 * floor(x) is the greatest integer that is smaller than or equal to x (i.e., rounds x down).
 *
 * Example 1:
 *
 * Input: piles = [5,4,9], k = 2
 * Output: 12
 * Explanation: Steps of a possible scenario are:
 * - Apply the operation on pile 2. The resulting piles are [5,4,5].
 * - Apply the operation on pile 0. The resulting piles are [3,4,5].
 * The total number of stones in [3,4,5] is 12.
 * Example 2:
 *
 * Input: piles = [4,3,6,7], k = 3
 * Output: 12
 * Explanation: Steps of a possible scenario are:
 * - Apply the operation on pile 2. The resulting piles are [4,3,3,7].
 * - Apply the operation on pile 3. The resulting piles are [4,3,3,4].
 * - Apply the operation on pile 0. The resulting piles are [2,3,3,4].
 * The total number of stones in [2,3,3,4] is 12.
 *
 * Constraints:
 *
 * 1 <= piles.length <= 105
 * 1 <= piles[i] <= 104
 * 1 <= k <= 105
 */
/**
 * Same idea but more efficient in this case.
 */
class Solution {
public:
    int minStoneSum(vector<int>& p, int k) {
        make_heap(begin(p), end(p));
        while (--k >= 0) {
            pop_heap(begin(p), end(p));
            p.back() -= p.back() / 2;
            push_heap(begin(p), end(p));
        }
        return accumulate(begin(p), end(p), 0);
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int minStoneSum(vector<int>& piles, int k) {
        int total = accumulate(begin(piles), end(piles), 0);
        priority_queue<int> p(begin(piles), end(piles));
        while (--k >= 0) {
            auto st = p.top(); p.pop();
            total -= st / 2;
            p.push((st + 1) / 2);
        }
        return total;
    }
};
