/**
 * Calculate Money in Leetcode Bank
 *
 * Hercy wants to save money for his first car. He puts money in the Leetcode bank every day.
 *
 * He starts by putting in $1 on Monday, the first day. Every day from Tuesday to Sunday, he will
 * put in $1 more than the day before. On every subsequent Monday, he will put in $1 more than the
 * previous Monday.  Given n, return the total amount of money he will have in the Leetcode bank at
 * the end of the nth day.
 *
 * Example 1:
 *
 * Input: n = 4
 * Output: 10
 * Explanation: After the 4th day, the total is 1 + 2 + 3 + 4 = 10.
 * Example 2:
 *
 * Input: n = 10
 * Output: 37
 * Explanation: After the 10th day, the total is (1 + 2 + 3 + 4 + 5 + 6 + 7) + (2 + 3 + 4) = 37. Notice that on the 2nd Monday, Hercy only puts in $2.
 * Example 3:
 *
 * Input: n = 20
 * Output: 96
 * Explanation: After the 20th day, the total is (1 + 2 + 3 + 4 + 5 + 6 + 7) + (2 + 3 + 4 + 5 + 6 + 7 + 8) + (3 + 4 + 5 + 6 + 7 + 8) = 96.
 *
 * Constraints:
 *
 * 1 <= n <= 1000
 */
/**
 * Totally math solution.
 */
class Solution {
public:
    int totalMoney(int n) {
        auto weeks = n / 7;
        auto days = n % 7;

        return 28 * weeks + 7 * weeks * (weeks - 1) / 2
             + (2 * (weeks + 1) + (days - 1)) * days / 2;
    }
};

/**
 * Using a little bit math.
 */
class Solution {
public:
    int totalMoney(int n) {
        auto weeks = n / 7;
        int ans = 28 * weeks + 7 * weeks * (weeks - 1) / 2;

        for (auto s = weeks + 1, days_left = n % 7; days_left > 0; --days_left, ++s) {
            ans += s;
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int totalMoney(int n) {
        int week = 1;
        int ans = 0;

        while (n > 0) {
            for (auto i = 0, save = week; i < 7 && n > 0; ++i, ++save, --n) {
                ans += save;
            }
            ++week;
        }

        return ans;
    }
};
