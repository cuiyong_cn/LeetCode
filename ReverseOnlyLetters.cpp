/**
 * Reverse Only Letters
 *
 * Given a string S, return the "reversed" string where all characters that are not a letter stay in the same place,
 * and all letters reverse their positions.
 *
 * Example 1:
 *
 * Input: "ab-cd"
 * Output: "dc-ba"
 *
 * Example 2:
 *
 * Input: "a-bC-dEf-ghIj"
 * Output: "j-Ih-gfE-dCba"
 *
 * Example 3:
 *
 * Input: "Test1ng-Leet=code-Q!"
 * Output: "Qedo1ct-eeLg=ntse-T!"
 *
 * Note:
 *
 *     S.length <= 100
 *     33 <= S[i].ASCIIcode <= 122
 *     S doesn't contain \ or "
 */
/**
 * Same idea.
 */
class Solution {
public:
    string reverseOnlyLetters(string S) {
        int i = 0;
        int j = S.size() - 1;
        while (i < j) {
            while (i < j && false == isLetter(S[i])) {
                ++i;
            }
            while (i < j && false == isLetter(S[j])) {
                --j;
            }
            if (i < j) {
                swap(S[i], S[j]);
                ++i;
                --j;
            }
        }
        return S;
    }
private:
    bool isLetter(char c) {
        if (('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z')) {
            return true;
        }
        return false;
    }
};
/**
 * My original solution.
 */
class Solution {
public:
    string reverseOnlyLetters(string S) {
        int i = 0;
        int j = S.size() - 1;
        while (i < j) {
            if (isLetter(S[i])) {
                if (isLetter(S[j])) {
                    swap(S[i], S[j]);
                    ++i;
                    --j;
                }
                else {
                    --j;
                }
            }
            else {
                ++i;
            }
        }
        return S;
    }
private:
    bool isLetter(char c) {
        if (('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z')) {
            return true;
        }
        return false;
    }
};
