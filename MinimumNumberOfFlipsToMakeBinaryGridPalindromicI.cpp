/**
 * Minimum Number of Flips to Make Binary Grid Palindromic I
 *
 * You are given an m x n binary matrix grid.
 *
 * A row or column is considered palindromic if its values read the same forward and backward.
 *
 * You can flip any number of cells in grid from 0 to 1, or from 1 to 0.
 *
 * Return the minimum number of cells that need to be flipped to make either all rows palindromic or all columns palindromic.
 *
 * Example 1:
 *
 * Input: grid = [[1,0,0],[0,0,0],[0,0,1]]
 *
 * Output: 2
 *
 * Explanation:
 *
 * Flipping the highlighted cells makes all the rows palindromic.
 *
 * Example 2:
 *
 * Input: grid = [[0,1],[0,1],[0,0]]
 *
 * Output: 1
 *
 * Explanation:
 *
 * Flipping the highlighted cell makes all the columns palindromic.
 *
 * Example 3:
 *
 * Input: grid = [[1],[0]]
 *
 * Output: 0
 *
 * Explanation:
 *
 * All rows are already palindromic.
 *
 * Constraints:
 *     m == grid.length
 *     n == grid[i].length
 *     1 <= m * n <= 2 * 105
 *     0 <= grid[i][j] <= 1
 */
/**
 * Original solution.
 */
auto flips_to_make_all_row_panlindromic(vector<vector<int>> const& g)
{
    int const cols = g[0].size();
    auto flips = 0;

    for (auto& row : g) {
        for (auto [i, j] = tuple(0, cols - 1); i <= j; ++i, --j) {
            if (row[i] != row[j]) {
                ++flips;
            }
        }
    }

    return flips;
}

auto flips_to_make_all_col_panlindromic(vector<vector<int>> const& g)
{
    int const rows = g.size();
    int const cols = g[0].size();
    auto flips = 0;

    for (auto [i, j] = tuple(0, rows - 1); i <= j; ++i, --j) {
        for (auto k = 0u; k < cols; ++k) {
            if (g[i][k] != g[j][k]) {
                ++flips;
            }
        }
    }

    return flips;
}

class Solution {
public:
    int minFlips(vector<vector<int>>& grid) {
        return min(flips_to_make_all_row_panlindromic(grid)
                 , flips_to_make_all_col_panlindromic(grid));
    }
};
