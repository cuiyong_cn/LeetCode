/**
 * Number of submatrics that sum to target
 *
 * Given a matrix, and a target, return the number of non-empty submatrices that sum to
 * target.
 *
 * A submatrix x1, y1, x2, y2 is the set of all cells matrix[x][y] with x1 <= x <= x2 and
 * y1 <= y <= y2.
 *
 * Two submatrices (x1, y1, x2, y2) and (x1', y1', x2', y2') are different if they have
 * some coordinate that is different: for example, if x1 != x1'.
 *
 * Example 1:
 *
 * Input: matrix = [[0,1,0],[1,1,1],[0,1,0]], target = 0
 * Output: 4
 * Explanation: The four 1x1 submatrices that only contain 0.
 *
 * Example 2:
 *
 * Input: matrix = [[1,-1],[-1,1]], target = 0
 * Output: 5
 * Explanation: The two 1x2 submatrices, plus the two 2x1 submatrices, plus the 2x2 submatrix.
 *
 * Note:
 *
 *     1 <= matrix.length <= 300
 *     1 <= matrix[0].length <= 300
 *     -1000 <= matrix[i] <= 1000
 *     -10^8 <= target <= 10^8
 */
/**
 * Using a map to reduce my version from O(n^4) to O(n^3)
 * The main idea is convert multi-row matrix to 1D matrix.
 */
class Solution {
public:
    int result=0,target;
    void get_result(vector<int>& nums)
    {
        int sum=0;
        unordered_map<int,int> map;
        map[0]++;
        for(int &i:nums)
        {
            sum+=i;
            result+=map[sum-target];
            map[sum]++;
        }
    }
    int numSubmatrixSumTarget(vector<vector<int>>& matrix, int target)
    {
        this->target=target;
        vector<int> row(matrix[0].size());
        for(int i=0;i<matrix.size();i++) //Convert 2D array to 1D by row.
        {
            fill(row.begin(),row.end(),0); //Clear vector to start the row with i as starting row.
            for(int j=i;j<matrix.size();j++)
            {
                for(int x=0;x<matrix[0].size();x++)         //Add next row
                    row[x]+=matrix[j][x];
                get_result(row);
            }
        }
        return result;
    }
};

/**
 * My original version. Though this method is correct, but leetcode suggests time limit
 * exceeded, because the time complexity is O(n^4)
 */
class Solution {
private:
    int spread(vector<vector<int>>& m, int x, int y) {
        int ans = 0;
        int preDia = 0;
        if (x > 0 && y > 0) {
            preDia = m[x - 1][y - 1];
        }
        for (int dx = 0; (x + dx) < m_rows; ++dx) {
            for (int dy = 0; (y + dy) < m_cols; ++dy) {
                int nx = x + dx;
                int ny = y + dy;
                int sum = m[nx][ny];
                sum -= x > 0 ? m[x - 1][ny] : 0;
                sum -= y > 0 ? m[nx][y - 1] : 0;
                sum += preDia;
                ans += (sum == m_target) ? 1 : 0;
            }
        }
        return ans;
    }
public:
    int numSubmatrixSumTarget(vector<vector<int>>& matrix, int target) {
        m_rows = matrix.size();
        m_cols = matrix[0].size();
        int ans = 0;
        m_target = target;
        for (int i = 0; i < m_rows; ++i) {
            for (int j = 1; j < m_cols; ++j) {
                matrix[i][j] += matrix[i][j - 1];
            }
        }

        for (int j = 0; j < m_cols; ++j) {
            for (int i = 1; i < m_rows; ++i) {
                matrix[i][j] += matrix[i - 1][j];
            }
        }

        for (int i = 0; i < m_rows; ++i) {
            for (int j = 0; j < m_cols; ++j) {
                ans += spread(matrix, i, j);
            }
        }
        return ans;
    }
private:
    int m_rows;
    int m_cols;
    int m_target;
};
