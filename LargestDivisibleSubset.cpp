/**
 * Largest Divisible Subset
 *
 * Given a set of distinct positive integers nums, return the largest subset answer such that every
 * pair (answer[i], answer[j]) of elements in this subset satisfies:
 *
 * answer[i] % answer[j] == 0, or
 * answer[j] % answer[i] == 0
 * If there are multiple solutions, return any of them.
 *
 * Example 1:
 *
 * Input: nums = [1,2,3]
 * Output: [1,2]
 * Explanation: [1,3] is also accepted.
 * Example 2:
 *
 * Input: nums = [1,2,4,8]
 * Output: [1,2,4,8]
 *
 * Constraints:
 *
 * 1 <= nums.length <= 1000
 * 1 <= nums[i] <= 2 * 109
 * All the integers in nums are unique.
 */
/**
 * Speed it up using DP.
 */
class Solution {
public:
    vector<int> largestDivisibleSubset(vector<int>& A) {
        int n = A.size();

        sort(A.begin(), A.end());

        vector<int> dp(n, 1);
        vector<int> previous_index(n, -1);

        int max_ind = 0;
        for(int i = 1; i < n; ++i) {
            for (int j = 0; j < i; ++j) {
                if (0 == (A[i] % A[j]) && dp[i] < (dp[j] + 1)) {
                    dp[i] = dp[j]+1;
                    previous_index[i] = j;
                }
            }

            if (dp[i] > dp[max_ind]) {
                max_ind = i;
            }
        }

        vector<int> answer;
        int t = max_ind;  
        while (t >= 0) {
            answer.push_back(A[t]);
            t = previous_index[t];
        }

        return answer;  
    }
};

/**
 * Original solution. TLE
 */
class Solution {
public:
    vector<int> largestDivisibleSubset(vector<int>& nums) {
        vector<vector<int>> subsets;

        sort(begin(nums), end(nums));
        for (auto n : nums) {
            vector<vector<int>> new_candidate;
            for (auto const& sub : subsets) {
                if (0 == (n %sub.back())) {
                    new_candidate.push_back(sub);
                    new_candidate.back().push_back(n);
                }
            }
            subsets.insert(end(subsets), {n});
            subsets.insert(end(subsets), begin(new_candidate), end(new_candidate));
        }

        int max_len = 0;
        int pos = 0;
        for (int i = 0; i < subsets.size(); ++i) {
            if (subsets[i].size() > max_len) {
                max_len = subsets[i].size();
                pos = i;
            }
        }

        return subsets[pos];
    }
};
