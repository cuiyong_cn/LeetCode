/**
 * Minesweeper
 *
 * Let's play the minesweeper game (Wikipedia, online game)!
 *
 * You are given a 2D char matrix representing the game board. 'M' represents an unrevealed mine, 'E' represents an
 * unrevealed empty square, 'B' represents a revealed blank square that has no adjacent (above, below, left, right,
 * and all 4 diagonals) mines, digit ('1' to '8') represents how many mines are adjacent to this revealed square, and
 * finally 'X' represents a revealed mine.
 *
 * Now given the next click position (row and column indices) among all the unrevealed squares ('M' or 'E'), return
 * the board after revealing this position according to the following rules:
 *
 *     If a mine ('M') is revealed, then the game is over - change it to 'X'.
 *     If an empty square ('E') with no adjacent mines is revealed, then change it to revealed blank ('B') and all of
 *     its adjacent unrevealed squares should be revealed recursively.
 *     If an empty square ('E') with at least one adjacent mine is revealed, then change it to a digit ('1' to '8')
 *     representing the number of adjacent mines.
 *     Return the board when no more squares will be revealed.
 *
 * Example 1:
 *
 * Input:
 *
 * [['E', 'E', 'E', 'E', 'E'],
 *  ['E', 'E', 'M', 'E', 'E'],
 *  ['E', 'E', 'E', 'E', 'E'],
 *  ['E', 'E', 'E', 'E', 'E']]
 *
 * Click : [3,0]
 *
 * Output:
 *
 * [['B', '1', 'E', '1', 'B'],
 *  ['B', '1', 'M', '1', 'B'],
 *  ['B', '1', '1', '1', 'B'],
 *  ['B', 'B', 'B', 'B', 'B']]
 *
 * Explanation:
 *
 * Example 2:
 *
 * Input:
 *
 * [['B', '1', 'E', '1', 'B'],
 *  ['B', '1', 'M', '1', 'B'],
 *  ['B', '1', '1', '1', 'B'],
 *  ['B', 'B', 'B', 'B', 'B']]
 *
 * Click : [1,2]
 *
 * Output:
 *
 * [['B', '1', 'E', '1', 'B'],
 *  ['B', '1', 'X', '1', 'B'],
 *  ['B', '1', '1', '1', 'B'],
 *  ['B', 'B', 'B', 'B', 'B']]
 *
 * Explanation:
 *
 * Note:
 *
 *     The range of the input matrix's height and width is [1,50].
 *     The click position will only be an unrevealed square ('M' or 'E'), which also means the input board contains at least one clickable square.
 *     The input board won't be a stage when game is over (some mines have been revealed).
 *     For simplicity, not mentioned rules should be ignored in this problem. For example, you don't need to reveal all the unrevealed mines when the game is over, consider any cases that you will win the game or flag any squares.
 */
/**
 * Refactored one.
 */
class Solution {
public:
    Solution() : neswdiag{-1, 0, 1, 0, -1, -1, 1, 1, -1} {
        
    }

    vector<vector<char>> updateBoard(vector<vector<char>>& board, vector<int>& click) {
        deque<pair<int, int>> q({ { click[0], click[1] } });
        int R = board.size();
        int C = board[0].size();
        if ('M' == board[click[0]][click[1]]) {
            board[click[0]][click[1]] = 'X';
            return board;
        }

        while (false == q.empty()) {
            auto r = q.front().first;
            auto c = q.front().second;
            auto mines = 0;
            vector<pair<int, int>> neighbours;
            char& object = board[r][c];
            if ('E' == object) {
                for (int i = 1; i < neswdiag.size(); ++i) {
                    int nr = r + neswdiag[i - 1];
                    int nc = c + neswdiag[i];
                    if (-1 < nr && nr < R && -1 < nc && nc < C) {
                        if ('M' == board[nr][nc]) {
                            ++mines;
                        }
                        else if ('E' == board[nr][nc]) {
                            neighbours.push_back({nr, nc});
                        }
                    }
                }

                if (mines > 0) {
                    object = '0' + mines;
                }
                else {
                    object = 'B';

                    for (auto n : neighbours) {
                        q.push_back(n);
                    }
                }
            }

            q.pop_front();
        }
        return board;
    }
private:
    vector<int> neswdiag;
};

/**
 * Memory friendly and more efficient solution. using bfs.
 */
class Solution {
public:
    Solution() : neswdiag{-1, 0, 1, 0, -1, -1, 1, 1, -1} {
        
    }

    vector<vector<char>> updateBoard(vector<vector<char>>& board, vector<int>& click) {
        deque<pair<int, int>> q({ { click[0], click[1] } });
        int R = board.size();
        int C = board[0].size();

        while (false == q.empty()) {
            auto r = q.front().first;
            auto c = q.front().second;
            auto mines = 0;
            vector<pair<int, int>> neighbours;
            char& object = board[r][c];
            if ('M' == object) {
                object = 'X';
            }
            else {
                for (int i = 1; i < neswdiag.size(); ++i) {
                    int nr = r + neswdiag[i - 1];
                    int nc = c + neswdiag[i];
                    if (-1 < nr && nr < R && -1 < nc && nc < C) {
                        if ('M' == board[nr][nc]) {
                            ++mines;
                        }
                        else if (0 == mines && 'E' == board[nr][nc]) {
                            neighbours.push_back({nr, nc});
                        }
                    }
                }
            }

            if (mines > 0) {
                object = '0' + mines;
            }
            else {
                if (object != 'X') {
                    object = 'B';
                }

                for (auto n : neighbours) {
                    board[n.first][n.second] = 'B';
                    q.push_back(n);
                }
            }
            q.pop_front();
        }
        return board;
    }
private:
    vector<int> neswdiag;
};

/**
 * My original solution using dfs. Not very efficient.
 */
class Solution {
public:
    Solution() : neswdiag{-1, 0, 1, 0, -1, -1, 1, 1, -1} {

    }

    vector<vector<char>> updateBoard(vector<vector<char>>& board, vector<int>& click) {
        int r = click[0];
        int c = click[1];
        int R = board.size();
        int C = 0;
        if (R > 0) {
            C = board[0].size();
        }
        char object = board[r][c];
        if ('M' == object) {
            board[r][c] = 'X';
        }
        else if ('E' == object) {
            int minesCount = minesAroundClick(board, click);
            if (minesCount > 0) {
                board[r][c] = '0' + minesCount;
            }
            else {
                board[r][c] = 'B';
                for (int i = 1; i < neswdiag.size(); ++i) {
                    int nr = r + neswdiag[i - 1];
                    int nc = c + neswdiag[i];
                    if (-1 < nr && nr < R && -1 < nc && nc < C) {
                        vector<int> point = {nr, nc};
                        updateBoard(board, point);
                    }
                }
            }
        }

        return board;
    }
private:
    int minesAroundClick(vector<vector<char> >& board, vector<int>& click) {
        int r = click[0];
        int c = click[1];
        int R = board.size();
        int C = 0;
        if (R > 0) {
            C = board[0].size();
        }

        int count = 0;
        for (int i = 1; i < neswdiag.size(); ++i) {
            int nr = r + neswdiag[i - 1];
            int nc = c + neswdiag[i];
            if (-1 < nr && nr < R && -1 < nc && nc < C) {
                if (board[nr][nc] == 'M') {
                    ++count;
                }
            }
        }
        return count;
    }

    vector<int> neswdiag;
};
