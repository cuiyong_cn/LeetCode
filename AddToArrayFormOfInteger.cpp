/**
 * Add to Array-Form of Integer
 * For a non-negative integer X, the array-form of X is an array of its digits in left to right order.  For example, if
 * X = 1231, then the array form is [1,2,3,1].
 *
 * Given the array-form A of a non-negative integer X, return the array-form of the integer X+K.
 *
 * Example 1:
 *
 * Input: A = [1,2,0,0], K = 34
 * Output: [1,2,3,4]
 * Explanation: 1200 + 34 = 1234
 * Example 2:
 *
 * Input: A = [2,7,4], K = 181
 * Output: [4,5,5]
 * Explanation: 274 + 181 = 455
 * Example 3:
 *
 * Input: A = [2,1,5], K = 806
 * Output: [1,0,2,1]
 * Explanation: 215 + 806 = 1021
 * Example 4:
 *
 * Input: A = [9,9,9,9,9,9,9,9,9,9], K = 1
 * Output: [1,0,0,0,0,0,0,0,0,0,0]
 * Explanation: 9999999999 + 1 = 10000000000
 *
 * Note：
 *
 * 1 <= A.length <= 10000
 * 0 <= A[i] <= 9
 * 0 <= K <= 10000
 * If A.length > 1, then A[0] != 0
 */
/**
 * Overcome the K limitation. make it more compact.
 */
class Solution {
public:
    vector<int> addToArrayForm(vector<int> A, int K) {
        auto [i, k] = handle_overflow_situation(A, K);

        for (; i >= 0 && k > 0; --i) {
            A[i] += k;
            k = A[i] / 10;
            A[i] %= 10;
        }

        while (k > 0) {
            A.insert(A.begin(), k % 10);
            k /= 10;
        }

        return A;
    }

private:
    std::pair<int, int> handle_overflow_situation(vector<int>& A, int K)
    {
        const int limit = std::numeric_limits<int>::max() - A.back();
        if (K > limit) {
            int sum = A.back() + K % 10;
            A.back() = sum % 10;
            K = K / 10 + sum / 10;
            return {A.size() - 2, K};
        }

        return {A.size() - 1, K};
    }
};

/**
 * To overcome the K limitation.
 * Or we could first handle the overflow sitation. Then using the K as
 * the carry role.
 */
class Solution {
public:
    vector<int> addToArrayForm(vector<int> A, int K) {
        int pos = A.size() - 1;
        int carry = 0;

        while (pos >= 0 && K > 0) {
            int sum = A[pos] + K % 10 + carry;
            A[pos] = sum % 10;
            carry = sum / 10;
            --pos;
            K /= 10;
        }

        while (carry > 0 && pos >= 0) {
            int sum = A[pos] + carry;
            A[pos] = sum % 10;
            carry = sum / 10;
            --pos;
        }

        while (K > 0) {
            int sum = K % 10 + carry;
            A.insert(A.begin(), sum % 10);
            carry = sum / 10;
            K /= 10;
        }

        if (carry) {
            A.insert(A.begin(), 1);
        }

        return A;
    }
};

/**
 * More efficient one. But has a little draw back. If K has no value limitation.
 * Then A[i] += K might suffer integer overflow.
 */
class Solution {
public:
    vector<int> addToArrayForm(vector<int> A, int K) {
        for (int i = A.size() - 1; i >= 0 && K > 0; --i) {
            A[i] += K;
            K = A[i] / 10;
            A[i] %= 10;
        }

        while (K > 0) {
            A.insert(A.begin(), K % 10);
            K /= 10;
        }

        return A;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<int> addToArrayForm(vector<int>& A, int K) {
        vector<int> ans;

        int carry = 0;
        for (int i = A.size() - 1; i >= 0; --i) {
            int sum = A[i] + K % 10 + carry;
            ans.push_back(sum % 10);
            carry = sum / 10;
            K = K / 10;
        }

        while (K > 0) {
            int sum = K % 10 + carry;
            ans.push_back(sum % 10);
            carry = sum / 10;
            K = K / 10;
        }

        if (carry) {
            ans.push_back(carry);
        }

        reverse(ans.begin(), ans.end());

        return ans;
    }
};
