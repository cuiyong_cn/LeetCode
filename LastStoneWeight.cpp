/**
 * Last Stone Weight
 *
 * We have a collection of rocks, each rock has a positive integer weight.
 * 
 * Each turn, we choose the two heaviest rocks and smash them together.  Suppose the
 * stones have weights x and y with x <= y.  The result of this smash is:
 * 
 *     If x == y, both stones are totally destroyed;
 *     If x != y, the stone of weight x is totally destroyed, and the stone of
 *     weight y has new weight y-x.
 * 
 * At the end, there is at most 1 stone left.  Return the weight of this stone
 * (or 0 if there are no stones left.)
 * 
 * Example 1:
 * 
 * Input: [2,7,4,1,8,1]
 * Output: 1
 * Explanation: 
 * We combine 7 and 8 to get 1 so the array converts to [2,4,1,1,1] then,
 * we combine 2 and 4 to get 2 so the array converts to [2,1,1,1] then,
 * we combine 2 and 1 to get 1 so the array converts to [1,1,1] then,
 * we combine 1 and 1 to get 0 so the array converts to [1] then that's the value of last stone.
 * 
 * Note:
 * 
 *     1 <= stones.length <= 30
 *     1 <= stones[i] <= 1000
 */
/**
 * Original version. Not really intuitive.
 */
class Solution {
public:
    int lastStoneWeight(vector<int>& stones) {
        int i = stones.size();
        while (i > 1) {
            sort(stones.begin(), stones.begin() + i);

            if (stones[i - 1] > stones[i - 2]) {
                stones[i - 2] = stones[i - 1] - stones[i -2];
                stones[i - 1] = 0;
                --i;
            }
            else {
                stones[i - 1] = 0; stones[i - 2] = 0;
                i -= 2;
            }
        }
        
        return stones[0];
    }
};

/**
 * Multiset version
 */
class Solution {
public:
    int lastStoneWeight(vector<int>& st) {
        multiset<int> s(begin(st), end(st));
        while (s.size() > 1) {
            auto w1 = *prev(s.end());
            s.erase(prev(s.end()));
            auto w2 = *prev(s.end());
            s.erase(prev(s.end()));
            if (abs(w1 - w2) > 0) s.insert(abs(w1 - w2));
        }
        return s.empty() ? 0 : *s.begin();
    }
};

/**
 * Priority Queue
 */
class Solution {
public:
    int lastStoneWeight(vector<int>& st) {
        priority_queue<int> q(begin(st), end(st));
        while (q.size() > 1) {
            auto w1 = q.top(); q.pop();
            auto w2 = q.top(); q.pop();
            if (abs(w1 - w2) > 0) q.push(abs(w1 - w2));
        }
        return q.empty() ? 0 : q.top();
    }
};
