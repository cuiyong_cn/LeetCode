/**
 * Possible Bipartition
 *
 * Given a set of N people (numbered 1, 2, ..., N), we would like to split everyone into two groups of any size.
 *
 * Each person may dislike some other people, and they should not go into the same group.
 *
 * Formally, if dislikes[i] = [a, b], it means it is not allowed to put the people numbered a and b into the same group.
 *
 * Return true if and only if it is possible to split everyone into two groups in this way.
 *
 * Example 1:
 *
 * Input: N = 4, dislikes = [[1,2],[1,3],[2,4]]
 * Output: true
 * Explanation: group1 [1,4], group2 [2,3]
 * Example 2:
 *
 * Input: N = 3, dislikes = [[1,2],[1,3],[2,3]]
 * Output: false
 * Example 3:
 *
 * Input: N = 5, dislikes = [[1,2],[2,3],[3,4],[4,5],[1,5]]
 * Output: false
 *
 * Constraints:
 *
 * 1 <= N <= 2000
 * 0 <= dislikes.length <= 10000
 * dislikes[i].length == 2
 * 1 <= dislikes[i][j] <= N
 * dislikes[i][0] < dislikes[i][1]
 * There does not exist i != j for which dislikes[i] == dislikes[j].
 */
/**
 * The core idea is same, but speed it up.
 * This is actually a DFS solution. Also we can convert this one into BFS solution.
 */
class Solution {
public:
    bool possibleBipartition(int N, vector<vector<int>>& dislikes) {
        vector<int> color(N + 1, 0);
        vector<vector<int>> dislike_of(N + 1);
        for (const auto& dg : dislikes) {
            dislike_of[dg[0]].push_back(dg[1]);
            dislike_of[dg[1]].push_back(dg[0]);
        }

        for (int p = 1; p <= N; ++p) {
            if (!color[p] && !group_to(dislike_of, color, p, 1)) {
                return false;
            }
        }

        return true;
    }

private:
    bool group_to(const vector<vector<int>>& dislike_of, vector<int>& color, int p, int c)
    {
        if (color[p]) {
            return color[p] == c;
        }

        color[p] = c;

        int op_color = 1 == c ? 2 : 1;
        for (auto op : dislike_of[p]) {
            if (!group_to(dislike_of, color, op, op_color)) {
                return false;
            }
        }

        return true;
    }
};

/**
 * Original solution. Brute force, TLE as expected.
 */
class Solution {
public:
    bool possibleBipartition(int N, vector<vector<int>>& dislikes) {
        vector<int> color(N + 1, 0);
        vector<vector<int>> dislike_of(N + 1);
        for (const auto& dg : dislikes) {
            dislike_of[dg[0]].push_back(dg[1]);
        }

        if (to_group_one(dislike_of, color, 1)) {
            return true;
        }

        return to_group_two(dislike_of, color, 1);
    }

private:
    bool to_group_one(const vector<vector<int>>& dislike_of, vector<int> color, int i)
    {
        if (i >= color.size()) {
            return true;
        }

        if (2 == color[i]) {
            return false;
        }

        color[i] = 1;
        for (auto& p : dislike_of[i]) {
            if (color[p] == 1) {
                return false;
            }
            color[p] = 2;
        }

        if (!to_group_one(dislike_of, color, i + 1) && !to_group_two(dislike_of, color, i + 1)) {
            return false;
        }

        return true;
    }

    bool to_group_two(const vector<vector<int>>& dislike_of, vector<int> color, int i)
    {
        if (i >= color.size()) {
            return true;
        }

        if (1 == color[i]) {
            return false;
        }

        color[i] = 2;
        for (auto& p : dislike_of[i]) {
            if (color[p] == 2) {
                return false;
            }
            color[p] = 1;
        }

        if (!to_group_one(dislike_of, color, i + 1) && !to_group_two(dislike_of, color, i + 1)) {
            return false;
        }

        return true;
    }
};
