/**
 * Find Original Array From Doubled Array
 *
 * An integer array original is transformed into a doubled array changed by appending twice the
 * value of every element in original, and then randomly shuffling the resulting array.
 *
 * Given an array changed, return original if changed is a doubled array. If changed is not a
 * doubled array, return an empty array. The elements in original may be returned in any order.
 *
 * Example 1:
 *
 * Input: changed = [1,3,4,2,6,8]
 * Output: [1,3,4]
 * Explanation: One possible original array could be [1,3,4]:
 * - Twice the value of 1 is 1 * 2 = 2.
 * - Twice the value of 3 is 3 * 2 = 6.
 * - Twice the value of 4 is 4 * 2 = 8.
 * Other original arrays could be [4,3,1] or [3,1,4].
 * Example 2:
 *
 * Input: changed = [6,3,0,1]
 * Output: []
 * Explanation: changed is not a doubled array.
 * Example 3:
 *
 * Input: changed = [1]
 * Output: []
 * Explanation: changed is not a doubled array.
 *
 * Constraints:
 *
 * 1 <= changed.length <= 105
 * 0 <= changed[i] <= 105
 */
/**
 * Original solution.
 */
class Solution {
public:
    vector<int> findOriginalArray(vector<int>& changed) {
        map<int, int> num_cnt;
        for (auto const& n : changed) {
            ++num_cnt[n];
        }

        vector<int> ans;
        // let's rule out the special case for 0
        auto zero = num_cnt.find(0);
        if (zero != num_cnt.end()) {
            if (zero->second & 1) {
                return {};
            }
            ans.insert(ans.end(), zero->second / 2, 0);
            num_cnt.erase(0);
        }

        while (!num_cnt.empty()) {
            auto [num, cnt] = *num_cnt.begin();
            auto it = num_cnt.find(num * 2);
            if (it == num_cnt.end() || cnt > it->second) {
                return {};
            }

            num_cnt.erase(num);
            it->second -= cnt;
            if (0 == it->second) {
                num_cnt.erase(num * 2);
            }

            ans.insert(ans.end(), cnt, num);
        }

        return ans;
    }
};
