/**
 * Number of People Aware of a Secret
 *
 * On day 1, one person discovers a secret.
 *
 * You are given an integer delay, which means that each person will share the secret with a new
 * person every day, starting from delay days after discovering the secret. You are also given an
 * integer forget, which means that each person will forget the secret forget days after discovering
 * it. A person cannot share the secret on the same day they forgot it, or on any day afterwards.
 *
 * Given an integer n, return the number of people who know the secret at the end of day n. Since
 * the answer may be very large, return it modulo 109 + 7.
 *
 * Example 1:
 *
 * Input: n = 6, delay = 2, forget = 4
 * Output: 5
 * Explanation:
 * Day 1: Suppose the first person is named A. (1 person)
 * Day 2: A is the only person who knows the secret. (1 person)
 * Day 3: A shares the secret with a new person, B. (2 people)
 * Day 4: A shares the secret with a new person, C. (3 people)
 * Day 5: A forgets the secret, and B shares the secret with a new person, D. (3 people)
 * Day 6: B shares the secret with E, and C shares the secret with F. (5 people)
 * Example 2:
 *
 * Input: n = 4, delay = 1, forget = 3
 * Output: 6
 * Explanation:
 * Day 1: The first person is named A. (1 person)
 * Day 2: A shares the secret with B. (2 people)
 * Day 3: A and B share the secret with 2 new people, C and D. (4 people)
 * Day 4: A forgets the secret. B, C, and D share the secret with 3 new people. (6 people)
 *
 * Constraints:
 *
 * 2 <= n <= 1000
 * 1 <= delay < forget <= n
 */
/**
 * Most efficient.
 */
class Solution {
public:
    int peopleAwareOfSecret(int n, int delay, int forget) {
        vector<long> dp(n + 1);
        dp[1] = 1;
        int share = 0, mod = 1e9 + 7, res = 0 ;
        for (int i = 2; i <= n; ++i) {
            share = (share + dp[max(i - delay, 0)] - dp[max(i - forget, 0)] + mod) % mod;
            dp[i] = share;
        }

        for (int i = n - forget + 1; i <= n; ++i) {
            res = (res + dp[i]) % mod;
        }

        return res;
    }
};

/**
 * Reduce memory usage and a bit more efficient.
 */
class Solution {
public:
    int peopleAwareOfSecret(int n, int delay, int forget) {
        auto dp = array<int, 1001>(forget, 0);
        auto const mod = 1'000'000'007;
        dp[0] = 1;
        for (auto i = 1; i < n; ++i) {
            for (auto d = forget - 1; d > 0; --d) {
                dp[d] = dp[d - 1];
            }
            dp[0] = 0;
            for (auto d = delay; d < forget; ++d) {
                dp[0] += dp[d];
                dp[0] = dp[0] % mod;
            }
        }

        auto ans = 0;
        for (auto n : dp) {
            ans += n;
            ans = ans % mod;
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int peopleAwareOfSecret(int n, int delay, int forget) {
        auto dp = vector<vector<int>>(n, vector<int>(forget, 0));
        auto const mod = 1'000'000'007;
        dp[0][0] = 1;
        for (auto i = 1; i < n; ++i) {
            for (auto d = delay - 1; d < (forget - 1); ++d) {
                dp[i][0] += dp[i - 1][d];
                dp[i][0] = dp[i][0] % mod;
            }

            for (auto j = 1; j < forget; ++j) {
                dp[i][j] = dp[i - 1][j - 1];
            }
            //copy(begin(dp[i]), end(dp[i]), ostream_iterator<int>(cout, ","));
            //cout << '\n';
        }

        auto ans = 0;
        for (auto const& n : dp.back()) {
            ans += n;
            ans = ans % mod;
        }

        return ans;
    }
};
