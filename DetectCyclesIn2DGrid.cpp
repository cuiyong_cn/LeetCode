/**
 * Detect Cycles in 2D Grid
 *
 * Given a 2D array of characters grid of size m x n, you need to find if there exists any cycle
 * consisting of the same value in grid.
 *
 * A cycle is a path of length 4 or more in the grid that starts and ends at the same cell. From a
 * given cell, you can move to one of the cells adjacent to it - in one of the four directions (up,
 * down, left, or right), if it has the same value of the current cell.
 *
 * Also, you cannot move to the cell that you visited in your last move. For example, the cycle (1,
 * 1) -> (1, 2) -> (1, 1) is invalid because from (1, 2) we visited (1, 1) which was the last
 * visited cell.
 *
 * Return true if any cycle of the same value exists in grid, otherwise, return false.
 *
 * Example 1:
 *
 * Input: grid = [["a","a","a","a"],["a","b","b","a"],["a","b","b","a"],["a","a","a","a"]]
 * Output: true
 * Explanation: There are two valid cycles shown in different colors in the image below:
 *
 * Example 2:
 *
 * Input: grid = [["c","c","c","a"],["c","d","c","c"],["c","c","e","c"],["f","c","c","c"]]
 * Output: true
 * Explanation: There is only one valid cycle highlighted in the image below:
 *
 * Example 3:
 *
 * Input: grid = [["a","b","b"],["b","z","b"],["b","b","a"]]
 * Output: false
 *
 * Constraints:
 *
 * m == grid.length
 * n == grid[i].length
 * 1 <= m, n <= 500
 * grid consists only of lowercase English letters.
 */
/**
 * BFS solution.
 */
class Solution {
public:
    vector<int> d = {1, 0, -1, 0, 1};
    bool containsCycle(vector<vector<char>>& g) {
        for (int i = 0; i < g.size(); ++i)
            for (int j = 0; j < g[i].size(); ++j) {
                if (g[i][j] >= 'a') {
                    char val = g[i][j];
                    vector<pair<int, int>> q = {{i, j}};
                    while (!q.empty()) {
                        vector<pair<int, int>> q1;
                        for (auto [x, y] : q) {
                            if (g[x][y] < 'a')
                                return true;
                            g[x][y] -= 26;
                            for (auto k = 0; k < 4; ++k) {
                                int dx = x + d[k], dy = y + d[k + 1];
                                if (dx >= 0 && dy >= 0 && dx < g.size() && dy < g[dx].size() && g[dx][dy] == val)
                                    q1.push_back({dx, dy});
                            }
                        }
                        swap(q, q1);
                    }
                }
            }
        return false;
    }
};

/**
 * Original solution. DFS solution.
 */
class Solution {
public:
    bool containsCycle(vector<vector<char>>& grid) {
        M = grid.size();
        N = grid[0].size();

        for (int i = 0; i < M; ++i) {
            for (int j = 0; j < N; ++j) {
                if (grid[i][j] > 0 && dfs(grid, {-1, -1}, {i, j}, -grid[i][j])) {
                    return true;
                }
            }
        }

        return false;
    }

private:
    bool dfs(vector<vector<char>>& grid, pair<int, int> prev, pair<int, int> cur, char c)
    {
        if (grid[cur.first][cur.second] == c) {
            return true;
        }

        if (0 == (grid[cur.first][cur.second] + c)) {
            grid[cur.first][cur.second] = -grid[cur.first][cur.second];
            return (cur.second - 1 < 0 ? false :
                    prev.second == cur.second - 1 ? false : dfs(grid, cur, {cur.first, cur.second - 1}, c))
                || (cur.second + 1 >= N ? false :
                    prev.second == cur.second + 1 ? false : dfs(grid, cur, {cur.first, cur.second + 1}, c))
                || (cur.first - 1 < 0 ? false :
                    prev.first == cur.first - 1 ? false : dfs(grid, cur, {cur.first - 1, cur.second}, c))
                || (cur.first + 1 >= M ? false :
                    prev.first == cur.first + 1 ? false : dfs(grid, cur, {cur.first + 1, cur.second}, c));
        }

        return false;
    }

    int M = 0;
    int N = 0;
};
