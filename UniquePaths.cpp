/** Unique Paths
 *
 * A robot is located at the top-left corner of a m x n grid (marked 'Start' in the diagram below).
 *
 * The robot can only move either down or right at any point in time. The robot is trying to reach the bottom-right
 * corner of the grid (marked 'Finish' in the diagram below).
 *
 * How many possible unique paths are there?
 *
 * Above is a 7 x 3 grid. How many possible unique paths are there?
 *
 * Example 1:
 *
 * Input: m = 3, n = 2 Output: 3 Explanation: From the top-left corner, there are a total of 3 ways to reach the
 * bottom-right corner: 1. Right -> Right -> Down 2. Right -> Down -> Right 3. Down -> Right -> Right
 *
 * Example 2:
 *
 * Input: m = 7, n = 3 Output: 28
 *
 * Constraints:
 *     1 <= m, n <= 100 It's guaranteed that the answer will be less than or equal to 2 * 10 ^ 9.
 */
/**
 * Math formula.
 * For mxn grid, robot has to move exactly m-1 steps down and n-1 steps right and these can be done in any order.
 *
 * We have to know two math concept, permutations and combinations.
 * The total unique paths is then (m + n - 2)! / ((m - 1)! * (n - 1)!)
 */
public class Solution
{
    public int uniquePaths(int m, int n)
    {
        if (m == 1 || n == 1) return 1;

        m--; n--;

        if (m < n) swap(m, n);

        long res = 1;
        int j = 1;
        for (int i = m + 1; i <= m + n; i++, j++) {
            res *= i;
            res /= j;
        }

        return (int)res;
    }
}

/**
 * Further reduce it to O(n) space.
 */
class Solution
{
public:
    int uniquePaths(int m, int n)
    {
        vector<int> cur(n, 1);
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                cur[j] += cur[j - 1];
            }
        }
        return cur[n - 1];
    }
};

/**
 * Improved one. I believe there must exists a math formular to get unique paths directly. Because it is like this.
 * 70 35 15 5  1
 * 35 20 10 4  1
 * 15 10 6  3  1
 * 5  4  3  2  1
 * 1  1  1  1  0
 */
class Solution
{
public:
    unordered_map<int, int> memo;

    int uniquePaths(int m, int n)
    {
        if (1 == m || 1 == n) return 1;

        auto key = m * 101 + n;
        auto it = memo.find(key);
        if (it != memo.end()) {
            return it->second;
        }

        int uniqueForRight = 0, uniqueForDown = 0;

        if (n > 0) {
            uniqueForRight = uniquePaths(m, n - 1);
        }
        if (m > 0) {
            uniqueForDown = uniquePaths(m - 1, n);
        }

        memo[key] = uniqueForRight + uniqueForDown;

        return uniqueForRight + uniqueForDown;
    }
};

/**
 * The first thing come to mind is recursion solution. But TLE.
 * So we must using some memo tricky to speed this up.
 */
class Solution
{
public:
    int uniquePaths(int m, int n)
    {
        if (1 == m || 1 == n) return 1;

        int uniqueForRight = 0, uniqueForDown = 0;

        if (n > 0) {
            uniqueForRight = uniquePaths(m, n - 1);
        }
        if (m > 0) {
            uniqueForDown = uniquePaths(m - 1, n);
        }

        return uniqueForRight + uniqueForDown;
    }
};

