/**
 * Print FooBar Alternately
 *
 * Suppose you are given the following code:
 *
 * class FooBar {
 *   public void foo() {
 *     for (int i = 0; i < n; i++) {
 *       print("foo");
 *     }
 *   }
 *
 *   public void bar() {
 *     for (int i = 0; i < n; i++) {
 *       print("bar");
 *     }
 *   }
 * }
 *
 * The same instance of FooBar will be passed to two different threads. Thread A will
 * call foo() while thread B will call bar(). Modify the given program to output
 * "foobar" n times.
 *
 * Example 1:
 *
 * Input: n = 1
 * Output: "foobar"
 * Explanation: There are two threads being fired asynchronously. One of them calls foo(),
 * while the other calls bar(). "foobar" is being output 1 time.
 *
 * Example 2:
 *
 * Input: n = 2
 * Output: "foobarfoobar"
 * Explanation: "foobar" is being output 2 times.
 */
/**
 * Condition_variable solution.
 */
class FooBar
{
private:
    int n;
    bool                    fooTime;
    std::mutex              mtx;
    std::condition_variable cv;

public:
    FooBar(int n) : n(n), fooTime(true) {}

    void setFooTime(bool ft, std::unique_lock<std::mutex>& lock)
    {
        fooTime = ft;
        lock.unlock();
        cv.notify_one();
        lock.lock();
    }

    void foo(function<void()> printFoo)
    {
        std::unique_lock<std::mutex> lock(mtx);
        for (int i = 0; i < n; i++) {
            cv.wait(lock, [this] { return fooTime; });
            printFoo();
            setFooTime(false, lock);
        }
    }

    void bar(function<void()> printBar)
    {
        std::unique_lock<std::mutex> lock(mtx);
        for (int i = 0; i < n; i++) {
            cv.wait(lock, [this] { return !fooTime; });
            printBar();
            setFooTime(true, lock);
        }
    }
};
/**
 * Lock version. and it is faster than non-lock version. So lock is always better than
 * sleep? Also this solution pass the test, but it is not a safe sound one. Because a
 * lock should be locked and unlocked by the same thread.
 */
class FooBar
{
private:
    int n;
    mutex m1, m2;

public:
    FooBar(int n)
    {
        this->n = n;
        m2.lock();
    }

    void foo(function<void()> printFoo)
    {
        for (int i = 0; i < n; i++) {
            m1.lock();
            // printFoo() outputs "foo". Do not change or remove this line.
            printFoo();
            m2.unlock();
        }
    }

    void bar(function<void()> printBar)
    {
        for (int i = 0; i < n; i++) {
            m2.lock();
            // printBar() outputs "bar". Do not change or remove this line.
            printBar();
            m1.unlock();
        }
    }
};
/**
 * Without using any locks. Simple one.
 */
class FooBar
{
private:
    int n;
    int barindex;
    int fooindex;
public:
    FooBar(int n)
    {
        this->n = n;
        barindex = 0;
        fooindex = -1;
    }

    void foo(function<void()> printFoo)
    {
        for (int i = 0; i < n; i++) {
            while (i != barindex) {
                std::this_thread::sleep_for(std::chrono::nanoseconds(1));
            }
            // printFoo() outputs "foo". Do not change or remove this line.
            printFoo();
            ++fooindex;
        }
    }

    void bar(function<void()> printBar)
    {
        for (int i = 0; i < n; i++) {
            while (i != fooindex) {
                std::this_thread::sleep_for(std::chrono::nanoseconds(1));
            }
            // printBar() outputs "bar". Do not change or remove this line.
            printBar();
            ++barindex;
        }
    }
};

