/**
 * Sell Diminishing-Valued Colored Balls
 *
 * You have an inventory of different colored balls, and there is a customer that wants orders balls
 * of any color.
 *
 * The customer weirdly values the colored balls. Each colored ball's value is the number of balls
 * of that color you currently have in your inventory. For example, if you own 6 yellow balls, the
 * customer would pay 6 for the first yellow ball. After the transaction, there are only 5 yellow
 * balls left, so the next yellow ball is then valued at 5 (i.e., the value of the balls decreases
 * as you sell more to the customer).
 *
 * You are given an integer array, inventory, where inventory[i] represents the number of balls of
 * the ith color that you initially own. You are also given an integer orders, which represents the
 * total number of balls that the customer wants. You can sell the balls in any order.
 *
 * Return the maximum total value that you can attain after selling orders colored balls. As the
 * answer may be too large, return it modulo 109 + 7.
 *
 * Example 1:
 *
 * Input: inventory = [2,5], orders = 4
 * Output: 14
 * Explanation: Sell the 1st color 1 time (2) and the 2nd color 3 times (5 + 4 + 3).
 * The maximum total value is 2 + 5 + 4 + 3 = 14.
 * Example 2:
 *
 * Input: inventory = [3,5], orders = 6
 * Output: 19
 * Explanation: Sell the 1st color 2 times (3 + 2) and the 2nd color 4 times (5 + 4 + 3 + 2).
 * The maximum total value is 3 + 2 + 5 + 4 + 3 + 2 = 19.
 *
 * Constraints:
 *
 * 1 <= inventory.length <= 105
 * 1 <= inventory[i] <= 109
 * 1 <= orders <= min(sum(inventory[i]), 109)
 */
/**
 * Cut the top block solution.
 *
 *    + + +
 *    + + + +
 *    + + + + +
 *    + + + + + + +
 */
class Solution {
public:
    int maxProfit(vector<int>& A, int T) {
        sort(rbegin(A), rend(A)); // sort in descending order
        long mod = 1e9+7, N = A.size(), cur = A[0], ans = 0, i = 0;
        while (T) {
            while (i < N && A[i] == cur) ++i; // handle those same numbers together
            long next = i == N ? 0 : A[i], h = cur - next, r = 0, cnt = min((long)T, i * h);
            if (T < i * h) { // the i * h balls are more than what we need.
                h = T / i; // we just reduce the height by `T / i` instead
                r = T % i; 
            }
            long val = cur - h; // each of the remainder `r` balls has value `cur - h`.
            ans = (ans + (cur + val + 1) * h / 2 * i + val * r) % mod;
            T -= cnt;
            cur = next;
        }
        return ans;
    }
};

/**
 * Finally passed version. Idea is:
 * 1. Binary search find the stoping number k that all number > k, has to sell all the > k part.
 * 2. Then if the orders still > 0, then every number > k should contibute mid.
 */
class Solution {
public:
    int maxProfit(vector<int>& inventory, int orders) {
        sort(inventory.begin(), inventory.end(), greater<int>());
        int64_t L = 1;
        int64_t R = 1'000'000'000;
        int const mod = 1'000'000'007;
        int64_t total = 0;
        while (L <= R) {
            total = 0;
            int64_t mid = (L + R) / 2;
            for (auto i : inventory) {
                if (i <= mid) {
                    break;
                }

                total += i - mid;
            }

            if (total >= orders) {
                L = mid + 1;
            }
            else {
                R = mid - 1;
            }
        }

        int64_t ans = 0;
        for (auto i : inventory) {
            if (i <= L) {
                break;
            }

            int64_t max_buy = min<int64_t>(i - L, orders);
            ans += (i - max_buy + 1 + i) * max_buy / 2;
            orders -= max_buy;
        }

        if (orders > 0) {
            ans += L * orders;
        }

        return ans % mod;
    }
};

/**
 * Original solution. TLE.
 */
class Solution {
public:
    int maxProfit(vector<int>& inventory, int orders) {
        priority_queue<int> pq(inventory.begin(), inventory.end());
        int sells = 0;
        int64_t ans = 0;
        int const mod = 1'000'000'007;

        while (!pq.empty() && orders > 0) {
            auto right = pq.top(); pq.pop();
            auto left = pq.top();
            auto max_buy = min(right - left + 1, orders);

            ans += (right - max_buy + 1 + right) * max_buy / 2;
            orders -= max_buy;

            if (left > 1) {
                pq.emplace(left - 1);
            }
        }

        return ans % mod;
    }
};
