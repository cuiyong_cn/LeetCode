/**
 * Move Zeroes
 *
 * Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.
 *
 * Example:
 *
 * Input: [0,1,0,3,12]
 * Output: [1,3,12,0,0]
 *
 * Note:
 *
 *     You must do this in-place without making a copy of the array.
 *     Minimize the total number of operations.
 */
/**
 * Original two-pointer solution.
 */
class Solution {
public:
    void moveZeroes(vector<int>& nums) {
        int j = 0;
        for (auto& n: nums) {
            if (0 != n) {
                nums[j] = n;
                ++j;
            }
        }
        while (j < nums.size()) {
            nums[j] = 0;
            ++j;
        }
    }
};

/**
 * Leetcode proposed that the blow solution is better. But I do not think this is true. But still a good solution.
 */
class Solution {
public:
    void moveZeroes(vector<int>& nums) {
        int j = 0;
        for (auto& n: nums) {
            if (0 != n) {
                swap(nums[j], n);
                ++j;
            }
        }
    }
};
