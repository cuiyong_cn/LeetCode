/**
 * Number of Good Leaf Nodes Pairs
 *
 * You are given the root of a binary tree and an integer distance. A pair of two different leaf
 * nodes of a binary tree is said to be good if the length of the shortest path between them is less
 * than or equal to distance.
 *
 * Return the number of good leaf node pairs in the tree.
 *
 * Example 1:
 *
 * Input: root = [1,2,3,null,4], distance = 3
 * Output: 1
 * Explanation: The leaf nodes of the tree are 3 and 4 and the length of the shortest path between them is 3. This is the only good pair.
 * Example 2:
 *
 * Input: root = [1,2,3,4,5,6,7], distance = 3
 * Output: 2
 * Explanation: The good pairs are [4,5] and [6,7] with shortest path = 2. The pair [4,6] is not good because the length of ther shortest path between them is 4.
 * Example 3:
 *
 * Input: root = [7,1,4,6,null,5,3,null,null,null,null,null,2], distance = 3
 * Output: 1
 * Explanation: The only good pair is [2,5].
 *
 * Constraints:
 *
 * The number of nodes in the tree is in the range [1, 210].
 * 1 <= Node.val <= 100
 * 1 <= distance <= 10
 */
/**
 * Original solution.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int countPairs(TreeNode* root, int distance) {
        dist = distance;

        dfs(root, 0);

        return ans;
    }

private:
    vector<int> dfs(TreeNode* node, int height)
    {
        if (nullptr == node) {
            return {};
        }

        if (nullptr == node->left && nullptr == node->right) {
            return {height};
        }

        auto left_leaves = dfs(node->left, height + 1);
        auto right_leaves = dfs(node->right, height + 1);

        left_leaves.erase(remove_if(left_leaves.begin(), left_leaves.end(),
                                   [this, height](auto h) {
                                       return (h - height) > dist;
                                   }), left_leaves.end());
        right_leaves.erase(remove_if(right_leaves.begin(), right_leaves.end(),
                                   [this, height](auto h) {
                                       return (h - height) > dist;
                                   }), right_leaves.end());

        for (auto LH : left_leaves) {
            for (auto RH : right_leaves) {
                if (((LH - height) + (RH - height)) <= dist) {
                    ++ans;
                }
            }
        }

        left_leaves.insert(left_leaves.end(), right_leaves.begin(), right_leaves.end());

        return left_leaves;
    }

    int ans = 0;
    int dist = 0;
};
