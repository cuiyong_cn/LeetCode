/**
 * Sort Array By Parity
 *
 * Given an array A of non-negative integers, return an array consisting of
 * all the even elements of A, followed by all the odd elements of A.
 * 
 * You may return any answer array that satisfies this condition.
 * 
 * Example 1:
 * 
 * Input: [3,1,2,4]
 * Output: [2,4,3,1]
 * The outputs [4,2,3,1], [2,4,1,3], and [4,2,1,3] would also be accepted.
 * 
 * Note:
 * 
 *     1. 1 <= A.length <= 5000
 *     2. 0 <= A[i] <= 5000
 */ 
/**
 * One pass solution. Time O(n), space O(1)
 *
 * class Solution {
 * public:
 *     vector<int> sortArrayByParity(vector<int>& A) {
 *         int i = 0, j = A.size() - 1;
 *         for (; i < j;) {
 *             if (A[i] % 2 > A[j] % 2) {
 *                 swap(A[i], A[j]);
 *             }
 *             
 *             if (A[i] % 2 == 0) ++i;
 *             if (A[j] % 2 == 1) --j;
 *         }
 *         
 *         return A;
 *     }
 * };
 *
 * Another one pass Solution, faster than first one.
 * Modulor operator can be replaced by (A[j] & 1) == 0
 * Can be more efficient.
 *
 * class Solution {
 * public:
 *     vector<int> sortArrayByParity(vector<int>& A) {
 *         for (int i =0, j = 0; j < A.size(); ++j) {
 *             if (A[j] % 2 ==0) swap(A[i++], A[j]);
 *
 *         }
 *         
 *         return A;
 *     }
 * };
 *
 * Use the standard algorithm.
 * Ref: https://en.cppreference.com/w/cpp/algorithm/partition
 *
 * class Solution {
 *      vector<int> sortArrayByParity(vector<int>& A) {
 *          auto is_even = [] (auto e) { return e % 2 == 0; };
 *          partition (A.begin (), A.end (), is_even);
 *          return A;
 *      }
 * };
 *
 * Two pass solution. Time O(n), space O(n)
 */
class Solution {
public:
    vector<int> sortArrayByParity(vector<int>& A) {
        vector<int> v;
        v.reserve(A.size());
        for (auto i : A) {
            if (i % 2 == 0) {
                v.push_back(i);
            }
        }
        
        for (auto i : A) {
            if (i % 2 != 0) {
                v.push_back(i);
            }
        }
        
        return v;
    }
};
