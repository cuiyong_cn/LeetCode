/**
 * Swap For Longest Repeated Character Substring
 *
 * Given a string text, we are allowed to swap two of the characters in the string. Find the length of the longest
 * substring with repeated characters.
 *
 * Example 1:
 *
 * Input: text = "ababa"
 * Output: 3
 * Explanation: We can swap the first 'b' with the last 'a', or the last 'b' with the first 'a'. Then, the longest
 * repeated character substring is "aaa", which its length is 3.
 *
 * Example 2:
 *
 * Input: text = "aaabaaa"
 * Output: 6
 * Explanation: Swap 'b' with the last 'a' (or the first 'a'), and we get longest repeated character substring "aaaaaa",
 * which its length is 6.
 *
 * Example 3:
 *
 * Input: text = "aaabbaaa"
 * Output: 4
 *
 * Example 4:
 *
 * Input: text = "aaaaa"
 * Output: 5
 * Explanation: No need to swap, longest repeated character substring is "aaaaa", length is 5.
 *
 * Example 5:
 *
 * Input: text = "abcdef"
 * Output: 1
 *
 * Constraints:
 *     1 <= text.length <= 20000
 *     text consist of lowercase English characters only.
 */
/**
 * Sliding window solution from leetcode user votrubac
 */
class Solution
{
public:
    int maxRepOpt1(string str, int res = 0)
    {
        for (auto ch = 'a'; ch <= 'z'; ++ch) {
            size_t i = 0;
            size_t j = 0;
            int gap = 0;
            while (i < str.size()) {
                gap += str[i++] != ch;
                if (gap > 1) {
                    gap -= str[j++] != ch;
                }
            }

            res = max(res, min(i - j,
                               count_if(begin(str), end(str), 
                                   [&](char ch1) {
                                       return ch1 == ch;
                                   }
                                       )
                              )
                     );
        }
        return res;
    }
};

/**
 * Origninal solution.
 */
struct CharBlock {
    size_t start;
    size_t end;
};

class Solution
{
public:
    int maxRepOpt1(string text)
    {
        vector<vector<CharBlock>> char_blocks(26);

        size_t start = 0;
        for (size_t i = 1; i < text.length(); ++i) {
            if (text[i] != text[i - 1]) {
                char_blocks[text[i - 1] - 'a'].push_back({start, i});
                start = i;
            }
        }

        char_blocks[text.back() - 'a'].push_back({start, text.length()});

        int ans = 1;
        for (auto& blocks : char_blocks) {
            if (!blocks.empty()) {
                int prev_len = blocks[0].end - blocks[0].start;
                int longest = prev_len + (blocks.size() > 1 ? 1 : 0);
                for (size_t i = 1; i < blocks.size(); ++i) {
                    int len = blocks[i].end - blocks[i].start;
                    int extend = 1;

                    if (1 == (blocks[i].start - blocks[i - 1].end)) {
                        if (blocks.size() > 2) {
                            extend = prev_len + 1;
                        } else {
                            extend = prev_len;
                        }
                    }

                    longest = std::max(longest, len + extend);
                    prev_len = len;
                }

                ans = std::max(ans, longest);
            }
        }

        return ans;
    }
};



