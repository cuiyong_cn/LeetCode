/**
 * Distant Barcodes
 *
 * In a warehouse, there is a row of barcodes, where the ith barcode is barcodes[i].
 * 
 * Rearrange the barcodes so that no two adjacent barcodes are equal. You may return any answer, and it is guaranteed an answer exists.
 * 
 *  
 * 
 * Example 1:
 * 
 * Input: barcodes = [1,1,1,2,2,2]
 * Output: [2,1,2,1,2,1]
 * Example 2:
 * 
 * Input: barcodes = [1,1,1,1,2,2,3,3]
 * Output: [1,3,1,3,1,2,1,2]
 *  
 * 
 * Constraints:
 * 
 * 1 <= barcodes.length <= 10000
 * 1 <= barcodes[i] <= 10000
 */
/**
 * actually we don't need the priority queue
 */
class Solution {
public:
    vector<int> rearrangeBarcodes(vector<int>& barcodes) {
        unordered_map<int, int> codes_count;
        for (auto const bc : barcodes) {
            ++codes_count[bc];
        }

        int fmax = 0;
        int fmax_code = 0;
        for (auto const cc : codes_count) {
            if (cc.second > fmax) {
                fmax = cc.second;
                fmax_code = cc.first;
            }
        }

        int idx = 0;
        vector<int> ans(barcodes.size(), 0);
        for (int i = 0; i < fmax; ++i, idx += 2) {
            ans[idx] = fmax_code;
        }

        codes_count.erase(fmax_code);
        for (auto const cc : codes_count) {
            for (int i = 0; i < cc.second; i++, idx += 2) {
                if (idx >= barcodes.size()) {
                    idx = 1;
                }

                ans[idx] = cc.first;
            }
        }
        return ans;
    }
};

/**
 * Improve the below code.
 */
class Solution {
public:
    vector<int> rearrangeBarcodes(vector<int>& barcodes) {
        std::unordered_map<int, int> codes_count;

        for (auto const n : barcodes) {
            ++codes_count[n];
        }

        std::priority_queue<std::pair<int,int>> pq;
        for (auto const cc : codes_count) {
            pq.push({cc.second, cc.first});
        }

        vector<int> ans(barcodes.size(), 0);
        int idx = 0;
        while (!pq.empty()) {
            auto cc = pq.top(); pq.pop();
            for (int i = 0; i < cc.first; ++i) {
                if (idx >= ans.size()) {
                    idx = 1;
                }
                ans[idx] = cc.second;
                idx += 2;
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<int> rearrangeBarcodes(vector<int>& barcodes) {
        std::unordered_map<int, int> codes_count;

        for (auto const n : barcodes) {
            ++codes_count[n];
        }

        std::priority_queue<std::pair<int,int>> pq;
        for (auto const cc : codes_count) {
            pq.push({cc.second, cc.first});
        }

        vector<int> ans;
        while (!pq.empty()) {
            auto cc1 = pq.top(); pq.pop();
            ans.push_back(cc1.second);
            if (!pq.empty()) {
                auto cc2 = pq.top(); pq.pop();
                ans.push_back(cc2.second);
                if (cc2.first > 1) {
                    pq.push({cc2.first - 1, cc2.second});
                }
            }

            if (cc1.first > 1) {
                pq.push({cc1.first - 1, cc1.second});
            }
        }

        return ans;
    }
};
