/**
 * Swapping Nodes in a Linked List
 *
 * You are given the head of a linked list, and an integer k.
 *
 * Return the head of the linked list after swapping the values of the kth node from the beginning
 * and the kth node from the end (the list is 1-indexed).
 *
 * Example 1:
 *
 * Input: head = [1,2,3,4,5], k = 2
 * Output: [1,4,3,2,5]
 * Example 2:
 *
 * Input: head = [7,9,6,6,7,8,3,0,9,5], k = 5
 * Output: [7,9,6,6,8,7,3,0,9,5]
 *
 * Constraints:
 *
 * The number of nodes in the list is n.
 * 1 <= k <= n <= 105
 * 0 <= Node.val <= 100
 */
/**
 * Make it more compact without consider edge cases
 */
class Solution {
public:
    ListNode* swapNodes(ListNode* head, int k) {
        ListNode **a = &head;
        while (--k) { a = &(*a)->next; }

        ListNode **b = &head, **x = &(*a)->next;

        while (*x) { x = &(*x)->next; b = &(*b)->next; }

        swap(*a, *b);
        swap((*a)->next, (*b)->next);

        return head;
    }
};

/**
 * Without additional space.
 */
class Solution {
public:
    ListNode* swapNodes(ListNode* head, int k) {
        ListNode dummy;
        dummy.next = head;
        int len = 0;
        auto firstPrev = &dummy;

        for (int i = 1; i < k; i++) {
            firstPrev = firstPrev->next;
            len++;
        }

        auto first = firstPrev->next;
        auto secondPrev = &dummy;
        auto fast = firstPrev->next;
        len++;

        while (nullptr != fast->next) {
            fast = fast->next;
            secondPrev = secondPrev->next;
            len++;
        }

        auto second = secondPrev->next;
        auto secondNext = second->next;
        auto firstNext = first->next;

        if (k > len / 2) {
            auto temp = first;
            first = second;
            second = temp;

            temp = firstPrev;
            firstPrev = secondPrev;
            secondPrev = temp;

            temp = firstNext;
            firstNext = secondNext;
            secondNext = temp;
        }

        if (firstNext == second) {
            firstPrev->next = second;
            second->next = first;
            first->next = secondNext;
        } else {
            firstPrev->next = second;
            second->next = firstNext;
            secondPrev->next = first;
            first->next = secondNext;
        }

        return dummy.next;
    }
};

/**
 * Original solution.
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* swapNodes(ListNode* head, int k) {
        vector<ListNode*> q;
        int len = 0;
        for (auto n = head; nullptr != n; n = n->next, ++len) {
            q.emplace_back(n);
        }

        swap(q[k - 1], q[len - k]);

        ListNode dummy;
        auto new_head = &dummy;
        for (auto n : q) {
            new_head->next = n;
            new_head = n;
        }
        new_head->next = nullptr;

        return dummy.next;
    }
};
