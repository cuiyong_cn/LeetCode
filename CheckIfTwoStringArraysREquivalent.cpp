/**
 * Check If Two String Arrays are Equivalent
 *
 * Given two string arrays word1 and word2, return true if the two arrays represent the same string,
 * and false otherwise.
 *
 * A string is represented by an array if the array elements concatenated in order forms the string.
 *
 * Example 1:
 *
 * Input: word1 = ["ab", "c"], word2 = ["a", "bc"]
 * Output: true
 * Explanation:
 * word1 represents string "ab" + "c" -> "abc"
 * word2 represents string "a" + "bc" -> "abc"
 * The strings are the same, so return true.
 * Example 2:
 *
 * Input: word1 = ["a", "cb"], word2 = ["ab", "c"]
 * Output: false
 * Example 3:
 *
 * Input: word1  = ["abc", "d", "defg"], word2 = ["abcddefg"]
 * Output: true
 *
 * Constraints:
 *
 * 1 <= word1.length, word2.length <= 103
 * 1 <= word1[i].length, word2[i].length <= 103
 * 1 <= sum(word1[i].length), sum(word2[i].length) <= 103
 * word1[i] and word2[i] consist of lowercase letters.
 */
/**
 * Original solution.
 */
class Solution {
public:
    bool arrayStringsAreEqual(vector<string>& word1, vector<string>& word2) {
        int const w1_n = word1.size();
        int const w2_n = word2.size();
        int w1_idx = 0, w2_idx = 0;
        int w1_i = 0, w1_len = 0;
        int w2_i = 0, w2_len = 0;

        while (w1_idx < w1_n && w2_idx < w2_n) {
            if (w1_i == w1_len) {
                w1_i = 0;
                w1_len = word1[w1_idx].length();
            }

            if (w2_i == w2_len) {
                w2_i = 0;
                w2_len = word2[w2_idx].length();
            }

            auto const& str1 = word1[w1_idx];
            auto const& str2 = word2[w2_idx];
            for (; w1_i < w1_len && w2_i < w2_len; ++w1_i, ++w2_i) {
                if (str1[w1_i] != str2[w2_i]) {
                    return false;
                }
            }

            w1_idx += w1_i == w1_len;
            w2_idx += w2_i == w2_len;
        }

        return w1_n == w1_idx && w2_n == w2_idx
            && w1_i == w1_len && w2_i == w2_len;
    }
};
