/**
 * Minimum Moves to Spread Stones Over Grid
 *
 * You are given a 0-indexed 2D integer matrix grid of size 3 * 3, representing the number of stones
 * in each cell. The grid contains exactly 9 stones, and there can be multiple stones in a single
 * cell.
 *
 * In one move, you can move a single stone from its current cell to any other cell if the two cells share a side.
 *
 * Return the minimum number of moves required to place one stone in each cell.
 *
 * Example 1:
 *
 * Input: grid = [[1,1,0],[1,1,1],[1,2,1]]
 * Output: 3
 * Explanation: One possible sequence of moves to place one stone in each cell is:
 * 1- Move one stone from cell (2,1) to cell (2,2).
 * 2- Move one stone from cell (2,2) to cell (1,2).
 * 3- Move one stone from cell (1,2) to cell (0,2).
 * In total, it takes 3 moves to place one stone in each cell of the grid.
 * It can be shown that 3 is the minimum number of moves required to place one stone in each cell.
 *
 * Example 2:
 *
 * Input: grid = [[1,3,0],[1,0,0],[1,0,3]]
 * Output: 4
 * Explanation: One possible sequence of moves to place one stone in each cell is:
 * 1- Move one stone from cell (0,1) to cell (0,2).
 * 2- Move one stone from cell (0,1) to cell (1,1).
 * 3- Move one stone from cell (2,2) to cell (1,2).
 * 4- Move one stone from cell (2,2) to cell (2,1).
 * In total, it takes 4 moves to place one stone in each cell of the grid.
 * It can be shown that 4 is the minimum number of moves required to place one stone in each cell.
 *
 * Constraints:
 *     grid.length == grid[i].length == 3
 *     0 <= grid[i][j] <= 9
 *     Sum of grid is equal to 9.
 */
/**
 * Original solution.
 */
vector<int> flat(vector<vector<int>> const& grid)
{
    auto g = vector<int>{};

    for (auto& row : grid) {
        for (auto n : row) {
            g.emplace_back(n);
        }
    }

    return g;
}

int distance(int r, int s)
{
    auto rx = r / 3;
    auto ry = r - r / 3 * 3;
    auto sx = s / 3;
    auto sy = s - s / 3 * 3;

    return abs(rx - sx) + abs(ry - sy);
}

class Solution {
public:
    int minimumMoves(vector<vector<int>>& grid)
    {
        auto g = flat(grid);

        for (auto i = 0; i < 9; ++i) {
            if (0 == g[i]) {
                m_recv.push_back(i);
            }
            else if (g[i] > 1) {
                m_send.push_back(i);
            }
        }

        dfs(g, 0);

        return m_moves;
    }

private:
    void dfs(vector<int>& g, int moves)
    {
        if (9 == count(g.begin(), g.end(), 1)) {
            m_moves = min(m_moves, moves);
            return;
        }

        for (auto r : m_recv) {
            if (0 == g[r]) {
                ++g[r];

                for (auto s : m_send) {
                    if (g[s] > 1) {
                        --g[s];
                        dfs(g, moves + distance(r, s));
                        ++g[s];
                    }
                }

                --g[r];
            }
        }
    }

    int m_moves = 100;
    vector<int> m_recv;
    vector<int> m_send;
};
