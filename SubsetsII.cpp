/**
 * Subsets II
 *
 * Given a collection of integers that might contain duplicates, nums, return all possible subsets (the power set).
 *
 * Note: The solution set must not contain duplicate subsets.
 *
 * Example:
 *
 * Input: [1,2,2]
 * Output:
 * [
 *   [2],
 *   [1],
 *   [1,2,2],
 *   [2,2],
 *   [1,2],
 *   []
 * ]
 */
/**
 * Same iterative version. but different thinking.
 */
class Solution {
public:
    vector<vector<int> > subsetsWithDup(vector<int> &S) {
        sort(S.begin(), S.end());
        vector<vector<int>> ret = {{}};
        int size = 0, startIndex = 0;
        for (int i = 0; i < S.size(); i++) {
            startIndex = i >= 1 && S[i] == S[i - 1] ? size : 0;
            size = ret.size();
            for (int j = startIndex; j < size; j++) {
                vector<int> temp = ret[j];
                temp.push_back(S[i]);
                ret.push_back(temp);
            }
        }
        return ret;
    }
};

/**
 * Iterative version.
 */
class Solution {
public:
    vector<vector<int> > subsetsWithDup(vector<int> &S) {
        vector<vector<int> > totalset = {{}};
        sort(S.begin(),S.end());

        for (int i=0; i<S.size();) {
            int count = 0; // num of elements are the same
            while ((count + i) < S.size() && S[count + i] == S[i]) {
                count++;
            }

            int previousN = totalset.size();
            for(int k=0; k<previousN; k++){
                vector<int> instance = totalset[k];
                for (int j=0; j<count; j++) {
                    instance.push_back(S[i]);
                    totalset.push_back(instance);
                }
            }
            i += count;
        }

        return totalset;
    }
};

/**
 * We can get rid of the memo.
 */
class Solution {
public:
    std::vector<std::vector<int> > subsetsWithDup(std::vector<int> &nums) {
		std::sort(nums.begin(), nums.end());
        std::vector<std::vector<int> > res;
		std::vector<int> vec;

		subsetsWithDup(res, nums, vec, 0);

		return res;
    }
private:
	void subsetsWithDup(std::vector<std::vector<int> > &res, std::vector<int> &nums, std::vector<int> &vec, int begin) {
		res.push_back(vec);
        for (int i = begin; i != nums.size(); ++i) {
			if (i == begin || nums[i] != nums[i - 1]) {
				vec.push_back(nums[i]);
				subsetsWithDup(res, nums, vec, i + 1);
				vec.pop_back();
			}
        }
	}
};

/**
 * Original solution.
 * At first, I did not sort the nums. Then one of the test case failed. [4,4,4,1,4]
 * err..., different subset contains same elements considered as duplicate. order matters.
 * So sort the nums. I expected TLE but it is not which suprises me?!
 */
class Solution {
public:
    vector<vector<int>> subsetsWithDup(vector<int>& nums) {
        vector<int> sub;
        sort(nums.begin(), nums.end());
        dfs(nums, 0, "", sub);

        return ans;
    }

private:
    void dfs(const vector<int>& nums, int i, string serial, vector<int>& sub)
    {
        if (i == nums.size()) {
            if (!memo.count(serial)) {
                memo.insert(serial);
                ans.push_back(sub);
            }
        }
        else {
            dfs(nums, i + 1, serial, sub);
            sub.push_back(nums[i]);
            dfs(nums, i + 1, serial + to_string(nums[i]), sub);
            sub.pop_back();
        }
    }
    unordered_set<string> memo;
    vector<vector<int>> ans;
};
