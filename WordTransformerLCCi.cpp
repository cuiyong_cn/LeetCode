/**
 * Word Transformer LCCI
 *
 * Given two words of equal length that are in a dictionary, write a method to transform one word into another word by
 * changing only one letter at a time. The new word you get in each step must be in the dictionary.
 *
 * Write code to return a possible transforming sequence. If there are more that one sequence, any one is ok.
 *
 * Example 1:
 *
 * Input: beginWord = "hit", endWord = "cog", wordList = ["hot","dot","dog","lot","log","cog"]
 *
 * Output: ["hit","hot","dot","lot","log","cog"]
 *
 * Example 2:
 *
 * Input: beginWord = "hit" endWord = "cog" wordList = ["hot","dot","dog","lot","log"]
 *
 * Output: []
 *
 * Explanation: endWord "cog" is not in the dictionary, so there's no possible transforming sequence.
 */
/**
 * Map is better than set.
 */
class Solution {
public:
    vector<string> findLadders(string beginWord, string endWord, vector<string>& wordList) {
        unordered_map<string, vector<string>> path = {{beginWord, {beginWord}}, {endWord, {endWord}}};
        unordered_map<string, bool> sq = {{beginWord, true}}, eq = {{endWord, true}};
        unordered_map<string, bool> wordDict;

        for (auto& s : wordList) wordDict[s] = true;

        if (wordDict.find(endWord) == wordDict.end()) return {};

        bool isBackWord = false;

        while (sq.size() > 0 && eq.size() > 0) {
            if (sq.size() > eq.size()) {
                swap(sq, eq);
                isBackWord = !isBackWord;
            }

            unordered_map<string, bool> tq;
            for (auto& kv : sq) {
                string word = kv.first;
                string oldWord = word;
                for (int i = 0; i < word.length(); ++i) {
                    char old = word[i];
                    for (char j = 'a'; j <= 'z'; ++j) {
                        word[i] = j;
                        if (eq.find(word) != eq.end()) {
                            vector<string> ans;
                            if (isBackWord) {
                                ans.insert(ans.begin(), path[word].begin(), path[word].end());
                                ans.insert(ans.end(), path[oldWord].begin(), path[oldWord].end());
                            }
                            else {
                                ans.insert(ans.begin(), path[oldWord].begin(), path[oldWord].end());
                                ans.insert(ans.end(), path[word].begin(), path[word].end());
                            }
                            return ans;
                        }

                        bool inDict = wordDict.find(word) != wordDict.end();
                        if (j != old && inDict && path[word].size() == 0) {
                            auto& vp = path[word];
                            if (isBackWord) {
                                vp.push_back(word);
                                vp.insert(vp.end(), path[oldWord].begin(), path[oldWord].end());
                            }
                            else {
                                vp.insert(vp.begin(), path[oldWord].begin(), path[oldWord].end());
                                vp.push_back(word);
                            }
                            tq[word] = true;
                        }
                    }
                    word[i] = old;
                }
            }
            sq = std::move(tq);
        }

        return {};
    }
};

/**
 * Transform the go solution into c++ solution. go solution run 3 times faster, memory 3 times less.
 */
class Solution {
public:
    vector<string> findLadders(string beginWord, string endWord, vector<string>& wordList) {
        unordered_map<string, vector<string>> path = {{beginWord, {beginWord}}, {endWord, {endWord}}};
        unordered_set<string> sq = {beginWord}, eq = {endWord};
        unordered_set<string> wordDict;

        wordDict.insert(wordList.begin(), wordList.end());

        if (wordDict.count(endWord) == 0) return {};

        bool isBackWord = false;

        while (sq.size() > 0 && eq.size() > 0) {
            if (sq.size() > eq.size()) {
                swap(sq, eq);
                isBackWord = !isBackWord;
            }

            unordered_set<string> tq;
            for (auto& pre : sq) {
                string cur = pre;
                for (int i = 0; i < pre.length(); ++i) {
                    char old = cur[i];
                    for (char j = 'a'; j <= 'z'; ++j) {
                        cur[i] = j;
                        auto& cp = path[cur];
                        auto& pp = path[pre];
                        if (eq.count(cur)) {
                            vector<string> ans;
                            if (isBackWord) {
                                ans.insert(ans.begin(), cp.begin(), cp.end());
                                ans.insert(ans.end(), pp.begin(), pp.end());
                            }
                            else {
                                ans.insert(ans.begin(), pp.begin(), pp.end());
                                ans.insert(ans.end(), cp.begin(), cp.end());
                            }
                            return ans;
                        }

                        bool inDict = wordDict.count(cur);
                        if (j != old && inDict && cp.empty()) {
                            if (isBackWord) {
                                cp.push_back(cur);
                                cp.insert(cp.end(), pp.begin(), pp.end());
                            }
                            else {
                                cp.insert(cp.begin(), pp.begin(), pp.end());
                                cp.push_back(cur);
                            }
                            tq.insert(cur);
                        }
                    }
                    cur[i] = old;
                }
            }
            sq = std::move(tq);
        }

        return {};
    }
};

/**
 * Original solution. TLE
 */
class Solution {
public:
    vector<string> findLadders(string beginWord, string endWord, vector<string>& wordList) {
        if (beginWord.length() == endWord.length()) {
            for (auto& s : wordList) {
                memo[s] = s.length() == beginWord.length() ? 0 : -1;
            }

            if (memo.find(endWord) != memo.end()) {
                wlist = &wordList;
                ans.push_back(beginWord);
                dfs(beginWord, endWord);
            }
        }

        return ans.size() == 1 ? vector<string>{} : ans;
    }

private:
    int distance(const string& s1, const string& s2)
    {
        int d = 0;

        if (s1.length() == s2.length()) {
            for (int i = 0; i < s1.length(); ++i) {
                d += s1[i] != s2[i] ? 1 : 0;
            }
        }
        return d;
    }

    bool dfs(const string& bw, const string& ew)
    {
        bool found = false;
        if (bw == ew) {
            found = true;
        }
        else {
            for (auto& s : *wlist) {
                int status = memo[s];
                if (status == 1 || status == -1) continue;
                if (distance(s, bw) == 1) {
                    ans.push_back(s);
                    memo[s] = 1;
                    found = dfs(s, ew);
                    if (found) break;
                    memo[s] = -1;
                    ans.erase(ans.end() - 1);
                }
            }
        }

        return found;
    }
private:
    unordered_map<string, int> memo;
    vector<string>* wlist;

    vector<string> ans;
};
