/**
 * Check if Every Row and Column Contains All Numbers
 *
 * An n x n matrix is valid if every row and every column contains all the integers from 1 to n (inclusive).
 *
 * Given an n x n integer matrix matrix, return true if the matrix is valid. Otherwise, return false.
 *
 * Example 1:
 *
 * Input: matrix = [[1,2,3],[3,1,2],[2,3,1]]
 * Output: true
 * Explanation: In this case, n = 3, and every row and column contains the numbers 1, 2, and 3.
 * Hence, we return true.
 * Example 2:
 *
 * Input: matrix = [[1,1,1],[1,2,3],[1,2,3]]
 * Output: false
 * Explanation: In this case, n = 3, but the first row and the first column do not contain the numbers 2 or 3.
 * Hence, we return false.
 *
 * Constraints:
 *
 * n == matrix.length == matrix[i].length
 * 1 <= n <= 100
 * 1 <= matrix[i][j] <= n
 */
/**
 * Original solution.
 */
class Solution {
public:
    bool checkValid(vector<vector<int>>& matrix) {
        auto n = 1 * matrix.size();
        auto zero = [](auto const& c) { return '0' == c; };
        for (auto const& row : matrix) {
            auto numset = string(n, '0');
            for (auto const& e : row) {
                numset[e - 1] = '1';
            }
            if (any_of(begin(numset), end(numset), zero)) {
                return false;
            }
        }

        for (auto c = 0 * n; c < n; ++c) {
            auto numset = string(n, '0');
            for (auto r = 0 * n; r < n; ++r) {
                numset[matrix[r][c] - 1] = '1';
            }
            if (any_of(begin(numset), end(numset), zero)) {
                return false;
            }
        }

        return true;
    }
};
