/**
 * Unique Binary Search Trees II
 *
 * Given an integer n, return all the structurally unique BST's (binary search trees), which has
 * exactly n nodes of unique values from 1 to n. Return the answer in any order.
 *
 * Example 1:
 *
 * Input: n = 3
 * Output: [[1,null,2,null,3],[1,null,3,2],[2,1,3],[3,1,null,null,2],[3,2,null,1]]
 * Example 2:
 *
 * Input: n = 1
 * Output: [[1]]
 *
 * Constraints:
 *
 * 1 <= n <= 8
 */
/**
 * More efficient and more clear one. Recursively select the break point.
 */
class Solution {
public:
    vector<TreeNode*> generateTree(int from, int to)
    {
        if (to < from) {
            return {nullptr};
        }

        if (to == from) {
            return {new TreeNode{from}};
        }

        vector<TreeNode*> result;
        for (int i= from; i <= to; i++) {
            vector<TreeNode*> left = generateTree(from, i - 1);
            vector<TreeNode*> right = generateTree(i + 1, to);

            for(auto L : left) {
                for (auto R : right) {
                    TreeNode* root = new TreeNode (i);
                    root->left = L;
                    root->right = R;
                    result.emplace_back(root);
                }
            }
        }

        return result;
    }

    vector<TreeNode*> generateTrees(int n) {
        if (n <= 0) {
            return {};
        }

        return generateTree(1, n);
    }
};

/**
 * Original solution. Add one node at a time.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<TreeNode*> generateTrees(int n) {
        vector<pair<TreeNode*, int>> q;
        for (int i = 1; i <= n; ++i) {
            q.push_back({new TreeNode{i}, 1 << (i - 1)});
        }

        for (int i = 1; i < n; ++i) {
            vector<pair<TreeNode*, int>> nq;
            while (!q.empty()) {
                auto tree = q.back(); q.pop_back();
                auto root = tree.first;
                auto seen = tree.second;

                for (int j = 1; j <= n; ++j) {
                    if (!(seen & (1 << (j - 1)))) {
                        auto new_root = deep_clone(root);
                        insert(new_root, j);
                        nq.push_back({new_root, seen | (1 << (j - 1))});
                    }
                }

                destroy_tree(root);
            }

            swap(nq, q);
        }

        vector<TreeNode*> ans;
        unordered_set<string> seen;
        for (auto const& p : q) {
            auto hash_str = hash(p.first);
            if (!seen.count(hash_str)) {
                ans.push_back(p.first);
                seen.insert(hash_str);
            }
            else {
                destroy_tree(p.first);
            }
        }

        return ans;
    }

private:
    TreeNode* deep_clone(TreeNode* root)
    {
        if (!root) {
            return nullptr;
        }

        TreeNode* node = new TreeNode{root->val};
        node->left = deep_clone(root->left);
        node->right = deep_clone(root->right);
        return node;
    }

    TreeNode* insert(TreeNode* root, int value)
    {
        if (!root) {
            return new TreeNode(value);
        }

        if (value < root->val) {
            root->left = insert(root->left, value);
        }
        else {
            root->right = insert(root->right, value);
        }

        return root;
    }

    void destroy_tree(TreeNode* root)
    {
        if (!root) {
            return;
        }

        destroy_tree(root->left);
        destroy_tree(root->right);
        delete root;
    }

    string hash(TreeNode* root)
    {
        if (!root) {
            return "#";
        }

        return hash(root->left) + hash(root->right) + to_string(root->val);
    }
};
