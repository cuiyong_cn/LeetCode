/**
 * Ways to Split Array Into Three Subarrays
 *
 * A split of an integer array is good if:
 *
 * The array is split into three non-empty contiguous subarrays - named left, mid, right
 * respectively from left to right.  The sum of the elements in left is less than or equal to the
 * sum of the elements in mid, and the sum of the elements in mid is less than or equal to the sum
 * of the elements in right.  Given nums, an array of non-negative integers, return the number of
 * good ways to split nums. As the number may be too large, return it modulo 109 + 7.
 *
 * Example 1:
 *
 * Input: nums = [1,1,1]
 * Output: 1
 * Explanation: The only good way to split nums is [1] [1] [1].
 * Example 2:
 *
 * Input: nums = [1,2,2,2,5,0]
 * Output: 3
 * Explanation: There are three good ways of splitting nums:
 * [1] [2] [2,2,5,0]
 * [1] [2,2] [2,5,0]
 * [1,2] [2,2] [5,0]
 * Example 3:
 *
 * Input: nums = [3,2,1]
 * Output: 0
 * Explanation: There is no good way to split nums.
 *
 * Constraints:
 *
 * 3 <= nums.length <= 105
 * 0 <= nums[i] <= 104
 */
/**
 * Brilliant one.
 */
class Solution {
public:
    int waysToSplit(vector<int>& nums) {
        int res = 0;
        int const N = nums.size();
        int const max_i = N - 2;
        int const max_j = N - 1;
        int const mod = 1'000'000'007;

        partial_sum(begin(nums), end(nums), begin(nums));
        for (int i = 0, j = 0, k = 0; i < max_i; ++i) {
            j = max(i + 1, j);
            while (j < max_j && nums[j] < nums[i] * 2) {
                ++j;
            }

            k = max(j, k);
            while (k < max_j && nums[k] - nums[i] <= nums.back() - nums[k]) {
                ++k;
            }

            res = (res + k - j) % mod;
        }

        return res;
    }
};

/**
 * Original solution. Chatty one.
 */
class Solution {
public:
    int waysToSplit(vector<int>& nums) {
        partial_sum(nums.begin(), nums.end(), nums.begin());

        int const mod = 1'000'000'007;
        int const N = nums.size();
        int ans = 0;

        for (int right = 2; right < N; ++right) {
            int const right_sum = nums.back() - nums[right - 1];
            int left_most_of_left_wall = -1;

            int L = 0, R = right - 2;
            while (L <= R) {
                int const mid = L + (R - L) / 2;
                int const left_sum = nums[mid];
                int const mid_sum = nums[right - 1] - left_sum;
                if (left_sum <= mid_sum && mid_sum <= right_sum) {
                    R = mid - 1;
                    left_most_of_left_wall = mid;
                }
                else {
                    if (left_sum > mid_sum) {
                        R = mid - 1;
                    }
                    else {
                        L = mid + 1;
                    }
                }
            }

            if (left_most_of_left_wall < 0) {
                continue;
            }

            int right_most_of_left_wall = left_most_of_left_wall;
            L = 0, R = right - 2;
            while (L <= R) {
                int const mid = L + (R - L) / 2;
                int const left_sum = nums[mid];
                int const mid_sum = nums[right - 1] - left_sum;
                if (left_sum <= mid_sum && mid_sum <= right_sum) {
                    L = mid + 1;
                    right_most_of_left_wall = mid;
                }
                else {
                    if (left_sum > mid_sum) {
                        R = mid - 1;
                    }
                    else {
                        L = mid + 1;
                    }
                }
            }

            ans += (right_most_of_left_wall - left_most_of_left_wall) + 1;
            ans %= mod;
        }

        return ans;
    }
};
