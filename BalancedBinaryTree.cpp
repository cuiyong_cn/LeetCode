/**
 * Balanced Binary Tree
 *
 * Given a binary tree, determine if it is height-balanced.
 *
 * For this problem, a height-balanced binary tree is defined as:
 *
 * a binary tree in which the left and right subtrees of every node differ in height by no more than 1.
 *
 * Example 1:
 *
 * Input: root = [3,9,20,null,null,15,7]
 * Output: true
 * Example 2:
 *
 * Input: root = [1,2,2,3,3,null,null,4,4]
 * Output: false
 * Example 3:
 *
 * Input: root = []
 * Output: true
 *
 * Constraints:
 *
 * The number of nodes in the tree is in the range [0, 5000].
 * -104 <= Node.val <= 104
 */
/**
 * Original solution. DFS solution.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    bool isBalanced(TreeNode* root) {
        return isTreeBalanced(root).second;
    }

private:
    std::pair<int, bool> isTreeBalanced(TreeNode* node)
    {
        if (!node) {
            return {0, true};
        }

        auto [lh, lb] = isTreeBalanced(node->left);
        auto [rh, rb] = isTreeBalanced(node->right);

        if (!lb || !rb) {
            return {0, false};
        }

        auto dh = lh - rh;
        if (dh < -1 || dh > 1) {
            return {0, false};
        }

        return {std::max(lh, rh) + 1, true};
    }
};
