/**
 * Find the Longest Substring Containing Vowels in Even Counts
 *
 * Given the string s, return the size of the longest substring containing each vowel an even number
 * of times. That is, 'a', 'e', 'i', 'o', and 'u' must appear an even number of times.
 *
 * Example 1:
 *
 * Input: s = "eleetminicoworoep"
 * Output: 13
 * Explanation: The longest substring is "leetminicowor" which contains two each of the vowels: e, i
 * and o and zero of the vowels: a and u.
 * Example 2:
 *
 * Input: s = "leetcodeisgreat"
 * Output: 5
 * Explanation: The longest substring is "leetc" which contains two e's.
 * Example 3:
 *
 * Input: s = "bcbcbc"
 * Output: 6
 * Explanation: In this case, the given string "bcbcbc" is the longest because all vowels: a, e, i,
 * o and u appear zero times.
 *
 * Constraints:
 *
 * 1 <= s.length <= 5 x 10^5
 * s contains only lowercase English letters.
 */
/**
 * More efficient one.
 */
class Solution {
public:
    static constexpr int c_m[26] = {1,0,0,0,2,0,0,0,4,0,0,0,0,0,8,0,0,0,0,0,16,0,0,0,0,0};
    int findTheLongestSubstring(string s) {
        int mask = 0, res = 0;
        vector<int> m(32, -1);
        for (int i = 0; i < s.size(); ++i) {
            mask ^= c_m[s[i] - 'a'];
            if (mask != 0 && m[mask] == -1) {
                m[mask] = i;
            }

            res = max(res, i - m[mask]);
        }
        return res;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int findTheLongestSubstring(string s) {
        byte const byte_zero{0};
        array<int, 26> vowel_shifts = { 1, 0, 0, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 5, };
        vector<byte> vowel_mask(s.length() + 1, byte_zero);
        for (int i = 0; i < s.length(); ++i) {
            if (vowel_shifts[s[i] - 'a'] > 0) {
                vowel_mask[i + 1] = (byte{1} << vowel_shifts[s[i] - 'a']) ^ vowel_mask[i];
            }
            else {
                vowel_mask[i + 1] = vowel_mask[i];
            }
        }

        for (int L = s.length(); L > 0; --L) {
            for (int i = 0; (i + L) <= s.length(); ++i) {
                if (vowel_mask[i] == vowel_mask[i + L]) {
                    return L;
                }
            }
        }

        return 0;
    }
};
