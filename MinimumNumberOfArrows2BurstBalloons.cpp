/**
 * Minimum Number of Arrows to Burst Balloons
 *
 * There are a number of spherical balloons spread in two-dimensional space. For each balloon, provided input is the
 * start and end coordinates of the horizontal diameter. Since it's horizontal, y-coordinates don't matter and hence the
 * x-coordinates of start and end of the diameter suffice. Start is always smaller than end. There will be at most 104
 * balloons.
 *
 * An arrow can be shot up exactly vertically from different points along the x-axis. A balloon with xstart and xend
 * bursts by an arrow shot at x if xstart ≤ x ≤ xend. There is no limit to the number of arrows that can be shot. An
 * arrow once shot keeps travelling up infinitely. The problem is to find the minimum number of arrows that must be shot
 * to burst all balloons.
 *
 * Example:
 *
 * Input:
 * [[10,16], [2,8], [1,6], [7,12]]
 *
 * Output:
 * 2
 *
 * Explanation:
 * One way is to shoot one arrow for example at x = 6 (bursting the balloons [2,8] and [1,6]) and another arrow at x =
 * 11 (bursting the other two balloons).
 */
/**
 * From discussion. Here is more siplified version. The idea is:
 *
 * 1. We know that eventually we have to shoot down every balloon, so for each ballon there must be an arrow whose position
 * is between balloon[0] and balloon[1] inclusively. Given that, we can sort the array of balloons by their ending
 * position. Then we make sure that while we take care of each balloon in order, we can shoot as many following balloons
 * as possible.
 *
 * 2. So what position should we pick each time? We should shoot as to the right as possible, because since balloons are
 * sorted, this gives you the best chance to take down more balloons. Therefore the position should always be
 * balloon[i][1] for the ith balloon.
 *
 * 3. This is exactly what I do in the for loop: check how many balloons I can shoot down with one shot aiming at the
 * ending position of the current balloon. Then I skip all these balloons and start again from the next one (or the
 * leftmost remaining one) that needs another arrow.
 */
class Solution {
public:
    int findMinArrowShots(vector<vector<int>>& points) {
        if (points.size() <= 1) return points.size();

        sort(points.begin(), points.end(),
            [](auto& p1, auto& p2) {
                return p1[1] < p2[1];
            });

        int ans = 1;
        int xe = points[0][1];

        for (size_t i = 1; i < points.size(); ++i) {
            if (xe < points[i][0]) {
                ++ans;
                xe = points[i][1];
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int findMinArrowShots(vector<vector<int>>& points) {
        if (points.size() <= 1) return points.size();

        sort(points.begin(), points.end(),
            [](auto& p1, auto& p2) {
                if (p1[0] < p2[0]) return true;
                if (p1[0] == p2[0]) {
                    if (p1[1] < p2[1]) return true;
                    else return false;
                }

                return false;
            });

        int ans = 1;
        int xs = points[0][0];
        int xe = points[0][1];
        for (size_t i = 1; i < points.size(); ++i) {
            int ns = points[i][0];
            int ne = points[i][1];
            if (xe < ns) {
                ++ans;
                xe = ne;
            }
            else {
                xe = std::min(xe, ne);
            }
            xs = ns;
        }

        return ans;
    }
};
