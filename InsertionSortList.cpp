/** Insertion Sort List
 *
 * Sort a linked list using insertion sort.
 *
 * A graphical example of insertion sort. The partial sorted list (black)
 * initially contains only the first element in the list.  With each iteration
 * one element (red) is removed from the input data and inserted in-place into
 * the sorted list
 *
 * Algorithm of Insertion Sort:
 *
 * Insertion sort iterates, consuming one input element each repetition, and
 * growing a sorted output list.  At each iteration, insertion sort removes one
 * element from the input data, finds the location it belongs within the sorted
 * list, and inserts it there.  It repeats until no input elements remain.
 *
 * Example 1:
 *
 * Input: 4->2->1->3 Output: 1->2->3->4 Example 2:
 * Input: -1->5->3->4->0 Output: -1->0->3->4->5
 */
/**
 * Real solution.
 */
class Solution {
public:
    ListNode* insertionSortList(ListNode* head) {
        if (nullptr == head) {
            return nullptr;
        }

        ListNode* front = head;
        ListNode* tail = front;

        head = head->next;
        while (head) {
            ListNode* next = head->next;
            if (head->val <= front->val) {
                head->next = front;
                front = head;
            }
            else if (head->val >= tail->val) {
                tail->next = head;
                tail = head;
            }
            else {
                ListNode* prev = front;
                while (prev->next && prev->next->val < head->val) {
                    prev = prev->next;
                }

                head->next = prev->next;
                prev->next = head;
            }

            head = next;
        }

        tail->next = nullptr;

        return front;
    }
};

/**
 * Original solution. Cheated.
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* insertionSortList(ListNode* head) {
        if (nullptr == head) {
            return nullptr;
        }

        vector<ListNode*> nodes;
        while (head) {
            nodes.push_back(head);
            head = head->next;
        }

        sort(nodes.begin(), nodes.end(), [](auto a, auto b) {
            return a->val < b->val;
        });

        for (int i = 1; i < nodes.size(); ++i) {
            nodes[i - 1]->next = nodes[i];
        }

        nodes.back()->next = nullptr;
        return nodes.front();
    }
};
