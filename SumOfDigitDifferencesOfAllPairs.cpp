/**
 * Sum of Digit Differences of All Pairs
 *
 * You are given an array nums consisting of positive integers where all integers have the same
 * number of digits.
 *
 * The digit difference between two integers is the count of different digits that are in the same
 * position in the two integers.
 *
 * Return the sum of the digit differences between all pairs of integers in nums.
 *
 * Example 1:
 *
 * Input: nums = [13,23,12]
 *
 * Output: 4
 *
 * Explanation:
 * We have the following:
 * - The digit difference between 13 and 23 is 1.
 * - The digit difference between 13 and 12 is 1.
 * - The digit difference between 23 and 12 is 2.
 * So the total sum of digit differences between all pairs of integers is 1 + 1 + 2 = 4.
 *
 * Example 2:
 *
 * Input: nums = [10,10,10,10]
 *
 * Output: 0
 *
 * Explanation:
 * All the integers in the array are the same. So the total sum of digit differences between all pairs of integers will be 0.
 *
 * Constraints:
 *     2 <= nums.length <= 105
 *     1 <= nums[i] < 109
 *     All integers in nums have the same number of digits.
 */
/**
 * Original solution.
 */
auto count_digits(int n)
{
    auto cnt = 0;

    while (n > 0) {
        n /= 10;
        ++cnt;
    }

    return cnt;
}

class Solution {
public:
    long long sumDigitDifferences(vector<int>& nums) {
        auto ans = 0LL;
        auto const digits = count_digits(nums[0]);
        auto digit_cnt_pos = vector<vector<int>>(digits, vector<int>(10, 0));

        for (auto n : nums) {
            for (auto i = 0; i < digits; ++i) {
                ++digit_cnt_pos[i][n % 10];
                n /= 10;
            }
        }

        auto const total = nums.size();
        for (auto const& cnt : digit_cnt_pos) {
            for (auto const& c : cnt) {
                ans += 1LL * c * (total - c);
            }
        }

        return ans / 2;
    }
};
