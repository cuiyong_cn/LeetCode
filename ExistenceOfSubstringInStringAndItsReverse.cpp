/**
 * Existence of a Substring in a String and Its Reverse
 *
 * Given a string s, find any
 * substring
 * of length 2 which is also present in the reverse of s.
 *
 * Return true if such a substring exists, and false otherwise.
 *
 * Example 1:
 *
 * Input: s = "leetcode"
 *
 * Output: true
 *
 * Explanation: Substring "ee" is of length 2 which is also present in reverse(s) == "edocteel".
 *
 * Example 2:
 *
 * Input: s = "abcba"
 *
 * Output: true
 *
 * Explanation: All of the substrings of length 2 "ab", "bc", "cb", "ba" are also present in reverse(s) == "abcba".
 *
 * Example 3:
 *
 * Input: s = "abcd"
 *
 * Output: false
 *
 * Explanation: There is no substring of length 2 in s, which is also present in the reverse of s.
 *
 * Constraints:
 *     1 <= s.length <= 100
 *     s consists only of lowercase English letters.
 */
/**
 * Original solution.
 */
auto reverse(string s)
{
    reverse(s.begin(), s.end());
    return s;
}

class Solution {
public:
    bool isSubstringPresent(string s) {
        auto rs = reverse(s);
        int const n = s.length();

        for (auto [i, end] = pair(0, n - 1); i < end; ++i) {
            auto sub = s.substr(i, 2);
            if (string::npos != rs.find(sub)) {
                return true;
            }
        }

        return false;
    }
};
