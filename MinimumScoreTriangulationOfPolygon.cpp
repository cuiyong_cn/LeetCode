/**
 * Minimum Score Triangulation of Polygon
 *
 * Given N, consider a convex N-sided polygon with vertices labelled A[0], A[i], ..., A[N-1] in clockwise order.
 *
 * Suppose you triangulate the polygon into N-2 triangles.  For each triangle, the value of that triangle is the product
 * of the labels of the vertices, and the total score of the triangulation is the sum of these values over all N-2
 * triangles in the triangulation.
 *
 * Return the smallest possible total score that you can achieve with some triangulation of the polygon.
 *
 * Example 1:
 *
 * Input: [1,2,3]
 * Output: 6
 * Explanation: The polygon is already triangulated, and the score of the only triangle is 6.
 *
 * Example 2:
 *
 * Input: [3,7,4,5]
 * Output: 144
 * Explanation: There are two triangulations, with possible scores: 3*7*5 + 4*5*7 = 245, or 3*4*5 + 3*4*7 = 144.  The minimum score is 144.
 *
 * Example 3:
 *
 * Input: [1,3,1,4,1,5]
 * Output: 13
 * Explanation: The minimum score triangulation has score 1*1*3 + 1*1*4 + 1*1*5 + 1*1*1 = 13.
 *
 * Note:
 *     3 <= A.length <= 50
 *     1 <= A[i] <= 100
 */
/**
 * Same idea, but bottom up solution.
 */
class Solution
{
public:
    int minScoreTriangulation(vector<int>& A)
    {
        int dp[50][50] = {};

        for (int i = A.size() - 3; i >= 0; --i) {
            for (int j = i + 2; j  < A.size();  ++j) {
                for (int k = i + 1; k < j; ++k) {
                    dp[i][j] = min(dp[i][j] == 0 ? INT_MAX : dp[i][j],
                            dp[i][k] + A[i] * A[k] * A[j] + dp[k][j]);
                }
            }
        }
        return dp[0][A.size() - 1];
    }
};

/**
 * I did not think up a solution here. So from discussion.
 * Refer: https://leetcode.com/problems/minimum-score-triangulation-of-polygon/discuss/286753/C%2B%2B-with-picture
 * PNG: https://assets.leetcode.com/users/votrubac/image_1557470819.png
 */
class Solution
{
public:
    int minScoreTriangulation(vector<int>& A)
    {
        memo = vector<vector<int>>(50, vector<int>(50, 0));

        return helper(A, 0, A.size() - 1);
    }

private:
    int helper(const vector<int>& A, int i, int j)
    {
        if ((j - i) == 1) return 0;

        if (memo[i][j] != 0) {
            return memo[i][j];
        }

        int ans = numeric_limits<int>::max();
        for (auto k = i + 1; k < j; ++k) {
            ans = std::min(ans,
                           helper(A, i, k)
                           + A[i] * A[k] * A[j]
                           + helper(A, k, j));
        }

        memo[i][j] = ans;

        return ans;
    }

    vector<vector<int>> memo;
};

