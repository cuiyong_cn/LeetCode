/**
 * Minimum Average Difference
 *
 * You are given a 0-indexed integer array nums of length n.
 *
 * The average difference of the index i is the absolute difference between the average of the first
 * i + 1 elements of nums and the average of the last n - i - 1 elements. Both averages should be
 * rounded down to the nearest integer.
 *
 * Return the index with the minimum average difference. If there are multiple such indices, return
 * the smallest one.
 *
 * Note:
 *
 * The absolute difference of two numbers is the absolute value of their difference.
 * The average of n elements is the sum of the n elements divided (integer division) by n.
 * The average of 0 elements is considered to be 0.
 *
 * Example 1:
 *
 * Input: nums = [2,5,3,9,5,3]
 * Output: 3
 * Explanation:
 * - The average difference of index 0 is: |2 / 1 - (5 + 3 + 9 + 5 + 3) / 5| = |2 / 1 - 25 / 5| = |2 - 5| = 3.
 * - The average difference of index 1 is: |(2 + 5) / 2 - (3 + 9 + 5 + 3) / 4| = |7 / 2 - 20 / 4| = |3 - 5| = 2.
 * - The average difference of index 2 is: |(2 + 5 + 3) / 3 - (9 + 5 + 3) / 3| = |10 / 3 - 17 / 3| = |3 - 5| = 2.
 * - The average difference of index 3 is: |(2 + 5 + 3 + 9) / 4 - (5 + 3) / 2| = |19 / 4 - 8 / 2| = |4 - 4| = 0.
 * - The average difference of index 4 is: |(2 + 5 + 3 + 9 + 5) / 5 - 3 / 1| = |24 / 5 - 3 / 1| = |4 - 3| = 1.
 * - The average difference of index 5 is: |(2 + 5 + 3 + 9 + 5 + 3) / 6 - 0| = |27 / 6 - 0| = |4 - 0| = 4.
 * The average difference of index 3 is the minimum average difference so return 3.
 * Example 2:
 *
 * Input: nums = [0]
 * Output: 0
 * Explanation:
 * The only index is 0 so return 0.
 * The average difference of index 0 is: |0 / 1 - 0| = |0 - 0| = 0.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 105
 * 0 <= nums[i] <= 105
 */
/**
 * Optimize. O(1) space.
 */
class Solution {
public:
    int minimumAverageDifference(vector<int>& nums) {
        auto sum_back = accumulate(begin(nums), end(nums), 0LL);
        auto sum_front = 0LL;
        auto ans = 0;
        auto n = nums.size();
        auto min_diff = numeric_limits<int>::max();
        for (auto i = 0*n; i < n; ++i) {
            sum_front += nums[i];
            auto avg_front = sum_front / (i + 1);
            auto avg_back = (i + 1) == n ? 0 : (sum_back - sum_front) / (n - i - 1);
            auto diff_avg = abs<int>(avg_front - avg_back);
            if (diff_avg < min_diff) {
                min_diff = diff_avg;
                ans = i;
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int minimumAverageDifference(vector<int>& nums) {
        auto prefix_sum = vector<int64_t>(nums.size(), 0);
        copy(begin(nums), end(nums), begin(prefix_sum));
        partial_sum(begin(prefix_sum), end(prefix_sum), begin(prefix_sum));

        auto ans = 0;
        auto diff = numeric_limits<int>::max();
        auto n = static_cast<int>(nums.size());
        auto sum = prefix_sum.back();
        for (auto i = 0; i < n; ++i) {
            auto avg1 = prefix_sum[i] / (i + 1);
            auto avg2 = (i + 1) == nums.size() ? 0 : (sum - prefix_sum[i]) / (n - i - 1);
            auto d_avg = abs(avg1 - avg2);
            if (d_avg < diff) {
                diff = d_avg;
                ans = i;
            }
        }

        return ans;
    }
};
