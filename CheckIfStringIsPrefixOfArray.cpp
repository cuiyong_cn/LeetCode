/**
 * Check If String Is a Prefix of Array
 *
 * Given a string s and an array of strings words, determine whether s is a prefix string of words.
 *
 * A string s is a prefix string of words if s can be made by concatenating the first k strings in
 * words for some positive k no larger than words.length.
 *
 * Return true if s is a prefix string of words, or false otherwise.
 *
 * Example 1:
 *
 * Input: s = "iloveleetcode", words = ["i","love","leetcode","apples"]
 * Output: true
 * Explanation:
 * s can be made by concatenating "i", "love", and "leetcode" together.
 * Example 2:
 *
 * Input: s = "iloveleetcode", words = ["apples","i","love","leetcode"]
 * Output: false
 * Explanation:
 * It is impossible to make s using a prefix of arr.
 *
 * Constraints:
 *
 * 1 <= words.length <= 100
 * 1 <= words[i].length <= 20
 * 1 <= s.length <= 1000
 * words[i] and s consist of only lowercase English letters.
 */
/**
 * More efficient one.
 */
class Solution {
public:
    bool isPrefixString(string const& s, vector<string>& words) {
        int i = 0;
        int const sl = s.length();

        for (auto const& w : words) {
            if (sl == i) {
                return true;
            }

            if ((sl - i) < w.length()) {
                return false;
            }

            if (0 != s.compare(i, w.length(), w)) {
                return false;
            }

            i += w.length();
        }

        return sl == i;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool isPrefixString(string const& s, vector<string>& words) {
        string prefix;
        unordered_set<string> prefix_sets;

        for (auto const& w : words) {
            prefix += w;
            prefix_sets.insert(prefix);
        }

        return 1 == prefix_sets.count(s);
    }
};
