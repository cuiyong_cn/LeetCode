/**
 * Non-overlapping Intervals
 *
 * Given a collection of intervals, find the minimum number of intervals you need to remove to make
 * the rest of the intervals non-overlapping.
 *
 * Example 1:
 *
 * Input: [[1,2],[2,3],[3,4],[1,3]]
 * Output: 1
 * Explanation: [1,3] can be removed and the rest of intervals are non-overlapping.
 * Example 2:
 *
 * Input: [[1,2],[1,2],[1,2]]
 * Output: 2
 * Explanation: You need to remove two [1,2] to make the rest of intervals non-overlapping.
 * Example 3:
 *
 * Input: [[1,2],[2,3]]
 * Output: 0
 * Explanation: You don't need to remove any of the intervals since they're already non-overlapping.
 *
 *
 * Note:
 *
 * You may assume the interval's end point is always bigger than its start point.
 * Intervals like [1,2] and [2,3] have borders "touching" but they don't overlap each other.
 */
/**
 * Greedy solution from discussion. CLRS theorem.
 */
class Solution {
public:
    int eraseOverlapIntervals(vector<vector<int>>& intervals) {
        if (intervals.size() <= 1) {
            return 0;
        }

        sort(intervals.begin(), intervals.end(),
            [](auto const& a, auto const& b) {
                return a[1] <= b[1];
            });

        int max_non_overlap = 1;
        int end = intervals[0][1];
        for (auto const& inter : intervals) {
            if (inter[0] >= end) {
                end = inter[1];
                ++max_non_overlap;
            }
        }

        return intervals.size() - max_non_overlap;
    }
};

/**
 * First try to speed things up. It improved, but still TLE.
 * This gets me thinking that maybe we should change a course.
 */
class Solution {
public:
    int eraseOverlapIntervals(vector<vector<int>>& intervals) {
        if (intervals.size() <= 1) {
            return 0;
        }

        sort(intervals.begin(), intervals.end(),
            [](auto const& a, auto const& b) {
                if (a[0] < b[0]) {
                    return true;
                }

                if (a[0] == b[0]) {
                    return a[1] <= b[1];
                }

                return false;
            });

        int max_non_overlap = 0;
        max_non_overlap = std::max(max_non_overlap, 1 + dfs(intervals, intervals[0][1], 1));
        max_non_overlap = std::max(max_non_overlap, dfs(intervals, std::numeric_limits<int>::min(), 1));
        return intervals.size() - max_non_overlap;
    }

private:
    int dfs(vector<vector<int>> const& intervals, int end, int i)
    {
        if (i >= intervals.size()) {
            return 0;
        }

        if (intervals[i][0] < end) {
            auto p = std::make_pair(end, 0);
            auto it = cache.find(p);
            if (it != cache.end()) {
                return it->second;
            }

            int count = dfs(intervals, end, i + 1);
            cache[p] = count;
            return count;
        }

        auto p = std::make_pair(end, 1);
        auto it = cache.find(p);
        if (it != cache.end()) {
            return it->second;
        }

        int count = std::max(1 + dfs(intervals, intervals[i][1], i + 1), dfs(intervals, end, i + 1));
        cache[p] = count;
        return count;
    }

    std::map<std::pair<int, int>, int> cache;
};

/**
 * Original solution. Not suprisingly, TLE. And obviously we can use some cache mechanism to speed
 * things up.
 */
class Solution {
public:
    int eraseOverlapIntervals(vector<vector<int>>& intervals) {
        if (intervals.size() <= 1) {
            return 0;
        }

        sort(intervals.begin(), intervals.end(),
            [](auto const& a, auto const& b) {
                if (a[0] < b[0]) {
                    return true;
                }

                if (a[0] == b[0]) {
                    return a[1] <= b[1];
                }

                return false;
            });

        int max_non_overlap = 0;
        max_non_overlap = std::max(max_non_overlap, 1 + dfs(intervals, intervals[0][1], 1));
        max_non_overlap = std::max(max_non_overlap, dfs(intervals, std::numeric_limits<int>::min(), 1));
        return intervals.size() - max_non_overlap;
    }

private:
    int dfs(vector<vector<int>> const& intervals, int end, int i)
    {
        if (i >= intervals.size()) {
            return 0;
        }

        if (intervals[i][0] < end) {
            return dfs(intervals, end, i + 1);
        }

        return std::max(1 + dfs(intervals, intervals[i][1], i + 1), dfs(intervals, end, i + 1));
    }
};
