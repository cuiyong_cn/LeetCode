/**
 * Encode and Decode TinyURL
 *
 * TinyURL is a URL shortening service where you enter a URL such as
 * https://leetcode.com/problems/design-tinyurl and it returns a short URL
 * such as http://tinyurl.com/4e9iAk.
 *
 * Design the encode and decode methods for the TinyURL service. There is
 * no restriction on how your encode/decode algorithm should work. You just
 * need to ensure that a URL can be encoded to a tiny URL and the tiny URL
 * can be decoded to the original URL.
 */
class Solution {
public:

    // Encodes a URL to a shortened URL.
    string encode(string longUrl) {
        string code;
        while (url2code.find(longUrl) == url2code.end()) {
            random_shuffle(alphabets.begin(), alphabets.end());
            code = alphabets.substr(0, 6);
            if (code2url.find(code) == code2url.end()) {
                code2url[code] = longUrl;
                url2code[longUrl] = code;
            }
        }
        return "http://tinyurl.com/" + code;
    }

    // Decodes a shortened URL to its original URL.
    string decode(string shortUrl) {
        int p = shortUrl.find_last_of("/");
        return code2url[shortUrl.substr(p + 1)];
    }
private:
    string alphabets = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    unordered_map<string, string> url2code, code2url;
};
