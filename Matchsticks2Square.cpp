/**
 * Matchsticks to Square
 *
 * You are given an integer array matchsticks where matchsticks[i] is the length of the ith
 * matchstick. You want to use all the matchsticks to make one square. You should not break any
 * stick, but you can link them up, and each matchstick must be used exactly one time.
 *
 * Return true if you can make this square and false otherwise.
 *
 * Example 1:
 *
 * Input: matchsticks = [1,1,2,2,2]
 * Output: true
 * Explanation: You can form a square with length 2, one side of the square came two sticks with length 1.
 * Example 2:
 *
 * Input: matchsticks = [3,3,3,3,4]
 * Output: false
 * Explanation: You cannot find a way to form a square with all the matchsticks.
 *
 * Constraints:
 *
 * 1 <= matchsticks.length <= 15
 * 1 <= matchsticks[i] <= 108
 */
/**
 * Let's optimize further.
 */
class Solution {
public:
    bool makesquare(vector<int>& nums) {
        if (nums.size() < 4) {
            return false;
        }

        auto sum = accumulate(nums.begin(), nums.end(), 0);
        if (0 != (sum % 4)) {
            return false;
        }

        int side = sum / 4;

        //Optimize :Reverse sort so that bigger numbers get rejected early on in the expansion tree
        sort(nums.begin(),nums.end(), greater<int>() );
        if (nums[0] > side) {
            return false;
        }

        vector<int> sides(4,0);
        return dfs(nums,sides,0,side);
    }

    bool dfs(vector<int> const& match, vector<int>& sides, int index, int side){
        if (index == match.size()) {
            return true;
        }

        for (int i = 0; i < 4; ++i) {
            //Optimize 1 & 2:
            if ((sides[i] + match[index]) > side || seenBefore(sides, i)) {
                continue;
            }

            sides[i] += match[index];
            if (dfs(match, sides, index + 1, side)) {
                return true;
            }

            sides[i] -= match[index];
        }

        return false;
    }

    bool seenBefore(vector<int> const& sides, int i) {
        return i==0 ? false:
               i==1 ? sides[1]==sides[0] :
               i==2 ? sides[2]==sides[1] || sides[2]==sides[0] :
               i==3 ? sides[3]==sides[2] || sides[3]==sides[1] || sides[3]==sides[0] : false;
    }
};

/**
 * Optimize below solution. Better but still TLE.
 */
class Solution {
public:
    bool makesquare(vector<int>& matchsticks) {
        auto total_len = accumulate(matchsticks.begin(), matchsticks.end(), 0);
        if (0 != (total_len % 4)) {
            return false;
        }

        int each_side_len = total_len / 4;
        sort(matchsticks.begin(), matchsticks.end());
        if (matchsticks.back() > each_side_len) {
            return false;
        }

        set<array<int, 4>> q = {{0, 0, 0, 0}};
        for (auto n : matchsticks) {
            if (q.empty()) {
                return false;
            }

            set<array<int, 4>> nq;
            for (auto& s : q) {
                if ((s[0] + n) <= each_side_len) {
                    auto clone = s;
                    clone[0] += n;
                    sort(clone.begin(), clone.end());
                    nq.insert(clone);
                }
                if ((s[1] + n) <= each_side_len) {
                    auto clone = s;
                    clone[1] += n;
                    sort(clone.begin(), clone.end());
                    nq.insert(clone);
                }
                if ((s[2] + n) <= each_side_len) {
                    auto clone = s;
                    clone[2] += n;
                    sort(clone.begin(), clone.end());
                    nq.insert(clone);
                }
                if ((s[3] + n) <= each_side_len) {
                    auto clone = s;
                    clone[3] += n;
                    sort(clone.begin(), clone.end());
                    nq.insert(clone);
                }
            }

            swap(q, nq);
        }

        for (auto const& s : q) {
            if (s[0] == s[1] && s[1] == s[2] && s[2] == s[3]) {
                return true;
            }
        }

        return false;
    }
};

/**
 * Original solution. TLE
 */
class Solution {
public:
    struct Square
    {
        int s1;
        int s2;
        int s3;
        int s4;
    };

    bool makesquare(vector<int>& matchsticks) {
        auto total_len = accumulate(matchsticks.begin(), matchsticks.end(), 0);
        if (0 != (total_len % 4)) {
            return false;
        }

        int each_side_len = total_len / 4;
        sort(matchsticks.begin(), matchsticks.end());
        if (matchsticks.back() > each_side_len) {
            return false;
        }

        deque<Square> q = { {0, 0, 0, 0} };
        for (auto n : matchsticks) {
            if (q.empty()) {
                return false;
            }

            int const N = q.size();
            for (int i = 0; i < N; ++i) {
                auto s = q.front(); q.pop_front();
                if ((s.s1 + n) <= each_side_len) {
                    auto clone = s;
                    clone.s1 += n;
                    q.push_back(clone);
                }
                if ((s.s2 + n) <= each_side_len) {
                    auto clone = s;
                    clone.s2 += n;
                    q.push_back(clone);
                }
                if ((s.s3 + n) <= each_side_len) {
                    auto clone = s;
                    clone.s3 += n;
                    q.push_back(clone);
                }
                if ((s.s4 + n) <= each_side_len) {
                    auto clone = s;
                    clone.s4 += n;
                    q.push_back(clone);
                }
            }
        }

        for (auto const& s : q) {
            if (s.s1 == s.s2 && s.s2 == s.s3 && s.s3 == s.s4) {
                return true;
            }
        }

        return false;
    }
};
