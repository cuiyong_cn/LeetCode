/**
 * Design HashSet
 *
 * Design a HashSet without using any built-in hash table libraries.
 *
 * To be specific, your design should include these functions:
 *
 *     add(value): Insert a value into the HashSet.
 *     contains(value) : Return whether the value exists in the HashSet or not.
 *     remove(value): Remove a value in the HashSet. If the value does not exist in the HashSet, do nothing.
 *
 * Example:
 *
 * MyHashSet hashSet = new MyHashSet();
 * hashSet.add(1);
 * hashSet.add(2);
 * hashSet.contains(1);    // returns true
 * hashSet.contains(3);    // returns false (not found)
 * hashSet.add(2);
 * hashSet.contains(2);    // returns true
 * hashSet.remove(2);
 * hashSet.contains(2);    // returns false (already removed)
 *
 * Note:
 *
 *     All values will be in the range of [0, 1000000].
 *     The number of operations will be in the range of [1, 10000].
 *     Please do not use the built-in HashSet library.
 */
/**
 * Just like the other problem, design map. Using vector of list to accomplish this.
 * We can also using the Binary Search Tree, but it is too heavy for a questioin that marked easy.
 */
class MyHashSet {
public:
    /** Initialize your data structure here. */
    MyHashSet() : set(10000, list<int>()) {

    }

    void add(int key) {
        list<int>& li = set[key % 10000];
        for (auto& n : li) {
            if (n == key) return;
        }
        li.push_back(key);
    }

    void remove(int key) {
        list<int>& li = set[key % 10000];
        for (auto n = li.begin(); n != li.end(); ++n) {
            if (*n == key) {
                li.erase(n);
                break;
            }
        }
    }

    /** Returns true if this set contains the specified element */
    bool contains(int key) {
        list<int>& li = set[key % 10000];
        for (auto& n : li) {
            if (n == key) return true;
        }
        return false;
    }
private:
    vector<list<int>> set;
};

/**
 * Your MyHashSet object will be instantiated and called as such:
 * MyHashSet* obj = new MyHashSet();
 * obj->add(key);
 * obj->remove(key);
 * bool param_3 = obj->contains(key);
 */
