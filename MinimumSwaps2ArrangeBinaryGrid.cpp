/**
 * Minimum Swaps to Arrange a Binary Grid
 *
 * Given an n x n binary grid, in one step you can choose two adjacent rows of the grid and swap
 * them.
 * A grid is said to be valid if all the cells above the main diagonal are zeros.
 * Return the minimum number of steps needed to make the grid valid, or -1 if the grid cannot be
 * valid.
 * The main diagonal of a grid is the diagonal that starts at cell (1, 1) and ends at cell (n, n).
 *
 * Example 1:
 *
 * Input: grid = [[0,0,1],[1,1,0],[1,0,0]]
 * Output: 3
 * Example 2:
 *
 * Input: grid = [[0,1,1,0],[0,1,1,0],[0,1,1,0],[0,1,1,0]]
 * Output: -1
 * Explanation: All rows are similar, swaps have no effect on the grid.
 * Example 3:
 *
 * Input: grid = [[1,0,0],[1,1,0],[1,1,1]]
 * Output: 0
 *
 * Constraints:
 *
 * n == grid.length
 * n == grid[i].length
 * 1 <= n <= 200
 * grid[i][j] is 0 or 1
 */
/**
 * Greedy solution from discussion. Using the tailing zero count.
 */
class Solution {
public:
    int minSwaps(vector<vector<int>>& grid) {
        const int n = grid.size();
        vector<int> tail_zeros(n);

        for (int i = 0; i < n; ++i) {
            auto const& row = grid[i];
            int count = 0;
            for (int j = n - 1; j >= 0; --j) {
                if (1 == row[j]) {
                    break;
                }
                ++count;
            }

            tail_zeros[i] = count;
        }

        int ans = 0;
        for (int i = 0; i < n; ++i) {
            int k = i;
            int req = n - 1 - i;
            while (k < n && tail_zeros[k] < req) {
                ++k;
            }

            if (k == n) {
                return -1;
            }

            ans += k - i;

            while (k > i) {
                tail_zeros[k] = tail_zeros[k - 1];
                --k;
            }
        }

        return ans;
    }
};

/**
 * Original solution. Idea is:
 * 1. For each row of the grid calculate the most right 1 in the grid in the array maxRight.
 * 2. To check if there exist answer, sort maxRight and check if maxRight[i] ≤ i for all possible i's
 * 3. If there exist an answer, simulate the swaps
 */
class Solution {
public:
    int minSwaps(vector<vector<int>>& grid) {
        int n = grid.size();
        vector<pair<int, int>> most_right_index;
        for (int j = 0; j < n; ++j) {
            auto const& row = grid[j];
            int mr = -1;
            for (int i = n - 1; i >= 0; --i) {
                if (1 == row[i]) {
                    mr = i;
                    break;
                }
            }

            most_right_index.push_back({j, mr});
        }

        sort(begin(most_right_index), end(most_right_index),
            [](auto a, auto b) {
                if (a.second < b.second) {
                    return true;
                }

                if (a.second == b.second) {
                    return a.first < b.first;
                }

                return false;
            });


        for (int i = 0; i < n; ++i) {
            if (most_right_index[i].second > i) {
                return -1;
            }
        }

        int ans = 0;
        for (int i = 0; i < n; ++i) {
            int min_steps = 201;
            int row = -1;
            for (auto const& mri : most_right_index) {
                if (mri.second > i) {
                    break;
                }

                if (mri.first >= i && mri.second <= i) {
                    int steps = mri.first - i;
                    if (steps < min_steps) {
                        min_steps = steps;
                        row = mri.first;
                    }
                }
            }

            ans += min_steps;
            if (row != i) {
                for (auto& mri : most_right_index) {
                    if (mri.first < row) {
                        mri.first += 1;
                    }
                    else if (mri.first == row) {
                        mri.first = i;
                    }
                }
            }
        }

        return ans;
    }
};
