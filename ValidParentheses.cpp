/**
 * Valid Parentheses
 *
 * Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the
 * input string is valid.
 *
 * An input string is valid if:
 *
 * Open brackets must be closed by the same type of brackets.
 * Open brackets must be closed in the correct order.
 *
 * Example 1:
 *
 * Input: s = "()"
 * Output: true
 * Example 2:
 *
 * Input: s = "()[]{}"
 * Output: true
 * Example 3:
 *
 * Input: s = "(]"
 * Output: false
 * Example 4:
 *
 * Input: s = "([)]"
 * Output: false
 * Example 5:
 *
 * Input: s = "{[]}"
 * Output: true
 *
 * Constraints:
 *
 * 1 <= s.length <= 104
 * s consists of parentheses only '()[]{}'
 */
/**
 * Shorter one.
 */
class Solution {
public:
    Solution()
    {
        bracket_mapping[')'] = '(';
        bracket_mapping['}'] = '{';
        bracket_mapping[']'] = '[';
    }

    bool isValid(string s) {
        vector<char> stk;
        for (auto c : s) {
            if (bracket_mapping.count(c)) {
                if (stk.empty()) {
                    return false;
                }

                if (stk.back() != bracket_mapping[c]) {
                    return false;
                }
                stk.pop_back();
            }
            else {
                stk.push_back(c);
            }
        }

        return stk.empty();
    }

private:
    unordered_map<char, char> bracket_mapping;
};

/**
 * Original solution.
 */
class Solution {
public:
    bool isValid(string s) {
        vector<char> stk;
        for (auto c : s) {
            if (is_open_bracket(c)) {
                stk.push_back(c);
            }
            else {
                if (stk.empty()) {
                    return false;
                }

                if (stk.back() != open_bracket(c)) {
                    return false;
                }
                stk.pop_back();
            }
        }

        return stk.empty();
    }

private:
    bool is_open_bracket(char c)
    {
        return '(' == c || '{' == c || '[' == c;
    }

    char open_bracket(char c)
    {
        if (')' == c) {
            return '(';
        }
        if ('}' == c) {
            return '{';
        }
        return '[';
    }
};
