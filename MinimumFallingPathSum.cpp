/**
 * Minimum Falling Path Sum
 *
 * Given a square array of integers A, we want the minimum sum of a falling path through A.
 *
 * A falling path starts at any element in the first row, and chooses one element from
 * each row.  The next row's choice must be in a column that is different from the
 * previous row's column by at most one.
 *
 * Example 1:
 *
 * Input: [[1,2,3],[4,5,6],[7,8,9]]
 * Output: 12
 * Explanation:
 * The possible falling paths are:
 *
 *     [1,4,7], [1,4,8], [1,5,7], [1,5,8], [1,5,9]
 *     [2,4,7], [2,4,8], [2,5,7], [2,5,8], [2,5,9], [2,6,8], [2,6,9]
 *     [3,5,7], [3,5,8], [3,5,9], [3,6,8], [3,6,9]
 *
 * The falling path with the smallest sum is [1,4,7], so the answer is 12.
 *
 * Note:
 *
 *     1 <= A.length == A[0].length <= 100
 *     -100 <= A[i][j] <= 100
 */
/**
 * More clean solution from the leetcode
 * The minimum path to get to element A[i][j] is the minimum of
 * A[i - 1][j - 1], A[i - 1][j] and A[i - 1][j + 1].
 * Starting from row 1, we add the minumum path to each element. The smallest number in
 * the last row is the miminum path sum.
 *
 * Example:
 * [1, 2, 3]
 * [4, 5, 6] => [5, 6, 8]
 * [7, 8, 9] => [7, 8, 9] => [12, 13, 15]
 *
 * The downsize of this solution is that it will modify the input. But this can be avoided
 * by work on a copy of A.
 */

class Solution {
private:
    int getMinInThree(vector<vector<int>>& A, int r, int c) {
        int minVal = INT_MAX;
        for (int i = -1; i < 2; ++i) {
            int idx = c + i;
            if (0 <= idx && idx < A[0].size()) {
                minVal = min(minVal, A[r][idx]);
            }
        }
        return minVal;
    }
public:
    int minFallingPathSum(vector<vector<int>>& A) {
        for (auto i = 1; i < A.size(); ++i) {
            for (auto j = 0; j < A.size(); ++j) {
                A[i][j] += getMinInThree(A, i - 1, j);
            }
        }

        return *min_element(begin(A[A.size() - 1]), end(A[A.size() - 1]));
    }
};

/**
 * My original version. It is not very clean IMO. Use a same array to memorize the walked
 * path for efficiency.
 */
class Solution {
private:
    int fallThrough(vector<vector<int>>& A, int r, int c, vector<vector<int>>& map) {
        if (r >= A.size() || r < 0) return 0;
        int minVal = INT_MAX;
        for (int i = -1; i < 2; ++i) {
            int idx = c + i;
            if (0 <= idx && idx < A[0].size()) {
                if (INT_MAX != map[r][idx]) {
                    minVal = min(minVal, map[r][idx]);
                }
                else {
                    int minMap = A[r][idx] + fallThrough(A, r + 1, idx, map);
                    map[r][idx] = minMap;
                    minVal = min(minVal, minMap);
                }
            }
        }

        return minVal;
    }

public:
    int minFallingPathSum(vector<vector<int>>& A) {
        int minVal = INT_MAX;
        vector<vector<int>> minMap(A.size(), vector<int>(A[0].size(), INT_MAX));
        for (int i = 0; i < A[0].size(); ++i) {
            minVal = min(minVal, A[0][i] + fallThrough(A, 1, i, minMap));
        }

        return minVal;
    }
};

