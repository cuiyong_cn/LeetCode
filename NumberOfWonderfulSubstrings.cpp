/**
 * Number of Wonderful Substrings
 *
 * A wonderful string is a string where at most one letter appears an odd number of times.
 *
 * For example, "ccjjc" and "abab" are wonderful, but "ab" is not.  Given a string word that
 * consists of the first ten lowercase English letters ('a' through 'j'), return the number of
 * wonderful non-empty substrings in word. If the same substring appears multiple times in word,
 * then count each occurrence separately.
 *
 * A substring is a contiguous sequence of characters in a string.
 *
 * Example 1:
 *
 * Input: word = "aba"
 * Output: 4
 * Explanation: The four wonderful substrings are underlined below:
 * - "aba" -> "a"
 * - "aba" -> "b"
 * - "aba" -> "a"
 * - "aba" -> "aba"
 * Example 2:
 *
 * Input: word = "aabb"
 * Output: 9
 * Explanation: The nine wonderful substrings are underlined below:
 * - "aabb" -> "a"
 * - "aabb" -> "aa"
 * - "aabb" -> "aab"
 * - "aabb" -> "aabb"
 * - "aabb" -> "a"
 * - "aabb" -> "abb"
 * - "aabb" -> "b"
 * - "aabb" -> "bb"
 * - "aabb" -> "b"
 * Example 3:
 *
 * Input: word = "he"
 * Output: 2
 * Explanation: The two wonderful substrings are underlined below:
 * - "he" -> "h"
 * - "he" -> "e"
 *
 * Constraints:
 *
 * 1 <= word.length <= 105
 * word consists of lowercase English letters from 'a' to 'j'.
 */
/**
 * Further optimized.
 */
class Solution {
public:
    long long wonderfulSubstrings(string word) {
        int const wordlen = word.length();
        unordered_map<int, int> prefix_mask_cnt{{0, 1}};

        int64_t ans = 0;
        int mask = 0;
        for (int i = 0; i < wordlen; ++i) {
            mask ^= 1 << (word[i] - 'a');
            // diff by 1
            for (auto c = 'a'; c < 'k'; ++c) {
                ans += prefix_mask_cnt[mask ^ (1 << (c - 'a'))];
            }
            // diff by 0
            ans += prefix_mask_cnt[mask];
            ++prefix_mask_cnt[mask];
        }

        return ans;
    }
};

/**
 * Optimized version. Still TLE.
 */
class Solution {
public:
    long long wonderfulSubstrings(string word) {
        int const wordlen = word.length();
        unordered_map<int, int> prefix_mask_cnt;

        int ans = 0;
        int mask = 0;
        for (int i = 0; i < wordlen; ++i) {
            mask ^= 1 << (word[i] - 'a');
            for (auto const& pmc : prefix_mask_cnt) {
                if (__builtin_popcount(pmc.first ^ mask) <= 1) {
                    ans += pmc.second;
                }
            }
            if (__builtin_popcount(mask) <= 1) {
                ++ans;
            }
            ++prefix_mask_cnt[mask];
        }

        return ans;
    }
};

/**
 * Original solution. TLE.
 */
class Solution {
public:
    long long wonderfulSubstrings(string word) {
        int const wordlen = word.length();
        vector<int> prefix_mask(wordlen + 1, 0);

        int mask = 0;
        for (int i = 0; i < wordlen; ++i) {
            mask ^= 1 << (word[i] - 'a');
            prefix_mask[i + 1] = mask;
        }

        int ans = wordlen;
        for (int L = 2; L <= wordlen; ++L) {
            int send = wordlen - L + 1;
            for (int i = 0; i < send; ++i) {
                int mask = prefix_mask[i] ^ prefix_mask[i + L];
                if (__builtin_popcount(mask) <= 1) {
                    ++ans;
                }
            }
        }

        return ans;
    }
};
