/**
 * Maximum Height of a Triangle
 *
 * You are given two integers red and blue representing the count of red and blue colored balls. You
 * have to arrange these balls to form a triangle such that the 1st row will have 1 ball, the 2nd
 * row will have 2 balls, the 3rd row will have 3 balls, and so on.
 *
 * All the balls in a particular row should be the same color, and adjacent rows should have different colors.
 * Return the maximum height of the triangle that can be achieved.
 *
 * Example 1:
 *
 * Input: red = 2, blue = 4
 *
 * Output: 3
 *
 * Explanation:
 *
 * The only possible arrangement is shown above.
 *
 * Example 2:
 *
 * Input: red = 2, blue = 1
 *
 * Output: 2
 *
 * Explanation:
 *
 * The only possible arrangement is shown above.
 *
 * Example 3:
 *
 * Input: red = 1, blue = 1
 *
 * Output: 1
 *
 * Example 4:
 *
 * Input: red = 10, blue = 1
 *
 * Output: 2
 *
 * Explanation:
 *
 * The only possible arrangement is shown above.
 *
 * Constraints:
 *     1 <= red, blue <= 100
 */
/**
 * Short one.
 */
auto height_start_with(array<int, 2> balls)
{
    auto h = 0;

    for (; balls[h & 1] >= h; ++h) {
        balls[h & 1] -= h;
    }

    return h - 1;
}

class Solution {
public:
    int maxHeightOfTriangle(int red, int blue) {
        return max(height_start_with({red, blue}), height_start_with({blue, red}));
    }
};

/**
 * Original solution.
 */
auto height_start_with(int a, int b)
{
    auto h = 0;

    while (true) {
        if (a < (h + 1)) {
            break;
        }

        h = h + 1;
        a = a - h;

        if (b < (h + 1)) {
            break;
        }

        h = h + 1;
        b = b - h;
    }

    return h;
}

class Solution {
public:
    int maxHeightOfTriangle(int red, int blue) {
        return max(height_start_with(red, blue), height_start_with(blue, red));
    }
};
