/**
 * Average of Levels in Binary Tree
 *
 * Given a non-empty binary tree, return the average value of the nodes on each level
 * in the form of an array.
 *
 * Example 1:
 *
 * Input:
 *     3
 *    / \
 *   9  20
 *     /  \
 *    15   7
 * Output: [3, 14.5, 11]
 * Explanation:
 * The average value of nodes on level 0 is 3,  on level 1 is 14.5, and on level 2 is 11. Hence return [3, 14.5, 11].
 *
 * Note:
 *     The range of node's value is in the range of 32-bit signed integer.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/**
 * BFS approach.
 */
class Solution {
public:
    vector<double> averageOfLevels(TreeNode* root) {
        vector<double> ans;
        deque<TreeNode*> dq;
        dq.push_back(root);

        while (false == dq.empty()) {
            int numNodes = dq.size();
            long sum = 0;
            for (int i = 0; i < numNodes; ++i) {
                TreeNode* tmp = dq.front();
                dq.pop_front();
                sum += tmp->val;
                if (nullptr != tmp->left) dq.push_back(tmp->left);
                if (nullptr != tmp->right) dq.push_back(tmp->right);
            }
            ans.push_back(sum * 1.0 / numNodes);
        }

        return ans;
    }
};

/**
 * DFS approach
 */
class Solution {
private:
    void dfs(TreeNode* root, int height, vector<double>& sum, vector<int>& count)
    {
        if (nullptr == root) return;

        if (height == sum.size()) {
            sum.push_back(0);
            count.push_back(0);
        }

        sum[height] += root->val;
        count[height] += 1;
        dfs(root->left, height + 1, sum, count);
        dfs(root->right, height + 1, sum, count);
    }

public:
    vector<double> averageOfLevels(TreeNode* root) {
        vector<double> ans;
        vector<int> count;
        dfs(root, 0, ans, count);
        for (int i = 0; i < ans.size(); ++i) {
            ans[i] = ans[i] / count[i];
        }
        return ans;
    }
};
