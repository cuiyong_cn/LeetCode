/**
 * Minimum Time to Collect All Apples in a Tree
 *
 * Given an undirected tree consisting of n vertices numbered from 0 to n-1, which has some apples
 * in their vertices. You spend 1 second to walk over one edge of the tree. Return the minimum time
 * in seconds you have to spend to collect all apples in the tree, starting at vertex 0 and coming
 * back to this vertex.
 *
 * The edges of the undirected tree are given in the array edges, where edges[i] = [ai, bi] means
 * that exists an edge connecting the vertices ai and bi. Additionally, there is a boolean array
 * hasApple, where hasApple[i] = true means that vertex i has an apple; otherwise, it does not have
 * any apple.
 *
 * Example 1:
 *
 * Input: n = 7, edges = [[0,1],[0,2],[1,4],[1,5],[2,3],[2,6]], hasApple = [false,false,true,false,true,true,false]
 * Output: 8
 * Explanation: The figure above represents the given tree where red vertices have an apple. One optimal path to collect all apples is shown by the green arrows.
 * Example 2:
 *
 * Input: n = 7, edges = [[0,1],[0,2],[1,4],[1,5],[2,3],[2,6]], hasApple = [false,false,true,false,false,true,false]
 * Output: 6
 * Explanation: The figure above represents the given tree where red vertices have an apple. One optimal path to collect all apples is shown by the green arrows.
 * Example 3:
 *
 * Input: n = 7, edges = [[0,1],[0,2],[1,4],[1,5],[2,3],[2,6]], hasApple = [false,false,false,false,false,false,false]
 * Output: 0
 *
 * Constraints:
 *
 * 1 <= n <= 105
 * edges.length == n - 1
 * edges[i].length == 2
 * 0 <= ai < bi <= n - 1
 * fromi < toi
 * hasApple.length == n
 */
/**
 * Seems less parameter passing will save some time.
 */
class Solution {
public:
    int minTime(int n, vector<vector<int>>& edges, vector<bool>& hasApple) {
        build_graph(edges);
        has_apple = &hasApple;
        return dfs(0, 0);
    }

private:
    void build_graph(vector<vector<int>> const& edges)
    {
        for (auto const& e : edges) {
            graph[e[0]].push_back(e[1]);
            graph[e[1]].push_back(e[0]);
        }
    }

    int dfs(int64_t v, int init)
    {
        if (visited.count(v)) {
            return 0;
        }

        visited.insert(v);

        auto cost = 0;
        for (auto n : graph[v]) {
            cost += dfs(n, 2);
        }

        return cost += (*has_apple)[v] || cost > 0 ? init : 0;
    }

    vector<bool>* has_apple = nullptr;
    unordered_set<int> visited;
    unordered_map<int, vector<int>> graph;
};

/**
 * More short one.
 */
class Solution {
public:
    int minTime(int n, vector<vector<int>>& edges, vector<bool>& hasApple) {
        build_graph(edges);

        return dfs(0, 0, hasApple);
    }

private:
    void build_graph(vector<vector<int>> const& edges)
    {
        for (auto const& e : edges) {
            graph[e[0]].push_back(e[1]);
            graph[e[1]].push_back(e[0]);
        }
    }

    int dfs(int64_t v, int init, vector<bool> const& has_apple)
    {
        if (visited.count(v)) {
            return 0;
        }

        visited.insert(v);

        auto cost = 0;
        for (auto n : graph[v]) {
            cost += dfs(n, 2, has_apple);
        }

        return cost += has_apple[v] || cost > 0 ? init : 0;
    }

    unordered_set<int> visited;
    unordered_map<int, vector<int>> graph;
};

/**
 * Original solution.
 */
class Solution {
public:
    int minTime(int n, vector<vector<int>>& edges, vector<bool>& hasApple) {
        build_graph(edges);
        unordered_set<int64_t> path;
        collect_edges(0, hasApple, path);

        return edges_used.size() * 2;
    }

private:
    void build_graph(vector<vector<int>> const& edges)
    {
        for (auto const& e : edges) {
            graph[e[0]].push_back(e[1]);
            graph[e[1]].push_back(e[0]);
        }
    }

    int64_t make_edge(int64_t a, int64_t b) {
        return a < b ? (a << 32) | b : (b << 32) | a;
    }

    void collect_edges(int64_t v, vector<bool> const& has_apple, unordered_set<int64_t>& path)
    {
        if (0 == graph.count(v)) {
            return;
        }

        for (auto n : graph[v]) {
            auto edge = make_edge(n, v);
            if (path.count(edge)) {
                continue;
            }
            path.insert(edge);
            if (has_apple[n]) {
                edges_used.insert(path.begin(), path.end());
            }
            collect_edges(n, has_apple, path);
            path.erase(edge);
        }
    }

    unordered_set<int64_t> edges_used;
    unordered_map<int, vector<int>> graph;
};
