/**
 * Max Consecutive Ones III
 *
 * Given an array A of 0s and 1s, we may change up to K values from 0 to 1.
 *
 * Return the length of the longest (contiguous) subarray that contains only 1s.
 *
 * Example 1:
 *
 * Input: A = [1,1,1,0,0,0,1,1,1,1,0], K = 2
 * Output: 6
 * Explanation:
 * [1,1,1,0,0,1,1,1,1,1,1]
 * Bolded numbers were flipped from 0 to 1.  The longest subarray is underlined.
 *
 * Example 2:
 *
 * Input: A = [0,0,1,1,0,0,1,1,1,0,1,1,0,0,0,1,1,1,1], K = 3
 * Output: 10
 * Explanation:
 * [0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,1,1,1,1]
 * Bolded numbers were flipped from 0 to 1.  The longest subarray is underlined.
 *
 * Note:
 *
 *     1 <= A.length <= 20000
 *     0 <= K <= A.length
 *     A[i] is 0 or 1
 */
/**
 * Using sliding windows solution.
 */
class Solution {
public:
    int longestOnes(vector<int>& A, int K) {
        int i = 0, j;
        for (j = 0; j < A.size(); ++j) {
            if (A[j] == 0) K--;
            if (K < 0 && A[i++] == 0) K++;
        }
        return j - i;
    }
};

/**
 * Original solution. Sadly, this is a TLE soluiton.
 */
class Solution {
public:
    int longestOnes(vector<int>& A, int K) {
        ans = 0;
        TK = K;
        return longest(A, 0, 0, K);
    }
private:
    int longest(vector<int>&A, int s, int count, int K) {
        if (0 == count && s > (A.size() - ans)) return ans;

        for (int i = s; i < A.size(); ++i) {
            if (0 == A[i]) {
                if (K > 0) {
                    ans = max(longest(A, i + 1, count + 1, K - 1), ans);
                }
                if (K == TK) {
                    ans = max(ans, count);
                    count = 0;
                }
                else {
                    break;
                }
            }
            else {
                ++count;
            }
        }
        ans = max(count, ans);
        return ans;
    }
    int ans;
    int TK;
};
