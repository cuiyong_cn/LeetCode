/**
 * Number of Islands
 *
 * Given a 2d grid map of '1's (land) and '0's (water), count the number of islands. An island is surrounded by water
 * and is formed by connecting adjacent lands horizontally or vertically. You may assume all four edges of the grid are
 * all surrounded by water.
 *
 * Example 1:
 *
 * Input: grid = [
 *   ["1","1","1","1","0"],
 *   ["1","1","0","1","0"],
 *   ["1","1","0","0","0"],
 *   ["0","0","0","0","0"]
 * ]
 * Output: 1
 * Example 2:
 *
 * Input: grid = [
 *   ["1","1","0","0","0"],
 *   ["1","1","0","0","0"],
 *   ["0","0","1","0","0"],
 *   ["0","0","0","1","1"]
 * ]
 * Output: 3
 */
/**
 * Original solution. I have solved these problems many times.
 * Also we can use the bfs method.
 */
class Solution {
public:
    int numIslands(vector<vector<char>>& grid) {
        int ans = 0;

        for (int i = 0; i < grid.size(); ++i) {
            for (int j = 0; j < grid[i].size(); ++j) {
                if ('1' == grid[i][j]) {
                    color_island(grid, i, j);
                    ++ans;
                }
            }
        }

        return ans;
    }

private:
    void color_island(vector<vector<char>>& grid, int r, int c)
    {
        if (r < grid.size() && c < grid[0].size()) {
            if ('1' == grid[r][c]) {
                grid[r][c] = '2';
                color_island(grid, r - 1, c);
                color_island(grid, r, c - 1);
                color_island(grid, r, c + 1);
                color_island(grid, r + 1, c);
            }
        }
    }
};
