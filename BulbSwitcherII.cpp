/**
 * Bulb Switcher II
 *
 * There is a room with n lights which are turned on initially and 4 buttons on the wall. After performing exactly m
 * unknown operations towards buttons, you need to return how many different kinds of status of the n lights could be.
 *
 * Suppose n lights are labeled as number [1, 2, 3 ..., n], function of these 4 buttons are given below:
 *     Flip all the lights.
 *     Flip lights with even numbers.
 *     Flip lights with odd numbers.
 *     Flip lights with (3k + 1) numbers, k = 0, 1, 2, ...
 *
 * Example 1:
 *
 * Input: n = 1, m = 1.
 * Output: 2
 * Explanation: Status can be: [on], [off]
 *
 * Example 2:
 *
 * Input: n = 2, m = 1.
 * Output: 3
 * Explanation: Status can be: [on, off], [off, on], [off, off]
 *
 * Example 3:
 *
 * Input: n = 3, m = 1.
 * Output: 4
 * Explanation: Status can be: [off, on, off], [on, off, on], [off, off, off], [off, on, on].
 *
 * Note: n and m both fit in range [0, 1000].
 */
/**
 * At first, I want to brute force this one. List all the possible op sequence. But considering the max op is
 * 4^1000. I realize this one will go nowhere.
 *
 * Then I found out, the operations have the following property:
 *      Let's call the 4 kinds of op are a, b, c, d
 *      1. Every op doing twice has no effect.
 *      2. a followed by b is same as c, vice versa (ex b followed by a is same as c)
 *      3. a followed by c is same as b, vice versa (ex c followed by a is same as b)
 *      4. b followed by c is same as a, vice versa (ex c followed by b is same as a)
 *
 * base on the first property, then we only need to consider wether the op is done or not
 *
 * Secondly, let's consider this: does lights number matter?
 * The answer is yes when lights numebr are under 3 and no when lights number are greater than 2, it does not matter.
 *
 * op b and c switch every two lights, op d switch every 3 lights. That means first 6 lights determine the rest.
 * But look a bit further, the first 3 lights can determine the rest also.
 *
 * Because 4 kinds op
 * So give with different m, we can conclude this.
 *      1. m == 0, then answer is 1
 *      2. m == 1, then we can have 4 kinds of op. a, b, c, or d. then base on lights number. we have different answer
 *         n = 1, ans = 2
 *         n = 2, ans = 3
 *         n >= 3, ans = 4
 *      3. m == 2, then we can have 16 different operations. But using the 4 properties, only 7 of them are unique
 *              a, b, c, ad, bd, cd, null (do nothing)
 *         then base on lights number. We use thse 7 different op to simulate
 *         n = 1, ans = 2
 *         n = 2, ans = 4
 *         n >= 3, ans = 7
 *      4. m >= 3, then using the 4 properties, we only got 8 unique ops:
 *              a, b, c, d, ad, bd, cd, null (do nothing)
 *         then base on lights number. we use the 8 different op to simulate
 *         n = 1, ans = 2
 *         n = 2, ans = 4
 *         n >= 3, ans = 8
 * Thus, come out the following the solution.
 */
/**
 * Simplify the below solution. Also there is a simulation one from leetcode.
 */
class Solution {
public:
    int flipLights(int n, int m) {
        if (m == 0 || n == 0) return 1;
        if (n == 1) return 2;
        if (n == 2) return m == 1? 3:4;
        if (m == 1) return 4;
        return m == 2? 7:8;
    }
};

class Solution {
public:
    int flipLights(int n, int m) {
        n = std::min(n, 3);

        int ans = 0;
        if (0 == m) {
            ans = 1;
        }
        else if (1 == m) {
            if (1 == n) {
                ans = 2;
            }
            else if (2 == n) {
                ans = 3;
            }
            else {
                ans = 4;
            }
        }
        else if (2 == m) {
            if (1 == n) {
                ans = 2;
            }
            else if (2 == n) {
                ans = 4;
            }
            else {
                ans = 7;
            }
        }
        else {
            if (1 == n) {
                ans = 2;
            }
            else if (2 == n) {
                ans = 4;
            }
            else {
                ans = 8;
            }
        }

        return ans;
    }
};

/**
 * Improve the below solution.
 */
class Solution {
public:
    int flipLights(int n, int m) {
        if (0 == m) return 1;

        n = std::min(n, 6);

        unordered_set<int> seen;

        int shift = std::max(0, 6 - n);
        for (int cand = 1; cand < 16; ++cand) {
            int bcount = std::bitset<sizeof(int)>(cand).count();
            if (bcount <= m) {
                int lights = 0;
                if (((cand >> 0) & 1) > 0) lights ^= 0b111111 >> shift;
                if (((cand >> 1) & 1) > 0) lights ^= 0b010101 >> shift;
                if (((cand >> 2) & 1) > 0) lights ^= 0b101010 >> shift;
                if (((cand >> 3) & 1) > 0) lights ^= 0b100100 >> shift;
                seen.insert(lights);
            }
        }

        return seen.size();
    }
};

/**
 * Using simulation.
 */
class Solution {
public:
    int flipLights(int n, int m) {
        n = std::min(n, 6);

        unordered_set<int> seen;

        int shift = std::max(0, 6 - n);
        for (int cand = 0; cand < 16; ++cand) {
            int bcount = std::bitset<sizeof(int)>(cand).count();
            if (bcount % 2 == m % 2 && bcount <= m) {
                int lights = 0;
                if (((cand >> 0) & 1) > 0) lights ^= 0b111111 >> shift;
                if (((cand >> 1) & 1) > 0) lights ^= 0b010101 >> shift;
                if (((cand >> 2) & 1) > 0) lights ^= 0b101010 >> shift;
                if (((cand >> 3) & 1) > 0) lights ^= 0b100100 >> shift;
                seen.insert(lights);
            }
        }

        return seen.size();
    }
};
