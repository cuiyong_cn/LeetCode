/**
 * Contiguous Array
 *
 * Given a binary array, find the maximum length of a contiguous subarray with equal number of 0 and 1.
 *
 * Example 1:
 * Input: [0,1]
 * Output: 2
 * Explanation: [0, 1] is the longest contiguous subarray with equal number of 0 and 1.
 * Example 2:
 * Input: [0,1,0]
 * Output: 2
 * Explanation: [0, 1] (or [1, 0]) is a longest contiguous subarray with equal number of 0 and 1.
 * Note: The length of the given binary array will not exceed 50,000.
 */
/**
 * Hash map solution.
 * We start traversing the array from the beginning. If at any moment, the countcount becomes zero,
 * it implies that we've encountered equal number of zeros and ones from the beginning till the
 * current index of the array(ii). Not only this, another point to be noted is that if we encounter
 * the same countcount twice while traversing the array, it means that the number of zeros and ones
 * are equal between the indices corresponding to the equal countcount values.
 */
class Solution {
public:
    int findMaxLength(vector<int>& nums) {
        unordered_map<int, int> map;
        map[0] = -1;
        int ans = 0, count = 0;
        for (int i = 0; i < nums.size(); ++i) {
            count += 1 == nums[i] ? 1 : -1;
            if (map.count(count)) {
                ans = max(ans, i - map[count]);
            }
            else {
                map[count] = i;
            }
        }

        return ans;
    }
};

/**
 * Original solution. TLE.
 */
class Solution {
public:
    int findMaxLength(vector<int>& nums) {
        vector<int> acc_zero_count(nums.size() + 1, 0);
        for (int i = 0; i < nums.size(); ++i) {
            acc_zero_count[i + 1] = acc_zero_count[i];
            if (0 == nums[i]) {
                ++acc_zero_count[i + 1];
            }
        }

        for (int L = nums.size() - (nums.size() % 2); L > 0; L -= 2) {
            int end = nums.size() - L;
            for (int i = 0; i <= end; ++i) {
                int zeros = acc_zero_count[i + L] - acc_zero_count[i];
                if (zeros == (L - zeros)) {
                    return L;
                }
            }
        }

        return 0;
    }
};
