/**
 * Design Front Middle Back Queue
 *
 * Design a queue that supports push and pop operations in the front, middle, and back.
 *
 * Implement the FrontMiddleBack class:
 *
 * FrontMiddleBack() Initializes the queue.
 * void pushFront(int val) Adds val to the front of the queue.
 * void pushMiddle(int val) Adds val to the middle of the queue.
 * void pushBack(int val) Adds val to the back of the queue.
 * int popFront() Removes the front element of the queue and returns it. If the queue is empty, return -1.
 * int popMiddle() Removes the middle element of the queue and returns it. If the queue is empty, return -1.
 * int popBack() Removes the back element of the queue and returns it. If the queue is empty, return -1.
 * Notice that when there are two middle position choices, the operation is performed on the frontmost middle position choice. For example:
 *
 * Pushing 6 into the middle of [1, 2, 3, 4, 5] results in [1, 2, 6, 3, 4, 5].
 * Popping the middle from [1, 2, 3, 4, 5, 6] returns 3 and results in [1, 2, 4, 5, 6].
 *
 * Example 1:
 *
 * Input:
 * ["FrontMiddleBackQueue", "pushFront", "pushBack", "pushMiddle", "pushMiddle", "popFront", "popMiddle", "popMiddle", "popBack", "popFront"]
 * [[], [1], [2], [3], [4], [], [], [], [], []]
 * Output:
 * [null, null, null, null, null, 1, 3, 4, 2, -1]
 *
 * Explanation:
 * FrontMiddleBackQueue q = new FrontMiddleBackQueue();
 * q.pushFront(1);   // [1]
 * q.pushBack(2);    // [1, 2]
 * q.pushMiddle(3);  // [1, 3, 2]
 * q.pushMiddle(4);  // [1, 4, 3, 2]
 * q.popFront();     // return 1 -> [4, 3, 2]
 * q.popMiddle();    // return 3 -> [4, 2]
 * q.popMiddle();    // return 4 -> [2]
 * q.popBack();      // return 2 -> []
 * q.popFront();     // return -1 -> [] (The queue is empty)
 *
 * Constraints:
 *
 * 1 <= val <= 109
 * At most 1000 calls will be made to pushFront, pushMiddle, pushBack, popFront, popMiddle, and popBack.
 */
/**
 * Simplify below code.
 */
class FrontMiddleBackQueue {
public:
    FrontMiddleBackQueue() {

    }

    void pushFront(int val) {
        first_half.emplace_front(val);
        update();
    }

    void pushMiddle(int val) {
        first_half.emplace_back(val);
        update();
    }

    void pushBack(int val) {
        second_half.emplace_back(val);
        update();
    }

    int popFront() {
        int ret = -1;
        if (first_half.empty()) {
            if (!second_half.empty()) {
                ret = second_half.front(); second_half.pop_front();
            }
        }
        else {
            ret = first_half.front(); first_half.pop_front();
        }

        update();

        return ret;
    }

    int popMiddle() {
        int ret = -1;
        if (first_half.size() < second_half.size()) {
            ret = second_half.front(); second_half.pop_front();
        }
        else {
            if (!first_half.empty()) {
                ret = first_half.back(); first_half.pop_back();
            }
        }
        update();
        return ret;
    }

    int popBack() {
        int ret = -1;
        if (!second_half.empty()) {
            ret = second_half.back(); second_half.pop_back();
        }

        update();

        return ret;
    }

    void update()
    {
        int size_diff = first_half.size() - second_half.size();
        if (size_diff < -1) {
            first_half.emplace_back(second_half.front());
            second_half.pop_front();
        }
        else if (size_diff > 0) {
            if (!first_half.empty()) {
                second_half.emplace_front(first_half.back());
                first_half.pop_back();
            }
        }
    }

private:
    deque<int> first_half;
    deque<int> second_half;
};

/**
 * Your FrontMiddleBackQueue object will be instantiated and called as such:
 * FrontMiddleBackQueue* obj = new FrontMiddleBackQueue();
 * obj->pushFront(val);
 * obj->pushMiddle(val);
 * obj->pushBack(val);
 * int param_4 = obj->popFront();
 * int param_5 = obj->popMiddle();
 * int param_6 = obj->popBack();
 */

/**
 * Original solution. Not simplified.
 */
class FrontMiddleBackQueue {
public:
    FrontMiddleBackQueue() {

    }

    void pushFront(int val) {
        first_half.emplace_front(val);
        if (first_half.size() > second_half.size()) {
            auto v = first_half.back(); first_half.pop_back();
            second_half.emplace_front(v);
        }
    }

    void pushMiddle(int val) {
        if (first_half.size() < second_half.size()) {
            first_half.emplace_back(val);
        }
        else {
            second_half.emplace_front(val);
        }
    }

    void pushBack(int val) {
        second_half.emplace_back(val);
        if ((second_half.size() - first_half.size()) > 1) {
            auto v = second_half.front(); second_half.pop_front();
            first_half.emplace_back(v);
        }
    }

    int popFront() {
        if (first_half.empty()) {
            if (second_half.empty()) {
                return -1;
            }

            auto v = second_half.front(); second_half.pop_front();
            return v;
        }

        auto v = first_half.front(); first_half.pop_front();
        if ((second_half.size() - first_half.size()) > 1) {
            auto v = second_half.front(); second_half.pop_front();
            first_half.emplace_back(v);
        }

        return v;
    }

    int popMiddle() {
        if (first_half.size() < second_half.size()) {
            if (second_half.empty()) {
                return -1;
            }
            auto v = second_half.front(); second_half.pop_front();
            return v;
        }

        if (first_half.empty()) {
            return -1;
        }
        auto v = first_half.back(); first_half.pop_back();
        return v;
    }

    int popBack() {
        if (second_half.empty()) {
            if (first_half.empty()) {
                return -1;
            }

            auto v = first_half.back(); first_half.pop_back();
            return v;
        }

        auto v = second_half.back(); second_half.pop_back();
        if (first_half.size() > second_half.size()) {
            auto v = first_half.back(); first_half.pop_back();
            second_half.emplace_front(v);
        }

        return v;
    }

private:
    deque<int> first_half;
    deque<int> second_half;
};

/**
 * Your FrontMiddleBackQueue object will be instantiated and called as such:
 * FrontMiddleBackQueue* obj = new FrontMiddleBackQueue();
 * obj->pushFront(val);
 * obj->pushMiddle(val);
 * obj->pushBack(val);
 * int param_4 = obj->popFront();
 * int param_5 = obj->popMiddle();
 * int param_6 = obj->popBack();
 */
