/**
 * Path with Maximum Probability
 *
 * You are given an undirected weighted graph of n nodes (0-indexed), represented by an edge list
 * where edges[i] = [a, b] is an undirected edge connecting the nodes a and b with a probability of
 * success of traversing that edge succProb[i].
 *
 * Given two nodes start and end, find the path with the maximum probability of success to go from
 * start to end and return its success probability.
 *
 * If there is no path from start to end, return 0. Your answer will be accepted if it differs from
 * the correct answer by at most 1e-5.
 *
 * Example 1:
 *
 * Input: n = 3, edges = [[0,1],[1,2],[0,2]], succProb = [0.5,0.5,0.2], start = 0, end = 2
 * Output: 0.25000
 * Explanation: There are two paths from start to end, one having a probability of success = 0.2 and the other has 0.5 * 0.5 = 0.25.
 * Example 2:
 *
 * Input: n = 3, edges = [[0,1],[1,2],[0,2]], succProb = [0.5,0.5,0.3], start = 0, end = 2
 * Output: 0.30000
 * Example 3:
 *
 * Input: n = 3, edges = [[0,1]], succProb = [0.5], start = 0, end = 2
 * Output: 0.00000
 * Explanation: There is no path between 0 and 2.
 *
 * Constraints:
 *
 * 2 <= n <= 10^4
 * 0 <= start, end < n
 * start != end
 * 0 <= a, b < n
 * a != b
 * 0 <= succProb.length == edges.length <= 2*10^4
 * 0 <= succProb[i] <= 1
 * There is at most one edge between every two nodes.
 */
/**
 * More time and space efficient.
 * The difference is:
 * at each iteration, we always start from the maximaml probability node.
 * So at each iteration, if we have traversed some node, we can skip that node.
 */
class Solution {
public:
    double maxProbability(int n, vector<vector<int>>& edges, vector<double>& pro, int start, int end) {
        vector<vector<pair<int, double>>> graph(n);

        for(int i=0; i<edges.size(); i++) {
            int a = edges[i][0], b = edges[i][1];

            graph[a].push_back({b, pro[i]});
            graph[b].push_back({a, pro[i]});
        }

        vector<bool> seen(n, false);

        priority_queue<pair<double, int>> q;
        q.emplace(make_pair(1.0, start));

        vector<double> mx(n, 0.0);
        mx[start] = 1.0;

        while (!q.empty()) {
            auto [proba, a] = q.top();
            q.pop();

            if (!seen[a]) {
                seen[a] = true;
                for (auto& to: graph[a]) {
                    auto prob2b = to.second * proba;
                    if (mx[to.first] < prob2b) {
                        mx[to.first] = prob2b;
						q.emplace(make_pair(prob2b, to.first));
                    }
                }
            }
        }

        return mx[end];
    }
};

/**
 * Use unordered_map to speed things up. And passed
 */
class Solution {
public:
    double maxProbability(int n, vector<vector<int>>& edges, vector<double>& succProb, int start, int end) {
        unordered_map<int, vector<pair<int, double>>> path_prob_map;
        auto edges_count = edges.size();
        for (size_t i = 0; i < edges_count; ++i) {
            int a = edges[i][0], b = edges[i][1];
            path_prob_map[a].emplace_back(make_pair(b, succProb[i]));
            path_prob_map[b].emplace_back(make_pair(a, succProb[i]));
        }

        vector<double> ans(n, 0);
        ans[start] = 1;
        vector<pair<int, double>> q = {{start, 1.0}};

        while (!q.empty()) {
            vector<pair<int, double>> nq;
            for (auto& p : q) {
                int a = p.first;
                double prob = p.second;

                for (auto& node : path_prob_map[a]) {
                    double prob2b = node.second * prob;
                    if (prob2b > ans[node.first]) {
                        ans[node.first] = prob2b;
                        nq.emplace_back(make_pair(node.first, prob2b));
                    }
                }
            }
            swap(q, nq);
        }

        return ans[end];
    }
};

/**
 * Original solution., sadly TLE.
 */
class Solution {
public:
    double maxProbability(int n, vector<vector<int>>& edges, vector<double>& succProb, int start, int end) {
        vector<vector<double>> path_prob_map(n, vector<double>(n, 0.0));
        auto edges_count = edges.size();
        for (size_t i = 0; i < edges_count; ++i) {
            int a = edges[i][0], b = edges[i][1];
            path_prob_map[a][b] = succProb[i];
            path_prob_map[b][a] = succProb[i];
        }

        vector<double> ans(n, 0);
        ans[start] = 1;
        vector<pair<int, double>> q = {{start, 1.0}};

        while (!q.empty()) {
            vector<pair<int, double>> nq;
            for (auto& p : q) {
                int a = p.first;
                double prob = p.second;

                for (size_t b = 0; b < n; ++b) {
                    if (path_prob_map[a][b] > 0) {
                        double prob2b = prob * path_prob_map[a][b];
                        if (prob2b > ans[b]) {
                            ans[b] = prob2b;
                            nq.emplace_back(make_pair(b, prob2b));
                        }
                    }
                }
            }
            swap(q, nq);
        }

        return ans[end];
    }
};
