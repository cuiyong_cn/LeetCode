/**
 * Minimum Operations to Make Array Equal
 *
 * You have an array arr of length n where arr[i] = (2 * i) + 1 for all valid values of i (i.e., 0
 * <= i < n).
 *
 * In one operation, you can select two indices x and y where 0 <= x, y < n and subtract 1 from
 * arr[x] and add 1 to arr[y] (i.e., perform arr[x] -=1 and arr[y] += 1). The goal is to make all
 * the elements of the array equal. It is guaranteed that all the elements of the array can be made
 * equal using some operations.
 *
 * Given an integer n, the length of the array, return the minimum number of operations needed to
 * make all the elements of arr equal.
 *
 * Example 1:
 *
 * Input: n = 3
 * Output: 2
 * Explanation: arr = [1, 3, 5]
 * First operation choose x = 2 and y = 0, this leads arr to be [2, 3, 4]
 * In the second operation choose x = 2 and y = 0 again, thus arr = [3, 3, 3].
 * Example 2:
 *
 * Input: n = 6
 * Output: 9
 *
 * Constraints:
 *
 * 1 <= n <= 104
 */
/**
 * Simplify below solution to O(1). Idea is:
 * we can observe that
 * when n is even: the answer will 1 + 3 + ... + (n - 1) => n * n / 2 / 2
 *      n is odd : the answer will 2 + 4 + ... + (n - 1) => (n + 1) / 2 * n / 2
 * not do not write n * (n + 1) / 4
 */
class Solution {
public:
    int minOperations(int n) {
        return n * n / 4;
    }
};

/**
 * Original solution. Idea is:
 * Let's assume all the elements will equal to x at last. Then
 *      n * x = 1 + 3 + 5 + ... + [2 * (n - 1) + 1]
 *      x = n;
 * so ans will be as n - (2 * i + 1) for all i <= n
 */
class Solution {
public:
    int minOperations(int n) {
        int ans = 0;
        for (int i = 1; i <= n; i += 2) {
            ans += n - i;
        }

        return ans;
    }
};
