/**
 * Sum of Nodes with Even-Valued Grandparent
 *
 * Given the root of a binary tree, return the sum of values of nodes with an even-valued
 * grandparent. If there are no nodes with an even-valued grandparent, return 0.
 *
 * A grandparent of a node is the parent of its parent if it exists.
 *
 * Example 1:
 *
 * Input: root = [6,7,8,2,7,1,3,9,null,1,4,null,null,null,5]
 * Output: 18
 * Explanation: The red nodes are the nodes with even-value grandparent while the blue nodes are the even-value grandparents.
 * Example 2:
 *
 * Input: root = [1]
 * Output: 0
 *
 * Constraints:
 *
 * The number of nodes in the tree is in the range [1, 104].
 * 1 <= Node.val <= 100
 */
/**
 * BFS solution.
 */
class Solution {
public:
    int sumEvenGrandparent(TreeNode* root) {
        int ans = 0;
        deque<TreeNode*> q = { root };
        while (!q.empty()) {
            auto const size = q.size();
            for (int i = 0; i < size; ++i) {
                auto node = q.front(); q.pop_front();
                if (0 == (node->val & 1)) {
                    if (node->left) {
                        if (node->left->left) {
                            ans += node->left->left->val;
                        }
                        if (node->left->right) {
                            ans += node->left->right->val;
                        }
                    }
                    if (node->right) {
                        if (node->right->left) {
                            ans += node->right->left->val;
                        }
                        if (node->right->right) {
                            ans += node->right->right->val;
                        }
                    }
                }

                if (node->left) {
                    q.push_back(node->left);
                }
                if (node->right) {
                    q.push_back(node->right);
                }
            }
        }

        return ans;
    }
};

/**
 * Make below solution shorter.
 */
class Solution {
public:
    int sumEvenGrandparent(TreeNode* root, int p = 1, int gp = 1) {
        return nullptr == root ? 0 : sumEvenGrandparent(root->left, root->val, p)
                                   + sumEvenGrandparent(root->right, root->val, p)
                                   + (gp & 1 ? 0 : root->val);
    }
};

/**
 * Original solution. DFS solution.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int sumEvenGrandparent(TreeNode* root) {
        return dfs(nullptr, nullptr, root);
    }

private:
    int dfs(TreeNode const* gp, TreeNode const* p, TreeNode* node)
    {
        if (nullptr == node) {
            return 0;
        }

        int ans = 0;
        if (nullptr != gp && (0 == (gp->val & 1))) {
            ans = node->val;
        }

        return ans + dfs(p, node, node->left) + dfs(p, node, node->right);
    }
};
