/**
 * Lexicographical Numbers
 *
 * Given an integer n, return 1 - n in lexicographical order.
 *
 * For example, given 13, return: [1,10,11,12,13,2,3,4,5,6,7,8,9].
 *
 * Please optimize your algorithm to use less time and space. The input size may be as large as 5,000,000.
 */
/**
 * DFS version from discussion. But this is bad practice becoz the stack might got too deep
 */
class Solution {
public:
    vector<int> lexicalOrder(int n) {
        vector<int> res;
        helper(1, n, res);
        return res;
    }
    
    void helper(int target, int n, vector<int>& res) {
        if (target > n) return;
        res.push_back(target);
        helper(target * 10, n, res);
        if (target % 10 != 9) helper(target+1, n, res);
    }
};

/**
 * Simplify the below version.
 */
class Solution {
public:
    vector<int> lexicalOrder(int n) {
        vector<int> res(n);
        int cur = 1;

        for (int i = 0; i < n; i++) {
            res[i] = cur;
            if (cur * 10 <= n) {
                cur *= 10;
            } else {
                if (cur >= n) cur /= 10;
                cur += 1;
                while (cur % 10 == 0) cur /= 10;
            }
        }
        return res;
    }
};

/**
 * Improved version. Make it blazing fast.
 */
class Solution {
public:
    vector<int> lexicalOrder(int n) {
        vector<int> ans(n, 0);
        int count = 0;
        int seq = 1;
        while (count < n) {
            if (seq > n) {
                seq = seq / 10;
                int bar = seq + 10;
                bar -= bar % 10;
                for (int i = seq + 1; i < bar && i <= n; ++i) {
                    ans[count] = i;
                    ++count;
                }

                seq = bar / 10;
                while ((seq % 10) == 0) {
                    seq /= 10;
                }
            }
            else {
                ans[count] = seq;
                ++count;
                seq *= 10;
            }
        }

        return ans;
    }
};

/**
 * Not so clever method.
 */
class Solution {
public:
    vector<int> lexicalOrder(int n) {
        vector<int> ans(n, 0);
        for (int i = 0; i < ans.size(); ++i) {
            ans[i] = i + 1;
        }

        sort(ans.begin(), ans.end(), [](auto a, auto b) {
            return to_string(a) < to_string(b) ? true : false;
        });

        return ans;
    }
};
