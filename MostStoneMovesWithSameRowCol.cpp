/**
 * Most Stones Removed with Same Row or Column
 *
 * On a 2D plane, we place stones at some integer coordinate points.  Each coordinate point may have at most one stone.
 *
 * Now, a move consists of removing a stone that shares a column or row with another stone on the grid.
 *
 * What is the largest possible number of moves we can make?
 *
 * Example 1:
 *
 * Input: stones = [[0,0],[0,1],[1,0],[1,2],[2,1],[2,2]]
 * Output: 5
 *
 * Example 2:
 *
 * Input: stones = [[0,0],[0,2],[1,1],[2,0],[2,2]]
 * Output: 3
 *
 * Example 3:
 *
 * Input: stones = [[0,0]]
 * Output: 0
 *
 * Note:
 *
 *     1 <= stones.length <= 1000
 *     0 <= stones[i][j] < 10000
 */
/**
 * DFS method.
 */
class Solution {
public:
    int dfs(unordered_map<int, vector<int>> &rows, unordered_map<int, vector<int>> &cols, unordered_set<int> &v_rows, int row) {
        auto res = 0;
        if (v_rows.insert(row).second) {
            res += rows[row].size();
            for (auto c : rows[row])
            for (auto r : cols[c]) res += dfs(rows, cols, v_rows, r);
        }
        return res;
    }
    int removeStones(vector<vector<int>>& stones) {
        unordered_map<int, vector<int>> rows, cols;
        unordered_set<int> v_rows;
        auto res = 0;
        for (auto s : stones) rows[s[0]].push_back(s[1]), cols[s[1]].push_back(s[0]);
        for (auto r : rows) res += max(0, dfs(rows, cols, v_rows, r.first) - 1);
        return res;
    }
};

/**
 * Union method. Really neat and interesting.
 */
class Solution {
public:
    int removeStones(vector<vector<int>>& stones) {
        for (int i = 0; i < stones.size(); ++i)
            uni(stones[i][0], ~stones[i][1]);
        return stones.size() - islands;
    }

    unordered_map<int, int> f;
    int islands = 0;

    int find(int x) {
        if (!f.count(x)) f[x] = x, islands++;
        if (x != f[x]) f[x] = find(f[x]);
        return f[x];
    }

    void uni(int x, int y) {
        x = find(x), y = find(y);
        if (x != y) f[x] = y, islands--;
    }
};

/**
 * Original solution. Flood method.
 */
class Solution {
public:
    struct Point
    {
        Point(int x, int y) : x{x}, y{y}, visited{false} {}
        int x;
        int y;
        bool visited;
    };
    int removeStones(vector<vector<int>>& stones) {
        map<int, vector<shared_ptr<Point> > > rows;
        map<int, vector<shared_ptr<Point> > > cols;
        map<int, bool> rowVisited;
        map<int, bool> colVisited;
        vector<shared_ptr<Point> > points;
        for (auto& p : stones) {
            auto point = shared_ptr<Point>(new Point(p[0], p[1]));
            rows[point->x].push_back(point);
            cols[point->y].push_back(point);
            points.push_back(point);
        }

        int ans = 0;
        for (auto& p : points) {
            if (true == p->visited) continue;

            int moves = 0;
            deque<shared_ptr<Point> > q{p};
            while (false == q.empty()) {
                shared_ptr<Point> tp = q.front();
                q.pop_front();
                if (false == rowVisited[tp->x]) {
                    for (auto& rp : rows[tp->x]) {
                        if (false == rp->visited) {
                            ++moves;
                            rp->visited = true;
                            q.push_back(rp);
                        }
                    }
                    rowVisited[tp->x] = true;
                }

                if (false == colVisited[tp->y]) {
                    for (auto& cp : cols[tp->y]) {
                        if (false == cp->visited) {
                            ++moves;
                            cp->visited = true;
                            q.push_back(cp);
                        }
                    }
                    colVisited[tp->y] = true;
                }
            }
            ans += max(0, moves - 1);
        }

        return ans;
    }
};
