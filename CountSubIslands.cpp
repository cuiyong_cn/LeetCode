/**
 * Count Sub Islands
 *
 * You are given two m x n binary matrices grid1 and grid2 containing only 0's (representing water)
 * and 1's (representing land). An island is a group of 1's connected 4-directionally (horizontal or
 * vertical). Any cells outside of the grid are considered water cells.
 *
 * An island in grid2 is considered a sub-island if there is an island in grid1 that contains all
 * the cells that make up this island in grid2.
 *
 * Return the number of islands in grid2 that are considered sub-islands.
 *
 * Example 1:
 *
 * Input: grid1 = [[1,1,1,0,0],[0,1,1,1,1],[0,0,0,0,0],[1,0,0,0,0],[1,1,0,1,1]], grid2 =
 * [[1,1,1,0,0],[0,0,1,1,1],[0,1,0,0,0],[1,0,1,1,0],[0,1,0,1,0]]
 * Output: 3
 * Explanation: In the picture above, the grid on the left is grid1 and the grid on the right is grid2.
 * The 1s colored red in grid2 are those considered to be part of a sub-island. There are three sub-islands.
 * Example 2:
 *
 * Input: grid1 = [[1,0,1,0,1],[1,1,1,1,1],[0,0,0,0,0],[1,1,1,1,1],[1,0,1,0,1]], grid2 =
 * [[0,0,0,0,0],[1,1,1,1,1],[0,1,0,1,0],[0,1,0,1,0],[1,0,0,0,1]]
 *
 * Output: 2
 * Explanation: In the picture above, the grid on the left is grid1 and the grid on the right is grid2.
 * The 1s colored red in grid2 are those considered to be part of a sub-island. There are two sub-islands.
 *
 * Constraints:
 *
 * m == grid1.length == grid2.length
 * n == grid1[i].length == grid2[i].length
 * 1 <= m, n <= 500
 * grid1[i][j] and grid2[i][j] are either 0 or 1.
 */
/**
 * Floodfill, DFS. Passed.
 */
bool dfs(vector<vector<int>>& grid1, vector<vector<int>>& grid2, int r, int c)
{
    if (r < 0 || r >= grid1.size() || c < 0 || c >= grid1[0].size() || 0 == grid2[r][c]) {
        return true;
    }

    grid2[r][c] = 0;

    auto left = dfs(grid1, grid2, r, c - 1);
    auto up = dfs(grid1, grid2, r - 1, c);
    auto right = dfs(grid1, grid2, r, c + 1);
    auto down = dfs(grid1, grid2, r + 1, c);

    return 1 == grid1[r][c] && left && right && up && down;
}

class Solution {
public:
    int countSubIslands(vector<vector<int>>& grid1, vector<vector<int>>& grid2) {
        int ans = 0;

        int const rows = grid2.size();
        int const cols = grid2[0].size();

        for (int r = 0; r < rows; ++r) {
            for (int c = 0; c < cols; ++c) {
                if (grid2[r][c] <= 0) {
                    continue;
                }

                if (dfs(grid1, grid2, r, c)) {
                    ++ans;
                }
            }
        }

        return ans;
    }
};

/**
 * Original solution. TLE. Floodfill solution. BFS
 */
class Solution {
public:
    int countSubIslands(vector<vector<int>>& grid1, vector<vector<int>>& grid2) {
        int ans = 0;

        int const rows = grid2.size();
        int const cols = grid2[0].size();

        for (int r = 0; r < rows; ++r) {
            for (int c = 0; c < cols; ++c) {
                if (grid2[r][c] <= 0) {
                    continue;
                }

                // floodfill the island and check if the island cell
                // also appears in grid1
                bool sub_island = true;
                deque<pair<int, int>> dq{{r, c}};
                while (!dq.empty()) {
                    auto [x, y] = dq.front();
                    if (0 == grid1[x][y]) {
                        sub_island = false;
                    }

                    grid2[x][y] = -1;
                    dq.pop_front();
                    if ((y - 1) >= 0 && 1 == grid2[x][y - 1]) {
                        dq.emplace_back(x, y - 1);
                    }
                    if ((y + 1) < cols && 1 == grid2[x][y + 1]) {
                        dq.emplace_back(x, y + 1);
                    }
                    if ((x - 1) >= 0 && 1 == grid2[x - 1][y]) {
                        dq.emplace_back(x - 1, y);
                    }
                    if ((x + 1) < rows && 1 == grid2[x + 1][y]) {
                        dq.emplace_back(x + 1, y);
                    }
                }

                if (sub_island) { // all the land cell appeared in grid1 too.
                    //cout << "row: " << r << ", col: " << c << '\n';
                    ++ans;
                }
            }
        }

        return ans;
    }
};
