/**
 * Minimum Number of Work Sessions to Finish the Tasks
 *
 * There are n tasks assigned to you. The task times are represented as an integer array tasks of
 * length n, where the ith task takes tasks[i] hours to finish. A work session is when you work for
 * at most sessionTime consecutive hours and then take a break.
 * 
 * You should finish the given tasks in a way that satisfies the following conditions:
 * 
 * If you start a task in a work session, you must complete it in the same work session.  You can
 * start a new task immediately after finishing the previous one.  You may complete the tasks in any
 * order.  Given tasks and sessionTime, return the minimum number of work sessions needed to finish
 * all the tasks following the conditions above.
 * 
 * The tests are generated such that sessionTime is greater than or equal to the maximum element in
 * tasks[i].
 * 
 * Example 1:
 * 
 * Input: tasks = [1,2,3], sessionTime = 3
 * Output: 2
 * Explanation: You can finish the tasks in two work sessions.
 * - First work session: finish the first and the second tasks in 1 + 2 = 3 hours.
 * - Second work session: finish the third task in 3 hours.
 * Example 2:
 * 
 * Input: tasks = [3,1,3,1,1], sessionTime = 8
 * Output: 2
 * Explanation: You can finish the tasks in two work sessions.
 * - First work session: finish all the tasks except the last one in 3 + 1 + 3 + 1 = 8 hours.
 * - Second work session: finish the last task in 1 hour.
 * Example 3:
 * 
 * Input: tasks = [1,2,3,4,5], sessionTime = 15
 * Output: 1
 * Explanation: You can finish all the tasks in one work session.
 * 
 * Constraints:
 * 
 * n == tasks.length
 * 1 <= n <= 14
 * 1 <= tasks[i] <= 10
 * max(tasks[i]) <= sessionTime <= 15*
 */
/**
 * My first attempt is greedy method. But failed.
 * Greedy method thought(wrong method though):
 *  1. sort the array.
 *  2. start from the longest task, and find next one can be completed within same session.
 *  3. repeat the step2, until no task can be filled in session
 *  4. start a new session, repeat stpe 2,3
 *
 * But the above succeed for most the test cases, but still not the correct one.
 *
 * So Here is the real solution from Discussion.
 */
class Solution {
public:
    vector<int> sessions;
    unordered_map<string, int> dp;
    
    string encodeState(int pos, vector<int> const& sessions) {
        auto s = sessions;
        sort(s.begin(), s.end());

        ostringstream oss;
        oss << pos << '$';

        std::copy(s.begin(), s.end(), ostream_iterator<int>(oss, "$"));
        
        return oss.str();
    }

    int solve(vector<int>& tasks, int pos) {
        if (pos >= tasks.size()) {
            return 0;;
        }

        auto key = encodeState(pos, sessions);
        if (dp.count(key)) {
            return dp[key];
        }

        sessions.push_back(tasks[pos]);
        int ans = 1 + solve(tasks, pos + 1);
        sessions.pop_back();

        for (auto& s : sessions) {
            if ((s + tasks[pos]) <= stime) {
                s += tasks[pos];
                ans = min(ans, solve(tasks,  pos + 1));
                s -= tasks[pos];
            }
        }

        dp[key] = ans;

        return ans;
    }
    
    int minSessions(vector<int>& tasks, int sessionTime) {
        stime = sessionTime;
        return solve(tasks, 0);
    }

private:
    int stime;
};
