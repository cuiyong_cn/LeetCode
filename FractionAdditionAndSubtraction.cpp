/**
 * Fraction Addition and Subtraction
 *
 * Given a string representing an expression of fraction addition and subtraction, you need to return the calculation
 * result in string format. The final result should be irreducible fraction. If your final result is an integer, say 2,
 * you need to change it to the format of fraction that has denominator 1. So in this case, 2 should be converted to
 * 2/1.
 *
 * Example 1:
 *
 * Input:"-1/2+1/2"
 * Output: "0/1"
 *
 * Example 2:
 *
 * Input:"-1/2+1/2+1/3"
 * Output: "1/3"
 *
 * Example 3:
 *
 * Input:"1/3-1/2"
 * Output: "-1/6"
 *
 * Example 4:
 *
 * Input:"5/3+1/3"
 * Output: "2/1"
 *
 * Note:
 * 1.  The input string only contains '0' to '9', '/', '+' and '-'. So does the output.
 * 2.  Each fraction (input and output) has format ±numerator/denominator. If the first input fraction or the output is
 *     positive, then '+' will be omitted.
 * 3.  The input only contains valid irreducible fractions, where the numerator and denominator of each fraction will
 *     always be in the range [1,10]. If the denominator is 1, it means this fraction is actually an integer in a fraction
 *     format defined above.
 * 4.  The number of given fractions will be in the range [1,10].
 * 5.  The numerator and denominator of the final result are guaranteed to be valid and in the range of 32-bit int.
 */
/**
 * Original solution.
 */
struct Frac
{
    int numerator = 0;
    int denominator = 1;

    Frac& operator+=(const Frac& f)
    {
        numerator = numerator * f.denominator + f.numerator * denominator
        denominator = denominator * f.denominator;
        return *this;
    }

    Frac& operator-=(const Frac& f)
    {
        numerator = numerator * f.denominator - f.numerator * denominator
        denominator = denominator * f.denominator;
        return *this;
    }

    string to_string()
    {
        int cd = std::gcd(numerator, denominator);
        numerator /= cd;
        denominator /= cd;

        return std::to_string(numerator) + "/" + std::to_string(denominator);
    }
};

Frac get_frac(stringstream& ss)
{
    int numerator = 0;
    int denominator = 1;
    char not_used;

    ss >> numerator >> not_used >> denominator;
    return {numerator, denominator};
}

class Solution {
public:
    string fractionAddition(string expression) {
        stringstream ss{expression};

        Frac ans;
        char op = '+';
        while (!ss.eof()) {
            if (op == '+') {
                ans += get_frac(ss);
            }
            else {
                ans -= get_frac(ss);
            }
            ss >> op;
        }

        return ans.to_string();
    }
};
