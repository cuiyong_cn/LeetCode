/**
 * Validate Binary Tree Nodes
 *
 * You have n binary tree nodes numbered from 0 to n - 1 where node i has two children leftChild[i] and rightChild[i],
 * return true if and only if all the given nodes form exactly one valid binary tree.
 *
 * If node i has no left child then leftChild[i] will equal -1, similarly for the right child.
 *
 * Note that the nodes have no values and that we only use the node numbers in this problem.
 *
 * Example 1:
 *
 * Input: n = 4, leftChild = [1,-1,3,-1], rightChild = [2,-1,-1,-1]
 * Output: true
 *
 * Example 2:
 *
 * Input: n = 4, leftChild = [1,-1,3,-1], rightChild = [2,3,-1,-1]
 * Output: false
 *
 * Example 3:
 *
 * Input: n = 2, leftChild = [1,0], rightChild = [-1,-1]
 * Output: false
 *
 * Example 4:
 *
 * Input: n = 6, leftChild = [1,-1,-1,4,-1,-1], rightChild = [2,-1,-1,5,-1,-1]
 * Output: false
 *
 * Constraints:
 *     1 <= n <= 10^4
 *     leftChild.length == rightChild.length == n
 *     -1 <= leftChild[i], rightChild[i] <= n - 1
 */
/**
 * Original solution. The idea is:
 * 1. every node number should appear only once.
 * 2. There should exists only one root node.
 * 3. The leaf node should have parent
 */
class Solution {
public:
    bool validateBinaryTreeNodes(int n, vector<int>& leftChild, vector<int>& rightChild) {
        vector<int> nodes(n, 0);
        vector<int> leaves;
        for (size_t i = 0; i < n; ++i) {
            if (leftChild[i] != -1) {
                if (nodes[leftChild[i]])
                    return false;
                nodes[leftChild[i]] = 1;
            }

            if (rightChild[i] != -1) {
                if (nodes[rightChild[i]])
                    return false;
                nodes[rightChild[i]] = 1;
            }

            if (leftChild[i] == rightChild[i]) {
                leaves.push_back(i);
            }
        }

        int root_node = -1;

        for (size_t i = 0; i < n; ++i) {
            if (nodes[i] == 0) {
                if (root_node >= 0) return false;
                root_node = i;
            }
        }

        if (root_node == -1) {
            return false;
        }

        if (n > 1) {
            for (auto nd : leaves) {
                if (nd == root_node || !nodes[nd]) {
                    return false;
                }
            }
        }

        return true;
    }
};
