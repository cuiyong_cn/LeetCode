/**
 * Same Tree
 *
 * Given two binary trees, write a function to check if they are the same or not.
 *
 * Two binary trees are considered the same if they are structurally identical and the nodes have the same value.
 *
 * Example 1:
 *
 * Input:     1         1
 *           / \       / \
 *          2   3     2   3
 *
 *         [1,2,3],   [1,2,3]
 *
 * Output: true
 *
 * Example 2:
 *
 * Input:     1         1
 *           /           \
 *          2             2
 *
 *         [1,2],     [1,null,2]
 *
 * Output: false
 *
 * Example 3:
 *
 * Input:     1         1
 *           / \       / \
 *          2   1     1   2
 *
 *         [1,2,1],   [1,1,2]
 *
 * Output: false
 */
/**
 * Stack version.
 */
class Solution {
public:
    bool isSameTree(TreeNode* p, TreeNode* q) {
        bool isSame = true;
        stack<TreeNode*> stkp;
        stack<TreeNode*> stkq;
        stkp.push(p);
        stkq.push(q);

        while (false == stkp.empty()) {
            TreeNode* tp = stkp.top(); stkp.pop();
            TreeNode* tq = stkq.top(); stkq.pop();

            if (nullptr == tp || nullptr == tq) {
                if (tp != tq) {
                    isSame = false;
                    break;
                }
            }
            else {
                if (tp->val != tq->val) {
                    isSame = false;
                    break;
                }
                else {
                    stkp.push(tp->left);
                    stkp.push(tp->right);
                    stkq.push(tq->left);
                    stkq.push(tq->right);
                }
            }
        }

        return isSame;
    }
};

/**
 * original solution. Recursive one.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    bool isSameTree(TreeNode* p, TreeNode* q) {
        bool isSame = true;
        if (nullptr == p || nullptr == q) {
            if (p != q) isSame = false;
        }
        else {
            if (p->val != q->val) {
                isSame = false;
            }
            else {
                isSame = isSameTree(p->left, q->left);
                if (isSame) {
                    isSame = isSameTree(p->right, q->right);
                }
            }
        }

        return isSame;
    }
};
