/**
 * Create Binary Tree From Descriptions
 *
 * You are given a 2D integer array descriptions where descriptions[i] = [parenti, childi, isLefti]
 * indicates that parenti is the parent of childi in a binary tree of unique values. Furthermore,
 *
 * If isLefti == 1, then childi is the left child of parenti.
 * If isLefti == 0, then childi is the right child of parenti.
 * Construct the binary tree described by descriptions and return its root.
 *
 * The test cases will be generated such that the binary tree is valid.
 *
 * Input: descriptions = [[20,15,1],[20,17,0],[50,20,1],[50,80,0],[80,19,1]]
 * Output: [50,20,80,15,17,19]
 * Explanation: The root node is the node with value 50 since it has no parent.
 * The resulting binary tree is shown in the diagram.
 * Example 2:
 *
 * Input: descriptions = [[1,2,1],[2,3,0],[3,4,1]]
 * Output: [1,2,null,null,3,4]
 * Explanation: The root node is the node with value 1 since it has no parent.
 * The resulting binary tree is shown in the diagram.
 *
 * Constraints:
 *
 * 1 <= descriptions.length <= 104
 * descriptions[i].length == 3
 * 1 <= parenti, childi <= 105
 * 0 <= isLefti <= 1
 * The binary tree described by descriptions is valid.
 */
/**
 * Original solution.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
 TreeNode* get_node(unordered_map<int, TreeNode*>& nodes, int key)
 {
     if (0 == nodes.count(key)) {
         nodes[key] = new TreeNode{key};
     }
     return nodes[key];
 }

int root(unordered_map<int, int> const& parent, int node)
{
     while (parent.count(node)) {
         node = parent.at(node);
     }
     return node;
}

bool is_left(int v)
{
    return 1 == v;
}

class Solution {
public:
    TreeNode* createBinaryTree(vector<vector<int>>& descriptions) {
        auto nodes = unordered_map<int, TreeNode*>{};
        auto p_nodes = unordered_map<int, int>{};
        for (auto const& desc : descriptions) {
            auto parent = get_node(nodes, desc[0]);
            auto child = get_node(nodes, desc[1]);
            p_nodes[child->val] = parent->val;
            if (is_left(desc[2])) {
                parent->left = child;
            }
            else {
                parent->right = child;
            }
        }

        return get_node(nodes, root(p_nodes, descriptions[0][0]));
    }
};
