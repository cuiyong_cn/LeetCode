/**
 * Recover Binary Search Tree
 *
 * You are given the root of a binary search tree (BST), where exactly two nodes of the tree were
 * swapped by mistake. Recover the tree without changing its structure.
 *
 * Follow up: A solution using O(n) space is pretty straight forward. Could you devise a constant
 * space solution?
 *
 * Example 1:
 *
 * Input: root = [1,3,null,null,2]
 * Output: [3,1,null,null,2]
 * Explanation: 3 cannot be a left child of 1 because 3 > 1. Swapping 1 and 3 makes the BST valid.
 * Example 2:
 *
 * Input: root = [3,1,4,null,null,2]
 * Output: [2,1,4,null,null,3]
 * Explanation: 2 cannot be in the right subtree of 3 because 2 < 3. Swapping 2 and 3 makes the BST valid.
 *
 * Constraints:
 *
 * The number of nodes in the tree is in the range [2, 1000].
 * -231 <= Node.val <= 231 - 1
 */
/**
 * O(1) space solution. Using morris traversal.
 */
class Solution {
public:
    void recoverTree(TreeNode* root) {
        TreeNode* pre = nullptr;
        TreeNode* first = nullptr;
        TreeNode* second = nullptr;

        while (root) {
            if (root->left) {
                auto temp = root->left;
                while (temp->right && temp->right != root) {
                    temp = temp->right;
                }

                if (temp->right) {
                    temp->right = nullptr;
                } else {
                    temp->right = root;
                    root = root->left;
                    continue;
                }
            }

            if (pre && pre->val > root->val) {
                if (nullptr == first) {
                    first = pre;
                }
                second = root;
            }

            pre = root;
            root = root->right;
        }

        swap(first->val, second->val);
    }
};

/**
 * Original solution.
 *
 * Think backwards.
 * If the BST is not corrupt. Then inorder traversal will give us a sorted array.
 * Now swap two nodes. Think how we would find these two.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    void recoverTree(TreeNode* root) {
        vector<TreeNode*> nodes;
        inorder_traversal(root, nodes);

        int left = 0;
        for (; left < nodes.size(); ++left) {
            if (nodes[left]->val > nodes[left + 1]->val) {
                break;
            }
        }

        int right = nodes.size() - 1;
        for (; right > 0; --right) {
            if (nodes[right]->val < nodes[right - 1]->val) {
                break;
            }
        }

        swap(nodes[left]->val, nodes[right]->val);
    }

private:
    void inorder_traversal(TreeNode* root, vector<TreeNode*>& nodes)
    {
        if (nullptr == root) {
            return;
        }

        inorder_traversal(root->left, nodes);
        nodes.push_back(root);
        inorder_traversal(root->right, nodes);
    }
};
