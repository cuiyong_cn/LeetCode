/**
 * Single Number III
 *
 * Given an array of numbers nums, in which exactly two elements appear only once and all
 * the other elements appear exactly twice. Find the two elements that appear only once.
 *
 * Example:
 *
 * Input:  [1,2,1,3,2,5]
 * Output: [3,5]
 *
 * Note:
 *
 *     The order of the result is not important. So in the above example, [5, 3] is also
 *     correct.
 *     Your algorithm should run in linear runtime complexity. Could you implement it
 *     using only constant space complexity?
 */
/**
 * Another xor approach, really neat. The idea behind this:
 * 1. assume that A and B are the two elements which we want to find;
 * 2. use XOR for all elements,the result is : r = A^B,we just need to distinguish A from B next step
 * 3. if we can find a bit '1' in r,then the bit in corresponding position in A and B must
 *    be different.We can use {last = r & (~(r-1))} to get the last bit 1 int r;
 * 4. we use last to divide all numbers into two groups,then A and B must fall into the
 *    two distrinct groups.XOR elements in eash group,get the A and B.
 */
class Solution {
public:
    vector<int> singleNumber(vector<int>& nums) {
        int r = 0, last = 0;
        vector<int> ans(2, 0);

        for (auto& n : nums) {
            r ^= n;
        }
        // or we can use last = r & (-r)
        // or we can use last = r ^ (r & (r - 1))
        last = r & (~(r - 1));
        for (auto& n : nums) {
            if ((last & n) == 0) {
                ans[0] ^= n;
            }
            else {
                ans[1] ^= n;
            }
        }

        return ans;
    }
};

/**
 * Original version using unordered_set approach.
 */
class Solution {
public:
    vector<int> singleNumber(vector<int>& nums) {
        unordered_set<int> uset;
        vector<int> ans;
        for (auto& n : nums) {
            if (0 == uset.count(n)) {
                uset.insert(n);
            }
            else {
                uset.erase(n);
            }
        }
        for (auto& n : uset) {
            ans.push_back(n);
        }
        return ans;
    }
};
