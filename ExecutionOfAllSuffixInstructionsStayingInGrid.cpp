/**
 * Execution of All Suffix Instructions Staying in a Grid
 *
 * There is an n x n grid, with the top-left cell at (0, 0) and the bottom-right cell at (n - 1, n -
 * 1). You are given the integer n and an integer array startPos where startPos = [startrow,
 * startcol] indicates that a robot is initially at cell (startrow, startcol).
 *
 * You are also given a 0-indexed string s of length m where s[i] is the ith instruction for the
 * robot: 'L' (move left), 'R' (move right), 'U' (move up), and 'D' (move down).
 *
 * The robot can begin executing from any ith instruction in s. It executes the instructions one by
 * one towards the end of s but it stops if either of these conditions is met:
 *
 * The next instruction will move the robot off the grid.
 * There are no more instructions left to execute.
 * Return an array answer of length m where answer[i] is the number of instructions the robot can
 * execute if the robot begins executing from the ith instruction in s.
 *
 * Example 1:
 *
 * Input: n = 3, startPos = [0,1], s = "RRDDLU"
 * Output: [1,5,4,3,1,0]
 * Explanation: Starting from startPos and beginning execution from the ith instruction:
 * - 0th: "RRDDLU". Only one instruction "R" can be executed before it moves off the grid.
 * - 1st:  "RDDLU". All five instructions can be executed while it stays in the grid and ends at (1, 1).
 * - 2nd:   "DDLU". All four instructions can be executed while it stays in the grid and ends at (1, 0).
 * - 3rd:    "DLU". All three instructions can be executed while it stays in the grid and ends at (0, 0).
 * - 4th:     "LU". Only one instruction "L" can be executed before it moves off the grid.
 * - 5th:      "U". If moving up, it would move off the grid.
 * Example 2:
 *
 * Input: n = 2, startPos = [1,1], s = "LURD"
 * Output: [4,1,0,0]
 * Explanation:
 * - 0th: "LURD".
 * - 1st:  "URD".
 * - 2nd:   "RD".
 * - 3rd:    "D".
 * Example 3:
 *
 * Input: n = 1, startPos = [0,0], s = "LRUD"
 * Output: [0,0,0,0]
 * Explanation: No matter which instruction the robot begins execution from, it would move off the grid.
 *
 * Constraints:
 *
 * m == s.length
 * 1 <= n, m <= 500
 * startPos.length == 2
 * 0 <= startrow, startcol < n
 * s consists of 'L', 'R', 'U', and 'D'.
 */
/**
 * WTF one. I need time to digest this one.
 */
vector<int> executeInstructions(int n, vector<int>& st, string s) {
    int m = s.size(), h = m + n, v = m + n;
    vector<int> hor((m + n) * 2, m), ver((m + n) * 2, m), res(m);
    for (int i = m - 1; i >= 0; --i) {
        hor[h] = ver[v] = i;
        h += s[i] == 'L' ? 1 : s[i] == 'R' ? -1 : 0;
        v += s[i] == 'U' ? 1 : s[i] == 'D' ? -1 : 0;
        res[i] = min({m, hor[h - st[1] - 1], hor[h - st[1] + n], ver[v - st[0] - 1], ver[v - st[0] + n]}) - i;
    }
    return res;
}

/**
 * Original solution.
 */
struct Robot
{
    Robot& toward(char dir)
    {
        if ('L' == dir) {
            --y;
        }
        else if ('R' == dir) {
            ++y;
        }
        else if ('U' == dir) {
            --x;
        }
        else {
            ++x;
        }
        return *this;
    }

    bool out_of_grid() const
    {
        return x < 0 || x >= n || y < 0 || y >= n;
    }

    int x;
    int y;
    int n;
};

class Solution {
public:
    vector<int> executeInstructions(int n, vector<int>& startPos, string s) {
        auto ssize = static_cast<int>(s.length());
        auto ans = vector<int>(ssize, 0);
        for (auto i = int{0}; i < ssize; ++i) {
            auto robot = Robot{startPos[0], startPos[1], n};
            auto moves = int{0};
            for (auto j = i; j < ssize; ++j) {
                if (robot.toward(s[j]).out_of_grid()) {
                    break;
                }

                ++moves;
            }
            ans[i] = moves;
        }
        return ans;
    }
};
