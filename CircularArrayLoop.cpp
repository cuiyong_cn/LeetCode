/**
 * Circular Array Loop
 *
 * You are playing a game involving a circular array of non-zero integers nums. Each nums[i] denotes
 * the number of indices forward/backward you must move if you are located at index i:
 *
 * If nums[i] is positive, move nums[i] steps forward, and
 * If nums[i] is negative, move nums[i] steps backward.
 * Since the array is circular, you may assume that moving forward from the last element puts you on
 * the first element, and moving backwards from the first element puts you on the last element.
 *
 * A cycle in the array consists of a sequence of indices seq of length k where:
 *
 * Following the movement rules above results in the repeating index sequence seq[0] -> seq[1] ->
 * ... -> seq[k - 1] -> seq[0] -> ...
 * Every nums[seq[j]] is either all positive or all negative.
 * k > 1
 * Return true if there is a cycle in nums, or false otherwise.
 *
 * Example 1:
 *
 * Input: nums = [2,-1,1,2,2]
 * Output: true
 * Explanation:
 * There is a cycle from index 0 -> 2 -> 3 -> 0 -> ...
 * The cycle's length is 3.
 * Example 2:
 *
 * Input: nums = [-1,2]
 * Output: false
 * Explanation:
 * The sequence from index 1 -> 1 -> 1 -> ... is not a cycle because the sequence's length is 1.
 * By definition the sequence's length must be strictly greater than 1 to be a cycle.
 * Example 3:
 *
 * Input: nums = [-2,1,-1,-2,-2]
 * Output: false
 * Explanation:
 * The sequence from index 1 -> 2 -> 1 -> ... is not a cycle because nums[1] is positive, but
 * nums[2] is negative.
 * Every nums[seq[j]] must be either all positive or all negative.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 5000
 * -1000 <= nums[i] <= 1000
 * nums[i] != 0
 *
 * Follow up: Could you solve it in O(n) time complexity and O(1) extra space complexity?
 */
/**
 * Original solution. We can also use a * b > 0 to test if they have the same sign.
 */
class Solution {
public:
    bool circularArrayLoop(vector<int>& nums) {
        int const N = nums.size();
        for (auto& n : nums) {
            n %= N;
        }

        for (int i = 0; i < N; ++i) {
            if (nums[i] == 0) {
                continue;
            }

            int j = i, k = jump(nums, i);
            while (((nums[i] > 0) == (nums[k] > 0))
                  && ((nums[jump(nums, k)] > 0) == (nums[i] > 0))) {
                if (j == k) {
                    if (j == jump(nums, j)) {
                        break;
                    }
                    return true;
                }

                j = jump(nums, j);
                k = jump(nums, jump(nums, k));
            }

            // there is no loop start from i, set all element along to 0
            j = i;
            int val = nums[i];
            while (nums[j] != 0 && (nums[j] > 0) == (val > 0)) {
                nums[j] = 0;
                j = jump(nums, j);
            }
        }

        return false;
    }

private:
    int jump(vector<int> const& nums, int pos)
    {
        int const N = nums.size();
        return (pos + nums[pos] + N) % N;
    }
};
