/**
 * Jump Game
 *
 * Given an array of non-negative integers nums, you are initially positioned at the first index of
 * the array.
 *
 * Each element in the array represents your maximum jump length at that position.
 *
 * Determine if you are able to reach the last index.
 *
 * Example 1:
 *
 * Input: nums = [2,3,1,1,4]
 * Output: true
 * Explanation: Jump 1 step from index 0 to 1, then 3 steps to the last index.
 * Example 2:
 *
 * Input: nums = [3,2,1,0,4]
 * Output: false
 * Explanation: You will always arrive at index 3 no matter what. Its maximum jump length is 0,
 * which makes it impossible to reach the last index.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 104
 * 0 <= nums[i] <= 105
 */
/**
 * More clearer and shorter one.
 */
class Solution {
public:
    bool canJump(vector<int> const& nums) {
        int const N = nums.size();
        int i = 0;
        for (int reach = 0; i < N && i <= reach; ++i) {
            auto farest = i + nums[i];
            if (farest > reach) {
                reach = farest;
            }
        }

        return i == N;
    }
};

/**
 * Depth first passed. - -!
 */
class Solution {
public:
    bool canJump(vector<int>& nums) {
        int end = nums.size() - 1;
        vector<int> jump_from = {0};
        vector<bool> visited(nums.size(), false);

        while (!jump_from.empty()) {
            auto s = jump_from.back();
            jump_from.pop_back();

            if (s == end) {
                return true;
            }

            if (visited[s]) {
                continue;
            }

            visited[s] = true;
            auto steps = nums[s];
            for (int i = 1; i <= steps; ++i) {
                auto to = s + i;
                if (to > end) {
                    break;
                }

                jump_from.push_back(to);
            }
        }

        return false;
    }
};

/**
 * TLE again.
 */
class Solution {
public:
    bool canJump(vector<int>& nums) {
        int end = nums.size() - 1;
        deque<int> jump_from = {0};
        vector<bool> visited(nums.size(), false);

        while (!jump_from.empty()) {
            auto s = jump_from.front(); jump_from.pop_front();
            if (end == s) {
                return true;
            }

            if (false == visited[s]) {
                visited[s] = true;
                auto steps = nums[s];
                for (int i = 1; i <= steps; ++i) {
                    int to = s + i;
                    if (to <= end) {
                        jump_from.push_back(to);
                    }
                }
            }
        }

        return false;
    }
};

/**
 * Original solution. TLE.
 */
class Solution {
public:
    bool canJump(vector<int>& nums) {
        int end = nums.size() - 1;
        vector<int> jump_from = {0};
        vector<bool> visited(nums.size(), false);
        visited[0] = true;

        while (!jump_from.empty()) {
            vector<int> next_jump;
            for (auto s : jump_from) {
                if (end == s) {
                    return true;
                }

                auto steps = nums[s];
                for (int i = 1; i <= steps; ++i) {
                    int to = s + i;
                    if (to <= end && false == visited[to]) {
                        visited[to] = true;
                        next_jump.push_back(to);
                    }
                }
            }

            swap(next_jump, jump_from);
        }

        return false;
    }
};
