/**
 * Count Number of Nice Subarrays
 *
 * Given an array of integers nums and an integer k. A subarray is called nice if there are k odd numbers on it.
 *
 * Return the number of nice sub-arrays.
 *
 * Example 1:
 *
 * Input: nums = [1,1,2,1,1], k = 3
 * Output: 2
 * Explanation: The only sub-arrays with 3 odd numbers are [1,1,2,1] and [1,2,1,1].
 *
 * Example 2:
 *
 * Input: nums = [2,4,6], k = 1
 * Output: 0
 * Explanation: There is no odd numbers in the array.
 *
 * Example 3:
 *
 * Input: nums = [2,2,2,1,2,2,1,2,2,2], k = 2
 * Output: 16
 *
 * Constraints:
 *
 *     1 <= nums.length <= 50000
 *     1 <= nums[i] <= 10^5
 *     1 <= k <= nums.length
 */
/**
 * Another solution by user lee215, not very well explained, but the code is elegant.
 *
 * Explained by ashishjain87:
 * It appears that what you mean by atMost is the number of subarrays where count of odd numbers is greater than 0 and
 * less than equal to k. By subtracting atMost(A, k-1), you are effectively removing all subarrays with count of odd
 * numbers greater than 0 and less than or equal to k-1 thereby giving you all subarrays which have number of odd
 * numbers exactly equal to k. Interesting approach, thanks for sharing. It will be great if you can add this
 * explanation to your description in the post as some folks might not have encountered the other problems you were
 * using as a reference.
 */
class Solution {
public:
    int numberOfSubarrays(vector<int>& A, int k) {
        return atMost(A, k) - atMost(A, k - 1);
    }

    int atMost(vector<int>& A, int k) {
        int res = 0, i = 0, n = A.size();
        for (int j = 0; j < n; j++) {
            k -= A[j] % 2;
            while (k < 0)
                k += A[i++] % 2;
            res += j - i + 1;
        }
        return res;
    }
};

/**
 * Original solution. Record hte odd number index in original array.
 */
class Solution {
public:
    int numberOfSubarrays(vector<int>& nums, int k) {
        vector<int> oddIndex;
        for (int i = 0; i < nums.size(); ++i) {
            if ((nums[i] % 2) == 1) {
                oddIndex.push_back(i);
            }
        }

        if (oddIndex.size() < k) return 0;

        int ans = 0;
        for (int i = 0; i < (oddIndex.size() - k + 1); ++i) {
            int prefix = i > 0 ? (oddIndex[i] - oddIndex[i - 1] - 1) : oddIndex[i];
            int suffix = 0;
            if ((i + k) < oddIndex.size()) {
                suffix = oddIndex[i + k] - oddIndex[i + k - 1] - 1;
            }
            else {
                suffix = nums.size() - oddIndex[i + k - 1] - 1;
            }

            ans += (prefix + 1) * (suffix + 1);
        }

        return ans;
    }
};

/**
 * Deque solution. The idea is same as mine original solution. But more compact.
 */
class Solution {
public:
    int numberOfSubarrays(vector<int>& nums, int k, int res = 0) {
      deque<int> deq = { -1 };
      for (auto i = 0; i < nums.size(); ++i) {
        if (nums[i] % 2) deq.push_back(i);
        if (deq.size() > k + 1) deq.pop_front();
        if (deq.size() == k + 1) res += deq[1] - deq[0];
      }
      return res;
    }
};
