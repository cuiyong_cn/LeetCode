/**
 * Non-decreasing Array
 *
 * Given an array nums with n integers, your task is to check if it could become non-decreasing by
 * modifying at most one element.
 *
 * We define an array is non-decreasing if nums[i] <= nums[i + 1] holds for every i (0-based) such
 * that (0 <= i <= n - 2).
 *
 * Example 1:
 *
 * Input: nums = [4,2,3]
 * Output: true
 * Explanation: You could modify the first 4 to 1 to get a non-decreasing array.
 * Example 2:
 *
 * Input: nums = [4,2,1]
 * Output: false
 * Explanation: You can't get a non-decreasing array by modify at most one element.
 *
 * Constraints:
 *
 * n == nums.length
 * 1 <= n <= 104
 * -105 <= nums[i] <= 105
 */
/**
 * Optimize.
 */
class Solution {
public:
    bool checkPossibility(vector<int>& nums) {
        if (nums.size() < 3) {
            return true;
        }

        int const N = nums.size();
        if (nums[0] > nums[1]) {
            return is_non_decreasing(nums, 1, N);
        }

        if (nums[N - 2] > nums[N - 1]) {
            return is_non_decreasing(nums, 0, N - 1);
        }

        int const last = N - 2;
        for (int i = 1; i < last; ++i) {
            if (nums[i] > nums[i + 1]) {
                return ((nums[i - 1] <= nums[i + 1] && nums[i + 1] <= nums[i + 2])
                            || (nums[i] <= nums[i + 2]))
                    && is_non_decreasing(nums, i + 2, N)
            }
        }

        return true;
    }

private:
    bool is_non_decreasing(vector<int> const& nums, int start, int end)
    {
        int const last = end - 1;
        for (int i = start; i < last; ++i) {
            if (nums[i] > nums[i + 1]) {
                return false;
            }
        }

        return true;
    }
};
/**
 * Original solution.
 */
class Solution {
public:
    bool checkPossibility(vector<int>& nums) {
        if (nums.size() < 3) {
            return true;
        }

        int const N = nums.size();
        if (nums[0] > nums[1]) {
            return is_non_decreasing(nums, 1, N);
        }

        if (nums[N - 2] > nums[N - 1]) {
            return is_non_decreasing(nums, 0, N - 1);
        }

        int const last = N - 2;
        for (int i = 1; i < last; ++i) {
            if (nums[i] > nums[i + 1]) {
                return (nums[i - 1] <= nums[i + 1] && is_non_decreasing(nums, i + 1, N))
                    || (nums[i] <= nums[i + 2] && is_non_decreasing(nums, i + 2, N));
            }
        }

        return true;
    }

private:
    bool is_non_decreasing(vector<int> const& nums, int start, int end)
    {
        int const last = end - 1;
        for (int i = start; i < last; ++i) {
            if (nums[i] > nums[i + 1]) {
                return false;
            }
        }

        return true;
    }
};
