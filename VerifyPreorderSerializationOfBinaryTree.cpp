/**
 * Verify Preorder Serialization of a Binary Tree
 *
 * One way to serialize a binary tree is to use preorder traversal. When we encounter a non-null
 * node, we record the node's value. If it is a null node, we record using a sentinel value such as
 * '#'.
 *
 * For example, the above binary tree can be serialized to the string "9,3,4,#,#,1,#,#,2,#,6,#,#",
 * where '#' represents a null node.
 *
 * Given a string of comma-separated values preorder, return true if it is a correct preorder
 * traversal serialization of a binary tree.
 *
 * It is guaranteed that each comma-separated value in the string must be either an integer or a
 * character '#' representing null pointer.
 *
 * You may assume that the input format is always valid.
 *
 * For example, it could never contain two consecutive commas, such as "1,,3".
 *
 * Example 1:
 *
 * Input: preorder = "9,3,4,#,#,1,#,#,2,#,6,#,#"
 * Output: true
 * Example 2:
 *
 * Input: preorder = "1,#"
 * Output: false
 * Example 3:
 *
 * Input: preorder = "9,#,#,1"
 * Output: false
 *
 * Constraints:
 *
 * 1 <= preorder.length <= 104
 * preoder consist of integers in the range [0, 100] and '#' separated by commas ','.
 *
 * Follow up: Find an algorithm without reconstructing the tree.
 */
/**
 * Get rid of stack. The idea is:
 * 1. all non-null node provides 2 outdegree and 1 indegree (2 children and 1 parent), except root
 * 2. all null node provides 0 outdegree and 1 indegree (0 child and 1 parent).
 *
 * Suppose we try to build this tree. During building, we record the difference between out degree
 * and in degree diff = outdegree - indegree. When the next node comes, we then decrease diff by 1,
 * because the node provides an in degree. If the node is not null, we increase diff by 2, because it
 * provides two out degrees. If a serialization is correct, diff should never be negative and diff
 * will be zero when finished.
 */
class Solution {
public:
    bool isValidSerialization(string preorder) {
        int diff = 1;
        preorder += ',';
        auto prev = preorder.front();
        for (auto c : preorder) {
            if (',' == c) {
                if (--diff < 0) {
                    return false;
                }

                if ('#' != prev) {
                    diff += 2;
                }
            }
            prev = c;
        }

        return 0 == diff;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool isValidSerialization(string preorder) {
        vector<int> stk;
        int i = 0;
        while (i != string::npos) {
            if ('#' == preorder[i]) {
                while (!stk.empty() && 1 == stk.back()) {
                    stk.pop_back();
                }
                if (stk.empty()) {
                    return preorder.find(',', i) == string::npos;
                }
                stk.back() = 1;
            }
            else {
                stk.push_back(0);
            }
            i = preorder.find(',', i);
            if (i != string::npos) {
                ++i;
            }
        }

        return stk.empty();
    }
};
