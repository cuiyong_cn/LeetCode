/**
 * Kth Missing Positive Number
 *
 * Given an array arr of positive integers sorted in a strictly increasing order, and an integer k.
 *
 * Find the kth positive integer that is missing from this array.
 *
 * Example 1:
 *
 * Input: arr = [2,3,4,7,11], k = 5
 * Output: 9
 * Explanation: The missing positive integers are [1,5,6,8,9,10,12,13,...]. The 5th missing positive integer is 9.
 * Example 2:
 *
 * Input: arr = [1,2,3,4], k = 2
 * Output: 6
 * Explanation: The missing positive integers are [5,6,7,...]. The 2nd missing positive integer is 6.
 *
 * Constraints:
 *
 * 1 <= arr.length <= 1000
 * 1 <= arr[i] <= 1000
 * 1 <= k <= 1000
 * arr[i] < arr[j] for 1 <= i < j <= arr.length
 */
/**
 * O(log(n)) solution.
 */
class Solution {
public:
    int findKthPositive(vector<int>& A, int k) {
        int l = 0, r = A.size(), m;
        while (l < r) {
            m = (l + r) / 2;
            if (A[m] - 1 - m < k)
                l = m + 1;
            else
                r = m;
        }
        return l + k;
    }
};

/**
 * Simplify below solution. Idea is:
 * 1. because arr is sorted, if arr[0] < k, means all elements in arr are < k,
 * then the answer will be k.
 * 2. So, if this is one element < k, then answer is k + 1.
 * 3. following this logic, whenever we see a element < k, we increase it.
 */
class Solution {
public:
    int findKthPositive(vector<int>& arr, int k) {
        for (auto n : arr) {
            if (n <= k) {
                ++k;
            }
        }

        return k;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int findKthPositive(vector<int>& arr, int k) {
        int missing = 0;
        int i = 1;
        for (auto n : arr) {
            while (i < n) {
                ++missing;
                if (k == missing) {
                    return i;
                }
                ++i;
            }
            i = n + 1;
        }

        return (i - 1) + (k - missing);
    }
};
