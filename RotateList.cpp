/**
 * Rotate List
 *
 * Given the head of a linked list, rotate the list to the right by k places.
 *
 * Example 1:
 *
 * Input: head = [1,2,3,4,5], k = 2
 * Output: [4,5,1,2,3]
 * Example 2:
 *
 * Input: head = [0,1,2], k = 4
 * Output: [2,0,1]
 *
 * Constraints:
 *
 * The number of nodes in the list is in the range [0, 500].
 * -100 <= Node.val <= 100
 * 0 <= k <= 2 * 109*
 */
/**
 * Original solution.
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* rotateRight(ListNode* head, int k) {
        if (0 == k || nullptr == head) {
            return head;
        }

        auto [tail, len] = get_tail_and_len(head);
        k = len - (k % len);
        if (len == k) {
            return head;
        }

        tail->next = head;
        for (int i = 0; i < k; ++i) {
            tail = tail->next;
        }

        ListNode* new_head = tail->next;
        tail->next = nullptr;

        return new_head;
    }

private:
    std::pair<ListNode*, size_t> get_tail_and_len(ListNode* n)
    {
        if (nullptr == n) {
            return {nullptr, 0};
        }

        size_t len = 1;
        while (n->next) {
            ++len;
            n = n->next;
        }

        return {n, len};
    }
};
