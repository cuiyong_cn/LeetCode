/**
 * Magical String
 *
 * A magical string S consists of only '1' and '2' and obeys the following rules:
 *
 * The string S is magical because concatenating the number of contiguous occurrences of characters '1' and '2'
 * generates the string S itself.
 *
 * The first few elements of string S is the following: S = "1221121221221121122……"
 * If we group the consecutive '1's and '2's in S, it will be:
 *
 * 1 22 11 2 1 22 1 22 11 2 11 22 ......
 * and the occurrences of '1's or '2's in each group are:
 * 1 2 2 1 1 2 1 2 2 1 2 2 ......
 *
 * You can see that the occurrence sequence above is the S itself.
 * Given an integer N as input, return the number of '1's in the first N number in the magical string S.
 *
 * Note: N will not exceed 100,000.
 *
 * Example 1:
 * Input: 6
 * Output: 3
 * Explanation: The first 6 elements of magical string S is "12211" and it contains three 1's, so return 3.
 */
/**
 * Improved one.
 */
class Solution {
public:
    int magicalString(int n) {
        string magical{"122"};

        int slow = 2;
        int fast = 2;

        while (fast < n) {
            int cnt = magical[slow] - '0';
            magical += string(cnt, '1' == magical[fast] ? '2' : '1');
            fast += cnt;
            ++slow;
        }

        return count_if(magical.begin(), magical.begin() + n, [](auto c) {
            return '1' == c;
        });
    }
};

/**
 * Original solution. First we generate the magical string of length n. Then count the '1's
 */
class Solution {
public:
    int magicalString(int n) {
        string magical{"122"};

        int slow = 2;
        int fast = 2;

        while (fast < n) {
            int cnt = magical[slow] - '0';
            if (2 == cnt) {
                if ('1' == magical[fast]) {
                    magical += string(2, '2');
                }
                else {
                    magical += string(2, '1');
                }
                fast += 2;
            }
            else {
                if ('1' == magical[fast]) {
                    magical += '2';
                }
                else {
                    magical += '1';
                }
                fast += 1;
            }
            ++slow;
        }

        return count_if(magical.begin(), magical.begin() + n, [](auto c) {
            return '1' == c;
        });
    }
};
