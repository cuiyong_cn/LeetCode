/**
 * Minimum Number of Flips to Make the Binary String Alternating
 *
 * You are given a binary string s. You are allowed to perform two types of operations on the string
 * in any sequence:
 *
 * Type-1: Remove the character at the start of the string s and append it to the end of the string.
 * Type-2: Pick any character in s and flip its value, i.e., if its value is '0' it becomes '1' and vice-versa.
 * Return the minimum number of type-2 operations you need to perform such that s becomes alternating.
 *
 * The string is called alternating if no two adjacent characters are equal.
 *
 * For example, the strings "010" and "1010" are alternating, while the string "0100" is not.
 *
 * Example 1:
 *
 * Input: s = "111000"
 * Output: 2
 * Explanation: Use the first operation two times to make s = "100011".
 * Then, use the second operation on the third and sixth elements to make s = "101010".
 * Example 2:
 *
 * Input: s = "010"
 * Output: 0
 * Explanation: The string is already alternating.
 * Example 3:
 *
 * Input: s = "1110"
 * Output: 1
 * Explanation: Use the second operation on the second element to make s = "1010".
 *
 * Constraints:
 *
 * 1 <= s.length <= 105
 * s[i] is either '0' or '1'.
 */
/**
 * Original solution.
 */
void incr(char c, int& a, int& b)
{
    if ('0' == c) {
        ++a;
    }
    else {
        ++b;
    }
}

class Solution {
public:
    int minFlips(string s) {
        int odd_zero = 0;
        int odd_one = 0;
        int even_zero = 0;
        int even_one = 0;
        int const SL = s.length();

        for (int i = 0; i < SL; ++i) {
            if (i & 1) {
                incr(s[i], odd_zero, odd_one);
            }
            else {
                incr(s[i], even_zero, even_one);
            }
        }

        int ans = min(SL - (odd_zero + even_one), SL - (odd_one + even_zero));
        int const last_idx = SL - 1;
        // for every shift
        for (int i = 0; i < SL; ++i) {
            if ('0' == s[i]) {
                --even_zero;
                swap(odd_zero, even_zero);
                swap(odd_one, even_one);
                if (last_idx & 1) {
                    ++odd_zero;
                }
                else {
                    ++even_zero;
                }
            }
            else {
                --even_one;
                swap(odd_zero, even_zero);
                swap(odd_one, even_one);
                if (last_idx & 1) {
                    ++odd_one;
                }
                else {
                    ++even_one;
                }
            }

            ans = min(ans, min(SL - (odd_zero + even_one), SL - (odd_one + even_zero)));
        }

        return ans;
    }
};
