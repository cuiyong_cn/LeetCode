/**
 * Minimum Number of Groups to Create a Valid Assignment
 *
 * You are given a collection of numbered balls and instructed to sort them into boxes for a nearly
 * balanced distribution. There are two rules you must follow:
 *
 * Balls with the same box must have the same value. But, if you have more than one ball with
 * the same number, you can put them in different boxes.
 *
 * The biggest box can only have one more ball than the smallest box.
 *
 * Return the fewest number of boxes to sort these balls following these rules.
 *
 * Example 1:
 *
 * Input:  balls = [3,2,3,2,3]
 *
 * Output:  2
 *
 * Explanation:
 *
 * We can sort balls into boxes as follows:
 *     [3,3,3]
 *     [2,2]
 *
 * The size difference between the two boxes doesn't exceed one.
 *
 * Example 2:
 *
 * Input:  balls = [10,10,10,3,1,1]
 *
 * Output:  4
 *
 * Explanation:
 *
 * We can sort balls into boxes as follows:
 *     [10]
 *     [10,10]
 *     [3]
 *     [1,1]
 *
 * You can't use fewer than four boxes while still following the rules. For example, putting all three balls numbered 10 in one box would break the rule about the maximum size difference between boxes.
 *
 * Constraints:
 *     1 <= nums.length <= 105
 *     1 <= nums[i] <= 109
 */
/**
 * Original solution.
 */
class Solution {
public:
    int minGroupsForValidAssignment(vector<int>& balls) {
        auto freq_num = unordered_map<int, int>{};

        for (auto n : balls) {
            ++freq_num[n];
        }

        auto freq_min = numeric_limits<int>::max();

        for (auto&& [n, f] : freq_num) {
            freq_min = min(freq_min, f);
        }

        int ans = balls.size();

        for (auto f = 1; f <= freq_min; ++f) {
            auto groups = 0;

            for (auto&& [n, freq] : freq_num) {
                auto const a = freq / (f + 1);
                auto const b = freq % (f + 1);

                if (0 == b) {
                    groups += a;
                }
                else if ((f - b) <= a) {
                    groups += a + 1;
                }
                else {
                    groups = balls.size();
                    break;
                }
            }

            ans = min(ans, groups);
        }

        return ans;
    }
};
