/**
 * Minimize the Maximum of Two Arrays
 *
 * We have two arrays arr1 and arr2 which are initially empty. You need to add
 * positive integers to them such that they satisfy all the following
 * conditions:
 *
 * arr1 contains uniqueCnt1 distinct positive integers, each of which is not
 * divisible by divisor1.  arr2 contains uniqueCnt2 distinct positive integers,
 * each of which is not divisible by divisor2.  No integer is present in both
 * arr1 and arr2.  Given divisor1, divisor2, uniqueCnt1, and uniqueCnt2, return
 * the minimum possible maximum integer that can be present in either array.
 *
 * Example 1:
 *
 * Input: divisor1 = 2, divisor2 = 7, uniqueCnt1 = 1, uniqueCnt2 = 3
 * Output: 4
 * Explanation:
 * We can distribute the first 4 natural numbers into arr1 and arr2.
 * arr1 = [1] and arr2 = [2,3,4].
 * We can see that both arrays satisfy all the conditions.
 * Since the maximum value is 4, we return it.
 * Example 2:
 *
 * Input: divisor1 = 3, divisor2 = 5, uniqueCnt1 = 2, uniqueCnt2 = 1
 * Output: 3
 * Explanation:
 * Here arr1 = [1,2], and arr2 = [3] satisfy all conditions.
 * Since the maximum value is 3, we return it.
 * Example 3:
 *
 * Input: divisor1 = 2, divisor2 = 4, uniqueCnt1 = 8, uniqueCnt2 = 2
 * Output: 15
 * Explanation:
 * Here, the final possible arrays can be arr1 = [1,3,5,7,9,11,13,15], and arr2 = [2,6].
 * It can be shown that it is not possible to obtain a lower maximum satisfying all conditions.
 *
 * Constraints:
 *
 * 2 <= divisor1, divisor2 <= 105
 * 1 <= uniqueCnt1, uniqueCnt2 < 109
 * 2 <= uniqueCnt1 + uniqueCnt2 <= 109
 */
/**
 * O(1) solution.
 */
class Solution {
public:
    int getMax(int cnt, long long d1, long long d2 = 1) {
        long long d = lcm(d1, d2);
        return cnt + cnt / (d - 1) - (cnt % (d - 1) ? 0 : 1);
    }

    int minimizeSet(int d1, int d2, int cnt1, int cnt2) {
        return max({getMax(cnt1, d1), getMax(cnt2, d2), getMax(cnt1 + cnt2, d1, d2) });
    }
};

/**
 * Original solution. Binary solution.
 */
class Solution {
public:
    int minimizeSet(int divisor1, int divisor2, int uniqueCnt1, int uniqueCnt2) {
        auto left = 1;
        auto right = numeric_limits<int>::max();
        auto lcm_div = lcm(divisor1, divisor2);
        auto ans = 1;
        while (left <= right) {
            auto mid = left + (right - left) / 2;
            auto cnt_div_1 = mid / divisor1;
            auto cnt_div_2 = mid / divisor2;
            auto cnt_div_lcm = mid / lcm_div;
            if ((uniqueCnt1 <= (mid - cnt_div_1))
              && (uniqueCnt2 <= (mid - cnt_div_2)
              && ((uniqueCnt1 + uniqueCnt2) <= (mid - cnt_div_lcm))))
            {
                ans = mid;
                right = mid - 1;
            }
            else {
                left = mid + 1;
            }
        }

        return left;
    }
};
