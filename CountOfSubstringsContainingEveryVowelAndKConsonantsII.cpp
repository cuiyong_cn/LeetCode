/**
 * Count of Substrings Containing Every Vowel and K Consonants II
 *
 * You are given a string word and a non-negative integer k.
 *
 * Return the total number of substrings of word that contain every vowel ('a', 'e', 'i', 'o', and
 * 'u') at least once and exactly k consonants.
 *
 * Example 1:
 *
 * Input: word = "aeioqq", k = 1
 *
 * Output: 0
 *
 * Explanation:
 *
 * There is no substring with every vowel.
 *
 * Example 2:
 *
 * Input: word = "aeiou", k = 0
 *
 * Output: 1
 *
 * Explanation:
 *
 * The only substring with every vowel and zero consonants is word[0..4], which is "aeiou".
 *
 * Example 3:
 *
 * Input: word = "ieaouqqieaouqq", k = 1
 *
 * Output: 3
 *
 * Explanation:
 *
 * The substrings with every vowel and one consonant are:
 *     word[0..5], which is "ieaouq".
 *     word[6..11], which is "qieaou".
 *     word[7..12], which is "ieaouq".
 *
 * Constraints:
 *     5 <= word.length <= 2 * 105
 *     word consists only of lowercase English letters.
 *     0 <= k <= word.length - 5
 */
/**
 * Original solution.
 */
class Solution {
public:
    long long countOfSubstrings(string word, int k) {
        return atLeast(word, k) - atLeast(word, k + 1);
    }

private:
    long long atLeast(string const& s, int k)
    {
        auto vowels = string{"aeiou"};
        auto ans = 0LL;
        auto const size = s.length();
        auto is_vowel = [&vowels](char c) {
            auto p = vowels.find(c);
            return string::npos == p ? pair(false, 0uz) : pair(true, p);
        };
        auto vowel_cnt = array<int, 5>{ 0, };

        for (auto [i, j, v, c] = tuple(0uz, 0uz, 0uz, 0uz); i < size; ++i) {
            auto [is_v, p] = is_vowel(s[i]);
            if (is_v) {
                auto const cnt = ++vowel_cnt[p];
                v += 1 == cnt;
            }
            else {
                ++c;
            }

            for (; 5 == v && c >= k; ++j) {
                ans += s.length() - i;
                auto [is_v, p] = is_vowel(s[j]);
                if (is_v) {
                    auto const cnt = --vowel_cnt[p];
                    v -= 0 == cnt;
                }
                else {
                    --c;
                }
            }
        }

        return ans;
    }
};
