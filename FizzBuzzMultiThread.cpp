/**
 * Fizz Buzz Multithreaded
 *
 * Write a program that outputs the string representation of numbers from 1 to n, however:
 *
 *     If the number is divisible by 3, output "fizz".
 *     If the number is divisible by 5, output "buzz".
 *     If the number is divisible by both 3 and 5, output "fizzbuzz".
 *
 * For example, for n = 15, we output: 1, 2, fizz, 4, buzz, fizz, 7, 8, fizz, buzz, 11, fizz, 13, 14, fizzbuzz.
 *
 * Suppose you are given the following code:
 *
 * class FizzBuzz {
 *   public FizzBuzz(int n) { ... }               // constructor
 *   public void fizz(printFizz) { ... }          // only output "fizz"
 *   public void buzz(printBuzz) { ... }          // only output "buzz"
 *   public void fizzbuzz(printFizzBuzz) { ... }  // only output "fizzbuzz"
 *   public void number(printNumber) { ... }      // only output the numbers
 * }
 *
 * Implement a multithreaded version of FizzBuzz with four threads. The same instance of
 * FizzBuzz will be passed to four different threads:
 *
 *     Thread A will call fizz() to check for divisibility of 3 and outputs fizz.
 *     Thread B will call buzz() to check for divisibility of 5 and outputs buzz.
 *     Thread C will call fizzbuzz() to check for divisibility of 3 and 5 and outputs fizzbuzz.
 *     Thread D will call number() which should only output the numbers.
 *
 */
class FizzBuzz {
private:
    int                 n;
    mutex               mtx;
    condition_variable  cv;
    int                 item = 1;

public:

    FizzBuzz(int n) {
        this->n = n;
    }

    /*
     * While item has not reached n, we wait until a condition is met. If the condition
     * is met, we execute a function and wake all the others waiting for a condition to 
     * be met.
     */
    #define CONSUMER(cond, fn) while (item <= n) {                          \
                                    unique_lock<mutex> lck(mtx);            \
                                    if (!(cond)) {                          \
                                        cv.wait(lck);                       \
                                        continue;                           \
                                    }                                       \
                                    fn; item++; cv.notify_all();            \
                                }

    // printFizz() outputs "fizz".
    void fizz(function<void()> printFizz) {
        CONSUMER(!(item % 3) && (item % 5), printFizz());
    }

    // printBuzz() outputs "buzz".
    void buzz(function<void()> printBuzz) {
        CONSUMER((item % 3) && !(item % 5), printBuzz())
    }

    // printFizzBuzz() outputs "fizzbuzz".
	void fizzbuzz(function<void()> printFizzBuzz) {
        CONSUMER(!(item % 3) && !(item % 5), printFizzBuzz())
    }

    // printNumber(x) outputs "x", where x is an integer.
    void number(function<void(int)> printNumber) {
        CONSUMER((item % 3) && (item % 5), printNumber(item));
    }
};
