/**
 * Product of Array Except Self
 *
 * Given an array nums of n integers where n > 1,  return an array output such that
 * output[i] is equal to the product of all the elements of nums except nums[i].
 *
 * Example:
 *
 * Input:  [1,2,3,4]
 * Output: [24,12,8,6]
 *
 * Note: Please solve it without division and in O(n).
 *
 * Follow up:
 * Could you solve it with constant space complexity? (The output array does not count
 * as extra space for the purpose of space complexity analysis.)
 */
/**
 * Improved version of L and R solution.
 */
class Solution {
public:
    vector<int> productExceptSelf(vector<int>& nums) {
        vector<int> ans(nums.size(), 1);

        for (int i = 1; i < nums.size(); ++i) {
            ans[i] = ans[i - 1] * nums[i - 1];
        }

        int R = 1;
        for (int i = nums.size() - 1; i >= 0; --i) {
            ans[i] = ans[i] * R;
            R *= nums[i];
        }
        return ans;
    }
};

/**
 * Respect the note solution. The idea is we use L to store product of all the numbers to
 * the left of index i, and use R to store product of all the numbers to the right of
 * index i. And the multiply the corresponding L and R would give us the desired result.
 */
class Solution {
public:
    vector<int> productExceptSelf(vector<int>& nums) {
        vector<int> L(nums.size(), 1);
        vector<int> R(nums.size(), 1);
        for (int i = 1; i < nums.size(); ++i) {
            L[i] = L[i - 1] * nums[i - 1];
        }
        for (int i = nums.size() - 2; i >= 0; --i) {
            R[i] = R[i + 1] * nums[i + 1];
        }

        vector<int> ans(nums.size(), 0);
        for (int i = 0; i < nums.size(); ++i) {
            ans[i] = L[i] * R[i];
        }
        return ans;
    }
};
/**
 * My original solution. Clearly violate the requirement. But at least this is a correct
 * solution.
 */
class Solution {
public:
    vector<int> productExceptSelf(vector<int>& nums) {
        long sum = 1;
        int zeroCount = 0;
        int zeroIndex = -1;
        for (int i = 0; i < nums.size(); ++i) {
            if (0 == nums[i]) {
                ++zeroCount;
                zeroIndex = i;
            }
            else {
                sum *= nums[i];
            }
        }

        vector<int> ans(nums.size(), 0);
        if (0 == zeroCount) {
            for (int i = 0; i < nums.size(); ++i) {
                ans[i] = sum / nums[i];
            }
        }
        else if (1 == zeroCount) {
            ans[zeroIndex] = sum;
        }
        return ans;
    }
};
