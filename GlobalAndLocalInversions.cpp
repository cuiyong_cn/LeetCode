/**
 * Global and Local Inversions
 *
 * We have some permutation A of [0, 1, ..., N - 1], where N is the length of A.
 *
 * The number of (global) inversions is the number of i < j with 0 <= i < j < N and A[i] > A[j].
 *
 * The number of local inversions is the number of i with 0 <= i < N and A[i] > A[i+1].
 *
 * Return true if and only if the number of global inversions is equal to the number of local
 * inversions.
 *
 * Example 1:
 *
 * Input: A = [1,0,2]
 * Output: true
 * Explanation: There is 1 global inversion, and 1 local inversion.
 * Example 2:
 *
 * Input: A = [1,2,0]
 * Output: false
 * Explanation: There are 2 global inversions, and 1 local inversion.
 * Note:
 *
 * A will be a permutation of [0, 1, ..., A.length - 1].
 * A will have length in range [1, 5000].
 * The time limit for this problem has been reduced
 */
/**
 * Another solution. Idea is:
 *
 * All global inversions in permutations are local inversions.
 * It also means that we can not find A[i] > A[j] with i+2<=j.
 * In other words, max(A[i]) < A[i + 2]
 */
class Solution {
public:
    bool isIdealPermutation(vector<int>& A) {
        int cmax = 0, n = A.size();
        for (int i = 0; i < n - 2; ++i) {
            cmax = max(cmax, A[i]);
            if (cmax > A[i + 2]) return false;
        }
        return true;
    }
};

/**
 * Improve the below version.
 */
class Solution {
public:
    bool isIdealPermutation(vector<int>& A) {
        const int N = A.size();
        for (int i = 0; i < N; ++i) {
            if (abs(A[i] - i) > 1) {
                return false;
            }
        }

        return true;
    }
};

/**
 * Original solution. Idea is:
 *
 * If A[i] > i + 1, means at least one number that is smaller than A[i] is kicked out from first
 * A[i] numbers, and the distance between this smaller number and A[i] is at least 2, then it is a
 * non-local global inversion.
 * For example, A[i] = 3, i = 1, at least one number that is smaller than 3 is kicked out from first
 * 3 numbers, and the distance between the smaller number and 3 is at least 2.
 *
 * If A[i] < i - 1, means at least one number that is bigger than A[i] is kicked out from last n - i
 * numbers, and the distance between this bigger number and A[i] is at least 2, then it is a
 * non-local global inversion.
 */
class Solution {
public:
    bool isIdealPermutation(vector<int>& A) {
        const int N = A.size();
        vector<int> pos(N, 0);
        for (int i = 0; i < N; ++i) {
            pos[A[i]] = i;
        }

        for (int i = 0; i < N; ++i) {
            if (pos[i] > (i + 1)) {
                return false;
            }
        }

        for (int i = N - 1; i >= 0; --i) {
            if (pos[i] < (i - 1)) {
                return false;
            }
        }

        return true;
    }
};
