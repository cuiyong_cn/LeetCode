/**
 * Implement Rand10() Using Rand7()
 *
 * Given the API rand7() that generates a uniform random integer in the range [1, 7], write a function rand10() that
 * generates a uniform random integer in the range [1, 10]. You can only call the API rand7(), and you shouldn't call
 * any other API. Please do not use a language's built-in random API.
 *
 * Each test case will have one internal argument n, the number of times that your implemented function rand10() will be
 * called while testing. Note that this is not an argument passed to rand10().
 *
 * Follow up:
 *
 * What is the expected value for the number of calls to rand7() function?
 * Could you minimize the number of calls to rand7()?
 *
 * Example 1:
 *
 * Input: n = 1
 * Output: [2]
 * Example 2:
 *
 * Input: n = 2
 * Output: [2,8]
 * Example 3:
 *
 * Input: n = 3
 * Output: [3,8,10]
 *
 * Constraints:
 *
 * 1 <= n <= 105
 */
/**
 * Bit of improve the efficency. The idea is, when we hit a out of range
 * number, we reuse it instead of throwing it out. For example.
 * First we get 41 - 49. Though they are out of range, but we get 1 - 9
 * uniformly. Then we can get 1 - 63 uniformly. Then repeat this process
 * again. If we get 61 - 63. Then we get 1 - 3 uniformly, combine with
 * rand7(), we can get 1 - 21 uniformly. if we get 21. then we should start
 * all over, because if not, we will get down to 1 - 7???
 */
class Solution {
public:
    int rand10() {
        int idx = 49;
        while (true) {
            idx = (rand7() - 1) * 7 + rand7();
            if (idx <= 40) {
                break;
            }

            idx = (idx - 41) * 7 + rand7();
            if (idx <= 60) {
                break;
            }

            idx = (idx - 61) * 7 + rand7();
            if (idx <= 20) {
                break;
            }
        }

        return (idx - 1) % 10 + 1;
    }
};

/**
 * Remove the array.
 */
class Solution {
public:
    int rand10() {
        int idx = 49;
        while (idx > 40) {
            idx = (rand7() - 1) * 7 + rand7();
        }

        return (idx - 1) % 10 + 1;
    }
};

/**
 * I did not come up with a solution. Bad at probability theory.
 *
 * Rejection sampling.
 * Using ran7, we get uniform number range in 1 - 49.
 */
class Solution {
public:
    int rand10() {
        int rd[49] = {
            0, 1, 2, 3, 4, 5, 6, 7,
            8, 9, 10, 1, 2, 3, 4,
            5, 6, 7, 8, 9, 10, 1,
            2, 3, 4, 5, 6, 7, 8,
            9, 10, 1, 2, 3, 4, 5,
            6, 7, 8, 9, 10, 0, 0,
        };

        while (true) {
            auto idx = (rand7() - 1) * 7 + rand7();
            if (idx < 41) {
                return rd[idx];
            }
        }

        return 0;
    }
};
