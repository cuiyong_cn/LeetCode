/**
 * First Unique Character in a String
 *
 *  Given a string, find the first non-repeating character in it and return it's index. If it doesn't exist, return -1.
 *
 * Examples:
 *
 * s = "leetcode"
 * return 0.
 *
 * s = "loveleetcode",
 * return 2.
 *
 * Note: You may assume the string contain only lowercase letters.
 */
/**
 * Improved version. Eliminate the map. We only use 1 or 2 to avoid the unnecessary write.
 */
class Solution {
public:
    int firstUniqChar(string s) {
        vector<pair<int, int>> chars(26, {0, 0});
        int index = 0;
        for (auto c : s) {
            auto& p = chars[c - 'a'];
            if (0 == p.second) {
                p.first = index;
                p.second = 1;
            }
            else if (1 == p.second) {
                p.second = 2;
            }
            ++index;
        }

        int uniqueIndex = numeric_limits<int>::max();

        for (auto& p : chars) {
            if (1 == p.second) {
                uniqueIndex = min(uniqueIndex, p.first);
            }
        }

        return uniqueIndex == numeric_limits<int>::max() ? -1 : uniqueIndex;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int firstUniqChar(string s) {
        unordered_map<char, int> charM;
        vector<int> chars(26, 0);

        int index = 0;
        for (auto c : s) {
            if (0 == chars[c - 'a']) {
                charM[c] = index;
                ++chars[c - 'a'];
            }
            else {
                charM.erase(c);
            }

            ++index;
        }

        int minIndex = numeric_limits<int>::max();
        for (auto& p : charM) {
            minIndex = min(minIndex, p.second);
        }

        return charM.empty() ? -1 : minIndex;
    }
};
