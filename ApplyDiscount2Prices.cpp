/**
 * Apply Discount to Prices
 *
 * A sentence is a string of single-space separated words where each word can contain digits,
 * lowercase letters, and the dollar sign '$'. A word represents a price if it is a sequence of
 * digits preceded by a dollar sign.
 *
 * For example, "$100", "$23", and "$6" represent prices while "100", "$", and "$1e5" do not.
 * You are given a string sentence representing a sentence and an integer discount. For each word
 * representing a price, apply a discount of discount% on the price and update the word in the
 * sentence. All updated prices should be represented with exactly two decimal places.
 *
 * Return a string representing the modified sentence.
 *
 * Note that all prices will contain at most 10 digits.
 *
 * Example 1:
 *
 * Input: sentence = "there are $1 $2 and 5$ candies in the shop", discount = 50
 * Output: "there are $0.50 $1.00 and 5$ candies in the shop"
 * Explanation:
 * The words which represent prices are "$1" and "$2".
 * - A 50% discount on "$1" yields "$0.50", so "$1" is replaced by "$0.50".
 * - A 50% discount on "$2" yields "$1". Since we need to have exactly 2 decimal places after a price, we replace "$2" with "$1.00".
 * Example 2:
 *
 * Input: sentence = "1 2 $3 4 $5 $6 7 8$ $9 $10$", discount = 100
 * Output: "1 2 $0.00 4 $0.00 $0.00 7 8$ $0.00 $10$"
 * Explanation:
 * Applying a 100% discount on any price will result in 0.
 * The words representing prices are "$3", "$5", "$6", and "$9".
 * Each of them is replaced by "$0.00".
 *
 * Constraints:
 *
 * 1 <= sentence.length <= 105
 * sentence consists of lowercase English letters, digits, ' ', and '$'.
 * sentence does not have leading or trailing spaces.
 * All words in sentence are separated by a single space.
 * All prices will be positive numbers without leading zeros.
 * All prices will have at most 10 digits.
 * 0 <= discount <= 100
 */
/**
 * More clear intent.
 */
string process(string const& w, int discount)
{
    if ('$' != w.front()) {
        return w;
    }

    if (1 == w.length()) {
        return w;
    }

    if (!all_of(begin(w) + 1, end(w), ::isdigit)) {
        return w;
    }

    auto dollars = stoll(w.substr(1)) * (1 - discount / 100.0);
    auto oss = ostringstream{};
    oss << '$' << fixed << setprecision(2) << dollars;
    return oss.str();
}

class Solution {
public:
    string discountPrices(string sentence, int discount) {
        auto iss = istringstream(sentence);
        auto words = vector<string>{istream_iterator<string>(iss), istream_iterator<string>()};
        auto out = vector<string>(words.size(), string{});
        transform(begin(words), end(words), begin(out), bind(process, std::placeholders::_1, discount));
        auto oss = ostringstream{};
        copy(begin(out), end(out), ostream_iterator<string>(oss, " "));
        auto ans = oss.str();
        ans.pop_back();
        return ans;
    }
};

/**
 * A little shorter.
 */
class Solution {
public:
    string discountPrices(string sentence, int discount) {
        auto iss = istringstream(sentence);
        auto words = vector<string>{istream_iterator<string>(iss), istream_iterator<string>()};
        auto ans = string{};
        for (auto const& w : words) {
            if ('$' == w.front()) {
                auto valid = w.length() > 1 && all_of(begin(w) + 1, end(w), ::isdigit);
                if (valid) {
                    auto dollars = stoll(w.substr(1)) * (1 - discount / 100.0);
                    auto os = ostringstream{};
                    os << fixed << setprecision(2) << dollars;
                    ans += '$';
                    ans += os.str();
                }
                else {
                    ans += w;
                }
            }
            else {
                ans += w;
            }
            ans += ' ';
        }
        ans.pop_back();
        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    string discountPrices(string sentence, int discount) {
        auto i = 0;
        auto slen = sentence.length();
        auto ans = string{};

        while (i < slen) {
            if ('$' == sentence[i]) {
                ans += '$';
                auto space = sentence.find(' ', i + 1);
                if (string::npos == space) {
                    space = slen;
                }
                auto dollar_str = sentence.substr(i + 1, space - i - 1);
                auto valid = !dollar_str.empty() && all_of(begin(dollar_str), end(dollar_str), ::isdigit);
                if (valid) {
                    auto dollars = stoll(dollar_str);
                    auto after_discount = dollars * (1 - discount / 100.0);
                    auto oss = ostringstream{};
                    oss << fixed << setprecision(2) << after_discount;
                    ans += oss.str();
                }
                else {
                    ans += dollar_str;
                }

                i = space;
            }
            else {
                while (i < slen) {
                    if (' ' == sentence[i]) {
                        break;
                    }
                    ans += sentence[i];
                    ++i;
                }
            }
            ans += ' ';
            ++i;
        }

        ans.pop_back();

        return ans;
    }
};
