/**
 * Words Within Two Edits of Dictionary
 *
 * You are given two string arrays, queries and dictionary. All words in each array comprise of
 * lowercase English letters and have the same length.
 *
 * In one edit you can take a word from queries, and change any letter in it to any other letter.
 * Find all words from queries that, after a maximum of two edits, equal some word from dictionary.
 *
 * Return a list of all words from queries, that match with some word from dictionary after a
 * maximum of two edits. Return the words in the same order they appear in queries.
 *
 * Example 1:
 *
 * Input: queries = ["word","note","ants","wood"], dictionary = ["wood","joke","moat"]
 * Output: ["word","note","wood"]
 * Explanation:
 * - Changing the 'r' in "word" to 'o' allows it to equal the dictionary word "wood".
 * - Changing the 'n' to 'j' and the 't' to 'k' in "note" changes it to "joke".
 * - It would take more than 2 edits for "ants" to equal a dictionary word.
 * - "wood" can remain unchanged (0 edits) and match the corresponding dictionary word.
 * Thus, we return ["word","note","wood"].
 * Example 2:
 *
 * Input: queries = ["yes"], dictionary = ["not"]
 * Output: []
 * Explanation:
 * Applying any two edits to "yes" cannot make it equal to "not". Thus, we return an empty array.
 *
 * Constraints:
 *
 * 1 <= queries.length, dictionary.length <= 100
 * n == queries[i].length == dictionary[j].length
 * 1 <= n <= 100
 * All queries[i] and dictionary[j] are composed of lowercase English letters.
 */
/**
 * Original solution.
 */
int diff_cnt(string const& w1, string const& w2)
{
    auto ans = 0;
    for (auto n = w1.size(), i = 0 * n; i < n; ++i) {
        if (w1[i] != w2[i]) {
            ++ans;
        }
    }
    return ans;
}

bool diff_within(vector<string> const& dictionary, string const& q, int max_edits)
{
    for (auto const& w : dictionary) {
        auto diffs = diff_cnt(w, q);
        if (diffs <= max_edits) {
            return true;
        }
    }
    return false;
}

class Solution {
public:
    vector<string> twoEditWords(vector<string>& queries, vector<string>& dictionary) {
        auto ans = vector<string>{};
        for (auto const& q : queries) {
            if (diff_within(dictionary, q, 2)) {
                ans.push_back(q);
            }
        }
        return ans;
    }
};
