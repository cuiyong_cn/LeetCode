/**
 * DI String Match
 *
 * Given a string S that only contains "I" (increase) or "D" (decrease), let N = S.length.
 *
 * Return any permutation A of [0, 1, ..., N] such that for all i = 0, ..., N-1:
 *
 *     If S[i] == "I", then A[i] < A[i+1]
 *     If S[i] == "D", then A[i] > A[i+1]
 *
 * Example 1:
 *
 * Input: "IDID"
 * Output: [0,4,1,3,2]
 *
 * Example 2:
 *
 * Input: "III"
 * Output: [0,1,2,3]
 *
 * Example 3:
 *
 * Input: "DDI"
 * Output: [3,2,0,1]
 *
 * Note:
 *
 *  1. 1 <= S.length <= 10000
 *  2. S only contains characters "I" or "D".
 */
/**
 * A elegent way.
 *
 * vector<int> diStringMatch(string S) {
 *      int left = count(S.begin(), S.end(), 'D'), right = left;
 *      vector<int> res = {left};
 *      for (char &c : S)
 *          res.push_back(c == 'I' ? ++right : --left);
 *      return res;
 *  }
 */
/**
 * Another way to think about
 *
 * class Solution {
 * public:
 *     vector<int> diStringMatch(string S) {
 *         int size = S.size();
 *         vector<int> A(size + 1, 0);
 *         int l = 0;
 *         int h = size;
 *         for (int i = 0; i < size; ++i) {
 *             if (S[i] == 'I') {
 *                 A[i] = l;
 *                 ++l;
 *             }
 *             else {
 *                 A[i] = h;
 *                 --h;
 *             }
 *         }
 *
 *         A[size] = l;
 *
 *         return A;
 *     }
 * };
 */
/**
 * Brute force
 */
class Solution {
public:
    vector<int> diStringMatch(string S) {
        int size = S.size();
        vector<int> A(size + 1, 0);
        for (int i = 1; i <= size; ++i) {
            A[i] = i;
        }
        bool gotIt = false;
        while (!gotIt) {
            gotIt = true;
            for (int i = 0; i < S.size(); ++i) {
                if (S[i] == 'I') {
                    if (A[i] > A[i + 1]) {
                        swap(A[i], A[i + 1]);
                        gotIt = false;
                    }
                }
                else {
                    if (A[i] < A[i + 1]) {
                        swap(A[i], A[i + 1]);
                        gotIt = false;
                    }
                }
            }
        }

        return A;
    }
};
