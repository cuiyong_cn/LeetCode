/**
 * Construct Smallest Number From DI String
 *
 * You are given a 0-indexed string pattern of length n consisting of the characters 'I' meaning
 * increasing and 'D' meaning decreasing.
 *
 * A 0-indexed string num of length n + 1 is created using the following conditions:
 *
 * num consists of the digits '1' to '9', where each digit is used at most once.
 * If pattern[i] == 'I', then num[i] < num[i + 1].
 * If pattern[i] == 'D', then num[i] > num[i + 1].
 * Return the lexicographically smallest possible string num that meets the conditions.
 *
 * Example 1:
 *
 * Input: pattern = "IIIDIDDD"
 * Output: "123549876"
 * Explanation:
 * At indices 0, 1, 2, and 4 we must have that num[i] < num[i+1].
 * At indices 3, 5, 6, and 7 we must have that num[i] > num[i+1].
 * Some possible values of num are "245639871", "135749862", and "123849765".
 * It can be proven that "123549876" is the smallest possible num that meets the conditions.
 * Note that "123414321" is not possible because the digit '1' is used more than once.
 * Example 2:
 *
 * Input: pattern = "DDD"
 * Output: "4321"
 * Explanation:
 * Some possible values of num are "9876", "7321", and "8742".
 * It can be proven that "4321" is the smallest possible num that meets the conditions.
 *
 * Constraints:
 *
 * 1 <= pattern.length <= 8
 * pattern consists of only the letters 'I' and 'D'.
 */
/**
 * One pass solution.
 */
class Solution {
public:
    string smallestNumber(string s) {
        string res;
        int n = s.size(), j = 0;
        for (int i = 0; i <= s.size(); ++i) {
            res.push_back('1' + i);
            if (i == n || s[i] == 'I') {
                reverse(res.begin() + j, res.end());
                j = i + 1;
            }
        }
        return res;
    }
};

/**
 * Original solution. Try every possible permutation.
 */
bool conform_with(string const& num, string const& pattern)
{
    for (auto i = 0; i < pattern.length(); ++i) {
        if ('I' == pattern[i]) {
            if (num[i] > num[i + 1]) {
                return false;
            }
        }
        else {
            if (num[i] < num[i + 1]) {
                return false;
            }
        }
    }
    return true;
}

class Solution {
public:
    string smallestNumber(string pattern) {
        auto ans = string(pattern.length() + 1, '1');
        for (auto i = 0; i <= pattern.length(); ++i) {
            ans[i] = '1' + i;
        }

        while (next_permutation(begin(ans), end(ans))) {
            if (conform_with(ans, pattern)) {
                break;
            }
        }

        return ans;
    }
};
