/**
 * Minimum Number of Swaps to Make the Binary String Alternating
 *
 * Given a binary string s, return the minimum number of character swaps to make it alternating, or
 * -1 if it is impossible.
 *
 * The string is called alternating if no two adjacent characters are equal. For example, the
 * strings "010" and "1010" are alternating, while the string "0100" is not.
 *
 * Any two characters may be swapped, even if they are not adjacent.
 *
 * Example 1:
 *
 * Input: s = "111000"
 * Output: 1
 * Explanation: Swap positions 1 and 4: "111000" -> "101010"
 * The string is now alternating.
 * Example 2:
 *
 * Input: s = "010"
 * Output: 0
 * Explanation: The string is already alternating, no swaps are needed.
 * Example 3:
 *
 * Input: s = "1110"
 * Output: -1
 *
 * Constraints:
 *
 * 1 <= s.length <= 1000
 * s[i] is either '0' or '1'.
 */
/**
 * Improve.
 */
class Solution {
public:
    int minSwaps(string const& s) {
        int cnt0 = count(begin(s), end(s), '0');
        int cnt1 = s.size() - cnt0;
        int miss0 = 0;
        int miss1 = 0;

        if (abs(cnt0 - cnt1) > 1) {
            return -1;
        }

        for (int i = 0; i < s.size(); i += 2) {
            miss0 += s[i] != '0';
            miss1 += s[i] != '1';
        }
        return cnt0 == cnt1 ? min(miss0, miss1) : cnt0 > cnt1 ? miss0 : miss1;
    }
};

/**
 * Original solution.
 */
pair<int, int> displace_count(string const& s, char first)
{
    int displace[2] = { 0, };
    for (auto const& c : s) {
        if (c != first) {
            ++displace[c - '0'];
        }
        first = '0' == first ? '1' : '0';
    }

    return {displace[0], displace[1]};
}

class Solution {
public:
    int minSwaps(string s) {
        // try '010101...' sequence
        auto oz = displace_count(s, '0');
        // try '101010...' sequence
        auto zo = displace_count(s, '1');

        int ans = 1001;
        if (oz.first == oz.second) {
            ans = min(ans, oz.first);
        }
        if (zo.first == zo.second) {
            ans = min(ans, zo.first);
        }

        return 1001 == ans ? -1 : ans;
    }
};
