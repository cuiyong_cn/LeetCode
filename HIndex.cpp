/**
 * H-Index
 *
 * Given an array of integers citations where citations[i] is the number of citations a researcher
 * received for their ith paper, return compute the researcher's h-index.
 *
 * According to the definition of h-index on Wikipedia: A scientist has an index h if h of their n
 * papers have at least h citations each, and the other n − h papers have no more than h citations
 * each.
 *
 * If there are several possible values for h, the maximum one is taken as the h-index.
 *
 * Example 1:
 *
 * Input: citations = [3,0,6,1,5]
 * Output: 3
 * Explanation: [3,0,6,1,5] means the researcher has 5 papers in total and each of them had received
 * 3, 0, 6, 1, 5 citations respectively.  Since the researcher has 3 papers with at least 3
 * citations each and the remaining two with no more than 3 citations each, their h-index is 3.
 * Example 2:
 *
 * Input: citations = [1,3,1]
 * Output: 1
 *
 * Constraints:
 *
 * n == citations.length
 * 1 <= n <= 5000
 * 0 <= citations[i] <= 1000
 */
/**
 * O(n) solution using extra space.
 */
class Solution {
public:
    int hIndex(vector<int>& citations) {
        int const N = citations.size();

        vector<int> cnt(N + 1, 0);
        for (auto c : citations) {
            ++cnt[min(c, N)];
        }

        int h = 0;
        for (int i = N; i >= 0; --i) {
            h += cnt[i];
            if (h >= i) {
                return i;
            }
        }

        return h;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int hIndex(vector<int>& citations) {
        sort(begin(citations), end(citations));

        int ans = 1;
        int N = citations.size();

        for (int i = 0; i < N; ++i) {
            if ((N - i) >= citations[i]) {
                ans = citations[i];
            }
            else {
                int min_idx = min(N, i > 0 ? citations[i - 1] : citations[i]);
                for (int j = citations[i] - 1; j >= min_idx; --j) {
                    if ((N - i) >= j) {
                        return j;
                    }
                }
            }
        }

        return ans;
    }
};
