/**
 * Sort Array by Increasing Frequency
 *
 * Given an array of integers nums, sort the array in increasing order based on the frequency of the
 * values. If multiple values have the same frequency, sort them in decreasing order.
 *
 * Return the sorted array.
 *
 * Example 1:
 *
 * Input: nums = [1,1,2,2,2,3]
 * Output: [3,1,1,2,2,2]
 * Explanation: '3' has a frequency of 1, '1' has a frequency of 2, and '2' has a frequency of 3.
 * Example 2:
 *
 * Input: nums = [2,3,1,3,2]
 * Output: [1,3,3,2,2]
 * Explanation: '2' and '3' both have a frequency of 2, so they are sorted in decreasing order.
 * Example 3:
 *
 * Input: nums = [-1,1,-6,4,5,-6,1,4,1]
 * Output: [5,-1,4,4,-6,-6,1,1,1]
 *
 * Constraints:
 *
 * 1 <= nums.length <= 100
 * -100 <= nums[i] <= 100
 */
/**
 * Shorter version.
 */
class Solution {
public:
    vector<int> frequencySort(vector<int>& nums) {
        unordered_map<int, int> num_cnt;
        for (auto n : nums) {
            ++num_cnt[n];
        }

        sort(nums.begin(), nums.end(),
            [&num_cnt](auto const& a, auto const& b) {
                return num_cnt[a] == num_cnt[b] ? a > b : num_cnt[a] < num_cnt[b];
            });

        return nums;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<int> frequencySort(vector<int>& nums) {
        unordered_map<int, int> num_cnt;
        for (auto n : nums) {
            ++num_cnt[n];
        }

        vector<pair<int, int>> freq_num(num_cnt.begin(), num_cnt.end());
        sort(freq_num.begin(), freq_num.end(),
            [](auto const& a, auto const& b) {
                return a.second < b.second ? true :
                       a.second > b.second ? false : a.first > b.first;
            });

        int const N = nums.size();
        vector<int> ans(N, 0);
        int i = 0;
        for (auto const& fn : freq_num) {
            int const cnt = fn.second;
            for (int j = 0; j < cnt; ++j) {
                ans[i] = fn.first;
                ++i;
            }
        }
        return ans;
    }
};
