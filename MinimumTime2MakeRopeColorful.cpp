/**
 * Minimum Time to Make Rope Colorful
 *
 * Alice has n balloons arranged on a rope. You are given a 0-indexed string colors where colors[i]
 * is the color of the ith balloon.
 *
 * Alice wants the rope to be colorful. She does not want two consecutive balloons to be of the same
 * color, so she asks Bob for help. Bob can remove some balloons from the rope to make it colorful.
 * You are given a 0-indexed integer array neededTime where neededTime[i] is the time (in seconds)
 * that Bob needs to remove the ith balloon from the rope.
 *
 * Return the minimum time Bob needs to make the rope colorful.
 *
 * Example 1:
 *
 * Input: colors = "abaac", neededTime = [1,2,3,4,5]
 * Output: 3
 * Explanation: In the above image, 'a' is blue, 'b' is red, and 'c' is green.
 * Bob can remove the blue balloon at index 2. This takes 3 seconds.
 * There are no longer two consecutive balloons of the same color. Total time = 3.
 * Example 2:
 *
 * Input: colors = "abc", neededTime = [1,2,3]
 * Output: 0
 * Explanation: The rope is already colorful. Bob does not need to remove any balloons from the rope.
 * Example 3:
 *
 * Input: colors = "aabaa", neededTime = [1,2,3,4,1]
 * Output: 2
 * Explanation: Bob will remove the ballons at indices 0 and 4. Each ballon takes 1 second to remove.
 * There are no longer two consecutive balloons of the same color. Total time = 1 + 1 = 2.
 *
 * Constraints:
 *
 * n == colors.length == neededTime.length
 * 1 <= n <= 105
 * 1 <= neededTime[i] <= 104
 * colors contains only lowercase English letters.
 */
/**
 * Shorter one.
 */
class Solution {
public:
    int minCost(string s, vector<int>& cost) {
        int res = 0, max_cost = cost.front(), n = s.size();
        for (int i = 1; i < n; ++i) {
            if (s[i] != s[i - 1]) {
                max_cost = 0;
            }

            res += min(max_cost, cost[i]);
            max_cost = max(max_cost, cost[i]);
        }
        return res;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int minCost(string colors, vector<int>& neededTime) {
        int ans = 0;
        int const N = colors.length();

        for (int i = 0; i < N;) {
            int max_time = neededTime[i];
            auto const color = colors[i];
            int j = i + 1;
            for (; j < N && color == colors[j]; ++j) {
                if (neededTime[j] >= max_time) {
                    ans += max_time;
                    max_time = neededTime[j];
                }
                else {
                    ans += neededTime[j];
                }
            }
            i = j;
        }

        return ans;
    }
};
