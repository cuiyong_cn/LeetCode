/**
 * Rotate String
 *
 * We are given two strings, A and B.
 *
 * A shift on A consists of taking string A and moving the leftmost character to the rightmost position. For example, if
 * A = 'abcde', then it will be 'bcdea' after one shift on A. Return True if and only if A can become B after some
 * number of shifts on A.
 *
 * Example 1:
 * Input: A = 'abcde', B = 'cdeab'
 * Output: true
 *
 * Example 2:
 * Input: A = 'abcde', B = 'abced'
 * Output: false
 *
 * Note:
 *
 *     A and B will have length at most 100.
 */
/**
 * More simple one. The search algorithm can use KMP. But its too overkill.
 */
class Solution {
public:
    bool rotateString(string A, string B) {
        if (B.size() != A.size()) return false;

        string two_a = A + A;

        return two_a.find(B) != string::npos ? true : false;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool rotateString(string A, string B) {
        if (B.size() != A.size()) return false;
        if (B.empty()) return true;

        auto fc = B[0];
        auto ans = false;
        auto len = A.length();
        auto idx = A.find(fc, 0);
        while (idx != string::npos) {
            size_t i = 1;
            for (; i < len; ++i) {
                if (A[(i + idx) % len] != B[i]) break;
            }

            if (i == len) {
                ans = true;
                break;
            }
            idx = A.find(fc, idx + 1);
        }

        return ans;
    }
};
