/**
 * Path In Zigzag Labelled Binary Tree
 *
 * In an infinite binary tree where every node has two children,
 * the nodes are labelled in row order.
 *
 * In the odd numbered rows (ie., the first, third, fifth,...),
 * the labelling is left to right, while in the even numbered rows
 * (second, fourth, sixth,...), the labelling is right to left.
 *
 * Given the label of a node in this tree, return the labels in the path from
 * the root of the tree to the node with that label.
 *
 *Example 1:
 *
 *Input: label = 14
 *Output: [1,3,4,14]
 *
 *Example 2:
 *
 *Input: label = 26
 *Output: [1,2,6,10,26]
 *
 *Constraints:
 *
 *    1 <= label <= 10^6
 *
 */
/**
 * There is same version but think in different view
 *
 * Intuition
 *
 * If the tree is numbered left-to-right (not zigzag), the parent's label can be
 * always determined as label / 2. For zigzag, we need to "invert" the parent label.
 *
 * Solution
 *
 * Determine the tree level where our value is located. The maximum label in the
 * level is 1 << level - 1, and minimum is 1 << (level - 1).
 * We will use this fact to "invert" the parent label.
 *
 * vector<int> pathInZigZagTree(int label, int level = 0)
 * {
 *   while (1 << level <= label) ++level;
 *   vector<int> res(level);
 *   for(; label >= 1; label /= 2, --level) {
 *     res[level - 1] = label;
 *     label = (1 << level) - 1 - label + (1 << (level - 1));
 *   }
 *   return res;
 * }
 *
 * Complexity Analysis
 *
 * Runtime: O(log n)
 * Memory: O(1) or O(log n) if we consider the memory required for the result.
 */
class Solution {
public:
    vector<int> pathInZigZagTree(int label) {
        int level = log2(label) + 1;
        int start = pow(2, level - 1);
        int end = (start << 1) - 1;
        vector<int> ans(level);
        int n = label;
        for (; n >= 1; ) {
            ans.push_back(n);
            n = (start >> 1) + ((end - n) >> 1);
            start >>= 1;
            end = (start << 1) - 1;
        }
        reverse(ans.begin(), ans.end());
        
        return ans;
    }
};
