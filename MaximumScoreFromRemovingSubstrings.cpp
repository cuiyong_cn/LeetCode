/**
 * Maximum Score From Removing Substrings
 *
 * You are given a string s and two integers x and y. You can perform two types of operations any
 * number of times.
 *
 * Remove substring "ab" and gain x points.  For example, when removing "ab" from "cabxbae" it
 * becomes "cxbae".  Remove substring "ba" and gain y points.  For example, when removing "ba" from
 * "cabxbae" it becomes "cabxe".  Return the maximum points you can gain after applying the above
 * operations on s.
 *
 * Example 1:
 *
 * Input: s = "cdbcbbaaabab", x = 4, y = 5
 * Output: 19
 * Explanation:
 * - Remove the "ba" underlined in "cdbcbbaaabab". Now, s = "cdbcbbaaab" and 5 points are added to
 *   the score.
 * - Remove the "ab" underlined in "cdbcbbaaab". Now, s = "cdbcbbaa" and 4 points are added to the
 *   score.
 * - Remove the "ba" underlined in "cdbcbbaa". Now, s = "cdbcba" and 5 points are added to the
 *   score.
 * - Remove the "ba" underlined in "cdbcba". Now, s = "cdbc" and 5 points are added to the score.
 * Total score = 5 + 4 + 5 + 5 = 19.
 * Example 2:
 *
 * Input: s = "aabbaaxybbaabb", x = 5, y = 4
 * Output: 20
 *
 * Constraints:
 *
 * 1 <= s.length <= 105
 * 1 <= x, y <= 104
 * s consists of lowercase English letters.
 */
/**
 * More space efficient.
 */
class Solution {
    int remove(string& s, string const& r, int x) {
        int i = 0, ans = 0; // `i` is write pointer, `j` is read pointer.
        for (int j = 0; j < s.size(); ++j) {
            s[i++] = s[j];
            if (i > 1 && s[i - 2] == r[0] && s[i - 1] == r[1]) {
                // We keep removing pattern string `r` when `r` shows up in the end of written part.
                i -= 2;
                ans += x;
            }
        }

        s.resize(i);
        return ans;
    }
public:
    int maximumGain(string s, int x, int y) {
        string a = "ab", b = "ba";

        if (x < y) {
            swap(a, b);
            swap(x, y);
        }

        auto s1 = remove(s, a, x);
        auto s2 = remove(s, b, y);
        return s1 + s2;
    }
};

/**
 * Another solution inspired from user votrubac.
 */
class Solution {
public:
    int greedy(string& s, char a, char b, int points) {
        int score = 0;
        string st;
        for (auto ch : s) {
            if (!st.empty() && st.back() == a && ch == b) {
                score += points;
                st.pop_back();
            } else {
                st += ch;
            }
        }
        swap(s, st);

        return score;
    }

    int maximumGain(string s, int x, int y, char a = 'a', char b = 'b') {
        if (y > x) {
            swap(x, y);
            swap(a, b);
        }

        auto s1 = greedy(s, a, b, x);
        auto s2 = greedy(s, b, a, y);
        return s1 + s2;
    }
};

/**
 * Original solution. TLE
 */
class Solution {
public:
    int maximumGain(string s, int x, int y) {
        vector<string> subs;
        string cur;
        for (auto c : s) {
            if (c > 'b') {
                if (cur.length() > 1) {
                    subs.emplace_back(std::move(cur));
                }
                cur.clear();
            }
            else {
                cur += c;
            }
        }
        subs.emplace_back(std::move(cur));

        int first_score = 0, second_score = 0;
        string first, second;
        if (x > y) {
            first_score = x;
            second_score = y;
            first = "ab";
            second = "ba";
        }
        else {
            first_score = y;
            second_score = x;
            first = "ba";
            second = "ab";
        }
        
        int ans = 0;
        for (auto&& sub : subs) {
            while (!sub.empty()) {
                auto pos = sub.find(first);
                if (pos != string::npos) {
                    ans += first_score;
                    
                }
                else {
                    pos = sub.find(second);
                    if (pos == string::npos) {
                        break;
                    }
                    ans += second_score;
                }
                sub = sub.substr(0, pos) + sub.substr(pos + 2);
            }
        }

        return ans;
    }
};
