/**
 * Course Schedule
 *
 * There are a total of numCourses courses you have to take, labeled from 0 to numCourses-1.
 *
 * Some courses may have prerequisites, for example to take course 0 you have to first take course 1, which is expressed
 * as a pair: [0,1]
 *
 * Given the total number of courses and a list of prerequisite pairs, is it possible for you to finish all courses?
 *
 * Example 1:
 *
 * Input: numCourses = 2, prerequisites = [[1,0]]
 * Output: true
 * Explanation: There are a total of 2 courses to take.
 *              To take course 1 you should have finished course 0. So it is possible.
 * Example 2:
 *
 * Input: numCourses = 2, prerequisites = [[1,0],[0,1]]
 * Output: false
 * Explanation: There are a total of 2 courses to take.
 *              To take course 1 you should have finished course 0, and to take course 0 you should
 *              also have finished course 1. So it is impossible.
 *
 * Constraints:
 *
 * The input prerequisites is a graph represented by a list of edges, not adjacency matrices. Read more about how a
 * graph is represented.
 * You may assume that there are no duplicate edges in the input prerequisites.
 * 1 <= numCourses <= 10^5
 */
/**
 * BFS solution.
 */
class Solution {
public:
    bool canFinish(int n, vector<vector<int>>& prerequisites) {
        vector<vector<int>> G(n);
        vector<int> degree(n, 0);
        vector<int> bfs;

        for (auto const& cp : prerequisites) {
            G[cp[1]].push_back(cp[0]);
            degree[cp[0]]++;
        }

        for (int i = 0; i < n; ++i){
            if (!degree[i]) {
                bfs.push_back(i);
            }
        }

        for (int i = 0; i < bfs.size(); ++i) {
            for (int j : G[bfs[i]]) {
                if (--degree[j] == 0) {
                    bfs.push_back(j);
                }
            }
        }

        return bfs.size() == n;
    }
};

/**
 * Speed things up. just detect cycle using DFS without calculate all the dependences.
 */
class Solution {
public:
    bool canFinish(int numCourses, vector<vector<int>>& prerequisites) {
        vector<vector<int>> courses_dependence(numCourses);
        vector<bool> detected(numCourses, false);
        for (auto const& cp : prerequisites) {
            courses_dependence[cp[0]].push_back(cp[1]);
        }

        for (int i = 0; i < numCourses; ++i) {
            if (detected[i]) {
                continue;
            }

            if (detect_cycle(courses_dependence, detected, i)) {
                return false;
            }
        }

        return true;
    }

private:
    bool detect_cycle(vector<vector<int>> const& cd, vector<bool>& detected, int i)
    {
        vector<bool> visited(cd.size(), false);

        visited[i] = true;
        return dfs(cd, detected, visited, i);
    }

    bool dfs(vector<vector<int>> const& cd, vector<bool>& detected, vector<bool>& visited, int i)
    {
        detected[i] = true;
        for (auto c : cd[i]) {
            if (visited[c]) {
                return true;
            }

            visited[c] = true;
            if (dfs(cd, detected, visited, c)) {
                return true;
            }
            visited[c] = false;
        }

        return false;
    }
};

/**
 * Original solution. BFS.
 */
class Solution {
public:
    bool canFinish(int numCourses, vector<vector<int>>& prerequisites) {
        vector<vector<bool>> is_depend(numCourses, vector<bool>(numCourses, false));
        vector<vector<int>> courses_dependence(numCourses);

        for (auto const& cp : prerequisites) {
            courses_dependence[cp[0]].push_back(cp[1]);
        }

        for (int i = 0; i < numCourses; ++i) {
            deque<int> q{i};
            while (!q.empty()) {
                auto c1 = q.front(); q.pop_front();
                for (auto c2 : courses_dependence[c1]) {
                    if (!is_depend[i][c2]) {
                        is_depend[i][c2] = true;
                        q.push_back(c2);
                    }
                    if (is_depend[c2][i]) {
                        return false;
                    }
                }
            }
        }

        return true;
    }
};
