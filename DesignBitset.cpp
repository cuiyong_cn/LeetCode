/**
 * Design Bitset
 *
 * A Bitset is a data structure that compactly stores bits.
 *
 * Implement the Bitset class:
 *
 * Bitset(int size) Initializes the Bitset with size bits, all of which are 0.
 * void fix(int idx) Updates the value of the bit at the index idx to 1. If the value was already 1, no change occurs.
 * void unfix(int idx) Updates the value of the bit at the index idx to 0. If the value was already 0, no change occurs.
 * void flip() Flips the values of each bit in the Bitset. In other words, all bits with value 0 will now have value 1 and vice versa.
 * boolean all() Checks if the value of each bit in the Bitset is 1. Returns true if it satisfies the condition, false otherwise.
 * boolean one() Checks if there is at least one bit in the Bitset with value 1. Returns true if it satisfies the condition, false otherwise.
 * int count() Returns the total number of bits in the Bitset which have value 1.
 * String toString() Returns the current composition of the Bitset. Note that in the resultant string, the character at the ith index should coincide with the value at the ith bit of the Bitset.
 *
 * Example 1:
 *
 * Input
 * ["Bitset", "fix", "fix", "flip", "all", "unfix", "flip", "one", "unfix", "count", "toString"]
 * [[5], [3], [1], [], [], [0], [], [], [0], [], []]
 * Output
 * [null, null, null, null, false, null, null, true, null, 2, "01010"]
 *
 * Explanation
 * Bitset bs = new Bitset(5); // bitset = "00000".
 * bs.fix(3);     // the value at idx = 3 is updated to 1, so bitset = "00010".
 * bs.fix(1);     // the value at idx = 1 is updated to 1, so bitset = "01010".
 * bs.flip();     // the value of each bit is flipped, so bitset = "10101".
 * bs.all();      // return False, as not all values of the bitset are 1.
 * bs.unfix(0);   // the value at idx = 0 is updated to 0, so bitset = "00101".
 * bs.flip();     // the value of each bit is flipped, so bitset = "11010".
 * bs.one();      // return True, as there is at least 1 index with value 1.
 * bs.unfix(0);   // the value at idx = 0 is updated to 0, so bitset = "01010".
 * bs.count();    // return 2, as there are 2 bits with value 1.
 * bs.toString(); // return "01010", which is the composition of bitset.
 *
 * Constraints:
 *
 * 1 <= size <= 105
 * 0 <= idx <= size - 1
 * At most 105 calls will be made in total to fix, unfix, flip, all, one, count, and toString.
 * At least one call will be made to all, one, count, or toString.
 * At most 5 calls will be made to toString.
 */
/**
 * Optimized.
 */
string flip(string const& bits) {
    auto ans = bits;
    for (auto& b : ans) {
        b = '1' == b ? '0' : '1';
    }
    return ans;
}

class Bitset {
public:
    Bitset(int size) : bits(size, '0'), one_count{0}, flipped{false} {

    }

    void fix(int idx) {
        if (need_fix(idx)) {
            bits[idx] = flipped ? '0' : '1';
            ++one_count;
        }
    }

    void unfix(int idx) {
        if (need_unfix(idx)) {
            bits[idx] = flipped ? '1' : '0';
            --one_count;
        }
    }

    void flip() {
        flipped = flipped ? false : true;
        one_count = bits.size() - one_count;
    }

    bool all() {
        return bits.size() == one_count;
    }

    bool one() {
        return one_count > 0;
    }

    int count() {
        return one_count;
    }

    string toString() {
        return flipped ? ::flip(bits) : bits;
    }

private:
    bool need_fix(int idx) {
        return flipped ? '1' == bits[idx] : '0' == bits[idx];
    }

    bool need_unfix(int idx) {
        return flipped ? '0' == bits[idx] : '1' == bits[idx];
    }

    string bits;
    uint32_t one_count;
    bool flipped;
};

/**
 * Your Bitset object will be instantiated and called as such:
 * Bitset* obj = new Bitset(size);
 * obj->fix(idx);
 * obj->unfix(idx);
 * obj->flip();
 * bool param_4 = obj->all();
 * bool param_5 = obj->one();
 * int param_6 = obj->count();
 * string param_7 = obj->toString();
 */

/**
 * Original solution. TLE.
 */
class Bitset {
public:
    Bitset(int size) : bits(size, false) {

    }

    void fix(int idx) {
        bits[idx] = true;
    }

    void unfix(int idx) {
        bits[idx] = false;
    }

    void flip() {
        transform(begin(bits), end(bits), begin(bits),
            [](auto const& b) {
                return false == b;
            });
    }

    bool all() {
        return all_of(begin(bits), end(bits), [](auto const& b) {
            return true == b;
        });
    }

    bool one() {
        return any_of(begin(bits), end(bits), [](auto const& b) {
            return true == b;
        });
    }

    int count() {
        return std::count(begin(bits), end(bits), true);
    }

    string toString() {
        ostringstream oss;
        for (auto const& b : bits) {
            oss << (true == b ? '1' : '0');
        }
        return oss.str();
    }

private:
    vector<bool> bits;
};

/**
 * Your Bitset object will be instantiated and called as such:
 * Bitset* obj = new Bitset(size);
 * obj->fix(idx);
 * obj->unfix(idx);
 * obj->flip();
 * bool param_4 = obj->all();
 * bool param_5 = obj->one();
 * int param_6 = obj->count();
 * string param_7 = obj->toString();
 */
