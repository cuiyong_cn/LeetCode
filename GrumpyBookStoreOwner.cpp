/**
 * Grumpy Bookstore Owner
 *
 * Today, the bookstore owner has a store open for customers.length minutes.  Every minute, some number of customers
 * (customers[i]) enter the store, and all those customers leave after the end of that minute.
 *
 * On some minutes, the bookstore owner is grumpy.  If the bookstore owner is grumpy on the i-th minute, grumpy[i] = 1,
 * otherwise grumpy[i] = 0.  When the bookstore owner is grumpy, the customers of that minute are not satisfied,
 * otherwise they are satisfied.
 *
 * The bookstore owner knows a secret technique to keep themselves not grumpy for X minutes straight, but can only use it once.
 *
 * Return the maximum number of customers that can be satisfied throughout the day.
 *
 * Example 1:
 *
 * Input: customers = [1,0,1,2,1,1,7,5], grumpy = [0,1,0,1,0,1,0,1], X = 3
 * Output: 16
 * Explanation: The bookstore owner keeps themselves not grumpy for the last 3 minutes.
 * The maximum number of customers that can be satisfied = 1 + 1 + 1 + 1 + 7 + 5 = 16.
 *
 * Note:
 *
 *     1 <= X <= customers.length == grumpy.length <= 20000
 *     0 <= customers[i] <= 1000
 *     0 <= grumpy[i] <= 1
 */
/**
 * More efficient one. Using sliding window approach.
 */
class Solution {
public:
    int maxSatisfied(vector<int>& cs, vector<int>& grumpy, int X) {
        auto satisfied = 0, m_add_satisfied = 0, add_satisfied = 0;
        for (auto i = 0; i < cs.size(); ++i) {
            satisfied += grumpy[i] ? 0 : cs[i];
            add_satisfied += grumpy[i] ? cs[i] : 0;
            if (i >= X) add_satisfied -= grumpy[i - X] ? cs[i - X] : 0;
            m_add_satisfied = max(m_add_satisfied, add_satisfied);
        }
        return satisfied + m_add_satisfied;
    }
};

/**
 * Improved one.
 */
class Solution {
public:
    map<int, int> m;
    int getSatisfied(vector<int>& customers, vector<int>& grumpy, int s, int X) {
        int ans = 0;
        int end = s + X;
        if (1 == m.count(s - 1)) {
            ans = m[s - 1];
            if (1 == grumpy[s - 1]) {
                ans -= customers[s - 1];
            }
            if (1 == grumpy[end - 1]) {
                ans += customers[end - 1];
            }
        }
        else {
            for (int i = 0; i < customers.size(); ++i) {
                if (s <= i && i < end) {
                    ans += customers[i];
                }
                else {
                    if (0 == grumpy[i]) {
                        ans += customers[i];
                    }
                }
            }
        }

        m[s] = ans;

        return ans;
    }

    int maxSatisfied(vector<int>& customers, vector<int>& grumpy, int X) {
        int ans = 0;
        for (int i = 0; i < customers.size() - X + 1; ++i) {
            ans = max(ans, getSatisfied(customers, grumpy, i, X));
        }
        return ans;
    }
};

/**
 * Original solution. Brute force, simple but TLE. We have to using some kind of memorizing method to speed it up.
 */
class Solution {
public:
    int getSatisfied(vector<int>& customers, vector<int>& grumpy, int s, int X) {
        int end = s + X;
        int ans = 0;
        for (int i = 0; i < customers.size(); ++i) {
            if (s <= i && i < end) {
                ans += customers[i];
            }
            else {
                if (0 == grumpy[i]) {
                    ans += customers[i];
                }
            }
        }
        return ans;
    }

    int maxSatisfied(vector<int>& customers, vector<int>& grumpy, int X) {
        int ans = 0;
        for (int i = 0; i < customers.size() - X + 1; ++i) {
            ans = max(ans, getSatisfied(customers, grumpy, i, X));
        }
        return ans;
    }
};
