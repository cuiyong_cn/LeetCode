/**
 * Sentence Similarity III
 *
 * A sentence is a list of words that are separated by a single space with no leading or trailing
 * spaces. For example, "Hello World", "HELLO", "hello world hello world" are all sentences. Words
 * consist of only uppercase and lowercase English letters.
 *
 * Two sentences sentence1 and sentence2 are similar if it is possible to insert an arbitrary
 * sentence (possibly empty) inside one of these sentences such that the two sentences become equal.
 * For example, sentence1 = "Hello my name is Jane" and sentence2 = "Hello Jane" can be made equal
 * by inserting "my name is" between "Hello" and "Jane" in sentence2.
 *
 * Given two sentences sentence1 and sentence2, return true if sentence1 and sentence2 are similar.
 * Otherwise, return false.
 *
 * Example 1:
 *
 * Input: sentence1 = "My name is Haley", sentence2 = "My Haley"
 * Output: true
 * Explanation: sentence2 can be turned to sentence1 by inserting "name is" between "My" and "Haley".
 * Example 2:
 *
 * Input: sentence1 = "of", sentence2 = "A lot of words"
 * Output: false
 * Explanation: No single sentence can be inserted inside one of the sentences to make it equal to the other.
 * Example 3:
 *
 * Input: sentence1 = "Eating right now", sentence2 = "Eating"
 * Output: true
 * Explanation: sentence2 can be turned to sentence1 by inserting "right now" at the end of the sentence.
 *
 * Constraints:
 *
 * 1 <= sentence1.length, sentence2.length <= 100
 * sentence1 and sentence2 consist of lowercase and uppercase English letters and spaces.
 * The words in sentence1 and sentence2 are separated by a single space.
 */
/**
 * Improved version.
 */
deque<string> splits(string const& sentence)
{
    stringstream ss{sentence};
    return deque<string>{istream_iterator<string>{ss}, istream_iterator<string>{}};
}

class Solution {
public:
    bool areSentencesSimilar(string sentence1, string sentence2) {
        auto words1 = splits(sentence1);
        auto words2 = splits(sentence2);

        while (!words1.empty() && !words2.empty() && words1.front() == words2.front()) {
            words1.pop_front();
            words2.pop_front();
        }

        while (!words1.empty() && !words2.empty() && words1.back() == words2.back()) {
            words1.pop_back();
            words2.pop_back();
        }

        return words1.empty() || words2.empty();
    }
};

/**
 * Original solution.
 */
vector<string> splits(string const& sentence)
{
    stringstream ss{sentence};
    vector<string> words;
    for (string w; ss >> w;) {
        words.emplace_back(w);
    }

    return words;
}

class Solution {
public:
    bool areSentencesSimilar(string sentence1, string sentence2) {
        auto words1 = splits(sentence1);
        auto words2 = splits(sentence2);

        int const len = min(words1.size(), words2.size());
        int prefix_idx = len;
        for (int i = 0; i < len; ++i) {
            if (words1[i] != words2[i]) {
                prefix_idx = i - 1;
                break;
            }
        }

        if (len == prefix_idx) {
            return true;
        }

        for (int i = words1.size() - 1, j = words2.size() - 1; i > prefix_idx && j > prefix_idx; --i,--j) {
            if (words1[i] != words2[j]) {
                return false;
            }
        }

        return true;
    }
};
