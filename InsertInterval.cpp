/**
 * Insert Interval
 *
 * Given a set of non-overlapping intervals, insert a new interval into the intervals (merge if
 * necessary).
 *
 * You may assume that the intervals were initially sorted according to their start times.
 *
 * Example 1:
 *
 * Input: intervals = [[1,3],[6,9]], newInterval = [2,5]
 * Output: [[1,5],[6,9]]
 * Example 2:
 *
 * Input: intervals = [[1,2],[3,5],[6,7],[8,10],[12,16]], newInterval = [4,8]
 * Output: [[1,2],[3,10],[12,16]]
 * Explanation: Because the new interval [4,8] overlaps with [3,5],[6,7],[8,10].
 * Example 3:
 *
 * Input: intervals = [], newInterval = [5,7]
 * Output: [[5,7]]
 * Example 4:
 *
 * Input: intervals = [[1,5]], newInterval = [2,3]
 * Output: [[1,5]]
 * Example 5:
 *
 * Input: intervals = [[1,5]], newInterval = [2,7]
 * Output: [[1,7]]
 *
 * Constraints:
 *
 * 0 <= intervals.length <= 104
 * intervals[i].length == 2
 * 0 <= intervals[i][0] <= intervals[i][1] <= 105
 * intervals is sorted by intervals[i][0] in ascending order.
 * newInterval.length == 2
 * 0 <= newInterval[0] <= newInterval[1] <= 105
 */
/**
 * STL version.
 */
class Solution {
public:
    using Interval = vector<int>;
    vector<Interval> insert(vector<Interval>& intervals, Interval newInterval) {
        auto compare = [] (const Interval &intv1, const Interval &intv2)
                          { return intv1[1] < intv2[0]; };
        auto range = equal_range(intervals.begin(), intervals.end(), newInterval, compare);
        auto itr1 = range.first, itr2 = range.second;
        if (itr1 == itr2) {
            intervals.insert(itr1, newInterval);
        } else {
            itr2--;
            (*itr2)[0] = min<int>(newInterval[0], (*itr1)[0]);
            (*itr2)[1] = max<int>(newInterval[1], (*itr2)[1]);
            intervals.erase(itr1, itr2);
        }
        return intervals;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<vector<int>> insert(vector<vector<int>>& intervals, vector<int>& newInterval) {
        vector<vector<int>> ans;

        int istart = newInterval[0], iend = newInterval[1];
        int i = 0;
        for (; i < intervals.size(); ++i) {
            if (intervals[i][1] >= istart) {
                break;
            }

            ans.push_back(intervals[i]);
        }

        if (i < intervals.size() && iend >= intervals[i][0]) {
            int s = min(istart, intervals[i][0]);
            int e = max(iend, intervals[i][1]);
            for (i+=1; i < intervals.size(); ++i) {
                if (iend < intervals[i][0]) {
                    break;
                }
                e = max(e, intervals[i][1]);
            }
            ans.push_back({s, e});
        }
        else {
            ans.push_back(newInterval);
        }

        for (; i < intervals.size(); ++i) {
            ans.push_back(intervals[i]);
        }

        return ans;
    }
};
