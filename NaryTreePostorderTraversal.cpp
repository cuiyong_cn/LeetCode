/**
 * N-ary Tree Postorder Traversal
 *
 * Given an n-ary tree, return the postorder traversal of its nodes' values.
 *
 * For example, given a 3-ary tree:
 *                        1
 *                       /|\
 *                      / | \
 *                     3  2  4
 *                    / \
 *                   5   6
 *
 * Return its postorder traversal as: [5,6,3,2,4,1].
 *
 * Note:
 *
 * Recursive solution is trivial, could you do it iteratively?
 */
/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/
/*
 * Iterative one withou reverse.
 *
 * class Solution {
 * public:
 *     vector<int> postorder(Node* root) {
 *         if (!root) {
 *             return {};
 *         }
 *         std::stack<std::pair<Node *, int>> s;
 *         std::vector<int> res;
 *         s.emplace(root, 0);
 *         while (!s.empty()) {
 *             auto &[node, index] = s.top();
 *             if (index == node->children.size()) {
 *                 res.push_back(node->val);
 *                 s.pop();
 *             } else {
 *                 s.emplace(node->children[index++], 0);
 *             }
 *         }
 *         return res;
 *     }
 * };
 */
/*
 * Iterative with reverse.
 *
 * vector<int> postorder(Node* root) {
 *     if(root==NULL) return {};
 *     vector<int> res;
 *     stack<Node*> stk;
 *     stk.push(root);
 *     while(!stk.empty())
 *     {
 *         Node* temp=stk.top();
 *         stk.pop();
 *         for(int i=0;i<temp->children.size();i++) stk.push(temp->children[i]);
 *         res.push_back(temp->val);
 *     }
 *     reverse(res.begin(), res.end());
 *     return res;
 * }
 */
class Solution {
public:
    void travel(Node* r, vector<int>& v) {
        for (auto n : r->children) {
            travel(n, v);
        }
        v.push_back(r->val);
    }

    vector<int> postorder(Node* root) {
        vector<int> ans;

        if (!root) return ans;

        travel(root, ans);

        return ans;
    }
};

