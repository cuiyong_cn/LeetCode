/**
 * Sort the People
 *
 * You are given an array of strings names, and an array heights that consists of distinct positive
 * integers. Both arrays are of length n.
 *
 * For each index i, names[i] and heights[i] denote the name and height of the ith person.
 *
 * Return names sorted in descending order by the people's heights.
 *
 * Example 1:
 *
 * Input: names = ["Mary","John","Emma"], heights = [180,165,170]
 * Output: ["Mary","Emma","John"]
 * Explanation: Mary is the tallest, followed by Emma and John.
 * Example 2:
 *
 * Input: names = ["Alice","Bob","Bob"], heights = [155,185,150]
 * Output: ["Bob","Alice","Bob"]
 * Explanation: The first Bob is the tallest, followed by Alice and the second Bob.
 *
 * Constraints:
 *
 * n == names.length == heights.length
 * 1 <= n <= 103
 * 1 <= names[i].length <= 20
 * 1 <= heights[i] <= 105
 * names[i] consists of lower and upper case English letters.
 * All the values of heights are distinct.
 */
/**
 * map solution.
 */
class Solution {
public:
    vector<string> sortPeople(vector<string>& names, vector<int>& heights) {
        auto name_height = map<int, string>{};
        for (auto i = size_t{0}; i < names.size(); ++i) {
            name_height[heights[i]] = names[i];
        }
        auto ans = vector<string>{};
        for (auto const& nh : name_height) {
            ans.push_back(nh.second);
        }
        reverse(begin(ans), end(ans));
        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<string> sortPeople(vector<string>& names, vector<int>& heights) {
        auto idxes = vector<int>(names.size(), 0);
        iota(begin(idxes), end(idxes), 0);
        sort(begin(idxes), end(idxes),
            [&heights](auto i, auto j) {
                return heights[i] > heights[j];
            });
        auto ans = vector<string>(names.size());
        transform(begin(idxes), end(idxes), begin(ans),
            [&names](auto i) {
                return names[i];
            });
        return ans;
    }
};
