/**
 * Maximum Product of Three Numbers
 *
 * Given an integer array, find three numbers whose product is maximum and output the maximum product.
 *
 * Example 1:
 *
 * Input: [1,2,3]
 * Output: 6
 *
 * Example 2:
 *
 * Input: [1,2,3,4]
 * Output: 24
 *
 * Note:
 *
 * The length of the given array will be in range [3,104] and all elements are in the range [-1000, 1000].
 * Multiplication of any three numbers in the input won't exceed the range of 32-bit signed integer.
 */
/**
 * DP solution but in fact it is just the same as the blow solution.
 * f[0][1]  ONE NUMBER maximum multiplation for array
 * f[0][2]  TWO NUMBERS maximum multiplation for array
 * f[0][3]  THREE NUMBERS maximum multiplation for array
 * f[1][1]  ONE NUMBER minimum multiplation for array
 * f[1][2]  TWO NUMBERS minimum multiplation for array
 * f[1][3]  THREE NUMBERS minimum multiplation for array
 */
class Solution {
public:
    int maximumProduct(vector<int>& nums) {
        int f[2][4], i, j;
        f[0][0] = f[1][0] = 1;
        for (j = 3; j > 0; --j) {
            f[0][j] = INT_MIN;
            f[1][j] = INT_MAX;
        }

        for (i = 0; i < nums.size(); ++i) {
            for (j = 3; j > 0; --j) {
                if (f[0][j - 1] == INT_MIN) {
                    continue;
                }
                f[0][j] = max(f[0][j], max(f[0][j - 1] * nums[i], f[1][j - 1] * nums[i]));
                f[1][j] = min(f[1][j], min(f[0][j - 1] * nums[i], f[1][j - 1] * nums[i]));
            }
        }

        return f[0][3];
    }
};

/**
 * Solution without the use of sort written in java. But I prefer the sort one.
 */
public class Solution {
    public int maximumProduct(int[] nums) {
        int min1 = Integer.MAX_VALUE, min2 = Integer.MAX_VALUE;
        int max1 = Integer.MIN_VALUE, max2 = Integer.MIN_VALUE, max3 = Integer.MIN_VALUE;
        for (int n: nums) {
            if (n <= min1) {
                min2 = min1;
                min1 = n;
            } else if (n <= min2) {     // n lies between min1 and min2
                min2 = n;
            }
            if (n >= max1) {            // n is greater than max1, max2 and max3
                max3 = max2;
                max2 = max1;
                max1 = n;
            } else if (n >= max2) {     // n lies betweeen max1 and max2
                max3 = max2;
                max2 = n;
            } else if (n >= max3) {     // n lies betwen max2 and max3
                max3 = n;
            }
        }
        return Math.max(min1 * min2 * max1, max1 * max2 * max3);
    }
}

/**
 * Original solution.
 */
class Solution {
public:
    int maximumProduct(vector<int>& nums) {
        sort(std::begin(nums), std::end(nums));

        return nums.back() * std::max(nums[0] * nums[1], nums[nums.size() - 2] * nums[nums.size() - 3]);
    }
};
