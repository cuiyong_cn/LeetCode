/**
 * Convert BST to Greater Tree
 *
 * Given a Binary Search Tree (BST), convert it to a Greater Tree such that every key of the original BST is changed to
 * the original key plus sum of all keys greater than the original key in BST.
 *
 * Example:
 *
 * Input: The root of a Binary Search Tree like this:
 *               5
 *             /   \
 *            2     13
 *
 * Output: The root of a Greater Tree like this:
 *              18
 *             /   \
 *           20     13
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/**
 * Iterative solution.
 * See also Morris solution.
 */
class Solution {
public:
    TreeNode* convertBST(TreeNode* root) {
        int sum = 0;
        TreeNode* node = root;
        stack<TreeNode*> stk;

        while (false == stk.empty() || nullptr != node) {
            while (nullptr != node) {
                stk.push(node);
                node = node->right;
            }

            node = stk.top();
            stk.pop();
            sum += node->val;
            node->val = sum;
            node = node->left;
        }
        return root;
    }
};

/**
 * Original solution. DFS inorder traversal method
 */
class Solution {
public:
    int dfs(TreeNode* root, int val) {
        int ans = val;
        if (nullptr != root) {
            root->val += dfs(root->right, val);
            ans = dfs(root->left, root->val);
        }
        return ans;
    }

    TreeNode* convertBST(TreeNode* root) {
        if (nullptr != root) {
            root->val += dfs(root->right, 0);
            dfs(root->left, root->val);
        }
        return root;
    }
};
