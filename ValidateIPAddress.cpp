/**
 * Validate IP Address
 *
 * Given a string IP, return "IPv4" if IP is a valid IPv4 address, "IPv6" if IP is a valid IPv6
 * address or "Neither" if IP is not a correct IP of any type.
 *
 * A valid IPv4 address is an IP in the form "x1.x2.x3.x4" where 0 <= xi <= 255 and xi cannot
 * contain leading zeros. For example, "192.168.1.1" and "192.168.1.0" are valid IPv4 addresses but
 * "192.168.01.1", while "192.168.1.00" and "192.168@1.1" are invalid IPv4 addresses.
 *
 * A valid IPv6 address is an IP in the form "x1:x2:x3:x4:x5:x6:x7:x8" where:
 *
 * 1 <= xi.length <= 4 xi is a hexadecimal string which may contain digits, lower-case English
 * letter ('a' to 'f') and upper-case English letters ('A' to 'F').  Leading zeros are allowed in
 * xi.  For example, "2001:0db8:85a3:0000:0000:8a2e:0370:7334" and
 * "2001:db8:85a3:0:0:8A2E:0370:7334" are valid IPv6 addresses, while
 * "2001:0db8:85a3::8A2E:037j:7334" and "02001:0db8:85a3:0000:0000:8a2e:0370:7334" are invalid IPv6
 * addresses.
 *
 * Example 1:
 *
 * Input: IP = "172.16.254.1"
 * Output: "IPv4"
 * Explanation: This is a valid IPv4 address, return "IPv4".
 * Example 2:
 *
 * Input: IP = "2001:0db8:85a3:0:0:8A2E:0370:7334"
 * Output: "IPv6"
 * Explanation: This is a valid IPv6 address, return "IPv6".
 * Example 3:
 *
 * Input: IP = "256.256.256.256"
 * Output: "Neither"
 * Explanation: This is neither a IPv4 address nor a IPv6 address.
 * Example 4:
 *
 * Input: IP = "2001:0db8:85a3:0:0:8A2E:0370:7334:"
 * Output: "Neither"
 * Example 5:
 *
 * Input: IP = "1e1.4.5.6"
 * Output: "Neither"
 *
 * Constraints:
 *
 * IP consists only of English letters, digits and the characters '.' and ':'.
 */
/**
 * Regex solution. Although this one is insanely short. But performance sucks.
 * Also uses lots of memory.
 * I like my the original one.
 */
class Solution {
public:
    string validIPAddress(string IP) {
        regex ipv4("(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])"), ipv6("((([0-9a-fA-F]){1,4})\\:){7}([0-9a-fA-F]){1,4}");   // create regex object for regulare expression
        if(regex_match(IP, ipv4)) // match regex expression with given IP string for IPv4
            return "IPv4";
        else if(regex_match(IP, ipv6)) // match regex expression with given IP string for IPv6
            return "IPv6";
        return "Neither"; // Otherwise return "Neither"
    }
};

/**
 * Original solution. Faster than 100%, with O(n) time, O(1) space
 */
class Solution {
public:
    string validIPAddress(string IP) {
        if (IP.length() > 39) {
            return "Neither";
        }

        if (!isxdigit(IP.back())) {
            return "Neither";
        }

        auto pos = IP.find_first_of(".:");
        if (pos == string::npos) {
            return "Neither";
        }

        int prev = 0;
        int cnt = 0;
        if ('.' == IP[pos]) {
            // maybe ipv4
            while (prev < IP.length()) {
                auto len = pos - prev;
                if (cnt > 3 || !isdigit(IP[prev]) || len > 3 || (len > 1 && '0' == IP[prev])) {
                    return "Neither";
                }

                int xi = IP[prev] - '0';
                for (size_t i = prev + 1; i < pos; ++i) {
                    if (!isdigit(IP[i])) {
                        return "Neither";
                    }
                    xi = xi * 10 + (IP[i] - '0');
                }

                if (xi > 255) {
                    return "Neither";
                }

                prev = pos + 1;
                pos = IP.find_first_of('.', pos + 1);
                if (pos == string::npos) {
                    pos = IP.length();
                }
                ++cnt;
            }

            return 4 == cnt ? "IPv4" : "Neither";
        }

        // maybe ipv6
        while (prev < IP.length()) {
            auto len = pos - prev;
            if (cnt > 7 || !isxdigit(IP[prev]) || len > 4) {
                return "Neither";
            }

            for (size_t i = prev + 1; i < pos; ++i) {
                if (!isxdigit(IP[i])) {
                    return "Neither";
                }
            }

            prev = pos + 1;
            pos = IP.find_first_of(':', pos + 1);
            if (pos == string::npos) {
                pos = IP.length();
            }
            ++cnt;
        }

        return 8 == cnt ? "IPv6" : "Neither";
    }
};
