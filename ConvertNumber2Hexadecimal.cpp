/**
 * Convert a Number to Hexadecimal
 *
 * Given an integer, write an algorithm to convert it to hexadecimal. For negative integer, two’s complement method is
 * used.
 *
 * Note:
 *
 * All letters in hexadecimal (a-f) must be in lowercase.
 * The hexadecimal string must not contain extra leading 0s. If the number is zero, it is represented by a single zero
 * character '0'; otherwise, the first character in the hexadecimal string will not be the zero character.  The given
 * number is guaranteed to fit within the range of a 32-bit signed integer.  You must not use any method provided by the
 * library which converts/formats the number to hex directly.
 *
 * Example 1:
 *
 * Input:
 * 26
 *
 * Output:
 * "1a"
 * Example 2:
 *
 * Input:
 * -1
 *
 * Output:
 * "ffffffff"
 */
/**
 * Improve the below solution.
 */
class Solution {
public:
    string toHex(int num) {
        if (0 == num) {
            return "0";
        }

        string ans;
        unsigned int n = num;
        while (n) {
            int b4 = n & 0xf;
            ans = string(1, b4 > 9 ? 'a' + (b4 - 10) : '0' + b4) + ans;
            n >>= 4;
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    string toHex(int num) {
        if (0 == num) {
            return "0";
        }

        string ans;
        int shift = 28;
        while (shift >= 0) {
            int n = (num >> shift) & 0xf;
            if (n > 0) {
                break;
            }
            shift -= 4;
        }

        for (int i = shift; i >= 0; i -= 4) {
            int n = (num >> i) & 0xf;
            ans += n > 9 ? 'a' + (n - 10) : '0' + n;
        }

        return ans;
    }
};
