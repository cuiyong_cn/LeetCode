/**
 * 3Sum With Multiplicity
 *
 * Given an integer array arr, and an integer target, return the number of tuples i, j, k such that
 * i < j < k and arr[i] + arr[j] + arr[k] == target.
 *
 * As the answer can be very large, return it modulo 109 + 7.
 *
 * Example 1:
 *
 * Input: arr = [1,1,2,2,3,3,4,4,5,5], target = 8
 * Output: 20
 * Explanation:
 * Enumerating by the values (arr[i], arr[j], arr[k]):
 * (1, 2, 5) occurs 8 times;
 * (1, 3, 4) occurs 8 times;
 * (2, 2, 4) occurs 2 times;
 * (2, 3, 3) occurs 2 times.
 * Example 2:
 *
 * Input: arr = [1,1,2,2,2,2], target = 5
 * Output: 12
 * Explanation:
 * arr[i] = 1, arr[j] = arr[k] = 2 occurs 12 times:
 * We choose one 1 from [1,1] in 2 ways,
 * and two 2s from [2,2,2,2] in 6 ways.
 *
 * Constraints:
 *
 * 3 <= arr.length <= 3000
 * 0 <= arr[i] <= 100
 * 0 <= target <= 300
 */
/**
 * same with below solution. But clear.
 */
class Solution {
public:
    int threeSumMulti(vector<int>& A, int target) {
        unordered_map<int, long> c;
        for (int a : A) c[a]++;
        long res = 0;
        for (auto it : c)
            for (auto it2 : c) {
                int i = it.first, j = it2.first, k = target - i - j;
                if (!c.count(k)) continue;
                if (i == j && j == k)
                    res += c[i] * (c[i] - 1) * (c[i] - 2) / 6;
                else if (i == j && j != k)
                    res += c[i] * (c[i] - 1) / 2 * c[k];
                else if (i < j && j < k)
                    res += c[i] * c[j] * c[k];
            }
        return res % int(1e9 + 7);
    }
};

/**
 * Fast but complex one.
 */
class Solution {
public:
    int threeSumMulti(vector<int>& arr, int target) {
        long const mod = 1000000007;
        vector<long> count(101, 0);
        long ans = 0;

        for (auto n : arr) {
            ++count[n];
        }

        // x != y != z
        for (int x = 0; x <= 100; ++x) {
            if (0 == count[x]) {
                continue;
            }

            for (int y = x + 1; y <= 100; ++y) {
                if (0 == count[y]) {
                    continue;
                }

                int z = target - x - y;
                if (y >= z) {
                    break;
                }

                if (z <= 100) {
                    ans += count[x] * count[y] * count[z];
                    ans %= mod;
                }
            }
        }

        // x == y != z
        for (int x = 0; x <= 100; ++x) {
            if (0 == count[x]) {
                continue;
            }

            int z = target - 2 * x;
            if (x >= z) {
                break;
            }

            if (z <= 100) {
                ans += count[x] * (count[x] - 1) / 2 * count[z];
                ans %= mod;
            }
        }

        // x != y == z
        for (int x = 0; x <= 100; ++x) {
            if (0 == count[x]) {
                continue;
            }

            if (target % 2 == x % 2) {
                int y = (target - x) / 2;
                if (y <= x) {
                    break;
                }

                if (y <= 100) {
                    ans += count[x] * count[y] * (count[y] - 1) / 2;
                    ans %= mod;
                }
            }
        }

        // x == y == z
        if (target % 3 == 0) {
            int x = target / 3;
            ans += count[x] * (count[x] - 1) * (count[x] - 2) / 6;
            ans %= mod;
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int threeSumMulti(vector<int>& arr, int target) {
        long const mod = 1000000007;
        vector<int> two_sum_count(301, 0);
        ++two_sum_count[arr[0] + arr[1]];
        long ans = 0;
        for (int j = 2; j < arr.size(); ++j) {
            if ((target - arr[j]) >= 0) {
                ans += two_sum_count[target - arr[j]];
                ans %= mod;
            }

            for (int i = 0; i < j; ++i) {
                ++two_sum_count[arr[i] + arr[j]];
            }
        }

        return ans;
    }
};
