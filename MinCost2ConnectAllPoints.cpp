/**
 * Min Cost to Connect All Points
 *
 * You are given an array points representing integer coordinates of some points on a 2D-plane, where points[i] = [xi,
 * yi].
 *
 * The cost of connecting two points [xi, yi] and [xj, yj] is the manhattan distance between them: |xi - xj| + |yi -
 * yj|, where |val| denotes the absolute value of val.
 *
 * Return the minimum cost to make all points connected. All points are connected if there is exactly one simple path
 * between any two points.
 *
 * Example 1:
 *
 * Input: points = [[0,0],[2,2],[3,10],[5,2],[7,0]]
 * Output: 20
 * Explanation:
 *
 * We can connect the points as shown above to get the minimum cost of 20.
 * Notice that there is a unique path between every pair of points.
 * Example 2:
 *
 * Input: points = [[3,12],[-2,5],[-4,1]]
 * Output: 18
 * Example 3:
 *
 * Input: points = [[0,0],[1,1],[1,0],[-1,1]]
 * Output: 4
 * Example 4:
 *
 * Input: points = [[-1000000,-1000000],[1000000,1000000]]
 * Output: 4000000
 * Example 5:
 *
 * Input: points = [[0,0]]
 * Output: 0
 *
 * Constraints:
 *
 * 1 <= points.length <= 1000
 * -106 <= xi, yi <= 106
 * All pairs (xi, yi) are distinct.
 */
/**
 * From the discussion. Prim's method.
 */
class Solution
{
public:
    int minCostConnectPoints(vector<vector<int>>& ps) {
        int n = ps.size();
        int res = 0;
        int i = 0;
        int connected = 0;
        vector<bool> visited(n);
        priority_queue<pair<int, int>> pq;

        while (++connected < n) {
            visited[i] = true;
            for (int j = 0; j < n; ++j) {
                if (!visited[j]) {
                    pq.push({-(abs(ps[i][0] - ps[j][0]) + abs(ps[i][1] - ps[j][1])), j});
                }
            }

            while (visited[pq.top().second]) {
                pq.pop();
            }

            res -= pq.top().first;
            i = pq.top().second;
            pq.pop();
        }

        return res;
    }
};

/**
 * Here is the solution based on the idea below.
 * Still not complete. This solution somewhat also complicated.
 * Finish it lator.
 */
class Solution {
public:
    int minCostConnectPoints(vector<vector<int>>& points) {
        vector<vector<int>> G;

        for (int i = 0; i < points.size(); ++i) {
            int min_weight = numeric_limits<int>::max();
            int closest = i;

            for (int j = 0; j < points.size(); ++j) {
                if (i != j) {
                    int weight = abs(points[i][0] - points[j][0]) + abs(points[i][1] - points[j][1]);
                    if (weight < min_weight) {
                        min_weight = weight;
                        closest = j;
                    }
                }
            }
            G.push_back({i, closest});
        }

        vector<int> ds(points.size(), -1);
        for (auto& p : G) {
            int i = parent_node(ds, p[0]);
            int j = parent_node(ds, p[1]);
            if (i != j) {
                ds[j] = i;
            }
        }

        vector<vector<int>> paths(points.size());
        for (int i = 0; i < points.size(); ++i) {
            paths[parent_node(ds, i)].push_back(i);
        }

        int ans = 0;

        return ans;
    }

private:
    int parent_node(vector<int>& ds, int node)
    {
        if (ds[node] < 0) {
            return node;
        }

        return ds[node] = parent_node(ds, ds[node]);
    }
};

/**
 * Original solution. This solution not completed, but it will work in theory.
 * But this gonna be an really slow one.
 * This just give me a idea that first we can build a connect graph with every
 * point with its closest point. Then we use this graph build paths using disjoint
 * set method. Then connect different path with min cost.
 */
class Solution {
public:
    int minCostConnectPoints(vector<vector<int>>& points) {
        vector<vector<int>> G(points.size(), vector<int>(points.size(), 0));

        for (int i = 0; i < points.size(); ++i) {
            for (int j = i + 1; j < points.size(); ++j) {
                int weight = abs(points[i][0] - points[j][0]) + abs(points[i][1] - points[j][1]);
                G[i][j] = weight;
                G[j][i] = weight;
            }
        }

        vector<bool> visited(points.size(), false);
        vector<vector<int>> paths;

        int ans = 0;
        for (int i = 0; i < points.size(); ++i) {
            if (!visited[i]) {
                visited[i] = true;
                paths.push_back(build_path(G, i, visited, ans));
            }
        }

        // the connect different path with min cost

        return ans;
    }

private:
    vector<int> build_path(const vector<vector<int>>& G, int p, vector<bool>& visited, int& ans)
    {
        vector<int> path = { p };

        int fixed = p;

        while (1) {
            int min_weight = numeric_limits<int>::max();
            int closest = fixed;

            for (int i = 0; i < G.size(); ++i) {
                if (!visited[i] && i != fixed) {
                    if (G[fixed][i] < min_weight) {
                        min_weight = G[fixed][i];
                        closest = i;
                    }
                }
            }

            if (closest == fixed) {
                break;
            }

            auto closest_r = find_closest(G, closest);
            if (closest_r != fixed) {
                break;
            }

            ans += min_weight;
            path.push_back(closest);
            visited[closest] = true;
            fixed = closest;
        }

        return path;
    }

    int find_closest(const vector<vector<int>>& G, int fixed)
    {
        int ans = 0;
        int min_weight = numeric_limits<int>::max();

        for (int i = 0; i < G.size(); ++i) {
            if (i != fixed) {
                if (G[fixed][i] < min_weight) {
                    min_weight = G[fixed][i];
                    ans = i;
                }
            }
        }

        return ans;
    }
};
