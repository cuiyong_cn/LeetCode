/**
 * Maximum Total Reward Using Operations I
 *
 * You are given an integer array rewardValues of length n, representing the values of rewards.
 *
 * Initially, your total reward x is 0, and all indices are unmarked. You are allowed to perform the
 * following operation any number of times:
 *     Choose an unmarked index i from the range [0, n - 1].
 *     If rewardValues[i] is greater than your current total reward x, then add rewardValues[i] to x
 *     (i.e., x = x + rewardValues[i]), and mark the index i.
 *
 * Return an integer denoting the maximum total reward you can collect by performing the operations optimally.
 *
 * Example 1:
 *
 * Input: rewardValues = [1,1,3,3]
 *
 * Output: 4
 *
 * Explanation:
 *
 * During the operations, we can choose to mark the indices 0 and 2 in order, and the total reward will be 4, which is the maximum.
 *
 * Example 2:
 *
 * Input: rewardValues = [1,6,4,3,2]
 *
 * Output: 11
 *
 * Explanation:
 *
 * Mark the indices 0, 2, and 1 in order. The total reward will then be 11, which is the maximum.
 *
 * Constraints:
 *     1 <= rewardValues.length <= 2000
 *     1 <= rewardValues[i] <= 2000
 */
/**
 * Original solution. Why 4000?
 *  Assume the last element we pick is x. Then the previous largest possible value be x - 1.
 *  So sum be 2*x - 1 = 2 * 2000 - 1 = 3999
 *
 * We can also use recursive method, and choose to pick or no pick.
 */
class Solution {
public:
    int maxTotalReward(vector<int>& rewardValues) {
        std::ranges::sort(rewardValues);
        auto const size = rewardValues.size();
        auto dp = vector<vector<bool>>(size + 1, vector<bool>(4000, false));

        dp[0][0] = true;

        for (auto i = 0uz; i < size; ++i) {
            for (auto j = 0; j < 4000; ++j) {
                dp[i + 1][j] = dp[i][j];

                if (!dp[i + 1][j] && j >= rewardValues[i] && (j - rewardValues[i]) < rewardValues[i]) {
                    dp[i + 1][j] = dp[i][j - rewardValues[i]];
                }
            }
        }

        auto max_pos = std::find(dp.back().rbegin(), dp.back().rend(), true);
        return distance(dp.back().begin(), max_pos.base()) - 1;
    }
};
