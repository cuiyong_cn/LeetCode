/**
 * Convert Integer to the Sum of Two No-Zero Integers
 *
 * Given an integer n. No-Zero integer is a positive integer which doesn't contain any 0 in its
 * decimal representation.
 *
 * Return a list of two integers [A, B] where:
 *
 * A and B are No-Zero integers.
 * A + B = n
 * It's guarateed that there is at least one valid solution. If there are many valid solutions you
 * can return any of them.
 *
 * Example 1:
 *
 * Input: n = 2
 * Output: [1,1]
 * Explanation: A = 1, B = 1. A + B = n and both A and B don't contain any 0 in their decimal
 * representation.
 * Example 2:
 *
 * Input: n = 11
 * Output: [2,9]
 * Example 3:
 *
 * Input: n = 10000
 * Output: [1,9999]
 * Example 4:
 *
 * Input: n = 69
 * Output: [1,68]
 * Example 5:
 *
 * Input: n = 1010
 * Output: [11,999]
 *
 * Constraints:
 *
 * 2 <= n <= 10^4
 */
/**
 * Non-brute force solution.
 */
class Solution {
public:
    vector<int> getNoZeroIntegers(int n) {
        int a = 0, b = 0, step = 1;

        while (n > 0) {
            int d = n % 10;
            n /= 10;

            if ((d == 0 || d == 1) && n > 0) {
                a += step * (1 + d);
                b += step * 9;
                n--;
            } else {
                a += step * 1;
                b += step * (d-1);
            }

            step *= 10;
        }

        return {a, b};
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<int> getNoZeroIntegers(int n) {
        for (int i = 1; i < n; ++i) {
            if (no_zero_digit(i) && no_zero_digit(n - i)) {
                return {i, n - i};
            }
        }

        return {};
    }

private:
    bool no_zero_digit(int n) {
        while (n > 0) {
            if (0 == (n % 10)) {
                return false;
            }
            n /= 10;
        }

        return true;
    }
};
