/**
 * Find All Anagrams in a String
 *
 * Given a string s and a non-empty string p, find all the start indices of p's anagrams in s.
 *
 * Strings consists of lowercase English letters only and the length of both strings s and p will not be larger than
 * 20,100.
 *
 * The order of output does not matter.
 *
 * Example 1:
 *
 * Input:
 * s: "cbaebabacd" p: "abc"
 *
 * Output:
 * [0, 6]
 *
 * Explanation:
 * The substring with start index = 0 is "cba", which is an anagram of "abc".
 * The substring with start index = 6 is "bac", which is an anagram of "abc".
 * Example 2:
 *
 * Input:
 * s: "abab" p: "ab"
 *
 * Output:
 * [0, 1, 2]
 *
 * Explanation:
 * The substring with start index = 0 is "ab", which is an anagram of "ab".
 * The substring with start index = 1 is "ba", which is an anagram of "ab".
 * The substring with start index = 2 is "ab", which is an anagram of "ab"."
 */
/**
 * Revise the below solution.
 */
class Solution {
public:
    vector<int> findAnagrams(string s, string p) {
        if (s.size() < p.size()) {
           return {};
        }

        vector<int> pv(26, 0);
        vector<int> sv(26, 0);
        vector<int> ans;
        for (int i = 0; i < p.size(); ++i) {
            ++pv[p[i]-'a'];
            ++sv[s[i]-'a'];
        }

        if (pv == sv) {
           ans.push_back(0);
        }

        const int plen = p.size();
        for (int i = p.size(); i < s.size(); ++i) {
            ++sv[s[i] - 'a'];
            --sv[s[i - plen] - 'a'];

            if (pv == sv) {
               ans.push_back(i - plen + 1);
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<int> findAnagrams(string s, string p) {
        if (p.length() > s.length()) {
            return {};
        }

        vector<int> ans;
        vector<int> trait_of_p = get_trait(p);
        vector<int> trait(26, 0);
        int len = 0;

        for (int i = 0; i < s.length(); ++i) {
            int index = s[i] - 'a';
            if (!trait_of_p[index]) {
                trait = vector<int>(26, 0);
                len = 0;
            }
            else {
                ++trait[index];
                ++len;
            }

            if (len >= p.length()) {
                if (len > p.length()) {
                    --trait[s[i + 1 - len] - 'a'];
                    --len;
                }

                if (trait == trait_of_p) {
                    ans.push_back(i + 1 - len);
                }
            }
        }

        return ans;
    }

private:
    vector<int> get_trait(const string& p)
    {
        vector<int> trait(26, 0);
        for (auto c : p) {
            ++trait[c - 'a'];
        }

        return trait;
    }
};
