/**
 * Available Captures for Rook
 *
 * On an 8 x 8 chessboard, there is one white rook.  There also may be empty squares,
 * white bishops, and black pawns.  These are given as characters 'R', '.', 'B',
 * and 'p' respectively. Uppercase characters represent white pieces, and lowercase
 * characters represent black pieces.
 *
 * The rook moves as in the rules of Chess: it chooses one of four cardinal directions
 * (north, east, west, and south), then moves in that direction until it chooses to stop,
 * reaches the edge of the board, or captures an opposite colored pawn by moving to the
 * same square it occupies.  Also, rooks cannot move into the same square as other
 * friendly bishops.
 *
 * Return the number of pawns the rook can capture in one move.
 *
 * Input:
 * [[".",".",".",".",".",".",".","."],
 *  [".",".",".","p",".",".",".","."],
 *  [".",".",".","R",".",".",".","p"],
 *  [".",".",".",".",".",".",".","."],
 *  [".",".",".",".",".",".",".","."],
 *  [".",".",".","p",".",".",".","."],
 *  [".",".",".",".",".",".",".","."],
 *  [".",".",".",".",".",".",".","."]]
 *
 * Output: 3
 * Explanation:
 * In this example the rook is able to capture all the pawns.
 *
 * Example 2:
 *
 * Input:
 * [[".",".",".",".",".",".",".","."],
 *  [".","p","p","p","p","p",".","."],
 *  [".","p","p","B","p","p",".","."],
 *  [".","p","B","R","B","p",".","."],
 *  [".","p","p","B","p","p",".","."],
 *  [".","p","p","p","p","p",".","."],
 *  [".",".",".",".",".",".",".","."],
 *  [".",".",".",".",".",".",".","."]]
 *
 * Output: 0
 * Explanation:
 * Bishops are blocking the rook to capture any pawn.
 *
 * Example 3:
 *
 * Input:
 * [[".",".",".",".",".",".",".","."],
 *  [".",".",".","p",".",".",".","."],
 *  [".",".",".","p",".",".",".","."],
 *  ["p","p",".","R",".","p","B","."],
 *  [".",".",".",".",".",".",".","."],
 *  [".",".",".","B",".",".",".","."],
 *  [".",".",".","p",".",".",".","."],
 *  [".",".",".",".",".",".",".","."]]
 *
 * Output: 3
 * Explanation:
 * The rook can capture the pawns at positions b5, d6 and f5.
 *
 * Note:
 *
 *     board.length == board[i].length == 8
 *     board[i][j] is either 'R', '.', 'B', or 'p'
 *     There is exactly one cell with board[i][j] == 'R'
 */
class Solution {
public:
    int numRookCaptures(vector<vector<char>>& board) {
        int rows = board.size();
        int cols = board[0].size();
        int Rx = 0, Ry = 0;
        int pcNum = 0;
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                if (board[i][j] == 'R') {
                    Rx = i;
                    Ry = j;
                }
            }
        }
        // East direction
        for (int i = Rx, j = Ry + 1; j < cols; ++j) {
            if (board[i][j] == 'B') break;
            if (board[i][j] == 'p') {
                ++pcNum;
                break;
            }
        }
        // South direction
        for (int i = Rx + 1, j = Ry; i < rows; ++i) {
            if (board[i][j] == 'B') break;
            if (board[i][j] == 'p') {
                ++pcNum;
                break;
            }
        }
        // East direction
        for (int i = Rx, j = Ry - 1; j >= 0; --j) {
            if (board[i][j] == 'B') break;
            if (board[i][j] == 'p') {
                ++pcNum;
                break;
            }
        }
        // North direction
        for (int i = Rx - 1, j = Ry; i >= 0; --i) {
            if (board[i][j] == 'B') break;
            if (board[i][j] == 'p') {
                ++pcNum;
                break;
            }
        }
        return pcNum;
    }
};

/**
 * Improved version
 */
class Solution {
public:
    int cap(vector<vector<char>>& b, int x, int y, int dx, int dy) {
      while (x >= 0 && x < b.size() && y >= 0 && y < b[x].size() && b[x][y] != 'B') {
        if (b[x][y] == 'p') return 1;
        x += dx, y += dy;
      }
      return 0;
    }

    int numRookCaptures(vector<vector<char>>& b) {
      for (auto i = 0; i < b.size(); ++i)
        for (auto j = 0; j < b[i].size(); ++j)
          if (b[i][j] == 'R') return cap(b,i,j,0,1)+cap(b,i,j,0,-1)+cap(b,i,j,1,0)+cap(b,i,j,-1,0);
      return 0;
    }
};
