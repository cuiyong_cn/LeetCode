/**
 * Group Anagrams
 *
 * Given an array of strings, group anagrams together.
 *
 * Example:
 *
 * Input: ["eat", "tea", "tan", "ate", "nat", "bat"],
 * Output:
 * [
 *   ["ate","eat","tea"],
 *   ["nat","tan"],
 *   ["bat"]
 * ]
 *
 * Note:
 *
 *     All inputs will be in lowercase.
 *     The order of your output does not matter.
 */
/**
 * Original solution. Also we can solve like this.
 *  Convert each word into a number count string
 *
 *      #x#x#x#x....  26# in total for 'a' to 'z'
 */
class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        unordered_map<string, vector<string>> anagrams;
        for (auto& s : strs) {
            string word = s;
            sort(word.begin(), word.end());
            anagrams[word].push_back(s);
        }

        vector<vector<string>> ans;
        for (auto& vs : anagrams) ans.push_back(vs.second);

        return ans;
    }
};
