/**
 * All Elements in Two Binary Search Trees
 *
 * Given two binary search trees root1 and root2.
 *
 * Return a list containing all the integers from both trees sorted in ascending order.
 *
 * Example 1:
 *
 * Input: root1 = [2,1,4], root2 = [1,0,3]
 * Output: [0,1,1,2,3,4]
 * Example 2:
 *
 * Input: root1 = [0,-10,10], root2 = [5,1,7,0,2]
 * Output: [-10,0,0,1,2,5,7,10]
 * Example 3:
 *
 * Input: root1 = [], root2 = [5,1,7,0,2]
 * Output: [0,1,2,5,7]
 * Example 4:
 *
 * Input: root1 = [0,-10,10], root2 = []
 * Output: [-10,0,10]
 * Example 5:
 *
 * Input: root1 = [1,null,8], root2 = [8,1]
 * Output: [1,1,8,8]
 *
 * Constraints:
 *
 * Each tree has at most 5000 nodes.
 * Each node's value is between [-10^5, 10^5].
 */
/**
 * Test with async, result is not very satisfying.
 */
class Solution {
public:
    vector<int> getAllElements(TreeNode* root1, TreeNode* root2) {
        vector<int> c2;
        future<vector<int>> result{async(
            [&]() {
                vector<int> ans;
                inorder_collect(root1, ans);
                return ans;
            }
        )};

        inorder_collect(root2, c2);
        auto c1 = move(result.get());

        vector<int> ans(c1.size() + c2.size(), 0);
        merge(c1.begin(), c1.end(), c2.begin(), c2.end(), ans.begin());

        return ans;
    }

private:
    void inorder_collect(TreeNode* node, vector<int>& collection)
    {
        if (nullptr == node) {
            return;
        }

        inorder_collect(node->left, collection);
        collection.push_back(node->val);
        inorder_collect(node->right, collection);
    }
};

/**
 * DFS inorder traversal each three, then merge.
 */
class Solution {
public:
    vector<int> getAllElements(TreeNode* root1, TreeNode* root2) {
        vector<int> c1, c2;
        inorder_collect(root1, c1);
        inorder_collect(root2, c2);

        vector<int> ans(c1.size() + c2.size(), 0);
        merge(c1.begin(), c1.end(), c2.begin(), c2.end(), ans.begin());

        return ans;
    }

private:
    void inorder_collect(TreeNode* node, vector<int>& collection)
    {
        if (nullptr == node) {
            return;
        }

        inorder_collect(node->left, collection);
        collection.push_back(node->val);
        inorder_collect(node->right, collection);
    }
};

/**
 * Make below code shorter.
 */
class Solution {
public:
    void pushLeft(stack<TreeNode*> &s, TreeNode* n) {
        while (nullptr != n) {
            s.push(exchange(n, n->left));
        }
    }

    vector<int> getAllElements(TreeNode* root1, TreeNode* root2) {
        vector<int> res;
        stack<TreeNode*> s1, s2;
        pushLeft(s1, root1);
        pushLeft(s2, root2);

        while (!s1.empty() || !s2.empty()) {
            auto& s = s1.empty() ? s2 :
                      s2.empty() ? s1 :
                      s1.top()->val < s2.top()->val ? s1 : s2;
            auto n = s.top(); s.pop();
            res.push_back(n->val);
            pushLeft(s, n->right);
        }

        return res;
    }
};

/**
 * inorder  traversal the binary search tree give us a sorted value. And we merge these two
 * traversal.
 */
class Solution {
public:
    vector<int> getAllElements(TreeNode* root1, TreeNode* root2) {
        vector<int> ans;
        stack<TreeNode*> t1;
        stack<TreeNode*> t2;

        if (root1) {
            t1.push(root1);
        }
        if (root2) {
            t2.push(root2);
        }

        while (!t1.empty() || !t2.empty()) {
            if (!t1.empty()) {
                while (t1.top()->left && 0 == seen.count(t1.top()->left)) {
                    t1.push(t1.top()->left);
                }
            }

            if (!t2.empty()) {
                while (t2.top()->left && 0 == seen.count(t2.top()->left)) {
                    t2.push(t2.top()->left);
                }
            }

            if (!t1.empty() && !t2.empty()) {
                if (t1.top()->val < t2.top()->val) {
                    push_right_or_pop_top(t1, ans);
                }
                else {
                    push_right_or_pop_top(t2, ans);
                }
            }
            else if (!t1.empty()) {
                push_right_or_pop_top(t1, ans);
            }
            else {
                push_right_or_pop_top(t2, ans);
            }
        }

        return ans;
    }

private:
    void push_right_or_pop_top(stack<TreeNode*>& stk, vector<int>& collection)
    {
        collection.push_back(stk.top()->val);
        seen.insert(stk.top());
        if (stk.top()->right) {
            exchange(stk.top(), stk.top()->right);
        }
        else {
            stk.pop();
        }
    }

    unordered_set<TreeNode*> seen;
};

/**
 * Original solution. Collect all values, then sort.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> getAllElements(TreeNode* root1, TreeNode* root2) {
        vector<int> ans;
        collect(root1, ans);
        collect(root2, ans);

        sort(ans.begin(), ans.end());
        return ans;
    }

private:
    void collect(TreeNode* node, vector<int>& collection)
    {
        if (nullptr == node) {
            return;
        }

        collection.push_back(node->val);
        collect(node->left, collection);
        collect(node->right, collection);
    }
};
