/**
 * Print Words Vertically
 * Given a string s. Return all the words vertically in the same order in which they appear in s.
 * Words are returned as a list of strings, complete with spaces when is necessary. (Trailing spaces
 * are not allowed).
 * Each word would be put on only one column and that in one column there will be only one word.
 *
 * Example 1:
 *
 * Input: s = "HOW ARE YOU"
 * Output: ["HAY","ORO","WEU"]
 * Explanation: Each word is printed vertically.
 *  "HAY"
 *  "ORO"
 *  "WEU"
 * Example 2:
 *
 * Input: s = "TO BE OR NOT TO BE"
 * Output: ["TBONTB","OEROOE","   T"]
 * Explanation: Trailing spaces is not allowed.
 * "TBONTB"
 * "OEROOE"
 * "   T"
 * Example 3:
 *
 * Input: s = "CONTEST IS COMING"
 * Output: ["CIC","OSO","N M","T I","E N","S G","T"]
 *
 * Constraints:
 *
 * 1 <= s.length <= 200
 * s contains only upper case English letters.
 * It's guaranteed that there is only one space between 2 words.
 */
/***
 * Original solution.
 */
class Solution {
public:
    vector<string> printVertically(string s) {
        vector<string> words;
        stringstream ss{s};
        size_t max_len = 0;
        while (ss) {
            string w; ss >> w;
            max_len = max(max_len, w.length());
            words.emplace_back(move(w));
        }

        vector<string> ans(max_len);
        for (auto const& w : words) {
            int i = 0;
            for (auto c : w) {
                ans[i++] += c;
            }
            while (i < max_len) {
                ans[i++] += ' ';
            }
        }

        for (auto& w : ans) {
            while (' ' == w.back()) {
                w.pop_back();
            }
        }

        return ans;
    }
};
