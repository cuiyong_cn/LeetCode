/**
 * Largest Palindromic Number
 *
 * You are given a string num consisting of digits only.
 *
 * Return the largest palindromic integer (in the form of a string) that can be formed using digits
 * taken from num. It should not contain leading zeroes.
 *
 * Notes:
 *
 * You do not need to use all the digits of num, but you must use at least one digit.
 * The digits can be reordered.
 *
 * Example 1:
 *
 * Input: num = "444947137"
 * Output: "7449447"
 * Explanation:
 * Use the digits "4449477" from "444947137" to form the palindromic integer "7449447".
 * It can be shown that "7449447" is the largest palindromic integer that can be formed.
 * Example 2:
 *
 * Input: num = "00009"
 * Output: "9"
 * Explanation:
 * It can be shown that "9" is the largest palindromic integer that can be formed.
 * Note that the integer returned should not contain leading zeroes.
 *
 * Constraints:
 *
 * 1 <= num.length <= 105
 * num consists of digits.
 */
/**
 * Optimize.
 */
string to_extra(int n)
{
    return n >= 0 ? string(1, '0' + n) : string("");
}

string reverse(string str)
{
    reverse(begin(str), end(str));
    return str;
}

class Solution {
public:
    string largestPalindromic(string num) {
        auto digit_cnt = array<int, 10>{ 0, };
        for (auto n : num) {
            ++digit_cnt[n - '0'];
        }

        auto ans = string{};
        auto extra = -1;
        for (auto i = 9; i >= 0; --i) {
            auto dc = 0 == i && ans.empty() ? 0 : digit_cnt[i] / 2;
            if (dc > 0) {
                ans += string(dc, i + '0');
            }

            if (-1 == extra && 1 == (digit_cnt[i] % 2)) {
                extra = i;
            }
        }

        if (ans.empty() && -1 == extra) {
            return "0";
        }

        return ans + to_extra(extra) + reverse(ans);
    }
};

/**
 * Original solution.
 */
string remove_leading_zero(string const& str)
{
    auto non_zero = str.find_first_not_of('0');
    return string::npos == non_zero ? string{} : str.substr(non_zero);
}

string to_extra(int n)
{
    return n >= 0 ? string(1, '0' + n) : string("");
}

string reverse(string str)
{
    reverse(begin(str), end(str));
    return str;
}

class Solution {
public:
    string largestPalindromic(string num) {
        auto digit_cnt = array<int, 10>{ 0, };
        for (auto n : num) {
            ++digit_cnt[n - '0'];
        }

        auto ans = string{};
        auto extra = -1;
        for (auto i = 9; i >= 0; --i) {
            if (digit_cnt[i] > 1) {
                ans += string(digit_cnt[i] / 2, i + '0');
            }

            if (-1 == extra && 1 == (digit_cnt[i] % 2)) {
                extra = i;
            }
        }

        ans = remove_leading_zero(ans);
        if (ans.empty() && -1 == extra) {
            return "0";
        }

        return ans + to_extra(extra) + reverse(ans);
    }
};
