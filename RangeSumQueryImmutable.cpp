/**
 * Range Sum Query - Immutable
 *
 * Given an integer array nums, find the sum of the elements between indices i and j (i ≤ j), inclusive.
 *
 * Implement the NumArray class:
 *
 * NumArray(int[] nums) Initializes the object with the integer array nums.  int sumRange(int i, int j) Return the sum
 * of the elements of the nums array in the range [i, j] inclusive (i.e., sum(nums[i], nums[i + 1], ... , nums[j]))
 *
 * Example 1:
 *
 * Input
 * ["NumArray", "sumRange", "sumRange", "sumRange"]
 * [[[-2, 0, 3, -5, 2, -1]], [0, 2], [2, 5], [0, 5]]
 * Output
 * [null, 1, -1, -3]
 *
 * Explanation
 * NumArray numArray = new NumArray([-2, 0, 3, -5, 2, -1]);
 * numArray.sumRange(0, 2); // return 1 ((-2) + 0 + 3)
 * numArray.sumRange(2, 5); // return -1 (3 + (-5) + 2 + (-1))
 * numArray.sumRange(0, 5); // return -3 ((-2) + 0 + 3 + (-5) + 2 + (-1))
 *
 * Constraints:
 *
 * 0 <= nums.length <= 104
 * -105 <= nums[i] <= 105
 * 0 <= i <= j < nums.length
 * At most 104 calls will be made to sumRange.
 */
/**
 * Do not copy.
 */
class NumArray {
public:
    NumArray(vector<int>& nums) : nums_ref(nums) {
        std::partial_sum(std::begin(nums_ref.get()), std::end(nums_ref.get()),
                         std::begin(nums_ref.get()));
    }

    int sumRange(int i, int j) {
        return i > 0 ? nums_ref.get()[j] - nums_ref.get()[i - 1] : nums_ref.get()[j];
    }

private:
    std::reference_wrapper<vector<int>> nums_ref;
};

/**
 * Original solution
 */
class NumArray {
public:
    NumArray(vector<int>& nums) : copy(nums) {
        std::partial_sum(std::begin(copy), std::end(copy), std::begin(copy));
    }

    int sumRange(int i, int j) {
        return i > 0 ? copy[j] - copy[i - 1] : copy[j];
    }

private:
    vector<int> copy;
};

/**
 * Your NumArray object will be instantiated and called as such:
 * NumArray* obj = new NumArray(nums);
 * int param_1 = obj->sumRange(i,j);
 */
