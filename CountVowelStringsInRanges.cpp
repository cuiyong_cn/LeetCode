/**
 * Count Vowel Strings in Ranges
 *
 * You are given a 0-indexed array of strings words and a 2D array of integers queries.
 *
 * Each query queries[i] = [li, ri] asks us to find the number of strings
 * present in the range li to ri (both inclusive) of words that start and end
 * with a vowel.
 *
 * Return an array ans of size queries.length, where ans[i] is the answer to the ith query.
 *
 * Note that the vowel letters are 'a', 'e', 'i', 'o', and 'u'.
 *
 * Example 1:
 *
 * Input: words = ["aba","bcb","ece","aa","e"], queries = [[0,2],[1,4],[1,1]]
 * Output: [2,3,0]
 * Explanation: The strings starting and ending with a vowel are "aba", "ece", "aa" and "e".
 * The answer to the query [0,2] is 2 (strings "aba" and "ece").
 * to query [1,4] is 3 (strings "ece", "aa", "e").
 * to query [1,1] is 0.
 * We return [2,3,0].
 * Example 2:
 *
 * Input: words = ["a","e","i"], queries = [[0,2],[0,1],[2,2]]
 * Output: [3,2,1]
 * Explanation: Every string satisfies the conditions, so we return [3,2,1].
 *
 * Constraints:
 *
 * 1 <= words.length <= 105
 * 1 <= words[i].length <= 40
 * words[i] consists only of lowercase English letters.
 * sum(words[i].length) <= 3 * 105
 * 1 <= queries.length <= 105
 * 0 <= li <= ri < words.length
 */
/**
 * Original solution.
 */
bool is_vowel(char c)
{
    return 'a' == c || 'e' == c || 'i' == c || 'o' == c || 'u' == c;
}

bool start_end_with_vowel(string const& s)
{
    return is_vowel(s.front()) && is_vowel(s.back());
}

class Solution {
public:
    vector<int> vowelStrings(vector<string>& words, vector<vector<int>>& queries) {
        auto const N = words.size();
        auto prefix = vector<int>(N + 1, 0);
        for (auto i = 0 * N; i < N; ++i) {
            prefix[i + 1] = prefix[i];
            prefix[i + 1] += start_end_with_vowel(words[i]);
        }

        auto const QN = queries.size();
        auto ans = vector<int>(QN, 0);
        for (auto i = 0 * QN; i < QN; ++i) {
            auto left = queries[i][0];
            auto right = queries[i][1];
            ans[i] = prefix[right + 1] - prefix[left];
        }
        return ans;
    }
};
