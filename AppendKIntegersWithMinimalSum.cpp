/**
 * Append K Integers With Minimal Sum
 *
 * You are given an integer array nums and an integer k. Append k unique positive integers that do
 * not appear in nums to nums such that the resulting total sum is minimum.
 *
 * Return the sum of the k integers appended to nums.
 *
 * Example 1:
 *
 * Input: nums = [1,4,25,10,25], k = 2
 * Output: 5
 * Explanation: The two unique positive integers that do not appear in nums which we append are 2 and 3.
 * The resulting sum of nums is 1 + 4 + 25 + 10 + 25 + 2 + 3 = 70, which is the minimum.
 * The sum of the two integers appended is 2 + 3 = 5, so we return 5.
 * Example 2:
 *
 * Input: nums = [5,6], k = 6
 * Output: 25
 * Explanation: The six unique positive integers that do not appear in nums which we append are 1,
 * 2, 3, 4, 7, and 8.
 * The resulting sum of nums is 5 + 6 + 1 + 2 + 3 + 4 + 7 + 8 = 36, which is the minimum.
 * The sum of the six integers appended is 1 + 2 + 3 + 4 + 7 + 8 = 25, so we return 25.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 105
 * 1 <= nums[i] <= 109
 * 1 <= k <= 108
 */
/**
 * Make it more efficient by skipping.
 */
class Solution {
public:
    long long minimalKSum(vector<int>& nums, int k) {
        sort(begin(nums), end(nums));

        auto ans = 1LL * k * (k + 1) / 2;
        auto last_num = 1LL * k;
        for (auto [i, nsize] = tuple(0, static_cast<int>(nums.size()));
                i < nsize && nums[i] <= last_num; ++i) {
            if (i > 0 && nums[i - 1] == nums[i]) {
                continue;
            }

            ++last_num;
            ans += -nums[i] + last_num;
        }

        return ans;
    }
};

/**
 * Original solution. Sort and compare. TLE.
 */
class Solution {
public:
    long long minimalKSum(vector<int>& nums, int k) {
        sort(begin(nums), end(nums));
        auto insert_num = 1;
        auto ans = 0LL;
        auto i = 0;
        auto const nsize = static_cast<int>(nums.size());

        while (i < nsize && k > 0) {
            if (i > 0 && nums[i - 1] == nums[i]) {
                ++i;
                continue;
            }

            if (insert_num != nums[i]) {
                ans += insert_num;
                --k;
            }
            else {
                ++i;
            }
            ++insert_num;
        }

        while (k > 0) {
            ans += insert_num;
            --k;
            ++insert_num;
        }

        return ans;
    }
};
