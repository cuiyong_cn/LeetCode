/**
 * Unique Substrings in Wraparound String
 *
 * We define the string s to be the infinite wraparound string of "abcdefghijklmnopqrstuvwxyz", so s
 * will look like this:
 *
 * "...zabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcd....".
 * Given a string p, return the number of unique non-empty substrings of p are present in s.
 *
 * Example 1:
 *
 * Input: p = "a"
 * Output: 1
 * Explanation: Only the substring "a" of p is in s.
 * Example 2:
 *
 * Input: p = "cac"
 * Output: 2
 * Explanation: There are two substrings ("a", "c") of p in s.
 * Example 3:
 *
 * Input: p = "zab"
 * Output: 6
 * Explanation: There are six substrings ("z", "a", "b", "za", "ab", and "zab") of p in s.
 *
 * Constraints:
 *
 * 1 <= p.length <= 105
 * p consists of lowercase English letters.
 */
/**
 * Original solution. We record the longest continous char sequence end with 'a'..'z'.
 * And the answer is the sum.
 */
class Solution {
public:
    int findSubstringInWraproundString(string p) {
        array<int, 26> cnt_end_with = { 0, };

        int con_len = 1;
        cnt_end_with[p.front() - 'a'] = 1;

        for (size_t i = 1; i < p.length(); ++i) {
            if ((1 == (p[i] - p[i - 1]))
                || (25 == (p[i - 1] - p[i]))
               ) {
                ++con_len;
            }
            else {
                con_len = 1;
            }

            int idx = p[i] - 'a';
            cnt_end_with[idx] = max(cnt_end_with[idx], con_len);
        }

        return accumulate(cnt_end_with.begin(), cnt_end_with.end(), 0);
    }
};
