/** Number of Substrings Containing All Three Characters
 *
 * Given a string s consisting only of characters a, b and c.
 *
 * Return the number of substrings containing at least one occurrence of all these characters a, b and c.
 *
 * Example 1:
 *
 * Input: s = "abcabc" Output: 10 Explanation: The substrings containing at least one occurrence of the characters a, b
 * and c are "abc", "abca", "abcab", "abcabc", "bca", "bcab", "bcabc", "cab", "cabc" and "abc" (again).
 *
 * Example 2:
 *
 * Input: s = "aaacb" Output: 3 Explanation: The substrings containing at least one occurrence of the characters a, b
 * and c are "aaacb", "aacb" and "acb".
 *
 * Example 3:
 *
 * Input: s = "abc" Output: 1
 *
 * Constraints:
 *
 *     3 <= s.length <= 5 x 10^4 s only consists of a, b or c characters.
 */
/**
 * My original solution seems like the variant of the sliding window solution.
 * Here is the sliding window solution.
 */
class Solution {
public:
    int numberOfSubstrings(string s) {
        int count[3] = {0, 0, 0}, res = 0 , i = 0, n = s.length();
        for (int j = 0; j < n; ++j) {
            ++count[s[j] - 'a'];
            while (count[0] && count[1] && count[2])
                --count[s[i++] - 'a'];
            res += i;
        }
        return res;
    }
};

/**
 * origianl solution. O(n) complexity.
 */
class Solution {
public:
    int numberOfSubstrings(string s) {
        vector<int> abcIndex(3, -1);
        char lc = s[0], mc, rc;
        int count = 1;
        for (int i = 0; i < s.length(); ++i) {
            switch (count) {
            case 1:
                if (s[i] != lc) {
                    mc = s[i];
                    ++count;
                }
                break;

                case 2:
                if (s[i] == lc) {
                    lc = mc;
                    mc = s[i];
                }
                else if (s[i] != mc) {
                    rc = s[i];
                    ++count;
                }
                break;

                default:
                    break;
            }
            abcIndex[s[i] - 'a'] = i;
            if (3 == count) break;
        }

        int abcCount;
        int ans = 0;

        if (count == 3) {
            abcCount = 1;
            ans += (abcIndex[lc - 'a'] + 1) * (s.length() - abcIndex[rc - 'a']);

            for (int i = abcIndex[rc - 'a'] + 1; i < s.length(); ++i) {
                if (s[i] == lc) {
                    lc = mc; mc = rc; rc = s[i];
                    ans += (abcIndex[lc - 'a'] - abcIndex[s[i] - 'a']) * (s.length() - i);
                    ++abcCount;
                }
                else if (s[i] == mc) {
                    mc = rc; rc = s[i];
                }
                abcIndex[s[i] - 'a'] = i;
            }
        }

        return ans;
    }
};

/**
 * same sliding window solution with different perspective.
 */
class Solution
{
public:
    int numberOfSubstrings(string s) {
        int last[3] = {-1, -1, -1}, res = 0, n = s.length();
        for (int i = 0; i < n; ++i) {
            last[s[i] - 'a'] = i;
            res += 1 + min({last[0], last[1], last[2]});
        }
        return res;
    }
};
