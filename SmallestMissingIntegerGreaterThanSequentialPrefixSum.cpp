/**
 * Smallest Missing Integer Greater Than Sequential Prefix Sum
 *
 * You are given a 0-indexed array of integers nums.
 *
 * A prefix nums[0..i] is sequential if, for all 1 <= j <= i, nums[j] = nums[j - 1] + 1. In
 * particular, the prefix consisting only of nums[0] is sequential.
 *
 * Return the smallest integer x missing from nums such that x is greater than or equal to the sum
 * of the longest sequential prefix.
 *
 * Example 1:
 *
 * Input: nums = [1,2,3,2,5]
 * Output: 6
 * Explanation: The longest sequential prefix of nums is [1,2,3] with a sum of 6. 6 is not in the array, therefore 6 is the smallest missing integer greater than or equal to the sum of the longest sequential prefix.
 *
 * Example 2:
 *
 * Input: nums = [3,4,5,1,12,14,13]
 * Output: 15
 * Explanation: The longest sequential prefix of nums is [3,4,5] with a sum of 12. 12, 13, and 14 belong to the array while 15 does not. Therefore 15 is the smallest missing integer greater than or equal to the sum of the longest sequential prefix.
 *
 * Constraints:
 *     1 <= nums.length <= 50
 *     1 <= nums[i] <= 50
 */
/**
 * Original solution.
 */
auto index_of_longest_sequential_prefix(vector<int>& nums)
{
    int const n = nums.size();

    for (auto i = 1; i < n; ++i) {
        if ((nums[i] - nums[i - 1]) != 1) {
            return i;
        }
    }

    return n;
}

class Solution {
public:
    int missingInteger(vector<int>& nums) {
        auto idx = index_of_longest_sequential_prefix(nums);
        auto ans = accumulate(nums.begin(), nums.begin() + idx, 0);
        auto num_set = unordered_set<int>{nums.begin() + idx - 1, nums.end()};

        while (num_set.count(ans)) {
            ++ans;
        }

        return ans;
    }
};
