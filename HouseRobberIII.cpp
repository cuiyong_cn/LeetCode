/**
 * House Robber III
 *
 * The thief has found himself a new place for his thievery again. There is only one entrance to this area, called the
 * "root." Besides the root, each house has one and only one parent house. After a tour, the smart thief realized that
 * "all houses in this place forms a binary tree". It will automatically contact the police if two directly-linked
 * houses were broken into on the same night.
 *
 * Determine the maximum amount of money the thief can rob tonight without alerting the police.
 *
 * Example 1:
 *
 * Input: [3,2,3,null,3,null,1]
 *
 *      3
 *     / \
 *    2   3
 *     \   \
 *      3   1
 *
 * Output: 7
 * Explanation: Maximum amount of money the thief can rob = 3 + 3 + 1 = 7.
 *
 * Example 2:
 *
 * Input: [3,4,5,1,3,null,1]
 *
 *      3
 *     / \
 *    4   5
 *   / \   \
 *  1   3   1
 *
 * Output: 9
 * Explanation: Maximum amount of money the thief can rob = 4 + 5 = 9.
 */
/**
 * Emm..., this solution beats mine. The idea is:
 *
 * So basically, every node will pass a list to its parent like this -
 * [
 *   max amount I can give you,
 *   max amount my left child can give you,
 *   max amount my right child can give you
 * ].
 * Now its parent has to decide which of the two ways will give him the max amount:

 * take itself and its grandchildrens don't add its own amount and add both its left and right children
 * Whatever the maximum it will get, it will return back to its parent along with telling the same about its child in a
 * list[1] an list[2] and list[0] will have its own max answer.
 */
class Solution
{
public:
    vector<int> helper(TreeNode* root)
    {
        if (!root) return vector<int> {0, 0, 0};
        vector<int> l = helper(root->left);
        vector<int> r = helper(root->right);
        return vector<int> {max(root->val + l[1] + l[2] + r[1] + r[2], l[0] + r[0]), l[0], r[0]};
    }
    int rob(TreeNode* root)
    {
        return helper(root)[0];
    }
};

/**
 * Improve the blow solution using memo. Passed, but not ideal
 */
class Solution
{
public:
    int rob(TreeNode* root)
    {
        return dfs(root, 1, true);
    }

private:
    int dfs(TreeNode* node, long long index, bool canRob)
    {
        string key = to_string(index) + "#" + (canRob ? "T" : "F");

        auto it = memo.find(key);
        if (it != memo.end()) return it->second;

        int ans = 0;
        if (node) {
            if (canRob) {
                int m1 = node->val
                         + dfs(node->left, index * 2, false)
                         + dfs(node->right, index * 2 + 1, false);
                ans = max(m1, dfs(node->left, index * 2, true) + dfs(node->right, index * 2 + 1, true));
            } else {
                ans = dfs(node->left, index * 2, true) + dfs(node->right, index * 2 + 1, true);
            }
        }

        memo[key] = ans;

        return ans;
    }
private:
    unordered_map<string, int> memo;
};

/**
 * Original solution. Sadly, TLE again.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution
{
public:
    int rob(TreeNode* root)
    {
        return dfs(root, true);
    }

private:
    int dfs(TreeNode* node, bool canRob)
    {
        int ans = 0;
        if (node) {
            if (canRob) {
                int m1 = node->val + dfs(node->left, false) + dfs(node->right, false);
                ans = max(m1, dfs(node->left, true) + dfs(node->right, true));
            } else {
                ans = dfs(node->left, true) + dfs(node->right, true);
            }
        }

        return ans;
    }
};

/**
 * The last one's idea is same as mine, using some memo. But this idea also better than mine.
 */
class Solution
{
public:
    map<pair<TreeNode*, int>, int> m;

    int rob(TreeNode* root)
    {
        return max(helper(root, 0), helper(root, 1));
    }

    int helper(TreeNode* root, int flag)
    {
        if (!root) return 0;

        if (m.find({root, flag}) != m.end()) return m[ {root, flag}];

        if (flag == 1) {
            m[ {root, flag}] = max(
                                   root->val + helper(root->left, 0) + helper(root->right, 0),
                                   helper(root->left, 1) + helper(root->right, 1)
                                  );
        }
        else {
            m[ {root, flag}] = helper(root->left, 1) + helper(root->right, 1);
        }

        return m[ {root, flag}];
    }

};

