/**
 * The kth Factor of n
 *
 * You are given two positive integers n and k. A factor of an integer n is defined as an integer i
 * where n % i == 0.
 *
 * Consider a list of all factors of n sorted in ascending order, return the kth factor in this list
 * or return -1 if n has less than k factors.
 *
 * Example 1:
 *
 * Input: n = 12, k = 3
 * Output: 3
 * Explanation: Factors list is [1, 2, 3, 4, 6, 12], the 3rd factor is 3.
 * Example 2:
 *
 * Input: n = 7, k = 2
 * Output: 7
 * Explanation: Factors list is [1, 7], the 2nd factor is 7.
 * Example 3:
 *
 * Input: n = 4, k = 4
 * Output: -1
 * Explanation: Factors list is [1, 2, 4], there is only 3 factors. We should return -1.
 *
 * Constraints:
 *
 * 1 <= k <= n <= 1000
 */
/**
 * Optimize again.
 */
class Solution {
public:
    int kthFactor(int n, int k) {
        int d = 1;
        for (; d * d < n; ++d) {
            if (n % d == 0 && --k == 0) {
                return d;
            }
        }

        if ((d * d) == n && --k == 0) {
            return d;
        }

        for (--d; d >= 1; --d) {
            if (n % d == 0 && --k == 0) {
                return n / d;
            }
        }

        return -1;
    }
};

/**
 * Optimize.
 */
class Solution {
public:
    int kthFactor(int n, int k) {
        for (int d = 1; d <= n / 2; ++d) {
            if (n % d == 0 && --k == 0) {
                return d;
            }
        }

        return k == 1 ? n : -1;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int kthFactor(int n, int k) {
        vector<int> factors;
        for (int i = 1; i <= n; ++i) {
            if (0 == (n % i)) {
                factors.push_back(i);
            }
        }

        return factors.size() < k ? -1 : factors[k - 1];
    }
};
