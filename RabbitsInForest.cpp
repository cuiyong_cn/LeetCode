/**
 * Rabbits in Forest
 *
 * In a forest, each rabbit has some color. Some subset of rabbits (possibly all of them) tell you how many other
 * rabbits have the same color as them. Those answers are placed in an array.
 *
 * Return the minimum number of rabbits that could be in the forest.
 *
 * Examples:
 * Input: answers = [1, 1, 2]
 * Output: 5
 * Explanation:
 * The two rabbits that answered "1" could both be the same color, say red.
 * The rabbit than answered "2" can't be red or the answers would be inconsistent.
 * Say the rabbit that answered "2" was blue.
 * Then there should be 2 other blue rabbits in the forest that didn't answer into the array.
 * The smallest possible number of rabbits in the forest is therefore 5: 3 that answered plus 2 that didn't.
 *
 * Input: answers = [10, 10, 10]
 * Output: 11
 *
 * Input: answers = []
 * Output: 0
 * Note:
 *
 * answers will have length at most 1000.
 * Each answers[i] will be an integer in the range [0, 999].
 */
/**
 * Improved version.
 */
class Solution {
public:
    unordered_map<int, int> rs;

    int numRabbits(vector<int>& answers) {
        for (auto&n : answers) {
            ++rs[n];
        }

        int ans = 0;
        for (auto& m : rs) {
            int mr = m.first + 1;
            int count = m.second;
            int groupNum = ceil(count * 1.0 / mr);
            ans += groupNum * mr;
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int numRabbits(vector<int>& answers) {
        if (answers.size() == 0) return 0;
        if (answers.size() == 1) return answers[0] + 1;

        sort(answers.begin(), answers.end());

        int ans = 0;
        int i = 0;
        while (i < answers.size()) {
            int count = answers[i] + 1;
            ans += count;
            for (int k = 1; k < count; ++k) {
                if ((i + 1) < answers.size()) {
                    if (answers[i] == answers[i + 1]) {
                        ++i;
                    }
                }
            }
            ++i;
        }

        return ans;
    }
};
