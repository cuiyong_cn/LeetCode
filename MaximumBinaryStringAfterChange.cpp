/**
 * Maximum Binary String After Change
 *
 * You are given a binary string binary consisting of only 0's or 1's. You can apply each of the
 * following operations any number of times:
 *
 * Operation 1: If the number contains the substring "00", you can replace it with "10".  For
 * example, "00010" -> "10010" Operation 2: If the number contains the substring "10", you can
 * replace it with "01".  For example, "00010" -> "00001" Return the maximum binary string you can
 * obtain after any number of operations. Binary string x is greater than binary string y if x's
 * decimal representation is greater than y's decimal representation.
 *
 * Example 1:
 *
 * Input: binary = "000110"
 * Output: "111011"
 * Explanation: A valid transformation sequence can be:
 * "000110" -> "000101"
 * "000101" -> "100101"
 * "100101" -> "110101"
 * "110101" -> "110011"
 * "110011" -> "111011"
 * Example 2:
 *
 * Input: binary = "01"
 * Output: "01"
 * Explanation: "01" cannot be transformed any further.
 *
 * Constraints:
 *
 * 1 <= binary.length <= 105
 * binary consist of '0' and '1'.
 */
/**
 * Simplify.
 */
class Solution {
public:
    string maximumBinaryString(string binary) {
        auto zero_cnt = count(binary.begin(), binary.end(), '0');
        if (zero_cnt > 0) {
            auto i = binary.find_first_of('0');
            fill(binary.begin(), binary.end(), '1');

            binary[zero_cnt + i - 1] = '0';
        }

        return binary;
    }
};

/**
 * One pass solution.
 */
class Solution {
public:
    string maximumBinaryString(string binary) {
        auto z1 = binary.find_first_of('0');
        if (string::npos != z1) {
            auto z2 = binary.find_first_of('0', z1 + 1);
            while (string::npos != z2) {
                binary[z1] = '1';
                binary[z1 + 1] = '0';
                if ((z2 - z1) > 1) {
                    binary[z2] = '1';
                }
                ++z1;
                z2 = binary.find_first_of('0', z2 + 1);
            }
        }

        return binary;
    }
};

/**
 * Original solution. TLE.
 */
class Solution {
public:
    string maximumBinaryString(string binary) {
        while (true) {
            auto z1 = binary.find_first_of('0');
            auto o1 = binary.find_first_of('1', z1);
            o1 = string::npos == o1 ? binary.size() - 1 : o1 - 1;
            if (o1 == z1) {
                auto z2 = binary.find_first_of('0', z1 + 1);
                if (string::npos == z2) {
                    break;
                }

                binary[z1] = '1';
                binary[z1 + 1] = '0';
                for (auto i = z1 + 2; i <= z2; ++i) {
                    binary[i] = '1';
                }
            }

            for (auto i = z1; i < o1; ++i) {
                binary[i] = '1';
            }
        }

        return binary;
    }
};
