/**
 * Smallest Integer Divisible by K
 *
 * Given a positive integer K, you need to find the length of the smallest positive integer N such
 * that N is divisible by K, and N only contains the digit 1.
 *
 * Return the length of N. If there is no such N, return -1.
 *
 * Note: N may not fit in a 64-bit signed integer.
 *
 * Example 1:
 *
 * Input: K = 1
 * Output: 1
 * Explanation: The smallest answer is N = 1, which has length 1.
 * Example 2:
 *
 * Input: K = 2
 * Output: -1
 * Explanation: There is no such positive integer N divisible by 2.
 * Example 3:
 *
 * Input: K = 3
 * Output: 3
 * Explanation: The smallest answer is N = 111, which has length 3.
 *
 * Constraints:
 *
 * 1 <= K <= 105
 */
/**
 * After reading other solutions. My original one sucks. But it can be transformed to
 * the short and clever one.
 *
 * Assume N = K * x + R, then N * 10 + 1 = K * x * 10 + R * 10 + 1
 * (N * 10 + 1) % K = (K * x * 10 + R * 10 + 1) % K = (R * 10 + 1) % K
 */
class Solution {
public:
    int smallestRepunitDivByK(int K) {
        auto re = K % 10;
        if (re != 1 && re != 3 && re != 7 && re != 9) {
            return -1;
        }

        re = 0;
        for (auto len = 1; len <= K; ++len) {
            re = (re * 10 + 1) % K;
            if (0 == re) {
                return len;
            }
        }

        return -1;
    }
};

/**
 * Original solution. Looks stupid, but works and fast.
 */
class Solution {
public:
    int smallestRepunitDivByK(int K) {
        auto re = K % 10;
        if (re != 1 && re != 3 && re != 7 && re != 9) {
            return -1;
        }

        int ans = 0;
        int num = 0;
        while (num != 1) {
            num /= 10;
            auto r = num % 10;
            auto need = 0 == r ? 1 : 11 - r;

            switch (need) {
                case 1:
                {
                    if (1 == re) {
                        num += K;
                    }
                    else if (3 == re) {
                        num += 7 * K;
                    }
                    else if (7 == re) {
                        num += 3 * K;
                    }
                    else {
                        num += 9 * K;
                    }
                    break;
                }

                case 2:
                {
                    if (1 == re) {
                        num += 2 * K;
                    }
                    else if (3 == re) {
                        num += 4 * K;
                    }
                    else if (7 == re) {
                        num += 6 * K;
                    }
                    else {
                        num += 8 * K;
                    }
                    break;
                }

                case 3:
                {
                    if (1 == re) {
                        num += 3 * K;
                    }
                    else if (3 == re) {
                        num += K;
                    }
                    else if (7 == re) {
                        num += 9 * K;
                    }
                    else {
                        num += 7 * K;
                    }
                    break;
                }
                case 4:
                {
                    if (1 == re) {
                        num += 4 * K;
                    }
                    else if (3 == re) {
                        num += 8 * K;
                    }
                    else if (7 == re) {
                        num += 2 * K;
                    }
                    else {
                        num += 6 * K;
                    }
                    break;
                }
                case 5:
                {
                    num += 5 * K;
                    break;
                }
                case 6:
                {
                    if (1 == re) {
                        num += 6 * K;
                    }
                    else if (3 == re) {
                        num += 2 * K;
                    }
                    else if (7 == re) {
                        num += 8 * K;
                    }
                    else {
                        num += 4 * K;
                    }
                    break;
                }
                case 7:
                {
                    if (1 == re) {
                        num += 7 * K;
                    }
                    else if (3 == re) {
                        num += 9 * K;
                    }
                    else if (7 == re) {
                        num += K;
                    }
                    else {
                        num += 3 * K;
                    }
                    break;
                }
                case 8:
                {
                    if (1 == re) {
                        num += 8 * K;
                    }
                    else if (3 == re) {
                        num += 6 * K;
                    }
                    else if (7 == re) {
                        num += 4 * K;
                    }
                    else {
                        num += 2 * K;
                    }
                    break;
                }
                case 9:
                {
                    if (1 == re) {
                        num += 9 * K;
                    }
                    else if (3 == re) {
                        num += 3 * K;
                    }
                    else if (7 == re) {
                        num += 7 * K;
                    }
                    else {
                        num += K;
                    }
                    break;
                }
            }

            ++ans;
        }

        return ans;
    }
};
