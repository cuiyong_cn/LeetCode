/**
 * Word Break
 *
 * Given a string s and a dictionary of strings wordDict, return true if s can be segmented into a
 * space-separated sequence of one or more dictionary words.
 *
 * Note that the same word in the dictionary may be reused multiple times in the segmentation.
 *
 * Example 1:
 *
 * Input: s = "leetcode", wordDict = ["leet","code"]
 * Output: true
 * Explanation: Return true because "leetcode" can be segmented as "leet code".
 * Example 2:
 *
 * Input: s = "applepenapple", wordDict = ["apple","pen"]
 * Output: true
 * Explanation: Return true because "applepenapple" can be segmented as "apple pen apple".
 * Note that you are allowed to reuse a dictionary word.
 * Example 3:
 *
 * Input: s = "catsandog", wordDict = ["cats","dog","sand","and","cat"]
 * Output: false
 *
 * Constraints:
 *
 * 1 <= s.length <= 300
 * 1 <= wordDict.length <= 1000
 * 1 <= wordDict[i].length <= 20
 * s and wordDict[i] consist of only lowercase English letters.
 * All the strings of wordDict are unique.
 */
/**
 * DP solution.
 */
class Solution {
public:
    bool wordBreak(string s, vector<string>& wordDict) {
        unordered_set<string> dict{begin(wordDict), end(wordDict)};
        vector<bool> dp(s.length() + 1, false);
        dp[0] = true;

        for (int i = 1; i <= s.length(); ++i) {
            int gate = std::max(0, i - 20);
            for (int j = i - 1; j >= gate; --j) {
                if (dp[j]) {
                    auto sub = s.substr(j, i - j);
                    if (dict.count(sub)) {
                        dp[i] = true;
                        break;
                    }
                }
            }
        }

        return dp.back();
    }
};

/**
 * Using memo to speed it up
 */
class Solution {
public:
    bool wordBreak(string s, vector<string>& wordDict) {
        vector<int> memo(s.length(), -1);
        return dfs(s, {begin(wordDict), end(wordDict)}, 0, memo);
    }

private:
    bool dfs(string const& s, unordered_set<string> const& dict, int pos, vector<int>& memo)
    {
        if (pos == s.length()) {
            return true;
        }

        if (memo[pos] != -1) {
            return 1 == memo[pos];
        }

        string word;
        for (int i = pos; i < s.length(); ++i) {
            word.push_back(s[i]);
            if (dict.count(word)) {
                if (dfs(s, dict, pos + word.length(), memo)) {
                    return true;
                }
            }
        }

        memo[pos] = 0;
        return false;
    }
};

/**
 * Original solution. TLE as expected.
 */
class Solution {
public:
    bool wordBreak(string s, vector<string>& wordDict) {
        return dfs(s, wordDict, 0);
    }

private:
    bool dfs(const string s, vector<string> const& wordDict, int pos)
    {
        if (pos == s.length()) {
            return true;
        }

        for (auto w : wordDict) {
            if (s[pos] == w[0]) {
                if (0 == s.compare(pos, w.length(), w)) {
                    if (dfs(s, wordDict, pos + w.length())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
};
