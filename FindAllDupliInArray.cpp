/**
 * Find All Duplicates in an Array
 *
 * Given an array of integers, 1 ≤ a[i] ≤ n (n = size of array), some elements
 * appear twice and others appear once.
 *
 * Find all the elements that appear twice in this array.
 *
 * Could you do it without extra space and in O(n) runtime?
 *
 * Example:
 *
 * Input:
 * [4,3,2,7,8,2,3,1]
 *
 * Output:
 * [2,3]
 */
/**
 * Original code. Obviously not in O(n) time.
 */
class Solution {
public:
    vector<int> findDuplicates(vector<int>& nums) {
        vector<int> ans;
        sort(nums.begin(), nums.end());
        int loopCnt = nums.size() - 1;
        for (int i = 0; i < loopCnt; ++i) {
            if (nums[i] == nums[i + 1]) {
                ans.push_back(nums[i]);
            }
        }
        return ans;
    }
};

/**
 * using the idea that, if we get a positive number by negating.
 * That means we negate it before (meets twice)
 */
class Solution {
public:
    vector<int> findDuplicates(vector<int>& nums) {
        vector<int> ans;

        int loopCnt = nums.size();
        for (int i = 0; i < loopCnt; ++i) {
            int index = abs(nums[i]) - 1;
            nums[index] = -nums[index];
            if (nums[index] > 0) ans.push_back(abs(nums[i]));
        }
        return ans;
    }
};

class Solution {
public:
    vector<int> findDuplicates(vector<int>& nums) {
        vector<int> res;
        int i = 0;
        /**
         * Here it kind of like sort the elements in it right position
         */
        while (i < nums.size()) {
            while(nums[i] != nums[nums[i] - 1]) {
                swap(nums[i], nums[nums[i] - 1]);
            }
            i++;
        }
        for (i = 0; i < nums.size(); i++) {
            if (nums[i] != i + 1) res.push_back(nums[i]);
        }
        return res;
    }
};
