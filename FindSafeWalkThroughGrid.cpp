/**
 * Find a Safe Walk Through a Grid
 *
 * You are given an m x n binary matrix grid and an integer health.
 * You start on the upper-left corner (0, 0) and would like to get to the lower-right corner (m - 1, n - 1).
 * You can move up, down, left, or right from one cell to another adjacent cell as long as your health remains positive.
 * Cells (i, j) with grid[i][j] = 1 are considered unsafe and reduce your health by 1.
 * Return true if you can reach the final cell with a health value of 1 or more, and false otherwise.
 *
 * Example 1:
 *
 * Input: grid = [[0,1,0,0,0],[0,1,0,1,0],[0,0,0,1,0]], health = 1
 *
 * Output: true
 *
 * Explanation:
 *
 * The final cell can be reached safely by walking along the gray cells below.
 *
 * Example 2:
 *
 * Input: grid = [[0,1,1,0,0,0],[1,0,1,0,0,0],[0,1,1,1,0,1],[0,0,1,0,1,0]], health = 3
 *
 * Output: false
 *
 * Explanation:
 *
 * A minimum of 4 health points is needed to reach the final cell safely.
 *
 * Example 3:
 *
 * Input: grid = [[1,1,1],[1,0,1],[1,1,1]], health = 5
 *
 * Output: true
 *
 * Explanation:
 *
 * The final cell can be reached safely by walking along the gray cells below.
 *
 * Any path that does not go through the cell (1, 1) is unsafe since your health will drop to 0 when reaching the final cell.
 *
 * Constraints:
 *     m == grid.length
 *     n == grid[i].length
 *     1 <= m, n <= 50
 *     2 <= m * n
 *     1 <= health <= m + n
 *     grid[i][j] is either 0 or 1.
 */
/**
 * Original solution.
 */
class Solution {
public:
    bool findSafeWalk(vector<vector<int>>& grid, int health) {
        auto const rows = grid.size();
        auto const cols = grid[0].size();
        auto cache = vector(rows, vector<int>(cols, -1));
        auto pq = priority_queue<tuple<int, int, int>>{};
        auto within_grid = [rows, cols](int x, int y) {
            return (0 <= x) && (x < rows)
                && (0 <= y) && (y < cols);
        };
        auto reach_target = [r = rows - 1, c = cols - 1](int x, int y) {
            return (r == x) && (c == y);
        };

        pq.push({health, 0, 0});

        while (!pq.empty()) {
            auto [h, x, y] = pq.top();
            pq.pop();

            if (!within_grid(x, y)) {
                continue;
            }

            if (1 == grid[x][y]) {
                --h;
            }

            if (h > 0) {
                if (reach_target(x, y)) {
                    return true;
                }

                if ((-1 == cache[x][y]) && (h > cache[x][y])) {
                    cache[x][y] = h;
                    pq.push({h, x, y - 1});
                    pq.push({h, x, y + 1});
                    pq.push({h, x - 1, y});
                    pq.push({h, x + 1, y});
                }
            }
        }

        return false;
    }
};
