/**
 * Number of Ways to Arrive at Destination
 *
 * You are in a city that consists of n intersections numbered from 0 to n - 1 with bi-directional
 * roads between some intersections. The inputs are generated such that you can reach any
 * intersection from any other intersection and that there is at most one road between any two
 * intersections.
 *
 * You are given an integer n and a 2D integer array roads where roads[i] = [ui, vi, timei] means
 * that there is a road between intersections ui and vi that takes timei minutes to travel. You want
 * to know in how many ways you can travel from intersection 0 to intersection n - 1 in the shortest
 * amount of time.
 *
 * Return the number of ways you can arrive at your destination in the shortest amount of time.
 * Since the answer may be large, return it modulo 109 + 7.
 *
 * Example 1:
 *
 * Input: n = 7, roads = [[0,6,7],[0,1,2],[1,2,3],[1,3,3],[6,3,3],[3,5,1],[6,5,1],[2,5,1],[0,4,5],[4,6,2]]
 * Output: 4
 * Explanation: The shortest amount of time it takes to go from intersection 0 to intersection 6 is 7 minutes.
 * The four ways to get there in 7 minutes are:
 * - 0 ➝ 6
 * - 0 ➝ 4 ➝ 6
 * - 0 ➝ 1 ➝ 2 ➝ 5 ➝ 6
 * - 0 ➝ 1 ➝ 3 ➝ 5 ➝ 6
 * Example 2:
 *
 * Input: n = 2, roads = [[1,0,10]]
 * Output: 1
 * Explanation: There is only one way to go from intersection 0 to intersection 1, and it takes 10 minutes.
 *
 * Constraints:
 *
 * 1 <= n <= 200
 * n - 1 <= roads.length <= n * (n - 1) / 2
 * roads[i].length == 3
 * 0 <= ui, vi <= n - 1
 * 1 <= timei <= 109
 * ui != vi
 * There is at most one road connecting any two intersections.
 * You can reach any intersection from any other intersection.
 */
/**
 * BFS solution. The idea is same as DFS. But it iteratively update the shorteest one using the cache.
 */
constexpr int mod = 1'000'000'007;
using Vertex = pair<int64_t, int>;
using MinHeap = priority_queue<Vertex, vector<Vertex>, greater<Vertex>>;

class Solution {
public:
    int countPaths(int n, vector<vector<int>>& roads) {
        unordered_map<int, vector<pair<int, int64_t>>> graph;
        vector<pair<int, int64_t>> dp(n, make_pair(0, numeric_limits<int64_t>::max()));
        dp[0].first = 1;
        dp[0].second = 0;

        for (auto const& r : roads) {
            graph[r[0]].emplace_back(r[1], r[2]);
            graph[r[1]].emplace_back(r[0], r[2]);
        }

        MinHeap mh;
        mh.emplace(0, 0);

        while (!mh.empty()) {
            auto [times, u] = mh.top();
            mh.pop();

            if (times > dp[u].second) {
                continue;
            }

            for (auto const& [v, t] : graph[u]) {
                auto const total = times + t;
                if (dp[v].second > total) {
                    dp[v].second = total;
                    dp[v].first = dp[u].first;
                    mh.emplace(total, v);
                }
                else if (total == dp[v].second) {
                    dp[v].first = (dp[v].first + dp[u].first) % mod;
                }
            }
        }

        return dp[n - 1].first;
    }
};

/**
 * Original DFS solution. TLE.
 */
constexpr int visited = 1;
constexpr int unvisit = 0;
constexpr int mod = 1'000'000'007;

class Solution {
public:
    int countPaths(int n, vector<vector<int>>& roads) {
        visit = vector<int>(n, 0);
        dp = vector<pair<int, int64_t>>(n, make_pair(0, numeric_limits<int64_t>::max()));

        for (auto const& r : roads) {
            graph[r[0]].emplace_back(r[1], r[2]);
            graph[r[1]].emplace_back(r[0], r[2]);
        }

        dfs(0, 0);

        return dp[n - 1].first;
    }

private:
    void dfs(int u, int64_t times)
    {
        if (visited == visit[u] || times > dp[u].second) {
            return;
        }

        visit[u] = visited;

        if (times < dp[u].second) {
            dp[u].first = 1;
            dp[u].second = times;
        }
        else {
            if (mod == dp[u].first) {
                dp[u].first = 0;
            }
            ++dp[u].first;
        }

        for (auto const& [v, time] : graph[u]) {
            dfs(v, times + time);
        }

        visit[u] = unvisit;
    }

    vector<int> visit;
    unordered_map<int, vector<pair<int, int64_t>>> graph;
    vector<pair<int, int64_t>> dp;
};
