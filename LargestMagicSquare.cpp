/**
 * Largest Magic Square
 *
 * A k x k magic square is a k x k grid filled with integers such that every row sum, every column
 * sum, and both diagonal sums are all equal. The integers in the magic square do not have to be
 * distinct. Every 1 x 1 grid is trivially a magic square.
 * 
 * Given an m x n integer grid, return the size (i.e., the side length k) of the largest magic
 * square that can be found within this grid.
 * 
 * Example 1:
 * 
 * Input: grid =
 * [[7,1,4,5,6],
 * [2,5,1,6,4],
 * [1,5,4,3,2],
 * [1,2,7,3,4]]
 *
 * Output: 3
 * Explanation: The largest magic square has a size of 3.
 * Every row sum, column sum, and diagonal sum of this magic square is equal to 12.
 * - Row sums: 5+1+6 = 5+4+3 = 2+7+3 = 12
 * - Column sums: 5+5+2 = 1+4+7 = 6+3+3 = 12
 * - Diagonal sums: 5+4+3 = 6+4+2 = 12
 * Example 2:
 * 
 * Input: grid =
 * [[5,1,3,1],
 * [9,3,3,1],
 * [1,3,3,8]]
 *
 * Output: 2
 * 
 * Constraints:
 * 
 * m == grid.length
 * n == grid[i].length
 * 1 <= m, n <= 50
 * 1 <= grid[i][j] <= 106
 */
/**
 * Original solution. I really hate this kind of problem.
 */
class Solution {
public:
    int largestMagicSquare(vector<vector<int>>& grid) {
        int const rows = grid.size();
        int const cols = grid[0].size();

        vector<vector<int>> row_psum(rows, vector<int>(cols + 1, 0));
        vector<vector<int>> col_psum(rows + 1, vector<int>(cols, 0));

        for (int r = 0; r < rows; ++r) {
            partial_sum(grid[r].begin(), grid[r].end(), row_psum[r].begin() + 1);
        }

        for (int c = 0; c < cols; ++c) {
            for (int r = 0; r < rows; ++r) {
                col_psum[r + 1][c] += grid[r][c] + col_psum[r][c];
            }
        }

        int const max_k = min(rows, cols);
        for (int k = max_k; k > 1; --k) {
            int max_row = rows - k;
            int max_col = cols - k;

            // check every square with top left cord (r, c)
            for (int r = 0; r <= max_row; ++r) {
                for (int c = 0; c <= max_col; ++c) {
                    int const sum = row_psum[r][c + k] - row_psum[r][c];
                    bool row_equal = true;
                    // row check
                    for (int ro = 1; ro < k; ++ro) {
                        if (sum != (row_psum[r + ro][c + k] - row_psum[r + ro][c])) {
                            row_equal = false;
                            break;
                        }
                    }

                    if (!row_equal) {
                        continue;
                    }

                    // col check
                    bool col_equal = true;
                    for (int co = 1; co < k; ++co) {
                        if (sum != (col_psum[r + k][c + co] - col_psum[r][c + co])) {
                            col_equal = false;
                            break;
                        }
                    }

                    if (!col_equal) {
                        continue;
                    }

                    // diagonal check
                    int diag = grid[r][c];
                    for (int d = 1; d < k; ++d) {
                        diag += grid[r + d][c + d];
                    }

                    if (sum != diag) {
                        continue;
                    }
                    
                    int rev_diag = grid[r][c + k - 1];
                    for (int d = 1; d < k; ++d) {
                        rev_diag += grid[r + d][c + k - 1 - d];
                    }

                    if (sum == rev_diag) {
                        return k;
                    }
                }
            }
        }

        return 1;
    }
};
