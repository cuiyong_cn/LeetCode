/**
 * Number of Longest Increasing Subsequence
 *
 * Given an integer array nums, return the number of longest increasing subsequences.
 *
 * Notice that the sequence has to be strictly increasing.
 *
 * Example 1:
 *
 * Input: nums = [1,3,5,4,7]
 * Output: 2
 * Explanation: The two longest increasing subsequences are [1, 3, 4, 7] and [1, 3, 5, 7].
 * Example 2:
 *
 * Input: nums = [2,2,2,2,2]
 * Output: 5
 * Explanation: The length of longest continuous increasing subsequence is 1, and there are 5
 * subsequences' length is 1, so output 5.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 2000
 * -106 <= nums[i] <= 106
 */
/**
 * Same idea, without store the actual subs
 */
class Solution {
public:
    int findNumberOfLIS(vector<int>& nums) {
        int n = nums.size(), res = 0, max_len = 0;
        vector<pair<int,int>> dp(n, {1,1}); //dp[i]: {length, number of LIS which ends with nums[i]}

        for (int i = 0; i<n; i++) {
            for (int j = 0; j <i ; j++) {
                if (nums[i] > nums[j]) {
                    if (dp[i].first == dp[j].first + 1) {
                        dp[i].second += dp[j].second;
                    }
                    else if (dp[i].first < dp[j].first + 1) {
                        dp[i] = {dp[j].first + 1, dp[j].second};
                    }
                }
            }

            if (max_len == dp[i].first) {
                res += dp[i].second;
            }
            else if (max_len < dp[i].first) {
                max_len = dp[i].first;
                res = dp[i].second;
            }
        }

        return res;
    }
};

/**
 * Original solution. This should work. But TLE.
 */
class Solution {
public:
    int findNumberOfLIS(vector<int>& nums) {
        vector<vector<int>> incr_subs;
        for (auto n : nums) {
            vector<vector<int>> new_incr_subs;
            for (auto& sub : incr_subs) {
                if (n > sub.back()) {
                    auto clone = sub;
                    clone.push_back(n);
                    new_incr_subs.emplace_back(std::move(clone));
                }
            }
            incr_subs.insert(end(incr_subs), begin(new_incr_subs), end(new_incr_subs));
            incr_subs.push_back({n});
        }

        sort(begin(incr_subs), end(incr_subs),
            [](auto const& a, auto const& b) {
                return a.size() > b.size();
            });

        for (int i = 1; i < incr_subs.size(); ++i) {
            if (incr_subs[i].size() != incr_subs[i - 1].size()) {
                return i;
            }
        }

        return incr_subs.size();
    }
};
