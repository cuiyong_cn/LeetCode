/**
 * Shortest Path with Alternating Colors
 *
 * Consider a directed graph, with nodes labelled 0, 1, ..., n-1.  In this graph, each edge is
 * either red or blue, and there could be self-edges or parallel edges.
 *
 * Each [i, j] in red_edges denotes a red directed edge from node i to node j.  Similarly, each [i,
 * j] in blue_edges denotes a blue directed edge from node i to node j.
 *
 * Return an array answer of length n, where each answer[X] is the length of the shortest path from
 * node 0 to node X such that the edge colors alternate along the path (or -1 if such a path doesn't
 * exist).
 *
 * Example 1:
 *
 * Input: n = 3, red_edges = [[0,1],[1,2]], blue_edges = []
 * Output: [0,1,-1]
 * Example 2:
 *
 * Input: n = 3, red_edges = [[0,1]], blue_edges = [[2,1]]
 * Output: [0,1,-1]
 * Example 3:
 *
 * Input: n = 3, red_edges = [[1,0]], blue_edges = [[2,1]]
 * Output: [0,-1,-1]
 * Example 4:
 *
 * Input: n = 3, red_edges = [[0,1]], blue_edges = [[1,2]]
 * Output: [0,1,2]
 * Example 5:
 *
 * Input: n = 3, red_edges = [[0,1],[0,2]], blue_edges = [[1,0]]
 * Output: [0,1,1]
 *
 * Constraints:
 *
 * 1 <= n <= 100
 * red_edges.length <= 400
 * blue_edges.length <= 400
 * red_edges[i].length == blue_edges[i].length == 2
 * 0 <= red_edges[i][j], blue_edges[i][j] < n
 */
/**
 * Original solution.
 */
class Solution {
public:
    enum class Color
    {
        red,
        blue,
    };

    using Graph = unordered_map<int, vector<int>>;

    vector<int> shortestAlternatingPaths(int n, vector<vector<int>>& red_edges, vector<vector<int>>& blue_edges) {
        unordered_map<Color, Graph> color_graph;

        for (auto const& re : red_edges) {
            color_graph[Color::red][re[0]].push_back(re[1]);
        }

        for (auto const& be : blue_edges) {
            color_graph[Color::blue][be[0]].push_back(be[1]);
        }

        int len = 1;
        vector<pair<int, Color>> q{ {0, Color::red}, {0, Color::blue} };
        set<pair<int, Color>> seen;
        vector<int> ans(n, 101);

        while (!q.empty()) {
            vector<pair<int, Color>> nq;

            for (auto const& p : q) {
                if (0 != seen.count(p)) {
                    continue;
                }

                seen.insert(p);

                Color color = Color::red == p.second ? Color::blue : Color::red;
                auto const& graph = color_graph[color];

                auto it = graph.find(p.first);
                if (it != graph.end()) {
                    for (auto const& bp : it->second) {
                        if (len < ans[bp]) {
                            ans[bp] = len;
                        }
                        nq.emplace_back(make_pair(bp, color));
                    }
                }
            }

            ++len;

            swap(q, nq);
        }

        ans[0] = 0;
        auto setCanNotReach = [](int& len) {
            if (len > 100) {
                len = -1;
            }
        };

        for_each(begin(ans), end(ans), setCanNotReach);

        return ans;
    }
};
