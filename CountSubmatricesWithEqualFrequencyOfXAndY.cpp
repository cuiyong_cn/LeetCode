/**
 * Count Submatrices With Equal Frequency of X and Y
 *
 * Given a 2D character matrix grid, where grid[i][j] is either 'X', 'Y', or '.', return the number
 * of submatrices that contain:
 *     grid[0][0]
 *     an equal frequency of 'X' and 'Y'.
 *     at least one 'X'.
 *
 * Example 1:
 *
 * Input: grid = [["X","Y","."],["Y",".","."]]
 *
 * Output: 3
 *
 * Explanation:
 *
 * Example 2:
 *
 * Input: grid = [["X","X"],["X","Y"]]
 *
 * Output: 0
 *
 * Explanation:
 *
 * No submatrix has an equal frequency of 'X' and 'Y'.
 *
 * Example 3:
 *
 * Input: grid = [[".","."],[".","."]]
 *
 * Output: 0
 *
 * Explanation:
 *
 * No submatrix has at least one 'X'.
 *
 * Constraints:
 *     1 <= grid.length, grid[i].length <= 1000
 *     grid[i][j] is either 'X', 'Y', or '.'.
 */
/**
 * Original solution.
 */
auto operator+(pair<int, int> a, pair<int, int> b)
{
    return pair{a.first + b.first, a.second + b.second};
}

class Solution {
public:
    int numberOfSubmatrices(vector<vector<char>>& grid) {
        auto const rows = grid.size();
        auto const cols = grid[0].size();
        auto prefix = vector<vector<pair<int, int>>>(rows, vector<pair<int, int>>(cols));

        for (auto r = 0uz; r < rows; ++r) {
            auto [x_cnt, y_cnt] = pair(0, 0);

            for (auto c = 0uz; c < cols; ++c) {
                if ('X' == grid[r][c]) {
                    ++x_cnt;
                }
                else if ('Y' == grid[r][c]) {
                    ++y_cnt;
                }

                prefix[r][c] = {x_cnt, y_cnt};
            }
        }

        for (auto c = 0uz; c < cols; ++c) {
            for (auto r = 1uz; r < rows; ++r) {
                prefix[r][c] = prefix[r][c] + prefix[r - 1][c];
            }
        }

        auto ans = 0;

        for (auto const& row : prefix) {
            for (auto const& [x, y] : row) {
                if (x > 0 && x == y) {
                    ++ans;
                }
            }
        }

        return ans;
    }
};
