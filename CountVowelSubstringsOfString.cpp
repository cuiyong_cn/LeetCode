/**
 * Count Vowel Substrings of a String
 *
 * A substring is a contiguous (non-empty) sequence of characters within a string.
 *
 * A vowel substring is a substring that only consists of vowels ('a', 'e', 'i', 'o', and 'u') and
 * has all five vowels present in it.
 *
 * Given a string word, return the number of vowel substrings in word.
 *
 * Example 1:
 *
 * Input: word = "aeiouu"
 * Output: 2
 * Explanation: The vowel substrings of word are as follows (underlined):
 * - "aeiouu"
 * - "aeiouu"
 * Example 2:
 *
 * Input: word = "unicornarihan"
 * Output: 0
 * Explanation: Not all 5 vowels are present, so there are no vowel substrings.
 * Example 3:
 *
 * Input: word = "cuaieuouac"
 * Output: 7
 * Explanation: The vowel substrings of word are as follows (underlined):
 * - "cuaieuouac"
 * - "cuaieuouac"
 * - "cuaieuouac"
 * - "cuaieuouac"
 * - "cuaieuouac"
 * - "cuaieuouac"
 * - "cuaieuouac"
 *
 * Constraints:
 *
 * 1 <= word.length <= 100
 * word consists of lowercase English letters only.
 */
/**
 * Linear time.
 * j mark the start of an "all-vowel" substring, and i is the current position. The window between
 * k - 1 and i is the smallest window with all 5 vowels.*
 */
/**                         j        k           i
 *             -------------------------------------
 *             |  |  |  |  |  |  |  |  |  |  |  |  |
 *             -------------------------------------
 */
class Solution {
public:
    int countVowelSubstrings(string w) {
        int j = 0, k = 0, vow = 0, cnt = 0;
        unordered_map<char, int> m {{'a', 0}, {'e', 0}, {'i', 0}, {'o', 0}, {'u', 0}};
        for (int i = 0; i < w.size(); ++i) {
            if (m.count(w[i])) {
                vow += ++m[w[i]] == 1;
                for (; vow == 5; ++k)
                    vow -= --m[w[k]] == 0;
                cnt += k - j;
            }
            else {
                m['a'] = m['e'] = m['i'] = m['o'] = m['u'] = vow = 0;
                j = k = i + 1;
            }
        }
        return cnt;
    }
};

/**
 * Original solution. Brite force. This one can passed!
 */
bool is_vowel(char c)
{
    return 'a' == c || 'e' == c || 'i' == c || 'o' == c || 'u' == c;
}

bool is_vowel_string(int* cnt)
{
    return cnt[0] > 0 && cnt['e' - 'a'] > 0 && cnt['i' - 'a'] > 0
        && cnt['o' - 'a'] > 0 && cnt['u' - 'a'] > 0;
}

class Solution {
public:
    int countVowelSubstrings(string word) {
        int const N = word.length();
        int const SI = N - 4;
        int ans = 0;

        for (int i = 0; i < SI; ++i) {
            if (!is_vowel(word[i])) {
                continue;
            }

            int cnt[26] = {0, };
            bool vowel_string = false;
            for (int j = i; j < N; ++j) {
                if (!is_vowel(word[j])) {
                    break;
                }

                if (vowel_string) {
                    ++ans;
                    continue;
                }

                ++cnt[word[j] - 'a'];
                if (is_vowel_string(cnt)) {
                    ++ans;
                    vowel_string = true;
                }
            }
        }

        return ans;
    }
};
