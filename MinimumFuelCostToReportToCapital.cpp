/**
 * Minimum Fuel Cost to Report to the Capital
 *
 * There is a tree (i.e., a connected, undirected graph with no cycles) structure country network
 * consisting of n cities numbered from 0 to n - 1 and exactly n - 1 roads. The capital city is city
 * 0. You are given a 2D integer array roads where roads[i] = [ai, bi] denotes that there exists a
 * bidirectional road connecting cities ai and bi.
 *
 * There is a meeting for the representatives of each city. The meeting is in the capital city.
 *
 * There is a car in each city. You are given an integer seats that indicates the number of seats in
 * each car.
 *
 * A representative can use the car in their city to travel or change the car and ride with another
 * representative. The cost of traveling between two cities is one liter of fuel.
 *
 * Return the minimum number of liters of fuel to reach the capital city.
 *
 * Example 1:
 *
 * Input: roads = [[0,1],[0,2],[0,3]], seats = 5
 * Output: 3
 * Explanation:
 * - Representative1 goes directly to the capital with 1 liter of fuel.
 * - Representative2 goes directly to the capital with 1 liter of fuel.
 * - Representative3 goes directly to the capital with 1 liter of fuel.
 * It costs 3 liters of fuel at minimum.
 * It can be proven that 3 is the minimum number of liters of fuel needed.
 * Example 2:
 *
 * Input: roads = [[3,1],[3,2],[1,0],[0,4],[0,5],[4,6]], seats = 2
 * Output: 7
 * Explanation:
 * - Representative2 goes directly to city 3 with 1 liter of fuel.
 * - Representative2 and representative3 go together to city 1 with 1 liter of fuel.
 * - Representative2 and representative3 go together to the capital with 1 liter of fuel.
 * - Representative1 goes directly to the capital with 1 liter of fuel.
 * - Representative5 goes directly to the capital with 1 liter of fuel.
 * - Representative6 goes directly to city 4 with 1 liter of fuel.
 * - Representative4 and representative6 go together to the capital with 1 liter of fuel.
 * It costs 7 liters of fuel at minimum.
 * It can be proven that 7 is the minimum number of liters of fuel needed.
 * Example 3:
 *
 * Input: roads = [], seats = 1
 * Output: 0
 * Explanation: No representatives need to travel to the capital city.
 *
 * Constraints:
 *
 * 1 <= n <= 105
 * roads.length == n - 1
 * roads[i].length == 2
 * 0 <= ai, bi < n
 * ai != bi
 * roads represents a valid tree.
 * 1 <= seats <= 105
 */
/**
 * Readuce a bit memory usage.
 */
auto build_road_map(vector<vector<int>> const& roads)
{
    auto road_map = unordered_map<int, vector<int>>{};
    for (auto const& r : roads) {
        road_map[r[0]].push_back(r[1]);
        road_map[r[1]].push_back(r[0]);
    }
    return road_map;
}

auto fule_costs_start_from(
        unordered_map<int, vector<int>> const& road_map,
        int node,
        int parent_node,
        int seats,
        long long& costs) -> int
{
    auto people = 1;
    for (auto const& r : road_map.at(node)) {
        if (parent_node != r) {
            auto pc = fule_costs_start_from(road_map, r, node, seats, costs);
            costs += (pc + seats - 1) / seats;
            people += pc;
        }
    }

    return people;
}

class Solution {
public:
    long long minimumFuelCost(vector<vector<int>>& roads, int seats) {

        auto road_map = build_road_map(roads);
        if (road_map.empty()) {
            return 0;
        }

        auto costs = 0LL;
        fule_costs_start_from(road_map, 0, -1, seats, costs);
        return costs;
    }
};

/**
 * Original solution.
 * Revers thinking. Send all representatives back to their origin city from capital
 */
auto build_road_map(vector<vector<int>> const& roads)
{
    auto road_map = unordered_map<int, vector<int>>{};
    for (auto const& r : roads) {
        road_map[r[0]].push_back(r[1]);
        road_map[r[1]].push_back(r[0]);
    }
    return road_map;
}

auto update_sub_node_size(
        unordered_map<int, vector<int>> const& road_map,
        unordered_map<int, int>& sub_node_size,
        int node,
        int parent_node)
{
    if (sub_node_size.count(node)) {
        return sub_node_size[node];
    }

    auto size = 1;
    for (auto const& r : road_map.at(node)) {
        if (parent_node != r) {
            size += update_sub_node_size(road_map, sub_node_size, r, node);
        }
    }

    sub_node_size[node] = size;
    return size;
}

auto fule_costs_start_from(
        unordered_map<int, vector<int>> const& road_map,
        unordered_map<int, int>& sub_node_size,
        int node,
        int parent_node,
        int seats) -> long long
{
    auto costs = 0LL;
    for (auto const& r : road_map.at(node)) {
        if (r != parent_node) {
            costs += (sub_node_size[r] + seats - 1) / seats;
            costs += fule_costs_start_from(road_map, sub_node_size, r, node, seats);
        }
    }
    return costs;
}

class Solution {
public:
    long long minimumFuelCost(vector<vector<int>>& roads, int seats) {

        auto road_map = build_road_map(roads);
        if (road_map.empty()) {
            return 0;
        }

        auto sub_node_size = unordered_map<int, int>{};
        update_sub_node_size(road_map, sub_node_size, 0, -1);

        return fule_costs_start_from(road_map, sub_node_size, 0, -1, seats);
    }
};
