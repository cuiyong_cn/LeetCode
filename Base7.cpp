/**
 * Base 7
 *
 * Given an integer, return its base 7 string representation.
 *
 * Example 1:
 * Input: 100
 * Output: "202"
 * Example 2:
 * Input: -7
 * Output: "-10"
 * Note: The input will be in range of [-1e7, 1e7].
 */
/**
 * Recursive version.
 */
class Solution {
public:
    string convertToBase7(int n) {
        if (n < 0) {
            return "-" + convertToBase7(-n);
        }

        if (n < 7) {
            return to_string(n);
        }

        return convertToBase7(n / 7) + to_string(n % 7);
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    string convertToBase7(int num) {
        string ans;
        if (0 == num) {
            ans += '0';
        }

        int abs_num = std::abs(num);

        while (abs_num > 0) {
            ans += '0' + abs_num % 7;
            abs_num = abs_num / 7;
        }

        if (num < 0) {
            ans += '-';
        }

        std::reverse(std::begin(ans), std::end(ans));

        return ans;
    }
};
