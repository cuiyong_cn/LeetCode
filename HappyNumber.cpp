/**
 * Happy Number
 *
 * Write an algorithm to determine if a number n is "happy".
 *
 * A happy number is a number defined by the following process: Starting with any positive integer, replace the number
 * by the sum of the squares of its digits, and repeat the process until the number equals 1 (where it will stay), or it
 * loops endlessly in a cycle which does not include 1. Those numbers for which this process ends in 1 are happy
 * numbers.
 *
 * Return True if n is a happy number, and False if not.
 *
 * Example:
 *
 * Input: 19
 * Output: true
 * Explanation:
 * 12 + 92 = 82
 * 82 + 22 = 68
 * 62 + 82 = 100
 * 12 + 02 + 02 = 1
 */
/**
 * And from discussion, I learn two new method to detect the cycle.
 * 1. Floyd's cycle detection
 * 2. Brent's cycle detection
 */
class Solution {
public:
    int next(int n)
    {
        int sum = 0;

        while(n != 0)
        {
            sum += pow(n % 10,2);
            n = n / 10;
        }

        return sum;
    }

public:
    bool isHappy(int n) {
        int slow = next(n);
        int fast = next(next(n));

        while(slow != fast)
        {
            slow = next(slow);
            fast = next(next(fast));
        }

        return fast == 1 ;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool isHappy(int n) {
        bool ans = false;
        unordered_set<int> visited;
        while (0 == visited.count(n)) {
            visited.insert(n);
            n = happify(n);
            if (1 == n) {
                ans = true;
                break;
            }
        }

        return ans;
    }

private:
    int happify(int n)
    {
        int sum = 0;
        while (n) {
            int d = n % 10;
            sum += d * d;
            n = n / 10;
        }

        return sum;
    }
};
