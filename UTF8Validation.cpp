/**
 * UTF-8 Validation
 *
 * Given an integer array data representing the data, return whether it is a valid UTF-8 encoding.
 *
 * A character in UTF8 can be from 1 to 4 bytes long, subjected to the following rules:
 *
 * For a 1-byte character, the first bit is a 0, followed by its Unicode code.
 * For an n-bytes character, the first n bits are all one's, the n + 1 bit is 0, followed by n - 1
 * bytes with the most significant 2 bits being 10.
 * This is how the UTF-8 encoding would work:
 *
 *    Char. number range  |        UTF-8 octet sequence
 *       (hexadecimal)    |              (binary)
 *    --------------------+---------------------------------------------
 *    0000 0000-0000 007F | 0xxxxxxx
 *    0000 0080-0000 07FF | 110xxxxx 10xxxxxx
 *    0000 0800-0000 FFFF | 1110xxxx 10xxxxxx 10xxxxxx
 *    0001 0000-0010 FFFF | 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
 * Note: The input is an array of integers. Only the least significant 8 bits of each integer is
 * used to store the data. This means each integer represents only 1 byte of data.
 *
 * Example 1:
 *
 * Input: data = [197,130,1]
 * Output: true
 * Explanation: data represents the octet sequence: 11000101 10000010 00000001.
 * It is a valid utf-8 encoding for a 2-bytes character followed by a 1-byte character.
 * Example 2:
 *
 * Input: data = [235,140,4]
 * Output: false
 * Explanation: data represented the octet sequence: 11101011 10001100 00000100.
 * The first 3 bits are all one's and the 4th bit is 0 means it is a 3-bytes character.
 * The next byte is a continuation byte which starts with 10 and that's correct.
 * But the second continuation byte does not start with 10, so it is invalid.
 *
 * Constraints:
 *
 * 1 <= data.length <= 2 * 104
 * 0 <= data[i] <= 255
 */
/**
 * More elegant solution.
 */
class Solution {
public:
    bool validUtf8(vector<int>& data) {
        int count = 0;
        for (auto c : data) {
            if (count == 0) {
                count = bytes_num(c);
                if (1 == count || count > 4) {
                    return false;
                }
                count -= count > 0;
            } else {
                if ((c >> 6) != 0b10) {
                    return false;
                }
                --count;
            }
        }
        return count == 0;
    }

private:
    int bytes_num(int data)
    {
        int cnt = 0;
        for (int i = 7; i > 2; --i) {
            if (data & (1 << i)) {
                ++cnt;
            }
            else {
                break;
            }
        }

        return cnt;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool validUtf8(vector<int>& data) {
        for (size_t i = 0; i < data.size();) {
            auto bnum = bytes_num(data[i]);
            if (0 == bnum) {
                ++i; continue;
            }
            if (1 == bnum) {
                return false;
            }

            if (data[i] & (1 << (7 - bnum))) {
                return false;
            }

            if ((data.size() - i) < bnum) {
                return false;
            }

            for (size_t j = 1; j < bnum; ++j) {
                if (0x80 != (data[i + j] & 0xC0)) {
                    return false;
                }
            }

            i += bnum;
        }

        return true;
    }

private:
    int bytes_num(int data)
    {
        int cnt = 0;
        for (int i = 7; i > 3; --i) {
            if (data & (1 << i)) {
                ++cnt;
            }
            else {
                break;
            }
        }

        return cnt;
    }
};
