/**
 * Longest Uncommon Subsequence II
 *
 * Given an array of strings strs, return the length of the longest uncommon subsequence between
 * them. If the longest uncommon subsequence does not exist, return -1.
 *
 * An uncommon subsequence between an array of strings is a string that is a subsequence of one
 * string but not the others.
 *
 * A subsequence of a string s is a string that can be obtained after deleting any number of
 * characters from s.
 *
 * For example, "abc" is a subsequence of "aebdc" because you can delete the underlined characters
 * in "aebdc" to get "abc". Other subsequences of "aebdc" include "aebdc", "aeb", and "" (empty
 * string).
 *
 * Example 1:
 *
 * Input: strs = ["aba","cdc","eae"]
 * Output: 3
 * Example 2:
 *
 * Input: strs = ["aaa","aaa","aa"]
 * Output: -1
 *
 * Constraints:
 *
 * 2 <= strs.length <= 50
 * 1 <= strs[i].length <= 10
 * strs[i] consists of lowercase English letters.
 */
/**
 * Optimize.
 */
class Solution {
public:
    int findLUSlength(vector<string>& strs) {
        sort(strs.begin(), strs.end(),
            [](auto const& a, auto const& b) {
                return a.length() > b.length();
            });
        auto duplicates = get_duplicates(strs);
        if (!duplicates.count(strs[0])) {
            return strs[0].length();
        }

        for (size_t i = 1; i < strs.size(); ++i) {
            if (!duplicates.count(strs[i])) {
                auto it_end = strs.begin() + i;
                auto it = find_if(strs.begin(), it_end,
                                 [b = strs[i], this](auto const& a) {
                                     return is_sub(a, b);
                                 });
                if (it == it_end) {
                    return strs[i].length();
                }
            }
        }

        return -1;
    }

private:
    unordered_set<string> get_duplicates(vector<string> const& strs)
    {
        unordered_set<string> seen;
        unordered_set<string> ans;
        for (auto& s : strs) {
            if (seen.count(s)) {
                ans.insert(s);
            }
            seen.insert(s);
        }

        return ans;
    }

    bool is_sub(string const& a, string const& b)
    {
        size_t i = 0;
        size_t j = 0;
        while (i < a.length() && j < b.length()) {
            if (a[i] == b[j]) {
                ++j;
            }
            ++i;
        }

        return b.length() == j;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int findLUSlength(vector<string>& strs) {
        for (auto const& s : strs) {
            string sub;
            cnt_all_subs(s, sub, 0);
        }

        int ans = 0;
        for (auto const& sc : sub_cnt) {
            if (1 == sc.second && sc.first.length() > ans) {
                ans = sc.first.length();
            }
        }

        return 0 == ans ? -1 : ans;
    }

private:
    void cnt_all_subs(string const& s, string& sub, int i)
    {
        if (i == s.length()) {
            ++sub_cnt[sub];
            return;
        }

        cnt_all_subs(s, sub, i + 1);

        sub.push_back(s[i]);
        cnt_all_subs(s, sub, i + 1);
        sub.pop_back();
    }

    unordered_map<string, int> sub_cnt;
};
