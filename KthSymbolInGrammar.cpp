/**
 * K-th Symbol in Grammar
 *
 * We build a table of n rows (1-indexed). We start by writing 0 in the 1st row. Now in every
 * subsequent row, we look at the previous row and replace each occurrence of 0 with 01, and each
 * occurrence of 1 with 10.
 *
 * For example, for n = 3, the 1st row is 0, the 2nd row is 01, and the 3rd row is 0110.  Given two
 * integer n and k, return the kth (1-indexed) symbol in the nth row of a table of n rows.
 *
 * Example 1:
 *
 * Input: n = 1, k = 1
 * Output: 0
 * Explanation: row 1: 0
 * Example 2:
 *
 * Input: n = 2, k = 1
 * Output: 0
 * Explanation:
 * row 1: 0
 * row 2: 01
 * Example 3:
 *
 * Input: n = 2, k = 2
 * Output: 1
 * Explanation:
 * row 1: 0
 * row 2: 01
 * Example 4:
 *
 * Input: n = 3, k = 1
 * Output: 0
 * Explanation:
 * row 1: 0
 * row 2: 01
 * row 3: 0110
 *
 * Constraints:
 *
 * 1 <= n <= 30
 * 1 <= k <= 2n - 1
 */
/**
 * Another way of thinking. idea is
 * The L_th number in row N-1 will generate 2 numbers in row N at position 2L and 2L+1
 * Note the pattern given in the problem 0 -> 01, 1->10.  This means the first digit will always
 * stay the same, and the second digit will flip between 0 and 1
 *
 * Now looking backward, our K could be 2L or 2L+1 If K is 2L, we know it'll take the same value as
 * L in row N-1.  If K is 2L+1, it'll take the flipped value keep doing this x times until we hit
 * the initial value of 0 in row 1.  During this x times of recursion, say there are y times where K
 * is odd. Those are the times when the value flipped between 0 and 1 so the end result is the
 * initial value 0, flipped y times
 *
 * Now going from K (2L or 2L+1) to L, is really just right shifting K by 1 bit Before you shift,
 * check if the least significant bit of K is 1, if so, record one occurrence of an odd number
 * essentially this is the same as counting the number of 1's in K's binary representation
 */
class Solution {
public:
    int kthGrammar(int N, int K) {
        int n = 0;
        for (K -= 1; K ; K &= (K - 1)) {
            n++;
        }

        return n & 1;
    }
};

/**
 * Improve the code. Idea is:
 *
 * parent:  0         1
 *         / \       / \
 *        0   1     1   0
 */
class Solution {
public:
    int kthGrammar(int N, int K) {
        if (N == 1) {
            return 0;
        }

        if (K % 2 == 0) {
            // right child, parent is K / 2
            return (kthGrammar(N - 1, K / 2) == 0) ? 1 : 0;
        }
        else {
            // left child, parent is (K + 1) / 2
            return (kthGrammar(N - 1, (K + 1) / 2) == 0) ? 0 : 1;
        }
    }
};

/**
 * Original solution. Dfs solution. Idea is
 * 0: 0
 * 1: 01
 * 2: 01 10
 * 3: 01 10 1001
 * 4: 01 10 1001 10010110
 * we can see that (n, k) if k < 2^(n - 1) == (n - 1, k)
 *                 (n, k) if k >= 2^(n - 1) == !(n - 1, k - half)
 */
class Solution {
public:
    int kthGrammar(int n, int k) {
        return dfs(pow(2, n - 1), k - 1);
    }

private:
    int dfs(int n, int k)
    {
        if (1 == n) {
            return 0;
        }

        int half = n / 2;
        if (k >= half) {
            return (dfs(half, k - half) + 1) % 2;
        }

        return dfs(half, k);
    }
};
