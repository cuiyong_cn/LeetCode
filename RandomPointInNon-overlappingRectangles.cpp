/**
 * Random Point in Non-overlapping Rectangles
 *
 * Given a list of non-overlapping axis-aligned rectangles rects, write a function pick which
 * randomly and uniformily picks an integer point in the space covered by the rectangles.
 *
 * Note:
 *
 * An integer point is a point that has integer coordinates.  A point on the perimeter of a
 * rectangle is included in the space covered by the rectangles.  ith rectangle = rects[i] =
 * [x1,y1,x2,y2]
 where [x1, y1] are the integer coordinates of the bottom-left corner, and [x2, y2] are the integer
 coordinates of the top-right corner.
 * length and width of each rectangle does not exceed 2000.  1 <= rects.length <= 100 pick return a
 * point as an array of integer coordinates [p_x, p_y] pick is called at most 10000 times.
 * Example 1:
 *
 * Input:
 * ["Solution","pick","pick","pick"]
 * [[[[1,1,5,5]]] [][][]]
 * Output:
 * [null,[4,1] [4,1] [3,3]]
 * Example 2:
 *
 * Input:
 * ["Solution","pick","pick","pick","pick","pick"]
 * [[[[-2,-2,-1,-1] [1,0,3,0]]] [] [] [] [] []]
 * Output:
 * [null,[-1,-2]
 * [2,0]
 * [-2,-1]
 * [3,0]
 * [-2,-2]]
 * Explanation of Input Syntax:
 *
 * The input is two lists: the subroutines called and their arguments. Solution's constructor has
 * one argument, the array of rectangles rects. pick has no arguments. Arguments are always wrapped
 * with a list, even if there aren't any.
 */
/**
 * passed version. Idea is that, we should use area weight as criteria to choose the rect.
 */
class Solution {
public:
    Solution(vector<vector<int>>& rects) : rd{}, rand_engine{rd()} {
        int weight = 0;
        for (auto& rect : rects) {
            rect_gen.emplace_back(
                vector{ uniform_int_distribution{rect[0], rect[2]}, uniform_int_distribution{rect[1], rect[3]} });

            weight += (rect[2] - rect[0] + 1) * (rect[3] - rect[1] + 1);
            rect_weight.emplace_back(weight);
        }
        rect_picker = uniform_int_distribution<int>{0, weight};
    }

    vector<int> pick() {
        auto weight = rect_picker(rand_engine) - 1;
        auto rect_idx = std::distance(begin(rect_weight), upper_bound(begin(rect_weight), end(rect_weight), weight));
        return { rect_gen[rect_idx][0](rand_engine),  rect_gen[rect_idx][1](rand_engine) };
    }

private:
    std::random_device rd;
    std::mt19937 rand_engine;
    vector<vector<std::uniform_int_distribution<int>>> rect_gen;
    vector<int> rect_weight;
    uniform_int_distribution<int> rect_picker;
};

/**
 * Your Solution object will be instantiated and called as such:
 * Solution* obj = new Solution(rects);
 * vector<int> param_1 = obj->pick();
 */

/**
 * Original solution. Please note, this is not correct solution.
 */
class Solution {
public:
    Solution(vector<vector<int>>& rects) : rd{}, rand_engine{rd()}, rect_picker{0UL, rects.size() - 1} {
        for (auto& rect : rects) {
            rect_gen.emplace_back(
                vector{ uniform_int_distribution{rect[0], rect[2]}, uniform_int_distribution{rect[1], rect[3]} });
        }
    }

    vector<int> pick() {
        auto rect_idx = rect_picker(rand_engine);
        return { rect_gen[rect_idx][0](rand_engine),  rect_gen[rect_idx][1](rand_engine) };
    }

private:
    std::random_device rd;
    std::mt19937 rand_engine;
    vector<vector<std::uniform_int_distribution<int>>> rect_gen;
    uniform_int_distribution<size_t> rect_picker;
};

/**
 * Your Solution object will be instantiated and called as such:
 * Solution* obj = new Solution(rects);
 * vector<int> param_1 = obj->pick();
 */
