/**
 * Lexicographically Minimum String After Removing Stars
 *
 * You are given a string s. It may contain any number of '*' characters. Your task is to remove all
 * '*' characters.
 *
 * While there is a '*', do the following operation:
 *     Delete the leftmost '*' and the smallest non-'*' character to its left. If there are several
 *     smallest characters, you can delete any of them.
 *
 * Return the lexicographically smallest resulting string after removing all '*' characters.
 *
 * Example 1:
 *
 * Input: s = "aaba*"
 *
 * Output: "aab"
 *
 * Explanation:
 *
 * We should delete one of the 'a' characters with '*'. If we choose s[3], s becomes the lexicographically smallest.
 *
 * Example 2:
 *
 * Input: s = "abc"
 *
 * Output: "abc"
 *
 * Explanation:
 *
 * There is no '*' in the string.
 *
 * Constraints:
 *     1 <= s.length <= 105
 *     s consists only of lowercase English letters and '*'.
 *     The input is generated such that it is possible to delete all '*' characters.
 */
/**
 * Original solution.
 */
class Solution {
public:
    template<typename T>
    using MinHeap = priority_queue<T, vector<T>, greater<T>>;

    string clearStars(string s) {
        auto pq = MinHeap<pair<char, int>>{};

        int const slen = s.length();

        for (auto i = 0; i < slen; ++i) {
            if ('*' == s[i]) {
                s[i] = 0;
                auto [_, li] = pq.top();
                pq.pop();

                s[-li] = 0;
            }
            else {
                pq.emplace(s[i], -i);
            }
        }

        auto ans = string{};

        for (auto c : s) {
            if (0 != c) {
                ans += c;
            }
        }

        return ans;
    }
};
