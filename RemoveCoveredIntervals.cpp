/**
 * Remove Covered Intervals
 *
 * Given a list of intervals, remove all intervals that are covered by another interval in the list. Interval [a,b) is
 * covered by interval [c,d) if and only if c <= a and b <= d.
 *
 * After doing so, return the number of remaining intervals.
 *
 * Example 1:
 *
 * Input: intervals = [[1,4],[3,6],[2,8]]
 * Output: 2
 * Explanation: Interval [3,6] is covered by [2,8], therefore it is removed.
 *
 * Constraints:
 *
 *     1 <= intervals.length <= 1000
 *     0 <= intervals[i][0] < intervals[i][1] <= 10^5
 *     intervals[i] != intervals[j] for all i != j
 */
/**
 * A more clean one. using sort to clarify the intention.
 */
class Solution {
private:
    static bool SortByLeftBound(vector<int>& v1, vector<int>& v2) {
        int a = v1[0], b = v1[1];
        int c = v2[0], d = v2[1];
        bool ret = true;
        if (a > c) {
            ret = false;
        }
        else if (a == c) {
            if (b < d) {
                ret = false;
            }
        }
        return ret;
    }

public:
    int removeCoveredIntervals(vector<vector<int>>& intervals) {
        sort(intervals.begin(), intervals.end(), SortByLeftBound);
        int ans = 0;
        int right = 0;
        for (auto& v : intervals) {
            if (v[1] > right) {
                ++ans;
                right = v[1];
            }
        }
        return ans;
    }
};

/**
 * My original solution. Fast but not very clean.
 */
class Solution {
public:
    int removeCoveredIntervals(vector<vector<int>>& intervals) {
        int j = intervals.size() - 1;
        int i = 0;
        while (i <= j) {
            for (int k = 0; k <= j; ++k) {
                if (k == i) continue;
                int a = intervals[i][0];
                int b = intervals[i][1];
                int c = intervals[k][0];
                int d = intervals[k][1];

                if (c <= a && b <= d) {
                    swap(intervals[i], intervals[j]);
                    --j;
                    k = -1;
                }
                else if (a <= c && d <= b) {
                    swap(intervals[k], intervals[j]);
                    --j;
                    --k;
                }
            }
            ++i;
        }
        return j + 1;
    }
};
