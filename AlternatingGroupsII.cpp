/**
 * Alternating Groups II
 *
 * There is a circle of red and blue tiles. You are given an array of integers colors and an integer
 * k. The color of tile i is represented by colors[i]:
 *     colors[i] == 0 means that tile i is red.
 *     colors[i] == 1 means that tile i is blue.
 *
 * An alternating group is every k contiguous tiles in the circle with alternating colors (each tile
 * in the group except the first and last one has a different color from its left and right tiles).
 *
 * Return the number of alternating groups.
 *
 * Note that since colors represents a circle, the first and the last tiles are considered to be next to each other.
 *
 * Example 1:
 *
 * Input: colors = [0,1,0,1,0], k = 3
 *
 * Output: 3
 *
 * Explanation:
 *
 * Alternating groups:
 *
 * Example 2:
 *
 * Input: colors = [0,1,0,0,1,0,1], k = 6
 *
 * Output: 2
 *
 * Explanation:
 *
 * Alternating groups:
 *
 * Example 3:
 *
 * Input: colors = [1,1,0,1], k = 4
 *
 * Output: 0
 *
 * Explanation:
 *
 * Constraints:
 *     3 <= colors.length <= 105
 *     0 <= colors[i] <= 1
 *     3 <= k <= colors.length
 */
/**
 * Short version.
 */
class Solution {
public:
    int numberOfAlternatingGroups(vector<int>& colors, int k) {
        auto ans = 0;
        auto len = 1;
        auto const size = colors.size();
        auto const end = size + k - 1;

        for (auto i = 1uz; i < end; ++i) {
            if (colors[i % size] == colors[(i - 1) % size]) {
                len = 1;
            }
            else {
                len += 1;
            }

            ans += len >= k ? 1 : 0;
        }

        return ans;
    }
};

/**
 * Original solution.
 */
auto alt_head(vector<int> const& colors)
{
    auto len = 1;
    auto const size = colors.size();
    auto i = 1uz;

    for (; i < size; ++i) {
        if (colors[i] == colors[i - 1]) {
            break;
        }
        len += 1;
    }

    return pair(len, i);
}

auto alt_tail(vector<int> const& colors)
{
    auto len = 1;
    auto const size = colors.size();
    auto i = size - 1;

    for (; i > 0; --i) {
        if (colors[i] == colors[i - 1]) {
            break;
        }
        len += 1;
    }

    return pair(len, i);
}

class Solution {
public:
    int numberOfAlternatingGroups(vector<int>& colors, int k) {
        auto ans = 0;
        auto const size = colors.size();
        auto const [head_len, h] = alt_head(colors);

        if (head_len == size) {
            if (colors.front() != colors.back()) {
                return head_len;
            }
            else {
                return head_len - k + 1;
            }
        }

        auto const [tail_len, t] = alt_tail(colors);
        auto len = 1;

        for (auto i = h; i <= t; ++i) {
            if (colors[i] != colors[i - 1]) {
                len += 1;
            }
            else {
                ans += max(0, len - k + 1);
                len = 1;
            }
        }


        if (colors.front() != colors.back()) {
            if (h <= t) {
                ans += max(0, head_len + tail_len - k + 1);
            }
            else {
                ans += max(0, head_len - k + 1);
            }
        }
        else {
            if (h <= t) {
                ans += max(0, head_len - k + 1) + max(0, tail_len - k + 1);
            }
            else {
                ans += max(0, head_len - k + 1);
            }
        }

        return ans;
    }
};
