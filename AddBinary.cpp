/**
 * Add Binary
 *
 * Given two binary strings, return their sum (also a binary string).
 *
 * The input strings are both non-empty and contains only characters 1 or 0.
 *
 * Example 1:
 *
 * Input: a = "11", b = "1"
 * Output: "100"
 * Example 2:
 *
 * Input: a = "1010", b = "1011"
 * Output: "10101"
 *
 * Constraints:
 *
 * Each string consists only of '0' or '1' characters.
 * 1 <= a.length, b.length <= 10^4
 * Each string is either "0" or doesn't contain any leading zero.
 */
/**
 * Improve the code
 */
class Solution {
public:
    string addBinary(string a, string b) {
        int i = a.length() - 1;
        int j = b.length() - 1;
        int carry = 0;

        string ans;
        while (i >= 0 || j >= 0) {
            int sum = (i >= 0 ? a[i] - '0' : 0)
                + (j >= 0 ? b[j] - '0' : 0) + carry;
            ans += '0' + sum % 2;
            carry = sum > 1;
            --i; --j;
        }

        if (carry > 0) {
            ans += '1';
        }

        std::reverse(ans.begin(), ans.end());

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    string addBinary(string a, string b) {
        int i = a.length() - 1;
        int j = b.length() - 1;
        int carry = 0;

        string ans;
        while (i >= 0 && j >= 0) {
            int sum = a[i] - '0' + b[j] - '0' + carry;
            ans += '0' + (sum > 1 ? sum - 2 : sum);
            carry = sum > 1;
            --i; --j;
        }

        while (i >= 0) {
            int sum = a[i] - '0' + carry;
            ans += '0' + (sum > 1 ? sum - 2 : sum);
            carry = sum > 1;
            --i;
        }

        while (j >= 0) {
            int sum = b[j] - '0' + carry;
            ans += '0' + (sum > 1 ? sum - 2 : sum);
            carry = sum > 1;
            --j;
        }

        if (carry > 0) {
            ans += '1';
        }

        std::reverse(ans.begin(), ans.end());

        return ans;
    }
};
