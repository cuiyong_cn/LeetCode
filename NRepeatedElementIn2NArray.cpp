/**
 * N-Repeated Element in Size 2N Array
 *
 * In a array A of size 2N, there are N+1 unique elements, and exactly one of
 * these elements is repeated N times.
 * 
 * Return the element repeated N times.
 * 
 * Example 1:
 * 
 * Input: [1,2,3,3]
 * Output: 3
 * 
 * Example 2:
 * 
 * Input: [2,1,2,5,3,2]
 * Output: 2
 * 
 * Example 3:
 * 
 * Input: [5,1,5,2,5,3,5,4]
 * Output: 5
 * 
 * Note:
 * 
 *     1. 4 <= A.length <= 10000
 *     2. 0 <= A[i] < 10000
 *     3. A.length is even
 */
/**
 * Really intresting random picker
 *
 *  int repeatedNTimes(vector<int>& A) {
 *      int i = 0, j = 0, n = A.size();
 *      while (i == j || A[i] != A[j])
 *          i = rand() % n, j = rand() % n;
 *      return A[i];
 *  }
 */
/**
 *  search in radius 3 subarray
 *
 *  int repeatedNTimes(vector<int>& A) {
 *       for (int i = 2; i < A.size(); ++i)
 *       if (A[i] == A[i - 1] || A[i] == A[i - 2])
 *           return A[i];
 *       return A[0];
 *  }
 */
class Solution {
public:
    int repeatedNTimes(vector<int>& A) {
        int N = A.size() / 2 + 1;
        unordered_set<int> set;
        int i = 0;
        for (; i < N; ++i) {
            if (set.count(A[i]) == 0) {
                set.insert(A[i]);
            }
            else {
               break;
            }
        }
        
        return A[i];
    }
};
