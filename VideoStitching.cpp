/**
 * Video Stitching
 *
 * You are given a series of video clips from a sporting event that lasted T seconds.  These video clips can be
 * overlapping with each other and have varied lengths.
 *
 * Each video clip clips[i] is an interval: it starts at time clips[i][0] and ends at time clips[i][1].  We can cut
 * these clips into segments freely: for example, a clip [0, 7] can be cut into segments [0, 1] + [1, 3] + [3, 7].
 *
 * Return the minimum number of clips needed so that we can cut the clips into segments that cover the entire sporting
 * event ([0, T]).  If the task is impossible, return -1.
 *
 * Example 1:
 *
 * Input: clips = [[0,2],[4,6],[8,10],[1,9],[1,5],[5,9]], T = 10
 * Output: 3
 * Explanation:
 * We take the clips [0,2], [8,10], [1,9]; a total of 3 clips.
 * Then, we can reconstruct the sporting event as follows:
 * We cut [1,9] into segments [1,2] + [2,8] + [8,9].
 * Now we have segments [0,2] + [2,8] + [8,10] which cover the sporting event [0, 10].
 *
 * Example 2:
 *
 * Input: clips = [[0,1],[1,2]], T = 5
 * Output: -1
 * Explanation:
 * We can't cover [0,5] with only [0,1] and [1,2].
 *
 * Example 3:
 *
 * Input: clips = [[0,1],[6,8],[0,2],[5,6],[0,4],[0,3],[6,7],[1,3],[4,7],[1,4],[2,5],[2,6],[3,4],[4,5],[5,7],[6,9]], T = 9
 * Output: 3
 * Explanation:
 * We can take clips [0,4], [4,7], and [6,9].
 *
 * Example 4:
 *
 * Input: clips = [[0,4],[2,8]], T = 5
 * Output: 2
 * Explanation:
 * Notice you can have extra video after the event ends.
 *
 * Constraints:
 *     1 <= clips.length <= 100
 *     0 <= clips[i][0] <= clips[i][1] <= 100
 *     0 <= T <= 100
 */
/**
 * Also from discussion. DP solution.
 */
class Solution
{
public:
    int videoStitching(vector<vector<int>>& clips, int T)
    {
        sort(clips.begin(), clips.end());
        vector<int> dp(101, 101);
        dp[0] = 0;
        for (auto& c : clips)
            for (int i = c[0] + 1; i <= c[1]; i++)
                dp[i] = min(dp[i], dp[c[0]] + 1);
        return dp[T] >= 100 ? -1 : dp[T];
    }
};

/**
 * Solution from discussion. Greedy method. Basic idea is:
 *      From all the overlapping clips, we pick the one that
 *      advances furthest.
 */
class Solution {
public:
    int videoStitching(vector<vector<int>>& clips, int T) {
        sort(begin(clips), end(clips));

        int res = 0;
        for (auto i = 0, st = 0, end = 0; st < T; st = end, ++res) {
            for (; i < clips.size() && clips[i][0] <= st; ++i)
                end = max(end, clips[i][1]);
            if (st == end) return -1;
        }
        return res;
    }
};

/**
 * Original solution. using dfs and memo. somehow inefficient.
 */
class Solution {
public:
    int videoStitching(vector<vector<int>>& clips, int T) {
        vector<vector<int>> time(101);
        std::array<int, 101> memo;
        memo.fill(0);

        sort(clips.begin(), clips.end(), [](auto& c1, auto& c2) {
            if (c1[0] < c2[0]) return true;
            if (c1[0] == c2[0]) return c1[1] <= c2[1];

            return false;
        });

        for (auto& c : clips) time[c[0]].push_back(c[1]);

        int ans = dfs(time, memo, 0, T);

        return ans == 101 ? -1 : ans;
    }

private:
    int dfs(const vector<vector<int>>& time, std::array<int, 101>& memo, int s, int e)
    {
        if (time[s].empty()) return 101;
        if (memo[s]) return memo[s];

        int ans = 101;
        for (auto t : time[s]) {
            if (t >= e) return 1;

            for (int ns = s + 1; ns <= t; ++ns) {
                ans = std::min(ans, 1 + dfs(time, memo, ns, e));
            }
        }

        memo[s] = ans;
        return ans;
    }
};
