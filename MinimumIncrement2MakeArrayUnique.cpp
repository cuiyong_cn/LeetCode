/**
 * Minimum Increment to Make Array Unique
 *
 * Given an array of integers A, a move consists of choosing any A[i], and incrementing it by 1.
 *
 * Return the least number of moves to make every value in A unique.
 *
 * Example 1:
 *
 * Input: [1,2,2]
 * Output: 1
 * Explanation:  After 1 move, the array could be [1, 2, 3].
 * Example 2:
 *
 * Input: [3,2,1,2,1,7]
 * Output: 6
 * Explanation:  After 6 moves, the array could be [3, 4, 1, 2, 5, 7].
 * It can be shown with 5 or less moves that it is impossible for the array to have all unique values.
 *
 * Note:
 *
 * 0 <= A.length <= 40000
 * 0 <= A[i] < 40000
 */
/**
 * same idea with different implementation.
 */
class Solution {
public:
    int minIncrementForUnique(vector<int>& A) {
        int s = A.size();
        int res=0;
        if (s<2)  return 0;
        sort(A.begin(),A.end());

        for (int i=1; i<s; ++i) {
            if (A[i] <= A[i-1]){
                res += A[i-1] - A[i] + 1;
                A[i] = A[i-1] + 1;
            }
        }
        return res;
    }
};

/**
 * Another interresting solution. Not efficient though.
 */
class Solution {
public:
    unordered_map<int, int> root;
    int minIncrementForUnique(vector<int>& A) {
        int res = 0;
        for (int a : A)
            res += find(a) - a;
        return res;
    }

    int find(int x) {
        return root[x] = root.count(x) ? find(root[x] + 1) : x;
    }
};

/**
 * Same as below solution. But make some case faster.
 */
class Solution {
public:
    int minIncrementForUnique(vector<int>& A) {
        map<int,int> count;
        for (int a : A) count[a]++;
        int res = 0, need = 0;
        for (auto x: count) {
            res += x.second * max(need - x.first, 0) + x.second * (x.second - 1) / 2;
            need = max(need, x.first) + x.second;
        }
        return res;
    }
};

/**
 * Really interesting solution. Deep down, it is the same with original but with different thinking process.
 *
 * need: we need "need" number to fill. if need < a, it mean we don't need to increment a to get a unique number.
 * if need > a, mean we need to increment a (need - a) step to get need to make another unique number.
 */
class Solution {
public:
    int minIncrementForUnique(vector<int>& A) {
        sort(A.begin(), A.end());
        int res = 0, need = 0;
        for (int a: A) {
            res += max(need - a, 0);
            need = max(a, need) + 1;
        }
        return res;
    }
};

/**
 * Counting method written in java
 */
class Solution {
    public int minIncrementForUnique(int[] A) {
        int[] count = new int[100000];
        for (int x: A) count[x]++;

        int ans = 0, taken = 0;

        for (int x = 0; x < 100000; ++x) {
            if (count[x] >= 2) {
                taken += count[x] - 1;
                ans -= x * (count[x] - 1);
            }
            else if (taken > 0 && count[x] == 0) {
                taken--;
                ans += x;
            }
        }

        return ans;
    }
}

/**
 * Same idea with different implementation.
 */
class Solution {
public:
    int minIncrementForUnique(vector<int>& A) {
        sort(std::begin(A), std::end(A));

        int ans = 0;
        int dup_count = 0;
        for (int i = 1; i < A.size(); ++i) {
            if (A[i - 1] == A[i]) {
                ++dup_count;
                ans -= A[i];
            }
            else {
                int blanks = std::min(dup_count, A[i] - A[i - 1] - 1);
                ans += blanks * A[i - 1] + blanks * (blanks + 1) / 2;
                dup_count -= blanks;
            }
        }

        if (dup_count > 0) {
            ans += dup_count * A.back() + dup_count * (dup_count + 1) / 2;
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int minIncrementForUnique(vector<int>& A) {
        int ans = 0;
        sort(std::begin(A), std::end(A));

        std::pair<int, int> cur = {0 , 0};
        int i = 0;
        while (i < A.size()) {
            if (A[i] > cur.first) {
                if (cur.second > 1) {
                    ans += cur.second - 1;
                    cur = { cur.first + 1, cur.second - 1 };
                }
                else {
                    cur = get_pair(A, i);
                    i += cur.second;
                }
            }
            else if (A[i] == cur.first) {
                auto p = get_pair(A, i);
                i += p.second;
                cur = { cur.first, p.second + cur.second };
            }
        }

        if (cur.second > 1) {
            ans += cur.second * (cur.second - 1) / 2;
        }

        return ans;
    }

private:
    std::pair<int, int> get_pair(const vector<int>& A, int start)
    {
        int value = A[start];
        int count = 1;

        for (int i = start + 1; i < A.size(); ++i) {
            if (value != A[i]) {
                break;
            }
            ++count;
        }

        return { value, count };
    }
};
