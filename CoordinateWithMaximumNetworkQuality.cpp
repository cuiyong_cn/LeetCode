/**
 * Coordinate With Maximum Network Quality
 *
 * You are given an array of network towers towers, where towers[i] = [xi, yi, qi] denotes the ith
 * network tower with location (xi, yi) and quality factor qi. All the coordinates are integral
 * coordinates on the X-Y plane, and the distance between the two coordinates is the Euclidean
 * distance.
 *
 * You are also given an integer radius where a tower is reachable if the distance is less than or
 * equal to radius. Outside that distance, the signal becomes garbled, and the tower is not
 * reachable.
 *
 * The signal quality of the ith tower at a coordinate (x, y) is calculated with the formula ⌊qi /
 * (1 + d)⌋, where d is the distance between the tower and the coordinate. The network quality at a
 * coordinate is the sum of the signal qualities from all the reachable towers.
 *
 * Return the array [cx, cy] representing the integral coordinate (cx, cy) where the network quality
 * is maximum. If there are multiple coordinates with the same network quality, return the
 * lexicographically minimum non-negative coordinate.
 *
 * Note:
 *
 * A coordinate (x1, y1) is lexicographically smaller than (x2, y2) if either:
 * x1 < x2, or
 * x1 == x2 and y1 < y2.
 * ⌊val⌋ is the greatest integer less than or equal to val (the floor function).
 *
 * Example 1:
 *
 * Input: towers = [[1,2,5],[2,1,7],[3,1,9]], radius = 2
 * Output: [2,1]
 * Explanation: At coordinate (2, 1) the total quality is 13.
 * - Quality of 7 from (2, 1) results in ⌊7 / (1 + sqrt(0)⌋ = ⌊7⌋ = 7
 * - Quality of 5 from (1, 2) results in ⌊5 / (1 + sqrt(2)⌋ = ⌊2.07⌋ = 2
 * - Quality of 9 from (3, 1) results in ⌊9 / (1 + sqrt(1)⌋ = ⌊4.5⌋ = 4
 * No other coordinate has a higher network quality.
 * Example 2:
 *
 * Input: towers = [[23,11,21]], radius = 9
 * Output: [23,11]
 * Explanation: Since there is only one tower, the network quality is highest right at the tower's location.
 * Example 3:
 *
 * Input: towers = [[1,2,13],[2,1,7],[0,1,9]], radius = 2
 * Output: [1,2]
 * Explanation: Coordinate (1, 2) has the highest network quality.
 *
 * Constraints:
 *
 * 1 <= towers.length <= 50
 * towers[i].length == 3
 * 0 <= xi, yi, qi <= 50
 * 1 <= radius <= 50
 */
/**
 * BFS solution.
 */
class Solution {
public:
    vector<int> bestCoordinate(vector<vector<int>>& towers, int radius) {
        int best_quality = 0;
        int const R2 = radius * radius;
        auto ans = make_pair(0, 0);
        deque<pair<int, int>> q;

        for (auto const& t : towers) {
            q.emplace_back(t[0], t[1]);
        }

        while (!q.empty()){
            auto [x, y] = q.front(); q.pop_front();

            int quality = 0;

            for (auto const& t : towers) {
                int dx = x - t[0];
                int dy = y - t[1];
                int distance = dx * dx + dy * dy;
                if (distance <= R2) {
                    quality += floor(t[2] / (1 + sqrt(distance * 1.0)));
                }
            }

            if (quality > best_quality) {
                best_quality = quality;

                ans = {x, y};

                q.emplace_back(x - 1, y);
                q.emplace_back(x + 1, y);
                q.emplace_back(x, y + 1);
                q.emplace_back(x, y - 1);
            }
            else if (quality == best_quality) {
                auto new_coord = make_pair(x, y);
                if (new_coord < ans) {
                    ans = new_coord;
                }
            }
        }

        return {ans.first, ans.second};
    }
};

/**
 * Sliding window solution.
 */
class Solution {
public:
    vector<int> bestCoordinate(vector<vector<int>>& t, int radius) {
        sort(begin(t), end(t));
        deque<int> reach_x;
        int tid = 0, max_x = 0, max_y = 0, max_q = 0;
        for (int x = t.front()[0]; x <= t.back()[0]; ++x) {
            while (tid < t.size() && x + radius >= t[tid][0])
                reach_x.push_back(tid++);
            while (!reach_x.empty() && t[reach_x.front()][0] < x - radius)
                reach_x.pop_front();
            for (int y = 0; y <= 50; ++y) {
                int q = 0;
                for (auto id : reach_x) {
                    if (t[id][1] < y - radius || t[id][1] > y + radius)
                        continue;
                    auto dd = (t[id][0] - x) * (t[id][0] - x) + (t[id][1] - y) * (t[id][1] - y);
                    if (dd <= radius * radius)
                        q += t[id][2] / (1 + sqrt(dd));
                }
                if (max_q < q) {
                    max_q = q;
                    max_x = x;
                    max_y = y;
                }
            }
        }
        return {max_x, max_y};
    }
};

/**
 * Original brute force solution.
 */
class Solution {
public:
    vector<int> bestCoordinate(vector<vector<int>>& towers, int radius) {
        int const MX = 101;
        int const MY = 101;
        int const R2 = radius * radius;
        auto coordinate = make_pair(0, 0);
        int best_quality = 0;
        for (int x = 0; x < MX; ++x) {
            for (int y = 0; y < MY; ++y) {
                int quality = 0;
                for (auto const& t : towers) {
                    int dx = x - t[0];
                    int dy = y - t[1];
                    int distance = dx * dx + dy * dy;
                    if (distance <= R2) {
                        quality += floor(t[2] / (1 + sqrt(distance * 1.0)));
                    }
                }

                if (quality > best_quality) {
                    coordinate = {x, y};
                    best_quality = quality;
                }
            }
        }

        return {coordinate.first, coordinate.second};
    }
};
