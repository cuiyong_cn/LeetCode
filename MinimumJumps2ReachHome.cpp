/**
 * Minimum Jumps to Reach Home
 *
 * A certain bug's home is on the x-axis at position x. Help them get there from position 0.
 *
 * The bug jumps according to the following rules:
 *
 * It can jump exactly a positions forward (to the right).
 * It can jump exactly b positions backward (to the left).
 * It cannot jump backward twice in a row.
 * It cannot jump to any forbidden positions.
 * The bug may jump forward beyond its home, but it cannot jump to positions numbered with negative
 * integers.
 *
 * Given an array of integers forbidden, where forbidden[i] means that the bug cannot jump to the
 * position forbidden[i], and integers a, b, and x, return the minimum number of jumps needed for
 * the bug to reach its home. If there is no possible sequence of jumps that lands the bug on
 * position x, return -1.
 *
 * Example 1:
 *
 * Input: forbidden = [14,4,18,1,15], a = 3, b = 15, x = 9
 * Output: 3
 * Explanation: 3 jumps forward (0 -> 3 -> 6 -> 9) will get the bug home.
 * Example 2:
 *
 * Input: forbidden = [8,3,16,6,12,20], a = 15, b = 13, x = 11
 * Output: -1
 * Example 3:
 *
 * Input: forbidden = [1,6,2,14,5,17,4], a = 16, b = 9, x = 7
 * Output: 2
 * Explanation: One jump forward (0 -> 16) then one jump backward (16 -> 7) will get the bug home.
 *
 * Constraints:
 *
 * 1 <= forbidden.length <= 1000
 * 1 <= a, b, forbidden[i] <= 2000
 * 0 <= x <= 2000
 * All the elements in forbidden are distinct.
 * Position x is not forbidden.
 */
/**
 * Passed solution. There is a proof about the stop condition.
 *
 * Also the original two solution are wrong because I think we don't need to
 * care from which direction we arrive at position x. But in fact this is the
 * key point of this problem. I spend a long time to digest this one.
 */
class Solution {
public:
    int minimumJumps(vector<int>& forbidden, int a, int b, int x) {
        auto const max_forbid = *max_element(forbidden.begin(), forbidden.end());
        auto const stop = a + b + max(x, max_forbid);

        int jumps = 0;

        unordered_set<int> forward_visited;
        unordered_set<int> back_visited;
        for (auto f : forbidden) {
            forward_visited.insert(f);
            back_visited.insert(f);
        }

        deque<pair<int, bool>> deq = { {0, false} };
        while (!deq.empty()) {
            int const QS = deq.size();
            for (auto i = 0; i < QS; ++i) {
                auto const [pos, canGoBack] = deq.front(); deq.pop_front();
                if (pos == x) {
                    return jumps;
                }

                if (canGoBack) {
                    auto back_pos = pos - b;
                    if (back_pos > 0 && 0 == back_visited.count(back_pos)) {
                        back_visited.insert(back_pos);
                        deq.emplace_back(back_pos, false);
                    }
                }

                auto forward_pos = pos + a;
                if (forward_pos <= stop && 0 == forward_visited.count(forward_pos)) {
                    forward_visited.insert(forward_pos);
                    deq.emplace_back(forward_pos, true);
                }
            }

            ++jumps;
        }

        return -1;
    }
};

/**
 * The original two solution that came up to me turned out to be failures.
 */
