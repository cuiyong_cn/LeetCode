/**
 * Minimum Swaps to Make Strings Equal
 *
 * You are given two strings s1 and s2 of equal length consisting of letters "x" and "y"
 * only. Your task is to make these two strings equal to each other. You can swap any two
 * characters that belong to different strings, which means: swap s1[i] and s2[j].
 *
 * Return the minimum number of swaps required to make s1 and s2 equal, or return -1 if
 * it is impossible to do so.
 *
 * Example 1:
 *
 * Input: s1 = "xx", s2 = "yy"
 * Output: 1
 * Explanation:
 * Swap s1[0] and s2[1], s1 = "yx", s2 = "yx".
 *
 * Example 2:
 *
 * Input: s1 = "xy", s2 = "yx"
 * Output: 2
 * Explanation:
 * Swap s1[0] and s2[0], s1 = "yy", s2 = "xx".
 * Swap s1[0] and s2[1], s1 = "xy", s2 = "xy".
 * Note that you can't swap s1[0] and s1[1] to make s1 equal to "yx", cause we can only
 * swap chars in different strings.
 *
 * Example 3:
 *
 * Input: s1 = "xx", s2 = "xy"
 * Output: -1
 *
 * Example 4:
 *
 * Input: s1 = "xxyyxyxyxx", s2 = "xyyxyxxxyx"
 * Output: 4
 *
 * Constraints:
 *
 *     1 <= s1.length, s2.length <= 1000
 *     s1, s2 only contain 'x' or 'y'.
 */
/**
 * As it turns out, we only need two variables to track, because if it can be repaired
 * it is a mirror situation. Improved version.
 */
class Solution {
public:
    int minimumSwap(string s1, string s2) {
        int ans = -1;
        vector<int> unmatched(2, 0);
        for (int i = 0; i < s1.size(); ++i) {
            if (s1[i] != s2[i]) {
                if ('x' == s1[i]) {
                    ++unmatched[0];
                }
                else {
                    ++unmatched[1];
                }
            }
        }

        if (((unmatched[0] + unmatched[1]) & 1) == 0) {
            ans = unmatched[0] / 2 + unmatched[1] / 2;
            ans += unmatched[0] & 1 ? 2 : 0;
        }

        return ans;
    }
};
/**
 * My original solution. Though it is correct, and fast enough. But seems chatty.
 */
class Solution {
public:
    int minimumSwap(string s1, string s2) {
        vector<vector<int> > unmatched(2, vector<int>(2, 0));
        for (int i = 0; i < s1.size(); ++i) {
            if (s1[i] != s2[i]) {
                if (s1[i] == 'x') {
                    ++unmatched[0][0];
                    ++unmatched[1][1];
                }
                else {
                    ++unmatched[0][1];
                    ++unmatched[1][0];
                }
            }
        }

        int ans = -1;
        int xCount = unmatched[0][0] + unmatched[1][0];
        int yCount = unmatched[0][1] + unmatched[1][1];
        if (0 == (xCount & 1)) {
            ans = 0;
            while (unmatched[0][0] >= 2) {
                ++ans;
                unmatched[0][0] -= 2;
            }
            while (unmatched[0][1] >= 2) {
                ++ans;
                unmatched[0][1] -= 2;
            }
            if (unmatched[0][0] == 1) ans += 2;
        }
        return ans;
    }
};
