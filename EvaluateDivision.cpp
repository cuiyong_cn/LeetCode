/**
 * Evaluate Division
 *
 * Equations are given in the format A / B = k, where A and B are variables represented as strings, and k is a real
 * number (floating point number). Given some queries, return the answers. If the answer does not exist, return -1.0.
 *
 * Example:
 * Given a / b = 2.0, b / c = 3.0.
 * queries are: a / c = ?, b / a = ?, a / e = ?, a / a = ?, x / x = ? .
 * return [6.0, 0.5, -1.0, 1.0, -1.0 ].
 *
 * The input is: vector<pair<string, string>> equations, vector<double>& values, vector<pair<string, string>> queries ,
 * where equations.size() == values.size(), and the values are positive. This represents the equations. Return
 * vector<double>.
 *
 * According to the example above:
 *
 * equations = [ ["a", "b"], ["b", "c"] ],
 * values = [2.0, 3.0],
 * queries = [ ["a", "c"], ["b", "a"], ["a", "e"], ["a", "a"], ["x", "x"] ].
 *
 * The input is always valid. You may assume that evaluating the queries will result in no division by zero and there is
 * no contradiction.
 */
/**
 * Original solution. This one is a graph problem.
 * We could also using the union find method.
 */
class Solution {
public:
    vector<double> calcEquation(vector<vector<string>>& equations, vector<double>& values, vector<vector<string>>& queries) {
        /**
         * Construct the graph
         */
        for (size_t i = 0; i < equations.size(); ++i) {
            G[equations[i][0]].push_back({ equations[i][1], values[i] });
            G[equations[i][1]].push_back({ equations[i][0], 1.0 / values[i] });
            vars.insert(equations[i][0]);
            vars.insert(equations[i][1]);
        }

        vector<double> ans(queries.size(), 0);

        /**
         * Compute the expression
         */
        for (size_t i = 0; i < queries.size(); ++i) {
            ans[i] = connect(queries[i]);
        }

        return ans;
    }
private:
    double connect(const vector<string>& qry)
    {
        if (vars.count(qry[0]) == 0 || vars.count(qry[1]) == 0) return -1.0;
        if (qry[0] == qry[1]) return 1.0;

        string s = qry[0];
        string e = qry[1];

        deque< pair<string, double> > q;
        double ans = -1.0;

        for (auto& p : G[s]) {
            q.push_back(p);
        }

        while (!q.empty()) {
            auto p = q.front();
            q.pop_front();
            if (p.first == e) {
                ans = p.second;
                break;
            }

            if (p.first == s) continue;

            auto it = G.find(p.first);
            if (it != G.end()) {
                for (auto& d : it->second) {
                    q.push_back({d.first, d.second * p.second});
                }
            }
        }

        return ans;
    }

private:
    unordered_set<string> vars;
    unordered_map<string, vector< pair<string, double> > > G;
};
