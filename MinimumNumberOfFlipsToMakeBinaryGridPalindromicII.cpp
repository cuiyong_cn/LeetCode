/**
 * Minimum Number of Flips to Make Binary Grid Palindromic II
 *
 * You are given an m x n binary matrix grid.
 *
 * A row or column is considered palindromic if its values read the same forward and backward.
 *
 * You can flip any number of cells in grid from 0 to 1, or from 1 to 0.
 *
 * Return the minimum number of cells that need to be flipped to make all rows and columns
 * palindromic, and the total number of 1's in grid divisible by 4.
 *
 * Example 1:
 *
 * Input: grid = [[1,0,0],[0,1,0],[0,0,1]]
 *
 * Output: 3
 *
 * Explanation:
 *
 * Example 2:
 *
 * Input: grid = [[0,1],[0,1],[0,0]]
 *
 * Output: 2
 *
 * Explanation:
 *
 * Example 3:
 *
 * Input: grid = [[1],[1]]
 *
 * Output: 2
 *
 * Explanation:
 *
 * Constraints:
 *     m == grid.length
 *     n == grid[i].length
 *     1 <= m * n <= 2 * 105
 *     0 <= grid[i][j] <= 1
 */
/**
 * Original solution.
 */
auto odd_number(int n)
{
    return 1 == (n % 2);
}

class Solution {
public:
    int minFlips(vector<vector<int>>& grid) {
        int const m = grid.size();
        int const n = grid[0].size();
        int const mid_r = m / 2;
        int const mid_c = n / 2;
        auto ans = 0;

        for (auto r = 0; r < mid_r; ++r) {
            for (auto c = 0; c < mid_c; ++c) {
                auto cc = n - 1 - c;
                auto cr = m - 1 - r;
                auto sum = grid[r][c] + grid[r][cc] + grid[cr][c] + grid[cr][cc];
                ans += min(sum, 4 - sum);
            }
        }

        auto [cnt2, cnt1] = tuple(0, 0);

        if (odd_number(n)) {
            for (auto r = 0; r < mid_r; ++r) {
                auto cr = m - 1 - r;
                if (grid[r][mid_c] == grid[cr][mid_c]) {
                    cnt2 += 1 == grid[r][mid_c];
                }
                else {
                    ++cnt1;
                }
            }
        }


        if (odd_number(m)) {
            for (auto c = 0; c < mid_c; ++c) {
                auto cc = n - 1 - c;
                if (grid[mid_r][c] == grid[mid_r][cc]) {
                    cnt2 += 1 == grid[mid_r][c];
                }
                else {
                    ++cnt1;
                }
            }
        }

        cnt2 = cnt2 % 2;
        if (cnt2 > 0) {
            ans += cnt1 > 0 ? cnt1 : 2;
        }
        else {
            ans += cnt1;
        }

        if (odd_number(m) && odd_number(n)) {
            ans += grid[mid_r][mid_c];
        }

        return ans;
    }
};
