/**
 * Score of Parentheses
 *
 * Given a balanced parentheses string S, compute the score of the string based on the
 * following rule:
 *
 *     () has score 1
 *     AB has score A + B, where A and B are balanced parentheses strings.
 *     (A) has score 2 * A, where A is a balanced parentheses string.
 *
 * Example 1:
 *
 * Input: "()"
 * Output: 1
 *
 * Example 2:
 *
 * Input: "(())"
 * Output: 2
 *
 * Example 3:
 *
 * Input: "()()"
 * Output: 2
 *
 * Example 4:
 *
 * Input: "(()(()))"
 * Output: 6
 *
 * Note:
 *
 *     S is a balanced parentheses string, containing only ( and ).
 *     2 <= S.length <= 50
 */
/**
 * Most intuitive one. Use the formular: x * (u + v) = x * u + x * v
 */
class Solution {
public:
    int scoreOfParentheses(string S) {
        int ans = 0;
        int bal = 0;
        for (int i = 0; i < S.size(); ++i) {
            if ('(' == S[i]) {
                ++bal;
            }
            else {
                --bal;
                if ('(' == S[i - 1]) {
                    ans += 1 << bal;
                }
            }
        }
        return ans;
    }
};

/**
 * More efficient version. Use stack. Every () at depth n, we push a corresponding score
 * on to the stack.
 */
class Solution {
public:
    int scoreOfParentheses(string S) {
        stack<int> stk;
        stk.push(0);
        for (auto& c : S) {
            if ('(' == c) {
                stk.push(0);
            }
            else {
                int v = stk.top(); stk.pop();
                int w = stk.top(); stk.pop();
                stk.push(w + max(2 * v, 1));
            }
        }
        return stk.top();
    }
};
/**
 * Intuitive and simple
 */
class Solution {
private:
    int F(string& s, int i, int j) {
        int ans = 0;
        int bal = 0;

        for (int k = i; k < j; ++k) {
            bal += '(' == s[k] ? 1 : -1;
            if (bal == 0) {
                if ((k - i) == 1) ++ans;
                else ans += 2 * F(s, i + 1, k);
                i = k + 1;
            }
        }
        return ans;
    }
public:
    int scoreOfParentheses(string S) {
        return F(S, 0, S.size());
    }
};
