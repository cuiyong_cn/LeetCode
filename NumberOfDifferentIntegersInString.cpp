/**
 * Number of Different Integers in a String
 *
 * You are given a string word that consists of digits and lowercase English letters.
 *
 * You will replace every non-digit character with a space. For example, "a123bc34d8ef34" will
 * become " 123  34 8  34". Notice that you are left with some integers that are separated by at
 * least one space: "123", "34", "8", and "34".
 *
 * Return the number of different integers after performing the replacement operations on word.
 *
 * Two integers are considered different if their decimal representations without any leading zeros
 * are different.
 *
 * Example 1:
 *
 * Input: word = "a123bc34d8ef34"
 * Output: 3
 * Explanation: The three different integers are "123", "34", and "8". Notice that "34" is only counted once.
 * Example 2:
 *
 * Input: word = "leet1234code234"
 * Output: 2
 * Example 3:
 *
 * Input: word = "a1b01c001"
 * Output: 1
 * Explanation: The three integers "1", "01", and "001" all represent the same integer because
 * the leading zeros are ignored when comparing their decimal values.
 *
 * Constraints:
 *
 * 1 <= word.length <= 1000
 * word consists of digits and lowercase English letters.
 */
/**
 * More clear.
 */

class Solution {
public:
    int numDifferentIntegers(string word) {
        unordered_set<string> nums;

        size_t left = 0, right = 0;
        int const len = word.length();
        string const digits = "0123456789";

        while (right < len) {
            left = word.find_first_of(digits, left);
            if (string::npos == left) {
                break;
            }

            left = word.find_first_not_of('0', left);
            if (string::npos == left) {
                left = len;
            }

            right = word.find_first_not_of(digits, left);
            if (string::npos == right) {
                right = len;
            }

            nums.insert(word.substr(left, right - left));
            left = right;
        }

        return nums.size();
    }
};

/**
 * more efficient one.
 */
class Solution {
public:
    int numDifferentIntegers(string word) {
        unordered_set<string> nums;
        int i = 0;
        int const len = word.length();
        string const digits = "0123456789";

        while (i < len) {
            auto left = word.find_first_of(digits, i);
            if (string::npos == left) {
                break;
            }

            auto right = word.find_first_not_of(digits, left);
            if (string::npos == right) {
                right = len;
            }

            for (; left < right; ++left) {
                if ('0' != word[left]) {
                    break;
                }
            }

            nums.insert(word.substr(left, right - left));

            i = right;
        }

        return nums.size();
    }
};

/**
 * Original solution.
 */
string remove_head_zero(string const& s)
{
    auto idx = s.find_first_not_of('0');
    if (string::npos == idx) {
        return "0";
    }

    return s.substr(idx);
}
class Solution {
public:
    int numDifferentIntegers(string word) {
        replace_if(word.begin(), word.end(), ::isalpha, ' ');

        stringstream ss{word};
        unordered_set<string> nums;
        string num;

        while (ss >> num) {
            nums.insert(remove_head_zero(num));
        }

        return nums.size();
    }
};
