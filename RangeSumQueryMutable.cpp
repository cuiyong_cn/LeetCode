/**
 * Range Sum Query - Mutable
 *
 * Given an integer array nums, handle multiple queries of the following types:
 *
 * Update the value of an element in nums.
 * Calculate the sum of the elements of nums between indices left and right inclusive where left <=
 * right.  Implement the NumArray class:
 *
 * NumArray(int[] nums) Initializes the object with the integer array nums.  void update(int index,
 * int val) Updates the value of nums[index] to be val.  int sumRange(int left, int right) Returns
 * the sum of the elements of nums between indices left and right inclusive (i.e. nums[left] +
 * nums[left + 1] + ... + nums[right]).
 *
 * Example 1:
 *
 * Input
 * ["NumArray", "sumRange", "update", "sumRange"]
 * [[[1, 3, 5]], [0, 2], [1, 2], [0, 2]]
 * Output
 * [null, 9, null, 8]
 *
 * Explanation
 * NumArray numArray = new NumArray([1, 3, 5]);
 * numArray.sumRange(0, 2); // return 1 + 3 + 5 = 9
 * numArray.update(1, 2);   // nums = [1, 2, 5]
 * numArray.sumRange(0, 2); // return 1 + 2 + 5 = 8
 *
 * Constraints:
 *
 * 1 <= nums.length <= 3 * 104
 * -100 <= nums[i] <= 100
 * 0 <= index < nums.length
 * -100 <= val <= 100
 * 0 <= left <= right < nums.length
 * At most 3 * 104 calls will be made to update and sumRange.
 */
/**
 * Segment Tree
 */
struct SegmentTreeNode {
    int start, end, sum;
    SegmentTreeNode* left;
    SegmentTreeNode* right;
    SegmentTreeNode(int a, int b):start(a),end(b),sum(0),left(nullptr),right(nullptr){}
};
class NumArray {
    SegmentTreeNode* root;
public:
    NumArray(vector<int> &nums) {
        int n = nums.size();
        root = buildTree(nums,0,n-1);
    }
   
    void update(int i, int val) {
        modifyTree(i,val,root);
    }

    int sumRange(int i, int j) {
        return queryTree(i, j, root);
    }
    SegmentTreeNode* buildTree(vector<int> &nums, int start, int end) {
        if(start > end) return nullptr;
        SegmentTreeNode* root = new SegmentTreeNode(start,end);
        if(start == end) {
            root->sum = nums[start];
            return root;
        }
        int mid = start + (end - start) / 2;
        root->left = buildTree(nums,start,mid);
        root->right = buildTree(nums,mid+1,end);
        root->sum = root->left->sum + root->right->sum;
        return root;
    }
    int modifyTree(int i, int val, SegmentTreeNode* root) {
        if(root == nullptr) return 0;
        int diff;
        if(root->start == i && root->end == i) {
            diff = val - root->sum;
            root->sum = val;
            return diff;
        }
        int mid = (root->start + root->end) / 2;
        if(i > mid) {
            diff = modifyTree(i,val,root->right);
        } else {
            diff = modifyTree(i,val,root->left);
        }
        root->sum = root->sum + diff;
        return diff;
    }
    int queryTree(int i, int j, SegmentTreeNode* root) {
        if(root == nullptr) return 0;
        if(root->start == i && root->end == j) return root->sum;
        int mid = (root->start + root->end) / 2;
        if(i > mid) return queryTree(i,j,root->right);
        if(j <= mid) return queryTree(i,j,root->left);
        return queryTree(i,mid,root->left) + queryTree(mid+1,j,root->right);
    }
};

/**
 * Block sum
 */
class NumArray {
public:
    NumArray(vector<int>& n) : nums{n} {
        int const len = nums.size();
        int blocks = std::ceil(len / sqrt(len));
        block_sum = vector<int>(blocks, 0);

        for (int i = 0; i < len; ++i) {
            block_sum[i / blocks] += nums[i];
        }
    }

    void update(int index, int val) {
        int block = index / block_sum.size();
        block_sum[block] += val - nums[index];
        nums[index] = val;
    }

    int sumRange(int left, int right) {
        int block_s = left / block_sum.size(), block_e = right / block_sum.size();
        if (block_s == block_e) {
            return accumulate(begin(nums) + left, begin(nums) + right + 1, 0);
        }

        int sum = 0;
        for (int i = left; i <= (block_s + 1) * block_sum.size() - 1; ++i) {
            sum += nums[i];
        }
        for (int i = block_s + 1; i <= (block_e - 1); ++i) {
            sum += block_sum[i];
        }
        for (int i = block_e * block_sum.size(); i <= right; ++i) {
            sum += nums[i];
        }

        return sum;
    }

private:
    vector<int>& nums;
    vector<int> block_sum;
};

/**
 * Your NumArray object will be instantiated and called as such:
 * NumArray* obj = new NumArray(nums);
 * obj->update(index,val);
 * int param_2 = obj->sumRange(left,right);
 */

/**
 * Original solution. TLE as expected.
 */
class NumArray {
public:
    NumArray(vector<int>& n) : nums{n} {

    }

    void update(int index, int val) {
        nums[index] = val;
    }

    int sumRange(int left, int right) {
        return accumulate(begin(nums) + left, begin(nums) + right + 1, 0);
    }

private:
    vector<int>& nums;
};

/**
 * Your NumArray object will be instantiated and called as such:
 * NumArray* obj = new NumArray(nums);
 * obj->update(index,val);
 * int param_2 = obj->sumRange(left,right);
 */
