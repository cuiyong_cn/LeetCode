/**
 * Count Symmetric Integers
 *
 * You are given two positive integers low and high.
 *
 * An integer x consisting of 2 * n digits is symmetric if the sum of the first n digits of x is
 * equal to the sum of the last n digits of x. Numbers with an odd number of digits are never
 * symmetric.
 *
 * Return the number of symmetric integers in the range [low, high].
 *
 * Example 1:
 *
 * Input: low = 1, high = 100
 * Output: 9
 * Explanation: There are 9 symmetric integers between 1 and 100: 11, 22, 33, 44, 55, 66, 77, 88, and 99.
 *
 * Example 2:
 *
 * Input: low = 1200, high = 1230
 * Output: 4
 * Explanation: There are 4 symmetric integers between 1200 and 1230: 1203, 1212, 1221, and 1230.
 *
 * Constraints:
 *     1 <= low <= high <= 104
 */
/**
 * Original solution.
 */
auto number_of_digits(int n)
{
    auto cnt = 1;

    while (n > 9) {
        ++cnt;
        n /= 10;
    }

    return cnt;
}

auto odd(int n)
{
    return n & 1;
}

auto symmetric(int n)
{
    auto const nod = number_of_digits(n);

    if (odd(nod)) {
        return false;
    }

    auto const half = nod / 2;
    auto sum_first_half = 0;
    auto sum_second_half = 0;

    for (auto i = 0; i < nod; ++i, n /= 10) {
        if (i < half) {
            sum_first_half += n % 10;
        }
        else {
            sum_second_half += n % 10;
        }
    }

    return sum_first_half == sum_second_half;
}

class Solution {
public:
    int countSymmetricIntegers(int low, int high) {
        auto ans = 0;

        for (auto i = low; i <= high; ++i) {
            ans += symmetric(i);
        }

        return ans;
    }
};
