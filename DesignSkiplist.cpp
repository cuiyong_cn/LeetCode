/**
 * Design Skiplist
 *
 * Design a Skiplist without using any built-in libraries.
 *
 * A Skiplist is a data structure that takes O(log(n)) time to add, erase and search.
 * Comparing with treap and red-black tree which has the same function and performance,
 * the code length of Skiplist can be comparatively short and the idea behind Skiplists
 * are just simple linked lists.
 *
 * For example: we have a Skiplist containing [30,40,50,60,70,90] and we want to add 80
 * and 45 into it. The Skiplist works this way:
 *
 * You can see there are many layers in the Skiplist. Each layer is a sorted linked list.
 * With the help of the top layers, add , erase and search can be faster than O(n). It
 * can be proven that the average time complexity for each operation is O(log(n)) and
 * space complexity is O(n).
 *
 * To be specific, your design should include these functions:
 *     bool search(int target) : Return whether the target exists in the Skiplist or not.
 *     void add(int num): Insert a value into the SkipList.
 *     bool erase(int num): Remove a value in the Skiplist. If num does not exist in the
 *          Skiplist, do nothing and return false. If there exists multiple num values,
 *          removing any one of them is fine.
 *
 * See more about Skiplist : https://en.wikipedia.org/wiki/Skip_list
 *
 * Note that duplicates may exist in the Skiplist, your code needs to handle this situation.
 *
 * Example:
 *
 * Skiplist skiplist = new Skiplist();
 *
 * skiplist.add(1);
 * skiplist.add(2);
 * skiplist.add(3);
 * skiplist.search(0);   // return false.
 * skiplist.add(4);
 * skiplist.search(1);   // return true.
 * skiplist.erase(0);    // return false, 0 is not in skiplist.
 * skiplist.erase(1);    // return true.
 * skiplist.search(1);   // return false, 1 has already been erased.
 *
 * Constraints:
 *
 *     0 <= num, target <= 20000
 *     At most 50000 calls will be made to search, add, and erase.
 */
/**
 * Copy from leetcode, but this solution got memory leak. And not very elegant.
 * Probably doing a research and give it a modification later if possible.
 */
class Skiplist
{
    struct Node {
        int val;
        Node* next{ NULL };
        Node* pre{ NULL };
        Node* down{ NULL };
        Node(int val = 0)
        {
            this->val = val;
        }
    };

    Node* heads{ NULL };
    int layers = 0;

public:
    Skiplist()
    {
        srand(time(NULL));
    }

    bool search(int target)
    {
        if (heads == NULL) return false;
        auto p = heads; // top down
        while (p) {
            while (p->next && p->next->val < target) p = p->next;
            if (p->next && p->next->val == target) return true;
            p = p->down;
        }
        return false;
    }

    void add(int num)
    {
        Node* p = heads;
        vector<Node*> path(layers, NULL);
        for (int i = layers - 1; i >= 0; i--) {
            while (p->next && p->next->val < num) {
                p = p->next;
            }
            path[i] = p;
            p = p->down;
        }

        for (int i = 0; i <= path.size(); i++) { // bottom up
            p = new Node(num);
            if (i == path.size()) {
                Node* last = heads;
                heads = new Node();
                heads->down = last;
                heads->next = p;
                p->pre = heads;
                layers++;
            } else {
                p->next = path[i]->next;
                p->pre = path[i];
                path[i]->next = p;
                if (p->next) p->next->pre = p;
            }
            if (i) {
                p->down = path[i - 1]->next;
            }
            if (rand() % 2) {
                break;
            }

        }
    }

    bool erase(int num)
    {
        auto p = heads;
        for (int i = layers - 1; i >= 0; i--) {
            while (p->next && p->next->val < num) p = p->next;
            if (p->next && p->next->val == num) {
                p = p->next;
                while (p) {
                    p->pre->next = p->next;
                    if (p->next) p->next->pre = p->pre;
                    p = p->down;
                }
                while (heads && heads->next == NULL) {
                    heads = heads -> down;
                    layers--;
                }
                return true;
            } else {
                p = p->down;
                if (p == NULL) return false;
            }
        }
        return false;
    }
};

/**
 * Your Skiplist object will be instantiated and called as such:
 * Skiplist* obj = new Skiplist();
 * bool param_1 = obj->search(target);
 * obj->add(num);
 * bool param_3 = obj->erase(num);
 */

