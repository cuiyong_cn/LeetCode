/**
 * Print Zero Even Odd
 *
 * Suppose you are given the following code:
 *
 * class ZeroEvenOdd {
 *   public ZeroEvenOdd(int n) { ... }      // constructor
 *   public void zero(printNumber) { ... }  // only output 0's
 *   public void even(printNumber) { ... }  // only output even numbers
 *   public void odd(printNumber) { ... }   // only output odd numbers
 * }
 * The same instance of ZeroEvenOdd will be passed to three different threads:
 *
 * Thread A will call zero() which should only output 0's.
 * Thread B will call even() which should only ouput even numbers.
 * Thread C will call odd() which should only output odd numbers.
 * Each of the threads is given a printNumber method to output an integer. Modify the given program to output the
 * series 010203040506... where the length of the series must be 2n.
 *
 * Example 1:
 *
 * Input: n = 2
 * Output: "0102"
 * Explanation: There are three threads being fired asynchronously. One of them calls zero(), the other calls even(),
 * and the last one calls odd(). "0102" is the correct output.
 * Example 2:
 *
 * Input: n = 5
 * Output: "0102030405"
 */
/**
 * Not my own solution. Copied user IgorB's method. pretty neat. Lack lots of knowlege in concurrent programming.
 */
class ZeroEvenOdd {
private:
    int n;
    mutex mx;
    condition_variable cv;
    atomic<int> num_to_print;
    atomic<int> current;

public:
    ZeroEvenOdd(int n) {
        this->n = n;
        num_to_print = 0;
        current = 0;
    }

    // printNumber(x) outputs "x", where x is an integer.
    void zero(function<void(int)> printNumber) {
        do_work(printNumber, [&]{return num_to_print == 0;});
    }

    void even(function<void(int)> printNumber) {
        do_work(printNumber, [&]{return num_to_print != 0 && num_to_print % 2 == 0;});
    }

    void odd(function<void(int)> printNumber) {
         do_work(printNumber, [&]{return num_to_print % 2 != 0;});
    }

protected:
   void do_work(function<void(int)> print, function<bool()> eval)
    {
        while(current <= n)
        {
            unique_lock<mutex> ul(mx);
            cv.wait(ul, [&]{return eval() || current > n;});
            if (current > n) break;
            print(num_to_print);
            num_to_print = num_to_print == 0 || num_to_print == n ? ++current : 0;
            cv.notify_all();
        }
    }
};
