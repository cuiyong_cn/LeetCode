/**
 * Fraction to Recurring Decimal
 *
 * Given two integers representing the numerator and denominator of a fraction, return the fraction
 * in string format.
 *
 * If the fractional part is repeating, enclose the repeating part in parentheses.
 *
 * If multiple answers are possible, return any of them.
 *
 * It is guaranteed that the length of the answer string is less than 104 for all the given inputs.
 *
 * Example 1:
 *
 * Input: numerator = 1, denominator = 2
 * Output: "0.5"
 * Example 2:
 *
 * Input: numerator = 2, denominator = 1
 * Output: "2"
 * Example 3:
 *
 * Input: numerator = 2, denominator = 3
 * Output: "0.(6)"
 * Example 4:
 *
 * Input: numerator = 4, denominator = 333
 * Output: "0.(012)"
 * Example 5:
 *
 * Input: numerator = 1, denominator = 5
 * Output: "0.2"
 *
 * Constraints:
 *
 * -231 <= numerator, denominator <= 231 - 1
 * denominator != 0
 */
/**
 * Original solution.
 * I don't know how to handle this if the host does not have long long type?
 */
class Solution {
public:
    string fractionToDecimal(int numerator, int denominator) {
        if (0 == numerator) {
            return "0";
        }

        long long remainder = labs(numerator);
        long long denom = labs(denominator);
        long long quotient = remainder / denom;
        string int_part = to_string(quotient);
        string frac_part;

        unordered_map<int, int> seen;
        remainder = remainder - (quotient * denom);
        while (0 != remainder) {
            remainder *= 10;
            if (seen.count(remainder)) {
                break;
            }

            seen.insert({remainder, frac_part.size()});
            quotient = remainder / denom;
            frac_part += to_string(quotient);
            remainder = remainder - (quotient * denom);
        }

        if (bool negative = (numerator > 0) ^ (denominator > 0)) {
            int_part = "-" + int_part;
        }

        if (!frac_part.empty()) {
            if (bool repeat = (0 != remainder)) {
                int sp = seen[remainder];
                frac_part = frac_part.substr(0, sp) + "(" + frac_part.substr(sp) + ")";
            }

            frac_part = "." + frac_part;
        }

        return int_part + frac_part;
    }
};
