/**
 * Integer to Roman
 *
 * Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.
 *
 * Symbol       Value
 * I             1
 * V             5
 * X             10
 * L             50
 * C             100
 * D             500
 * M             1000
 * For example, two is written as II in Roman numeral, just two one's added together. Twelve is written as, XII, which
 * is simply X + II. The number twenty seven is written as XXVII, which is XX + V + II.
 *
 * Roman numerals are usually written largest to smallest from left to right. However, the numeral for four is not IIII.
 * Instead, the number four is written as IV. Because the one is before the five we subtract it making four. The same
 * principle applies to the number nine, which is written as IX. There are six instances where subtraction is used:
 *
 * I can be placed before V (5) and X (10) to make 4 and 9.
 * X can be placed before L (50) and C (100) to make 40 and 90.
 * C can be placed before D (500) and M (1000) to make 400 and 900.
 * Given an integer, convert it to a roman numeral. Input is guaranteed to be within the range from 1 to 3999.
 *
 * Example 1:
 *
 * Input: 3
 * Output: "III"
 * Example 2:
 *
 * Input: 4
 * Output: "IV"
 * Example 3:
 *
 * Input: 9
 * Output: "IX"
 * Example 4:
 *
 * Input: 58
 * Output: "LVIII"
 * Explanation: L = 50, V = 5, III = 3.
 * Example 5:
 *
 * Input: 1994
 * Output: "MCMXCIV"
 * Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.
 */
/**
 * Improved version.
 */
class Solution {
public:
    struct RomanNum {
        string symbol;
        int val;
    };

    string intToRoman(int num) {
        string ans;
        int remain = num;
        vector<RomanNum> roman = { {"M", 1000}, {"CM", 900}, {"D", 500}, {"CD", 400},
                                  {"C", 100}, {"XC", 90}, {"L", 50}, {"XL", 40},
                                  {"X", 10}, {"IX", 9}, {"V", 5}, {"IV", 4}, {"I", 1}};
        for (int i = 0; i < roman.size(); ++i) {
            if (remain >= roman[i].val) {
                string symbol = roman[i].symbol;
                int count = remain / roman[i].val;
                remain = remain - count * roman[i].val;
                for (int j = 0; j < count; ++j) {
                    ans += symbol;
                }
            }
        }

        return ans;
    }
};

/**
 * Original solution. Very wordy. Can be simplified using Factory and Virtual Inheritance pattern. But unworthy.
 */
class Solution {
public:
    string intToRoman(int num) {
        string ans;
        int remain = num;
        while (remain > 0) {
            string symbol;
            int count = 0;
            if (remain >= 1000) {
                symbol = "M";
                count = remain / 1000;
                remain = remain - count * 1000;
            }
            else if (remain >= 900) {
                symbol = "CM";
                count = remain / 900;
                remain = remain - count * 900;
            }
            else if (remain >= 500) {
                symbol = "D";
                count = remain / 500;
                remain = remain - count * 500;
            }
            else if (remain >= 400) {
                symbol = "CD";
                count = remain / 400;
                remain = remain - count * 400;
            }
            else if (remain >= 100) {
                symbol = "C";
                count = remain / 100;
                remain = remain - count * 100;
            }
            else if (remain >= 90) {
                symbol = "XC";
                count = remain / 90;
                remain = remain - count * 90;
            }
            else if (remain >= 50) {
                symbol = "L";
                count = remain / 50;
                remain = remain - count * 50;
            }
            else if (remain >= 40) {
                symbol = "XL";
                count = remain / 40;
                remain = remain - count * 40;
            }
            else if (remain >= 10) {
                symbol = "X";
                count = remain / 10;
                remain = remain - count * 10;
            }
            else if (remain >= 9) {
                symbol = "IX";
                count = 1;
                remain = remain - 9;
            }
            else if (remain >= 5) {
                symbol = "V";
                count = 1;
                remain = remain - 5;
            }
            else if (remain >= 4) {
                symbol = "IV";
                count = 1;
                remain = remain - 4;
            }
            else if (remain >= 1) {
                symbol = "I";
                count = remain;
                remain = 0;
            }

            for (int i = 0; i < count; ++i) {
                ans += symbol;
            }
        }

        return ans;
    }
};
