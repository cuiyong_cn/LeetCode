/**
 * Maximum Value at a Given Index in a Bounded Array
 *
 * You are given three positive integers: n, index, and maxSum. You want to construct an array nums
 * (0-indexed) that satisfies the following conditions:
 *
 * nums.length == n
 * nums[i] is a positive integer where 0 <= i < n.
 * abs(nums[i] - nums[i+1]) <= 1 where 0 <= i < n-1.
 * The sum of all the elements of nums does not exceed maxSum.
 * nums[index] is maximized.
 * Return nums[index] of the constructed array.
 *
 * Note that abs(x) equals x if x >= 0, and -x otherwise.
 *
 * Example 1:
 *
 * Input: n = 4, index = 2,  maxSum = 6
 * Output: 2
 * Explanation: nums = [1,2,2,1] is one array that satisfies all the conditions.
 * There are no arrays that satisfy all the conditions and have nums[2] == 3, so 2 is the maximum nums[2].
 * Example 2:
 *
 * Input: n = 6, index = 1,  maxSum = 10
 * Output: 3
 *
 * Constraints:
 *
 * 1 <= n <= maxSum <= 109
 * 0 <= index < n
 */
/**
 * From discussion, we found one intresting point.
 *
 * We first do maxSum -= n;
 *
 * I spend 3 mins to understand this. Doing this, basically means: we substract one from every
 * element in the array. By doing this, now the element's constraints is >= 0 instead of >= 1.
 */
/**
 * Original solution.
 */
int64_t cal_sum(int64_t limit, int64_t len)
{
    if (limit > len) {
        auto begin = limit - len;
        auto end = limit - 1;
        return (len * (begin + end)) >> 1;
    }

    auto n = limit - 1;
    auto sum = len - n;
    return sum + ((n * (1 + n)) >> 1);
}

class Solution {
public:
    int maxValue(int n, int index, int maxSum) {
        int left = 1;
        int right = maxSum;

        while (left <= right) {
            auto mid = (right + left) >> 1;
            auto sum_left = cal_sum(mid, index);
            auto sum_right = cal_sum(mid + 1, n - index);
            auto sum = sum_left + sum_right;

            if (sum <= maxSum) {
                left = mid + 1;
            }
            else {
                right = mid - 1;
            }
        }

        return left - 1;
    }
};
