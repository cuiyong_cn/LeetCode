/**
 * Best Time to Buy and Sell Stock
 *
 * Say you have an array for which the ith element is the price of a given stock on day i.
 *
 * If you were only permitted to complete at most one transaction (i.e., buy one and sell one share of the stock),
 * design an algorithm to find the maximum profit.
 *
 * Note that you cannot sell a stock before you buy one.
 *
 * Example 1:
 *
 * Input: [7,1,5,3,6,4]
 * Output: 5
 * Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
 *              Not 7-1 = 6, as selling price needs to be larger than buying price.
 *
 * Example 2:
 *
 * Input: [7,6,4,3,1]
 * Output: 0
 * Explanation: In this case, no transaction is done, i.e. max profit = 0.
 */
/**
 * Errr..., it turns out that this is not a dp problem.
 * We only have to find the max profit from a point of price valley.
 */
class Solution {
public:
    int maxProfit(vector<int>& prices) {
        if (prices.size() < 2) return 0;

        int min_price = numeric_limits<int>::max();
        int ans = 0;

        for (int i = 0; i < prices.size(); ++i) {
            if (prices[i] < min_price) {
                min_price = prices[i];
            }
            else {
                ans = max(ans, prices[i] - min_price);
            }
        }

        return ans;
    }
};

/**
 * Think another time, this dp thing is too heavy. But TLE again.
 */
class Solution {
public:
    int maxProfit(vector<int>& prices) {
        if (prices.size() < 2) return 0;

        int ans = 0;

        for (int i = 0; i < prices.size(); ++i) {
            for (int j = i + 1; j < prices.size(); ++j) {
                ans = max(prices[j] - prices[i], ans);
            }
        }

        return ans;
    }
};

/**
 * Original solution. The first method comes into mind is dp method. But sadily millions time: TLE.
 */
class Solution {
public:
    int maxProfit(vector<int>& prices) {
        if (prices.size() < 2) return 0;

        vector<vector<int>> dp(prices.size(), vector<int>(prices.size()));
        for (int i = 0; i < prices.size(); ++i) {
            for (int j = 0; j < prices.size(); ++j) {
                dp[i][j] = i >= j ? 0 : -1;
            }
        }

        for (int i = 1; i < prices.size(); ++i) {
            for (int j = 0; j < prices.size() - i; ++j) {
                dp[j][j + i] = max(prices[j + i] - prices[j],
                                     max(dp[j + 1][j + i], dp[j][j + i - 1]));

            }
        }

        return dp[0][prices.size() - 1];
    }
};
