/**
 * Number of Segments in a String
 *
 * You are given a string s, return the number of segments in the string.
 *
 * A segment is defined to be a contiguous sequence of non-space characters.
 *
 * Example 1:
 *
 * Input: s = "Hello, my name is John"
 * Output: 5
 * Explanation: The five segments are ["Hello,", "my", "name", "is", "John"]
 * Example 2:
 *
 * Input: s = "Hello"
 * Output: 1
 * Example 3:
 *
 * Input: s = "love live! mu'sic forever"
 * Output: 4
 * Example 4:
 *
 * Input: s = ""
 * Output: 0
 *
 * Constraints:
 *
 * 0 <= s.length <= 300
 * s consists of lower-case and upper-case English letters, digits or one of the following
 * characters "!@#$%^&*()_+-=',.:".
 * The only space character in s is ' '.
 */
/**
 * shorter one.
 */
class Solution {
public:
    int countSegments(string s) {
        int res = 0;
        s.push_back(' ');
        for (int i = 1; i < s.size(); ++i) {
            if (s[i] == ' ' && s[i-1] != ' ') {
                ++res;
            }
        }

        return res;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int countSegments(string s) {
        int i = 0;
        int cnt = 0;
        while (i < s.length()) {
            // skip continuous spaces
            auto pos = s.find_first_not_of(' ', i);
            if (pos == string::npos) {
                break;
            }
            ++cnt;
            pos = s.find_first_of(' ', pos);
            i = string::npos == pos ? s.length() : pos;
        }

        return cnt;
    }
};
