/**
 * Shortest Bridge
 *
 * In a given 2D binary array A, there are two islands.  (An island is a 4-directionally connected group of 1s not
 * connected to any other 1s.)
 *
 * Now, we may change 0s to 1s so as to connect the two islands together to form 1 island.
 *
 * Return the smallest number of 0s that must be flipped.  (It is guaranteed that the answer is at least 1.)
 *
 * Example 1:
 *
 * Input: A = [[0,1],[1,0]]
 * Output: 1
 *
 * Example 2:
 *
 * Input: A = [[0,1,0],[0,0,0],[0,0,1]]
 * Output: 2
 *
 * Example 3:
 *
 * Input: A = [[1,1,1,1,1],[1,0,0,0,1],[1,0,1,0,1],[1,0,0,0,1],[1,1,1,1,1]]
 * Output: 1
 *
 * Constraints:
 *     2 <= A.length == A[0].length <= 100
 *     A[i][j] == 0 or A[i][j] == 1
 */
/**
 * Here comes the improved one.
 */
class Solution {
public:
    int shortestBridge(vector<vector<int>>& A) {
        int island_label = 2;
        deque<Point> one_island;
        vector<int> dir{0, -1, 0, 1, -1, 0, 1, 0};

        deque<Point> q;
        q.push_back(first_one(A));

        // find the first island
        while (!q.empty()) {
            int size = q.size();
            for (int i = 0; i < size; ++i) {
                auto pos = q.front();
                q.pop_front();

                if (A[pos.x][pos.y] != 1) {
                    continue;
                }

                A[pos.x][pos.y] = island_label;
                one_island.push_back(pos);

                for (int d = 0; d < 8; d += 2) {
                    Point np{pos.x + dir[d], pos.y + dir[d + 1]};
                    if (0 <= np.x && np.x < A.size() && 0 <= np.y && np.y < A.size()) {
                        if (1 == A[np.x][np.y]) {
                            q.push_back(np);
                        }
                    }
                }
            }
        }

        // grow the island until we touch the second island
        int flood_turn = 3;
        while (1) {
            bool touched = false;
            int size = one_island.size();
            for (int i = 0; i < size; ++i) {
                auto pos = one_island.front();
                one_island.pop_front();

                for (int d = 0; d < 8; d += 2) {
                    Point np{pos.x + dir[d], pos.y + dir[d + 1]};
                    if (0 <= np.x && np.x < A.size() && 0 <= np.y && np.y < A.size()) {
                        if (1 == A[np.x][np.y]) {
                            touched = true;
                        }

                        if (0 == A[np.x][np.y]) {
                            A[np.x][np.y] = flood_turn;
                            one_island.push_back(np);
                        }
                    }
                }
            }

            if (touched) {
                break;
            }
            ++flood_turn;
        }

        return flood_turn - 3;
    }
private:
    struct Point
    {
        int x;
        int y;
    };

private:
    Point first_one(const vector<vector<int>>& A)
    {
        for (int i = 0; i < A.size(); ++i) {
            for (int j = 0; j < A.size(); ++j) {
                if (A[i][j] == 1) {
                    return {i, j};
                }
            }
        }

        return {0, 0};
    }
};

/**
 * Original solution. Passed. But not efficient and not trigger TLE, weird. I expected this to be correct but TLE.
 * Anyway, the idea is, we get the collection of the points that belong to the corresponding island. And calculate
 * the min distance between any two points.
 *
 * Refer to https://leetcode.com/problems/shortest-bridge/discuss/189293/C%2B%2B-BFS-Island-Expansion-%2B-UF-Bonus
 *
 * We only need to find the first island, and label it as 2. Then start from first island, and grow the grow one by
 * one until we touch the second island. This may much faster than the original one.
 */
class Solution {
public:
    int shortestBridge(vector<vector<int>>& A) {
        int island_label = -1;
        int island_num = 0;
        vector<vector<Point>> islands(2);
        vector<int> dir{0, -1, 0, 1, -1, 0, 1, 0};

        while (island_num < 2) {
            deque<Point> q;
            q.push_back(first_one(A));

            while (!q.empty()) {
                int size = q.size();
                for (int i = 0; i < size; ++i) {
                    auto pos = q.front();
                    q.pop_front();

                    if (A[pos.x][pos.y] != 1) {
                        continue;
                    }

                    A[pos.x][pos.y] = island_label;
                    islands[island_num].push_back(pos);

                    for (int d = 0; d < 8; d += 2) {
                        Point np{pos.x + dir[d], pos.y + dir[d + 1]};
                        if (0 <= np.x && np.x < A.size() && 0 <= np.y && np.y < A.size()) {
                            if (1 == A[np.x][np.y]) {
                                q.push_back(np);
                            }
                        }
                    }
                }
            }

            --island_label;
            ++island_num;
        }

        int ans = 10000;
        for (int i = 0; i < islands[0].size(); ++i) {
            auto p1 = islands[0][i];
            for (int j = 0; j < islands[1].size(); ++j) {
                auto p2 = islands[1][j];
                ans = std::min(ans, std::abs(p1.x - p2.x) + std::abs(p1.y - p2.y) - 1);
            }
        }

        return ans;
    }
private:
    struct Point
    {
        int x;
        int y;
    };

private:
    Point first_one(const vector<vector<int>>& A)
    {
        for (int i = 0; i < A.size(); ++i) {
            for (int j = 0; j < A.size(); ++j) {
                if (A[i][j] == 1) {
                    return {i, j};
                }
            }
        }

        return {0, 0};
    }
};
