/**
 * Find Right Interval
 *
 * You are given an array of intervals, where intervals[i] = [starti, endi] and each starti is unique.
 *
 * The right interval for an interval i is an interval j such that startj >= endi and startj is minimized.
 *
 * Return an array of right interval indices for each interval i. If no right interval exists for interval i, then put -1 at index i.
 *
 *
 *
 * Example 1:
 *
 * Input: intervals = [[1,2]]
 * Output: [-1]
 * Explanation: There is only one interval in the collection, so it outputs -1.
 * Example 2:
 *
 * Input: intervals = [[3,4],[2,3],[1,2]]
 * Output: [-1,0,1]
 * Explanation: There is no right interval for [3,4].
 * The right interval for [2,3] is [3,4] since start0 = 3 is the smallest start that is >= end1 = 3.
 * The right interval for [1,2] is [2,3] since start1 = 2 is the smallest start that is >= end2 = 2.
 * Example 3:
 *
 * Input: intervals = [[1,4],[2,3],[3,4]]
 * Output: [-1,2,-1]
 * Explanation: There is no right interval for [1,4] and [3,4].
 * The right interval for [2,3] is [3,4] since start2 = 3 is the smallest start that is >= end1 = 3.
 *
 *
 * Constraints:
 *
 * 1 <= intervals.length <= 2 * 104
 * intervals[i].length == 2
 * -106 <= starti <= endi <= 106
 * The start point of each interval is unique.
 */
/**
 * It turns out that map has built-in lower_bound method.
 * But this is not as efficient as the original one.
 */
class Solution {
public:
    vector<int> findRightInterval(vector<vector<int>>& intervals) {
        vector<int> ans(intervals.size(), -1);
        map<int, int> m;

        for(int i = 0; i < intervals.size(); i++) {
            m[intervals[i][0]] = i;
        }

        for(int i = 0; i < intervals.size(); i++) {
            auto it = m.lower_bound(intervals[i][1]);
            if (it != m.end()) {
                ans[i] = it->second;
            }
        }

        return ans;
    }
};

/**
 * Using lower_bound algorithm.
 */
class Solution {
public:
    vector<int> findRightInterval(vector<vector<int>>& intervals) {
        vector<std::pair<int, size_t>> end_index(intervals.size());

        for (int i = 0; i < intervals.size(); ++i) {
            end_index[i] = { intervals[i][0], i };
        }

        sort(end_index.begin(), end_index.end(), [](auto a, auto b) {
            return a.first < b.first;
        });

        vector<int> ans(intervals.size(), -1);

        for (size_t i = 0; i < intervals.size(); ++i) {
            auto it = lower_bound(end_index.begin(), end_index.end(),
                                 intervals[i][1], [](auto a, auto b) {
                                     return a.first < b;
                                 });
            if (it != end_index.end()) {
                ans[i] = it->second;
            }
        }

        return ans;
    }
};

/**
 * Original solution. Also the hand-written binary search can be replaced with lower_bound algorithm.
 */
class Solution {
public:
    vector<int> findRightInterval(vector<vector<int>>& intervals) {
        vector<std::pair<int, size_t>> end_index(intervals.size());

        for (int i = 0; i < intervals.size(); ++i) {
            end_index[i] = { intervals[i][0], i };
        }

        sort(end_index.begin(), end_index.end(), [](auto a, auto b) {
            return a.first < b.first;
        });

        vector<int> ans(intervals.size(), -1);

        for (size_t i = 0; i < intervals.size(); ++i) {
            int target = intervals[i][1];

            int left = 0;
            int right = intervals.size() - 1;
            int index = -1;
            while (left <= right) {
                int mid = (left + right) / 2;

                if (end_index[mid].first >= target) {
                    index = end_index[mid].second;
                    right = mid - 1;
                }
                else {
                    left = mid + 1;
                }
            }

            ans[i] = index;
        }

        return ans;
    }
};
