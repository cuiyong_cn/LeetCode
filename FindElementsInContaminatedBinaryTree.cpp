/**
 * Find Elements in a Contaminated Binary Tree
 *
 * Given a binary tree with the following rules:
 *
 * root.val == 0
 * If treeNode.val == x and treeNode.left != null, then treeNode.left.val == 2 * x + 1
 * If treeNode.val == x and treeNode.right != null, then treeNode.right.val == 2 * x + 2
 * Now the binary tree is contaminated, which means all treeNode.val have been changed to -1.
 *
 * Implement the FindElements class:
 *
 * FindElements(TreeNode* root) Initializes the object with a contaminated binary tree and recovers it.
 * bool find(int target) Returns true if the target value exists in the recovered binary tree.
 *
 * Example 1:
 *
 * Input
 * ["FindElements","find","find"]
 * [[[-1,null,-1]],[1],[2]]
 * Output
 * [null,false,true]
 * Explanation
 * FindElements findElements = new FindElements([-1,null,-1]);
 * findElements.find(1); // return False
 * findElements.find(2); // return True
 * Example 2:
 *
 *
 * Input
 * ["FindElements","find","find","find"]
 * [[[-1,-1,-1,-1,-1]],[1],[3],[5]]
 * Output
 * [null,true,true,false]
 * Explanation
 * FindElements findElements = new FindElements([-1,-1,-1,-1,-1]);
 * findElements.find(1); // return True
 * findElements.find(3); // return True
 * findElements.find(5); // return False
 * Example 3:
 *
 * Input
 * ["FindElements","find","find","find","find"]
 * [[[-1,null,-1,-1,null,-1]],[2],[3],[4],[5]]
 * Output
 * [null,true,false,false,true]
 * Explanation
 * FindElements findElements = new FindElements([-1,null,-1,-1,null,-1]);
 * findElements.find(2); // return True
 * findElements.find(3); // return False
 * findElements.find(4); // return False
 * findElements.find(5); // return True
 *
 * Constraints:
 *
 * TreeNode.val == -1
 * The height of the binary tree is less than or equal to 20
 * The total number of nodes is between [1, 104]
 * Total calls of find() is between [1, 104]
 * 0 <= target <= 106
 */
/**
 * Without the unordered_set
 */
class FindElements {
public:
    FindElements(TreeNode* root) : m_root{root} {
        recover(root, 0);
    }

    bool find(int target) {
        auto node = m_root;
        string bin = bitset<sizeof(int) * 8>(target + 1).to_string();
        for (int i = bin.find_first_of('1') + 1; node && i < bin.length(); ++i) {
            if (node->val == target) {
                return true;
            }
            if ('1' == bin[i]) {
                node = node->right;
            }
            else {
                node = node->left;
            }
        }

        return node && node->val == target;
    }

private:
    void recover(TreeNode* node, int val)
    {
        if (nullptr == node) {
            return;
        }

        node->val = val;
        recover(node->left, 2 * val + 1);
        recover(node->right, 2 * val + 2);
    }
private:
    TreeNode* m_root;
};

/**
 * Your FindElements object will be instantiated and called as such:
 * FindElements* obj = new FindElements(root);
 * bool param_1 = obj->find(target);
 */

/**
 * Original solution.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class FindElements {
public:
    FindElements(TreeNode* root) {
        recover(root, 0);
    }

    bool find(int target) {
        return 1 == m_val_set.count(target);
    }

private:
    void recover(TreeNode* node, int val)
    {
        if (nullptr == node) {
            return;
        }

        m_val_set.insert(val);
        node->val = val;
        recover(node->left, 2 * val + 1);
        recover(node->right, 2 * val + 2);
    }

private:
    unordered_set<int> m_val_set;
};

/**
 * Your FindElements object will be instantiated and called as such:
 * FindElements* obj = new FindElements(root);
 * bool param_1 = obj->find(target);
 */
