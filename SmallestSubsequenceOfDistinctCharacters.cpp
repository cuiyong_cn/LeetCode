/**
 * Smallest Subsequence of Distinct Characters
 *
 * Return the lexicographically smallest subsequence of text that contains all the distinct characters of text exactly
 * once.
 *
 * Example 1:
 *
 * Input: "cdadabcc"
 * Output: "adbc"
 *
 * Example 2:
 *
 * Input: "abcd"
 * Output: "abcd"
 *
 * Example 3:
 *
 * Input: "ecbacba"
 * Output: "eacb"
 *
 * Example 4:
 *
 * Input: "leetcode"
 * Output: "letcod"
 *
 * Constraints:
 *     1 <= text.length <= 1000
 *     text consists of lowercase English letters.
 * Note: This question is the same as 316: https://leetcode.com/problems/remove-duplicate-letters/
 */
/**
 * The example 4 really confusese me. So I did not sovle this one.
 * Here is the solution from the discussion.
 */
class Solution
{
public:
    string smallestSubsequence(string s, string res = "")
    {
        int cnt[26] = {}, used[26] = {};
        for (auto ch : s) ++cnt[ch - 'a'];
        for (auto ch : s) {
            --cnt[ch - 'a'];
            if (used[ch - 'a']++ > 0) continue;

            while (!res.empty() && res.back() > ch && cnt[res.back() - 'a'] > 0) {
                used[res.back() - 'a'] = 0;
                res.pop_back();
            }
            res.push_back(ch);
        }
        return res;
    }
};

