/**
 * K Closest Points to Origin
 *
 * We have a list of points on the plane.  Find the K closest points to the origin (0, 0).
 *
 * (Here, the distance between two points on a plane is the Euclidean distance.)
 *
 * You may return the answer in any order.  The answer is guaranteed to be unique (except
 * for the order that it is in.)
 *
 * Example 1:
 *
 * Input: points = [[1,3],[-2,2]], K = 1
 * Output: [[-2,2]]
 * Explanation:
 * The distance between (1, 3) and the origin is sqrt(10).
 * The distance between (-2, 2) and the origin is sqrt(8).
 * Since sqrt(8) < sqrt(10), (-2, 2) is closer to the origin.
 * We only want the closest K = 1 points from the origin, so the answer is just [[-2,2]].
 *
 * Example 2:
 *
 * Input: points = [[3,3],[5,-1],[-2,4]], K = 2
 * Output: [[3,3],[-2,4]]
 * (The answer [[-2,4],[3,3]] would also be accepted.)
 *
 * Note:
 *
 *     1 <= K <= points.length <= 10000
 *     -10000 < points[i][0] < 10000
 *     -10000 < points[i][1] < 10000
 */
/**
 * Original version. Using sort.
 */
class Solution
{
public:
    vector<vector<int>> kClosest(vector<vector<int>>& points, int K)
    {
        sort(points.begin(), points.end(), [](vector<int>& p1, vector<int>& p2) {
            return (p1[0] * p1[0] + p1[1] * p1[1]) < (p2[0] * p2[0] + p2[1] * p2[1]);
        });
        vector<vector<int>> ans;
        for (int i = 0; i < K; ++i) {
            ans.push_back(points[i]);
        }

        return ans;
    }
};

/**
 * Using std::nth_element function.
 */
class Solution
{
    vector<vector<int>> kClosest(vector<vector<int>>& ps, int K)
    {
        nth_element(begin(ps), begin(ps) + K, end(ps), [](vector<int> &a, vector<int> &b) {
            return a[0] * a[0] + a[1] * a[1] < b[0] * b[0] + b[1] * b[1];
        });

        ps.resize(K);

        return ps;
    }
};

/**
 * Quickselect method. This method complexity depends on the how the pivot is selected.
 */
class Solution
{
public:
    vector<vector<int>> kClosest(vector<vector<int>>& ps, int K)
    {
        quickselect(begin(ps), end(ps), K, [](vector<int> &a, vector<int> &b) {
            return a[0] * a[0] + a[1] * a[1] < b[0] * b[0] + b[1] * b[1];
        });
        ps.resize(K);
        return ps;
    }

    template<class RndIt, class Compare>
    void quickselect(RndIt first, RndIt end, int K, Compare cmp)
    {
        auto p = partition(first, end, cmp);
        if (p - first + 1 == K) return;
        if (p - first + 1 < K) quickselect(p + 1, end, K - (p - first + 1), cmp);
        else quickselect(first, p, K, cmp);
    }

    template<class RndIt, class Compare>
    RndIt partition(RndIt first, RndIt end, Compare cmp)
    {
        auto last = next(end, -1);
        auto pivot = *last;
        while (first < last) {
            while (cmp(*first, pivot)) ++first;
            while (cmp(pivot, *last)) --last;
            if (first < last) swap(*first, *last);
        }
        return first;
    }
};

