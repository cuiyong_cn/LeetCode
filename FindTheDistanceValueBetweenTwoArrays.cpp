/**
 * Find the Distance Value Between Two Arrays
 *
 * Given two integer arrays arr1 and arr2, and the integer d, return the distance value between the two arrays.
 *
 * The distance value is defined as the number of elements arr1[i] such that there is not any
 * element arr2[j] where |arr1[i]-arr2[j]| <= d.
 *
 * Example 1:
 *
 * Input: arr1 = [4,5,8], arr2 = [10,9,1,8], d = 2
 * Output: 2
 * Explanation:
 * For arr1[0]=4 we have:
 * |4-10|=6 > d=2
 * |4-9|=5 > d=2
 * |4-1|=3 > d=2
 * |4-8|=4 > d=2
 * For arr1[1]=5 we have:
 * |5-10|=5 > d=2
 * |5-9|=4 > d=2
 * |5-1|=4 > d=2
 * |5-8|=3 > d=2
 * For arr1[2]=8 we have:
 * |8-10|=2 <= d=2
 * |8-9|=1 <= d=2
 * |8-1|=7 > d=2
 * |8-8|=0 <= d=2
 * Example 2:
 *
 * Input: arr1 = [1,4,2,3], arr2 = [-4,-3,6,10,20,30], d = 3
 * Output: 2
 * Example 3:
 *
 * Input: arr1 = [2,1,100,3], arr2 = [-5,-2,10,-3,7], d = 6
 * Output: 1
 *
 * Constraints:
 *
 * 1 <= arr1.length, arr2.length <= 500
 * -1000 <= arr1[i], arr2[j] <= 1000
 * 0 <= d <= 100
 */
/**
 * Improve the original one. Shorter and elegant.
 * Idea is:
 * For each A[i], find the length of range [A[i] - d, A[i] + d] in array B. If the length is 0, then
 * increment the answer.
 *
 * The start point of the range (A[i] - d) can be found using lower_bound(begin(B), end(B), A[i] -
 * d).
 *
 * The next element after the end point of the range (the element after A[i] + d) can be found using
 * upper_bound(begin(B), end(B), n + d).
 *
 * If these two iterators are the same, it means the length of the range is 0.
 */
class Solution {
public:
    int findTheDistanceValue(vector<int>& arr1, vector<int>& arr2, int d) {
        sort( begin(arr2), end(arr2) );
        return count_if( begin(arr1), end(arr1), [&] (int a) {
            return lower_bound(begin(arr2), end(arr2), a - d)
                == upper_bound(begin(arr2), end(arr2), a + d);
        });
    }
};

/**
 * Found a simple brute force one.
 */
class Solution {
public:
    int findTheDistanceValue(vector<int>& A, vector<int>& B, int d) {
        return count_if(begin(A), end(A), [&](const auto &a) {
            return all_of(begin(B), end(B), [&](const auto &b) {
                return abs(a - b) > d;
            });
        });
    }
};

/**
 * Original solution. Sort arr2, find the closest element from arr2 for each element of arr1
 */
class Solution {
public:
    int findTheDistanceValue(vector<int>& arr1, vector<int>& arr2, int d) {
        sort(arr2.begin(), arr2.end());
        int ans = 0;
        for (auto n : arr1) {
            auto it = lower_bound(arr2.begin(), arr2.end(), n);
            if (it == arr2.end()) {
                if (abs(n - arr2.back()) <= d) {
                    ++ans;
                }
            }
            else {
                if (abs(n - *it) <= d || (it != arr2.begin() && abs(n - *(it - 1)) <= d)) {
                    ++ans;
                }
            }
        }

        return arr1.size() - ans;
    }
};
