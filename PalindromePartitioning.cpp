/**
 * Palindrome Partitioning
 *
 * Given a string s, partition s such that every substring of the partition is a palindrome.
 *
 * Return all possible palindrome partitioning of s.
 *
 * Example:
 *
 * Input: "aab"
 * Output:
 * [
 *   ["aa","b"],
 *   ["a","a","b"]
 * ]
 */
/**
 * After reading the discussion group. Here is the solution using backtracking.
 * Apparently, original thingking diverage too far away.
 */
class Solution {
public:
    vector<vector<string>> partition(string s)
    {
        vector<vector<bool>> pld_cache(s.size(), vector<bool>(s.size(), false));

        for (int L = 0; L < s.length(); ++L) {
            for (int i = 0; (i + L) < s.length(); ++i) {
                pld_cache[i][i + L] = s[i] == s[i + L] && (L <= 2 || pld_cache[i + 1][i + L - 1]);
            }
        }

        dfs(s, 0, pld_cache);

        return ans;
    }

private:
    void dfs(const string& s, int start, vector<vector<bool>>& pld_cache)
    {
        if (start == s.length()) {
            ans.push_back(path);
            return;
        }

        for (size_t i = start; i < s.length(); ++i) {
            if (pld_cache[start][i]) {
                path.push_back(s.substr(start, i - start + 1));
                dfs(s, i + 1, pld_cache);
                path.pop_back();
            }
        }
    }

    vector<vector<string>> ans;
    vector<string> path;
};

/**
 * Below is not a correct solution. But maybe useful in later time.
 */
class Solution {
public:
    vector<vector<string>> partition(string s) {
        vector<vector<string>> ans;
        vector<string> sub_parts = split_to_longest_palindrome(s);

        ans.push_back(sub_parts);

        auto iter = sub_parts.begin();
        while (iter != sub_parts.end()) {
            if (iter->length() > 1) {
                auto str = *iter;

                iter = sub_parts.erase(iter);
                vector<string> splits;
                splits.push_back(string(1, str.front()));
                if (str.length() > 2) {
                    splits.push_back(str.substr(1, str.length() - 2));
                }
                splits.push_back(string(1, str.back()));

                sub_parts.insert(iter, splits.begin(), splits.end());
                ans.push_back(sub_parts);

                iter = sub_parts.begin();
            }
            else {
                ++iter;
            }
        }

        return ans;
    }

private:
    vector<string> split_to_longest_palindrome(const string& s)
    {
        vector<string> ans;

        for (int distance = s.length() - 1; distance > 0; --distance) {
            int i = 0;
            bool palindrome = false;
            for (; i <= (s.length() - distance); ++i) {
                int left = i;
                int right = i + distance;
                while (left <= right) {
                    if (s[left] != s[right]) {
                        break;
                    }
                    ++left;
                    --right;
                }

                if (left > right) {
                    palindrome = true;
                    break;
                }
            }

            if (palindrome) {
                auto pre = split_to_longest_palindrome(s.substr(0, i));
                ans.insert(ans.end(), pre.begin(), pre.end());

                ans.push_back(s.substr(i, distance + 1));

                auto back = split_to_longest_palindrome(s.substr(i + distance + 1));
                ans.insert(ans.end(), back.begin(), back.end());
                break;
            }
        }

        if (ans.empty()) {
            for (auto c : s) {
                ans.push_back(string(1, c));
            }
        }

        return ans;
    }
};
