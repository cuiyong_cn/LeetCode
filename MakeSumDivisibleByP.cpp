/**
 * Make Sum Divisible by P
 *
 * Given an array of positive integers nums, remove the smallest subarray (possibly empty) such that
 * the sum of the remaining elements is divisible by p. It is not allowed to remove the whole array.
 *
 * Return the length of the smallest subarray that you need to remove, or -1 if it's impossible.
 *
 * A subarray is defined as a contiguous block of elements in the array.
 *
 * Example 1:
 *
 * Input: nums = [3,1,4,2], p = 6
 * Output: 1
 * Explanation: The sum of the elements in nums is 10, which is not divisible by 6. We can remove the subarray [4], and the sum of the remaining elements is 6, which is divisible by 6.
 * Example 2:
 *
 * Input: nums = [6,3,5,2], p = 9
 * Output: 2
 * Explanation: We cannot remove a single element to get a sum divisible by 9. The best way is to remove the subarray [5,2], leaving us with [6,3] with sum 9.
 * Example 3:
 *
 * Input: nums = [1,2,3], p = 3
 * Output: 0
 * Explanation: Here the sum is 6. which is already divisible by 3. Thus we do not need to remove anything.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 105
 * 1 <= nums[i] <= 109
 * 1 <= p <= 109
 */
/**
 * Turns out we don't need int64_t at all.
 */
class Solution {
public:
    int minSubarray(vector<int>& nums, int p) {
        int const N = nums.size();
        int ans = N;
        int rem1 = 0;
        for (auto n : nums) {
            rem1 = (rem1 + n) % p;
        }

        if (0 == rem1) {
            return 0;
        }

        unordered_map<int, int> rem_idx = {{0, -1}};
        for (int i = 0, rem2 = 0; i < N; ++i) {
            rem2 = (rem2 + nums[i]) % p;
            rem_idx[rem2] = i;
            int want = (rem2 - rem1 + p) % p;
            if (rem_idx.count(want)) {
                ans = min(ans, i - rem_idx[want]);
            }
        }

        return N == ans ? -1 : ans;
    }
};

/**
 * Original solution. We use int64_t to avoid signed integer overflow.
 */
class Solution {
public:
    int minSubarray(vector<int>& nums, int p) {
        int64_t rem1 = 0;
        for (auto n : nums) {
            rem1 = (rem1 + (n % p) + p) % p;
        }

        if (0 == rem1) {
            return 0;
        }

        int const N = nums.size();
        int const rem2 = p - rem1;
        unordered_map<int, int> rem_idx = { {0, -1} };
        int ans = N;
        int64_t rem = 0;
        for (int i = 0; i < N; ++i) {
            rem = (rem + (nums[i] % p) + p) % p;
            int r1 = (rem + rem2 + p) % p;
            if (rem_idx.count(r1)) {
                ans = min(ans, i - rem_idx[r1]);
            }
            rem_idx[rem] = i;
        }

        return N == ans ? -1 : ans;
    }
};
