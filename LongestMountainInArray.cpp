/**
 * Longest Mountain in Array
 *
 * You may recall that an array arr is a mountain array if and only if:
 *
 * arr.length >= 3
 * There exists some index i (0-indexed) with 0 < i < arr.length - 1 such that:
 * arr[0] < arr[1] < ... < arr[i - 1] < arr[i]
 * arr[i] > arr[i + 1] > ... > arr[arr.length - 1]
 * Given an integer array arr, return the length of the longest subarray, which is a mountain.
 * Return 0 if there is no mountain subarray.
 *
 * Example 1:
 *
 * Input: arr = [2,1,4,7,3,2,5]
 * Output: 5
 * Explanation: The largest mountain is [1,4,7,3,2] which has length 5.
 * Example 2:
 *
 * Input: arr = [2,2,2]
 * Output: 0
 * Explanation: There is no mountain.
 *
 * Constraints:
 *
 * 1 <= arr.length <= 104
 * 0 <= arr[i] <= 104
 *
 * Follow up:
 *
 * Can you solve it using only one pass?
 * Can you solve it in O(1) space?
 */
/**
 * Here comes the shorter one.
 */
class Solution {
public:
    int longestMountain(vector<int> A) {
        int ans = 0, up = 0, down = 0;

        for (int i = 1; i < A.size(); ++i) {
            if (down && (A[i - 1] < A[i] || A[i - 1] == A[i])) {
                up = down = 0; // reset
            }

            up += A[i - 1] < A[i];
            down += A[i - 1] > A[i];

            if (up && down) {
                ans = max(ans, up + down + 1);
            }
        }

        return ans;
    }
};

/**
 * Original solution. Though it works, I'm not satisfied with this one.
 */
class Solution {
public:
    int longestMountain(vector<int>& arr) {
        int const N = arr.size();
        if (N < 3) {
            return 0;
        }

        int ans = 0;;
        for (int i = 0; i < (N - 1); ) {
            auto left = find_left_valley(arr, i);
            if (left.first) {
                auto mid = find_peak(arr, i);
                if (mid.first) {
                    auto right = find_right_valley(arr, i);
                    if ((right.second - left.second) > 2) {
                        ans = max(ans, right.second - left.second);
                    }
                }
            }
        }

        return ans;
    }
private:
    pair<bool, int> find_left_valley(vector<int> const& arr, int& i)
    {
        int const N = arr.size() - 1;
        for (; i < N; ++i) {
            if (arr[i] < arr[i + 1]) {
                return {true, i};
            }
        }

        return {false, arr.size()};
    }

    pair<bool, int> find_peak(vector<int> const& arr, int& i) {
        int const N = arr.size() - 1;
        for (; i < N; ++i) {
            if (arr[i] > arr[i + 1]) {
                return {true, i};
            }
            else if (arr[i] == arr[i + 1]) {
                return {false, i};
            }
        }

        return {false, arr.size()};
    }

    pair<bool, int> find_right_valley(vector<int> const& arr, int& i)
    {
        int const N = arr.size() - 1;
        for (; i < N; ++i) {
            if (arr[i] <= arr[i + 1]) {
                return {true, i + 1};
            }
        }

        return {true, arr.size()};
    }
};
