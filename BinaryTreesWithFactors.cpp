/**
 * Binary Trees With Factors
 *
 * Given an array of unique integers, arr, where each integer arr[i] is strictly greater than 1.
 *
 * We make a binary tree using these integers, and each number may be used for any number of times.
 * Each non-leaf node's value should be equal to the product of the values of its children.
 *
 * Return the number of binary trees we can make. The answer may be too large so return the answer
 * modulo 109 + 7.
 *
 * Example 1:
 *
 * Input: arr = [2,4]
 * Output: 3
 * Explanation: We can make these trees: [2], [4], [4, 2, 2]
 * Example 2:
 *
 * Input: arr = [2,4,5,10]
 * Output: 7
 * Explanation: We can make these trees: [2], [4], [5], [10], [4, 2, 2], [10, 2, 5], [10, 5, 2].
 *
 * Constraints:
 *
 * 1 <= arr.length <= 1000
 * 2 <= arr[i] <= 109
 */
/**
 * More efficient one. Seems like unordered_map does not handle int key well.
 * So reduce the size can speed things up.
 */
class Solution {
public:
    int numFactoredBinaryTrees(vector<int>& arr) {
        int const mod = 1e9 + 7;
        sort(std::begin(arr), std::end(arr));
        vector<long> dp(arr.size(), 1);

        long ans = 0;
        unordered_map<int, int> index;
        for (int i = 0; i < arr.size(); ++i) {
            index[arr[i]] = i;
        }

        for (int i = 0; i < arr.size(); ++i) {
            for (int j = 0; j < i; ++j) {
                if (0 == (arr[i] % arr[j])) {
                    int k = arr[i] / arr[j];
                    if (index.count(k)) {
                        dp[i] = (dp[i] + dp[j] * dp[index[k]]) % mod;
                    }
                }
            }
            ans += dp[i];
        }

        return ans % mod;
    }

};

/**
 * Same idea, Using dp solution.
 */
class Solution {
public:
    int numFactoredBinaryTrees(vector<int>& arr) {
        int const mod = 1e9 + 7;
        sort(std::begin(arr), std::end(arr));

        long ans = 0;
        unordered_map<int, long> dp;
        for (int i = 0; i < arr.size(); ++i) {
            dp[arr[i]] = 1;
            for (int j = 0; j < i; ++j) {
                if (0 == (arr[i] % arr[j])) {
                    dp[arr[i]] = (dp[arr[i]] + dp[arr[j]] * dp[arr[i] / arr[j]]) % mod;
                }
            }
            ans = (ans + dp[arr[i]]) % mod;
        }

        return ans;
    }

};

/**
 * weird. I just reduce the size of the factors. And it passed.
 */
class Solution {
public:
    long const mod = 1e9 + 7;
    int numFactoredBinaryTrees(vector<int>& arr) {
        unordered_set<unsigned long long> nums{std::begin(arr), std::end(arr)};
        unordered_map<unsigned long long, vector<pair<int, int>>> factors;
        for (int i = 0; i < arr.size(); ++i) {
            for (int j = i; j < arr.size(); ++j) {
                unsigned long long prod = static_cast<unsigned long long>(arr[i]) * arr[j];
                if (nums.count(prod)) {
                    factors[prod].push_back({arr[i], arr[j]});
                }
            }
        }

        long ans = 0;
        unordered_map<int, int> memo;
        for (auto n : arr) {
            ans = (ans + dfs(factors, n, memo)) % mod;
        }

        return (ans + arr.size()) % mod;
    }

private:
    long dfs(unordered_map<unsigned long long, vector<pair<int, int>>>& factors, int n, unordered_map<int, int>& memo)
    {
        long ans = 0;
        if (memo.count(n)) {
            return memo[n];
        }

        if (factors.count(n)) {
            for (auto p : factors[n]) {
                if (p.first == p.second) {
                    ans = (ans + 1) % mod;
                    long left = dfs(factors, p.first, memo);
                    if (1 == left) {
                        ans = (ans + 3) % mod;
                    }
                    else if (left > 1) {
                        ans = (ans + left * (left + 2)) % mod;
                    }
                }
                else {
                    ans = (ans + 2) % mod;
                    long left = dfs(factors, p.first, memo);
                    long right = dfs(factors, p.second, memo);
                    if (0 == left && right > 0) {
                        ans = (ans + 2 * right) % mod;
                    }
                    else if (left > 0 && 0 == right) {
                        ans = (ans + 2 * left) % mod;
                    }
                    else if (left > 0 && right > 0) {
                        ans = (ans + 2 * (left + right) + 2 * left * right) % mod;
                    }
                }
            }
        }

        memo[n] = ans;
        return ans;
    }
};

/**
 * Original solution. TLE but I don't know how I can optimize this.
 */
class Solution {
public:
    long const mod = 1e9 + 7;
    int numFactoredBinaryTrees(vector<int>& arr) {
        unordered_map<unsigned long long, vector<pair<int, int>>> factors;
        for (int i = 0; i < arr.size(); ++i) {
            for (int j = i; j < arr.size(); ++j) {
                unsigned long long prod = static_cast<unsigned long long>(arr[i]) * arr[j];
                factors[prod].push_back({arr[i], arr[j]});
            }
        }

        long ans = 0;
        unordered_map<int, int> memo;
        for (auto n : arr) {
            ans = (ans + dfs(factors, n, memo)) % mod;
        }

        return (ans + arr.size()) % mod;
    }

private:
    long dfs(unordered_map<unsigned long long, vector<pair<int, int>>>& factors, int n, unordered_map<int, int>& memo)
    {
        long ans = 0;
        if (memo.count(n)) {
            return memo[n];
        }

        if (factors.count(n)) {
            for (auto p : factors[n]) {
                if (p.first == p.second) {
                    ans = (ans + 1) % mod;
                    long left = dfs(factors, p.first, memo);
                    if (1 == left) {
                        ans = (ans + 3) % mod;
                    }
                    else if (left > 1) {
                        ans = (ans + left * (left + 2)) % mod;
                    }
                }
                else {
                    ans = (ans + 2) % mod;
                    long left = dfs(factors, p.first, memo);
                    long right = dfs(factors, p.second, memo);
                    if (0 == left && right > 0) {
                        ans = (ans + 2 * right) % mod;
                    }
                    else if (left > 0 && 0 == right) {
                        ans = (ans + 2 * left) % mod;
                    }
                    else if (left > 0 && right > 0) {
                        ans = (ans + 2 * (left + right) + 2 * left * right) % mod;
                    }
                }
            }
        }

        memo[n] = ans;
        return ans;
    }
};
