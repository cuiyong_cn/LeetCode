/**
 * Ugly Number
 *
 * Given an integer n, return true if n is an ugly number.
 *
 * Ugly number is a positive number whose prime factors only include 2, 3, and/or 5.
 *
 * Example 1:
 *
 * Input: n = 6
 * Output: true
 * Explanation: 6 = 2 × 3
 * Example 2:
 *
 * Input: n = 8
 * Output: true
 * Explanation: 8 = 2 × 2 × 2
 * Example 3:
 *
 * Input: n = 14
 * Output: false
 * Explanation: 14 is not ugly since it includes another prime factor 7.
 * Example 4:
 *
 * Input: n = 1
 * Output: true
 * Explanation: 1 is typically treated as an ugly number.
 *
 * Constraints:
 *
 * -231 <= n <= 231 - 1
 */
/**
 * A little improvement.
 */
template<typename T, size_t N>
size_t array_size(T const (&)[N])
{
    return N;
}

class Solution {
public:
    bool isUgly(int n) {
        if (n <= 0) {
            return false;
        }

        int const primes[] = {2, 3, 5};
        auto N = array_size(primes);
        for (int i = 0; i < N; ++i) {
            while (0 == (n % primes[i])) {
                n /= primes[i];
            }
        }

        return 1 == n;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool isUgly(int n) {
        if (n <= 0) {
            return false;
        }

        while (n != 1) {
            if (0 == (n % 2)) {
                n /= 2;
            }
            else if (0 == (n % 3)) {
                n /= 3;
            }
            else if (0 == (n % 5)) {
                n /= 5;
            }
            else {
                break;
            }
        }

        return 1 == n;
    }
};
