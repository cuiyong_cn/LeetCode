/**
 * Split Array into Fibonacci Sequence
 *
 * You are given a string of digits num, such as "123456579". We can split it into a Fibonacci-like
 * sequence [123, 456, 579].
 *
 * Formally, a Fibonacci-like sequence is a list f of non-negative integers such that:
 *
 * 0 <= f[i] < 231, (that is, each integer fits in a 32-bit signed integer type), f.length >= 3, and
 * f[i] + f[i + 1] == f[i + 2] for all 0 <= i < f.length - 2.  Note that when splitting the string
 * into pieces, each piece must not have extra leading zeroes, except if the piece is the number 0
 * itself.
 *
 * Return any Fibonacci-like sequence split from num, or return [] if it cannot be done.
 *
 * Example 1:
 *
 * Input: num = "123456579"
 * Output: [123,456,579]
 * Example 2:
 *
 * Input: num = "11235813"
 * Output: [1,1,2,3,5,8,13]
 * Example 3:
 *
 * Input: num = "112358130"
 * Output: []
 * Explanation: The task is impossible.
 * Example 4:
 *
 * Input: num = "0123"
 * Output: []
 * Explanation: Leading zeroes are not allowed, so "01", "2", "3" is not valid.
 * Example 5:
 *
 * Input: num = "1101111"
 * Output: [11,0,11,11]
 * Explanation: The output [11, 0, 11, 11] would also be accepted.
 *
 * Constraints:
 *
 * 1 <= num.length <= 200
 * num contains only digits.
 */
/**
 * Original solution. Pretty long ass answer, we can improve it.
 */
class Solution {
public:
    vector<int> splitIntoFibonacci(string num) {
        if (num.length() < 3) {
            return {};
        }

        bool found = false;
        vector<int> ans;
        // fibonacci sequence is determined by the first two integers
        if ('0' == num[0]) {
            ans.push_back(0);
            if ('0' == num[1]) {
                ans.push_back(0);
                if (dfs(num, 2, 0, 0, ans)) {
                    found = true;
                }
            }
            else {
                long long n2 = 0;
                for (int j = 1; j < (num.length() - 1); ++j) {
                    n2 = n2 * 10 + (num[j] - '0');
                    if (n2 > numeric_limits<int>::max()) {
                        break;
                    }
                    ans.push_back(n2);
                    if (dfs(num, j + 1, 0, n2, ans)) {
                        found = true;
                        break;
                    }
                    ans.pop_back();
                }
            }
        }
        else {
            for (int i = 1; i < num.length(); ++i) {
                long long n1 = stol(num.substr(0, i));
                if (n1 >= numeric_limits<int>::max()) {
                    break;
                }

                ans.push_back(n1);
                if ('0' == num[i]) {
                    ans.push_back(0);
                    if (dfs(num, i + 1, n1, 0, ans)) {
                        found = true;
                        break;
                    }
                    ans.pop_back();
                }
                else {
                    long long n2 = 0;
                    for (int j = i; j < (num.length() - 1); ++j) {
                        n2 = n2 * 10 + (num[j] - '0');
                        if (n2 > numeric_limits<int>::max()) {
                            break;
                        }
                        ans.push_back(n2);
                        if (dfs(num, j + 1, n1, n2, ans)) {
                            found = true;
                            break;
                        }
                        ans.pop_back();
                    }
                }
                if (found) {
                    break;
                }
                ans.pop_back();
            }
        }

        if (!found) {
            ans.clear();
        }

        return ans;
    }

private:
    bool dfs(string const& num, int pos, long long n1, long long n2, vector<int>& ans)
    {
        if (pos >= num.length()) {
            return ans.size() > 2;
        }

        auto target = n1 + n2;
        if (target > numeric_limits<int>::max()) {
            return false;
        }

        if ('0' == num[pos]) {
            if (0 == (n1 + n2)) {
                ans.push_back(0);
                if (dfs(num, pos + 1, n2, 0, ans)) {
                    return true;
                }
                ans.pop_back();
            }
            return false;
        }

        long long n3 = 0;
        for (int i = pos; i < num.length(); ++i) {
            n3 = n3 * 10 + (num[i] - '0');
            if (n3 > (n1 + n2)) {
                return false;
            }

            if ((n1 + n2) == n3) {
                ans.push_back(n3);
                if (dfs(num, i + 1, n2, n3, ans)) {
                    return true;
                }
                ans.pop_back();
            }
        }

        return false;
    }
};
