/**
 * Divide Array Into Equal Pairs
 *
 * You are given an integer array nums consisting of 2 * n integers.
 *
 * You need to divide nums into n pairs such that:
 *
 * Each element belongs to exactly one pair.
 * The elements present in a pair are equal.
 * Return true if nums can be divided into n pairs, otherwise return false.
 *
 * Example 1:
 *
 * Input: nums = [3,2,3,2,2,2]
 * Output: true
 * Explanation:
 * There are 6 elements in nums, so they should be divided into 6 / 2 = 3 pairs.
 * If nums is divided into the pairs (2, 2), (3, 3), and (2, 2), it will satisfy all the conditions.
 * Example 2:
 *
 * Input: nums = [1,2,3,4]
 * Output: false
 * Explanation:
 * There is no way to divide nums into 4 / 2 = 2 pairs such that the pairs satisfy every condition.
 *
 * Constraints:
 *
 * nums.length == 2 * n
 * 1 <= n <= 500
 * 1 <= nums[i] <= 500
 */
/**
 * More efficient and memory friendly.
 */
class Solution {
public:
    bool divideArray(vector<int>& nums) {
        auto coins = bitset<501>{};

        for (auto n : nums) {
            coins[n].flip();
        }

        return coins.none();
    }
};

/**
 * A little improve.
 */
class Solution {
public:
    bool divideArray(vector<int>& nums) {
        auto num_cnt = vector<int>(501, 0);

        for (auto n : nums) {
            ++num_cnt[n];
        }

        return all_of(begin(num_cnt), end(num_cnt),
            [](auto cnt) {
                return 0 == (cnt % 2);
            });
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool divideArray(vector<int>& nums) {
        auto num_cnt = vector<int>(501, 0);

        for (auto n : nums) {
            ++num_cnt[n];
        }

        auto odd_cnt = find_if(begin(num_cnt), end(num_cnt),
            [](auto cnt) {
                return 1 == (cnt % 2);
            });
        return odd_cnt == end(num_cnt);
    }
};
