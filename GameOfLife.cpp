/**
 * Game of Life
 *
 * According to the Wikipedia's article: "The Game of Life, also known simply as Life, is a cellular automaton devised
 * by the British mathematician John Horton Conway in 1970."
 *
 * Given a board with m by n cells, each cell has an initial state live (1) or dead (0). Each cell interacts with its
 * eight neighbors (horizontal, vertical, diagonal) using the following four rules (taken from the above Wikipedia
 * article):
 *
 *     Any live cell with fewer than two live neighbors dies, as if caused by under-population.  Any live cell with two
 *     or three live neighbors lives on to the next generation.  Any live cell with more than three live neighbors dies,
 *     as if by over-population..  Any dead cell with exactly three live neighbors becomes a live cell, as if by
 *     reproduction.
 *
 * Write a function to compute the next state (after one update) of the board given its current state. The next state is
 * created by applying the above rules simultaneously to every cell in the current state, where births and deaths occur
 * simultaneously.
 *
 * Example:
 *
 * Input:
 * [
 *   [0,1,0],
 *   [0,0,1],
 *   [1,1,1],
 *   [0,0,0]
 * ]
 * Output:
 * [
 *   [0,0,0],
 *   [1,0,1],
 *   [0,1,1],
 *   [0,1,0]
 * ]
 *
 * Follow up:
 *     Could you solve it in-place? Remember that the board needs to be updated at the same time: You cannot update some
 *     cells first and then use their updated values to update other cells.  In this question, we represent the board
 *     using a 2D array. In principle, the board is infinite, which would cause problems when the active area encroaches
 *     the border of the array. How would you address these problems?
 */
/**
 * original solution. As for the in-place method, we can actully give each cell a new value to represent the new state.
 * Like -1 for live to dead, 2 for dead to live.
 */
class Solution {
public:
    vector<vector<int>> org;
    vector<int> dir;
    int m;
    int n;

    void update(vector<vector<int>>& board, int row, int col) {
        bool dead = board[row][col] == 0 ? true : false;
        int livesAround = 0;

        for (int i = 1; i < dir.size(); ++i) {
            int nr = dir[i - 1] + row;
            int nc = dir[i] + col;
            if (0 <= nr && nr < m
               && 0 <= nc && nc < n) {
                livesAround += org[nr][nc] == 1 ? 1 : 0;
            }
        }
        //cout << "(" << row << ", " << col << ") = " << livesAround << endl;
        if (livesAround < 2 || livesAround > 3) {
            board[row][col] = 0;
        }
        else if (livesAround == 3) {
            if (dead) {
                board[row][col] = 1;
            }
        }
    }

    void gameOfLife(vector<vector<int>>& board) {
        if (board.empty()) return;
        dir = {0, -1, -1, 0, 1, 1, -1, 1, 0};
        org = board;
        m = board.size();
        n = board[0].size();

        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                update(board, i, j);
            }
        }
    }
};
