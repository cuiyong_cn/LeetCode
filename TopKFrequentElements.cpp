/**
 * Top K Frequent Elements
 *
 * Given a non-empty array of integers, return the k most frequent elements.
 *
 * Example 1:
 *
 * Input: nums = [1,1,1,2,2,3], k = 2
 * Output: [1,2]
 *
 * Example 2:
 *
 * Input: nums = [1], k = 1
 * Output: [1]
 *
 * Note:
 *
 *     You may assume k is always valid, 1 ≤ k ≤ number of unique elements.
 *     Your algorithm's time complexity must be better than O(n log n), where n is the array's size.
 */
/**
 * My original solution. Simple and straight.
 */
class Solution {
public:
    vector<int> topKFrequent(vector<int>& nums, int k) {
        unordered_map<int, int> freqMap;
        for (auto& n : nums) { ++freqMap[n]; }
        multimap<int, int, greater<int>> orderedFreqMap;
        for (auto& m : freqMap) { orderedFreqMap.insert({m.second, m.first}); }

        int count = 0;
        vector<int> ans;
        for (auto& m : orderedFreqMap) {
            if (++count <= k) {
                ans.push_back(m.second);
            }
            else {
                break;
            }
        }
        return ans;
    }
};

/**
 * Priority queue solution.
 */
class Solution {
public:
    vector<int> topKFrequent(vector<int>& nums, int k) {
        unordered_map<int, int> counts;
        priority_queue<int, vector<int>, greater<int>> max_k;
        for(auto i : nums) ++counts[i];
        for(auto & i : counts) {
            max_k.push(i.second);
            // Size of the min heap is maintained at equal to or below k
            while(max_k.size() > k) max_k.pop();
        }
        vector<int> res;
        for(auto & i : counts) {
            if(i.second >= max_k.top()) res.push_back(i.first);
        }
        return res;
    }
};

/**
 * Bucket sort
 */
class Solution {
public:
    vector<int> topKFrequent(vector<int>& nums, int k) {
        unordered_map<int, int> counts;
        for(auto i : nums) ++counts[i];

        vector<vector<int>> buckets(nums.size() + 1);
        for(auto & k : counts)
            buckets[k.second].push_back(k.first);
        reverse(begin(buckets), end(buckets)); // this line can be ignored. we can use cbegin()

        vector<int> res;
        for(auto & bucket: buckets)
            for(auto i : bucket) {
                res.push_back(i);
                if(res.size() == k) return res;
            }

        return res;
    }
};
