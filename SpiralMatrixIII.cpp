/**
 * Spiral Matrix III
 *
 * On a 2 dimensional grid with R rows and C columns, we start at (r0, c0) facing east.
 *
 * Here, the north-west corner of the grid is at the first row and column, and the
 * south-east corner of the grid is at the last row and column.
 *
 * Now, we walk in a clockwise spiral shape to visit every position in this grid.
 *
 * Whenever we would move outside the boundary of the grid, we continue our walk
 * outside the grid (but may return to the grid boundary later.)
 *
 * Eventually, we reach all R * C spaces of the grid.
 *
 * Return a list of coordinates representing the positions of the grid in the order
 * they were visited.
 *
 * Example 1:
 *
 * Input: R = 1, C = 4, r0 = 0, c0 = 0
 * Output: [[0,0],[0,1],[0,2],[0,3]]
 *
 * Example 2:
 *
 * Input: R = 5, C = 6, r0 = 1, c0 = 4
 * Output: [[1,4],[1,5],[2,5],[2,4],[2,3],[1,3],[0,3],[0,4],[0,5],[3,5],[3,4],[3,3],[3,2],[2,2],[1,2],[0,2],[4,5],[4,4],[4,3],[4,2],[4,1],[3,1],[2,1],[1,1],[0,1],[4,0],[3,0],[2,0],[1,0],[0,0]]
 *
 * Note:
 *
 *     1 <= R <= 100
 *     1 <= C <= 100
 *     0 <= r0 < R
 *     0 <= c0 < C
 */
class Solution {
public:
    enum Direction { E = 0, S, W, N };
    vector<vector<int>> spiralMatrixIII(int R, int C, int r0, int c0) {
        int step = 0;
        Direction d = E;
        int visited = 0;
        int total = R * C;
        vector<vector<int>> ans;
        ans.push_back({r0, c0});
        ++visited;
        int cOutBound = 0;
        int rOutBound = 0;
        while (visited < total) {
            Direction next;
            switch (d) {
            case E:
                ++step; next = S; break;
            case S:
                next = W; break;
            case W:
                ++step; next = N; break;
            case N:
                next = E; break;
            default:
                break;
            }

            for (int i = 0; i < step; ++i) {
                switch (d) {
                case E:
                    if ((c0 + 1) < C && !cOutBound) ++c0;
                    else ++cOutBound;
                    break;
                case S:
                    if ((r0 + 1) < R && !rOutBound) ++r0;
                    else ++rOutBound;
                    break;
                case W:
                    if (c0 > 0 && !cOutBound) --c0;
                    else --cOutBound;
                    break;
                case N:
                    if (r0 > 0 && !rOutBound) --r0;
                    else --rOutBound;
                }
                if (!rOutBound && !cOutBound) {
                    ans.push_back({r0, c0});
                    ++visited;
                }
            }
            d = next;
        }
        return ans;
    }
};

/**
 * shorter one
 */
class Solution {
public:
    vector<vector<int>> spiralMatrixIII(int R, int C, int r, int c) {
        vector<vector<int>> res = {{r, c}};
        int dx = 0, dy = 1, tmp;
        for (int n = 0; res.size() < R * C; n++) {
            for (int i = 0; i < n / 2 + 1; i++) {
                r += dx, c += dy;
                if (0 <= r && r < R && 0 <= c && c < C)
                    res.push_back({r, c});
            }
            tmp = dx, dx = dy, dy = -tmp;
        }
        return res;
    }
};
