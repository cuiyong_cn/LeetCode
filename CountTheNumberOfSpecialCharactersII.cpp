/**
 * Count the Number of Special Characters II
 *
 * You are given a string word. A letter c is called special if it appears both in lowercase and
 * uppercase in word, and every lowercase occurrence of c appears before the first uppercase
 * occurrence of c.
 *
 * Return the number of special letters in word.
 *
 * Example 1:
 *
 * Input: word = "aaAbcBC"
 *
 * Output: 3
 *
 * Explanation:
 *
 * The special characters are 'a', 'b', and 'c'.
 *
 * Example 2:
 *
 * Input: word = "abc"
 *
 * Output: 0
 *
 * Explanation:
 *
 * There are no special characters in word.
 *
 * Example 3:
 *
 * Input: word = "AbBCab"
 *
 * Output: 0
 *
 * Explanation:
 *
 * There are no special characters in word.
 *
 * Constraints:
 *     1 <= word.length <= 2 * 105
 *     word consists of only lowercase and uppercase English letters.
 */
/**
 * More simple one.
 */
class Solution {
public:
    int numberOfSpecialChars(string word) {
        auto lo = bitset<26>{};
        auto up = lo;

        for (char ch : word) {
            if (islower(ch)) {
                lo[ch - 'a'] = !up[ch - 'a'];
            }
            else {
                up[ch - 'A'] = true;
            }
        }

        return (lo & up).count();
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int numberOfSpecialChars(string word) {
        auto lower_pos = vector<int>(26, -1);
        auto upper_pos = lower_pos;
        int const n = word.length();

        for (auto i = 0; i < n; ++i) {
            if (islower(word[i])) {
                lower_pos[word[i] - 'a'] = i;
            }
            else {
                if (upper_pos[word[i] - 'A'] == -1) {
                    upper_pos[word[i] - 'A'] = i;
                }
            }
        }

        auto ans = 0;

        for (auto i = 0; i < 26; ++i) {
            if (lower_pos[i] >= 0 && upper_pos[i] >= 0 && upper_pos[i] > lower_pos[i]) {
                ++ans;
            }
        }

        return ans;
    }
};
