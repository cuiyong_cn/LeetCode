/**
 * Count Number of Teams
 *
 * There are n soldiers standing in a line. Each soldier is assigned a unique rating value.
 *
 * You have to form a team of 3 soldiers amongst them under the following rules:
 *
 * Choose 3 soldiers with index (i, j, k) with rating (rating[i], rating[j], rating[k]).  A team is
 * valid if: (rating[i] < rating[j] < rating[k]) or (rating[i] > rating[j] > rating[k]) where (0 <=
 * i < j < k < n).  Return the number of teams you can form given the conditions. (soldiers can be
 * part of multiple teams).
 *
 * Example 1:
 *
 * Input: rating = [2,5,3,4,1]
 * Output: 3
 * Explanation: We can form three teams given the conditions. (2,3,4), (5,4,1), (5,3,1).
 * Example 2:
 *
 * Input: rating = [2,1,3]
 * Output: 0
 * Explanation: We can't form any team given the conditions.
 * Example 3:
 *
 * Input: rating = [1,2,3,4]
 * Output: 4
 *
 * Constraints:
 *
 * n == rating.length
 * 3 <= n <= 1000
 * 1 <= rating[i] <= 105
 * All the integers in rating are unique.
 */
/**
 * Blazingly fast.
 */
class Solution {
public:
    using ui32=uint32_t;
    using ui64=uint64_t;

    void update(std::vector<ui32>& v, ui32 i, ui32 val)
    {
        while (i < v.size()) {
            v[i] += val;
            i += i&(-i);
        }
    }

    ui32 read(const std::vector<ui32>& v, ui32 i)
    {
        ui32 sum = 0;
        while (i > 0) {
            sum += v[i];
            i -= i&(-i);
        }
        return sum;
    }

    ui32 find(const std::vector<int>& v, int val)
    {
        ui32 b = 0, e = v.size() - 1;
        while (b < e) {
            ui32 mid = b + (e - b) / 2;
            if (v[mid] < val) {
                b = mid + 1;
                continue;
            }

            if(v[mid] > val){
                e = mid - 1;
                continue;
            }
            return mid;
        }

        return b;
    }

public:
    /* O(n log n) */
    ui64 numTeams(const std::vector<int>& rating) {
        const ui32 n = rating.size();

        std::vector<int> sorted(rating.begin(), rating.end());
        std::sort(sorted.begin(), sorted.end());

        std::vector<ui32> before(n+1), after(n+1);
        for (ui32 x = 1; x <= n; ++x) {
            update(after, x, 1);
        }

        ui64 cnt = 0;
        for (ui32 i = 0;i<n;++i) {
            int x = 1 + find(sorted, rating[i]);
            int b = read(before, x);
            int a = read(after, x);

            cnt += b * (n - i - a) + (i - b)*(a - 1);
            update(before, x, 1);
            update(after, x, -1);
        }

        return cnt;
    }
};

/**
 * Without the memo
 */
class Solution {
public:
    int numTeams(vector<int>& rating) {
        int res = 0;
        for (auto i = 1; i < rating.size() - 1; ++i) {
            int less[2] = {}, greater[2] = {};
            for (auto j = 0; j < rating.size(); ++j) {
                if (rating[i] < rating[j])
                    ++less[j > i];
                if (rating[i] > rating[j])
                    ++greater[j > i];
            }
            res += less[0] * greater[1] + greater[0] * less[1];
        }
        return res;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int numTeams(vector<int>& rating) {
        int ans = 0;
        unordered_map<int, pair<int, int>> memo;
        int const N = rating.size();
        for (int i = 1; i < N; ++i) {
            int gt = 0, lt = 0;
            for (int j = i + 1; j < N; ++j) {
                if (rating[i] < rating[j]) {
                    ++gt;
                }
                else {
                    ++lt;
                }
            }
            memo[rating[i]] = {gt, lt};
        }

        for (int i = 0; i < N; ++i) {
            for (int j = i + 1; j < N; ++j) {
                if (rating[i] < rating[j]) {
                    ans += memo[rating[j]].first;
                }
                else {
                    ans += memo[rating[j]].second;
                }
            }
        }

        return ans;
    }
};
