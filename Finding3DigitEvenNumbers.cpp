/**
 * Finding 3-Digit Even Numbers
 *
 * You are given an integer array digits, where each element is a digit. The array may contain
 * duplicates.
 *
 * You need to find all the unique integers that follow the given requirements:
 *
 * The integer consists of the concatenation of three elements from digits in any arbitrary order.
 * The integer does not have leading zeros.
 * The integer is even.
 * For example, if the given digits were [1, 2, 3], integers 132 and 312 follow the requirements.
 *
 * Return a sorted array of the unique integers.
 *
 * Example 1:
 *
 * Input: digits = [2,1,3,0]
 * Output: [102,120,130,132,210,230,302,310,312,320]
 * Explanation: All the possible integers that follow the requirements are in the output array.
 * Notice that there are no odd integers or integers with leading zeros.
 * Example 2:
 *
 * Input: digits = [2,2,8,8,2]
 * Output: [222,228,282,288,822,828,882]
 * Explanation: The same digit can be used as many times as it appears in digits.
 * In this example, the digit 8 is used twice each time in 288, 828, and 882.
 * Example 3:
 *
 * Input: digits = [3,7,5]
 * Output: []
 * Explanation: No even integers can be formed using the given digits.
 *
 * Constraints:
 *
 * 3 <= digits.length <= 100
 * 0 <= digits[i] <= 9
 */
/**
 * Improve below.
 */
class Solution {
public:
    vector<int> findEvenNumbers(vector<int>& digits) {
        int digit_cnt[10]{ 0, };
        for (auto const& n : digits) {
            ++digit_cnt[n];
        }

        dfs(1, 10, digit_cnt, 0);

        return ans;
    }

private:
    void dfs(int s, int e, int* dc, int num)
    {
        if (num >= 100) {
            if (0 == (num & 1)) {
                ans.emplace_back(num);
            }
            return;
        }

        for (auto i = s; i < e; ++i) {
            if (0 == dc[i]) {
                continue;
            }
            --dc[i];
            dfs(0, 10, dc, num * 10 + i);
            ++dc[i];
        }
    }

private:
    vector<int> ans;
};

/**
 * Three loops.
 */
class Solution {
public:
    vector<int> findEvenNumbers(vector<int>& digits) {
        auto ans = vector<int>{};
        int digit_cnt[10]{ 0, };
        for (auto const& n : digits) {
            ++digit_cnt[n];
        }

        for (auto i = int{1}; i < 10; ++i) {
            if (0 == digit_cnt[i]) {
                continue;
            }
            --digit_cnt[i];
            for (auto j = int{0}; j < 10; ++j) {
                if (0 == digit_cnt[j]) {
                    continue;
                }
                --digit_cnt[j];
                for (auto k = int{0}; k < 10; ++k) {
                    if (0 == digit_cnt[k]) {
                        continue;
                    }
                    auto num = i * 100 + j * 10 + k;
                    if (num & 1) {
                        continue;
                    }
                    ans.emplace_back(num);
                }
                ++digit_cnt[j];
            }
            ++digit_cnt[i];
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<int> findEvenNumbers(vector<int>& digits) {
        auto ans = vector<int>{};
        int digit_cnt[10]{ 0, };
        for (auto const& n : digits) {
            ++digit_cnt[n];
        }

        for (auto n = int{100}; n < 1000; n += 2) {
            int dc[10]{ 0, };
            for (auto k = n; k > 0; k /= 10) {
                ++dc[k % 10];
            }

            auto i = int{0};
            for (; i < 10; ++i) {
                if (digit_cnt[i] < dc[i]) {
                    break;
                }
            }

            if (10 == i) {
                ans.emplace_back(n);
            }
        }

        return ans;
    }
};
