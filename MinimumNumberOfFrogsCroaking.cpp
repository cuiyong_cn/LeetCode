/**
 * Minimum Number of Frogs Croaking
 *
 * Given the string croakOfFrogs, which represents a combination of the string "croak" from different frogs, that is,
 * multiple frogs can croak at the same time, so multiple “croak” are mixed. Return the minimum number of different
 * frogs to finish all the croak in the given string.
 *
 * A valid "croak" means a frog is printing 5 letters ‘c’, ’r’, ’o’, ’a’, ’k’ sequentially. The frogs have to print all
 * five letters to finish a croak. If the given string is not a combination of valid "croak" return -1.
 *
 * Example 1:
 *
 * Input: croakOfFrogs = "croakcroak"
 * Output: 1
 * Explanation: One frog yelling "croak" twice.
 * Example 2:
 *
 * Input: croakOfFrogs = "crcoakroak"
 * Output: 2
 * Explanation: The minimum number of frogs is two.
 * The first frog could yell "crcoakroak".
 * The second frog could yell later "crcoakroak".
 * Example 3:
 *
 * Input: croakOfFrogs = "croakcrook"
 * Output: -1
 * Explanation: The given string is an invalid combination of "croak" from different frogs.
 * Example 4:
 *
 * Input: croakOfFrogs = "croakcroa"
 * Output: -1
 *
 * Constraints:
 *
 * 1 <= croakOfFrogs.length <= 10^5
 * All characters in the string are: 'c', 'r', 'o', 'a' or 'k'.
 */
/**
 * Improve the coding.
 */
class Solution {
public:
    int minNumberOfFrogs(string croakOfFrogs) {
        int cnt[5] = {}, frogs = 0, max_frogs = 0;
        string sing = "croak";

        for (auto ch : croakOfFrogs) {
            auto n = sing.find(ch);
            ++cnt[n];
            if (n == 0) {
                max_frogs = max(max_frogs, ++frogs);
            }
            else if (--cnt[n - 1] < 0) {
                return -1;
            }
            else if (n == 4) {
                --frogs;
            }
        }

        // frogs > 0 means some frog did not finish its singing
        return frogs == 0 ? max_frogs : -1;
    }
};

/**
 * we can observe that count['c'] >= count['r'] >= count['o'] >= count['a'] >= count['k']
 * Therefore, come up with the following solution.
 */
class Solution {
public:
    int minNumberOfFrogs(string croakOfFrogs) {
        if (croakOfFrogs.length() % 5) {
            return -1;
        }

        vector<int> count(26, 0);
        int ans = 0;
        int rest_frogs = 0;
        for (auto ch : croakOfFrogs) {
            switch (ch) {
                case 'c':
                    if (rest_frogs > 0) {
                        --rest_frogs;
                    }
                    else {
                        ++ans;
                    }
                    break;
                case 'r':
                    if (count['r' - 'a'] >= count['c' - 'a']) {
                        return -1;
                    }
                    break;
                case 'o':
                    if (count['o' - 'a'] >= count['r' - 'a']) {
                        return -1;
                    }
                    break;
                case 'a':
                    if (count['a' - 'a'] >= count['o' - 'a']) {
                        return -1;
                    }
                    break;
                case 'k':
                    if (count['k' - 'a'] >= count['a' - 'a']) {
                        return -1;
                    }
                    ++rest_frogs;
                    break;
                default:
                    break;
            }
            ++count[ch - 'a'];
        }

        string croak = "croak";
        for (int i = 1; i < croak.length(); ++i) {
            if (count[croak[i] - 'a'] != count[croak[i - 1] - 'a']) {
                return - 1;
            }
        }

        return ans;
    }
};

/**
 * Original solution. Not elegant but correct, and sadly TLE.
 * The idea is:
 *   1  if found a start 'c', then eliminate all the "croak" belong to this frog.
 *   2  start from begin, repeat step 1 for a new frog.
 *
 * As this is a TLE one. We need to find a way to speed things up.
 */
class Solution {
public:
    int minNumberOfFrogs(string croakOfFrogs) {
        if (croakOfFrogs.length() % 5) {
            return -1;
        }

        used = vector<bool>(croakOfFrogs.length(), false);
        croak = "croak";

        int ans = 0;
        int count = 0;

        while (1) {
            bool found = false;
            for (int i = 0; i < croakOfFrogs.length(); ++i) {
                if (!used[i] && 'c' == croakOfFrogs[i]) {
                    auto [next, valid] = find_croak(croakOfFrogs, i);
                    if (!valid) {
                        return -1;
                    }
                    i = next - 1;
                    count += croak.length();
                    found = true;
                }
            }

            if (found) {
                ++ans;
            }
            else if (count < croakOfFrogs.length()) {
                return -1;
            }
            else {
                break;
            }
        }

        return ans;
    }

private:
    pair<int, bool> find_croak(const string& croakOfFrogs, int start)
    {
        int i = start;
        int j = 0;
        for (; i < croakOfFrogs.length() && j < croak.length(); ++i) {
            if (!used[i] && croakOfFrogs[i] == croak[j]) {
                ++j;
                used[i] = true;
            }
        }

        return {i , j == croak.length()};
    }

    vector<bool> used;
    string croak;
};
