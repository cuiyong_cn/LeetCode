/**
 * Find the Integer Added to Array II
 *
 * You are given two integer arrays nums1 and nums2.
 *
 * From nums1 two elements have been removed, and all other elements have been increased (or
 * decreased in the case of negative) by an integer, represented by the variable x.
 *
 * As a result, nums1 becomes equal to nums2. Two arrays are considered equal when they contain the
 * same integers with the same frequencies.
 *
 * Return the minimum possible integer x that achieves this equivalence.
 *
 * Example 1:
 *
 * Input: nums1 = [4,20,16,12,8], nums2 = [14,18,10]
 *
 * Output: -2
 *
 * Explanation:
 *
 * After removing elements at indices [0,4] and adding -2, nums1 becomes [18,14,10].
 *
 * Example 2:
 *
 * Input: nums1 = [3,5,5,3], nums2 = [7,7]
 *
 * Output: 2
 *
 * Explanation:
 *
 * After removing elements at indices [0,3] and adding 2, nums1 becomes [7,7].
 *
 * Constraints:
 *     3 <= nums1.length <= 200
 *     nums2.length == nums1.length - 2
 *     0 <= nums1[i], nums2[i] <= 1000
 *     The test cases are generated in a way that there is an integer x such that nums1 can become equal to nums2 by removing two elements and adding x to each element of nums1.
 */
/**
 * Original solution.
 *     Observation:
 *          1. If nums1[0] not removed, this means we have to remove nums1[i], nums1[j] (i != j)
 *             and the rest match, answer will be nums2[0] - nums1[0]
 *          2. If nums1[0] be removed, this means we have to remove nums1[i] (1 <= i < nums1.size())
 *             and the rest match, answer will be nums2[0] - nums1[1] or nums2[0] - nums[2] (first two removed)
 *
 * Based on above observation, we can only check if observation 2 can be fullfilled.
 */
class Solution {
public:
    int minimumAddedInteger(vector<int>& nums1, vector<int>& nums2) {
        std::ranges::sort(nums1);
        std::ranges::sort(nums2);

        for (auto i : {2, 1}) {
            auto skip = i;
            auto const diff = nums2[0] - nums1[i];
            int const n2size = nums2.size();

            for (auto j = i + 1; skip < 3 && (j - skip) < n2size; ++j) {
                if ((nums2[j - skip] - nums1[j]) != diff) {
                    ++skip;
                }
            }

            if (skip < 3) {
                return diff;
            }
        }

        return nums2[0] - nums1[0];
    }
};
