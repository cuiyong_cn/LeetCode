/**
 * The k-th Lexicographical String of All Happy Strings of Length n
 *
 * A happy string is a string that:
 *
 * consists only of letters of the set ['a', 'b', 'c'].  s[i] != s[i + 1] for all values of i from 1
 * to s.length - 1 (string is 1-indexed).  For example, strings "abc", "ac", "b" and "abcbabcbcb"
 * are all happy strings and strings "aa", "baa" and "ababbc" are not happy strings.
 *
 * Given two integers n and k, consider a list of all happy strings of length n sorted in
 * lexicographical order.
 *
 * Return the kth string of this list or return an empty string if there are less than k happy
 * strings of length n.
 *
 * Example 1:
 *
 * Input: n = 1, k = 3
 * Output: "c"
 * Explanation: The list ["a", "b", "c"] contains all happy strings of length 1. The third string is "c".
 * Example 2:
 *
 * Input: n = 1, k = 4
 * Output: ""
 * Explanation: There are only 3 happy strings of length 1.
 * Example 3:
 *
 * Input: n = 3, k = 9
 * Output: "cab"
 * Explanation: There are 12 different happy string of length 3 ["aba", "abc", "aca", "acb", "bab", "bac", "bca", "bcb", "cab", "cac", "cba", "cbc"]. You will find the 9th string = "cab"
 *
 * Constraints:
 *
 * 1 <= n <= 10
 * 1 <= k <= 100
 */
/**
 * Math solution.
 */
class Solution {
public:
    string getHappyString(int n, int k) {
        auto prem = 1 << (n - 1);
        if (k > 3 * prem) {
            return "";
        }

        auto s = string(1, 'a' + (k - 1) / prem);
        while (prem > 1) {
            k = (k - 1) % prem + 1;
            prem >>= 1;
            s += (k - 1) / prem == 0 ? 'a' + (s.back() == 'a') : 'b' + (s.back() != 'c');
        }

        return s;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    string getHappyString(int n, int k) {
        idx = k;

        dfs(n, -1, 0);

        if (idx > 0) {
            return "";
        }

        return decode(ans, n);
    }

private:
    bool dfs(int len, int prev, int str)
    {
        if (0 == idx) {
            return false;
        }

        if (0 == len) {
            --idx;
            ans = str;

            return true;
        }

        int const pos = len << 1;
        for (int i = 0; i < 3; ++i) {
            if (i == prev) {
                continue;
            }

            if (false == dfs(len - 1, i, str | (i << pos))) {
                return false;
            }
        }

        return true;
    }

    string decode(int enc, int n)
    {
        string str;
        int const mask = 0x03;
        for (int i = n; i > 0; --i) {
            auto shft = i << 1;
            auto c = (enc & (mask << shft)) >> shft;
            str += 'a' + c;
        }

        return str;
    }

    int ans;
    int idx;
};
