/**
 * Detonate the Maximum Bombs
 *
 * You are given a list of bombs. The range of a bomb is defined as the area where its effect can be
 * felt. This area is in the shape of a circle with the center as the location of the bomb.
 *
 * The bombs are represented by a 0-indexed 2D integer array bombs where bombs[i] = [xi, yi, ri]. xi
 * and yi denote the X-coordinate and Y-coordinate of the location of the ith bomb, whereas ri
 * denotes the radius of its range.
 *
 * You may choose to detonate a single bomb. When a bomb is detonated, it will detonate all bombs
 * that lie in its range. These bombs will further detonate the bombs that lie in their ranges.
 *
 * Given the list of bombs, return the maximum number of bombs that can be detonated if you are
 * allowed to detonate only one bomb.
 *
 * Example 1:
 *
 * Input: bombs = [[2,1,3],[6,1,4]]
 * Output: 2
 * Explanation:
 * The above figure shows the positions and ranges of the 2 bombs.
 * If we detonate the left bomb, the right bomb will not be affected.
 * But if we detonate the right bomb, both bombs will be detonated.
 * So the maximum bombs that can be detonated is max(1, 2) = 2.
 * Example 2:
 *
 * Input: bombs = [[1,1,5],[10,10,5]]
 * Output: 1
 * Explanation:
 * Detonating either bomb will not detonate the other bomb, so the maximum number of bombs that can be detonated is 1.
 * Example 3:
 *
 * Input: bombs = [[1,2,3],[2,3,1],[3,4,2],[4,5,3],[5,6,4]]
 * Output: 5
 * Explanation:
 * The best bomb to detonate is bomb 0 because:
 * - Bomb 0 detonates bombs 1 and 2. The red circle denotes the range of bomb 0.
 * - Bomb 2 detonates bomb 3. The blue circle denotes the range of bomb 2.
 * - Bomb 3 detonates bomb 4. The green circle denotes the range of bomb 3.
 * Thus all 5 bombs are detonated.
 *
 * Constraints:
 *
 * 1 <= bombs.length <= 100
 * bombs[i].length == 3
 * 1 <= xi, yi, ri <= 105
 */
/**
 * Original solution.
 */
constexpr int visited = 1;

class Solution {
public:
    int maximumDetonation(vector<vector<int>>& bombs) {
        auto G = build_bomb_graph(bombs);
        auto bsize = static_cast<int>(bombs.size());

        auto ans = int{0};
        for (auto i = int{0}; i < bsize; ++i) {
            auto visit = vector<int>(bsize, 0);
            ans = max(ans, dfs(G, i, visit));
        }
        return ans;
    }

private:
    using BombGraph = unordered_map<int, vector<int>>;

    BombGraph build_bomb_graph(vector<vector<int>> const& bombs) const
    {
        auto graph = BombGraph{};
        auto bsize = static_cast<int>(bombs.size());

        for (auto i = int{0}; i < bsize; ++i) {
            for (auto j = i + 1; j < bsize; ++j) {
                auto dx = static_cast<int64_t>(bombs[i][0] - bombs[j][0]);
                auto dy = static_cast<int64_t>(bombs[i][1] - bombs[j][1]);
                auto r1 = static_cast<int64_t>(bombs[i][2]);
                auto r2 = static_cast<int64_t>(bombs[j][2]);
                auto distance = dx * dx + dy * dy;

                if (distance <= (r1 * r1)) {
                    graph[i].emplace_back(j);
                }
                if (distance <= (r2 * r2)) {
                    graph[j].emplace_back(i);
                }
            }
        }

        return graph;
    }

    int dfs(BombGraph& G, int i, vector<int>& visit) {
        if (visit[i] == visited) {
            return 0;
        }

        visit[i] = visited;
        auto ans = int{1};

        if (G.count(i)) {
            for (auto const& b : G[i]) {
                ans += dfs(G, b, visit);
            }
        }

        return ans;
    }
};
