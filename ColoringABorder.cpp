/**
 * Coloring A Border
 *
 * Given a 2-dimensional grid of integers, each value in the grid represents the color of the grid square at that
 * location.

 * Two squares belong to the same connected component if and only if they have the same color and are next to each other
 * in any of the 4 directions.
 *
 * The border of a connected component is all the squares in the connected component that are either 4-directionally
 * adjacent to a square not in the component, or on the boundary of the grid (the first or last row or column).
 *
 * Given a square at location (r0, c0) in the grid and a color, color the border of the connected component of that
 * square with the given color, and return the final grid.
 *
 * Example 1:
 *
 * Input: grid = [[1,1],[1,2]], r0 = 0, c0 = 0, color = 3
 * Output: [[3, 3], [3, 2]]
 * Example 2:
 *
 * Input: grid = [[1,2,2],[2,3,2]], r0 = 0, c0 = 1, color = 3
 * Output: [[1, 3, 3], [2, 3, 3]]
 * Example 3:
 *
 * Input: grid = [[1,1,1],[1,1,1],[1,1,1]], r0 = 1, c0 = 1, color = 2
 * Output: [[2, 2, 2], [2, 1, 2], [2, 2, 2]]
 *
 * Note:
 *
 * 1 <= grid.length <= 50
 * 1 <= grid[0].length <= 50
 * 1 <= grid[i][j] <= 1000
 * 0 <= r0 < grid.length
 * 0 <= c0 < grid[0].length
 * 1 <= color <= 1000
 */
/**
 * Original solution.
 * Also we can split into 3 steps to make it more clear.
 * 1. we flip the island (make the same cell negative, at the
 * same time, it indicates that we visited already)
 * 2. flip all the cells inside
 * 3. make the negative cell the border
 */
class Solution {
public:
    vector<vector<int>> colorBorder(vector<vector<int>>& grid, int r0, int c0, int color) {
        vector<vector<bool>> visited(grid.size(), vector<bool>(grid[0].size(), false));

        int island_color = grid[r0][c0];
        if (color != island_color) {
            dfs(grid, visited, r0, c0, island_color, color);

            for (auto& row : grid) {
                for (auto& cell : row) {
                    if (cell < 0) {
                        cell = -cell;
                    }
                }
            }
        }

        return grid;
    }

private:
    void dfs(vector<vector<int>>& grid, vector<vector<bool>>& visited,
            int r, int c, int island_color, int border_color)
    {
        if (r < 0 || r >= grid.size() || c < 0 || c >= grid[0].size()) {
            return;
        }

        if (!visited[r][c] && grid[r][c] == island_color) {
            visited[r][c] = true;
            if (is_border(grid, r, c)) {
                grid[r][c] = -border_color;
            }
            dfs(grid, visited, r, c - 1, island_color, border_color);
            dfs(grid, visited, r, c + 1, island_color, border_color);
            dfs(grid, visited, r - 1, c, island_color, border_color);
            dfs(grid, visited, r + 1, c, island_color, border_color);
        }
    }

    bool is_border(vector<vector<int>>& grid, int r, int c)
    {
        if (0 == c || grid[0].size() == (c + 1)) {
            return true;
        }

        if (r == 0 || grid.size() == (r + 1)) {
            return true;
        }

        int island_color = grid[r][c];
        if ((grid[r][c - 1] > 0 && island_color != grid[r][c - 1])
            || (grid[r][c + 1] > 0 && island_color != grid[r][c + 1])
            || (grid[r - 1][c] > 0 && island_color != grid[r - 1][c])
            || (grid[r + 1][c] > 0 && island_color != grid[r + 1][c])) {
            return true;
        }

        return false;
    }
};
