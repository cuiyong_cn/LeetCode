/**
 * Check If All 1's Are at Least Length K Places Away
 *
 * Given an binary array nums and an integer k, return true if all 1's are at least k places away
 * from each other, otherwise return false.
 *
 * Example 1:
 *
 * Input: nums = [1,0,0,0,1,0,0,1], k = 2
 * Output: true
 * Explanation: Each of the 1s are at least 2 places away from each other.
 * Example 2:
 *
 * Input: nums = [1,0,0,1,0,1], k = 2
 * Output: false
 * Explanation: The second 1 and third 1 are only one apart from each other.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 105
 * 0 <= k <= nums.length
 * nums[i] is 0 or 1
 */
/**
 * More clear one.
 */
class Solution {
public:
    bool kLengthApart(vector<int>& nums, int k) {
        auto len = k;
        for (auto n : nums) {
            if (1 == n) {
                if (len < k) {
                    return false;
                }
                len = 0;
            }
            else {
                ++len;
            }
        }

        return true;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool kLengthApart(vector<int>& nums, int k) {
        for (auto prev = find(nums.begin(), nums.end(), 1); prev != nums.end();) {
            auto next = find(prev + 1, nums.end(), 1);
            if (next == nums.end()) {
                break;
            }
            if ((distance(prev, next) - 1) < k) {
                return false;
            }
            prev = next;
        }

        return true;
    }
};
