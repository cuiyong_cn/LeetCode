/**
 * Remove All Adjacent Duplicates In String
 *
 * Given a string S of lowercase letters, a duplicate removal consists of choosing two
 * adjacent and equal letters, and removing them.
 *
 * We repeatedly make duplicate removals on S until we no longer can.
 *
 * Return the final string after all such duplicate removals have been made.
 * It is guaranteed the answer is unique.
 *
 * Example 1:
 *
 * Input: "abbaca"
 * Output: "ca"
 * Explanation:
 * For example, in "abbaca" we could remove "bb" since the letters are adjacent
 * and equal, and this is the only possible move.  The result of this move is that the
 * string is "aaca", of which only "aa" is possible, so the final string is "ca".
 *
 * Note:
 *
 *     1 <= S.length <= 20000
 *     S consists only of English lowercase letters.
 */
class Solution
{
public:
    string removeDuplicates(string S)
    {
        string res = "";
        for (char& c : S)
            if (res.size() && c == res.back())
                res.pop_back();
            else
                res.push_back(c);
        return res;
    }
};
/**
 * Two index method
 */
class Solution
{
public:
    string removeDuplicates(string s)
    {
        int i = 0, n = s.length();
        for (int j = 0; j < n; ++j, ++i) {
            s[i] = s[j];
            if (i > 0 && s[i - 1] == s[i]) // count = 2
                i -= 2;
        }
        return s.substr(0, i);
    }
};
/**
 * Recursive one
 */
class Solution
{
public:
    string removeDuplicates(string S)
    {
        for (auto i = 1; i < S.size(); ++i)
            if (S[i - 1] == S[i]) return removeDuplicates(S.substr(0, i - 1) + S.substr(i + 1));
        return S;
    }
};

