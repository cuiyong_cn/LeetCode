/**
 * Kth Largest Element in a Stream
 *
 * Design a class to find the kth largest element in a stream. Note that it is the kth largest element in the sorted
 * order, not the kth distinct element.
 *
 * Your KthLargest class will have a constructor which accepts an integer k and an integer array nums, which contains
 * initial elements from the stream. For each call to the method KthLargest.add, return the element representing the kth
 * largest element in the stream.
 *
 * Example:
 *
 * int k = 3;
 * int[] arr = [4,5,8,2];
 * KthLargest kthLargest = new KthLargest(3, arr);
 * kthLargest.add(3);   // returns 4
 * kthLargest.add(5);   // returns 5
 * kthLargest.add(10);  // returns 5
 * kthLargest.add(9);   // returns 8
 * kthLargest.add(4);   // returns 8
 *
 * Note:
 * You may assume that nums' length ≥ k-1 and k ≥ 1.
 */
/**
 * Finally, passed one. We only have to maintain a K element priority queue to solve this
 */
class KthLargest {
public:
    KthLargest(int k, vector<int>& nums) {
        kth = k > 0 ? k : 1;
        for (int n : nums) {
            insert(n);
        }
    }

    int add(int val) {
        insert(val);
        return pq.top();
    }

void insert(int val)
{
    if (pq.size() >= kth) {
        if (pq.top() < val) {
            pq.pop();
            pq.push(val);
        }
    }
    else {
        pq.push(val);
    }
}

private:
    size_t kth;
    priority_queue<int, vector<int>, std::greater<int>> pq;
};

/**
 * Little bit faster than below, but still TLE.
 */
class KthLargest {
public:
    KthLargest(int k, vector<int>& nums) {
        kth = k;
        vec = nums;
    }

    int add(int val) {
        vec.size() > k)
        vec.push_back(val);
        nth_element(vec.begin(), vec.begin() + kth - 1, vec.end(), std::greater<int>());
        return vec[kth - 1];
    }

private:
    int kth;
    vector<int> vec;
};

/**
 * Your KthLargest object will be instantiated and called as such:
 * KthLargest* obj = new KthLargest(k, nums);
 * int param_1 = obj->add(val);
 */
/**
 * Original TLE solution. Simple but you know.
 */
class KthLargest {
public:
    KthLargest(int k, vector<int>& nums) {
        kth = k;
        vec = nums;
    }

    int add(int val) {
        vec.push_back(val);
        sort(vec.begin(), vec.end(), std::greater<int>());
        return vec[kth - 1];
    }

private:
    int kth;
    vector<int> vec;
};

/**
 * Your KthLargest object will be instantiated and called as such:
 * KthLargest* obj = new KthLargest(k, nums);
 * int param_1 = obj->add(val);
 */
