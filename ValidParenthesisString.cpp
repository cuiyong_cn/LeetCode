/**
 * Valid Parenthesis String
 *
 * Given a string s containing only three types of characters: '(', ')' and '*', return true if s is
 * valid.
 *
 * The following rules define a valid string:
 *
 * Any left parenthesis '(' must have a corresponding right parenthesis ')'.  Any right parenthesis
 * ')' must have a corresponding left parenthesis '('.  Left parenthesis '(' must go before the
 * corresponding right parenthesis ')'.  '*' could be treated as a single right parenthesis ')' or a
 * single left parenthesis '(' or an empty string "".
 *
 * Example 1:
 *
 * Input: s = "()"
 * Output: true
 * Example 2:
 *
 * Input: s = "(*)"
 * Output: true
 * Example 3:
 *
 * Input: s = "(*))"
 * Output: true
 *
 * Constraints:
 *
 * 1 <= s.length <= 100
 * s[i] is '(', ')' or '*'.
 */
/**
 * Optimize further
 */
class Solution {
public:
    bool checkValidString(string s) {
        int low = 0;
        int high = 0;

        for (auto c : s) {
            low += '(' == c ? 1 : -1;
            high += ')' != c ? 1 : -1;
            if (high < 0) {
                break;
            }
            low = max(0, low);
        }

        return 0 == low;
    }
};

/**
 * Optimize below.
 */
class Solution {
public:
    bool checkValidString(string s) {
        return dfs(s, 0, 0);
    }

private:
    bool dfs(string const& s, int left_cnt, int i)
    {
        if (i >= s.length()) {
            return 0 == left_cnt;
        }

        if ('(' == s[i]) {
            ++left_cnt;
        }
        else if (')' == s[i]) {
            if (0 == left_cnt) {
                return false;
            }
            --left_cnt;
        }
        else {
            if (dfs(s, left_cnt + 1, i + 1)) {
                return true;
            }

            if (left_cnt > 0) {
                if (dfs(s, left_cnt - 1, i + 1)) {
                    return true;
                }
            }
        }

        return dfs(s, left_cnt, i + 1);
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool checkValidString(string s) {
        vector<char> stk;
        return dfs(s, stk, 0);
    }

private:
    bool dfs(string const& s, vector<char>& stk, int i)
    {
        if (i >= s.length()) {
            return stk.empty();
        }

        if ('(' == s[i]) {
            stk.push_back(s[i]);
        }
        else if (')' == s[i]) {
            if (stk.empty()) {
                return false;
            }
            stk.pop_back();
        }
        else {
            auto left = stk;
            left.push_back('(');
            if (dfs(s, left, i + 1)) {
                return true;
            }

            if (!stk.empty()) {
                auto right = stk;
                right.pop_back();
                if (dfs(s, right, i + 1)) {
                    return true;
                }
            }
        }

        return dfs(s, stk, i + 1);;
    }
};
