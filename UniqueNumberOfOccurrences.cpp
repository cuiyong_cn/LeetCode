/**
 * Unique Number of Occurrences
 *
 * Given an array of integers arr, return true if the number of occurrences of each value in the
 * array is unique, or false otherwise.
 *
 * Example 1:
 *
 * Input: arr = [1,2,2,1,1,3]
 * Output: true
 * Explanation: The value 1 has 3 occurrences, 2 has 2 and 3 has 1. No two values have the same
 * number of occurrences.
 * Example 2:
 *
 * Input: arr = [1,2]
 * Output: false
 * Example 3:
 *
 * Input: arr = [-3,0,1,-3,1,1,1,-3,10,0]
 * Output: true
 *
 * Constraints:
 *
 * 1 <= arr.length <= 1000
 * -1000 <= arr[i] <= 1000
 */
/**
 * A little optimization.
 */
class Solution {
public:
    bool uniqueOccurrences(vector<int>& arr) {
        unordered_map<int, int> num_cnt;
        for (auto&& n : arr) {
            ++num_cnt[n];
        }

        unordered_set<int> cnt;
        for (auto&& nc : num_cnt) {
            if (!cnt.insert(nc.second).second) {
                return false;
            }
        }

        return true;
    }
};

/**
 * Without the sort.
 */
class Solution {
public:
    bool uniqueOccurrences(vector<int>& arr) {
        unordered_map<int, int> num_cnt;
        for (auto&& n : arr) {
            ++num_cnt[n];
        }

        unordered_set<int> cnt;
        for (auto&& nc : num_cnt) {
            cnt.insert(nc.second);
        }

        return cnt.size() == num_cnt.size();
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool uniqueOccurrences(vector<int>& arr) {
        unordered_map<int, int> num_cnt;
        for (auto&& n : arr) {
            ++num_cnt[n];
        }

        vector<int> cnt;
        for (auto&& nc : num_cnt) {
            cnt.emplace_back(nc.second);
        }

        sort(cnt.begin(), cnt.end());

        for (int i = 1; i < cnt.size(); ++i) {
            if (cnt[i] == cnt[i - 1]) {
                return false;
            }
        }

        return true;
    }
};
