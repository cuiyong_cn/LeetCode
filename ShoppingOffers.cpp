/**
 * Shopping Offers
 *
 * In LeetCode Store, there are some kinds of items to sell. Each item has a price.
 *
 * However, there are some special offers, and a special offer consists of one or more different kinds of items with a
 * sale price.
 *
 * You are given the each item's price, a set of special offers, and the number we need to buy for each item. The job is
 * to output the lowest price you have to pay for exactly certain items as given, where you could make optimal use of
 * the special offers.
 *
 * Each special offer is represented in the form of an array, the last number represents the price you need to pay for
 * this special offer, other numbers represents how many specific items you could get if you buy this offer.
 *
 * You could use any of special offers as many times as you want.
 *
 * Example 1:
 *
 * Input: [2,5], [[3,0,5],[1,2,10]], [3,2]
 * Output: 14
 * Explanation:
 * There are two kinds of items, A and B. Their prices are $2 and $5 respectively.
 * In special offer 1, you can pay $5 for 3A and 0B
 * In special offer 2, you can pay $10 for 1A and 2B.
 * You need to buy 3A and 2B, so you may pay $10 for 1A and 2B (special offer #2), and $4 for 2A.
 *
 * Example 2:
 *
 * Input: [2,3,4], [[1,1,0,4],[2,2,1,9]], [1,2,1]
 * Output: 11
 * Explanation:
 * The price of A is $2, and $3 for B, $4 for C.
 * You may pay $4 for 1A and 1B, and $9 for 2A ,2B and 1C.
 * You need to buy 1A ,2B and 1C, so you may pay $4 for 1A and 1B (special offer #1), and $3 for 1B, $4 for 1C.
 * You cannot add more items, though only $9 for 2A ,2B and 1C.
 *
 * Note:
 *     There are at most 6 kinds of items, 100 special offers.
 *     For each item, you need to buy at most 6 of them.
 *     You are not allowed to buy more items than you want, even if that would lower the overall price.
 */
/**
 * Improve the DFS solution with memorization.
 */
void operator+=(vector<int> &a, const vector<int> &b)
{
    for (int i = 0; i < a.size(); i++)
        a[i] += b[i];
}

void operator-=(vector<int> &a, const vector<int> &b)
{
    for (int i = 0; i < a.size(); i++)
        a[i] -= b[i];
}

bool operator<(const vector<int>& a, const int b)
{
    for (auto n : a) {
        if (n < b) return true;
    }
    
    return false;
}

int toIntKey(const vector<int>& a)
{
    int key = 0;
    for (auto n : a) {
        key = key * 10 + n;
    }
    
    return key;
}

class Solution {
public:
    int shoppingOffers(vector<int>& price, vector<vector<int>>& special, vector<int>& needs, int cost = 0) {
        if (needs < 0) return std::numeric_limits<int>::max();
        
        auto key = toIntKey(needs);
        auto iter = memo.find(key);
        if (iter != memo.end()) return iter->second;

        int m = std::inner_product(needs.begin(), needs.end(), price.begin(), cost);

        for (auto &offer : special) {
            if (cost + offer.back() >= m) continue;

            needs -= offer;
            m = min(m, shoppingOffers(price, special, needs, cost + offer.back()));
            needs += offer;
        }

        memo[key] = m;

        return m;
    }

private:
    unordered_map<int, int> memo;
};


/**
 * DFS and correction solution.
 */
void operator+=(vector<int> &a, const vector<int> &b)
{
    for (int i = 0; i < a.size(); i++)
        a[i] += b[i];
}

void operator-=(vector<int> &a, const vector<int> &b)
{
    for (int i = 0; i < a.size(); i++)
        a[i] -= b[i];
}

bool operator<(const vector<int>& a, const int b)
{
    for (auto n : a) {
        if (n < b) return true;
    }
    
    return false;
}

class Solution {
public:
    int shoppingOffers(vector<int>& price, vector<vector<int>>& special, vector<int>& needs, int cost = 0) {
        if (needs < 0) return std::numeric_limits<int>::max();

        int m = std::inner_product(needs.begin(), needs.end(), price.begin(), cost);

        for (auto &offer : special) {
            if (cost + offer.back() >= m) continue;

            needs -= offer;
            m = min(m, shoppingOffers(price, special, needs, cost + offer.back()));
            needs += offer;
        }

        return m;
    }
};


/**
 * origianal solution. I think this solution has flaw: not 100% correct. But somehow it passed the test.
 * I think leetcode lack some of the test cases. I already think of a test case that may fail this one (though
 * I don't actually test it)
 */
class Solution {
public:
    struct SpSave
    {
        int id;
        int org;
        int save;
    };

    int shoppingOffers(vector<int>& price, vector<vector<int>>& special, vector<int>& needs) {
        vector<SpSave> saves;

        for (int i = 0; i < special.size(); ++i) {
            int org = 0;
            for (int j = 0; j < price.size(); ++j) {
                org += special[i][j] * price[j];
            }
            saves.push_back({i, org, max(org - special[i][price.size()], 0)});
        }

        sort(saves.begin(), saves.end(), [](auto& s1, auto& s2) { return s1.save > s2.save; });

        int lowest = 0;
        for (int i = 0; i < needs.size(); ++i) lowest += price[i] * needs[i];

        for (int i = 0; i < saves.size(); ++i) {
            int id = saves[i].id;
            int count = buySpecialCount(id, special, needs);
            if (count > 0) {
                lowest = min(lowest, lowestPrice(i, saves, price, special, needs));
            }
        }

        return lowest;
    }

private:
    int lowestPrice(int start, const vector<SpSave>& saves,
           const vector<int>& price, const vector< vector<int> >& special, vector<int> needs)
    {
        int lowest = 0;
        for (int i = start; i < saves.size(); ++i) {
            int id = saves[i].id;
            int count = buySpecialCount(id, special, needs);
            if (count > 0) {
                lowest += (saves[i].org - saves[i].save) * count;
                for (int j = 0; j < needs.size(); ++j) {
                    needs[j] -= special[id][j] * count;
                }
            }
        }

        for (int i = 0; i < needs.size(); ++i) {
            if (needs[i] > 0) {
                lowest += needs[i] * price[i];
            }
        }

        return lowest;
    }

    int buySpecialCount(int id, const vector< vector<int> >& special, const vector<int>& needs) {
        int count = numeric_limits<int>::max();

        for (int j = 0; j < needs.size(); ++j) {
            int ic = special[id][j];
            if (ic > 0) {
                count = min(count, needs[j] / ic);
            }
        }

        return count;
    }
};

