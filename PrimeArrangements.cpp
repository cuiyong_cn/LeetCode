/**
 * Prime Arrangements
 *
 * Return the number of permutations of 1 to n so that prime numbers are at prime indices (1-indexed.)
 *
 * (Recall that an integer is prime if and only if it is greater than 1, and cannot be written as a product of two
 * positive integers both smaller than it.)
 *
 * Since the answer may be large, return the answer modulo 10^9 + 7.
 *
 * Example 1:
 *
 * Input: n = 5
 * Output: 12
 * Explanation: For example [1,2,5,4,3] is a valid permutation, but [5,2,3,4,1] is not because the prime number 5 is at index 1.
 *
 * Example 2:
 *
 * Input: n = 100
 * Output: 682289015
 *
 * Constraints:
 *     1 <= n <= 100
 */
/**
 * More interesting solution.
 */
class Solution {
public:
    int numPrimeArrangements(int n) {
        std::vector<int> non(n + 1, 0);
        std::vector<int> cnt = {0, 1};
        long res = 1;
        for (int i = 2; i<=n; i++) {
            if (non[i] == 0) {
                for (int m = i * i; m <= n; m+=i) {
                    non[m] = 1;
                }
            }
            res = res * ++cnt[non[i]] % 1_000_000_007;
        }
        return res;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int numPrimeArrangements(int n) {
        std::vector<int> primeCount = {0, 0, 1, 2, 2, 3, 3, 4, 4, 4, 4,
                                      5, 5, 6, 6, 6, 6, 7, 7, 8, 8,
                                      8, 8, 9, 9, 9, 9, 9, 9, 10, 10,
                                      11, 11, 11, 11, 11, 11, 12, 12, 12, 12,
                                      13, 13, 14, 14, 14, 14, 15, 15, 15, 15,
                                      15, 15, 16, 16, 16, 16, 16, 16, 17, 17,
                                      18, 18, 18, 18, 18, 18, 19, 19, 19, 19,
                                      20, 20, 21, 21, 21, 21, 21, 21, 22, 22,
                                      22, 22, 23, 23, 23, 23, 23, 23, 24, 24,
                                      24, 24, 24, 24, 24, 24, 25, 25, 25, 25};
        int primeNums = primeCount[n];
        int nonPrimesNums = n - primeNums;
        int mod = 1000000007;
        long long ans = 1;
        for (int i = 1; i <= primeNums; ++i) {
            ans *= i;
            if (ans >= mod) ans %= mod;
        }

        for (int i = 1; i <= nonPrimesNums; ++i) {
            ans *= i;
            if (ans >= mod) ans %= mod;
        }

        return ans;
    }

private:
    long factorial(long n) {
        if (n <= 1) return 1;
        return n * factorial(n - 1);
    }

    bool is_prime(long N)
    {
        if (N < 2) return false;
        if (N < 4) return true;
        if ((N & 1) == 0) return false;
        if (N % 3 == 0) return false;

        int curr = 5, s = sqrt(N);

        // Loop to check if any number number is divisible by any other number or not
        while (curr <= s) {
            if (N % curr == 0) return false;
            curr += 2;

            if (N % curr == 0) return false;
            curr += 4;
        }

        return true;
    }
};
