/**
 * Minimum Swaps To Make Sequences Increasing
 *
 * We have two integer sequences nums1 and nums2 of the same non-zero length.
 *
 * We are allowed to swap elements nums1[i] and nums2[i]. Note that both elements are in the same
 * index position in their respective sequences.
 *
 * At the end of some number of swaps, nums1 and nums2 are both strictly increasing. (An array A is
 * strictly increasing if and only if A[0] < A[1] < A[2] < ... < A[A.length - 1].)
 *
 * Given nums1 and nums2, return the minimum number of swaps to make both sequences strictly
 * increasing. It is guaranteed that the given input always makes it possible.
 *
 * Example:
 * Input: nums1 = [1,3,5,4], nums2 = [1,2,3,7]
 * Output: 1
 * Explanation:
 * Swap nums1[3] and nums2[3].  Then the sequences are:
 * nums1 = [1, 3, 5, 7] and nums2 = [1, 2, 3, 4]
 * which are both strictly increasing.
 * Note:
 *
 * nums1, nums2 are arrays with the same length, and that length will be in the range [1, 1000].
 * nums1[i], nums2[i] are integer values in the range [0, 2000].
 */
/**
 * Original solution.
 * swap[n] means the minimum swaps to make the A[i] and B[i] sequences increasing for 0 <= i <= n,
 * in condition that we swap A[n] and B[n]
 * not_swap[n] is the same with A[n] and B[n] not swapped.
 *
 * A[i - 1] < A[i] && B[i - 1] < B[i].
 * In this case, if we want to keep A and B increasing before the index i, can only have two choices.
 * -> 1.1 don't swap at (i-1) and don't swap at i, we can get not_swap[i] = not_swap[i-1]
 * -> 1.2 swap at (i-1) and swap at i, we can get swap[i] = swap[i-1]+1
 * if swap at (i-1) and do not swap at i, we can not guarantee A and B increasing.
 *
 * A[i-1] < B[i] && B[i-1] < A[i]
 * In this case, if we want to keep A and B increasing before the index i, can only have two choices.
 * -> 2.1 swap at (i-1) and do not swap at i, we can get notswap[i] = Math.min(swap[i-1], notswap[i] )
 * -> 2.2 do not swap at (i-1) and swap at i, we can get swap[i]=Math.min(notswap[i-1]+1, swap[i])
 */
class Solution {
public:
    int minSwap(vector<int>& A, vector<int>& B) {
        int N = A.size();
        int not_swap[1000] = {0};
        int swap[1000] = {1};
        for (int i = 1; i < N; ++i) {
            not_swap[i] = swap[i] = N;
            if (A[i - 1] < A[i] && B[i - 1] < B[i]) {
                swap[i] = swap[i - 1] + 1;
                not_swap[i] = not_swap[i - 1];
            }
            if (A[i - 1] < B[i] && B[i - 1] < A[i]) {
                swap[i] = min(swap[i], not_swap[i - 1] + 1);
                not_swap[i] = min(not_swap[i], swap[i - 1]);
            }
        }
        return min(swap[N - 1], not_swap[N - 1]);
    }
};
