/**
 * Relative Ranks
 *
 * Given scores of N athletes, find their relative ranks and the people with the top three highest scores, who will be
 * awarded medals: "Gold Medal", "Silver Medal" and "Bronze Medal".
 *
 * Example 1:
 *
 * Input: [5, 4, 3, 2, 1]
 * Output: ["Gold Medal", "Silver Medal", "Bronze Medal", "4", "5"]
 * Explanation: The first three athletes got the top three highest scores, so they got "Gold Medal", "Silver Medal" and
 * "Bronze Medal".  For the left two athletes, you just need to output their relative ranks according to their scores.
 *
 * Note:
 *     N is a positive integer and won't exceed 10,000.
 *     All the scores of athletes are guaranteed to be unique.
 */
/**
 * Original solution.
 */
class Solution {
public:
    class SortProxy
    {
    public:
        SortProxy(const vector<int>& seq) : s{seq} {}
        bool operator()(int a, int b) const {
            return s[a] > s[b];
        }
    private:
        const vector<int>& s;
    };

    vector<string> findRelativeRanks(vector<int>& nums) {
        if (nums.empty()) return {};

        vector<int> seq(nums.size() + 1, 0);
        for (size_t i = 0; i < nums.size(); ++i) {
            seq[i] = i;
        }

        sort(seq.begin() + 1, seq.end(), SortProxy(nums));

        vector<string> ans(nums.size());
        for (size_t i = 1; i < seq.size(); ++i) {
            if (i > 3) {
                ans[seq[i]] = to_string(i);
            }
            else if (1 == i) {
                ans[seq[i]] = "Gold Medal";
            }
            else if (2 == i) {
                ans[seq[i]] = "Silver Medal";
            }
            else if (3 == i) {
                ans[seq[i]] = "Bronze Medal";
            }
        }

        return ans;
    }
};

/**
 * Solution from discussion
 */
class Solution {
public:
    vector<string> findRelativeRanks(vector<int>& nums) {
        const int n = nums.size();
        vector<string> ans(n);
        map<int, int> dict;
        for(int i = 0; i < n; i++) dict[-nums[i]] = i;
        int cnt = 0;
        vector<string> top3{"Gold Medal", "Silver Medal", "Bronze Medal"};
        for(auto& [k, i]: dict){
            cnt++;
            if(cnt <=3) ans[i] = top3[cnt-1];
            else ans[i] = to_string(cnt);
        }
        return ans;
    }
};
