/**
 * Maximum Trailing Zeros in a Cornered Path
 *
 * You are given a 2D integer array grid of size m x n, where each cell contains a positive integer.
 *
 * A cornered path is defined as a set of adjacent cells with at most one turn. More specifically,
 * the path should exclusively move either horizontally or vertically up to the turn (if there is
 * one), without returning to a previously visited cell. After the turn, the path will then move
 * exclusively in the alternate direction: move vertically if it moved horizontally, and vice versa,
 * also without returning to a previously visited cell.
 *
 * The product of a path is defined as the product of all the values in the path.
 *
 * Return the maximum number of trailing zeros in the product of a cornered path found in grid.
 *
 * Note:
 *
 * Horizontal movement means moving in either the left or right direction.
 * Vertical movement means moving in either the up or down direction.
 *
 * Example 1:
 *
 * Input: grid = [[23,17,15,3,20],[8,1,20,27,11],[9,4,6,2,21],[40,9,1,10,6],[22,7,4,5,3]]
 * Output: 3
 * Explanation: The grid on the left shows a valid cornered path.
 * It has a product of 15 * 20 * 6 * 1 * 10 = 18000 which has 3 trailing zeros.
 * It can be shown that this is the maximum trailing zeros in the product of a cornered path.
 *
 * The grid in the middle is not a cornered path as it has more than one turn.
 * The grid on the right is not a cornered path as it requires a return to a previously visited cell.
 * Example 2:
 *
 * Input: grid = [[4,3,2],[7,6,1],[8,8,8]]
 * Output: 0
 * Explanation: The grid is shown in the figure above.
 * There are no cornered paths in the grid that result in a product with a trailing zero.
 *
 * Constraints:
 *
 * m == grid.length
 * n == grid[i].length
 * 1 <= m, n <= 105
 * 1 <= m * n <= 105
 * 1 <= grid[i][j] <= 1000
 */
/**
 * Original solution.
 * Optimization:
 *      We can eliminate factor2 and factor5 vector
 */
auto trailing0_of_cornered_path(
    vector<vector<int>> const& f2_hor,
    vector<vector<int>> const& f2_ver,
    vector<vector<int>> const& f5_hor,
    vector<vector<int>> const& f5_ver,
    int i,
    int j)
{
    auto ans = 0;
    // top left
    {
        auto f2 = f2_ver[i][j] + (j > 0 ? f2_hor[i][j - 1] : 0);
        auto f5 = f5_ver[i][j] + (j > 0 ? f5_hor[i][j - 1] : 0);
        ans = max(ans, min(f2, f5));
    }
    // top right
    {
        auto f2 = f2_ver[i][j] + (f2_hor[i].back() - f2_hor[i][j]);
        auto f5 = f5_ver[i][j] + (f5_hor[i].back() - f5_hor[i][j]);
        ans = max(ans, min(f2, f5));
    }
    // left bottom
    {
        auto f2 = f2_hor[i][j] + (f2_ver.back()[j] - f2_ver[i][j]);
        auto f5 = f5_hor[i][j] + (f5_ver.back()[j] - f5_ver[i][j]);
        ans = max(ans, min(f2, f5));
    }
    // right bottom
    {
        auto f2 = f2_hor[i].back() - f2_hor[i][j] + (f2_ver.back()[j] - (i > 0 ? f2_ver[i - 1][j] : 0));
        auto f5 = f5_hor[i].back() - f5_hor[i][j] + (f5_ver.back()[j] - (i > 0 ? f5_ver[i - 1][j] : 0));
        ans = max(ans, min(f2, f5));
    }

    return ans;
}

class Solution {
public:
    int maxTrailingZeros(vector<vector<int>>& grid) {
        auto const m = static_cast<int>(grid.size());
        auto const n = static_cast<int>(grid[0].size());

        auto factor2 = vector<vector<int>>(m, vector<int>(n, 0));
        auto factor5 = factor2;

        for (auto i = 0; i < m; ++i) {
            for (auto j = 0; j < n; ++j) {
                auto e = grid[i][j];
                while (e >= 2) {
                    if (0 == (e % 2)) {
                        ++factor2[i][j];
                        e = e / 2;
                    }
                    else if (0 == (e % 5)) {
                        ++factor5[i][j];
                        e = e / 5;
                    }
                    else {
                        break;
                    }
                }
            }
        }

        auto prefix2_hor = factor2;
        auto prefix2_ver = factor2;
        auto prefix5_hor = factor5;
        auto prefix5_ver = factor5;
        for (auto& row : prefix2_hor) {
            partial_sum(begin(row), end(row), begin(row));
        }
        for (auto& row : prefix5_hor) {
            partial_sum(begin(row), end(row), begin(row));
        }
        for (auto i = 0; i < n; ++i) {
            for (auto j = 1; j < m; ++j) {
                prefix2_ver[j][i] += prefix2_ver[j - 1][i];
            }
        }
        for (auto i = 0; i < n; ++i) {
            for (auto j = 1; j < m; ++j) {
                prefix5_ver[j][i] += prefix5_ver[j - 1][i];
            }
        }

        auto ans = 0;
        for (auto i = 0; i < m; ++i) {
            for (auto j = 0; j < n; ++j) {
                auto trailing0s = trailing0_of_cornered_path(prefix2_hor, prefix2_ver, prefix5_hor, prefix5_ver, i, j);
                ans = max(ans, trailing0s);
            }
        }

        return ans;
    }
};
