/**
 * Replace Words
 *
 * In English, we have a concept called root, which can be followed by some other words to form another longer word
 * - let's call this word successor. For example, the root an, followed by other, which can form another word another.
 *
 * Now, given a dictionary consisting of many roots and a sentence. You need to replace all the successor in the
 * sentence with the root forming it. If a successor has many roots can form it, replace it with the root with the
 * shortest length.
 *
 * You need to output the sentence after the replacement.
 *
 * Example 1:
 *
 * Input: dict = ["cat", "bat", "rat"]
 * sentence = "the cattle was rattled by the battery"
 * Output: "the cat was rat by the bat"
 *
 * Note:
 *
 *     The input will only have lower-case letters.
 *     1 <= dict words number <= 1000
 *     1 <= sentence words number <= 1000
 *     1 <= root length <= 100
 *     1 <= sentence words length <= 1000
 */
/**
 * Original solution. Trie method.
 */
class Solution {
public:
    struct Trie {
        Trie() : sub(26), isRoot{false} {}
        vector<shared_ptr<Trie>> sub;
        bool isRoot;
    };
    string ans;
    shared_ptr<Trie> root;

    int getNextPos(string& sentence, int s) {
        auto t = root;

        size_t stop = sentence.find(' ', s);
        if (string::npos == stop) {
            stop = sentence.size();
        }

        int len = 0;
        if (nullptr == t->sub[sentence[s] - 'a']) {
            len = stop - s;
        }
        else {
            int j = s;
            for (; j < stop; ++j) {
                if (t->isRoot) break;
                if (nullptr != t->sub[sentence[j] - 'a']) {
                    t = t->sub[sentence[j] - 'a'];
                }
                else {
                    j = stop - 1;
                }
            }

            len = j - s;
        }

        ans += sentence.substr(s, len);

        return stop;
    }

    void constructTrie(vector<string>& dict) {
        root = shared_ptr<Trie>(new Trie);

        for (auto& s : dict) {
            auto t = root;
            for (auto& c : s) {
                if (nullptr == t->sub[c - 'a']) {
                    t->sub[c - 'a'] = shared_ptr<Trie>(new Trie());
                }
                t = t->sub[c - 'a'];
            }
            t->isRoot = true;
        }
    }

    string replaceWords(vector<string>& dict, string sentence) {
        constructTrie(dict);

        int i = 0;
        while (i < sentence.length()) {
            if (isalpha(sentence[i])) {
                i = getNextPos(sentence, i);
            }
            else {
                ans += sentence[i];
                ++i;
            }
        }

        return ans;
    }
};
