/**
 * Single Element in a Sorted Array
 *
 * You are given a sorted array consisting of only integers where every element appears
 * exactly twice, except for one element which appears exactly once. Find this single
 * element that appears only once.
 *
 * Example 1:
 *
 * Input: [1,1,2,3,3,4,4,8,8]
 * Output: 2
 *
 * Example 2:
 *
 * Input: [3,3,7,7,10,11,11]
 * Output: 10
 *
 * Note: Your solution should run in O(log n) time and O(1) space.
 */
/**
 * Since the solution wants us to solve this in O(log n). we can use binary search.
 * Note: (this magic copied from leetcode user, really brilliant idea)
 *      mid => odd     mid ^ 1 => even => mid - 1
 *      mid => even    mid ^ 1 => odd  => mid + 1
 *
 * every 2 numbers are partner. (even,odd), (even,odd).
 * If mid is even, it's partner is next odd, if mid is odd, it's partner is previous even.
 */
class Solution {
public:
    int singleNonDuplicate(vector<int>& nums) {
        int left = 0;
        int right = nums.size() - 1;
        while (left < right) {
            int mid = (left + right) >> 1;
            if (nums[mid] == nums[mid ^ 1]) {
                left = mid + 1;
            }
            else {
                right = mid;
            }
        }
        return nums[left];
    }
};

/**
 * Also Binary seach, too chatty compared with the above solution.
 */
class Solution {
public:
    int singleNonDuplicate(vector<int>& nums) {
        int n = nums.size(), left = 0, right = n - 1;
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (mid % 2 == 0) {
                if (nums[mid] == nums[mid-1]) right = mid - 2;
                else if (nums[mid] == nums[mid+1]) left = mid + 2;
                else return nums[mid];
            }
            else {
                if (nums[mid] == nums[mid-1]) left = mid + 1;
                else if (nums[mid] == nums[mid+1]) right = mid - 1;
            }
        }
        return nums[left];
    }
};

/**
 * Since the array is sorted, we can use this.
 */
class Solution {
public:
    int singleNonDuplicate(vector<int>& nums) {
        int ans = 0;
        for (int i = 0; i < nums.size(); i += 2) {
            if ((i + 1) >= nums.size() || nums[i] != nums[i + 1]) {
                ans = nums[i];
                break;
            }
        }
        return ans;
    }
};

/**
 * Trivial solution no matter the nums is sorted or not.
 */
class Solution {
public:
    int singleNonDuplicate(vector<int>& nums) {
        int ans = 0;
        for (auto& n : nums) {
            ans ^= n;
        }
        return ans;
    }
};
