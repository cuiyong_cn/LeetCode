/**
 * Apply Bitwise Operations to Make Strings Equal
 *
 * You are given two 0-indexed binary strings s and target of the same length
 * n. You can do the following operation on s any number of times:
 *
 * Choose two different indices i and j where 0 <= i, j < n.
 * Simultaneously, replace s[i] with (s[i] OR s[j]) and s[j] with (s[i] XOR s[j]).
 *
 * For example, if s = "0110", you can choose i = 0 and j = 2, then
 * simultaneously replace s[0] with (s[0] OR s[2] = 0 OR 1 = 1), and s[2] with
 * (s[0] XOR s[2] = 0 XOR 1 = 1), so we will have s = "1110".
 *
 * Return true if you can make the string s equal to target, or false otherwise.
 *
 * Example 1:
 *
 * Input: s = "1010", target = "0110"
 * Output: true
 * Explanation: We can do the following operations:
 * - Choose i = 2 and j = 0. We have now s = "0010".
 * - Choose i = 2 and j = 1. We have now s = "0110".
 * Since we can make s equal to target, we return true.
 * Example 2:
 *
 * Input: s = "11", target = "00"
 * Output: false
 * Explanation: It is not possible to make s equal to target with any number of operations.
 *
 * Constraints:
 *
 * n == s.length == target.length
 * 2 <= n <= 105
 * s and target consist of only the digits 0 and 1.
 */
/**
 * More efficient one.
 * Rationale:
 *    If there is a '1', we can make any '0' to '1' and '1' to '0'.
 */
auto contain(string const& s, char c)
{
    return string::npos != s.find(c);
}

class Solution {
public:
    bool makeStringsEqual(string s, string target) {
        return contain(s, '1') == contain(target, '1');
    }
};

/**
 * Original solution.
 */
auto all_zero(string const& s)
{
    return all_of(begin(s), end(s), [](auto c) { return '0' == c; });
}
class Solution {
public:
    bool makeStringsEqual(string s, string target) {
        auto s_all_zero = all_zero(s);
        auto t_all_zero = all_zero(target);
        return (!s_all_zero && !t_all_zero) || (s_all_zero && t_all_zero);
    }
};
