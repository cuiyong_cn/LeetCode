/**
 * Maximum Non Negative Product in a Matrix
 *
 * You are given a m x n matrix grid. Initially, you are located at the top-left corner (0, 0), and
 * in each step, you can only move right or down in the matrix.
 *
 * Among all possible paths starting from the top-left corner (0, 0) and ending in the bottom-right
 * corner (m - 1, n - 1), find the path with the maximum non-negative product. The product of a path
 * is the product of all integers in the grid cells visited along the path.
 *
 * Return the maximum non-negative product modulo 109 + 7. If the maximum product is negative,
 * return -1.
 *
 * Notice that the modulo is performed after getting the maximum product.
 *
 * Example 1:
 *
 * Input: grid = [[-1,-2,-3],[-2,-3,-3],[-3,-3,-2]]
 * Output: -1
 * Explanation: It is not possible to get non-negative product in the path from (0, 0) to (2, 2), so return -1.
 * Example 2:
 *
 * Input: grid = [[1,-2,1],[1,-2,1],[3,-4,1]]
 * Output: 8
 * Explanation: Maximum non-negative product is shown (1 * 1 * -2 * -4 * 1 = 8).
 * Example 3:
 *
 *
 * Input: grid = [[1,3],[0,-4]]
 * Output: 0
 * Explanation: Maximum non-negative product is shown (1 * 0 * -4 = 0).
 *
 * Constraints:
 *
 * m == grid.length
 * n == grid[i].length
 * 1 <= m, n <= 15
 * -4 <= grid[i][j] <= 4
 */
/**
 * Make below solution shorter.
 */
class Solution {
public:
    int maxProductPath(vector<vector<int>>& grid) {
        int m=grid.size(), n=grid[0].size(), MOD = 1e9+7;
        vector<vector<long long>>mx(m,vector<long long>(n)), mn(m,vector<long long>(n));
        mx[0][0]=mn[0][0]=grid[0][0];

        for(int i=1; i<m; i++){
            mn[i][0] = mx[i][0] = mx[i-1][0] * grid[i][0];
        }
        for(int j=1; j<n; j++){
            mn[0][j] = mx[0][j] = mx[0][j-1] * grid[0][j];
        }

        for(int i=1; i<m; i++){
            for(int j=1; j<n; j++){
                if(grid[i][j] < 0){ // minimum product * negative number = new maximum product
                    mx[i][j] = (min(mn[i-1][j], mn[i][j-1]) * grid[i][j]);
                    mn[i][j] = (max(mx[i-1][j], mx[i][j-1]) * grid[i][j]);
                }
                else{ // maximum product * positive number = new maximum product
                    mx[i][j] = (max(mx[i-1][j], mx[i][j-1]) * grid[i][j]);
                    mn[i][j] = (min(mn[i-1][j], mn[i][j-1]) * grid[i][j]);
                }
            }
        }

        int ans = mx[m-1][n-1] % MOD;
        return ans < 0 ? -1 : ans;
    }
};

/**
 * DP solution. Ugly code as hell.
 */
class Solution {
public:
    int maxProductPath(vector<vector<int>>& grid) {
        if (0 == grid.front().front() || 0 == grid.back().back()) {
            return 0;
        }

        int const M = grid.size();
        int const N = grid[0].size();
        int const mod = 1'000'000'007;
        int64_t const int64_max = numeric_limits<int64_t>::max();
        int64_t const int64_min = numeric_limits<int64_t>::min();

        vector<vector<pair<int64_t, int64_t>>> dp(M, vector<pair<int64_t, int64_t>>(N, {int64_min, int64_max}));

        dp[0][0] = {grid.front().front(), grid.front().front()};

        for (int i = 1; i < N; ++i) {
            for (int x = 0, y = i; x < M && y >= 0; ++x, --y) {
                if (grid[x][y] > 0) {
                    if (x > 0) {
                        dp[x][y].first = max(dp[x][y].first, dp[x - 1][y].first * grid[x][y]);
                        dp[x][y].second = min(dp[x][y].second, dp[x - 1][y].second * grid[x][y]);
                    }
                    if (y > 0) {
                        dp[x][y].first = max(dp[x][y].first, dp[x][y - 1].first * grid[x][y]);
                        dp[x][y].second = min(dp[x][y].second, dp[x][y - 1].second * grid[x][y]);
                    }
                }
                else if (grid[x][y] < 0) {
                    if (x > 0) {
                        dp[x][y].first = max(dp[x][y].first, dp[x - 1][y].second * grid[x][y]);
                        dp[x][y].second = min(dp[x][y].second, dp[x - 1][y].first * grid[x][y]);
                    }
                    if (y > 0) {
                        dp[x][y].first = max(dp[x][y].first, dp[x][y - 1].second * grid[x][y]);
                        dp[x][y].second = min(dp[x][y].second, dp[x][y - 1].first * grid[x][y]);
                    }
                }
                else {
                    dp[x][y] = {0, 0};
                }
            }
        }

        for (int i = 1; i < M; ++i) {
            for (int x = i, y = N - 1; x < M && y >= 0; ++x, --y) {
                if (0 == grid[x][y]) {
                    dp[x][y] = {0, 0};
                }
                else if (grid[x][y] > 0) {
                    if (x > 0) {
                        dp[x][y].first = max(dp[x][y].first, dp[x - 1][y].first * grid[x][y]);
                        dp[x][y].second = min(dp[x][y].second, dp[x - 1][y].second * grid[x][y]);
                    }
                    if (y > 0) {
                        dp[x][y].first = max(dp[x][y].first, dp[x][y - 1].first * grid[x][y]);
                        dp[x][y].second = min(dp[x][y].second, dp[x][y - 1].second * grid[x][y]);
                    }
                }
                else {
                    if (x > 0) {
                        dp[x][y].first = max(dp[x][y].first, dp[x - 1][y].second * grid[x][y]);
                        dp[x][y].second = min(dp[x][y].second, dp[x - 1][y].first * grid[x][y]);
                    }
                    if (y > 0) {
                        dp[x][y].first = max(dp[x][y].first, dp[x][y - 1].second * grid[x][y]);
                        dp[x][y].second = min(dp[x][y].second, dp[x][y - 1].first * grid[x][y]);
                    }
                }
            }
        }

        return dp.back().back().first < 0 ? -1 : dp.back().back().first % mod;
    }
};

/**
 * Original solution. DFS solution. It is correct and passed the test. But TLE.
 * So we need to speed it up.
 */
class Solution {
public:
    int maxProductPath(vector<vector<int>>& grid) {
        if (0 == grid.back().back() || 0 == grid.front().front()) {
            return 0;
        }

        M = grid.size() - 1;
        N = grid[0].size() - 1;
        max_prod = -1;

        dfs(grid, 0, 0, 1);

        return max_prod < 0 ? -1 : max_prod % mod;
    }

private:
    void dfs(vector<vector<int>> const& grid, int const x, int const y, int64_t const prod)
    {
        if (x > M || y > N) {
            return;
        }

        if (x == M && y == N) {
            max_prod = max(max_prod, prod * grid.back().back());
            return;
        }

        if (0 == grid[x][y]) {
            max_prod = max<int64_t>(max_prod, 0);
            return;
        }

        dfs(grid, x, y + 1, prod * grid[x][y]);
        dfs(grid, x + 1, y, prod * grid[x][y]);
    }

    int M;
    int N;
    int64_t max_prod;
    int const mod = 1'000'000'007;
};
