/**
 * Find And Replace in String
 *
 * To some string S, we will perform some replacement operations that replace groups of letters with new ones (not
 * necessarily the same size).
 *
 * Each replacement operation has 3 parameters: a starting index i, a source word x and a target word y.  The rule is
 * that if x starts at position i in the original string S, then we will replace that occurrence of x with y.  If not,
 * we do nothing.
 *
 * For example, if we have S = "abcd" and we have some replacement operation i = 2, x = "cd", y = "ffff", then because
 * "cd" starts at position 2 in the original string S, we will replace it with "ffff".
 *
 * Using another example on S = "abcd", if we have both the replacement operation i = 0, x = "ab", y = "eee", as well as
 * another replacement operation i = 2, x = "ec", y = "ffff", this second operation does nothing because in the original
 * string S[2] = 'c', which doesn't match x[0] = 'e'.
 *
 * All these operations occur simultaneously.  It's guaranteed that there won't be any overlap in replacement: for
 * example, S = "abc", indexes = [0, 1], sources = ["ab","bc"] is not a valid test case.
 *
 * Example 1:
 *
 * Input: S = "abcd", indexes = [0,2], sources = ["a","cd"], targets = ["eee","ffff"]
 * Output: "eeebffff"
 * Explanation: "a" starts at index 0 in S, so it's replaced by "eee".
 * "cd" starts at index 2 in S, so it's replaced by "ffff".
 *
 * Example 2:
 *
 * Input: S = "abcd", indexes = [0,2], sources = ["ab","ec"], targets = ["eee","ffff"]
 * Output: "eeecd"
 * Explanation: "ab" starts at index 0 in S, so it's replaced by "eee".
 * "ec" doesn't starts at index 2 in the original S, so we do nothing.
 *
 * Notes:
 *     0 <= indexes.length = sources.length = targets.length <= 100
 *     0 < indexes[i] < S.length <= 1000
 *     All characters in given inputs are lowercase letters.
 */
/**
 * Original solution. The idea is:
 * 1. First we construct all the replace pair.
 * 2. Scan all pairs to find out which one should apply to the original S.
 *    And mark it with * in S
 * 3. Go through the S, replace the * with the target string, others remain same.
 */
class Solution {
public:
    string findReplaceString(string S, vector<int>& indexes, vector<string>& sources, vector<string>& targets) {
        map< int, pair<string, string> > memo;

        for (size_t i = 0; i < indexes.size(); ++i) {
            memo[indexes[i]] = { sources[i], targets[i] };
        }

        for (auto& m : memo) {
            if (0 == S.compare(m.first, m.second.first.length(), m.second.first)) {
                for (size_t j = m.first; j < (m.first + m.second.first.length()); ++j) {
                    S[j] = '*';
                }
            }
        }

        string ans;

        for (size_t k = 0; ; ) {
            if (k >= S.length()) break;

            if ('*' == S[k]) {
                ans += memo[k].second;
                k += memo[k].first.length();
            }
            else {
                ans += S[k];
                ++k;
            }
        }
        
        return ans;
    }
};
