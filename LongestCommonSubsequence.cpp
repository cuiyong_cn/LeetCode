/**
 * Longest Common Subsequence
 *
 * Given two strings text1 and text2, return the length of their longest common subsequence.
 *
 * A subsequence of a string is a new string generated from the original string with some
 * characters(can be none) deleted without changing the relative order of the remaining
 * characters. (eg, "ace" is a subsequence of "abcde" while "aec" is not). A common
 * subsequence of two strings is a subsequence that is common to both strings.
 *
 * If there is no common subsequence, return 0.
 *
 * Example 1:
 *
 * Input: text1 = "abcde", text2 = "ace"
 * Output: 3
 * Explanation: The longest common subsequence is "ace" and its length is 3.
 *
 * Example 2:
 *
 * Input: text1 = "abc", text2 = "abc"
 * Output: 3
 * Explanation: The longest common subsequence is "abc" and its length is 3.
 *
 * Example 3:
 *
 * Input: text1 = "abc", text2 = "def"
 * Output: 0
 * Explanation: There is no such common subsequence, so the result is 0.
 *
 * Constraints:
 *
 *     1 <= text1.length <= 1000
 *     1 <= text2.length <= 1000
 *     The input strings consist of lowercase English characters only.
 */
/**
 * Same idea but iterative. More clear and more compact. Faster than below
 * Every time we only need to check one row up. Therefore 2 rows are sufficient.
 */
class Solution {
public:
    int longestCommonSubsequence(string &a, string &b)
    {
        if (a.size() < b.size()) return longestCommonSubsequence(b, a);
        vector<vector<short>> m(2, vector<short>(b.size() + 1));
        for (auto i = 1; i <= a.size(); ++i) {
            for (auto j = 1; j <= b.size(); ++j) {
                if (a[i - 1] == b[j - 1]) {
                    m[i & 1][j] = m[(i - 1) & 1][j - 1] + 1;
                }
                else {
                    m[i & 1][j] = max(m[(i - 1) & 1][j], m[i & 1][j - 1]);
                }
            }
        }

        return m[a.size() & 1][b.size()];
    }
};

/**
 * My original solution. Using dynamic programming.
 *
 * DP[i][j] represents the longest common subsequence of text1[0 ... i] & text2[0 ... j]
 */
class Solution
{
public:
    int longestCommonSubsequence(string text1, string text2)
    {
        int rows = text1.size();
        int cols = text2.size();
        vector<vector<int> > memo(rows, vector<int>(cols, 0));
        return dp(rows - 1, cols - 1, text1, text2, memo);
    }
private:
    int dp(int i, int j, string& s1, string& s2, vector<vector<int> >& memo)
    {
        if (i < 0 || j < 0 || memo[i][j] < 0) return 0;
        if (memo[i][j] > 0) return memo[i][j];

        if (s1[i] == s2[j]) {
            memo[i][j] = 1 + dp(i - 1, j - 1, s1, s2, memo);
        } else {
            memo[i][j] = max(dp(i - 1, j, s1, s2, memo), dp(i, j - 1, s1, s2, memo));
        }
        if (0 == memo[i][j]) memo[i][j] = -1;
        return memo[i][j] > 0 ? memo[i][j] : 0;
    }
};
