/**
 * Rectangle Area
 *
 * Given the coordinates of two rectilinear rectangles in a 2D plane, return the total area covered
 * by the two rectangles.
 *
 * The first rectangle is defined by its bottom-left corner (ax1, ay1) and its top-right corner (ax2, ay2).
 *
 * The second rectangle is defined by its bottom-left corner (bx1, by1) and its top-right corner (bx2, by2).
 *
 * Example 1:
 *
 * Rectangle Area
 * Input: ax1 = -3, ay1 = 0, ax2 = 3, ay2 = 4, bx1 = 0, by1 = -1, bx2 = 9, by2 = 2
 * Output: 45
 * Example 2:
 *
 * Input: ax1 = -2, ay1 = -2, ax2 = 2, ay2 = 2, bx1 = -2, by1 = -2, bx2 = 2, by2 = 2
 * Output: 16
 *
 * Constraints:
 *
 * -104 <= ax1, ay1, ax2, ay2, bx1, by1, bx2, by2 <= 104
 */
/**
 * same.
 */
class Solution {
public:
    int computeArea(int A, int B, int C, int D, int E, int F, int G, int H) {
        int left = max(A,E), right = max(min(C,G), left);
        int bottom = max(B,F), top = max(min(D,H), bottom);
        return (C-A)*(D-B) - (right - left)*(top - bottom) + (G-E)*(H-F);
    }
};

/**
 * Compact below solution.
 */
class Solution {
public:
    int computeArea(int A, int B, int C, int D, int E, int F, int G, int H) {
        int x = min(G, C) > max(E, A) ? (min(G, C) - max(E, A)) : 0;
        int y = min(D, H) > max(B, F) ? (min(D, H) - max(B, F)) : 0;
        return (D - B) * (C - A) - (x * y) + (G - E) * (H - F);
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int computeArea(int ax1, int ay1, int ax2, int ay2, int bx1, int by1, int bx2, int by2) {
        return (ax2 - ax1) * (ay2 - ay1) + (bx2 - bx1) * (by2 - by1)
            - overlap_area({ax1, ay1}, {ax2, ay2}, {bx1, by1}, {bx2, by2});
    }
private:
    struct Point
    {
        int x;
        int y;
    };

    int overlap_area(Point l1, Point r1, Point l2, Point r2)
    {
        if (l1.x == r1.x || l1.y == r1.y || l2.x == r2.x || l2.y == r2.y) {
            return 0;
        }

        if (l1.x >= r2.x || l2.x >= r1.x) {
            return 0;
        }

        if (l1.y >= r2.y || l2.y >= r1.y) {
            return 0;
        }

        int obx = l1.x <= l2.x ? l2.x : l1.x;
        int oby = l2.y <= l1.y ? l1.y : l2.y;
        int otx = r1.x <= r2.x ? r1.x : r2.x;
        int oty = r2.y <= r1.y ? r2.y : r1.y;

        return (otx - obx) * (oty - oby);
    }
};
