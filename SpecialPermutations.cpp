/**
 * Special Permutations
 *
 * You are given a 0-indexed integer array nums containing n distinct positive
 * integers. A permutation of nums is called special if:
 *
 * For all indexes 0 <= i < n - 1, either nums[i] % nums[i+1] == 0 or nums[i+1] % nums[i] == 0.
 * Return the total number of special permutations. As the answer could be large, return it modulo 109 + 7.
 *
 * Example 1:
 *
 * Input: nums = [2,3,6]
 * Output: 2
 * Explanation: [3,6,2] and [2,6,3] are the two special permutations of nums.
 * Example 2:
 *
 * Input: nums = [1,4,3]
 * Output: 2
 * Explanation: [3,1,4] and [4,1,3] are the two special permutations of nums.
 *
 * Constraints:
 *
 * 2 <= nums.length <= 14
 * 1 <= nums[i] <= 109
 */
/**
 * Original solution.
 */
constexpr int const N = 1 << 14;
constexpr int const mod = 1e9 + 7;
int dp[14][N];

bool all_nums_selected(int mask, int exp_cnt)
{
    return mask == ((1 << exp_cnt) - 1);
}

bool selected(int mask, int idx)
{
    return mask & (1 << idx);
}

int dfs(int i, int mask, vector<int>& nums)
{
    if (all_nums_selected(mask, nums.size())) {
        return 1;
    }

    if (-1 == dp[i][mask]) {
        dp[i][mask] = 0;

        for (int j = 0; j < nums.size(); ++j) {
            if (!selected(mask, j)
                && (0 == mask || nums[i] % nums[j] == 0 || nums[j] % nums[i] == 0))
            {
                dp[i][mask] = (dp[i][mask] + dfs(j, mask | (1 << j), nums)) % mod;
            }
        }
    }

    return dp[i][mask];
}

class Solution {
public:
    int specialPerm(vector<int>& nums) {
        memset(dp, -1, sizeof(dp));
        return dfs(0, 0, nums);
    }
};
