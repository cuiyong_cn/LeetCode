/**
 * Rearrange Words in a Sentence
 *
 * Given a sentence text (A sentence is a string of space-separated words) in the following format:
 *
 * First letter is in upper case.  Each word in text are separated by a single space.  Your task is
 * to rearrange the words in text such that all words are rearranged in an increasing order of their
 * lengths. If two words have the same length, arrange them in their original order.
 *
 * Return the new text following the format shown above.
 *
 * Example 1:
 *
 * Input: text = "Leetcode is cool"
 * Output: "Is cool leetcode"
 * Explanation: There are 3 words, "Leetcode" of length 8, "is" of length 2 and "cool" of length 4.
 * Output is ordered by length and the new first word starts with capital letter.
 * Example 2:
 *
 * Input: text = "Keep calm and code on"
 * Output: "On and keep calm code"
 * Explanation: Output is ordered as follows:
 * "On" 2 letters.
 * "and" 3 letters.
 * "keep" 4 letters in case of tie order by position in original text.
 * "calm" 4 letters.
 * "code" 4 letters.
 * Example 3:
 *
 * Input: text = "To be or not to be"
 * Output: "To be or to be not"
 *
 * Constraints:
 *
 * text begins with a capital letter and then contains lowercase letters and single space between words.
 * 1 <= text.length <= 10^5
 */
/**
 * Optimize again.
 */
class Solution {
public:
    string arrangeWords(string text) {
        text[0] = tolower(text[0]);
        text += " ";

        unordered_map<int, vector<string_view>> len_words;
        for (size_t prev = 0; prev < text.length();) {
            auto ps = text.find(' ', prev);
            auto wlen = ps - prev;
            len_words[wlen].emplace_back(text.c_str() + prev, wlen);
            prev = ps + 1;
        }

        set<int> lens;
        for (auto const& lw : len_words) {
            lens.insert(lw.first);
        }

        auto copy = text;
        int p = 0;
        for (auto L : lens) {
            for (auto const& lw : len_words[L]) {
                for (auto c : lw) {
                    copy[p++] = c;
                }
                copy[p++] = ' ';
            }
        }

        copy.pop_back();
        copy[0] = toupper(copy[0]);

        return copy;
    }
};

/**
 * Optimize.
 */
class Solution {
public:
    string arrangeWords(string text) {
        text[0] = tolower(text[0]);

        unordered_map<int, string> len_words;
        stringstream ss{text};
        for (; !ss.eof(); ) {
            string word; ss >> word;
            len_words[word.length()] += word + " ";
        }

        set<int> lens;
        for (auto const& lw : len_words) {
            lens.insert(lw.first);
        }

        string ans;
        for (auto L : lens) {
            ans += len_words[L];
        }
        ans.pop_back();
        ans[0] = toupper(ans[0]);

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    string arrangeWords(string text) {
        text[0] = tolower(text[0]);

        unordered_map<int, vector<string>> len_words;
        stringstream ss{text};
        for (; !ss.eof(); ) {
            string word; ss >> word;
            len_words[word.length()].emplace_back(word);
        }

        set<int> lens;
        for (auto const& lw : len_words) {
            lens.insert(lw.first);
        }

        string ans;
        for (auto L : lens) {
            for (auto& w : len_words[L]) {
                ans += w + " ";
            }
        }
        ans.pop_back();
        ans[0] = toupper(ans[0]);

        return ans;
    }
};
