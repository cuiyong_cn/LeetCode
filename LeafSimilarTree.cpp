/**
 * Leaf-Similar Trees
 *
 * Consider all the leaves of a binary tree.  From left to right order, the
 * values of those leaves form a leaf value sequence.
 *                 3
 *                / \
 *               /   \
 *              5     1
 *             / \   / \
 *            6   2 9   8
 *               / \
 *              7   4
 *
 * For example, in the given tree above, the leaf value sequence is (6, 7, 4, 9, 8).
 *
 * Two binary trees are considered leaf-similar if their leaf value sequence is the same.
 *
 * Return true if and only if the two given trees with head nodes root1 and
 * root2 are leaf-similar.
 */
/**
 * My original version
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution
{
public:
    void leafSeq(TreeNode* r, vector<TreeNode*>& s)
    {
        if (r == nullptr) return;
        if (r->left) leafSeq(r->left, s);
        if (r->right) leafSeq(r->right, s);
        if (r->left == nullptr && r->right == nullptr) s.push_back(r);
    }
    bool leafSimilar(TreeNode* root1, TreeNode* root2)
    {
        vector<TreeNode*> s1, s2;
        leafSeq(root1, s1);
        leafSeq(root2, s2);
        return s1 == s2;
    }
};

/**
 * stack version
 */
class Solution
{
public:
    bool leafSimilar(TreeNode* root1, TreeNode* root2)
    {
        stack<TreeNode*> s1, s2;
        s1.push(root1);
        s2.push(root2);
        while (!s1.empty() && !s2.empty())
            if (dfs(s1) != dfs(s2)) return false;
        return s1.empty() && s2.empty();
    }

    int dfs(stack<TreeNode*>& s)
    {
        while (true) {
            TreeNode* node = s.top();
            s.pop();
            if (node->right) s.push(node->right);
            if (node->left) s.push(node->left);
            if (!node->left && !node->right) return node->val;
        }
    }
};

/**
 * Same core idea, but using string instead of vector
 */
class Solution
{
public:
    bool leafSimilar(TreeNode* root1, TreeNode* root2)
    {
        string t1, t2;
        DFS(root1, t1);
        DFS(root2, t2);
        return t1 == t2;
    }

    void DFS(TreeNode* root, string& s)
    {
        if (root == NULL) return;
        if (root->left == NULL && root->right == NULL) s += to_string(root->val) + "#";
        DFS(root->left, s);
        DFS(root->right, s);
    }
};

/**
 * Same core idea, but using hash
 */
class Solution
{
public:
    void computeLeafsHash(TreeNode* node, long long &leafsHash)
    {
        if (!node) return;
        if (node->left || node->right) {
            computeLeafsHash(node->left, leafsHash);
            computeLeafsHash(node->right, leafsHash);
        } else {
            leafsHash = (leafsHash * 31) + node->val;
        }
    }
    bool leafSimilar(TreeNode* root1, TreeNode* root2)
    {
        long long leafsHash1 = 0, leafsHash2 = 0;
        computeLeafsHash(root1, leafsHash1);
        computeLeafsHash(root2, leafsHash2);
        return leafsHash1 == leafsHash2;
    };
};

