/** Cousins in Binary Tree
 *
 * In a binary tree, the root node is at depth 0, and children of each depth k node are at depth k+1.
 * Two nodes of a binary tree are cousins if they have the same depth, but have different parents.
 * We are given the root of a binary tree with unique values, and the values x and y of two different nodes in the tree.
 *
 * Return true if and only if the nodes corresponding to the values x and y are cousins.
 *
 * Example 1:
 *
 * Input: root = [1,2,3,4], x = 4, y = 3 Output: false
 *
 * Example 2:
 *
 * Input: root = [1,2,3,null,4,null,5], x = 5, y = 4 Output: true
 *
 * Example 3:
 *
 * Input: root = [1,2,3,null,4], x = 2, y = 3 Output: false
 *
 * Note:
 *     The number of nodes in the tree will be between 2 and 100.  Each node has a unique integer value from 1 to 100.
 */
/**
 * original solution. simple and intuitive.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution
{
public:
    bool isCousins(TreeNode* root, int x, int y)
    {
        auto tx = find(root, nullptr, x, -1);

        auto ty = find(root, nullptr, y, -1);
        if (tx.second > 0 && tx.second == ty.second) {
            if (tx.first != ty.first) return true;
        }

        return false;
    }

    pair<TreeNode*, int> find(TreeNode* cur, TreeNode* parent, int t, int lvl)
    {
        if (nullptr == cur) return {};
        if (cur->val == t) return {parent, lvl + 1};

        auto pl = find(cur->left, cur, t, lvl + 1);
        if (pl.first) return pl;
        return find(cur->right, cur, t, lvl + 1);
    }
};

/**
 * User votrubac's solution. Using level order traversal,
 * using nullptr as a maker to disguish the siblings from cousins.
 */
class Solution
{
public:
    bool isCousins(TreeNode* root, int x, int y, bool siblings = false, bool cousin = false)
    {
        queue<TreeNode*> q, q1;
        q.push(root);

        while (!q.empty() && !cousin) { // cousin = true means we found one
            while (!q.empty()) {
                auto n = q.front();
                q.pop();
                if (n == nullptr) siblings = false;
                else {
                    if (n->val == x || n->val == y) {
                        if (!cousin) cousin = siblings = true;
                        else return !siblings;
                    }
                    q1.push(n->left), q1.push(n->right), q1.push(nullptr);
                }
            }
            swap(q, q1);
        }
        return false;
    }
};

