/**
 * Random Pick Index
 *
 * Given an array of integers with possible duplicates, randomly output the index of a given target number. You can
 * assume that the given target number must exist in the array.
 *
 * Note:
 * The array size can be very large. Solution that uses too much extra space will not pass the judge.
 *
 * Example:
 *
 * int[] nums = new int[] {1,2,3,3,3};
 * Solution solution = new Solution(nums);
 *
 * // pick(3) should return either index 2, 3, or 4 randomly. Each index should have equal probability of returning.
 * solution.pick(3);
 *
 * // pick(1) should return 0. Since in the array only nums[0] is equal to 1.
 * solution.pick(1);
 */
/**
 * From user lycjava. Using RESERVOIR SAMPLING theory.
 * Intresting one. I don't know that.
 */
public class Solution {
    int[] nums;
    Random rand;
    public Solution(int[] nums) {
        this.nums = nums;
        this.rand = new Random();
    }
    public int pick(int target) {
        int total = 0;
        int res = -1;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == target) {
                int x = rand.nextInt(++total); 
                res = x == 0 ? i : res;
            }
        }
        return res;
    }
}

/**
 * The following solution passed. But now I understand why so many people downvote this problem.
 */
class Solution {
public:
    class Cache
    {
    public:
        Cache() {}
        Cache(vector<int>& in) : index{in}, distribution(0, in.size() - 1) {}
        int getIndex() { return index[distribution(generator)]; }
        vector<int> index;
        std::default_random_engine generator;
        std::uniform_int_distribution<int> distribution;
    };

    Solution(vector<int>& nums) : n{nums} {

    }

    int pick(int target) {
        if (0 == cm.count(target)) {
            vector<int> index;
            for (int i = 0; i < n.size(); ++i) {
                if (n[i] == target) {
                    index.push_back(i);
                }
            }

            cm[target] = Cache(index);
        }

        return cm[target].getIndex();
    }

    vector<int>& n;
    unordered_map<int, Cache> cm;
};

/**
 * Original solution. I think this gonna work, but TLE(weird)
 */
class Solution {
public:
    vector<int> sorted;
    vector<int>& nums;
    class helper
    {
    public:
        helper(vector<int>& nums) : nums{nums} {}
        bool operator()(int i, int j) {
            return nums[i] < nums[j];
        }
        vector<int>& nums;
    };

    Solution(vector<int>& nums) : nums{nums} {
        sorted = vector<int>(nums.size());
        iota(sorted.begin(), sorted.end(), 0);
        sort(sorted.begin(), sorted.end(), helper(nums));
    }

    int pick(int target) {
        int L = 0, R = sorted.size(), mid = (L + R) / 2;
        while (1) {
            int val = nums[sorted[mid]];
            if (val == target) {
                break;
            }
            else if (val < target) {
                L = mid + 1;
            }
            else {
                R = mid - 1;
            }
        }

        int count = 1;
        for (int i = mid - 1; i >= 0; --i) {
            if (nums[sorted[i]] == target) {
                ++count;
            }
            else {
                break;
            }
        }
        int start = mid - count + 1;

        for (int i = mid + 1; i < sorted.size(); ++i) {
            if (nums[sorted[i]] == target) {
                ++count;
            }
            else {
                break;
            }
        }
        std::default_random_engine generator;
        std::uniform_int_distribution<int> distribution(0, count - 1);
        int pick = start + distribution(generator);
        return sorted[pick];
    }
};

/**
 * Your Solution object will be instantiated and called as such:
 * Solution* obj = new Solution(nums);
 * int param_1 = obj->pick(target);
 */
