/**
 * Minimum Flips to Make a OR b Equal to c
 *
 * Given 3 positives numbers a, b and c. Return the minimum flips required in some bits of a and b
 * to make ( a OR b == c ). (bitwise OR operation).
 * Flip operation consists of change any single bit 1 to 0 or change the bit 0 to 1 in their binary
 * representation.
 *
 * Example 1:
 *
 * Input: a = 2, b = 6, c = 5
 * Output: 3
 * Explanation: After flips a = 1 , b = 4 , c = 5 such that (a OR b == c)
 * Example 2:
 *
 * Input: a = 4, b = 2, c = 7
 * Output: 1
 * Example 3:
 *
 * Input: a = 1, b = 2, c = 3
 * Output: 0
 *
 * Constraints:
 *
 * 1 <= a <= 10^9
 * 1 <= b <= 10^9
 * 1 <= c <= 10^9
 */
/**
 * Or even shorter
 */
class Solution {
public:
    int minFlips(int a, int b, int c) {
        return __builtin_popcount((a | b) ^ c) + __builtin_popcount(a & b & ~c);
    }
};

/**
 * Without bitset. __builtin_popcount is gcc's extension. popcount is c++20 std lib.
 * But leetcode does not support c++20 at the time of this writing.
 */
class Solution {
public:
    int minFlips(int a, int b, int c) {
        auto flip = (a | b) ^ c;
        return __builtin_popcount(flip) + __builtin_popcount(a & b & flip);
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int minFlips(int a, int b, int c) {
        int ans = 0;
        bitset<8 * sizeof(int)> const A = a, B = b, C = c;

        for (size_t i = 0; i < A.size(); ++i) {
            if ((A[i] | B[i]) != C[i]) {
                if (C[i]) {
                    ++ans;
                }
                else {
                    if (A[i]) {
                        ++ans;
                    }
                    if (B[i]) {
                        ++ans;
                    }
                }
            }
        }

        return ans;
    }
};
