/**
 * Find Duplicate Subtrees
 *
 * Given a binary tree, return all duplicate subtrees. For each kind of duplicate subtrees, you only need to return the
 * root node of any one of them.
 *
 * Two trees are duplicate if they have the same structure with same node values.
 *
 * Example 1:
 *         1
 *        / \
 *       2   3
 *      /   / \
 *     4   2   4
 *        /
 *       4
 *
 * The following are two duplicate subtrees:
 *
 *       2
 *      /
 *     4
 *
 * and
 *     4
 *
 * Therefore, you need to return above trees' root in the form of a list.
 */
/**
 * Solution from leetcode. Idea is serialize the tree into a string.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<TreeNode*> findDuplicateSubtrees(TreeNode* root) {
        serial(root);
        return ans;
    }

private:
    std::string serial(TreeNode* node)
    {
        if (nullptr == node) return "#";

        std::string tree_str = std::to_string(node->val) + ","
                               + serial(node->left) + ","
                               + serial(node->right);
        int count = ++memo[tree_str];
        if (count == 2) ans.push_back(node);

        return tree_str;
    }
private:
    map<string, int> memo;
    vector<TreeNode*> ans;
};
