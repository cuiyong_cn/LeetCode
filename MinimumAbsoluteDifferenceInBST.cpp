/**
 * Minimum Absolute Difference in BST
 *
 * Given a binary search tree with non-negative values, find the minimum absolute difference between values of any two nodes.
 *
 * Example:
 *
 * Input:
 *
 *    1
 *     \
 *      3
 *     /
 *    2
 *
 * Output:
 * 1
 *
 * Explanation:
 * The minimum absolute difference is 1, which is the difference between 2 and 1 (or between 2 and 3).
 *
 * Note: There are at least two nodes in this BST.
 */
/**
 * Original solution. Using in-order traverse.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution
{
public:
    int getMinimumDifference(TreeNode* root)
    {
        stack<TreeNode*> stk;
        TreeNode* cur = root;
        int prev = -1;
        int ans = numeric_limits<int>::max();

        while (false == stk.empty() || nullptr != cur) {
            while (nullptr != cur) {
                stk.push(cur);
                cur = cur->left;
            }

            cur = stk.top();
            stk.pop();
            if (prev >= 0) {
                ans = min(ans, cur->val - prev);
            }
            prev = cur->val;
            cur = cur->right;
        }

        return ans;
    }
};

/**
 * Same idea, recursive version.
 */
class Solution
{
    int min_dif = INT_MAX, val = -1;
public:
    int getMinimumDifference(TreeNode* root)
    {
        if (root->left != NULL) getMinimumDifference(root->left);
        if (val >= 0) min_dif = min(min_dif, root->val - val);
        val = root->val;
        if (root->right != NULL) getMinimumDifference(root->right);
        return min_dif;
    }

