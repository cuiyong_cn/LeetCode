/**
 * Valid Triangle Number
 *
 * Given an array consists of non-negative integers, your task is to count the number of triplets chosen from the array
 * that can make triangles if we take them as side lengths of a triangle.
 *
 * Example 1:
 *
 * Input: [2,2,3,4]
 * Output: 3
 * Explanation:
 * Valid combinations are:
 * 2,3,4 (using the first 2)
 * 2,3,4 (using the second 2)
 * 2,2,3
 *
 * Note:
 *     The length of the given array won't exceed 1000.
 *     The integers in the given array are in the range of [0, 1000].
 */
/**
 * passed solution. The idea is:
 * 1. first we sort the nums
 * 2. then for each pair (nums[i], nums[j]) (j > i), we find the first index k that nums[i] + nums[j] > nums[k]
 *    do not hold. Then for pair (nums[i], nums[j]), the valid triangle can be formed is k - j - 1.
 *
 * This solution works, because nums[i] <= nums[j] <= nums[k], if (nums[i] + nums[j] < nums[k]), then all the index
 * int j .. k are all valid. Because nums[i] + nums[k] > nums[j] and nums[j] + nums[k] > nums[i] are implicitly
 * hold
 *
 * Also we can replace the search of k with binary search. (note: binary search does not guarrentee better than linear,
 * binary search may suffer from cache thrashing)
 */
class Solution {
public:
    int triangleNumber(vector<int>& nums) {
        if (nums.size() < 3) return 0;

        std::sort(std::begin(nums), std::end(nums));
        int ans = 0;
        for (size_t i = 0; i < (nums.size() - 2); ++i) {
            if (nums[i] > 0) {
                size_t k = i + 2;
                for (size_t j = i + 1; j < (nums.size() - 1); ++j) {
                    while (k < nums.size() && (nums[i] + nums[j]) > nums[k]) {
                        ++k;
                    }
                    ans += k - j - 1;
                }
            }
        }

        return ans;
    }
};

/**
 * Original solution. correct but TLE.
 */
class Solution {
public:
    int triangleNumber(vector<int>& nums) {
        int ans = 0;
        for (size_t i = 0; i < nums.size(); ++i) {
            for (size_t j = i + 1; j < nums.size(); ++j) {
                for (size_t k = j + 1; k < nums.size(); ++k) {
                    int a = nums[i], b = nums[j], c = nums[k];
                    if ((a + b) > c
                        && (a + c) > b
                        && (b + c) > a) {
                        ++ans;
                    }
                }
            }
        }

        return ans;
    }
};
