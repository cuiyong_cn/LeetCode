/**
 * Unique Paths III
 *
 * On a 2-dimensional grid, there are 4 types of squares:
 *
 *     1 represents the starting square.  There is exactly one starting square.
 *     2 represents the ending square.  There is exactly one ending square.
 *     0 represents empty squares we can walk over.
 *     -1 represents obstacles that we cannot walk over.
 *
 * Return the number of 4-directional walks from the starting square to the
 * ending square, that walk over every non-obstacle square exactly once.
 *
 *
 * Example 1:
 *
 * Input: [[1,0,0,0],[0,0,0,0],[0,0,2,-1]]
 * Output: 2
 * Explanation: We have the following two paths:
 * 1. (0,0),(0,1),(0,2),(0,3),(1,3),(1,2),(1,1),(1,0),(2,0),(2,1),(2,2)
 * 2. (0,0),(1,0),(2,0),(2,1),(1,1),(0,1),(0,2),(0,3),(1,3),(1,2),(2,2)
 *
 * Example 2:
 *
 * Input: [[1,0,0,0],[0,0,0,0],[0,0,0,2]]
 * Output: 4
 * Explanation: We have the following four paths:
 * 1. (0,0),(0,1),(0,2),(0,3),(1,3),(1,2),(1,1),(1,0),(2,0),(2,1),(2,2),(2,3)
 * 2. (0,0),(0,1),(1,1),(1,0),(2,0),(2,1),(2,2),(1,2),(0,2),(0,3),(1,3),(2,3)
 * 3. (0,0),(1,0),(2,0),(2,1),(2,2),(1,2),(1,1),(0,1),(0,2),(0,3),(1,3),(2,3)
 * 4. (0,0),(1,0),(2,0),(2,1),(1,1),(0,1),(0,2),(0,3),(1,3),(1,2),(2,2),(2,3)
 *
 * Example 3:
 *
 * Input: [[0,1],[2,0]]
 * Output: 0
 * Explanation:
 * There is no path that walks over every empty square exactly once.
 * Note that the starting and ending square can be anywhere in the grid.
 *
 * Note:
 *
 *     1 <= grid.length * grid[0].length <= 20
 */
class Solution {
public:
    enum dir { E = 0, S, W, N, DN };
    int width = 0;
    int height = 0;

    int explore(vector<vector<int>>& grid,
               int sx, int sy, int left) {
        if (sx < 0 || sy < 0 || sx > width || sy > height) return 0;

        switch (grid[sy][sx]) {
            case 1: case -1: case 3:
                return 0;
            case 2:
                return left == 0 ? 1 : 0;
            case 0:
                grid[sy][sx] = 3;
                --left;
                break;
            default:
                break;
        }

        int path = 0;
        // Here we could use two dimesion array to short this
        // block of code
        for (int dir = E; dir < DN; ++dir) {
            switch (dir) {
            case E:
                path += explore(grid, sx + 1, sy, left);
                break;
            case S:
                path += explore(grid, sx, sy + 1, left);
                break;
            case W:
                path += explore(grid, sx - 1, sy, left);
                break;
            case N:
                path += explore(grid, sx, sy - 1, left);
                break;
            }
        }

        grid[sy][sx] = 0;
        return path;
    }

    int uniquePathsIII(vector<vector<int>>& grid) {
        height = grid.size() - 1;
        width = height >= 0 ? grid[0].size() - 1 : 0;
        int sx = 0, sy = 0, left = 0;

        for (int h = 0; h <= height; ++h) {
            for (int w = 0; w <= width; ++w) {
                switch (grid[h][w]) {
                case 1:
                    sx = w, sy = h;
                    break;
                case 0:
                    left++;
                    break;
                default:
                    break;
                }
            }
        }

        int path = 0;
        for (int dir = E; dir < DN; ++dir) {
            switch (dir) {
            case E:
                path += explore(grid, sx + 1, sy, left);
                break;
            case S:
                path += explore(grid, sx, sy + 1, left);
                break;
            case W:
                path += explore(grid, sx - 1, sy, left);
                break;
            case N:
                path += explore(grid, sx, sy - 1, left);
                break;
            default:
                break;
            }
        }

        return path;
    }
};
