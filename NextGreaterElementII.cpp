/**
 * Next Greater Element II
 *
 * Given a circular array (the next element of the last element is the first element of the array), print the Next
 * Greater Number for every element. The Next Greater Number of a number x is the first greater number to its
 * traversing-order next in the array, which means you could search circularly to find its next greater number. If it
 * doesn't exist, output -1 for this number.
 *
 * Example 1:
 * Input: [1,2,1]
 * Output: [2,-1,2]
 * Explanation: The first 1's next greater number is 2;
 * The number 2 can't find next greater number;
 * The second 1's next greater number needs to search circularly, which is also 2.
 * Note: The length of given array won't exceed 10000.
 */
/**
 * More compact one.
 */
class Solution {
public:
    vector<int> nextGreaterElements(vector<int>& nums) {
        vector<int> ans(nums.size(), -1);
        auto len = nums.size();

        stack<int> stk;
        for (int i = 2 * len - 1; i >= 0; --i) {
            int base = nums[i % len];
            while (false == stk.empty()) {
                if (base < stk.top()) {
                    ans[i % len] = stk.top();
                    break;
                }
                else {
                    stk.pop();
                }
            }

            stk.push(base);
        }

        return ans;
    }
};

/**
 * Same idea with two pass method.
 */
class Solution {
public:
    vector<int> nextGreaterElements(vector<int>& nums) {
        vector<int> ans(nums.size(), -1);
        vector<int> filled(nums.size(), false);

        stack<int> stk;
        for (int i = nums.size() - 1; i >= 0; --i) {
            int base = nums[i];
            while (false == stk.empty()) {
                if (base < stk.top()) {
                    ans[i] = stk.top();
                    filled[i] = true;
                    break;
                }
                else {
                    stk.pop();
                }
            }
            stk.push(base);
        }

        for (int i = nums.size() - 1; i >= 0; --i) {
            if (true == filled[i]) continue;

            int base = nums[i];
            while (false == stk.empty()) {
                if (base < stk.top()) {
                    ans[i] = stk.top();
                    filled[i] = true;
                    break;
                }
                else {
                    stk.pop();
                }
            }
        }

        return ans;
    }
};

/**
 * Using stack to memorize the previous result to speed it up.
 */
class Solution {
public:
    vector<int> nextGreaterElements(vector<int>& nums) {
        vector<int> ans(nums.size(), 0);

        stack<int> stk;
        for (int i = nums.size() - 1; i >= 0; --i) {
            int base = nums[i];
            bool find = false;
            while (false == stk.empty()) {
                if (base < stk.top()) {
                    ans[i] = stk.top();
                    find = true;
                    break;
                }
                else {
                    stk.pop();
                }
            }

            if (false == find) {
                for (int j = 0; j < i; ++j) {
                    if (base < nums[j]) {
                        ans[i] = nums[j];
                        find = true;
                        stk.push(nums[j]);
                        break;
                    }
                }
            }

            if (false == find) {
                ans[i] = -1;
            }
            stk.push(base);
        }

        return ans;
    }
};
/**
 * Original solution. Brute force. For every element, we find the next greater element after it, if we does not find it.
 * We search the elements before it.
 */
class Solution {
public:
    vector<int> nextGreaterElements(vector<int>& nums) {
        vector<int> ans(nums.size(), 0);

        stack<int> stk;
        for (int i = 0; i < nums.size(); ++i) {
            int base = nums[i];
            bool find = false;
            for (int j = i + 1; j < nums.size(); ++j) {
                if (nums[j] > base) {
                    ans[i] = nums[j];
                    find = true;
                    break;
                }
            }

            if (false == find) {
                for (int k = 0; k < i; ++k) {
                    if (nums[k] > base) {
                        ans[i] = nums[k];
                        find = true;
                        break;
                    }
                }
            }

            if (false == find) {
                ans[i] = -1;
            }
        }

        return ans;
    }
};
