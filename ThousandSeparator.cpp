/**
 * Thousand Separator
 *
 * Given an integer n, add a dot (".") as the thousands separator and return it in string format.
 *
 * Example 1:
 *
 * Input: n = 987
 * Output: "987"
 * Example 2:
 *
 * Input: n = 1234
 * Output: "1.234"
 *
 * Constraints:
 *
 * 0 <= n <= 231 - 1
 */
/**
 * Original solution.
 */
class Solution {
public:
    string thousandSeparator(int n) {
        string number = to_string(n);
        string ans;
        for (int i = number.length() - 1, cnt = 0; i >= 0; --i) {
            if (3 == cnt) {
                ans.push_back('.');
                cnt = 0;
            }
            ans.push_back(number[i]);
            ++cnt;
        }

        reverse(ans.begin(), ans.end());

        return ans;
    }
};
