/**
 * Minimum Genetic Mutation
 *
 * A gene string can be represented by an 8-character long string, with choices from "A", "C", "G",
 * "T".
 *
 * Suppose we need to investigate about a mutation (mutation from "start" to "end"), where ONE
 * mutation is defined as ONE single character changed in the gene string.
 *
 * For example, "AACCGGTT" -> "AACCGGTA" is 1 mutation.
 *
 * Also, there is a given gene "bank", which records all the valid gene mutations. A gene must be in
 * the bank to make it a valid gene string.
 *
 * Now, given 3 things - start, end, bank, your task is to determine what is the minimum number of
 * mutations needed to mutate from "start" to "end". If there is no such a mutation, return -1.
 *
 * Note:
 *
 * Starting point is assumed to be valid, so it might not be included in the bank.
 * If multiple mutations are needed, all mutations during in the sequence must be valid.
 * You may assume start and end string is not the same.
 *
 * Example 1:
 *
 * start: "AACCGGTT"
 * end:   "AACCGGTA"
 * bank: ["AACCGGTA"]
 *
 * return: 1
 *
 * Example 2:
 *
 * start: "AACCGGTT"
 * end:   "AAACGGTA"
 * bank: ["AACCGGTA", "AACCGCTA", "AAACGGTA"]
 *
 * return: 2
 *
 * Example 3:
 *
 * start: "AAAAACCC"
 * end:   "AACCCCCC"
 * bank: ["AAAACCCC", "AAACCCCC", "AACCCCCC"]
 *
 * return: 3
 */
/**
 * A little improvement.
 */
class Solution {
public:
    int minMutation(string start, string end, vector<string>& bank) {
        unordered_set<string> gene_bank(std::begin(bank), std::end(bank));

        int glen = start.length();
        string const& base = "ACGT";
        deque<string> mq{start};
        int mutation_count = 0;
        while (!mq.empty()) {
            deque<string> nq;
            for (auto& gs : mq) {
                if (gs == end) {
                    return mutation_count;
                }

                for (int i = 0; i < glen; ++i) {
                    auto c = gs[i];
                    for (int j = 0; j < base.length(); ++j) {
                        if (c != base[j]) {
                            gs[i] = base[j];
                            if (gene_bank.count(gs)) {
                                gene_bank.erase(gs);
                                nq.push_back(gs);
                            }
                        }
                    }
                    gs[i] = c;
                }
            }

            ++mutation_count;
            swap(mq, nq);
        }

        return -1;
    }
};

/**
 * Original solution. The idea is:
 * We mutated the initial gene once a time. And return the steps onece start == end
 */
class Solution {
public:
    int minMutation(string start, string end, vector<string>& bank) {
        unordered_set<string> gene_bank(std::begin(bank), std::end(bank));
        unordered_set<string> mutated;

        int glen = start.length();
        int ans = 0;
        string const& base = "ACGT";
        deque<pair<string, int>> mq{{start, 0}};
        while (!mq.empty()) {
            deque<pair<string, int>> nq;
            for (auto& gp : mq) {
                auto& [gs, mc] = gp;
                if (gs == end) {
                    return mc;
                }

                for (int i = 0; i < glen; ++i) {
                    auto c = gs[i];
                    for (int j = 0; j < base.length(); ++j) {
                        if (c != base[j]) {
                            gs[i] = base[j];
                            if (gene_bank.count(gs) && !mutated.count(gs)) {
                                nq.push_back({gs, mc + 1});
                                mutated.insert(gs);
                            }
                        }
                    }
                    gs[i] = c;
                }
            }

            swap(mq, nq);
        }

        return -1;
    }
};
