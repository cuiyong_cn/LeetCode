/**
 * Maximum Number of Weeks for Which You Can Work
 *
 * There are n projects numbered from 0 to n - 1. You are given an integer array milestones where
 * each milestones[i] denotes the number of milestones the ith project has.
 *
 * You can work on the projects following these two rules:
 *
 * Every week, you will finish exactly one milestone of one project. You must work every week.  You
 * cannot work on two milestones from the same project for two consecutive weeks.  Once all the
 * milestones of all the projects are finished, or if the only milestones that you can work on will
 * cause you to violate the above rules, you will stop working. Note that you may not be able to
 * finish every project's milestones due to these constraints.
 *
 * Return the maximum number of weeks you would be able to work on the projects without violating
 * the rules mentioned above.
 *
 * Example 1:
 *
 * Input: milestones = [1,2,3]
 * Output: 6
 * Explanation: One possible scenario is:
 * - During the 1st week, you will work on a milestone of project 0.
 * - During the 2nd week, you will work on a milestone of project 2.
 * - During the 3rd week, you will work on a milestone of project 1.
 * - During the 4th week, you will work on a milestone of project 2.
 * - During the 5th week, you will work on a milestone of project 1.
 * - During the 6th week, you will work on a milestone of project 2.
 * The total number of weeks is 6.
 * Example 2:
 *
 * Input: milestones = [5,2,1]
 * Output: 7
 * Explanation: One possible scenario is:
 * - During the 1st week, you will work on a milestone of project 0.
 * - During the 2nd week, you will work on a milestone of project 1.
 * - During the 3rd week, you will work on a milestone of project 0.
 * - During the 4th week, you will work on a milestone of project 1.
 * - During the 5th week, you will work on a milestone of project 0.
 * - During the 6th week, you will work on a milestone of project 2.
 * - During the 7th week, you will work on a milestone of project 0.
 * The total number of weeks is 7.
 * Note that you cannot work on the last milestone of project 0 on 8th week because it would violate the rules.
 * Thus, one milestone in project 0 will remain unfinished.
 *
 * Constraints:
 *
 * n == milestones.length
 * 1 <= n <= 105
 * 1 <= milestones[i] <= 109
 */
/**
 * From discussion. Passed one.
 */
class Solution {
public:
    long long numberOfWeeks(vector<int>& milestones) {
        int max_milestones = 0;
        long long total_milestones = 0;
        for (auto const& m : milestones) {
            max_milestones = max(max_milestones, m);
            total_milestones += m;
        }

        auto total_milestones_ex_max = total_milestones - max_milestones;

        return total_milestones_ex_max < max_milestones ? 2 * total_milestones_ex_max + 1 : total_milestones;
    }
};

/**
 * Original solution. TLE.
 */
template<typename T>
using MaxHeap = priority_queue<T>;

class Solution {
public:
    long long numberOfWeeks(vector<int>& milestones) {
        MaxHeap<pair<int, int>> project_milestones;
        int const projects = milestones.size();
        for (int i = 0; i < projects; ++i) {
            project_milestones.emplace(milestones[i], i);
        }

        long long ans = 0;

        while (project_milestones.size() > 1) {
            cout << project_milestones.size() << '\n';
            auto [ms1, p1] = project_milestones.top();
            project_milestones.pop();
            auto [ms2, p2] = project_milestones.top();
            project_milestones.pop();
            if (ms1 == ms2) {
                ans += ms1 * 2;
            }
            else {
                auto [ms3, p3] = project_milestones.top();
                auto delta = ms2 - (ms3 - 1);
                ans += delta * 2;
                project_milestones.emplace(ms1 - delta, p1);
                if (ms3 > 1) {
                    project_milestones.emplace(ms3 - 1, p2);
                }
            }
        }

        return ans + (project_milestones.empty() ? 0 : 1);
    }
};
