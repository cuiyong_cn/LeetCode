/**
 * In the "100 game" two players take turns adding, to a running total, any integer from 1 to 10. The player who first causes the running total to reach or exceed 100 wins.
 *
 * What if we change the game so that players cannot re-use integers?
 *
 * For example, two players might take turns drawing from a common pool of numbers from 1 to 15 without replacement until they reach a total >= 100.
 *
 * Given two integers maxChoosableInteger and desiredTotal, return true if the first player to move can force a win, otherwise, return false. Assume both players play optimally.
 *
 *
 *
 * Example 1:
 *
 * Input: maxChoosableInteger = 10, desiredTotal = 11
 * Output: false
 * Explanation:
 * No matter which integer the first player choose, the first player will lose.
 * The first player can choose an integer from 1 up to 10.
 * If the first player choose 1, the second player can only choose integers from 2 up to 10.
 * The second player will win by choosing 10 and get a total = 11, which is >= desiredTotal.
 * Same with other integers chosen by the first player, the second player will always win.
 * Example 2:
 *
 * Input: maxChoosableInteger = 10, desiredTotal = 0
 * Output: true
 * Example 3:
 *
 * Input: maxChoosableInteger = 10, desiredTotal = 1
 * Output: true
 *
 *
 * Constraints:
 *
 * 1 <= maxChoosableInteger <= 20
 * 0 <= desiredTotal <= 300
 */
/**
 * Add one more early-terminate condition. If we need to choose all the numbers to reach
 * the desiredTotal, then obviously there are two cases:
 *  1. the count of the numbers are even, so first one choose always lose.
 *  2. the count of the numbers are odd, so first one choose always win.
 */
class Solution {
public:
    bool canIWin(int maxChoosableInteger, int desiredTotal) {
        if (desiredTotal <= maxChoosableInteger) {
            return true;
        }

        int sum = (1 + maxChoosableInteger) * maxChoosableInteger / 2;
        if (sum < desiredTotal) {
            return false;
        }

        if (sum == desiredTotal) {
            return maxChoosableInteger & 1;
        }

        memo = vector<uint8_t>(1 << maxChoosableInteger, 0);
        return canWin(desiredTotal, maxChoosableInteger + 1, 0);
    }

private:
    bool canWin(int desiredTotal, int N, int key)
    {
        if (desiredTotal <= 0) {
            return false;
        }

        if (memo[key] > 0) {
            return 1 == memo[key];
        }

        for (int i = 1; i < N; ++i) {
            if ((key >> (i - 1)) & 1) {
                continue;
            }

            if (!canWin(desiredTotal - i, N, key | (1 << (i - 1)))) {
                memo[key] = 1;
                return true;
            }
        }

        memo[key] = 2;

        return false;
    }

private:
    vector<uint8_t> memo;
};

/**
 * Optimise further, every time we need to use vector<bool> to calculate the key. This is time
 * consuming and can be eliminated by generating the key along the way.
 */
class Solution {
public:
    bool canIWin(int maxChoosableInteger, int desiredTotal) {
        int sum = (1 + maxChoosableInteger) * maxChoosableInteger / 2;
        if (sum < desiredTotal) {
            return false;
        }

        if (desiredTotal <= maxChoosableInteger) {
            return true;
        }

        memo = vector<uint8_t>(1 << maxChoosableInteger, 0);
        return canWin(desiredTotal, maxChoosableInteger + 1, 0);
    }

private:
    bool canWin(int desiredTotal, int N, int key)
    {
        if (desiredTotal <= 0) {
            return false;
        }

        if (memo[key] > 0) {
            return 1 == memo[key];
        }

        for (int i = 1; i < N; ++i) {
            if ((key >> (i - 1)) & 1) {
                continue;
            }

            if (!canWin(desiredTotal - i, N, key | (1 << (i - 1)))) {
                memo[key] = 1;
                return true;
            }
        }

        memo[key] = 2;

        return false;
    }

private:
    vector<uint8_t> memo;
};

/**
 * Ok, I change the unordered_map to vector. It passed, so, although unodered_map access is O(n)
 * same as vector, unordered_map is still slower than vector
 */
class Solution {
public:
    bool canIWin(int maxChoosableInteger, int desiredTotal) {
        int sum = (1 + maxChoosableInteger) * maxChoosableInteger / 2;
        if (sum < desiredTotal) {
            return false;
        }

        if (desiredTotal <= maxChoosableInteger) {
            return true;
        }

        memo = vector<uint8_t>(1 << (maxChoosableInteger + 1), 0);
        vector<bool> nums(maxChoosableInteger + 1, false);
        return canWin(nums, desiredTotal);
    }

private:
    bool canWin(vector<bool>& nums, int desiredTotal)
    {
        if (desiredTotal <= 0) {
            return false;
        }

        int key = reduce(nums);
        if (memo[key] > 0) {
            return 1 == memo[key];
        }

        for (int i = 1; i < nums.size(); ++i) {
            if (nums[i]) {
                continue;
            }

            nums[i] = true;
            if (!canWin(nums, desiredTotal - i)) {
                memo[key] = 1;
                nums[i] = false;
                return true;
            }
            nums[i] = false;
        }

        memo[key] = 2;

        return false;
    }

private:
    int reduce(vector<bool> const& nums)
    {
        int k = 0;
        for (int i = 1; i < nums.size(); ++i) {
            if (nums[i]) {
                k |= (1 << i);
            }
        }

        return k;
    }

private:
    vector<uint8_t> memo;
};


/**
 * Original solution. From discussion this should pass. But TLE. Need to optimize.
 */
class Solution {
public:
    bool canIWin(int maxChoosableInteger, int desiredTotal) {
        int sum = (1 + maxChoosableInteger) * maxChoosableInteger / 2;
        if (sum < desiredTotal) {
            return false;
        }

        if (desiredTotal <= maxChoosableInteger) {
            return true;
        }

        vector<bool> nums(maxChoosableInteger + 1, false);
        return canWin(nums, desiredTotal);
    }

private:
    bool canWin(vector<bool>& nums, int desiredTotal)
    {
        if (desiredTotal <= 0) {
            return false;
        }

        int key = reduce(nums);
        if (memo.count(key)) {
            return memo[key];
        }

        for (int i = 1; i < nums.size(); ++i) {
            if (nums[i]) {
                continue;
            }

            nums[i] = true;
            if (!canWin(nums, desiredTotal - i)) {
                memo[key] = true;
                nums[i] = false;
                return true;
            }
            nums[i] = false;
        }

        memo[key] = false;

        return false;
    }

private:
    int reduce(vector<bool> const& nums)
    {
        int k = 0;
        for (int i = 1; i < nums.size(); ++i) {
            if (nums[i]) {
                k |= (1 << i);
            }
        }

        return k;
    }

private:
    unordered_map<int, bool> memo;
};
