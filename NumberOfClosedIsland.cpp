/**
 * Number of Closed Islands
 *
 * Given a 2D grid consists of 0s (land) and 1s (water).  An island is a maximal
 * 4-directionally connected group of 0s and a closed island is an island totally (all
 * left, top, right, bottom) surrounded by 1s.
 *
 * Return the number of closed islands.
 *
 * Example 1:
 *
 * Input: grid = [[1,1,1,1,1,1,1,0],
 *                [1,0,0,0,0,1,1,0],
 *                [1,0,1,0,1,1,1,0],
 *                [1,0,0,0,0,1,0,1],
 *                [1,1,1,1,1,1,1,0]]
 * Output: 2
 * Explanation:
 * Islands in gray are closed because they are completely surrounded by water (group of 1s).
 *
 * Example 2:
 *
 * Input: grid = [[0,0,1,0,0],
 *                [0,1,0,1,0],
 *                [0,1,1,1,0]]
 * Output: 1
 *
 * Example 3:
 *
 * Input: grid = [[1,1,1,1,1,1,1],
 *                [1,0,0,0,0,0,1],
 *                [1,0,1,1,1,0,1],
 *                [1,0,1,0,1,0,1],
 *                [1,0,1,1,1,0,1],
 *                [1,0,0,0,0,0,1],
 *                [1,1,1,1,1,1,1]]
 * Output: 2
 *
 *
 *
 * Constraints:
 *
 *     1 <= grid.length, grid[0].length <= 100
 *     0 <= grid[i][j] <=1
 */
/**
 * My original version. Using color method
 */
class Solution {
private:
    enum class LandStatus : char {
        VISITED,
        CONNECT2EDGE
    };

    LandStatus walkAround(vector<vector<int>>& grid, int x, int y) {
        if (x < 0 || x >= m_rows || y < 0 || y >= m_cols || 3 == grid[x][y]) return LandStatus::CONNECT2EDGE;
        if (1 == grid[x][y] || 2 == grid[x][y]) return LandStatus::VISITED;

        grid[x][y] = 2;
        for (int i = 0; i < 4; ++i) {
            int dx = direction[i] + x;
            int dy = direction[i + 1] + y;
            if (LandStatus::CONNECT2EDGE == walkAround(grid, dx, dy)) {
                grid[x][y] = 3;
                return LandStatus::CONNECT2EDGE;
            }
        }

        return LandStatus::VISITED;
    }

public:
    int closedIsland(vector<vector<int>>& grid) {
        m_rows = grid.size();
        m_cols = grid[0].size();
        for (int x = 1; x < m_rows - 1; ++x) {
            for (int y = 1; y < m_cols - 1; ++y) {
                if (0 != grid[x][y]) continue;

                if (LandStatus::CONNECT2EDGE != walkAround(grid, x, y)) {
                    ++m_closedIsland;
                }
            }
        }
        
        return m_closedIsland;
    }
private:
    int m_rows;
    int m_cols;
    int m_closedIsland;
    int direction[5] = {0, -1, 0, 1, 0};
};
