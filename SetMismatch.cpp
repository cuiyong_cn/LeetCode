/**
 * Set Mismatch
 *
 * You have a set of integers s, which originally contains all the numbers from 1 to n.
 * Unfortunately, due to some error, one of the numbers in s got duplicated to another number in the
 * set, which results in repetition of one number and loss of another number.
 *
 * You are given an integer array nums representing the data status of this set after the error.
 *
 * Find the number that occurs twice and the number that is missing and return them in the form of
 * an array.
 *
 * Example 1:
 *
 * Input: nums = [1,2,2,4]
 * Output: [2,3]
 * Example 2:
 *
 * Input: nums = [1,1]
 * Output: [1,2]
 *
 * Constraints:
 *
 * 2 <= nums.length <= 104
 * 1 <= nums[i] <= 104
 */
/**
 * Not easily to understand xor solution
 *
 * Before we dive into the solution to this problem, let's consider a simple problem. Consider an
 * array with n-1n−1 elements containing numbers from 11 to nn with one number missing out of them.
 * Now, how to we find out this missing element. One of the solutions is to take the XOR of all the
 * elements of this array with all the numbers from 11 to nn. By doing so, we get the required
 * missing number. This works because XORing a number with itself results in a 0 result. Thus, only
 * the number which is missing can't get cancelled with this XORing.
 *
 * Now, using this idea as the base, let's take it a step forward and use it for the current
 * problem. By taking the XOR of all the elements of the given numsnums array with all the numbers
 * from 11 to nn, we will get a result, xorxor, as x^yx y . Here, xx and yy refer to the repeated
 * and the missing term in the given numsnums array. This happens on the same grounds as in the
 * first problem discussed above.
 *
 * Now, in the resultant xorxor, we'll get a 1 in the binary representation only at those bit
 * positions which have a 1 in one out of the numbers xx and yy, and a 0 at the same bit position in
 * the other one. In the current solution, we consider the rightmost bit which is 1 in the xorxor,
 * although any bit would work. Let's say, this position is called the rightmostbitrightmostbit.
 *
 * If we divide the elements of the given numsnums array into two parts such that the first set
 * contains the elements which have a 1 at the rightmostbitrightmostbit position and the second set
 * contains the elements having a 0 at the same position, we'll get one out of xx or yy in one set
 * and the other one in the second set. Now, our problem has reduced somewhat to the simple problem
 * discussed above.
 *
 * To solve this reduced problem, we can find out the elements from 11 to nn and consider them as a
 * part of the previous sets only, with the allocation of the set depending on a 1 or 0 at the
 * righmostbitrighmostbit position.
 *
 * Now, if we do the XOR of all the elements of the first set, all the elements will result in an
 * XOR of 0, due to cancellation of the similar terms in both numsnums and the numbers (1:n)(1:n),
 * except one term, which is either xx or yy.
 *
 * For the other term, we can do the XOR of all the elements in the second set as well.
 *
 * Consider the example [1 2 4 4 5 6]
 *
 * XOR
 *
 * One more traversal over the numsnums can be used to identify the missing and the repeated number
 * out of the two numbers found.
 *
 * Complexity Analysis
 *
 * Time complexity : O(n)O(n). We iterate over nn elements five times.
 * Space complexity : O(1)O(1). Constant extra space is used.
 */
public class Solution {
    public int[] findErrorNums(int[] nums) {
        int xor = 0, xor0 = 0, xor1 = 0;
        for (int n: nums)
            xor ^= n;

        for (int i = 1; i <= nums.length; i++)
            xor ^= i;

        int rightmostbit = xor & ~(xor - 1);
        for (int n: nums) {
            if ((n & rightmostbit) != 0)
                xor1 ^= n;
            else
                xor0 ^= n;
        }

        for (int i = 1; i <= nums.length; i++) {
            if ((i & rightmostbit) != 0)
                xor1 ^= i;
            else
                xor0 ^= i;
        }

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == xor0)
                return new int[]{xor0, xor1};
        }

        return new int[]{xor1, xor0};
    }
}

/**
 * And interesting one.
 */
vector<int> findErrorNums(vector<int>& nums)
{
    vector<int> result(2, -1);
    for (int i = 0; i < nums.size(); ++i) {
        while (nums[i] != i + 1) {
            if (nums[i] == nums[nums[i] - 1]) {
                result[0] = nums[i];
                result[1] = i+1;
                break;
            } else {
                swap(nums[i], nums[nums[i]-1]);
            }
        }
    }
    return result;
}
/**
 * Most efficient one.
 */
class Solution {
public:
    vector<int> findErrorNums(vector<int>& nums) {
        int duplicate = 0, missing = 0;
        int expected_sum = nums.size() * (nums.size() + 1) / 2;
        int actual_sum = 0;
        for (auto n : nums) {
            actual_sum += abs(n);
            int idx = abs(n) - 1;
            if (nums[idx] < 0) {
                duplicate = idx + 1;
            }
            else {
                nums[idx] *= -1;
            }
        }

        return {duplicate, duplicate + (expected_sum - actual_sum)};
    }
};

/**
 * More efficient one.
 */
class Solution {
public:
    vector<int> findErrorNums(vector<int>& nums) {
        int duplicate = 0, missing = 0;
        for (auto n : nums) {
            int idx = abs(n) - 1;
            if (nums[idx] < 0) {
                duplicate = idx + 1;
            }
            else {
                nums[idx] *= -1;
            }
        }
        for (int i = 0; i < nums.size(); ++i) {
            if (nums[i] > 0) {
                missing = i + 1;
                break;
            }
        }

        return {duplicate, missing};
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<int> findErrorNums(vector<int>& nums) {
        int next = 1;
        int missing = 0, duplicate = 0;

        sort(begin(nums), end(nums));
        for (auto&& n : nums) {
            if (n == next) {
                ++next;
            }
            else if (n < next) {
                duplicate = n;
            }
            else {
                missing = next;
                next = n + 1;
            }
        }

        if (0 == missing) {
            missing = next;
        }

        return {duplicate, missing};
    }
};
