/**
 * Four sum
 *
 * Given an array nums of n integers, return an array of all the unique quadruplets [nums[a],
 * nums[b], nums[c], nums[d]] such that:
 *
 * 0 <= a, b, c, d < n
 * a, b, c, and d are distinct.
 * nums[a] + nums[b] + nums[c] + nums[d] == target
 * You may return the answer in any order.
 *
 * Example 1:
 *
 * Input: nums = [1,0,-1,0,-2,2], target = 0
 * Output: [[-2,-1,1,2],[-2,0,0,2],[-1,0,0,1]]
 * Example 2:
 *
 * Input: nums = [2,2,2,2,2], target = 8
 * Output: [[2,2,2,2]]
 *
 * Constraints:
 *
 * 1 <= nums.length <= 200
 * -109 <= nums[i] <= 109
 * -109 <= target <= 109
 */
/**
 * Original solution. We take advantage of the previous three sum solution.
 */
class Solution {
public:
    vector<vector<int>> fourSum(vector<int>& nums, int target) {
        sort(begin(nums), end(nums));

        vector<vector<int>> ans;
        for (int i = 0; i < nums.size(); ++i) {
            auto ts = three_sum(nums, i + 1, target - nums[i]);
            for (auto& s : ts) {
                s.push_back(nums[i]);
                ans.emplace_back(std::move(s));
            }

            while ((i + 1) < nums.size() && nums[i] == nums[i + 1]) {
                ++i;
            }
        }

        return ans;
    }
private:
    vector<vector<int>> three_sum(vector<int> const& num, int start, int goal) {
        vector<vector<int>> res;

        for (int i = start; i < num.size(); i++) {
            int target = goal - num[i];
            int front = i + 1;
            int back = num.size() - 1;

            while (front < back) {
                int sum = num[front] + num[back];
                if (sum < target) {
                    ++front;
                }
                else if (sum > target) {
                    --back;
                }
                else {
                    vector<int> triplet = {num[i], num[front], num[back]};
                    res.push_back(triplet);

                    // Processing duplicates of Number 2
                    // Rolling the front pointer to the next different number forwards
                    while (front < back && num[front] == triplet[1]) {
                        front++;
                    }

                    // Processing duplicates of Number 3
                    // Rolling the back pointer to the next different number backwards
                    while (front < back && num[back] == triplet[2]) {
                        back--;
                    }
                }
            }

            // Processing duplicates of Number 1
            while ((i + 1) < num.size() && num[i + 1] == num[i]) {
                i++;
            }
        }

        return res;
    }
};
