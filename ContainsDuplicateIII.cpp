/**
 * Contains Duplicate III
 *
 * Given an integer array nums and two integers k and t, return true if there are two distinct
 * indices i and j in the array such that abs(nums[i] - nums[j]) <= t and abs(i - j) <= k.
 *
 * Example 1:
 *
 * Input: nums = [1,2,3,1], k = 3, t = 0
 * Output: true
 * Example 2:
 *
 * Input: nums = [1,0,1,1], k = 1, t = 2
 * Output: true
 * Example 3:
 *
 * Input: nums = [1,5,9,1,5,9], k = 2, t = 3
 * Output: false
 *
 * Constraints:
 *
 * 0 <= nums.length <= 2 * 104
 * -231 <= nums[i] <= 231 - 1
 * 0 <= k <= 104
 * 0 <= t <= 231 - 1
 */
/**
 * Using buckets
 */
class Solution {
public:
    bool containsNearbyAlmostDuplicate(vector<int>& nums, int k, int t) {
        int n = nums.size();
        if (n == 0 || k < 1  || t < 0) {
            return false;
        }

        unordered_map<int, int> buckets;

        for(int i = 0; i< n; ++i) {
            int bucket = nums[i] / ((long)t + 1);

            // For negative numbers, we need to decrement bucket by 1
            // to ensure floor division.
            // For example, -1/2 = 0 but -1 should be put in Bucket[-1].
            // Therefore, decrement by 1.
            if (nums[i] < 0) {
                --bucket;
            }

            if (buckets.find(bucket) != buckets.end()) {
                return true;
            }
            else {
                buckets[bucket] = nums[i];

                if (buckets.find(bucket - 1) != buckets.end()
                        && (long) nums[i] - buckets[bucket-1] <= t) {
                    return true;
                }

                if (buckets.find(bucket+1) != buckets.end()
                        && (long) buckets[bucket+1] - nums[i] <= t) {
                    return true;
                }

                if (buckets.size() > k) {
                    int key_to_remove = nums[i-k] / ((long)t + 1);

                    if (nums[i-k] < 0) {
                        --key_to_remove;
                    }

                    buckets.erase(key_to_remove);
                }
            }
        }

        return false;
    }
};

/**
 * Passed one.
 */
class Solution {
public:
    bool containsNearbyAlmostDuplicate(vector<int>& nums, int k, int t) {
        vector<pair<long long, int>> vi;
        for (int i = 0; i < nums.size(); ++i) {
            vi.emplace_back(nums[i], i);
        }

        sort(begin(vi), end(vi));

        int i = 0;
        while (i < vi.size()) {
            int j = i + 1;
            while (j < vi.size() && (vi[j].first - vi[i].first) <= t) {
                if (labs(vi[j].second - vi[i].second) <= k) {
                    return true;
                }
                ++j;
            }
            ++i;
        }

        return false;
    }
};

/**
 * Original solution. TLE.
 */
class Solution {
public:
    bool containsNearbyAlmostDuplicate(vector<int>& nums, int k, int t) {
        if (0 == k) {
            return false;
        }

        for (int i = 0; i < nums.size(); ++i) {
            auto right = min<int>(i + k, nums.size() - 1);
            for (int j = i + 1; j <= right; ++j) {
                long long first = nums[i], second = nums[j];
                if (llabs(first - second) <= t) {
                    return true;
                }
            }
        }

        return false;
    }
};
