/**
 * Second Largest Digit in a String
 *
 * Given an alphanumeric string s, return the second largest numerical digit that appears in s, or
 * -1 if it does not exist.
 *
 * An alphanumeric string is a string consisting of lowercase English letters and digits.
 *
 * Example 1:
 *
 * Input: s = "dfa12321afd"
 * Output: 2
 * Explanation: The digits that appear in s are [1, 2, 3]. The second largest digit is 2.
 * Example 2:
 *
 * Input: s = "abc1111"
 * Output: -1
 * Explanation: The digits that appear in s are [1]. There is no second largest digit.
 *
 * Constraints:
 *
 * 1 <= s.length <= 500
 * s consists of only lowercase English letters and/or digits.
 */
/**
 * Original solution.
 */
class Solution {
public:
    int secondHighest(string s) {
        int digits[10] = { -1, };
        for (auto const& c : s) {
            if (isdigit(c)) {
                digits[c - '0'] = 1;
            }
        }

        int count = 0;
        for (int i = 9; i >= 0; --i) {
            if (1 == digits[i]) {
                if (1 == count) {
                    return i;
                }
                ++count;
            }
        }

        return -1;
    }
};
