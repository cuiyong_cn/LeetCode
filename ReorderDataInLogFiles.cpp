/**
 * Reorder Data in Log Files
 *
 * You have an array of logs.  Each log is a space delimited string of words.
 *
 * For each log, the first word in each log is an alphanumeric identifier.  Then, either:
 *
 * Each word after the identifier will consist only of lowercase letters, or;
 * Each word after the identifier will consist only of digits.
 * We will call these two varieties of logs letter-logs and digit-logs.  It is guaranteed that each log has at least
 * one word after its identifier.
 *
 * Reorder the logs so that all of the letter-logs come before any digit-log.  The letter-logs are ordered
 * lexicographically ignoring identifier, with the identifier used in case of ties.  The digit-logs should be put in
 * their original order.
 *
 * Return the final order of the logs.
 *
 * Example 1:
 *
 * Input: logs = ["dig1 8 1 5 1","let1 art can","dig2 3 6","let2 own kit dig","let3 art zero"]
 * Output: ["let1 art can","let3 art zero","let2 own kit dig","dig1 8 1 5 1","dig2 3 6"]
 *
 * Constraints:
 *
 * 0 <= logs.length <= 100
 * 3 <= logs[i].length <= 100
 * logs[i] is guaranteed to have an identifier, and a word after the identifier.
 */
/**
 * Using built-in stable sort. Avoid using extra data structure.
 */
class Solution {
public:
    static bool compare(const string& left, const string& right) {
        int pos1 = left.find(' ', 0);
        int pos2 = right.find(' ', 0);
        bool isDigit1 = isdigit(left[pos1 + 1]);
        bool isDigit2 = isdigit(right[pos2 + 1]);

        int ret = 1;
        if (false == isDigit1) {
            if (false == isDigit2) {
                ret = left.compare(pos1, string::npos,
                                right, pos2, string::npos);
                if (0 == ret) {
                    ret = left.compare(0, pos1, right, 0, pos2);
                }
            }
            else {
                ret = -1;
            }
        }

        return ret < 0;
    }

    vector<string> reorderLogFiles(vector<string>& logs) {
        stable_sort(logs.begin(), logs.end(), compare);

        return logs;
    }
};

/**
 * Original solution. Divide the log string into two different categories. Sort the letter logs.
 */
class Solution {
public:
    struct LogEntry {
        string identifier;
        string rest;
    };

    static bool compare(LogEntry& left, LogEntry& right) {
        int ret = left.rest.compare(right.rest);
        if (ret != 0) {
            return ret < 0 ? true : false;
        }

        return left.identifier < right.identifier;
    }

    vector<string> reorderLogFiles(vector<string>& logs) {
        vector<LogEntry> letterLogs;
        vector<LogEntry> digitLogs;
        vector<string> ans;
        for (auto& s : logs) {
            LogEntry le;
            int pos = s.find(' ', 0);
            le.identifier = s.substr(0, pos);
            le.rest = s.substr(pos);
            if (isalpha(le.rest[1])) {
                letterLogs.push_back(le);
            }
            else {
                digitLogs.push_back(le);
            }
        }

        sort(letterLogs.begin(), letterLogs.end(), compare);
        for (auto& l : letterLogs) {
            ans.push_back(l.identifier + l.rest);
        }
        for (auto& d : digitLogs) {
            ans.push_back(d.identifier + d.rest);
        }

        return ans;
    }
};
