/**
 * Find Eventual Safe States
 *
 * In a directed graph, we start at some node and every turn, walk along a directed edge of the graph.  If we reach a
 * node that is terminal (that is, it has no outgoing directed edges), we stop.
 *
 * Now, say our starting node is eventually safe if and only if we must eventually walk to a terminal node.  More
 * specifically, there exists a natural number K so that for any choice of where to walk, we must have stopped at a
 * terminal node in less than K steps.
 *
 * Which nodes are eventually safe?  Return them as an array in sorted order.
 *
 * The directed graph has N nodes with labels 0, 1, ..., N-1, where N is the length of graph.  The graph is given in the
 * following form: graph[i] is a list of labels j such that (i, j) is a directed edge of the graph.
 *
 * Example:
 * Input: graph = [[1,2],[2,3],[5],[0],[5],[],[]]
 * Output: [2,4,5,6]
 * Here is a diagram of the above graph.
 *
 * Illustration of graph
 *
 * Note:
 *     graph will have length at most 10000.
 *     The number of edges in the graph will not exceed 32000.
 *     Each graph[i] will be a sorted list of different integers, chosen within the range [0, graph.length - 1].
 */
/**
 * Also from the leetcode, we can solve this problem backward. Start from the safe nodes, and then back track all
 * nodes that only leads to safe nodes.
 */
class Solution {
    public List<Integer> eventualSafeNodes(int[][] G) {
        int N = G.length;
        boolean[] safe = new boolean[N];

        List<Set<Integer>> graph = new ArrayList();
        List<Set<Integer>> rgraph = new ArrayList();
        for (int i = 0; i < N; ++i) {
            graph.add(new HashSet());
            rgraph.add(new HashSet());
        }

        Queue<Integer> queue = new LinkedList();

        for (int i = 0; i < N; ++i) {
            if (G[i].length == 0)
                queue.offer(i);
            for (int j: G[i]) {
                graph.get(i).add(j);
                rgraph.get(j).add(i);
            }
        }

        while (!queue.isEmpty()) {
            int j = queue.poll();
            safe[j] = true;
            for (int i: rgraph.get(j)) {
                graph.get(i).remove(j);
                if (graph.get(i).isEmpty())
                    queue.offer(i);
            }
        }

        List<Integer> ans = new ArrayList();
        for (int i = 0; i < N; ++i) if (safe[i])
            ans.add(i);

        return ans;
    }
}

/**
 * Original solution. And by checking the solution from the leetcode, tracks maybe named color be more proper.
 * We color not visited 0, visited 1, safe 2.
 */
class Solution {
public:
    using Graph = vector<vector<int>>;

    vector<int> eventualSafeNodes(vector<vector<int>>& graph) {
        vector<int> tracks(graph.size(), 0);
        for (int i = 0; i < graph.size(); ++i) {
            dfs(graph, tracks, i);
        }

        vector<int> ans;
        for (size_t i = 0; i < tracks.size(); ++i) {
            if (tracks[i] < 0) {
                ans.push_back(i);
            }
        }

        return ans;
    }

private:
    bool dfs(const Graph& g, vector<int>& tracks, int node) {
        if (tracks[node] < 0) return true;

        if (tracks[node]) {
            return false;
        }

        tracks[node] = 1;

        bool safe = true;
        if (!g[node].empty()) {
            for (auto n : g[node]) {
                if (!dfs(g, tracks, n)) {
                    safe = false;
                    break;
                }
            }
        }

        if (safe) {
            tracks[node] = -1;
        }
        else {
            tracks[node] = 1;
        }

        return safe;
    }
};
