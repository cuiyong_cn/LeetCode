/**
 * Harshad Number
 *
 * An integer divisible by the sum of its digits is said to be a Harshad number. You are given an
 * integer x. Return the sum of the digits of x if x is a Harshad number, otherwise, return -1.
 *
 * Example 1:
 *
 * Input: x = 18
 *
 * Output: 9
 *
 * Explanation:
 *
 * The sum of digits of x is 9. 18 is divisible by 9. So 18 is a Harshad number and the answer is 9.
 *
 * Example 2:
 *
 * Input: x = 23
 *
 * Output: -1
 *
 * Explanation:
 *
 * The sum of digits of x is 5. 23 is not divisible by 5. So 23 is not a Harshad number and the answer is -1.
 *
 * Constraints:
 *     1 <= x <= 100
 */
/**
 * Original solution.
 */
auto sum_of_digits(int x)
{
    auto sum = 0;

    while (x > 9) {
        sum += x % 10;
        x = x / 10;
    }

    return sum + x;
}

class Solution {
public:
    int sumOfTheDigitsOfHarshadNumber(int x) {
        auto sum = sum_of_digits(x);

        return 0 == (x % sum) ? sum : -1;
    }
};
