/**
 * Minimum Time to Visit Disappearing Nodes
 *
 * There is an undirected graph of n nodes. You are given a 2D array edges, where edges[i] = [ui,
 * vi, lengthi] describes an edge between node ui and node vi with a traversal time of lengthi
 * units.
 *
 * Additionally, you are given an array disappear, where disappear[i] denotes the time when the node
 * i disappears from the graph and you won't be able to visit it.
 *
 * Notice that the graph might be disconnected and might contain multiple edges.
 *
 * Return the array answer, with answer[i] denoting the minimum units of time required to reach node
 * i from node 0. If node i is unreachable from node 0 then answer[i] is -1.
 *
 * Example 1:
 *
 * Input: n = 3, edges = [[0,1,2],[1,2,1],[0,2,4]], disappear = [1,1,5]
 *
 * Output: [0,-1,4]
 *
 * Explanation:
 *
 * We are starting our journey from node 0, and our goal is to find the minimum time required to reach each node before it disappears.
 *     For node 0, we don't need any time as it is our starting point.
 *     For node 1, we need at least 2 units of time to traverse edges[0]. Unfortunately, it disappears at that moment, so we won't be able to visit it.
 *     For node 2, we need at least 4 units of time to traverse edges[2].
 *
 * Example 2:
 *
 * Input: n = 3, edges = [[0,1,2],[1,2,1],[0,2,4]], disappear = [1,3,5]
 *
 * Output: [0,2,3]
 *
 * Explanation:
 *
 * We are starting our journey from node 0, and our goal is to find the minimum time required to
 * reach each node before it disappears.
 *     For node 0, we don't need any time as it is the starting point.
 *     For node 1, we need at least 2 units of time to traverse edges[0].
 *     For node 2, we need at least 3 units of time to traverse edges[0] and edges[1].
 *
 * Example 3:
 *
 * Input: n = 2, edges = [[0,1,1]], disappear = [1,1]
 *
 * Output: [0,-1]
 *
 * Explanation:
 *
 * Exactly when we reach node 1, it disappears.
 *
 * Constraints:
 *     1 <= n <= 5 * 104
 *     0 <= edges.length <= 105
 *     edges[i] == [ui, vi, lengthi]
 *     0 <= ui, vi <= n - 1
 *     1 <= lengthi <= 105
 *     disappear.length == n
 *     1 <= disappear[i] <= 105
 */
/**
 * Using priority_queue to speed up.
 */
class Solution {
    template<typename T>
    using MinHeap = priority_queue<T, vector<T>, greater<T>>;
public:
    vector<int> minimumTime(int n, vector<vector<int>>& edges, vector<int>& disappear) {
        auto G = unordered_map<int, vector<pair<int, int>>>{};
        for (auto const& e : edges) {
            G[e[0]].emplace_back(e[1], e[2]);
            G[e[1]].emplace_back(e[0], e[2]);
        }

        auto const int_max = numeric_limits<int>::max();
        auto ans = vector<int>(n, int_max);
        auto visit = MinHeap<pair<int, int>>{};

        visit.push({0, 0});
        ans[0] = 0;

        while (!visit.empty()) {
            auto [dist, node] = visit.top(); visit.pop();

            if (dist > ans[node]) {
                continue;
            }

            for (auto [next, D] : G[node]) {
                auto total = dist + D;
                if (total < disappear[next] && total < ans[next]) {
                    visit.push({total, next});
                    ans[next] = total;
                }
            }
        }

        for (auto i = 0; i < n; ++i) {
            if (ans[i] >= disappear[i]) {
                ans[i] = -1;
            }
        }

        return ans;
    }
};

/**
 * Original solution. Should work but TLE.
 */
class Solution {
public:
    vector<int> minimumTime(int n, vector<vector<int>>& edges, vector<int>& disappear) {
        auto G = unordered_map<int, vector<pair<int, int>>>{};
        for (auto const& e : edges) {
            G[e[0]].emplace_back(e[1], e[2]);
            G[e[1]].emplace_back(e[0], e[2]);
        }

        auto const int_max = numeric_limits<int>::max();
        auto ans = vector<int>(n, int_max);
        auto visit_queue = deque<int>{0};

        while (!visit_queue.empty()) {
            int const qsize = visit_queue.size();

            for (auto i = 0; i < qsize; ++i) {
                auto node = visit_queue.front(); visit_queue.pop_front();

                if (int_max == ans[node]) {
                    ans[node] = 0;
                }

                if (ans[node] < disappear[node]) {
                    for (auto [next, cost] : G[node]) {
                        if (ans[node] < disappear[node]) {
                            auto total = ans[node] + cost;
                            if (total < ans[next]) {
                                visit_queue.push_back(next);
                                ans[next] = total;
                            }
                        }
                    }
                }
            }
        }

        for (auto i = 0; i < n; ++i) {
            if (ans[i] >= disappear[i]) {
                ans[i] = -1;
            }
        }

        return ans;
    }
};
