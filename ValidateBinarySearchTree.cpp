/**
 * Validate Binary Search Tree
 *
 * Given the root of a binary tree, determine if it is a valid binary search tree (BST).
 *
 * A valid BST is defined as follows:
 *
 * The left subtree of a node contains only nodes with keys less than the node's key.
 * The right subtree of a node contains only nodes with keys greater than the node's key.
 * Both the left and right subtrees must also be binary search trees.
 *
 * Example 1:
 *
 * Input: root = [2,1,3]
 * Output: true
 * Example 2:
 *
 * Input: root = [5,1,4,null,null,3,6]
 * Output: false
 * Explanation: The root node's value is 5 but its right child's value is 4.
 *
 * Constraints:
 *
 * The number of nodes in the tree is in the range [1, 104].
 * -231 <= Node.val <= 231 - 1
 */
/**
 * Iterative version.
 */
class Solution {
public:
    bool isValidBST(TreeNode* root) {
        if (nullptr == root) {
            return true;
        }

        vector<TreeNode*> stk;
        TreeNode* pre = nullptr;
        auto cur = root;
        while (cur || !stk.empty()) {
            while (cur) {
                stk.push_back(cur);
                cur = cur->left;
            }

            cur = stk.back(); stk.pop_back();

            if (pre && cur->val <= pre->val) {
                return false;
            }

            pre = cur;
            cur = cur->right;
        }

        return true;
    }
};

/**
 * Without using the extra the vector<int>
 */
class Solution {
public:
    bool isValidBST(TreeNode* root) {
        return is_valid_bst(root, nullptr, nullptr);
    }

    bool is_valid_bst(TreeNode* root, TreeNode* minNode, TreeNode* maxNode) {
        if (nullptr == root) {
            return true;
        }

        if ((minNode && root->val <= minNode->val)
                || (maxNode && root->val >= maxNode->val)) {
            return false;
        }

        return isValidBST(root->left, minNode, root) && isValidBST(root->right, root, maxNode);
    }
};

/**
 * Original solution. BST has below property.
 * in order traversal will be in sorted state.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    bool isValidBST(TreeNode* root) {
        vector<int> values;
        in_order_travel(root, values);

        for (size_t i = 1; i < values.size(); ++i) {
            if (values[i] <= values[i - 1]) {
                return false;
            }
        }

        return true;
    }

private:
    void in_order_travel(TreeNode* root, vector<int>& values)
    {
        if (nullptr == root) {
            return;
        }

        if (root->left) {
            in_order_travel(root->left, values);
        }

        values.push_back(root->val);
        
        if (root->right) {
            in_order_travel(root->right, values);
        }
    }
};
