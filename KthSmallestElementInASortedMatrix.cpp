/** Kth Smallest Element in a Sorted Matrix
 *
 * Given a n x n matrix where each of the rows and columns are sorted in ascending order, find the kth smallest element
 * in the matrix.
 *
 * Note that it is the kth smallest element in the sorted order, not the kth distinct element.
 *
 * Example:
 *
 * matrix = [ [ 1,  5,  9], [10, 11, 13], [12, 13, 15] ], k = 8,
 *
 * return 13.
 *
 * Note: You may assume k is always valid, 1 ≤ k ≤ n^2.
 */
/**
 * Passed solution. Read from the discussions.
 */
class Solution
{
public:
    struct numCompare {
        bool operator()(const pair<int, pair<int, int>> &x, const pair<int, pair<int, int>> &y)
        {
            return x.first > y.first;
        }
    };

    int kthSmallest(vector<vector<int>> &matrix, int k)
    {
        int n = matrix.size();
        priority_queue<pair<int, pair<int, int>>, vector<pair<int, pair<int, int>>>, numCompare> minHeap;

        // put the 1st element of each row in the min heap
        // we don't need to push more than 'k' elements in the heap
        for (int i = 0; i < n && i < k; i++) {
            minHeap.push(make_pair(matrix[i][0], make_pair(i, 0)));
        }

        // take the smallest element form the min heap, if the running count is equal to k return the number
        // if the row of the top element has more elements, add the next element to the heap
        int numberCount = 0, result = 0;
        while (!minHeap.empty()) {
            auto heapTop = minHeap.top();
            minHeap.pop();
            result = heapTop.first;
            if (++numberCount == k) {
                break;
            }

            heapTop.second.second++;
            if (n > heapTop.second.second) {
                heapTop.first = matrix[heapTop.second.first][heapTop.second.second];
                minHeap.push(heapTop);
            }
        }
        return result;
    }
};

/**
 * Original solution. TLE
 */
class Solution
{
public:
    struct Point {
        int x;
        int y;
    };

    vector<bool> visited;
    int kthSmallest(vector<vector<int>>& matrix, int k)
    {
        map<int, vector<Point>> memo;
        int N = matrix.size();
        for (int r = 0; r < N; ++r) {
            int val = matrix[r][0];
            map<int, vector<Point>>::iterator viter = memo.find(val);
            if (viter != memo.end()) {
                viter->second.push_back({r, 0});
            } else {
                memo.insert({val, {{r, 0}}});
            }
        }

        visited = vector<bool>(N, false);
        for (int d = 0; d < k; ++d) {
            auto it = memo.begin();
            Point pos = MyAdvance(it, d);
            int r = pos.x;
            if (visited[r]) continue;
            visited[r] = true;
            for (int c = pos.y + 1; c < N; ++c) {
                int val = matrix[r][c];
                if (memo.size() < k) {
                    map<int, vector<Point>>::iterator viter = memo.find(val);
                    if (viter != memo.end()) {
                        viter->second.push_back({r, c});
                    } else {
                        memo.insert({val, {{r, c}}});
                    }
                } else {
                    int maxVal = memo.rbegin()->first;
                    if (val >= maxVal) break;
                    map<int, vector<Point>>::iterator viter = memo.find(val);
                    if (viter != memo.end()) {
                        viter->second.push_back({r, c});
                    } else {
                        memo.insert({val, {{r, c}}});
                    }
                }
            }
        }

        auto it = memo.begin();
        MyAdvance(it, k - 1);

        return it->first;
    }

    Point MyAdvance(map<int, vector<Point>>::iterator& it, int d)
    {
        int size = 0;
        while (d > 0) {
            size = it->second.size();
            if (size > d) break;
            d -= size;
            ++it;
        }

        for (auto& p : it->second) {
            if (false == visited[p.x]) return p;
        }

        return {0, 0};
    }
};

