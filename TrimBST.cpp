/**
 * Trim a Binary Search Tree
 *
 *  Given a binary search tree and the lowest and highest boundaries as L and R, trim the
 *  tree so that all its elements lies in [L, R] (R >= L). You might need to change the
 *  root of the tree, so the result should return the new root of the trimmed binary
 *  search tree.
 *
 * Example 1:
 *
 * Input:
 *     1
 *    / \
 *   0   2
 *
 *   L = 1
 *   R = 2
 *
 * Output:
 *     1
 *       \
 *        2
 *
 * Example 2:
 *
 * Input:
 *     3
 *    / \
 *   0   4
 *    \
 *     2
 *    /
 *   1
 *
 *   L = 1
 *   R = 3
 *
 * Output:
 *       3
 *      /
 *    2
 *   /
 *  1
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/**
 * Original version
 */
class Solution
{
private:
    void destroyTree(TreeNode* root)
    {
        if (root == nullptr) return;

        destroyTree(root->left);
        destroyTree(root->right);
        delete root;
    }
public:
    TreeNode* trimBST(TreeNode* root, int L, int R)
    {
        if (root == nullptr) return nullptr;
        TreeNode* ans = nullptr;

        /* Free the memory, leetcode complains "heap use after free", errr.... weird */
        if (root->val < L) {
            //destroyTree(root->left);
            ans = trimBST(root->right, L, R);
            //delete root;
        } else if (root->val > R) {
            //destroyTree(root->right);
            ans = trimBST(root->left, L, R);
            //delete root;
        } else {
            root->left = trimBST(root->left, L, R);
            root->right = trimBST(root->right, L, R);
            ans = root;
        }

        return ans;
    }
};

/**
 * A more compact version. And it seems if we don't delete the root node, leetcode
 * stop complaining.
 */
class Solution
{
    TreeNode* trimBST(TreeNode* root, int L, int R, bool top = true)
    {
        if (nullptr == root) return nullptr;

        root->left = trimBST(root->left, L, R, false);
        root->right = trimBST(root->right, L, R, false);

        if (root->val >= L && root->val <= R) return root;
        TreeNode* result = root->val < L ? root->right : root->left;

        if (false == top) delete root;

        return result;
    }
};

