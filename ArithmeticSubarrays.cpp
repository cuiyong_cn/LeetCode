/**
 * Arithmetic Subarrays
 *
 * A sequence of numbers is called arithmetic if it consists of at least two elements, and the
 * difference between every two consecutive elements is the same. More formally, a sequence s is
 * arithmetic if and only if s[i+1] - s[i] == s[1] - s[0] for all valid i.
 *
 * For example, these are arithmetic sequences:
 *
 * 1, 3, 5, 7, 9
 * 7, 7, 7, 7
 * 3, -1, -5, -9
 * The following sequence is not arithmetic:
 *
 * 1, 1, 2, 5, 7
 * You are given an array of n integers, nums, and two arrays of m integers each, l and r,
 * representing the m range queries, where the ith query is the range [l[i], r[i]]. All the arrays
 * are 0-indexed.
 *
 * Return a list of boolean elements answer, where answer[i] is true if the subarray nums[l[i]],
 * nums[l[i]+1], ... , nums[r[i]] can be rearranged to form an arithmetic sequence, and false
 * otherwise.
 *
 * Example 1:
 *
 * Input: nums = [4,6,5,9,3,7], l = [0,0,2], r = [2,3,5]
 * Output: [true,false,true]
 * Explanation:
 * In the 0th query, the subarray is [4,6,5]. This can be rearranged as [6,5,4], which is an arithmetic sequence.
 * In the 1st query, the subarray is [4,6,5,9]. This cannot be rearranged as an arithmetic sequence.
 * In the 2nd query, the subarray is [5,9,3,7]. This can be rearranged as [3,5,7,9], which is an arithmetic sequence.
 * Example 2:
 *
 * Input: nums = [-12,-9,-3,-12,-6,15,20,-25,-20,-15,-10], l = [0,1,6,4,8,7], r = [4,4,9,7,9,10]
 * Output: [false,true,false,false,true,true]
 *
 * Constraints:
 *
 * n == nums.length
 * m == l.length
 * m == r.length
 * 2 <= n <= 500
 * 1 <= m <= 500
 * 0 <= l[i] < r[i] < n
 * -105 <= nums[i] <= 105
 */
/**
 * Without sort.
 */
class Solution {
public:
    vector<bool> checkArithmeticSubarrays(vector<int>& n, vector<int>& l, vector<int>& r) {
        int const M = l.size();
        vector<bool> ans(M, false);

        auto is_arithmetic = [&n](int max_e, int min_e, int left, int right) {
            int const delta = max_e - min_e;
            if (0 == delta) {
                return true;
            }

            int segs = right -left;
            if (delta % segs) {
                return false;
            }

            vector<bool> diffs(segs + 1, false);
            int const step = delta / segs;
            for (int i = left; i <= right; ++i) {
                if ((n[i] - min_e) % step || diffs[(n[i] - min_e) / step]) {
                    return false;
                }
                diffs[(n[i] - min_e) / step] = true;
            }
            return true;
        };

        for (auto i = 0; i < M; ++i) {
            int const L = l[i];
            int const R = r[i];
            auto mmp = minmax_element(n.begin() + L, n.begin() + R + 1);

            ans[i] = is_arithmetic(*mmp.second, *mmp.first, L, R);
        }
        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<bool> checkArithmeticSubarrays(vector<int>& nums, vector<int>& l, vector<int>& r) {
        int const N = nums.size();
        int const M = l.size();
        vector<int> idx(N, 0);
        vector<bool> ans(M, false);
        for (int i = 0; i < M; ++i) {
            int const L = l[i];
            int const R = r[i];
            iota(idx.begin() + L, idx.begin() + R + 1, L);
            sort(idx.begin() + L, idx.begin() + R + 1,
                 [&nums](auto p, auto q) {
                     return nums[p] < nums[q];
                 });

            bool arithmetic = true;
            int const delta = nums[idx[L + 1]] - nums[idx[L]];
            for (int j = L + 2; j <= R; ++j) {
                if (delta != (nums[idx[j]] - nums[idx[j - 1]])) {
                    arithmetic = false;
                    break;
                }
            }

            ans[i] = arithmetic;
        }

        return ans;
    }
};
