/**
 * Unique Length-3 Palindromic Subsequences
 *
 * Given a string s, return the number of unique palindromes of length three that are a subsequence of s.
 *
 * Note that even if there are multiple ways to obtain the same subsequence, it is still only counted once.
 *
 * A palindrome is a string that reads the same forwards and backwards.
 *
 * A subsequence of a string is a new string generated from the original string with some characters
 * (can be none) deleted without changing the relative order of the remaining characters.
 *
 * For example, "ace" is a subsequence of "abcde".
 *
 * Example 1:
 *
 * Input: s = "aabca"
 * Output: 3
 * Explanation: The 3 palindromic subsequences of length 3 are:
 * - "aba" (subsequence of "aabca")
 * - "aaa" (subsequence of "aabca")
 * - "aca" (subsequence of "aabca")
 * Example 2:
 *
 * Input: s = "adc"
 * Output: 0
 * Explanation: There are no palindromic subsequences of length 3 in "adc".
 * Example 3:
 *
 * Input: s = "bbcbaba"
 * Output: 4
 * Explanation: The 4 palindromic subsequences of length 3 are:
 * - "bbb" (subsequence of "bbcbaba")
 * - "bcb" (subsequence of "bbcbaba")
 * - "bab" (subsequence of "bbcbaba")
 * - "aba" (subsequence of "bbcbaba")
 *
 * Constraints:
 *
 * 3 <= s.length <= 105
 * s consists of only lowercase English letters.
 */
/**
 * More memory efficient.
 */
class Solution {
public:
    int countPalindromicSubsequence(string const& s) {
        int const size = s.length();
        vector<pair<int, int>> idx(26, {-1, -1});
        for (int i = 0; i < size; ++i) {
            int const cidx = s[i] - 'a';
            if (idx[cidx].first < 0) {
                idx[cidx].first = i;
            }
            idx[cidx].second = i;
        }

        int ans = 0;
        for (int i = 0; i < 26; ++i) {
            auto const [left, right] = idx[i];
            if ((right - left) < 2) {
                continue;
            }

            ans += unordered_set<char>{s.begin() + left + 1, s.begin() + right}.size();
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int countPalindromicSubsequence(string s) {
        int const size = s.length();
        vector<vector<int>> prefix_char_cnt(size + 1, vector<int>(26, 0));

        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < 26; ++j) {
                prefix_char_cnt[i + 1][j] = prefix_char_cnt[i][j] + (('a' + j) == s[i] ? 1 : 0);
            }
        }

        vector<pair<int, int>> idx(26, {-1, -1});
        for (int i = 0; i < size; ++i) {
            int const cidx = s[i] - 'a';
            if (idx[cidx].first < 0) {
                idx[cidx].first = i;
            }
            idx[cidx].second = i;
        }

        int ans = 0;
        for (int i = 0; i < 26; ++i) {
            auto const [left, right] = idx[i];
            if ((right - left) < 2) {
                continue;
            }

            for (int k = 0; k < 26; ++k) {
                ans += (prefix_char_cnt[right][k] - prefix_char_cnt[left + 1][k]) > 0 ? 1 : 0;
            }
        }

        return ans;
    }
};
