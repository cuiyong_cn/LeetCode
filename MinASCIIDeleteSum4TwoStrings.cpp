/**
 * Minimum ASCII Delete Sum for Two Strings
 *
 * Given two strings s1, s2, find the lowest ASCII sum of deleted characters to make two strings equal.
 *
 * Example 1:
 *
 * Input: s1 = "sea", s2 = "eat"
 * Output: 231
 * Explanation: Deleting "s" from "sea" adds the ASCII value of "s" (115) to the sum.
 * Deleting "t" from "eat" adds 116 to the sum.
 * At the end, both strings are equal, and 115 + 116 = 231 is the minimum sum possible to achieve this.
 *
 * Example 2:
 *
 * Input: s1 = "delete", s2 = "leet"
 * Output: 403
 * Explanation: Deleting "dee" from "delete" to turn the string into "let",
 * adds 100[d]+101[e]+101[e] to the sum.  Deleting "e" from "leet" adds 101[e] to the sum.
 * At the end, both strings are equal to "let", and the answer is 100+101+101+101 = 403.
 * If instead we turned both strings into "lee" or "eet", we would get answers of 433 or 417, which are higher.
 *
 * Note:
 * 0 < s1.length, s2.length <= 1000.
 * All elements of each string will have an ASCII value in [97, 122].
 */
/**
 * Iterative version. Eliminate the recursive function call.
 */
class Solution {
public:
    int minimumDeleteSum(string s1, string s2) {
        len1 = s1.size();
        len2 = s2.size();
        vector<vector<int> > dp(len1 + 1, vector<int>(len2 + 1, 0));

        for (int i = len1 - 1; i >= 0; --i) {
            dp[i][len2] = dp[i + 1][len2] + s1[i];
        }
        for (int i = len2 - 1; i >= 0; --i) {
            dp[len1][i] = dp[len1][i + 1] + s2[i];
        }

        for (int i = len1 - 1; i >= 0; --i) {
            for (int j = len2 - 1; j >= 0; --j) {
                if (s1[i] == s2[j]) {
                    dp[i][j] = dp[i + 1][j + 1];
                }
                else {
                    dp[i][j] = min(dp[i + 1][j] + s1[i], dp[i][j + 1] + s2[j]);
                }
            }
        }

        return dp[0][0];
    }

    int len1;
    int len2;
};

/**
 * My original solution. Dynamic programming. But seems not very efficient.
 */
class Solution {
public:
    int minimumDeleteSum(string s1, string s2) {
        len1 = s1.size();
        len2 = s2.size();
        vector<vector<int> > dp(len1, vector<int>(len2, 0));

        return dfs(s1, s2, 0, 0, dp);
    }
private:
    int dfs(string& s1, string& s2, int i, int j, vector<vector<int> >& dp)
    {
        int m = 0;
        if (i < len1) {
            if (j < len2) {
                if (dp[i][j] > 0) return dp[i][j];
                int m1 = dfs(s1, s2, i + 1, j, dp) + s1[i];
                int m2 = dfs(s1, s2, i, j + 1, dp) + s2[j];
                m = min(m1, m2);
                if (s1[i] == s2[j]) {
                    m = min(m, dfs(s1, s2, i + 1, j + 1, dp));
                }

                dp[i][j] = m;
            }
            else {
                for (int k = i; k < len1; ++k) {
                    m += s1[k];
                }
            }
        }
        else {
            for (int k = j; k < len2; ++k) {
                m += s2[k];
            }
        }
        return m;
    }

    int len1;
    int len2;
};
