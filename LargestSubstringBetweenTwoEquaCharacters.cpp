/**
 * Largest Substring Between Two Equal Characters
 *
 * Given a string s, return the length of the longest substring between two equal characters,
 * excluding the two characters. If there is no such substring return -1.
 *
 * A substring is a contiguous sequence of characters within a string.
 *
 * Example 1:
 *
 * Input: s = "aa"
 * Output: 0
 * Explanation: The optimal substring here is an empty substring between the two 'a's.
 * Example 2:
 *
 * Input: s = "abca"
 * Output: 2
 * Explanation: The optimal substring here is "bc".
 * Example 3:
 *
 * Input: s = "cbzxy"
 * Output: -1
 * Explanation: There are no characters that appear twice in s.
 *
 * Constraints:
 *
 * 1 <= s.length <= 300
 * s contains only lowercase English letters.
 */
/**
 * Original solution.
 */
class Solution {
public:
    int maxLengthBetweenEqualCharacters(string s) {
        vector<int> ch_pos(26, -1);
        int const N = s.length();
        int ans = -1;
        for (int i = 0; i < N; ++i) {
            int idx = s[i] - 'a';
            if (ch_pos[idx] < 0) {
                ch_pos[idx] = i;
            }
            else {
                ans = max(ans, i - ch_pos[idx] - 1);
            }
        }

        return ans;
    }
};
