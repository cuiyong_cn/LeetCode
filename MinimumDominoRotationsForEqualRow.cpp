/**
 * Minimum Domino Rotations For Equal Row
 *
 * In a row of dominoes, A[i] and B[i] represent the top and bottom halves of the i-th domino.  (A domino is a tile with
 * two numbers from 1 to 6 - one on each half of the tile.)
 *
 * We may rotate the i-th domino, so that A[i] and B[i] swap values.
 *
 * Return the minimum number of rotations so that all the values in A are the same, or all the values in B are the same.
 *
 * If it cannot be done, return -1.
 *
 * Example 1:
 *
 * Input: A = [2,1,2,4,2,2], B = [5,2,6,2,3,2]
 * Output: 2
 * Explanation:
 * The first figure represents the dominoes as given by A and B: before we do any rotations.  If we rotate the second
 * and fourth dominoes, we can make every value in the top row equal to 2, as indicated by the second figure.
 *
 * Example 2:
 *
 * Input: A = [3,5,1,2,3], B = [3,6,3,3,4]
 * Output: -1
 * Explanation:
 * In this case, it is not possible to rotate the dominoes to make one row of values equal.
 *
 * Note:
 *     1 <= A[i], B[i] <= 6
 *     2 <= A.length == B.length <= 20000
 */
/**
 * From user lee215. Better approach.
 *
 * One observation is that, if A[0] works, no need to check B[0].
 * Because if both A[0] and B[0] exist in all dominoes,
 * when you swap A[0] in a whole row,
 * you will swap B[0] in a whole at the same time.
 * The result of trying A[0] and B[0] will be the same.
 */
class Solution
{
public:
    int minDominoRotations(vector<int>& A, vector<int>& B) {
        vector<int> countA(7), countB(7), same(7);
        int n = A.size();
        for (int i = 0; i < n; ++i) {
            countA[A[i]]++;
            countB[B[i]]++;
            if (A[i] == B[i])
                same[A[i]]++;
        }
        for (int i  = 1; i < 7; ++i)
            if (countA[i] + countB[i] - same[i] == n)
                return n - max(countA[i], countB[i]);
        return -1;
    }
};

/**
 * Same idea, but less code.
 */
class Solution {
public:
    int minDominoRotations(vector<int>& A, vector<int>& B) {
        int equal2a = A[0],
            equal2b = B[0];

        int eqa0A = 0; int eqa0B = 0;
        int eqb0A = 0; int eqb0B = 0;
        int eqa0AB = 0; int eqb0AB = 0;

        for (size_t i = 0; i < A.size(); ++i) {
            if (A[i] == equal2a) ++eqa0A;
            if (A[i] == equal2b) ++eqb0A;
            if (B[i] == equal2a) ++eqa0B;
            if (B[i] == equal2b) ++eqb0B;
            if (A[i] == equal2a && A[i] == B[i]) ++eqa0AB;
            if (A[i] == equal2b && A[i] == B[i]) ++eqb0AB;
        }

        int r1 = ((eqa0A + eqa0B - eqa0AB) == A.size()) ?
                        std::min(eqa0A, eqa0B) - eqa0AB : -1;
        int r2 = ((eqb0A + eqb0B - eqb0AB) == A.size()) ?
                        std::min(eqb0A, eqb0B) - eqb0AB : -1;
        if (r1 >= 0 && r2 >= 0) {
            return std::min(r1, r2);
        }
        else if (r1 < 0 && r2 < 0) {
            return -1;
        }

        return std::max(r1, r2);
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int minDominoRotations(vector<int>& A, vector<int>& B) {
        int equal2a = A[0],
            equal2b = B[0];

        int rotateA = minRotation(A, B, equal2a);
        int rotateB = minRotation(A, B, equal2b);

        int ans = -1;
        if (rotateA >= 0 && rotateB >= 0) {
            ans = std::min(rotateA, rotateB);
        }
        else if (rotateA <0 && rotateB < 0) {
            ans = -1;
        }
        else {
            ans = std::max(rotateA, rotateB);
        }

        return ans;
    }

private:
    int minRotation(const vector<int>& A, const vector<int>& B, const int eq_num)
    {
        int rotate = 0;
        int same = 0;
        for (size_t i = 0; i < A.size(); ++i) {
            if (A[i] != eq_num) {
                if (B[i] == eq_num) {
                    ++rotate;
                }
                else {
                    rotate = -1;
                    same = 0;
                    break;
                }
            }
            else {
                if (B[i] == eq_num) {
                    ++same;
                }
            }
        }

        return std::min(rotate, int(A.size() - rotate - same));
    }
};
