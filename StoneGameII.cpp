/**
 * Stone Game II
 *
 * Alex and Lee continue their games with piles of stones.  There are a number of piles
 * arranged in a row, and each pile has a positive integer number of stones piles[i].
 * The objective of the game is to end with the most stones.
 *
 * Alex and Lee take turns, with Alex starting first.  Initially, M = 1.
 *
 * On each player's turn, that player can take all the stones in the first X remaining
 * piles, where 1 <= X <= 2M.  Then, we set M = max(M, X).
 *
 * The game continues until all the stones have been taken.
 *
 * Assuming Alex and Lee play optimally, return the maximum number of stones Alex can get.
 *
 * Example 1:
 *
 * Input: piles = [2,7,9,4,4]
 * Output: 10
 * Explanation:  If Alex takes one pile at the beginning, Lee takes two piles, then
 * Alex takes 2 piles again. Alex can get 2 + 4 + 4 = 10 piles in total. If Alex takes
 * two piles at the beginning, then Lee can take all three piles left. In this case,
 * Alex get 2 + 7 = 9 piles in total. So we return 10 since it's larger.
 *
 * Constraints:
 *
 *     1 <= piles.length <= 100
 *     1 <= piles[i] <= 10 ^ 4
 */
/**
 * 32 is obtained by 2 ^ (log2(100) - 1)
 * memo array record the max differences between alex and lee in position i with given m
 * And we have folloing equation.
 *
 *  X + Y = sum
 *  X - Y = delta
 *  X = (sum + delta) / 2;
 *
 *  X - Alex's stone number
 *  Y - Lee's stone number
 *  sum - The total stones
 *  delta - The diff between Alex and Lee
 */
class Solution
{
public:
    int memo[101][32] = {};
    int dfs(vector<int>& dp, int x, int m, int res = INT_MIN) {
        /**
         * The player can take all the rest
         */
        if (x + m * 2 >= dp.size()) return dp[dp.size() - 1] - dp[x - 1];
        /**
         * Already calculated.
         */
        if (memo[x][m]) return memo[x][m];

        for (int i = x; i < (x + m * 2); ++i) {
            int stonesTake = dp[i] - (0 == x ? 0 : dp[x - 1]);
            int otherTakeMax = dfs(dp, i + 1, max(m, i - x + 1));
            res = max(res, stonesTake - otherTakeMax);
        }

        return memo[x][m] = res;
    }

    int stoneGameII(vector<int>& ps) {
        partial_sum(begin(ps), end(ps), begin(ps));
        return (ps[ps.size() - 1] + dfs(ps, 0, 1)) / 2;
    }
};
