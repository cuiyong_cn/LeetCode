/**
 * Maximum Number of Vowels in a Substring of Given Length
 *
 * Given a string s and an integer k.
 *
 * Return the maximum number of vowel letters in any substring of s with length k.
 *
 * Vowel letters in English are (a, e, i, o, u).
 *
 * Example 1:
 *
 * Input: s = "abciiidef", k = 3
 * Output: 3
 * Explanation: The substring "iii" contains 3 vowel letters.
 *
 * Example 2:
 *
 * Input: s = "aeiou", k = 2
 * Output: 2
 * Explanation: Any substring of length 2 contains 2 vowels.
 *
 * Example 3:
 *
 * Input: s = "leetcode", k = 3
 * Output: 2
 * Explanation: "lee", "eet" and "ode" contain 2 vowels.
 *
 * Example 4:
 *
 * Input: s = "rhythms", k = 4
 * Output: 0
 * Explanation: We can see that s doesn't have any vowel letters.
 *
 * Example 5:
 *
 * Input: s = "tryhard", k = 4
 * Output: 1
 *
 * Constraints:
 *     1 <= s.length <= 10^5
 *     s consists of lowercase English letters.
 *     1 <= k <= s.length
 */
/**
 * Using array to speed things up.
 */
class Solution {
public:
    int maxVowels(string s, int k) {
        vector<int> isVowel(26, 0);
        isVowel['a' - 'a'] = 1;
        isVowel['e' - 'a'] = 1;
        isVowel['i' - 'a'] = 1;
        isVowel['o' - 'a'] = 1;
        isVowel['u' - 'a'] = 1;

        int maxVowels = 0;
        for (int i = 0; i < k && i < s.size(); ++i) {
            maxVowels += isVowel[s[i] - 'a'] ? 1 : 0;
        }
        int vowelsInWindow = maxVowels;
        for (int i = k; i < s.size(); ++i) {
            vowelsInWindow -= isVowel[s[i - k] - 'a'] ? 1 : 0;
            vowelsInWindow += isVowel[s[i] - 'a'] ? 1 : 0;
            maxVowels = max(maxVowels, vowelsInWindow);
        }

        return maxVowels;
    }
};

/**
 * Original solution. Track the vowels in window. Slide the window to find the max one.
 */
class Solution {
public:
    int maxVowels(string s, int k) {
        const string vowels{"aeiou"};
        int maxVowels = 0;
        for (int i = 0; i < k && i < s.size(); ++i) {
            if (vowels.find(s[i]) != string::npos) {
                ++maxVowels;
            }
        }
        int vowelsInWindow = maxVowels;
        for (int i = k; i < s.size(); ++i) {
            if (vowels.find(s[i - k]) != string::npos) {
                --vowelsInWindow;
            }
            if (vowels.find(s[i]) != string::npos) {
                ++vowelsInWindow;
            }

            maxVowels = max(maxVowels, vowelsInWindow);
        }

        return maxVowels;
    }
};
