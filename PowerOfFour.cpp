/**
 * Power of Four
 *
 * Given an integer n, return true if it is a power of four. Otherwise, return false.
 *
 * An integer n is a power of four, if there exists an integer x such that n == 4x.
 *
 * Example 1:
 *
 * Input: n = 16
 * Output: true
 * Example 2:
 *
 * Input: n = 5
 * Output: false
 * Example 3:
 *
 * Input: n = 1
 * Output: true
 *
 * Constraints:
 *
 * -231 <= n <= 231 - 1
 */
/**
 * without loop
 */
class Solution {
public:
    bool isPowerOfFour(int n) {
        if (n <= 0) {
            return false;
        }

        if ((n & (n - 1)) != 0) {
            return false;
        }

        return n & 0x55555555;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool isPowerOfFour(int n) {
        if (n <= 0) {
            return false;
        }

        while (n > 1) {
            auto re = n % 4;
            if (re != 0) {
                return false;
            }
            n = n / 4;
        }

        return 1 == n;
    }
};
