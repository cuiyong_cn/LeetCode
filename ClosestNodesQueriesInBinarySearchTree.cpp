/**
 * Closest Nodes Queries in a Binary Search Tree
 *
 * You are given the root of a binary search tree and an array queries of size n consisting of
 * positive integers.
 *
 * Find a 2D array answer of size n where answer[i] = [mini, maxi]:
 *
 * mini is the largest value in the tree that is smaller than or equal to queries[i]. If a such
 * value does not exist, add -1 instead.
 * maxi is the smallest value in the tree that is greater than or equal to queries[i]. If a such
 * value does not exist, add -1 instead.
 * Return the array answer.
 *
 * Example 1:
 *
 * Input: root = [6,2,13,1,4,9,15,null,null,null,null,null,null,14], queries = [2,5,16]
 * Output: [[2,2],[4,6],[15,-1]]
 * Explanation: We answer the queries in the following way:
 * - The largest number that is smaller or equal than 2 in the tree is 2, and the smallest number that is greater or equal than 2 is still 2. So the answer for the first query is [2,2].
 * - The largest number that is smaller or equal than 5 in the tree is 4, and the smallest number that is greater or equal than 5 is 6. So the answer for the second query is [4,6].
 * - The largest number that is smaller or equal than 16 in the tree is 15, and the smallest number that is greater or equal than 16 does not exist. So the answer for the third query is [15,-1].
 * Example 2:
 *
 * Input: root = [4,null,9], queries = [3]
 * Output: [[-1,4]]
 * Explanation: The largest number that is smaller or equal to 3 in the tree does not exist, and the smallest number that is greater or equal to 3 is 4. So the answer for the query is [-1,4].
 *
 * Constraints:
 *
 * The number of nodes in the tree is in the range [2, 105].
 * 1 <= Node.val <= 106
 * n == queries.length
 * 1 <= n <= 105
 * 1 <= queries[i] <= 106
 */
/**
 * Passed version. Flat the tree.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
auto flat_bst(TreeNode* node, vector<int>& arr)
{
    if (nullptr == node) return;

    flat_bst(node->left, arr);
    arr.push_back(node->val);
    flat_bst(node->right, arr);
}

class Solution {
public:
    vector<vector<int>> closestNodes(TreeNode* root, vector<int>& queries) {
        auto ans = vector<vector<int>>{};
        auto arr = vector<int>{};
        flat_bst(root, arr);

        for (auto q : queries) {
            auto lle = -1;
            auto sge = -1;
            auto it = lower_bound(begin(arr), end(arr), q);
            if (it != end(arr)) {
                sge = *it;
                lle = q == sge ? sge :
                      it != begin(arr) ? *(it  - 1) : -1;
            }
            else {
                lle = arr.back();
            }

            ans.push_back({lle, sge});
        }

        return ans;
    }
};

/**
 * Original solution. TLE. (Guess traverse the tree cost too much even with cache)
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
auto largest_le(TreeNode* node, int t)
{
    if (nullptr == node) {
        return -1;
    }

    if (node->val == t) {
        return t;
    }

    return node->val < t ? max(node->val, largest_le(node->right, t)) : largest_le(node->left, t);
}

auto smallest_ge(TreeNode* node, int t)
{
    if (nullptr == node) {
        return -1;
    }

    if (node->val == t) {
        return t;
    }

    if (node->val < t) {
        return smallest_ge(node->right, t);
    }
    auto sge = smallest_ge(node->left, t);
    return -1 == sge ? node->val : min(node->val, sge);
}

class Solution {
public:
    vector<vector<int>> closestNodes(TreeNode* root, vector<int>& queries) {
        auto ans = vector<vector<int>>{};
        for (auto q : queries) {
            ans.push_back({largest_le(root, q), smallest_ge(root, q)});
        }
        return ans;
    }
};
