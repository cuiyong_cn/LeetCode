/**
 * Populating Next Right Pointers in Each Node II
 *
 * Given a binary tree
 *
 * struct Node {
 *   int val;
 *   Node *left;
 *   Node *right;
 *   Node *next;
 * }
 * Populate each next pointer to point to its next right node. If there is no next right node, the
 * next pointer should be set to NULL.
 *
 * Initially, all next pointers are set to NULL.
 *
 * Follow up:
 *
 * You may only use constant extra space.
 * Recursive approach is fine, you may assume implicit stack space does not count as extra space for
 * this problem.
 *
 * Example 1:
 *
 * Input: root = [1,2,3,4,5,null,7]
 * Output: [1,#,2,3,#,4,5,7,#]
 * Explanation: Given the above binary tree (Figure A), your function should populate each next
 * pointer to point to its next right node, just like in Figure B. The serialized output is in level
 * order as connected by the next pointers, with '#' signifying the end of each level.
 *
 * Constraints:
 *
 * The number of nodes in the given tree is less than 6000.
 * -100 <= node.val <= 100
 */
/**
 * Similar to orginal solution but using O(1) space.
 * After reading the discussion. Recusive one basically are iterative variant.
 */
class Solution {
public:
    Node* connect(Node* root) {
        if (!root) {
            return nullptr;
        }

        Node dummyNode;
        Node* cur = root;
        Node* prev = &dummyNode;
        while (cur) {
            if (cur->left) {
                prev->next = cur->left;
                prev = cur->left;
            }
            if (cur->right) {
                prev->next = cur->right;
                prev = cur->right;
            }
            if (cur->next) {
                cur = cur->next;
            }
            else {
                cur = dummyNode.next;
                dummyNode.next = nullptr;
                prev = &dummyNode;
            }
        }

        return root;
    }
};

/**
 * Recursive solution. There gonna be better recursive solution.
 */
class Solution {
public:
    Node* connect(Node* root) {
        if (!root) {
            return nullptr;
        }

        auto cur = root;
        Node* prev = nullptr;
        while (cur) {
            if (prev) {
                auto n = cur->left ? cur->left : cur->right;
                if (n) {
                    prev->next = n;
                    prev = n;
                }
            }

            if (cur->right) {
                if (cur->left) {
                    cur->left->next = cur->right;
                }
                prev = cur->right;
            }
            else {
                prev = cur->left ? cur->left : prev;
            }

            if (cur->next) {
                auto nn = cur->next->left ? cur->next->left : cur->next->right;
                if (nn) {
                    auto n = cur->right ? cur->right : cur->left;
                    if (n) {
                        n->next = nn;
                    }
                }
            }
            cur = cur->next;
        }


        if (root->left) {
            connect(root->left);
        }
        else if (root->right) {
            connect(root->right);
        }
        else {
            connect(root->next);
        }

        return root;
    }
};

/**
 * Original solution.
 */
/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* left;
    Node* right;
    Node* next;

    Node() : val(0), left(NULL), right(NULL), next(NULL) {}

    Node(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}

    Node(int _val, Node* _left, Node* _right, Node* _next)
        : val(_val), left(_left), right(_right), next(_next) {}
};
*/
class Solution {
public:
    Node* connect(Node* root) {
        if (!root) {
            return nullptr;
        }

        vector<Node*> q{root};
        while (!q.empty()) {
            vector<Node*> nq;
            Node* prev = nullptr;
            for (auto n : q) {
                if (prev) {
                    prev->next = n;
                }
                prev = n;

                if (n->left) {
                    nq.push_back(n->left);
                }
                if (n->right) {
                    nq.push_back(n->right);
                }
            }

            swap(q, nq);
        }

        return root;
    }
};
