/**
 * Special Array With X Elements Greater Than or Equal X
 *
 * You are given an array nums of non-negative integers. nums is considered special if there exists
 * a number x such that there are exactly x numbers in nums that are greater than or equal to x.
 *
 * Notice that x does not have to be an element in nums.
 *
 * Return x if the array is special, otherwise, return -1. It can be proven that if nums is special,
 * the value for x is unique.
 *
 * Example 1:
 *
 * Input: nums = [3,5]
 * Output: 2
 * Explanation: There are 2 values (3 and 5) that are greater than or equal to 2.
 * Example 2:
 *
 * Input: nums = [0,0]
 * Output: -1
 * Explanation: No numbers fit the criteria for x.
 * If x = 0, there should be 0 numbers >= x, but there are 2.
 * If x = 1, there should be 1 number >= x, but there are 0.
 * If x = 2, there should be 2 numbers >= x, but there are 0.
 * x cannot be greater since there are only 2 numbers in nums.
 * Example 3:
 *
 * Input: nums = [0,4,3,0,4]
 * Output: 3
 * Explanation: There are 3 values that are greater than or equal to 3.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 100
 * 0 <= nums[i] <= 1000
 */
/**
 * O(n) solution.
 */
class Solution {
public:
    int specialArray(vector<int>& nums) {
        int const N = 102;
        array<int, N> cnt = { 0, };

        for (auto n : nums) {
            ++cnt[n > 100 ? 100 : n];
        }

        for (int i = N - 2; i >= 0; --i) {
            cnt[i] += cnt[i + 1];
            if (i == cnt[i]) {
                return i;
            }
        }

        return -1;
    }
};

/**
 * Using binary search.
 */
class Solution {
public:
    int specialArray(vector<int>& nums) {
        sort(nums.begin(), nums.end(), greater<int>());

        int L = 0;
        int R = nums.size();
        while (L < R) {
            auto mid = L + (R - L) / 2;
            if (mid < nums[mid]) {
                L = mid + 1;
            }
            else {
                R = mid;
            }
        }

        return L < nums.size() && L == nums[L] ? -1 : L;
    }
};

/**
 * Reduce to one single loop.
 */
class Solution {
public:
    int specialArray(vector<int>& nums) {
        sort(nums.begin(), nums.end(), greater<int>());

        int const N = nums.size();
        int i = 0;
        for (; i < N && nums[i] > i; ++i) {
            ;
        }

        return i < N && i == nums[i] ? - 1 : i;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int specialArray(vector<int>& nums) {
        sort(nums.begin(), nums.end());

        int R = nums.size();
        for (int i = 1; i <= R; ++i) {
            auto it = lower_bound(nums.begin(), nums.end(), i);
            auto idx = distance(nums.begin(), it);
            if ((R - idx) == i) {
                return i;
            }
        }

        return -1;
    }
};
