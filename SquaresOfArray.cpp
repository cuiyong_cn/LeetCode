/**
 * Squares of a Sorted Array
 *
 * Given an array of integers A sorted in non-decreasing order, return an array of the squares of each number, also in sorted non-decreasing order.
 *
 * Example 1:
 *
 * Input: [-4,-1,0,3,10]
 * Output: [0,1,9,16,100]
 *
 * Example 2:
 *
 * Input: [-7,-3,2,3,11]
 * Output: [4,9,9,49,121]
 *
 * Note:
 *
 *     1 <= A.length <= 10000
 *     -10000 <= A[i] <= 10000
 *     A is sorted in non-decreasing order.
 */
/**
 * Found a shorter and clener one
 *
 * class Solution {
 * public:
 *     vector<int> sortedSquares(vector<int>& A) {
 *         std::vector<int> squares(A.size());
 *         int l = 0, r = A.size() - 1, p = A.size() - 1; 
 *         while (l <= r)
 *             squares[p--] = std::pow(A[(std::abs(A[l]) > std::abs(A[r])) ? l++ : r--], 2);
 *         return squares;
 *     }
 * };
 */
class Solution {
public:
    vector<int> sortedSquares(vector<int>& A) {
        int idx = -1;
        for (auto n : A) {
            if (n >= 0) break;
            ++idx;
        }
        int i = idx + 1;
        int j = idx;
        vector<int> ans;
        ans.reserve(ans.size());
        if (idx >= 0) {
            for (; j >= 0 && i < A.size();) {
                int num = 0;
                if ((A[i] + A[j]) > 0) {
                    num = -A[j--];
                }
                else {
                    num = A[i++];
                }
                ans.push_back(num * num);
            }
        }
        
        for (; j >= 0; --j) {
            ans.push_back(A[j] * A[j]);
        }
        for (; i < A.size(); ++i) {
            ans.push_back(A[i] * A[i]);
        }
        
        return ans;
    }
};
