/**
 * Binary Subarrays With Sum
 *
 * In an array A of 0s and 1s, how many non-empty subarrays have sum S?
 *
 * Example 1:
 *
 * Input: A = [1,0,1,0,1], S = 2
 * Output: 4
 * Explanation:
 * The 4 subarrays are bolded below:
 * [1,0,1,0,1]
 * [1,0,1,0,1]
 * [1,0,1,0,1]
 * [1,0,1,0,1]
 *
 * Note:
 *
 * A.length <= 30000
 * 0 <= S <= A.length
 * A[i] is either 0 or 1.
 */
/**
 * Three pointer solution. kind of like the original dp solution.
 */
class Solution {
public:
    int numSubarraysWithSum(vector<int>& A, int S) {
        int iLo = 0, iHi = 0;
        int sumLo = 0, sumHi = 0;
        int ans = 0;

        for (int j = 0; j < A.size(); ++j) {
            sumLo += A[j];
            sumHi += A[j];

            while (iLo < j && sumLo > S) {
                sumLo -= A[iLo++];
            }
            while (iHi < j && (sumHi > S || (sumHi == S && 0 == A[iHi]))) {
                sumHi -= A[iHi++];
            }

            if (sumLo == S) {
                ans += iHi - iLo + 1;
            }
        }

        return ans;
    }
};

/**
 * Improve the below code.
 */
class Solution {
public:
    int numSubarraysWithSum(vector<int>& A, int S) {
        unordered_map<int, int> sum_count;
        sum_count[0] = 1;
        int ans = 0;
        int sum = 0;
        for (auto n : A) {
            sum += n;
            auto target = sum - S;
            if (sum_count.count(target)) {
                ans += sum_count[target];
            }
            ++sum_count[sum];
        }

        return ans;
    }
};

/**
 * Variant of prefix sum solution.
 */
class Solution {
public:
    int numSubarraysWithSum(vector<int>& A, int S) {
        unordered_map<int, int> sum_count;
        sum_count[0] = 1;
        int sum = 0;
        for (auto n : A) {
            sum += n;
            ++sum_count[sum];
        }

        int ans = 0;
        if (0 == S) {
            for (auto const& sc : sum_count) {
                ans += sc.second * (sc.second - 1) / 2;
            }
        }
        else {
            for (auto const& sc : sum_count) {
                auto [s, c] = sc;
                if (sum_count.count(s + S)) {
                    ans += c * sum_count[s + S];
                }
            }
        }

        return ans;
    }
};

/**
 * It turns out, DP solution is a dead end.
 */
class Solution {
public:
    int numSubarraysWithSum(vector<int>& A, int S) {
        vector<int> ones_index;

        ones_index.push_back(-1);
        for (int i = 0; i < A.size(); ++i) {
            if (A[i]) {
                ones_index.push_back(i);
            }
        }
        ones_index.push_back(A.size());

        int ans = 0;
        if (0 == S) {
            for (int i = 0; i < (ones_index.size() - 1); ++i) {
                auto consecutive_zeros = ones_index[i + 1] - ones_index[i] - 1;
                ans += consecutive_zeros * (consecutive_zeros + 1) / 2;
            }
        }
        else {
            for (int i = 1; i < ones_index.size() - S; ++i) {
                auto j = i + S - 1;
                ans += (ones_index[i] - ones_index[i - 1]) * (ones_index[j + 1] - ones_index[j]);
            }
        }

        return ans;
    }
};

/**
 * Original solution. Almost passed. but TLE at last few cases.
 */
class Solution {
public:
    int numSubarraysWithSum(vector<int>& A, int S) {
        if (A.empty()) {
            return 0;
        }

        vector<vector<int>> dp(A.size(), vector<int>(S + 1, 0));

        if (A.front()) {
            if (S > 0) {
                dp[0][1] = 1;
            }
        }
        else {
            dp[0][0] = 1;
        }

        for (int i = 1; i < A.size(); ++i) {
            if (A[i]) {
                for (int j = S; j > 0; --j) {
                    dp[i][j] = dp[i - 1][j - 1];
                }
                if (S > 0) {
                    ++dp[i][1];
                }
            }
            else {
                for (int j = S; j >= 0; --j) {
                    dp[i][j] = dp[i - 1][j];
                }
                ++dp[i][0];
            }
        }

        return accumulate(dp.begin(), dp.end(), 0, [S](auto sum, auto const& r) {
            return sum + r[S];
        });
    }
};
