/**
 * Count Unreachable Pairs of Nodes in an Undirected Graph
 *
 * You are given an integer n. There is an undirected graph with n nodes, numbered from 0 to n - 1.
 * You are given a 2D integer array edges where edges[i] = [ai, bi] denotes that there exists an
 * undirected edge connecting nodes ai and bi.
 *
 * Return the number of pairs of different nodes that are unreachable from each other.
 *
 * Example 1:
 *
 * Input: n = 3, edges = [[0,1],[0,2],[1,2]]
 * Output: 0
 * Explanation: There are no pairs of nodes that are unreachable from each other. Therefore, we return 0.
 * Example 2:
 *
 * Input: n = 7, edges = [[0,2],[0,5],[2,4],[1,6],[5,4]]
 * Output: 14
 * Explanation: There are 14 pairs of nodes that are unreachable from each other:
 * [[0,1],[0,3],[0,6],[1,2],[1,3],[1,4],[1,5],[2,3],[2,6],[3,4],[3,5],[3,6],[4,6],[5,6]].
 * Therefore, we return 14.
 *
 * Constraints:
 *
 * 1 <= n <= 105
 * 0 <= edges.length <= 2 * 105
 * edges[i].length == 2
 * 0 <= ai, bi < n
 * ai != bi
 * There are no repeated edges.
 */
/**
 * Seems dfs is more efficient in this kinds of issue.
 */
class Solution {
public:
    long long countPairs(int n, vector<vector<int>>& edges) {
        auto visited = vector<bool>(n, false);
        auto links = unordered_map<int, vector<int>>{};

        for (auto const& e : edges) {
            links[e[0]].push_back(e[1]);
            links[e[1]].push_back(e[0]);
        }

        auto pairs = static_cast<long long>(n) * (n  - 1) / 2;
        for (auto i = 0; i < n; ++i) {
            if (visited[i]) {
                continue;
            }
            auto nodes = dfs(i, links, visited);
            pairs -= nodes * (nodes - 1) / 2;
        }

        return pairs;
    }

private:
    long long dfs(int i, unordered_map<int, vector<int>> const& links, vector<bool>& visited)
    {
        if (visited[i]) {
            return 0;
        }

        visited[i] = true;
        auto nodes = 1LL;
        if (links.count(i)) {
            for (auto n : links.at(i)) {
                nodes += dfs(n, links, visited);
            }
        }

        return nodes;
    }
};

/**
 * different thinking.
 *  From n nodes, we can get n * (n - 1) / 2 pairs.
 *  Nodes in the same group can not participate in paring. k * (k - 1) / 2
 */
class Solution {
public:
    long long countPairs(int n, vector<vector<int>>& edges) {
        auto visited = vector<bool>(n, false);
        auto links = unordered_map<int, vector<int>>{};
        auto group = vector<long long>{};

        for (auto const& e : edges) {
            links[e[0]].push_back(e[1]);
            links[e[1]].push_back(e[0]);
        }

        for (auto i = 0; i < n; ++i) {
            if (visited[i]) {
                continue;
            }

            auto nodes = 0LL;
            auto dq = deque<int>{i};
            while (!dq.empty()) {
                auto size = dq.size();
                for (auto i = 0; i < size; ++i) {
                    auto node = dq.front();
                    dq.pop_front();
                    if (visited[node]) {
                        continue;
                    }

                    ++nodes;
                    visited[node] = true;
                    for (auto n : links[node]) {
                        dq.push_back(n);
                    }
                }
            }

            group.push_back(nodes);
        }

        auto ans = 0LL;
        auto sum = accumulate(begin(group), end(group), 0LL);
        for (auto n : group) {
            ans += n * (sum - n);
        }

        return ans / 2;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    long long countPairs(int n, vector<vector<int>>& edges) {
        auto visited = vector<bool>(n, false);
        auto links = unordered_map<int, vector<int>>{};
        auto group = vector<long long>{};

        for (auto const& e : edges) {
            links[e[0]].push_back(e[1]);
            links[e[1]].push_back(e[0]);
        }

        for (auto i = 0; i < n; ++i) {
            if (visited[i]) {
                continue;
            }

            auto nodes = 0LL;
            auto dq = deque<int>{i};
            while (!dq.empty()) {
                auto size = dq.size();
                for (auto i = 0; i < size; ++i) {
                    auto node = dq.front();
                    dq.pop_front();
                    if (visited[node]) {
                        continue;
                    }

                    ++nodes;
                    visited[node] = true;
                    for (auto n : links[node]) {
                        dq.push_back(n);
                    }
                }
            }

            group.push_back(nodes);
        }

        auto ans = 0LL;
        auto sum = accumulate(begin(group), end(group), 0LL);
        for (auto n : group) {
            ans += n * (sum - n);
        }

        return ans / 2;
    }
};
