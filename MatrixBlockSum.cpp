/**
 * Matrix Block Sum
 *
 * Given a m x n matrix mat and an integer k, return a matrix answer where each answer[i][j] is the
 * sum of all elements mat[r][c] for:
 *
 * i - k <= r <= i + k,
 * j - k <= c <= j + k, and
 * (r, c) is a valid position in the matrix.
 *
 * Example 1:
 *
 * Input: mat = [[1,2,3],[4,5,6],[7,8,9]], k = 1
 * Output: [[12,21,16],[27,45,33],[24,39,28]]
 * Example 2:
 *
 * Input: mat = [[1,2,3],[4,5,6],[7,8,9]], k = 2
 * Output: [[45,45,45],[45,45,45],[45,45,45]]
 *
 * Constraints:
 *
 * m == mat.length
 * n == mat[i].length
 * 1 <= m, n, k <= 100
 * 1 <= mat[i][j] <= 100
 */
/**
 * Original solution. Prefix sum solution.
 */
class Solution {
public:
    vector<vector<int>> matrixBlockSum(vector<vector<int>>& mat, int k) {
        int const M = mat.size();
        int const N = mat[0].size();
        vector<vector<int>> blocksum(M + 1, vector<int>(N + 1, 0));

        for (int i = 0; i < M; ++i) {
            auto const& row = mat[i];
            partial_sum(row.begin(), row.end(), blocksum[i + 1].begin() + 1);
        }

        for (int r = 2; r <= M; ++r) {
            for (int c = 1; c <= N; ++c) {
                blocksum[r][c] += blocksum[r - 1][c];
            }
        }

        vector<vector<int>> ans(M, vector<int>(N, 0));
        for (int r = 0; r < M; ++r) {
            for (int c = 0; c < N; ++c) {
                int tlr = max(r - k, 0), tlc = max(c - k, 0);
                int brr = min(r + k + 1, M), brc = min(c + k + 1, N);
                ans[r][c] = blocksum[brr][brc] - blocksum[brr][tlc] - blocksum[tlr][brc] + blocksum[tlr][tlc];
            }
        }
        return ans;
    }
};
