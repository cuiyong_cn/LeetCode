/**
 * Partition Array for Maximum Sum
 *
 * Given an integer array A, you partition the array into (contiguous) subarrays
 * of length at most K.  After partitioning, each subarray has their values changed to become the maximum value of that subarray.
 *
 * Return the largest sum of the given array after partitioning.
 *
 * Example 1:
 *
 * Input: A = [1,15,7,9,2,5,10], K = 3
 * Output: 84
 * Explanation: A becomes [15,15,15,9,10,10,10]
 *
 * Note:
 *
 *     1 <= K <= A.length <= 500
 *     0 <= A[i] <= 10^6
 */
/**
 * dp[i] record the maximum sum we can get considering A[0] ~ A[i]
 * To get dp[i], we will try to change k last numbers separately to the maximum of them,
 * where k = 1 to k = K.
 *
 * Complexity
 *
 * Time O(NK), Space O(N)
 */
class Solution
{
    int maxSumAfterPartitioning(vector<int>& A, int K)
    {
        int N = A.size();
        vector<int> dp(N);
        for (int i = 0; i < N; ++i) {
            int curMax = 0;
            for (int k = 1; k <= K && i - k + 1 >= 0; ++k) {
                curMax = max(curMax, A[i - k + 1]);
                dp[i] = max(dp[i], (i >= k ? dp[i - k] : 0) + curMax * k);
            }
        }
        return dp[N - 1];
    }
};

/**
 * Top-down method.
 * For each position pos, increase your subarray size i from 1 to k, tracking the
 * maximum value mv so far. The sum of the subarray is mv * i.
 * Then, call our function recursively for position pos + i.
 */
class Solution
{
public:
    int dp[501] = {};
    int maxSumAfterPartitioning(vector<int>& A, int K, int pos = 0, int res = 0)
    {
        if (pos < A.size() && dp[pos] != 0) return dp[pos];
        for (int i = 1, mv = 0; i <= K && pos + i <= A.size(); ++i) {
            mv = max(mv, A[pos + i - 1]);
            res = max(res, mv * i + maxSumAfterPartitioning(A, K, pos + i));
        }
        return dp[pos] = res;
    }
};

