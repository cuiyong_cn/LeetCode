/**
 * Check if Word Can Be Placed In Crossword
 *
 * You are given an m x n matrix board, representing the current state of a crossword puzzle. The
 * crossword contains lowercase English letters (from solved words), ' ' to represent any empty
 * cells, and '#' to represent any blocked cells.
 *
 * A word can be placed horizontally (left to right or right to left) or vertically (top to bottom
 * or bottom to top) in the board if:
 *
 * It does not occupy a cell containing the character '#'.  The cell each letter is placed in must
 * either be ' ' (empty) or match the letter already on the board.  There must not be any empty
 * cells ' ' or other lowercase letters directly left or right of the word if the word was placed
 * horizontally.  There must not be any empty cells ' ' or other lowercase letters directly above or
 * below the word if the word was placed vertically.  Given a string word, return true if word can
 * be placed in board, or false otherwise.
 *
 * Example 1:
 *
 * Input: board = [["#", " ", "#"], [" ", " ", "#"], ["#", "c", " "]], word = "abc"
 * Output: true
 * Explanation: The word "abc" can be placed as shown above (top to bottom).
 * Example 2:
 *
 * Input: board = [[" ", "#", "a"], [" ", "#", "c"], [" ", "#", "a"]], word = "ac"
 * Output: false
 * Explanation: It is impossible to place the word because there will always be a space/letter above or below it.
 * Example 3:
 *
 * Input: board = [["#", " ", "#"], [" ", " ", "#"], ["#", " ", "c"]], word = "ca"
 * Output: true
 * Explanation: The word "ca" can be placed as shown above (right to left).
 *
 * Constraints:
 *
 * m == board.length
 * n == board[i].length
 * 1 <= m * n <= 2 * 105
 * board[i][j] will be ' ', '#', or a lowercase English letter.
 * 1 <= word.length <= max(m, n)
 * word will contain only lowercase English letters.
 */
/**
 * Original solution. We can also only use horizontal check by rotate the board.
 */
template<typename Iter1, typename Iter2>
bool canBePlaced(Iter1 b1, Iter1 e1, Iter2 b2, Iter2 e2)
{
    for (; b1 != e1; ++b1,++b2) {
        if (' ' != *b1 && *b1 != *b2) {
            return false;
        }
    }

    return true;
}

bool top_to_bottom_check(vector<vector<char>> const& board, string& word, int col, int top)
{
    int const len = word.length();
    for (int i = 0; i < len; ++i, ++top) {
        if (' ' != board[top][col] && board[top][col] != word[i]) {
            return false;
        }
    }

    return true;
}

bool bottom_to_top_check(vector<vector<char>> const& board, string& word, int col, int bot)
{
    int const len = word.length();
    for (int i = 0; i < len; ++i, --bot) {
        if (' ' != board[bot][col] && board[bot][col] != word[i]) {
            return false;
        }
    }

    return true;
}

class Solution {
public:
    bool placeWordInCrossword(vector<vector<char>>& board, string word) {
        int const M = board.size();
        int const N = board[0].size();

        for (auto const& row : board) {
            int left = 0;
            while (left < N) {
                auto right = left;
                while (right < N && '#' != row[right]) {
                    ++right;
                }
                auto const len = right - left;
                if (len == word.length()) {
                    if (canBePlaced(row.begin() + left, row.begin() + right, word.begin(), word.end())
                        || canBePlaced(row.rbegin() + (N - right), row.rbegin() + (N - left), word.begin(), word.end())) {
                        return true;
                    }
                }
                left = right + 1;
            }
        }

        for (int c = 0; c < N; ++c) {
            int top = 0;
            while (top < M) {
                auto bot = top;
                while (bot < M && '#' != board[bot][c]) {
                    ++bot;
                }
                auto const len = bot - top;
                if (len == word.length()) {
                    if (top_to_bottom_check(board, word, c, top)
                        || bottom_to_top_check(board, word, c, bot - 1)) {
                        return true;
                    }
                }
                top = bot + 1;
            }
        }

        return false;
    }
};
