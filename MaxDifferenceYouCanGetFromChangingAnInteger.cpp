/**
 * Max Difference You Can Get From Changing an Integer
 *
 * You are given an integer num. You will apply the following steps exactly two times:
 *
 * Pick a digit x (0 <= x <= 9).
 * Pick another digit y (0 <= y <= 9). The digit y can be equal to x.
 * Replace all the occurrences of x in the decimal representation of num by y.
 * The new integer cannot have any leading zeros, also the new integer cannot be 0.
 * Let a and b be the results of applying the operations to num the first and second times,
 * respectively.
 *
 * Return the max difference between a and b.
 *
 * Example 1:
 *
 * Input: num = 555
 * Output: 888
 * Explanation: The first time pick x = 5 and y = 9 and store the new integer in a.
 * The second time pick x = 5 and y = 1 and store the new integer in b.
 * We have now a = 999 and b = 111 and max difference = 888
 * Example 2:
 *
 * Input: num = 9
 * Output: 8
 * Explanation: The first time pick x = 9 and y = 9 and store the new integer in a.
 * The second time pick x = 9 and y = 1 and store the new integer in b.
 * We have now a = 9 and b = 1 and max difference = 8
 * Example 3:
 *
 * Input: num = 123456
 * Output: 820000
 * Example 4:
 *
 * Input: num = 10000
 * Output: 80000
 * Example 5:
 *
 * Input: num = 9288
 * Output: 8700
 *
 * Constraints:
 *
 * 1 <= num <= 10^8
 */
/**
 * Improve below solution.
 */
class Solution {
public:
    int maxDiff(int num) {
        string num_str = to_string(num);
        int i = 0;
        for (auto c : num_str) {
            if (c != '9') {
                break;
            }
            ++i;
        }
        string num1 = num_str;

        std::replace(std::begin(num1), std::end(num1), num_str[i], '9');

        int j = 0;
        string num2 = num_str;
        if (num2[0] != '1') {
            std::replace(std::begin(num2), std::end(num2), num_str[0], '1');
        }
        else if (num2.length() > 1) {
            for (j = 1; j < num2.length(); ++j) {
                if (num2[j] != '0' && num2[j] != num2[0]) {
                    break;
                }
            }

            if (j < num2.length()) {
                std::replace(std::begin(num2), std::end(num2), num_str[j], '0');
            }
        }

        return stoi(num1) - stoi(num2);
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int maxDiff(int num) {
        string num_str = to_string(num);
        int i = 0;
        for (auto c : num_str) {
            if (c != '9') {
                break;
            }
            ++i;
        }
        string num1 = num_str;
        transform(std::begin(num1), std::end(num1), std::begin(num1),
                 [t = num1[i]](auto c) {
                     return t == c ? '9' : c;
                 });

        int j = 0;
        string num2 = num_str;
        if (num2[0] != '1') {
            transform(std::begin(num2), std::end(num2), std::begin(num2),
                     [t = num2[0]](auto c) {
                         return t == c ? '1' : c;
                     });
        }
        else if (num2.length() > 1) {
            for (j = 1; j < num2.length(); ++j) {
                if (num2[j] != '0' && num2[j] != num2[0]) {
                    break;
                }
            }
            if (j < num2.length()) {
                transform(std::begin(num2), std::end(num2), std::begin(num2),
                     [t = num2[j]](auto c) {
                         return t == c ? '0' : c;
                     });
            }
        }

        return stoi(num1) - stoi(num2);
    }
};
