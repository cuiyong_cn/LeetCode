/**
 * Pascal's Triangle II
 *
 * Given a non-negative index k where k ≤ 33, return the kth index row of the Pascal's triangle.
 *
 * Note that the row index starts from 0.
 *
 * In Pascal's triangle, each number is the sum of the two numbers directly above it.
 *
 * Example:
 *
 * Input: 3
 * Output: [1,3,3,1]
 *
 * Follow up:
 *
 * Could you optimize your algorithm to use only O(k) extra space?
 */
/**
 * same idea, but cal from the back.
 */
class Solution {
public:
    vector<int> getRow(int rowIndex) {
        vector<int> row(rowIndex + 1, 0);
        row[0] = 1;

        for (int k = 1; k <= rowIndex; ++k) {
            for (int i = k; i > 0; --i) {
                row[i] += row[i - 1];
            }
        }
        return A;
    }
};

/**
 * Improve the below solution.
 */
class Solution {
public:
    vector<int> getRow(int rowIndex) {
        vector<int> row(rowIndex + 1, 1);

        for (int k = 1; k <= rowIndex; ++k) {
            int prev = 1;
            for (size_t i = 1; i < k; ++i) {
                int tmp = row[i];
                row[i] += prev;
                prev = tmp;
            }
        }

        return row;
    }
};

/**
 * Original solution. Pretty straight forward.
 */
class Solution {
public:
    vector<int> getRow(int rowIndex) {
        vector<int> row{1};

        for (int k = 1; k <= rowIndex; ++k) {
            vector<int> tmp{1};
            for (size_t i = 1; i < row.size(); ++i) {
                tmp.push_back(row[i - 1] + row[i]);
            }
            tmp.push_back(1);

            swap(tmp, row);
        }

        return row;
    }
};
