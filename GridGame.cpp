/**
 * Grid Game
 *
 * You are given a 0-indexed 2D array grid of size 2 x n, where grid[r][c] represents the number of
 * points at position (r, c) on the matrix. Two robots are playing a game on this matrix.
 *
 * Both robots initially start at (0, 0) and want to reach (1, n-1). Each robot may only move to the
 * right ((r, c) to (r, c + 1)) or down ((r, c) to (r + 1, c)).
 *
 * At the start of the game, the first robot moves from (0, 0) to (1, n-1), collecting all the
 * points from the cells on its path. For all cells (r, c) traversed on the path, grid[r][c] is set
 * to 0. Then, the second robot moves from (0, 0) to (1, n-1), collecting the points on its path.
 * Note that their paths may intersect with one another.
 *
 * The first robot wants to minimize the number of points collected by the second robot. In
 * contrast, the second robot wants to maximize the number of points it collects. If both robots
 * play optimally, return the number of points collected by the second robot.
 *
 * Example 1:
 *
 * Input: grid = [[2,5,4],[1,5,1]]
 * Output: 4
 * Explanation: The optimal path taken by the first robot is shown in red, and the optimal path taken by the second robot is shown in blue.
 * The cells visited by the first robot are set to 0.
 * The second robot will collect 0 + 0 + 4 + 0 = 4 points.
 * Example 2:
 *
 * Input: grid = [[3,3,1],[8,5,2]]
 * Output: 4
 * Explanation: The optimal path taken by the first robot is shown in red, and the optimal path taken by the second robot is shown in blue.
 * The cells visited by the first robot are set to 0.
 * The second robot will collect 0 + 3 + 1 + 0 = 4 points.
 * Example 3:
 *
 * Input: grid = [[1,3,1,15],[1,3,3,1]]
 * Output: 7
 * Explanation: The optimal path taken by the first robot is shown in red, and the optimal path taken by the second robot is shown in blue.
 * The cells visited by the first robot are set to 0.
 * The second robot will collect 0 + 1 + 3 + 3 + 0 = 7 points.
 *
 * Constraints:
 *
 * grid.length == 2
 * n == grid[r].length
 * 1 <= n <= 5 * 104
 * 1 <= grid[r][c] <= 105
 */
/**
 * More space friendily one.
 */
class Solution {
public:
    long long gridGame(vector<vector<int>>& g) {
        long long top = accumulate(begin(g[0]), end(g[0]), 0ll), bottom = 0, res = LLONG_MAX;
        for(int i = 0; i < g[0].size(); ++i) {
            top -= g[0][i];
            res = min(res, max(top, bottom));
            bottom += g[1][i];
        }
        return res;
    }
};

/**
 * Original solution using prefix sum.
 */
class Solution {
public:
    long long gridGame(vector<vector<int>>& grid) {
        int const N = grid[0].size();
        vector<vector<long long>> prefix_sum(2, vector<long long>(N + 1, 0));

        for (int i = 0; i < 2; ++i) {
            copy(grid[i].begin(), grid[i].end(), prefix_sum[i].begin() + 1);
            partial_sum(prefix_sum[i].begin(), prefix_sum[i].end(), prefix_sum[i].begin());
        }

        long long max_score = 0;
        long long ans = numeric_limits<long long>::max();

        for (int i = 0; i < N; ++i) {
            auto score = prefix_sum[0][i] + (prefix_sum[1].back() - prefix_sum[1][i]);
            if (score >= max_score) {
                ans = min(ans, max(prefix_sum[0].back() - prefix_sum[0][i + 1], prefix_sum[1][i]));
            }
        }

        return ans;
    }
};
