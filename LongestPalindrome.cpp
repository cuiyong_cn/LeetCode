/**
 * Longest Palindrome
 *
 * Given a string which consists of lowercase or uppercase letters, find the length of the longest palindromes that can
 * be built with those letters.
 *
 * This is case sensitive, for example "Aa" is not considered a palindrome here.
 *
 * Note:
 * Assume the length of given string will not exceed 1,010.
 *
 * Example:
 *
 * Input:
 * "abccccdd"
 *
 * Output:
 * 7
 *
 * Explanation:
 * One longest palindrome that can be built is "dccaccd", whose length is 7.
 */
/**
 * A little improve. Also if we dont't care the memory, we can just use a vector of length 128.
 */
class Solution {
public:
    int longestPalindrome(string s) {
        if (s.length() < 2) return s.length();

        int ans = 0;
        vector<int> letters(52, 0);

        for (auto c : s) {
            if (islower(c)) {
                ++letters[c - 'a'];
            }
            else {
                ++letters[c - 'A' + 26];
            }
        }

        for (auto n : letters) {
            ans += n - n % 2;
        }

        if (ans < s.length()) ++ans;

        return ans;
    }
};

/**
 * Original solution. simple.
 */
class Solution {
public:
    int longestPalindrome(string s) {
        if (s.length() < 2) return s.length();

        int ans = 0;
        vector<int> letters(52, 0);

        for (auto c : s) {
            if (islower(c)) {
                ++letters[c - 'a'];
            }
            else {
                ++letters[c - 'A' + 26];
            }
        }

        for (auto n : letters) {
            ans += n - n % 2;
        }

        auto isOdd = [](auto n) { return n % 2; };
        auto it = find_if(letters.begin(), letters.end(), isOdd);
        if (it != letters.end()) {
            ++ans;
        }

        return ans;
    }
};
