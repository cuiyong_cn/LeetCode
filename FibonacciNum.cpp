/**
 * Fibonacci Number
 *
 * The Fibonacci numbers, commonly denoted F(n) form a sequence, called the
 * Fibonacci sequence, such that each number is the sum of the two preceding ones,
 * starting from 0 and 1. That is,
 *
 * F(0) = 0,   F(1) = 1
 * F(N) = F(N - 1) + F(N - 2), for N > 1.
 *
 * Given N, calculate F(N).
 *
 * Example 1:
 *
 * Input: 2
 * Output: 1
 * Explanation: F(2) = F(1) + F(0) = 1 + 0 = 1.
 *
 * Example 2:
 *
 * Input: 3
 * Output: 2
 * Explanation: F(3) = F(2) + F(1) = 1 + 1 = 2.
 *
 * Example 3:
 *
 * Input: 4
 * Output: 3
 * Explanation: F(4) = F(3) + F(2) = 2 + 1 = 3.
 *
 * Note:
 *
 * 0 ≤ N ≤ 30.
 */
/**
 * Golden ratio method
 */
class Solution {
public:
    int fib(int N) {
        double goldenRatio = (1 + sqrt(5)) / 2;
        return round(pow(goldenRatio, N) / sqrt(5));
    }
};
/**
 * Matrix method
 */
class Solution {
public:
    typedef vector<vector<int> > Matrix;
    Matrix mFibBase;

    Solution() : mFibBase{{1, 1}, {1, 0}} {

    }

    void MatrixMultiply(Matrix& a, Matrix& b) {
        int x1 = a[0][0] * b[0][0] + a[0][1] * b[1][0];
        int x2 = a[0][0] * b[0][1] + a[0][1] * b[1][1];
        int x3 = a[1][0] * b[0][0] + a[1][1] * b[1][0];
        int x4 = a[1][0] * b[0][1] + a[1][1] * b[1][1];

        a[0][0] = x1; a[0][1] = x2;
        a[1][0] = x3; a[1][1] = x4;
    }

    void MatrixPower(Matrix& m, int N) {
        if (N <= 1) return;
        MatrixPower(m, N/2);
        MatrixMultiply(m, m);
        if (N % 2) {
            MatrixMultiply(m, mFibBase);
        }
    }

    int fib(int N) {
        if (N < 2) return N;
        if (N == 2) return 1;

        Matrix mt{{1, 1}, {1, 0}};

        MatrixPower(mt, N - 1);
        return mt[0][0];
    }
};
/**
 * cache method
 */
class Solution {
public:
    int memorize(int N) {
        vector<int> cache(N + 1);
        cache[0] = 0;
        cache[1] = 1;
        cache[2] = 1;
        for (int i = 3; i <= N; ++i) {
            cache[i] = cache[i - 1] + cache[i - 2];
        }
        return cache[N];
    }

    int fib(int N) {
        if (N < 2) return N;
        if (N == 2) return 1;

        return memorize(N);
    }
};
/**
 * Simple recursive one.
 */
class Solution {
public:
    int fib(int N) {
        if (N < 2) return N;
        return fib(N - 1) + fib(N - 2);
    }
};
