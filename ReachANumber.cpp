/**
 * Reach a Number
 *
 * You are standing at position 0 on an infinite number line. There is a goal at position target.
 *
 * On each move, you can either go left or right. During the n-th move (starting from 1), you take n steps.
 *
 * Return the minimum number of steps required to reach the destination.
 *
 * Example 1:
 * Input: target = 3
 * Output: 2
 * Explanation:
 * On the first move we step from 0 to 1.
 * On the second step we step from 1 to 3.
 * Example 2:
 * Input: target = 2
 * Output: 3
 * Explanation:
 * On the first move we step from 0 to 1.
 * On the second move we step  from 1 to -1.
 * On the third move we step from -1 to 2.
 * Note:
 * target will be a non-zero integer in the range [-10^9, 10^9].
 */
/**
 * From discussion. More intuitive. The idea is:
 *
 * 1. since the axis is symmetric, we can only consider target >= 0
 * 2. 1 + 2 + 3 + ... + n = sum
 * 3. if sum < target, then we have to keep going
 * 4. if target == sum, then good, it's n
 * 5. if target > sum, now we can flip the sign of some number in [1..n]
 * 6. if we flip a x, then the sum of the sequence is sum - 2 * x.
 * 7. select subset of the [1..n] sequence, flip the sign, the sum of the sequence become
 *    sum - 2 * (x1 + x2 + .. + xn)
 * 8. what we want is: sum - 2 * (x1 + x2 + .. + xn) == target, this mean
 *    sum - target = 2 * (x1 + x2 + .. + xn)
 * 9. we can be sure that 2 * (x1 + x2 + .. + xn) is a even number
 * 10. so if we find a sum - target is a evene number, we can be sure that we can find
 *     the subset satisfy the requirement.
 */
class Solution {
public:
    int reachNumber(int target) {
        if (target < 0) {
            target = -target;
        }

        int step=0;
        int sum=0;

        while (sum < target || ((sum - target) % 2) != 0) {
            ++step;
            sum += step;
        }

        return step;
    }
};

/**
 * Math solution from leetcode.
 */
class Solution {
public:
    int reachNumber(int target) {
        if (target < 0) {
            target = -target;
        }

        int step = 0;
        while (target > 0) {
            ++step;
            target -= step;
        }

        return target % 2 == 0 ? step : step + 1 + step % 2;
    }
};

/**
 * Original solution. TLE.
 */
class Solution {
public:
    int reachNumber(int target) {
        if (target < 0) {
            target = -target;
        }

        unordered_set<int> queue{0};

        int step = 1;
        while (!queue.empty()) {
            unordered_set<int> next_round;
            for (auto n : queue) {
                if (n == target) {
                    return step - 1;
                }

                next_round.insert(n + step);
                next_round.insert(n - step);
            }

            swap(next_round, queue);
            ++step;
        }

        return step;
    }
};
