/**
 * Number of Good Pairs
 *
 * Given an array of integers nums, return the number of good pairs.
 *
 * A pair (i, j) is called good if nums[i] == nums[j] and i < j.
 *
 * Example 1:
 *
 * Input: nums = [1,2,3,1,1,3]
 * Output: 4
 * Explanation: There are 4 good pairs (0,3), (0,4), (3,4), (2,5) 0-indexed.
 * Example 2:
 *
 * Input: nums = [1,1,1,1]
 * Output: 6
 * Explanation: Each pair in the array are good.
 * Example 3:
 *
 * Input: nums = [1,2,3]
 * Output: 0
 *
 * Constraints:
 *
 * 1 <= nums.length <= 100
 * 1 <= nums[i] <= 100
 */
/**
 * Simplified solution.
 */
class Solution {
public:
    int numIdenticalPairs(vector<int>& nums) {
        int ans = 0;
        unordered_map<int, int> num_cnt;
        for (auto n : nums) {
            ans += num_cnt[n]++;
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int numIdenticalPairs(vector<int>& nums) {
        unordered_map<int, int> num_cnt;
        for (auto n : nums) {
            ++num_cnt[n];
        }

        int ans = 0;
        for (auto [num, cnt] : num_cnt) {
            ans += cnt * (cnt - 1) / 2;
        }

        return ans;
    }
};
