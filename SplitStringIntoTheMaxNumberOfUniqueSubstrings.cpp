/**
 * Split a String Into the Max Number of Unique Substrings
 *
 * Given a string s, return the maximum number of unique substrings that the given string can be split into.
 *
 * You can split string s into any list of non-empty substrings, where the concatenation of the substrings forms the
 * original string. However, you must split the substrings such that all of them are unique.
 *
 * A substring is a contiguous sequence of characters within a string.
 *
 * Example 1:
 *
 * Input: s = "ababccc"
 * Output: 5
 * Explanation: One way to split maximally is ['a', 'b', 'ab', 'c', 'cc']. Splitting like ['a', 'b', 'a', 'b', 'c',
 * 'cc'] is not valid as you have 'a' and 'b' multiple times.
 * Example 2:
 *
 * Input: s = "aba"
 * Output: 2
 * Explanation: One way to split maximally is ['a', 'ba'].
 * Example 3:
 *
 * Input: s = "aa"
 * Output: 1
 * Explanation: It is impossible to split the string any further.
 */
/**
 * More efficient code.
 */
class Solution {
public:
    int maxUniqueSplit(string s) {
        uint32_t maxi = 1;
        int32_t N = s.size();
        for (uint32_t fences = 0; fences < (1<<(N-1)); ++fences) {
            std::bitset<16> bits = fences;
            if (bits.count() < maxi) {
                continue;
            }

            std::unordered_set<std::string> seen;
            std::string curr{s[0]};
            bool unique = true;
            for (int32_t i = 0; i < N-1; ++i) {
                if (bits[i]) {
                    if (!seen.insert(curr).second) {
                        unique = false;
                        break;
                    }

                    curr.clear();
                }
                curr.push_back(s[i+1]);
            }

            if (unique && seen.insert(curr).second) {
                maxi = seen.size();
            }
        }

        return maxi;
    }
};


/**
 * from user HideLord. Basic idea is the following way to split the string
 *
 * a1 | a2 | a3 | a4 => 111
 * a1 a2 | a3 | a4   => 011
 * a1 a2 | a3 a4     => 010
 */
class Solution {
public:
    int maxUniqueSplit(string s) {
        uint32_t maxi = 1;
        int32_t N = s.length();

        for(uint32_t fences = 0; fences < (1<<(N - 1)); ++fences) {
            std::bitset<16> bits = fences;
            if (bits.count() < maxi) {
                continue;
            }

            std::unordered_set<std::string> seen;

            int prev = 0;
            for (int32_t i = 0; i < N - 1; ++i) {
                if (bits[i]) {
                    seen.insert(s.substr(N - 1 - i, i - prev + 1));
                    prev = i + 1;
                }
            }
            seen.insert(s.substr(0, N - prev));

            if (seen.size() == (bits.count() + 1)) {
                maxi = bits.count() + 1;
            }
        }

        return maxi;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int maxUniqueSplit(string s) {
        dfs(s, 0);

        return ans;
    }

private:
    void dfs(const string& s, int start)
    {
        if (start >= s.length()) {
            ans = std::max(ans, seen.size());
            return;
        }

        std::string sub;
        for (int i = start; i < s.length(); ++i) {
            sub += s[i];
            if (!seen.count(sub)) {
                seen.insert(sub);
                dfs(s, i + 1);
                seen.erase(sub);
            }
        }
    }

    std::unordered_set<std::string> seen;
    size_t ans = 0;
};
