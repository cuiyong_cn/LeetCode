/**
 * Largest Sum of Averages
 *
 * We partition a row of numbers A into at most K adjacent (non-empty) groups, then our score is the sum of the average
 * of each group. What is the largest score we can achieve?
 *
 * Note that our partition must use every number in A, and that scores are not necessarily integers.
 *
 * Example:
 * Input:
 * A = [9,1,2,3,9]
 * K = 3
 * Output: 20
 * Explanation:
 * The best choice is to partition A into [9], [1, 2, 3], [9]. The answer is 9 + (1 + 2 + 3) / 3 + 9 = 20.
 * We could have also partitioned A into [9, 1], [2], [3, 9], for example.
 * That partition would lead to a score of 5 + 2 + 6 = 13, which is worse.
 *
 * Note:
 *     1 <= A.length <= 100.
 *     1 <= A[i] <= 10000.
 *     1 <= K <= A.length.
 *     Answers within 10^-6 of the correct answer will be accepted as correct.
 */
/**
 * More clearer dp solution. And this one can be simplified to the third solution.
 */
class Solution {
public:
    double largestSumOfAverages(vector<int>& A, int K) {
        if (A.empty() || K == 0) return 0;

        vector<vector<double>> dp(K+1, vector<double>(A.size(), 0));
        vector<double> sum(A.size(), 0);

        partial_sum(A.begin(), A.end(), sum.begin());

        for (int k = 1; k <= K; k++){
            for (int i = k-1; i < A.size(); i++){
                if (k == 1) {
                    dp[k][i] = sum[i] / (i+1);
                }
                else{
                    for(int j = k-2 ; j < i; j++){
                        dp[k][i] = max(dp[k-1][j] + (sum[i] - sum[j]) / (i - j), dp[k][i]);
                    }
                }
            }
        }
        return dp[K][A.size()-1];
    }
};

/**
 * More understandable dfs solution. Faster than the dp solution. (at least from leetcode judgement)
 */
class Solution {
public:
    double m[101][101] = {};

    double largestSumOfAverages(vector<int>& A, int K, int p = 0) {
      if (p >= A.size() || m[K][p] > 0) return m[K][p];

      for (double i = p, sum = 0; i <= A.size() - K; ++i) {
          sum += A[i];
          if (K == 1 && i < A.size() - 1) continue;
          m[K][p] = max(m[K][p], sum / (i - p + 1) + largestSumOfAverages(A, K - 1, i + 1));
      }
      return m[K][p];
    }
};

/**
 * Damn dp problem. Do not come up a solution. The below approach is from leetcode.
 */
class Solution {
public:
    double largestSumOfAverages(vector<int>& A, int K) {
        int N = A.size();
        vector<double> dp(N, 0);
        vector<double> P(N + 1, 0);

        partial_sum(A.begin(), A.end(), next(P.begin()));

        for (size_t i = 0; i < N; ++i) {
            dp[i] = (P[N] - P[i]) / (N - i);
        }

        for (int k = 0; k < K - 1; ++k) {
            for (int i = 0; i < N; ++i) {
                for (int j = i + 1; j < N; ++j) {
                    dp[i] = max(dp[i], (P[j] - P[i]) / (j - i) + dp[j]);
                }
            }
        }

        return dp[0];
    }
};
