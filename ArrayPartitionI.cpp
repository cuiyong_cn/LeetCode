/**
 * Array Partition I
 *
 * Given an array of 2n integers, your task is to group these integers into n pairs
 * of integer, say (a1, b1), (a2, b2), ..., (an, bn) which makes sum of min(ai, bi)
 * for all i from 1 to n as large as possible.
 *
 * Example 1:
 *
 * Input: [1,4,3,2]
 *
 * Output: 4
 * Explanation: n is 2, and the maximum sum of pairs is 4 = min(1, 2) + min(3, 4).
 *
 * Note:
 *   1. n is a positive integer, which is in the range of [1, 10000].
 *   2. All the integers in the array will be in the range of [-10000, 10000].
 */
/**
 * class Solution {
 * public:
 *     int arrayPairSum(vector<int>& nums) {
 *         vector<int> hashtable(20001,0);
 *         for (auto& n : nums) {
 *             ++hashtable[n + 10000];
 *         }
 *         int ret = 0;
 *         bool flag = true;
 *         for(size_t i = 0; i < 20001;){
 *             if(hashtable[i] > 0) {
 *                 if (flag) {
 *                     flag = false;
 *                     ret += (i - 10000);
 *                 }
 *                 else {
 *                     flag = true;
 *                 }
 *                 --hashtable[i];
 *             }
 *             else {
 *                 ++i;
 *             }
 *         }
 *         return ret;
 *     }
 * };
 */

/**
 * simple one. But because the sort algorithm is not the fastest one, so the time
 * can be improved. See the above one.
 */
class Solution {
public:
    int arrayPairSum(vector<int>& nums) {
        sort(nums.begin(), nums.end());
        int sum = 0;
        for (int i = 0; i < nums.size() - 1; i+=2) {
            sum += nums[i];
        }

        return sum;
    }
};
