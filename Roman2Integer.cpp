/**
 * Roman to Integer
 *
 * Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.
 *
 * Symbol       Value
 * I             1
 * V             5
 * X             10
 * L             50
 * C             100
 * D             500
 * M             1000
 *
 * For example, two is written as II in Roman numeral, just two one's added together. Twelve is written as, XII, which
 * is simply X + II. The number twenty seven is written as XXVII, which is XX + V + II.
 *
 * Roman numerals are usually written largest to smallest from left to right. However, the numeral for four is not IIII.
 * Instead, the number four is written as IV. Because the one is before the five we subtract it making four. The same
 * principle applies to the number nine, which is written as IX. There are six instances where subtraction is used:
 *
 *     I can be placed before V (5) and X (10) to make 4 and 9.
 *     X can be placed before L (50) and C (100) to make 40 and 90.
 *     C can be placed before D (500) and M (1000) to make 400 and 900.
 *
 * Given a roman numeral, convert it to an integer. Input is guaranteed to be within the range from 1 to 3999.
 *
 * Example 1:
 *
 * Input: "III"
 * Output: 3
 *
 * Example 2:
 *
 * Input: "IV"
 * Output: 4
 *
 * Example 3:
 *
 * Input: "IX"
 * Output: 9
 *
 * Example 4:
 *
 * Input: "LVIII"
 * Output: 58
 * Explanation: L = 50, V= 5, III = 3.
 *
 * Example 5:
 *
 * Input: "MCMXCIV"
 * Output: 1994
 * Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.
 */
/**
 * More short one.
 */
class Solution {
public:
    int romanToInt(string s) {
        unordered_map<char, int> T = { { 'I' , 1 },
                                       { 'V' , 5 },
                                       { 'X' , 10 },
                                       { 'L' , 50 },
                                       { 'C' , 100 },
                                       { 'D' , 500 },
                                       { 'M' , 1000 } };

       int sum = T[s.back()];
       for (int i = s.length() - 2; i >= 0; --i) {
           if (T[s[i]] < T[s[i + 1]]) {
               sum -= T[s[i]];
           }
           else {
               sum += T[s[i]];
           }
       }

       return sum;
    }
};
/**
 * Original solution.
 */
class Solution {
public:
    int cur;
    int handleI(string& s) {
        ++cur;
        if (cur < s.length()) {
            if ('V' == s[cur]) {
                ++cur;
                return 4;
            }
            else if ('X' == s[cur]) {
                ++cur;
                return 9;
            }
        }
        return 1;
    }
    int handleV(string& s) {
        ++cur;
        return 5;
    }
    int handleX(string& s) {
        ++cur;
        if (cur < s.length()) {
            if ('L' == s[cur]) {
                ++cur;
                return 40;
            }
            else if ('C' == s[cur]) {
                ++cur;
                return 90;
            }
        }
        return 10;
    }
    int handleL(string& s) {
        ++cur;
        return 50;
    }
    int handleC(string& s) {
        ++cur;
        if (cur < s.length()) {
            if ('D' == s[cur]) {
                ++cur;
                return 400;
            }
            else if ('M' == s[cur]) {
                ++cur;
                return 900;
            }
        }
        return 100;
    }
    int handleD(string& s) {
        ++cur;
        return 500;
    }
    int handleM(string& s) {
        ++cur;
        return 1000;
    }

    typedef int (Solution::*handleFunc)(string& s);

    int evaluate(string& s) {
        vector<handleFunc> func(26, nullptr);
        func['I' - 'A'] = &Solution::handleI;
        func['V' - 'A'] = &Solution::handleV;
        func['X' - 'A'] = &Solution::handleX;
        func['L' - 'A'] = &Solution::handleL;
        func['C' - 'A'] = &Solution::handleC;
        func['D' - 'A'] = &Solution::handleD;
        func['M' - 'A'] = &Solution::handleM;

        cur = 0;
        int ans = 0;
        while (cur < s.length()) {
            handleFunc f = func[s[cur] - 'A'];
            if (nullptr != f) ans += (this->*f)(s);
        }
        return ans;
    }

    int romanToInt(string s) {
        return evaluate(s);
    }
};
