/**
 * Decremental String Concatenation
 *
 * You are given a 0-indexed array words containing n strings.
 *
 * Let's define a join operation join(x, y) between two strings x and y as
 * concatenating them into xy. However, if the last character of x is equal to
 * the first character of y, one of them is deleted.
 *
 * For example join("ab", "ba") = "aba" and join("ab", "cde") = "abcde".
 *
 * You are to perform n - 1 join operations. Let str0 = words[0]. Starting from
 * i = 1 up to i = n - 1, for the ith operation, you can do one of the
 * following:
 *
 * Make stri = join(stri - 1, words[i])
 * Make stri = join(words[i], stri - 1)
 * Your task is to minimize the length of strn - 1.
 *
 * Return an integer denoting the minimum possible length of strn - 1.
 *
 * Example 1:
 *
 * Input: words = ["aa","ab","bc"]
 * Output: 4
 * Explanation: In this example, we can perform join operations in the following order to minimize the length of str2:
 * str0 = "aa"
 * str1 = join(str0, "ab") = "aab"
 * str2 = join(str1, "bc") = "aabc"
 * It can be shown that the minimum possible length of str2 is 4.
 * Example 2:
 *
 * Input: words = ["ab","b"]
 * Output: 2
 * Explanation: In this example, str0 = "ab", there are two ways to get str1:
 * join(str0, "b") = "ab" or join("b", str0) = "bab".
 * The first string, "ab", has the minimum length. Hence, the answer is 2.
 * Example 3:
 *
 * Input: words = ["aaa","c","aba"]
 * Output: 6
 * Explanation: In this example, we can perform join operations in the following order to minimize the length of str2:
 * str0 = "aaa"
 * str1 = join(str0, "c") = "aaac"
 * str2 = join("aba", str1) = "abaaac"
 * It can be shown that the minimum possible length of str2 is 6.
 *
 * Constraints:
 *
 * 1 <= words.length <= 1000
 * 1 <= words[i].length <= 50
 * Each character in words[i] is an English lowercase letter
 */
/**
 * Original solution.
 */
int indice(char c)
{
    return c - 'a';
}

class Solution {
public:
    int minimizeConcatenatedLength(vector<string>& words) {
        int const wcnt = words.size();

        if (1 == wcnt) {
            return words[0].size();
        }

        auto const int_max = numeric_limits<int>::max();
        int dp[1001][26][26] = { 0, };
        int res = 0;

        dp[0][indice(words[0].front())][indice(words[0].back())] = words[0].size();

        for (int i = 1; i < wcnt; ++i) {
            res = int_max;
            auto L = indice(words[i].front());
            auto R = indice(words[i].back());
            int len = words[i].size();

            for (int j = 0; j < 26; ++j) {
                for (int k = 0; k < 26; ++k) {
                    if (dp[i - 1][j][k]) {
                        auto& x = dp[i][L][k];

                        if (0 == x) {
                            x = int_max;
                        }

                        x = min(x, dp[i-1][j][k] + len - (R == j));
                        res = min(res, x);
                    }

                    if (dp[i - 1][j][k]) {
                        auto& x = dp[i][j][R];

                        if (0 == x) {
                            x = int_max;
                        }

                        x = min(x, dp[i-1][j][k] + len - (k == L));
                        res = min(res, x);
                    }
                }
            }
        }

        return res;
    }
};
