/**
 * Minimum Difference Between Largest and Smallest Value in Three Moves
 *
 * You are given an integer array nums. In one move, you can choose one element of nums and change
 * it by any value.
 *
 * Return the minimum difference between the largest and smallest value of nums after performing at
 * most three moves.
 *
 * Example 1:
 *
 * Input: nums = [5,3,2,4]
 * Output: 0
 * Explanation: Change the array [5,3,2,4] to [2,2,2,2].
 * The difference between the maximum and minimum is 2-2 = 0.
 * Example 2:
 *
 * Input: nums = [1,5,0,10,14]
 * Output: 1
 * Explanation: Change the array [1,5,0,10,14] to [1,1,0,1,1].
 * The difference between the maximum and minimum is 1-0 = 1.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 105
 * -109 <= nums[i] <= 109
 */
/**
 * Sliding window solution.
 */
class Solution {
public:
    using VI = vector<int>;
    static constexpr int INF = 2e9 + 7;
    int minDifference(VI& A, int min = INF) {
        sort(A.begin(), A.end());
        int N = A.size(), i = 0, j = N - 4;
        while (0 <= j && j < N)
			min = std::min(min, A[j++] - A[i++]); // slide window by 3 👉
        return min < INF ? min : 0;
    }
};

/**
 * More efficient only for this one. Idea is:
 * We can do the following moves,
 * 1. remove 3 largest numbers
 * 2. remove 2 largest numbers, and 1 smallest
 * 3. remove 1 largest numbers, and 2 smallest
 * 4. remove 3 smallest
 */
class Solution {
public:
    int minDifference(vector<int>& A) {
        int n = A.size();
        if (n < 5)
            return 0;
        partial_sort(A.begin(), A.begin() + 4, A.end());
        nth_element(A.begin() + 4, A.end() - 4, A.end());
        sort(A.end() - 4, A.end());
        return min({A[n - 1] - A[3], A[n - 2] - A[2], A[n - 3] - A[1], A[n - 4] - A[0]});
    }
};

/**
 * Original solution. This is a general solution.
 */
class Solution {
public:
    int minDifference(vector<int>& nums) {
        sort(nums.begin(), nums.end());

        int ans = nums.back() - nums.front();
        deque<pair<int, int>> q = {{0, nums.size() - 1}};
        for (int k = 0; k < 3; ++k) {
            int const N = q.size();
            for (int i = 0; i < N; ++i) {
                auto [L, R] = q.front(); q.pop_front();
                if (L < R) {
                    auto diff_remove_right = nums[R - 1] - nums[L];
                    auto diff_remove_left = nums[R] - nums[L + 1];
                    q.emplace_back(L + 1, R);
                    q.emplace_back(L, R - 1);
                    ans = min(ans, min(diff_remove_right, diff_remove_left));
                }
            }
        }

        return ans;
    }
};
