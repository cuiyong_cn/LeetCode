/**
 * Double a Number Represented as a Linked List
 *
 * You are given the head of a non-empty linked list representing a non-negative integer without leading zeroes.
 *
 * Return the head of the linked list after doubling it.
 *
 * Example 1:
 *
 * Input: head = [1,8,9]
 * Output: [3,7,8]
 * Explanation: The figure above corresponds to the given linked list which represents the number 189. Hence, the returned linked list represents the number 189 * 2 = 378.
 *
 * Example 2:
 *
 * Input: head = [9,9,9]
 * Output: [1,9,9,8]
 * Explanation: The figure above corresponds to the given linked list which represents the number 999. Hence, the returned linked list reprersents the number 999 * 2 = 1998.
 *
 * Constraints:
 *
 *     The number of nodes in the list is in the range [1, 104]
 *     0 <= Node.val <= 9
 *     The input is generated such that the list represents a number that does not have leading zeros, except the number 0 itself.
 */
/**
 * Original solution. This problem can be solve without reverse. Check if next node's val > 4.
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
auto reverse(ListNode* n)
{
    ListNode* prev = nullptr;

    while (n) {
        auto next = n->next;
        n->next = prev;
        prev = n;
        n = next;
    }

    return prev;
}

class Solution {
public:
    ListNode* doubleIt(ListNode* head) {
        auto rev_head = reverse(head);
        auto carry = 0;

        head = rev_head;

        while (rev_head) {
            auto dval = rev_head->val * 2 + carry;
            carry = dval > 9;
            rev_head->val = carry ? dval - 10 : dval;

            if (!rev_head->next) {
                break;
            }

            rev_head = rev_head->next;
        }

        if (carry) {
            auto node = new ListNode(1);
            rev_head->next = node;
            rev_head = node;
        }

        return reverse(head);
    }
};
