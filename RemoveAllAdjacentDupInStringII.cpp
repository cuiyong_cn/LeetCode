/**
 * Remove All Adjacent Duplicates in String II
 *
 * Given a string s, a k duplicate removal consists of choosing k adjacent and equal letters from s and removing them
 * causing the left and the right side of the deleted substring to concatenate together.
 *
 * We repeatedly make k duplicate removals on s until we no longer can.
 *
 * Return the final string after all such duplicate removals have been made.
 *
 * It is guaranteed that the answer is unique.
 *
 * Example 1:
 *
 * Input: s = "abcd", k = 2
 * Output: "abcd"
 * Explanation: There's nothing to delete.
 *
 * Example 2:
 *
 * Input: s = "deeedbbcccbdaa", k = 3
 * Output: "aa"
 * Explanation:
 * First delete "eee" and "ccc", get "ddbbbdaa"
 * Then delete "bbb", get "dddaa"
 * Finally delete "ddd", get "aa"
 *
 * Example 3:
 *
 * Input: s = "pbbcggttciiippooaais", k = 2
 * Output: "ps"
 *
 * Constraints:
 *
 *     1 <= s.length <= 10^5
 *     2 <= k <= 10^4
 *     s only contains lower case English letters.
 */
/**
 * Non-stack version. more efficient and clearer.
 */
class Solution {
public:
    string removeDuplicates(string s, int k) {
        int i = 0, n = s.length();
        vector<int> count(n);

        for (int j = 0; j < n; ++j, ++i) {
            s[i] = s[j];
            if (i > 0 && s[i - 1] == s[i]) {
                count[i] = count[i - 1] + 1;
            }
            else {
                count[i] = 1;
            }

            if (count[i] == k) {
                i -= k;
            }
        }
        return s.substr(0, i);
    }
};

/**
 * Improved stack version.
 */
class Solution {
public:
    string removeDuplicates(string s, int k) {
        vector<pair<int, char>> stack = {{0, '#'}};
        for (char c: s) {
            if (stack.back().second != c) {
                stack.push_back({1, c});
            } else if (++stack.back().first == k) {
                stack.pop_back();
            }
        }
        string res;
        for (auto & p : stack) {
            res += string(p.first, p.second);
        }
        return res;
    }
};

/**
 * My original solution.
 */
class Solution {
public:
    string removeDuplicates(string s, int k) {
        vector<pair<char, int> > stack;
        for (int i = 0; i < s.size(); ++i) {
            int r = 1;
            if (false == stack.empty()) {
                auto& p = stack.back();
                if (p.first == s[i]) {
                    r = p.second + 1;
                }
            }

            if (r == k) {
                while (r > 1) {
                    stack.pop_back();
                    --r;
                }
            }
            else {
                stack.push_back({s[i], r});
            }
        }
        string ans;
        for (auto& p : stack) {
            ans += p.first;
        }
        return ans;
    }
};

/**
 * Brute force. Time consuming and memory consuming.
 */
class Solution {
public:
    string removeDuplicates(string s, int k) {
        for (auto i = 1, cnt = 1; i < s.size(); ++i) {
            if (s[i] != s[i - 1]) {
                cnt = 1;
            }
            else if (++cnt == k) {
              return removeDuplicates(s.substr(0, i - k + 1) + s.substr(i + 1), k);
            }
        }
        return s;
    }
};
