/**
 * Find if Path Exists in Graph
 *
 * There is a bi-directional graph with n vertices, where each vertex is labeled from 0 to n - 1
 * (inclusive). The edges in the graph are represented as a 2D integer array edges, where each
 * edges[i] = [ui, vi] denotes a bi-directional edge between vertex ui and vertex vi. Every vertex
 * pair is connected by at most one edge, and no vertex has an edge to itself.
 *
 * You want to determine if there is a valid path that exists from vertex source to vertex
 * destination.
 *
 * Given edges and the integers n, source, and destination, return true if there is a valid path
 * from source to destination, or false otherwise.
 *
 * Example 1:
 *
 * Input: n = 3, edges = [[0,1],[1,2],[2,0]], source = 0, destination = 2
 * Output: true
 * Explanation: There are two paths from vertex 0 to vertex 2:
 * - 0 → 1 → 2
 * - 0 → 2
 * Example 2:
 *
 * Input: n = 6, edges = [[0,1],[0,2],[3,5],[5,4],[4,3]], source = 0, destination = 5
 * Output: false
 * Explanation: There is no path from vertex 0 to vertex 5.
 *
 * Constraints:
 *
 * 1 <= n <= 2 * 105
 * 0 <= edges.length <= 2 * 105
 * edges[i].length == 2
 * 0 <= ui, vi <= n - 1
 * ui != vi
 * 0 <= source, destination <= n - 1
 * There are no duplicate edges.
 * There are no self edges.
 */
/**
 * Probably more efficient. Union find solution.
 */
class Solution {
public:
    vector<int> parent;
    int findParent(int node) {
        return parent[node] == node ? node : (parent[node] = findParent(parent[node]));
    }
    void makeSameGroup(int u , int v) {
        int pu = findParent(u) ;
        int pv = findParent(v);
        parent[pu] = pv;
    }

    bool validPath(int n, vector<vector<int>>& edges, int start, int end) {
        parent.resize(n);
        iota(parent.begin(), parent.end(), 0);

        for(auto e : edges) {
            makeSameGroup(e[0] , e[1]);
        }
        return findParent(start) == findParent(end);
    }
};

/**
 * Original solution. BFS.
 */
class Solution {
public:
    bool validPath(int n, vector<vector<int>>& edges, int source, int destination) {
        unordered_map<int, vector<int>> eg;

        for (auto const& e : edges) {
            eg[e[0]].emplace_back(e[1]);
            eg[e[1]].emplace_back(e[0]);
        }

        unordered_set<int> visited;
        deque<int> dq{source};

        while (!dq.empty()) {
            int const qsize = dq.size();
            for (int i = 0; i < qsize; ++i) {
                auto const u = dq.front();
                dq.pop_front();

                if (u == destination) {
                    return true;
                }

                if (visited.count(u)) {
                    continue;
                }

                visited.insert(u);
                dq.insert(dq.end(), eg[u].begin(), eg[u].end());
            }
        }

        return false;
    }
};
