/**
 * Goat Latin
 *
 * A sentence S is given, composed of words separated by spaces. Each word consists of
 * lowercase and uppercase letters only.
 *
 * We would like to convert the sentence to "Goat Latin" (a made-up language similar to
 * Pig Latin.)
 *
 * The rules of Goat Latin are as follows:
 *
 *     If a word begins with a vowel (a, e, i, o, or u), append "ma" to the end of the
 *     word. For example, the word 'apple' becomes 'applema'.
 *
 *     If a word begins with a consonant (i.e. not a vowel), remove the first letter and
 *     append it to the end, then add "ma". For example, the word "goat" becomes "oatgma".
 *
 *     Add one letter 'a' to the end of each word per its word index in the sentence,
 *     starting with 1. For example, the first word gets "a" added to the end, the second
 *     word gets "aa" added to the end and so on.
 *
 * Return the final sentence representing the conversion from S to Goat Latin.
 *
 * Example 1:
 *
 * Input: "I speak Goat Latin"
 * Output: "Imaa peaksmaaa oatGmaaaa atinLmaaaaa"
 *
 * Example 2:
 *
 * Input: "The quick brown fox jumped over the lazy dog"
 * Output: "heTmaa uickqmaaa rownbmaaaa oxfmaaaaa umpedjmaaaaaa overmaaaaaaa hetmaaaaaaaa
 *          azylmaaaaaaaaa ogdmaaaaaaaaaa"
 *
 * Notes:
 *
 *     S contains only uppercase, lowercase and spaces. Exactly one space between each word.
 *     1 <= S.length <= 150.
 */
/**
 * My original version, not very satisfactory.
 */
class Solution {
private:
    void processWord(string& ans, string& S, int start, int end)
    {
        const string vowels{"aeiou"};
        if (vowels.find_first_of(tolower(S[start])) != string::npos) {
            ans = ans + S.substr(start, end - start);
        }
        else {
            ans = ans + S.substr(start + 1, end - start - 1) + S[start];
        }
    }
public:
    string toGoatLatin(string S) {
        string ans;
        string na{"maa"};

        int start = 0;
        for (int i = 0; i < S.size(); ++i) {
            if (' ' == S[i]) {
                processWord(ans, S, start, i);
                ans = ans + na + " ";
                na.push_back('a');
                start = i + 1;
            }
        }

        processWord(ans, S, start, S.size());
        ans = ans + na;
        return ans;
    }
};

/**
 * Using stringstream, more clean one
 */
class Solution {
public:
    string toGoatLatin(string S) {
        stringstream iss(S);
        stringstream oss;
        string vowels("aeiou");
        string word;
        string na{"ma"};
        while (iss >> word) {
            na.push_back('a');
            if (vowels.find_first_of(tolower(word[0])) != string::npos)
                oss << ' ' << word << na;
            else
                oss << ' ' << word.substr(1) << word[0] << na;
        }
        return oss.str().substr(1);
    }
};
