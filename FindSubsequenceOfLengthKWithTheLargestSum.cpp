/**
 * Find Subsequence of Length K With the Largest Sum
 *
 * You are given an integer array nums and an integer k. You want to find a subsequence of nums of
 * length k that has the largest sum.
 *
 * Return any such subsequence as an integer array of length k.
 *
 * A subsequence is an array that can be derived from another array by deleting some or no elements
 * without changing the order of the remaining elements.
 *
 * Example 1:
 *
 * Input: nums = [2,1,3,3], k = 2
 * Output: [3,3]
 * Explanation:
 * The subsequence has the largest sum of 3 + 3 = 6.
 * Example 2:
 *
 * Input: nums = [-1,-2,3,4], k = 3
 * Output: [-1,3,4]
 * Explanation:
 * The subsequence has the largest sum of -1 + 3 + 4 = 6.
 * Example 3:
 *
 * Input: nums = [3,4,3,3], k = 2
 * Output: [3,4]
 * Explanation:
 * The subsequence has the largest sum of 3 + 4 = 7.
 * Another possible subsequence is [4, 3].
 *
 * Constraints:
 *
 * 1 <= nums.length <= 1000
 * -105 <= nums[i] <= 105
 * 1 <= k <= nums.length*
 */
/**
 * Take advantage of nth_element.
 */
class Solution {
public:
    vector<int> maxSubsequence(vector<int>& nums, int k) {
        if (k == nums.size())
            return nums;
        vector<int> v(begin(nums), end(nums)), res;
        nth_element(begin(v), begin(v) + k - 1, end(v), greater<int>());
        int cnt = count(begin(v), begin(v) + k, v[k - 1]);
        for (int i = 0; i < nums.size(); ++i)
            if (nums[i] > v[k - 1] || (nums[i] == v[k - 1] && --cnt >= 0))
                res.push_back(nums[i]);
        return res;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<int> maxSubsequence(vector<int>& nums, int k) {
        auto nsize = static_cast<int>(nums.size());
        auto indexes = vector<int>(nsize, 0);

        iota(indexes.begin(), indexes.end(), 0);

        sort(indexes.begin(), indexes.end(),
            [&nums](auto const& i, auto const& j) {
                return nums[i] < nums[j];
            });

        auto ans = vector<int>(indexes.end() - k, indexes.end());

        sort(ans.begin(), ans.end());
        transform(ans.begin(), ans.end(), ans.begin(), [&nums](auto const& i) { return nums[i]; });

        return ans;
    }
};
