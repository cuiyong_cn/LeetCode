/**
 * Sum of Subarray Minimums
 *
 * Given an array of integers arr, find the sum of min(b), where b ranges over every (contiguous)
 * subarray of arr. Since the answer may be large, return the answer modulo 109 + 7.
 *
 * Example 1:
 *
 * Input: arr = [3,1,2,4]
 * Output: 17
 * Explanation:
 * Subarrays are [3], [1], [2], [4], [3,1], [1,2], [2,4], [3,1,2], [1,2,4], [3,1,2,4].
 * Minimums are 3, 1, 2, 4, 1, 1, 2, 1, 1, 1.
 * Sum is 17.
 * Example 2:
 *
 * Input: arr = [11,81,94,43,3]
 * Output: 444
 *
 * Constraints:
 *
 * 1 <= arr.length <= 3 * 104
 * 1 <= arr[i] <= 3 * 104
 */
/**
 * Based on the below three, it seems that using "bitwise or of subarray" solution is a dead end.
 * We must use other method. And it is stack solution (new to me)
 */
class Solution {
public:
    int sumSubarrayMins(vector<int>& A) {
        stack<pair<int, int>> in_stk_p, in_stk_n;
        // left is for the distance to previous less element
        // right is for the distance to next less element
        vector<int> left(A.size()), right(A.size());

        //initialize
        for (int i = 0; i < A.size(); i++) {
            left[i] =  i + 1;
        }

        for (int i = 0; i < A.size(); i++) {
            right[i] = A.size() - i;
        }

        for(int i = 0; i < A.size(); i++){
            // for previous less
            while (!in_stk_p.empty() && in_stk_p.top().first > A[i]) {
                in_stk_p.pop();
            }

            left[i] = in_stk_p.empty()? i + 1 : i - in_stk_p.top().second;
            in_stk_p.push({A[i],i});

            // for next less
            while (!in_stk_n.empty() && in_stk_n.top().first > A[i]) {
                auto x = in_stk_n.top();
                in_stk_n.pop();
                right[x.second] = i - x.second;
            }

            in_stk_n.push({A[i], i});
        }

        int64_t ans = 0;
        int const mod = 1e9 +7;
        for (int i = 0; i < A.size(); ++i) {
            ans = (ans + static_cast<int64_t>(A[i]) * left[i] * right[i]) % mod;
        }
        return ans;
    }
};

/**
 * Optimize idea: there maybe lots of same numbers. Still TLE.
 */
class Solution {
public:
    int sumSubarrayMins(vector<int>& arr) {
        int ans = 0;
        int const mod = 1e9 + 7;
        unordered_map<int, int> cur;

        for (auto n1 : arr) {
            int sum = n1;
            unordered_map<int, int> next = {{n1, 1}};
            for (auto const& n2 : cur) {
                int m = min(n1, n2.first);
                next[m] += n2.second;
                sum += m * n2.second;
            }

            ans = (ans + sum) % mod;
            swap(cur, next);
        }

        return ans;
    }
};

/**
 * Optimize idea: using only one vector instead of two, reduce the mem alloation times. Still TLE.
 */
class Solution {
public:
    int sumSubarrayMins(vector<int>& arr) {
        int ans = 0;
        int const mod = 1e9 + 7;
        vector<int> cur;
        for (auto n1 : arr) {
            int sum = n1;
            for (auto& n2 : cur) {
                n2 = min(n1, n2);
                sum += n2;
            }
            cur.push_back(n1);

            ans = (ans + sum) % mod;
        }

        return ans;
    }
};

/**
 * Original solution. TLE. This is actually the same as "bitwise or of subarrays"
 */
class Solution {
public:
    int sumSubarrayMins(vector<int>& arr) {
        int ans = 0;
        int const mod = 1e9 + 7;
        vector<int> cur;
        for (auto n1 : arr) {
            vector<int> next = {n1};
            for (auto n2 : cur) {
                next.push_back(min(n1, n2));
            }

            auto sum = accumulate(next.begin(), next.end(), 0);
            ans = (ans + sum) % mod;
            swap(next, cur);
        }

        return ans;
    }
};
