/**
 * Binary Tree Zigzag Level Order Traversal
 *
 * Given a binary tree, return the zigzag level order traversal of its nodes' values. (ie, from left to right, then
 * right to left for the next level and alternate between).
 *
 * For example:
 * Given binary tree [3,9,20,null,null,15,7],
 *     3
 *    / \
 *   9  20
 *     /  \
 *    15   7
 * return its zigzag level order traversal as:
 * [
 *   [3],
 *   [20,9],
 *   [15,7]
 * ]
 */
/**
 * Original solution. Also we can do this without the reverse.
 * For left to right, we pop front, push_back
 * For right to left, we pop_back, push_front()
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
        if (nullptr == root) {
            return {};
        }

        vector<vector<int>> ans;
        deque<TreeNode*> q{root};
        bool left_to_right = true;

        while (!q.empty()) {
            vector<int> cur_level;
            size_t N = q.size();
            for (size_t i = 0; i < N; ++i) {
                auto node = q.front();
                q.pop_front();
                cur_level.push_back(node->val);

                if (node->left) {
                    q.push_back(node->left);
                }
                if (node->right) {
                    q.push_back(node->right);
                }
            }

            if (!left_to_right) {
                std::reverse(cur_level.begin(), cur_level.end());
            }

            ans.push_back(cur_level);
            left_to_right = !left_to_right;
        }

        return ans;
    }
};
