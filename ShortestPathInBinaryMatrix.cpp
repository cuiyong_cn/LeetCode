/**
 * Shortest Path in Binary Matrix
 *
 * Given an n x n binary matrix grid, return the length of the shortest clear path in the matrix. If
 * there is no clear path, return -1.
 *
 * A clear path in a binary matrix is a path from the top-left cell (i.e., (0, 0)) to the
 * bottom-right cell (i.e., (n - 1, n - 1)) such that:
 *
 * All the visited cells of the path are 0.  All the adjacent cells of the path are 8-directionally
 * connected (i.e., they are different and they share an edge or a corner).  The length of a clear
 * path is the number of visited cells of this path.
 *
 * Example 1:
 *
 * Input: grid = [[0,1],[1,0]]
 * Output: 2
 * Example 2:
 *
 * Input: grid = [[0,0,0],[1,1,0],[1,1,0]]
 * Output: 4
 * Example 3:
 *
 * Input: grid = [[1,0,0],[1,1,0],[1,1,0]]
 * Output: -1
 *
 * Constraints:
 *
 * n == grid.length
 * n == grid[i].length
 * 1 <= n <= 100
 * grid[i][j] is 0 or 1
 */
/**
 * Original solution. BFS solution.
 */
class Solution {
public:
    int shortestPathBinaryMatrix(vector<vector<int>>& grid) {
        if (1 == grid[0][0] || 1 == grid.back().back()) {
            return -1;
        }

        vector<pair<int, int>> queue{ {0, 0} };
        vector<pair<int, int>> const directions{
            {0, -1}, {-1, -1}, {-1, 0}, {-1, 1},
            {0, 1}, {1, 1}, {1, 0}, {1, -1}
        };

        int n = grid.size();
        int destx = n - 1;
        int desty = destx;
        int distance = 1;

        grid[0][0] = 1;
        while (!queue.empty()) {
            vector<pair<int, int>> next_round;

            for (auto const& p : queue) {
                if (p.first == destx && p.second == desty) {
                    return distance;
                }

                for (auto const& d : directions) {
                    auto x = p.first + d.first;
                    auto y = p.second + d.second;
                    if (0 <= x && x < n && 0 <= y && y < n) {
                        if (0 == grid[x][y]) {
                            next_round.emplace_back(make_pair(x, y));
                            grid[x][y] = 1;
                        }
                    }
                }
            }

            ++distance;
            swap(queue, next_round);
        }

        return -1;
    }
};
