/**
 * Maximum Score from Performing Multiplication Operations
 *
 * You are given two integer arrays nums and multipliers of size n and m respectively, where n >= m.
 * The arrays are 1-indexed.
 *
 * You begin with a score of 0. You want to perform exactly m operations. On the ith operation
 * (1-indexed), you will:
 *
 * Choose one integer x from either the start or the end of the array nums.
 * Add multipliers[i] * x to your score.
 * Remove x from the array nums.
 * Return the maximum score after performing m operations.
 *
 * Example 1:
 *
 * Input: nums = [1,2,3], multipliers = [3,2,1]
 * Output: 14
 * Explanation: An optimal solution is as follows:
 * - Choose from the end, [1,2,3], adding 3 * 3 = 9 to the score.
 * - Choose from the end, [1,2], adding 2 * 2 = 4 to the score.
 * - Choose from the end, [1], adding 1 * 1 = 1 to the score.
 * The total score is 9 + 4 + 1 = 14.
 * Example 2:
 *
 * Input: nums = [-5,-3,-3,-2,7,1], multipliers = [-10,-5,3,4,6]
 * Output: 102
 * Explanation: An optimal solution is as follows:
 * - Choose from the start, [-5,-3,-3,-2,7,1], adding -5 * -10 = 50 to the score.
 * - Choose from the start, [-3,-3,-2,7,1], adding -3 * -5 = 15 to the score.
 * - Choose from the start, [-3,-2,7,1], adding -3 * 3 = -9 to the score.
 * - Choose from the end, [-2,7,1], adding 1 * 4 = 4 to the score.
 * - Choose from the end, [-2,7], adding 7 * 6 = 42 to the score.
 * The total score is 50 + 15 - 9 + 4 + 42 = 102.
 *
 * Constraints:
 *
 * n == nums.length
 * m == multipliers.length
 * 1 <= m <= 103
 * m <= n <= 105
 * -1000 <= nums[i], multipliers[i] <= 1000
 */
/**
 * Seems the OJ only count the heap memory.
 */
class Solution {
public:
    int maximumScore(vector<int>& nums, vector<int>& multipliers) {
        return dfs(nums, multipliers, 0, 0);
    }

private:
    int dfs(vector<int> const& nums, vector<int> const& multipliers, int const left, int const i) {
        if (i == multipliers.size()) {
            return 0;
        }

        if (0 == visited[left][i]) {
            auto const right = nums.size() - 1 - (i - left);
            memo[left][i] = max(nums[left] * multipliers[i] + dfs(nums, multipliers, left + 1, i + 1),
                               nums[right] * multipliers[i] + dfs(nums, multipliers, left, i + 1));
            visited[left][i] = true;
        }

        return memo[left][i];
    }

private:
    int memo[1001][1001] = { 0, };
    bool visited[1001][1001] = { false, };
};

/**
 * Using hash map will trigger TLE.
 */
class Solution {
public:
    int maximumScore(vector<int>& nums, vector<int>& multipliers) {
        return dfs(nums, multipliers, 0, 0);
    }

private:
    int dfs(vector<int> const& nums, vector<int> const& multipliers, int const left, int const i) {
        if (i == multipliers.size()) {
            return 0;
        }

        int hash = (left << 10) | i;
        if (0 == memo.count(hash)) {
            auto const right = nums.size() - 1 - (i - left);
            auto score = max(nums[left] * multipliers[i] + dfs(nums, multipliers, left + 1, i + 1),
                               nums[right] * multipliers[i] + dfs(nums, multipliers, left, i + 1));
            memo[hash] = score;
        }

        return memo[hash];
    }

private:
    unordered_map<int, int> memo;
};

/**
 * Original solution. But got memory limit exceeded.
 */
class Solution {
public:
    int maximumScore(vector<int>& nums, vector<int>& multipliers) {
        int const N = multipliers.size();
        memo = vector<vector<int>>(N, vector<int>(N, 0));

        return dfs(nums, multipliers, 0, 0);
    }

private:
    int dfs(vector<int> const& nums, vector<int> const& multipliers, int const left, int const i) {
        if (i == multipliers.size()) {
            return 0;
        }

        if (0 == memo[left][i]) {
            auto const right = nums.size() - 1 - (i - left);
            memo[left][i] = max(nums[left] * multipliers[i] + dfs(nums, multipliers, left + 1, i + 1),
                               nums[right] * multipliers[i] + dfs(nums, multipliers, left, i + 1));
        }

        return memo[left][i];
    }

private:
    vector<vector<int>> memo;
};
