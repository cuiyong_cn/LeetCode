/**
 * Can Make Palindrome from Substring
 *
 * You are given a string s and array queries where queries[i] = [lefti, righti, ki]. We may
 * rearrange the substring s[lefti...righti] for each query and then choose up to ki of them to
 * replace with any lowercase English letter.
 *
 * If the substring is possible to be a palindrome string after the operations above, the result of
 * the query is true. Otherwise, the result is false.
 *
 * Return a boolean array answer where answer[i] is the result of the ith query queries[i].
 *
 * Note that each letter is counted individually for replacement, so if, for example
 * s[lefti...righti] = "aaa", and ki = 2, we can only replace two of the letters. Also, note that no
 * query modifies the initial string s.
 *
 * Example :
 *
 * Input: s = "abcda", queries = [[3,3,0],[1,2,0],[0,3,1],[0,3,2],[0,4,1]]
 * Output: [true,false,false,true,true]
 * Explanation:
 * queries[0]: substring = "d", is palidrome.
 * queries[1]: substring = "bc", is not palidrome.
 * queries[2]: substring = "abcd", is not palidrome after replacing only 1 character.
 * queries[3]: substring = "abcd", could be changed to "abba" which is palidrome. Also this can be
 * changed to "baab" first rearrange it "bacd" then replace "cd" with "ab".
 * queries[4]: substring = "abcda", could be changed to "abcba" which is palidrome.
 * Example 2:
 *
 * Input: s = "lyb", queries = [[0,1,0],[2,2,1]]
 * Output: [false,true]
 *
 * Constraints:
 *
 * 1 <= s.length, queries.length <= 105
 * 0 <= lefti <= righti < s.length
 * 0 <= ki <= s.length
 * s consists of lowercase English letters.
 */
/**
 * Optimization. We only care each letters parity. not count.
 */
class Solution {
public:
    vector<bool> canMakePaliQueries(string s, vector<vector<int>>& queries) {
        int const N = s.size();
        vector<int> ps(N + 1, 0);
        for (int i = 0; i < N; ++i) {
            ps[i + 1] = ps[i] ^ (1 << (s[i] - 'a'));
        }

        vector<bool> r;
        for (auto const& q : queries) {
            int odds = bitset<32>(ps[q[1] + 1] ^ ps[q[0]]).count();
            r.push_back(q[2] >= (odds/2));
        }

        return r;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<bool> canMakePaliQueries(string s, vector<vector<int>>& queries) {
        vector<vector<int>> ch_cnt_prefix(26, vector<int>(s.length() + 1, 0));
        for (int i = 0; i < s.length(); ++i) {
            for (int j = 0; j < 26; ++j) {
                ch_cnt_prefix[j][i + 1] += ch_cnt_prefix[j][i];
            }
            ++ch_cnt_prefix[s[i] - 'a'][i + 1];
        }

        vector<bool> ans(queries.size());
        for (int j = 0; j < queries.size(); ++j) {
            auto const& q = queries[j];
            int odd_cnt = 0;
            for (int i = 0; i < 26; ++i) {
                auto ch_cnt = ch_cnt_prefix[i][q[1] + 1] - ch_cnt_prefix[i][q[0]];
                if (ch_cnt & 1) {
                    ++odd_cnt;
                }
            }

            if ((q[1] - q[0] + 1) & 1) {
                --odd_cnt;
            }

            ans[j] = (odd_cnt / 2) <= q[2];
        }

        return ans;
    }
};
