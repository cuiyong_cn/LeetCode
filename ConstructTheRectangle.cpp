/**
 * Construct the Rectangle
 *
 * For a web developer, it is very important to know how to design a web page's size. So, given a specific rectangular
 * web page’s area, your job by now is to design a rectangular web page, whose length L and width W satisfy the
 * following requirements:
 *
 * 1. The area of the rectangular web page you designed must equal to the given target area.
 * 2. The width W should not be larger than the length L, which means L >= W.
 * 3. The difference between length L and width W should be as small as possible.
 *
 * You need to output the length L and the width W of the web page you designed in sequence.
 *
 * Example:
 *
 * Input: 4
 * Output: [2, 2]
 * Explanation: The target area is 4, and all the possible ways to construct it are [1,4], [2,2], [4,1].
 * But according to requirement 2, [1,4] is illegal; according to requirement 3,  [4,1] is not optimal compared to
 * [2,2]. So the length L is 2, and the width W is 2.
 *
 * Note:
 *     The given area won't exceed 10,000,000 and is a positive integer
 *     The web page's width and length you designed must be positive integers.
 */
/**
 * Simplified version.
 */
class Solution {
public:
    vector<int> constructRectangle(int area) {
        int W = sqrt(area);

        while (area % W) --W;

        return {area / W, W};
    }
};

/**
 * Original solution. Suprisingly it passed?!
 */
bool is_prime(long N)
{
    if (N < 2) return false;
    if (N < 4) return true;
    if ((N & 1) == 0) return false;
    if (N % 3 == 0) return false;

    int curr = 5, s = sqrt(N);

    // Loop to check if any number number is divisible by any other number or not
    while (curr <= s) {
        if (N % curr == 0) return false;
        curr += 2;

        if (N % curr == 0) return false;
        curr += 4;
    }

    return true;
}

class Solution {
public:
    vector<int> constructRectangle(int area) {
        // this one is redundant, but my first thought categorize number into 3 groups
        // 1. prime number
        // 2. sqrt number
        // 3. others
        if (is_prime(area)) return {area, 1};

        int W = sqrt(area);
        if ((W * W) == area) return {W, W};

        while (W > 1) {
            if (area % W == 0) break;
            --W;
        }

        return {area / W, W};
    }
};
