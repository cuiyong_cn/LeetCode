/**
 * Excel Sheet Column Number
 *
 * Given a column title as appear in an Excel sheet, return its corresponding column number.
 *
 * For example:
 *     A -> 1
 *     B -> 2
 *     C -> 3
 *     ...
 *     Z -> 26
 *     AA -> 27
 *     AB -> 28
 *     ...
 * Example 1:
 *
 * Input: "A"
 * Output: 1
 * Example 2:
 *
 * Input: "AB"
 * Output: 28
 * Example 3:
 *
 * Input: "ZY"
 * Output: 701
 */
/**
 * Improved one. Avoid using expensive pow function.
 */
class Solution {
public:
    int titleToNumber(string s) {
        int base = 26;
        int ans = 0;
        for (auto& c : s) {
            ans = ans * base + (c - 'A' + 1);
        }

        return ans;
    }
};

/**
 * Original solution. Basically this is a base 26 to base 10 conversion problem.
 */
class Solution {
public:
    int titleToNumber(string s) {
        int len = s.length();
        int ans = 0;
        for (auto& c : s) {
            int base = pow(26, len - 1);
            int val = c - 'A' + 1;
            ans += base * val;
            --len;
        }

        return ans;
    }
};
