/**
 * Pacific Atlantic Water Flow
 *
 * Given an m x n matrix of non-negative integers representing the height of each unit cell in a
 * continent, the "Pacific ocean" touches the left and top edges of the matrix and the "Atlantic
 * ocean" touches the right and bottom edges.
 *
 * Water can only flow in four directions (up, down, left, or right) from a cell to another one with
 * height equal or lower.
 *
 * Find the list of grid coordinates where water can flow to both the Pacific and Atlantic ocean.
 *
 * Note:
 *
 * The order of returned grid coordinates does not matter.
 * Both m and n are less than 150.
 *
 * Example:
 *
 * Given the following 5x5 matrix:
 *
 *   Pacific ~   ~   ~   ~   ~
 *        ~  1   2   2   3  (5) *
 *        ~  3   2   3  (4) (4) *
 *        ~  2   4  (5)  3   1  *
 *        ~ (6) (7)  1   4   5  *
 *        ~ (5)  1   1   2   4  *
 *           *   *   *   *   * Atlantic
 *
 * Return:
 *
 * [[0, 4], [1, 3], [1, 4], [2, 2], [3, 0], [3, 1], [4, 0]] (positions with parentheses in above
 * matrix).
 */
/**
 * Original solution. Expand each border from its corresponding ocean.
 * Find out the common peak they can reach.
 * Also we can use dfs.
 */
class Solution {
public:
    vector<vector<int>> pacificAtlantic(vector<vector<int>>& matrix) {
        if (matrix.empty() || matrix[0].empty()) {
            return {};
        }

        const int M = matrix.size();
        const int N = matrix[0].size();
        vector<vector<bool>> pac_visited(M, vector<bool>(N, false));
        vector<vector<bool>> atl_visited(M, vector<bool>(N, false));
        deque<pair<int, int>> pacq;
        deque<pair<int, int>> atlq;

        get_pacific_border(pacq, pac_visited);
        get_atlantic_border(atlq, atl_visited);

        climb_up(pacq, pac_visited, matrix);
        climb_up(atlq, atl_visited, matrix);

        vector<vector<int>> ans;
        for (int i = 0; i < M; ++i) {
            for (int j = 0; j < N; ++j) {
                if (pac_visited[i][j] && atl_visited[i][j]) {
                    ans.push_back({i, j});
                }
            }
        }

        return ans;
    }

private:
    void get_pacific_border(deque<pair<int, int>>& q, vector<vector<bool>>& visited)
    {
        const int M = visited.size();
        const int N = visited[0].size();

        for (int i = 0; i < N; ++i) {
            visited[0][i] = true;
            q.push_back({0, i});
        }

        for (int i = 1; i < M; ++i) {
            visited[i][0] = true;
            q.push_back({i, 0});
        }
    }

    void get_atlantic_border(deque<pair<int, int>>& q, vector<vector<bool>>& visited)
    {
        const int M = visited.size();
        const int N = visited[0].size();

        for (int i = 0; i < N; ++i) {
            visited[M - 1][i] = true;
            q.push_back({M - 1, i});
        }

        for (int i = 0; i < M; ++i) {
            visited[i][N - 1] = true;
            q.push_back({i, N - 1});
        }
    }

    void climb_up(deque<pair<int, int>>& q, vector<vector<bool>>& visited, vector<vector<int>> const& matrix)
    {
        const int M = matrix.size();
        const int N = matrix[0].size();

        while (!q.empty()) {
            deque<pair<int, int>> nq;
            while (!q.empty()) {
                auto [r, c] = q.front();
                q.pop_front();
                if ((c + 1) < N && !visited[r][c + 1] && matrix[r][c] <= matrix[r][c + 1]) {
                    visited[r][c + 1] = true;
                    nq.push_back({r, c + 1});
                }
                if ((r + 1) < M && !visited[r + 1][c] && matrix[r][c] <= matrix[r + 1][c]) {
                    visited[r + 1][c] = true;
                    nq.push_back({r + 1, c});
                }
                if ((c - 1) >= 0 && !visited[r][c - 1] && matrix[r][c] <= matrix[r][c - 1]) {
                    visited[r][c - 1] = true;
                    nq.push_back({r, c - 1});
                }
                if ((r - 1) >= 0 && !visited[r - 1][c] && matrix[r][c] <= matrix[r - 1][c]) {
                    visited[r - 1][c] = true;
                    nq.push_back({r - 1, c});
                }
            }

            swap(q, nq);
        }
    }
};
