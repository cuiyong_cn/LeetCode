/**
 * Ugly Number II
 *
 * Write a program to find the n-th ugly number.
 * 
 * Ugly numbers are positive numbers whose prime factors only include 2, 3, 5. 
 * 
 * Example:
 * 
 * Input: n = 10
 * Output: 12
 * Explanation: 1, 2, 3, 4, 5, 6, 8, 9, 10, 12 is the sequence of the first 10 ugly numbers.
 * Note:  
 * 
 * 1 is typically treated as an ugly number.
 * n does not exceed 1690.
 */
/**
 * DP solution.
 */
class Solution {
public:
    int nthUglyNumber(int n) {
        vector<int> ugly = {1};
        int i = 0, j = 0, k = 0;
        for (; n > 1; --n) {
            auto u2 = ugly[i] * 2;
            auto u3 = ugly[j] * 3;
            auto u5 = ugly[k] * 5;
            auto next = std::min(u2, std::min(u3, u5));
            ugly.push_back(next);
            i += u2 == next;
            j += u3 == next;
            k += u5 == next;
        }

        return ugly.back();
    }
};

/**
 * Original solution. Will hit the timing wall.
 */
vector<int> g_ugly_num = {1};

class Solution {
public:
    int nthUglyNumber(int n) {
        if (n > g_ugly_num.size()) {
            int num = g_ugly_num.back() + 1;
            for (int i = g_ugly_num.size() + 1; i <= n; ++i) {
                while (num) {
                    if (isUgly(num)) {
                        break;
                    }
                    ++num;
                }

                g_ugly_num.push_back(num);
                ++num;
            }
        }

        return g_ugly_num[n - 1];
    }

private:
    bool isUgly(int num)
    {
        while (num > 6) {
            if (0 == (num % 2)) {
                num = num / 2;
            }
            else if (0 == (num % 3)) {
                num = num / 3;
            }
            else if (0 == (num % 5)) {
                num = num / 5;
            }
            else {
                return false;
            }
        }

        return true;
    }
};
