/**
 * Number of Subarrays with Bounded Maximum
 *
 * We are given an array A of positive integers, and two positive integers L and R (L <= R).
 *
 * Return the number of (contiguous, non-empty) subarrays such that the value of the maximum array element in that subarray is at least L and at most R.
 *
 * Example :
 * Input:
 * A = [2, 1, 4, 3]
 * L = 2
 * R = 3
 * Output: 3
 * Explanation: There are three subarrays that meet the requirements: [2], [2, 1], [3].
 * Note:
 *
 * L, R  and A[i] will be an integer in the range [0, 10^9].
 * The length of A will be in the range of [1, 50000].
 */
/**
 * Compact the below one.
 */
class Solution {
public:
    int numSubarrayBoundedMax(vector<int>& A, int L, int R) {
        int cnt=0,i=0,j=0;
        for (auto n: A){
            i=(n<L) ? i+1 : 0;
            j=(n<=R) ? j+1 : 0;
            cnt+=j-i;
        }
        return cnt;
    }
};

/**
 * After reading below solution, I finally understand the compact version.
 */
class Solution {
public:
    int numSubarrayBoundedMax(vector<int>& A, int L, int R) {
        int cnt=0,lessThanLeft=0,lessThanOrEqToRight=0;
        for (auto n: A){

            if (n<L)
                ++lessThanLeft;
            else
                lessThanLeft=0;

            if (n<=R)
                ++lessThanOrEqToRight;
            else
                lessThanOrEqToRight=0;

            cnt+=lessThanOrEqToRight-lessThanLeft;
        }
        return cnt;
    }
};

/**
 * From discussion. I still don't understand this one. But the speed is the same as
 * the original one.
 */
class Solution {
public:
    int numSubarrayBoundedMax(vector<int>& A, int L, int R) {
        int result=0, left=-1, right=-1;
        for (int i=0; i<A.size(); i++) {
            if (A[i]>R) left=i;
            if (A[i]>=L) right=i;
            result+=right-left;
        }

        return result;
    }
};

/**
 * Original solution. The idea is:
 * all value of elements in array[left, right] are under R.
 * array[left, right] has (right - left) * (right - left + 1) / 2 subarrays.
 * lt_count represents an array has lt_count elements, and all elements
 * value are under L. this array has lt_count * (lt_count + 1)/ 2 subarrays.
 * And the number of subarrays that satisfy the description are the diff of the above two.
 */
class Solution {
public:
    int numSubarrayBoundedMax(vector<int>& A, int L, int R) {
        int ans = 0;
        int left = 0, right = 0;
        int lt_count = 0, exclusions = 0;
        while (right < A.size()) {
            if (A[right] >= L) {
                if (lt_count > 0) {
                    exclusions += lt_count * (lt_count + 1) / 2;
                }
                lt_count = 0;
            }
            else {
                ++lt_count;
            }

            if (A[right] > R) {
                int count = right - left;
                ans += count * (count + 1) / 2;
                left = right + 1;
            }
            ++right;
        }

        if (lt_count > 0) {
            exclusions += lt_count * (lt_count + 1) / 2;
        }

        int count = right - left;
        ans += count * (count + 1) / 2;
        ans -= exclusions;

        return ans;
    }
};
