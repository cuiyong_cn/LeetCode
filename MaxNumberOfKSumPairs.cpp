/**
 * Max Number of K-Sum Pairs
 *
 * You are given an integer array nums and an integer k.
 *
 * In one operation, you can pick two numbers from the array whose sum equals k and remove them from
 * the array.
 *
 * Return the maximum number of operations you can perform on the array.
 *
 * Example 1:
 *
 * Input: nums = [1,2,3,4], k = 5
 * Output: 2
 * Explanation: Starting with nums = [1,2,3,4]:
 * - Remove numbers 1 and 4, then nums = [2,3]
 * - Remove numbers 2 and 3, then nums = []
 * There are no more pairs that sum up to 5, hence a total of 2 operations.
 * Example 2:
 *
 * Input: nums = [3,1,3,4,3], k = 6
 * Output: 1
 * Explanation: Starting with nums = [3,1,3,4,3]:
 * - Remove the first two 3's, then nums = [1,4,3]
 * There are no more pairs that sum up to 6, hence a total of 1 operation.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 105
 * 1 <= nums[i] <= 109
 * 1 <= k <= 109
 */
/**
 * Sort solution.
 */
class Solution {
public:
    int maxOperations(vector<int>& num, int k) {
      sort(num.begin(), num.end());
      int ans = 0;
      int i = 0, j = num.size() - 1;
      while (i < j) {
          auto const sum = num[i] + num[j];
          if (sum == k) {
              ans++; i++; j--;
          }
          else if (sum > k) {
              j--;
          }
          else {
              i++;
          }
      }

      return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int maxOperations(vector<int>& nums, int k) {
        unordered_map<int, int> num_cnt;
        for (auto n : nums) {
            ++num_cnt[n];
        }

        int ans = 0;
        while (!num_cnt.empty()) {
            auto [p1, c1] = *num_cnt.begin();
            auto p2 = k - p1;
            auto it = num_cnt.find(p2);
            if (it != num_cnt.end()) {
                auto c2 = it->second;
                if (p1 != p2) {
                    ans += min(c1, c2);
                    num_cnt.erase(it);
                }
                else {
                    ans += c1 / 2;
                }
            }
            num_cnt.erase(p1);
        }

        return ans;
    }
};
