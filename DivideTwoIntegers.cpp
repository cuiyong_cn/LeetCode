/**
 * Divide Two Integers
 *
 * Given two integers dividend and divisor, divide two integers without using multiplication,
 * division, and mod operator.
 *
 * Return the quotient after dividing dividend by divisor.
 *
 * The integer division should truncate toward zero, which means losing its fractional part. For
 * example, truncate(8.345) = 8 and truncate(-2.7335) = -2.
 *
 * Note: Assume we are dealing with an environment that could only store integers within the 32-bit
 * signed integer range: [−231, 231 − 1]. For this problem, assume that your function returns 231 −
 * 1 when the division result overflows.
 *
 * Example 1:
 *
 * Input: dividend = 10, divisor = 3
 * Output: 3
 * Explanation: 10/3 = truncate(3.33333..) = 3.
 * Example 2:
 *
 * Input: dividend = 7, divisor = -3
 * Output: -2
 * Explanation: 7/-3 = truncate(-2.33333..) = -2.
 * Example 3:
 *
 * Input: dividend = 0, divisor = 1
 * Output: 0
 * Example 4:
 *
 * Input: dividend = 1, divisor = 1
 * Output: 1
 *
 * Constraints:
 *
 * -231 <= dividend, divisor <= 231 - 1
 * divisor != 0
 */
/**
 * I think below one is bettern than mine. But with a little tweak, mine can improved a lot
 */
public class Solution {
    public int divide(int dividend, int divisor) {
        if (divisor==0) return Integer.MAX_VALUE;
        if (divisor==1 || dividend==0) return dividend;
        if (divisor==Integer.MIN_VALUE) {
            if (dividend==Integer.MIN_VALUE) return 1;
            else return 0;
        }
        if (dividend==Integer.MIN_VALUE) {
            if (divisor==1) return dividend;
            else if (divisor==-1) return Integer.MAX_VALUE;
            else return (((divisor&1)==1) ? divide(dividend+1,divisor):divide(dividend>>1,divisor>>1));
        }
        int res=0;
        int divid=Math.abs(dividend);
        int divis=Math.abs(divisor);
        while (divid>=divis){
            int temp=divis, multiple=1;
            while ((temp<<1)>=0 && (divid>=(temp<<1))) {
                temp <<= 1;
                multiple <<= 1;
            }
            divid -= temp;
            res += multiple;
        }
        return (dividend>0)^(divisor>0) ? -res:res;
    }
}

/**
 * Original solution. This problem really got me. lots of corner cases to be considered.
 */
class Solution {
public:
    int divide(int dividend, int divisor) {
        if (dividend == divisor) {
            return 1;
        }

        if (dividend == numeric_limits<int>::min() && -1 == divisor) {
            return numeric_limits<int>::max();
        }

        if (divisor == numeric_limits<int>::min()) {
            return 0;
        }

        int ans = 0;
        if (dividend > 0) {
            int base = divisor > 0 ? divisor : -divisor;
            int base_max = pow(2, 30) - 1;
            while (dividend >= base) {
                int factor = divisor > 0 ? 1 : -1;
                while (dividend >= base) {
                    dividend -= base;
                    ans += factor;
                    if (base < base_max) {
                        base <<= 1;
                        factor += factor;
                    }
                }
                base = divisor > 0 ? divisor : -divisor;
            }
        }
        else {
            int base = divisor > 0 ? -divisor : divisor;
            int base_min = -pow(2, 30);
            while (dividend <= base) {
                int factor = divisor > 0 ? -1 : 1;
                while (dividend <= base) {
                    dividend -= base;
                    ans += factor;
                    if (base > base_min) {
                        base += base;
                        factor += factor;
                    }
                }
                base = divisor > 0 ? -divisor : divisor;
            }
        }

        return ans;
    }
};
