/**
 * Pascal's Triangle
 *
 * Given a non-negative integer numRows, generate the first numRows of Pascal's triangle.
 *
 * In Pascal's triangle, each number is the sum of the two numbers directly above it.
 *
 * Example:
 *
 * Input: 5
 * Output:
 * [
 *      [1],
 *     [1,1],
 *    [1,2,1],
 *   [1,3,3,1],
 *  [1,4,6,4,1]
 * ]
 */
/**
 * Original solution.
 */
class Solution {
public:
    vector<vector<int>> generate(int numRows) {
        if (0 == numRows) return {};

        vector< vector<int> > ans;
        for (int i = 1; i <= numRows; ++i) {
            ans.emplace_back(vector<int>(i, 1));
        }

        for (int i = 2; i < numRows; ++i) {
            for (int j = 1; j < i; ++j) {
                ans[i][j] = ans[i - 1][j - 1] + ans[i - 1][j];
            }
        }

        return ans;
    }
};
