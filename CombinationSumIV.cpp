/**
 * Combination Sum IV
 *
 * Given an integer array with all positive numbers and no duplicates, find the number of possible combinations that add
 * up to a positive integer target.
 *
 * Example:
 *
 * nums = [1, 2, 3]
 * target = 4
 *
 * The possible combination ways are:
 * (1, 1, 1, 1)
 * (1, 1, 2)
 * (1, 2, 1)
 * (1, 3)
 * (2, 1, 1)
 * (2, 2)
 * (3, 1)
 *
 * Note that different sequences are counted as different combinations.
 *
 * Therefore the output is 7.
 *
 * Follow up:
 * What if negative numbers are allowed in the given array?
 * How does it change the problem?
 * What limitation we need to add to the question to allow negative numbers?
 *
 * Credits:
 * Special thanks to @pbrother for adding this problem and creating all test cases.
 */
/**
 * There is a bottom up solution come from discussion.
 * Theoretically it works.
 *
 * But this solution does not work on all case like [3, 33, 333] 1000
 * Because s[3] = 1, s[6] = 1, s[9] = 1, ...
 *         s[33] = 2, s[36] = 3, s[39] = 4, s[42] = 5, ...
 *         s[63] = 12, s[66] = 14, s[69] = 17, ...
 * and finally gonna hit signed integer overflow
 *
 * DFS solution gonna suffer the same symptom. For example [3, 33, 333] 999
 */
class Solution {
public:
    int combinationSum4(vector<int>& nums, int target) {
        vector<int> comb(target + 1, 0);
        comb[0] = 1;
        for (int i = 1; i < comb.size(); i++) {
            for (int j = 0; j < nums.size(); j++) {
                if (i - nums[j] >= 0) {
                    comb[i] += comb[i - nums[j]];
                }
            }
        }
        return comb[target];
    }
};

/**
 * Original solution. DFS + memo.
 * Please note that, there are no duplicates in array.
 * If there are duplicates, we have to skip the same
 * all the array start with the same number.
 */
class Solution {
public:
    int combinationSum4(vector<int>& nums, int target) {
        sort(nums.begin(), nums.end());

        return dfs(nums, target);
    }
private:
    int dfs(const vector<int>& nums, int target)
    {
        if (0 == target) {
            return 1;
        }

        int ans = 0;
        if (sum4.count(target)) {
            return sum4[target];
        }

        for (int i = 0; i < nums.size(); ++i) {
            if (nums[i] > target) {
                break;
            }
            ans += dfs(nums, target - nums[i]);
        }

        sum4[target] = ans;

        return ans;
    }
    map<int, int> sum4;
};
