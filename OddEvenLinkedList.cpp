/**
 * Odd Even Linked List
 *
 * Given a singly linked list, group all odd nodes together followed by the even nodes. Please note here we are talking about the node number and not the value in the nodes.
 *
 * You should try to do it in place. The program should run in O(1) space complexity and O(nodes) time complexity.
 *
 * Example 1:
 *
 * Input: 1->2->3->4->5->NULL
 * Output: 1->3->5->2->4->NULL
 *
 * Example 2:
 *
 * Input: 2->1->3->5->6->4->7->NULL
 * Output: 2->3->6->7->1->5->4->NULL
 *
 * Note:
 *
 *     The relative order inside both the even and odd groups should remain as it was in the input.
 *     The first node is considered odd, the second node even and so on ...
 */
/**
 * Improved version.
 */
class Solution {
public:
    ListNode* oddEvenList(ListNode* head) {
        if (nullptr == head) return nullptr;

        ListNode* odd = head;
        ListNode* even = head->next;
        ListNode* evenhead = even;

        while (even && even->next) {
            odd->next = even->next;
            odd = even->next;
            even->next = odd->next;
            even = even->next;
        }
        odd->next = evenhead;

        return head;
    }
};

/**
 * Original solution. Something wrong with it, but can not express it.
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* oddEvenList(ListNode* head) {
        if (nullptr == head) return nullptr;
        if (nullptr == head->next) return head;

        ListNode* cur = head;
        ListNode* evenListHead = head->next;

        bool odd = true;
        while (cur) {
            ListNode* next = cur->next;
            if (next) {
                if (next->next) {
                    cur->next = next->next;
                }
                else {
                    if (false == odd) {
                        cur->next = nullptr;
                        cur = next;
                    }
                    break;
                }
            }
            cur = next;
            odd = odd ? false : true;
        }
        cur->next = evenListHead;

        return head;
    }
};
