/**
 * Partition Labels
 *
 * A string S of lowercase letters is given. We want to partition this string
 * into as many parts as possible so that each letter appears in at most one part,
 * and return a list of integers representing the size of these parts.
 * 
 * Example 1:
 * 
 * Input: S = "ababcbacadefegdehijhklij"
 * Output: [9,7,8]
 * Explanation:
 * The partition is "ababcbaca", "defegde", "hijhklij".
 * This is a partition so that each letter appears in at most one part.
 * A partition like "ababcbacadefegde", "hijhklij" is incorrect, because it
 * splits S into less parts.
 * 
 * Note:
 * 
 *     S will have length in range [1, 500].
 *     S will consist of lowercase letters ('a' to 'z') only.
 */ 
class Solution {
public:
    vector<int> partitionLabels(string S) {
        vector<int> partition;
        int last[26];
        for (int i = 0; i < S.size(); ++i) {
            last[S[i] - 'a'] = i;
        }
        
        for (int i = 0; i < S.size(); ++i) {
            int j = last[S[i] - 'a'];
            for (int k = i + 1; k < j; ++k) {
                if (last[S[k] - 'a'] > j) {
                    j = last[S[k] - 'a'];
                }
            }
            partition.push_back(j - i + 1);
            i = j;
        }
        return partition;
    }
};
