/**
 * Find Longest Special Substring That Occurs Thrice II
 *
 * You are given a string s that consists of lowercase English letters.
 *
 * A string is called special if it is made up of only a single character. For example, the string
 * "abc" is not special, whereas the strings "ddd", "zz", and "f" are special.
 *
 * Return the length of the longest special substring of s which occurs at least thrice, or -1 if no
 * special substring occurs at least thrice.
 *
 * A substring is a contiguous non-empty sequence of characters within a string.
 *
 * Example 1:
 *
 * Input: s = "aaaa"
 * Output: 2
 * Explanation: The longest special substring which occurs thrice is "aa": substrings "aaaa", "aaaa", and "aaaa".
 * It can be shown that the maximum length achievable is 2.
 *
 * Example 2:
 *
 * Input: s = "abcdef"
 * Output: -1
 * Explanation: There exists no special substring which occurs at least thrice. Hence return -1.
 *
 * Example 3:
 *
 * Input: s = "abcaba"
 * Output: 1
 * Explanation: The longest special substring which occurs thrice is "a": substrings "abcaba", "abcaba", and "abcaba".
 * It can be shown that the maximum length achievable is 1.
 *
 * Constraints:
 *     3 <= s.length <= 5 * 105
 *     s consists of only lowercase English letters.
 */
/**
 * Original solution.
 */
template<typename Heap>
Heap default_heap(initializer_list<typename Heap::value_type> const& values)
{
    Heap h;

    for (auto const& v : values) {
        h.push(v);
    }

    return h;
}

class Solution {
public:
    template<typename T>
    using MinHeap = priority_queue<T, vector<T>, greater<T>>;

    int maximumLength(string s) {
        s.push_back('A');

        auto char_min_heap = vector<MinHeap<int>>(26, default_heap<MinHeap<int>>({-1, -1, -1}));
        auto ans = -1;
        int const n = s.length();
        auto prev = s[0];
        auto cnt = 1;

        for (auto i = 1; i < n; ++i) {
            if (prev != s[i]) {
                auto& cmh = char_min_heap[prev - 'a'];

                for (auto k = cnt; k > 0 && k > cmh.top(); --k) {
                    cmh.push(k);

                    while (cmh.size() > 3) {
                        cmh.pop();
                    }
                }

                prev = s[i];
                cnt = 1;
            }
            else {
                cnt += 1;
            }
        }

        for (auto& cmh : char_min_heap) {
            ans = max(ans, cmh.top());
        }

        return ans;
    }
};
