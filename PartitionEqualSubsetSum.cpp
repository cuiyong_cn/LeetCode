/**
 * Partition Equal Subset Sum
 *
 * Given a non-empty array nums containing only positive integers, find if the array can be partitioned into two subsets
 * such that the sum of elements in both subsets is equal.
 *
 * Example 1:
 *
 * Input: nums = [1,5,11,5]
 * Output: true
 * Explanation: The array can be partitioned as [1, 5, 5] and [11].
 * Example 2:
 *
 * Input: nums = [1,2,3,5]
 * Output: false
 * Explanation: The array cannot be partitioned into equal sum subsets.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 200
 * 1 <= nums[i] <= 100
 */
/**
 * From discussion. Really impressive. Time is 8ms
 */
class Solution {
public:
    bool canPartition(vector<int>& nums) {
        int sum = accumulate(nums.begin(), nums.end(), 0);
        if (sum & 1) {
            return false;
        }

        bitset<10001> bits(1);
        for (auto n : nums) {
            bits |= bits << n;
        }

        return bits[sum / 2];
    }
};

/**
 * Use vector to speed things up. result is 88ms.
 */
class Solution {
public:
    bool canPartition(vector<int>& nums) {
        int sum = accumulate(nums.begin(), nums.end(), 0);
        if (sum & 1) {
            return false;
        }

        int target = sum / 2;
        vector<short> subsum(sum + 1, 0);
        subsum[0] = 1;

        for (auto n : nums) {
            vector<short> tmp = subsum;
            for (int i = 0; i < subsum.size(); ++i) {
                if (subsum[i] > 0 && (i + n) < (sum + 1)) {
                    tmp[i + n] = 1;
                }
            }

            if (tmp[target]) {
                return true;
            }

            swap(subsum, tmp);
        }

        return false;
    }
};

/**
 * DP solution to speed it up. But the result is till not satisfying.
 */
class Solution {
public:
    bool canPartition(vector<int>& nums) {
        int sum = accumulate(nums.begin(), nums.end(), 0);
        if (sum & 1) {
            return false;
        }

        int target = sum / 2;
        unordered_set<int> subsum{0};

        for (auto n : nums) {
            unordered_set<int> tmp;
            for (auto sub : subsum) {
                tmp.insert(sub + n);
            }
            subsum.insert(tmp.begin(), tmp.end());
            if (subsum.count(target)) {
                return true;
            }
        }

        return false;
    }
};

/**
 * Original solution. DFS, TLE as expected.
 */
class Solution {
public:
    bool canPartition(vector<int>& nums) {
        int sum = accumulate(nums.begin(), nums.end(), 0);
        if (sum & 1) {
            return false;
        }

        return dfs(nums, 0, 0, sum / 2);
    }

private:
    bool dfs(const vector<int>& nums, int i, int sum, int target)
    {
        if (sum == target) {
            return true;
        }

        if (sum > target) {
            return false;
        }

        if (i >= nums.size()) {
            return false;
        }

        if (dfs(nums, i + 1, sum + nums[i], target)) {
            return true;
        }

        return dfs(nums, i + 1, sum, target);
    }
};
