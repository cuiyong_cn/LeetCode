/**
 * Lucky Numbers in a Matrix
 *
 * Given an m x n matrix of distinct numbers, return all lucky numbers in the matrix in any order.
 *
 * A lucky number is an element of the matrix such that it is the minimum element in its row and
 * maximum in its column.
 *
 * Example 1:
 *
 * Input: matrix = [[3,7,8],[9,11,13],[15,16,17]]
 * Output: [15]
 * Explanation: 15 is the only lucky number since it is the minimum in its row and the maximum in its column
 * Example 2:
 *
 * Input: matrix = [[1,10,4,2],[9,3,8,7],[15,16,17,12]]
 * Output: [12]
 * Explanation: 12 is the only lucky number since it is the minimum in its row and the maximum in its column.
 * Example 3:
 *
 * Input: matrix = [[7,8],[1,2]]
 * Output: [7]
 * Explanation: 7 is the only lucky number since it is the minimum in its row and the maximum in its column.
 * Example 4:
 *
 * Input: matrix = [[3,6],[7,1],[5,2],[4,8]]
 * Output: []
 * Explanation: There is no lucky number.
 *
 * Constraints:
 *
 * m == mat.length
 * n == mat[i].length
 * 1 <= n, m <= 50
 * 1 <= matrix[i][j] <= 105.
 * All elements in the matrix are distinct.
 */
/**
 * Original solution. Find the minimim element in row. Then find out if it is the max ele in column.
 *
 * Actually there will only be one lucky number at most in the matrix.
 * Proof:
 *
 * If there are two luck number. Then they will be at different place (not at the same row nor same
 * column)
 *
 * Let's assume A and D are the two lunck numebr. According to the description.
 *
 * C < A < B, B < D < C
 *
 * We arrive at a contradiction.
 *
 * A ------------ B
 * |              |
 * |              |
 * |              |
 * C ------------ D
 */
class Solution {
public:
    vector<int> luckyNumbers (vector<vector<int>>& matrix) {
        int const M = matrix.size();
        int const N = matrix[0].size();
        vector<int> ans;
        for (auto& row : matrix) {
            int min_ele_row = row[0];
            int col = 0;
            for (int i = 1; i < N; ++i) {
                if (row[i] < min_ele_row) {
                    min_ele_row = row[i];
                    col = i;
                }
            }

            auto is_max_ele_in_col = true;
            for (int r = 0; r < M; ++r) {
                if (matrix[r][col] > min_ele_row) {
                    is_max_ele_in_col = false;
                    break;
                }
            }

            if (is_max_ele_in_col) {
                ans.push_back(min_ele_row);
            }
        }

        return ans;
    }
};
