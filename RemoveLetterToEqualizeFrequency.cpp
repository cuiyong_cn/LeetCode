/**
 * Remove Letter To Equalize Frequency
 *
 * You are given a 0-indexed string word, consisting of lowercase English letters. You need to
 * select one index and remove the letter at that index from word so that the frequency of every
 * letter present in word is equal.
 *
 * Return true if it is possible to remove one letter so that the frequency of all letters in word
 * are equal, and false otherwise.
 *
 * Note:
 *
 * The frequency of a letter x is the number of times it occurs in the string.
 * You must remove exactly one letter and cannot chose to do nothing.
 *
 * Example 1:
 *
 * Input: word = "abcc"
 * Output: true
 * Explanation: Select index 3 and delete it: word becomes "abc" and each character has a frequency of 1.
 * Example 2:
 *
 * Input: word = "aazz"
 * Output: false
 * Explanation: We must delete a character, so either the frequency of "a" is 1 and the frequency of "z" is 2, or vice versa. It is impossible to make all present letters have equal frequency.
 *
 * Constraints:
 *
 * 2 <= word.length <= 100
 * word consists of lowercase English letters only.
 */
/**
 * Original solution.
 */
bool non_zero_same(array<int, 26> const& cc)
{
    auto const last = *find_if(begin(cc), end(cc), [](auto n) { return 0 != n; });
    for (auto n : cc) {
        if (0 != n && last != n) {
            return false;
        }
    }
    return true;
}

class Solution {
public:
    bool equalFrequency(string word) {
        auto char_cnt = array<int, 26>{ 0, };
        for (auto c : word) {
            ++char_cnt[c - 'a'];
        }

        for (auto& cc : char_cnt) {
            --cc;
            if (non_zero_same(char_cnt)) {
                return true;
            }
            ++cc;
        }

        return false;
    }
};
