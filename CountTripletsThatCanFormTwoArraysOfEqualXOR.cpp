/**
 * Count Triplets That Can Form Two Arrays of Equal XOR
 *
 * Given an array of integers arr.
 *
 * We want to select three indices i, j and k where (0 <= i < j <= k < arr.length).
 *
 * Let's define a and b as follows:
 *
 * a = arr[i] ^ arr[i + 1] ^ ... ^ arr[j - 1]
 * b = arr[j] ^ arr[j + 1] ^ ... ^ arr[k]
 * Note that ^ denotes the bitwise-xor operation.
 *
 * Return the number of triplets (i, j and k) Where a == b.
 *
 * Example 1:
 *
 * Input: arr = [2,3,1,6,7]
 * Output: 4
 * Explanation: The triplets are (0,1,2), (0,2,2), (2,3,4) and (2,4,4)
 * Example 2:
 *
 * Input: arr = [1,1,1,1,1]
 * Output: 10
 *
 * Constraints:
 *
 * 1 <= arr.length <= 300
 * 1 <= arr[i] <= 108
 */
/**
 * O(n) solution.
 * Explaination:
 *  if we are at index i, and prefix is pre_i, let's say before index i,
 *  pre_i occured at index i1, i2, i3 where i1 < i2 < i3
 *  Then it means, 0 == xor(i1, i), 0 == xor(i2, i), 0 == xor(i3, i)
 *  Then we have (i - i1 - 1) + (i - i2 - 1) + (i - i3 - 1) =
 *               N * i - (i1 + i2 + i3) - N = (N is number of time pre_i has occured before index i)
 *               (N - 1) * i - (i1 + i2 + i3)
 *                                  |
 *                                  ---------- sum of the index where pre_i has occured
 */
class Solution {
public:
    int countTriplets(vector<int>& A) {
        int n = A.size(), res = 0, prefix = 0;
        unordered_map<int, int> count = {{0, 1}}, total;
        for (int i = 0; i < n; ++i) {
            prefix ^= A[i];
            res += count[prefix]++ * i - total[prefix];
            total[prefix] += i + 1;
        }
        return res;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int countTriplets(vector<int>& arr) {
        for (size_t i = 1; i < arr.size(); ++i) {
            arr[i] ^= arr[i - 1];
        }

        int ans = 0;
        for (size_t L = 2; L <= arr.size(); ++L) {
            ans += 0 == arr[L - 1] ? L - 1 : 0;
            auto end = arr.size() - L;
            for (size_t i = 0; i < end; ++i) {
                ans += 0 == (arr[i] ^ arr[i + L]) ? L - 1 : 0;
            }
        }

        return ans;
    }
};
