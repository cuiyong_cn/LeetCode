/**
 * Maximum Number of Non-Overlapping Subarrays With Sum Equals Target
 *
 * Given an array nums and an integer target.
 *
 * Return the maximum number of non-empty non-overlapping subarrays such that the sum of values in
 * each subarray is equal to target.
 *
 * Example 1:
 *
 * Input: nums = [1,1,1,1,1], target = 2
 * Output: 2
 * Explanation: There are 2 non-overlapping subarrays [1,1,1,1,1] with sum equals to target(2).
 * Example 2:
 *
 * Input: nums = [-1,3,5,1,4,2,-9], target = 6
 * Output: 2
 * Explanation: There are 3 subarrays with sum equal to 6.
 * ([5,1], [4,2], [3,5,1,4,2,-9]) but only the first 2 are non-overlapping.
 * Example 3:
 *
 * Input: nums = [-2,6,6,3,5,4,1,2,8], target = 10
 * Output: 3
 * Example 4:
 *
 * Input: nums = [0,0,0], target = 0
 * Output: 3
 *
 * Constraints:
 *
 * 1 <= nums.length <= 10^5
 * -10^4 <= nums[i] <= 10^4
 * 0 <= target <= 10^6
 */
/**
 * as the i is increasing, we can get rid of the subarrays.
 */
class Solution {
public:
    int maxNonOverlapping(vector<int>& nums, int target) {
        std::unordered_map<int, int> prefix_sum;

        prefix_sum[0] = -1;
        int ans = 0;
        int sum = 0;
        int end = -1;
        for (int i = 0; i < nums.size(); ++i) {
            sum += nums[i];
            if (prefix_sum.count(sum - target)) {
                auto start = prefix_sum[sum - target];
                if (end <= start) {
                    ++ans;
                    end = i;
                }
            }

            prefix_sum[sum] = i;
        }

        return ans;
    }
};

/**
 * Original solution. This problem combines interval scheduling with target sum.
 */
class Solution {
public:
    int maxNonOverlapping(vector<int>& nums, int target) {
        std::unordered_map<int, int> prefix_sum;

        prefix_sum[0] = -1;
        std::vector<std::pair<int, int>> subarrays;
        int sum = 0;
        for (int i = 0; i < nums.size(); ++i) {
            sum += nums[i];
            if (prefix_sum.count(sum - target)) {
                subarrays.push_back({prefix_sum[sum - target] + 1, i});
            }
            prefix_sum[sum] = i;
        }

        std::sort(subarrays.begin(), subarrays.end(),
                [](auto&& a, auto&& b) {
                    return a.second < b.second;
                });

        int ans = 0;
        int end = -1;
        for (int i = 0; i < subarrays.size(); ++i) {
            if (subarrays[i].first > end) {
                ++ans;
                end = subarrays[i].second;
            }
        }

        return ans;
    }
};
