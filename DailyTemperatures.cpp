/**
 * Daily Temperatures
 *
 * Given a list of daily temperatures T, return a list such that, for each day in the
 * input, tells you how many days you would have to wait until a warmer temperature.
 * If there is no future day for which this is possible, put 0 instead.
 *
 * For example, given the list of temperatures T = [73, 74, 75, 71, 69, 72, 76, 73],
 * your output should be [1, 1, 4, 2, 1, 1, 0, 0].
 *
 * Note: The length of temperatures will be in the range [1, 30000]. Each temperature
 * will be an integer in the range [30, 100].
 */
/**
 * We compute the result in reverse order. Given temperatures = [73, 74, 75, 71, 69, 72,
 * 76, 73], suppose that we already obtain number of days for i >= 3: res = [?, ?, ?, 2,
 * 1, 1, 0, 0] where ? denotes values to be determined. In order to find the number of
 * days for i = 2, we proceed as follows:
 *
 * Let j = i + 1, i.e., j = 3. If temperatures[i] < temperatures[j], res[i] = 1;
 * Otherwise, temperatures[i] >= temperatures[j]. we examine the value of res[j], which
 * is equal to 2, which means that the most recent day with higher temperature than
 * temperatures[j] in the future is 2 days away. Since temperatures[i] >= temperatures[j],
 * the most recent day with higher temperature than temperatures[i] should be at least 2
 * days away. Therefore, we can skip some days and arrive directly at day j + res[j].
 * This process continues until we find higher temperature or we know we should stop as
 * descibed in the third point.
 * If temperatures[j] == 0, we shoud directly set res[i] to 0 since we know we cannot
 * find higher temperature in the future.
 *
 * Time: O(n) (actually O(2n))
 */
class Solution
{
public:
    vector<int> res(temperatures.size());
    for (int i = temperatures.size() - 1; i >= 0; --i)
    {
        int j = i + 1;
        while (j < temperatures.size() && temperatures[j] <= temperatures[i]) {
            if (res[j] > 0) j = res[j] + j;
            else j = temperatures.size();
        }
        // either j == size || temperatures[j] > temperatures[i]
        if (j < temperatures.size()) res[i] = j - i;
    }
    return res;
};

/**
 * for each day from end to start, record the next day of temperature t for all t in [30, 100]
 * for each day from n-1 to 0; for all temperature higher than temp[i], find the earliest
 * I don't think this method is really intuitive.
 * time: O(n*w) here w is 70
 */
class Solution
{
public:
    vector<int> dailyTemperatures(vector<int>& temps)
    {
        int n = temps.size();
        vector<int> waits(n, 0);
        vector<int> next(101, INT_MAX); // next day with with certain temperature.
        for (int i = n - 1; i >= 0; i--) {
            int earliest = INT_MAX;
            for (int t = temps[i] + 1; t <= 100; t++) {
                earliest = min(earliest, next[t]);
            }
            if (earliest != INT_MAX) waits[i] = earliest - i;
            next[temps[i]] = i;
        }
        return waits;
    }
};

/**
 * Using a stack.
 *
 *  1. You can keep track of the numbers seen so that when you move left -> That left
 *     element already knows which is the maximum number it will encounter
 *  2. If the current number is smaller than the previously seen number -> Simply get
 *     the diff in the positions and insert into the stack
 *  3. If the number is greater than the previously seen in stack -> Keep removing all
 *     the numbers (Since we need to find the number larger than it. IF found then great
 *     if not then it has result 0).
 *  One great reason you can just remove the smaller numbers from stack(seen in right)
 *  is that now that we have current number larger than its right elements -> Hence for
 *  the lefter elements THIS CURRENT element will itself be the nearest largest number
 *
 *  time(O(n)) (actually same as the first one, cuase its underlying thoughts are same)
 */
class Solution
{
public:
    vector<int> dailyTemperatures(vector<int>& temperatures)
    {
        stack<pair<int, int>> s; //First is the number while second is the position

        int n = temperatures.size();
        vector<int> result(n, 0);

        for (int i = n - 1; i >= 0; i--) {
            int curr = temperatures[i];
            while (!s.empty() && s.top().first <= curr) s.pop();

            result[i] = s.empty() ? 0 : s.top().second - i;

            s.push({curr, i});
        }

        return result;
    }
};
