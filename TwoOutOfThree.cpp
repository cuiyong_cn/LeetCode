/**
 * Two Out of Three
 *
 * Given three integer arrays nums1, nums2, and nums3, return a distinct array containing all the
 * values that are present in at least two out of the three arrays. You may return the values in any
 * order.
 *
 * Example 1:
 *
 * Input: nums1 = [1,1,3,2], nums2 = [2,3], nums3 = [3]
 * Output: [3,2]
 * Explanation: The values that are present in at least two arrays are:
 * - 3, in all three arrays.
 * - 2, in nums1 and nums2.
 * Example 2:
 *
 * Input: nums1 = [3,1], nums2 = [2,3], nums3 = [1,2]
 * Output: [2,3,1]
 * Explanation: The values that are present in at least two arrays are:
 * - 2, in nums2 and nums3.
 * - 3, in nums1 and nums2.
 * - 1, in nums1 and nums3.
 * Example 3:
 *
 * Input: nums1 = [1,2,2], nums2 = [4,3,3], nums3 = [5]
 * Output: []
 * Explanation: No value is present in at least two arrays.
 *
 * Constraints:
 *
 * 1 <= nums1.length, nums2.length, nums3.length <= 100
 * 1 <= nums1[i], nums2[j], nums3[k] <= 100
 */
/**
 * More efficient one.
 */
class Solution {
public:
    vector<int> twoOutOfThree(vector<int>& nums1, vector<int>& nums2, vector<int>& nums3) {
        array<int, 101> num_in_arr{0,};
        for (int n : nums1) num_in_arr[n] |= 1<<0;
        for (int n : nums2) num_in_arr[n] |= 1<<1;
        for (int n : nums3) num_in_arr[n] |= 1<<2;

        vector<int> ans;
        for (int i = 1; i < num_in_arr.size(); i++) {
            if (num_in_arr[i] == 3 || num_in_arr[i] >= 5) {
                ans.emplace_back(i);
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
vector<int> set_intersection(vector<int> const& n1, vector<int> const& n2)
{
    vector<int> ans;
    set_intersection(n1.begin(), n1.end(), n2.begin(), n2.end(), back_inserter(ans));
    return ans;
}

class Solution {
public:
    vector<int> twoOutOfThree(vector<int>& nums1, vector<int>& nums2, vector<int>& nums3) {
        sort(nums1.begin(), nums1.end());
        sort(nums2.begin(), nums2.end());
        sort(nums3.begin(), nums3.end());

        set<int> ans;
        for (auto const& n : set_intersection(nums1, nums2)) {
            ans.insert(n);
        }

        for (auto const& n : set_intersection(nums2, nums3)) {
            ans.insert(n);
        }
        for (auto const& n : set_intersection(nums1, nums3)) {
            ans.insert(n);
        }

        return vector<int>{ans.begin(), ans.end()};
    }
};
