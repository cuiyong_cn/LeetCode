/**
 * Smallest Value After Replacing With Sum of Prime Factors
 *
 * You are given a positive integer n.
 *
 * Continuously replace n with the sum of its prime factors.
 *
 * Note that if a prime factor divides n multiple times, it should be included in the sum as many times as it divides n.
 * Return the smallest value n will take on.
 *
 *
 *
 * Example 1:
 *
 * Input: n = 15
 * Output: 5
 * Explanation: Initially, n = 15.
 * 15 = 3 * 5, so replace n with 3 + 5 = 8.
 * 8 = 2 * 2 * 2, so replace n with 2 + 2 + 2 = 6.
 * 6 = 2 * 3, so replace n with 2 + 3 = 5.
 * 5 is the smallest value n will take on.
 * Example 2:
 *
 * Input: n = 3
 * Output: 3
 * Explanation: Initially, n = 3.
 * 3 is the smallest value n will take on.
 *
 *
 * Constraints:
 *
 * 2 <= n <= 105
 */
/**
 * Original solution.
 */
auto is_prime(int n) -> pair<int, int>
{
    if (n < 3) {
        return {true, 1 == n ? 0 : n};
    }

    for (auto i : {2, 3}) {
        if (0 == (n % i)) {
            return {false, i};
        }
    }

    auto upper = int(sqrt(n));
    for (auto i = 5; i <= upper; i += 6) {
        for (auto j : {i, i + 2}) {
            if (0 == (n % j)) {
                return {false, j};
            }
        }
    }

    return {true, n};
}

class Solution {
public:
    int smallestValue(int n) {
        auto ans = n;
        for (; n > 3;) {
            auto sum = 0;
            for (auto nn = n;;) {
                auto [prime, div] = is_prime(nn);
                sum += div;
                if (prime) {
                    break;
                }

                nn /= div;
            }

            if (sum == n) {
                break;
            }

            ans = min(ans, sum);
            n = sum;
        }

        return ans;
    }
};
