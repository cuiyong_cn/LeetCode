/**
 * Sum of Two Integers
 *
 * Calculate the sum of two integers a and b, but you are not allowed to use the operator + and -.
 *
 * Example 1:
 *
 * Input: a = 1, b = 2
 * Output: 3
 *
 * Example 2:
 *
 * Input: a = -2, b = 3
 * Output: 1
 */
/**
 * And the following solution is what I prefer. It basically simulate the addition process.
 */
class Solution {
public:
    int getSum(int a, int b) {
        if (a == 0) return b;
        if (b == 0) return a;

        int sum = 0;
        int mask = 1;
        int c = 0;

        while (mask) {
            int m1 = (a & mask) != 0 ? 1 : 0;
            int m2 = (b & mask) != 0 ? 1 : 0;

            if (m1 == 1 && m2 == 1) {
                if (c == 1) sum |= mask;

                c = 1;
            }
            else if ((m1 ^ m2) == 1) {
                if (c == 1)
                    c = 1;
                else
                    sum |= mask;
            }
            else {
                if (c == 1) {
                    sum |= mask;
                    c = 0;
                }
            }

            mask <<= 1;
        }

        return sum;
    }
};

/**
 * Other user's solution from dicussion.
 * The static_cast is used to avoid the runtime error that one of the interger is negative.
 * Because negative number shift is not well defined in c++ standard.
 */
class Solution {
public:
    int getSum(int a, int b) {
        int sum = a;

        while (b != 0)
        {
            sum = a ^ b; //calculate sum of a and b without thinking the carry
            b = static_cast<unsigned int>(a & b) << 1; //calculate the carry
            a = sum;
        }

        return sum;
    }
};
