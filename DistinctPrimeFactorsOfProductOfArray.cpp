/**
 * Distinct Prime Factors of Product of Array
 *
 * Given an array of positive integers nums, return the number of distinct prime
 * factors in the product of the elements of nums.
 *
 * Note that:
 *
 * A number greater than 1 is called prime if it is divisible by only 1 and itself.
 * An integer val1 is a factor of another integer val2 if val2 / val1 is an integer.
 *
 * Example 1:
 *
 * Input: nums = [2,4,3,7,10,6]
 * Output: 4
 * Explanation:
 * The product of all the elements in nums is: 2 * 4 * 3 * 7 * 10 * 6 = 10080 = 25 * 32 * 5 * 7.
 * There are 4 distinct prime factors so we return 4.
 * Example 2:
 *
 * Input: nums = [2,4,8,16]
 * Output: 1
 * Explanation:
 * The product of all the elements in nums is: 2 * 4 * 8 * 16 = 1024 = 210.
 * There is 1 distinct prime factor so we return 1.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 104
 * 2 <= nums[i] <= 1000
 */
/**
 * Optimization. There is a fact that a number n can only have at most one prime
 * factor that greater than sqrt(n).
 */
class Solution {
public:
    int distinctPrimeFactors(vector<int>& nums) {
        auto primes = std::vector<int>{2,3,5,7,11,13,17,19,23,29,31};
        auto ans = unordered_set<int>{};
        for (auto n : nums) {
            for (auto p : primes) {
                if (0 == (n % p)) {
                    ans.insert(p);
                    do {
                        n /= p;
                    } while (0 == (n % p));
                }
            }

            if (n > 1) {
                ans.insert(n);
            }
        }

        return ans.size();
    }
};

/**
 * Original solution. Precompute the primes within 1000.
 */
class Solution {
public:
    int distinctPrimeFactors(vector<int>& nums) {
        auto primes = std::vector<int>{2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,181,191,193,197,199,211,223,227,229,233,239,241,251,257,263,269,271,277,281,283,293,307,311,313,317,331,337,347,349,353,359,367,373,379,383,389,397,401,409,419,421,431,433,439,443,449,457,461,463,467,479,487,491,499,503,509,521,523,541,547,557,563,569,571,577,587,593,599,601,607,613,617,619,631,641,643,647,653,659,661,673,677,683,691,701,709,719,727,733,739,743,751,757,761,769,773,787,797,809,811,821,823,827,829,839,853,857,859,863,877,881,883,887,907,911,919,929,937,941,947,953,967,971,977,983,991,997};
        auto ans = unordered_set<int>{};
        for (auto n : nums) {
            for (auto p : primes) {
                if (p > n) {
                    break;
                }

                if (0 == (n % p)) {
                    ans.insert(p);
                }
            }
        }
        return ans.size();
    }
};
