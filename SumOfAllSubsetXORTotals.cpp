/**
 * Sum of All Subset XOR Totals
 *
 * The XOR total of an array is defined as the bitwise XOR of all its elements, or 0 if the array is
 * empty.
 *
 * For example, the XOR total of the array [2,5,6] is 2 XOR 5 XOR 6 = 1.  Given an array nums,
 * return the sum of all XOR totals for every subset of nums.
 *
 * Note: Subsets with the same elements should be counted multiple times.
 *
 * An array a is a subset of an array b if a can be obtained from b by deleting some (possibly zero)
 * elements of b.
 *
 * Example 1:
 *
 * Input: nums = [1,3]
 * Output: 6
 * Explanation: The 4 subsets of [1,3] are:
 * - The empty subset has an XOR total of 0.
 * - [1] has an XOR total of 1.
 * - [3] has an XOR total of 3.
 * - [1,3] has an XOR total of 1 XOR 3 = 2.
 * 0 + 1 + 3 + 2 = 6
 * Example 2:
 *
 * Input: nums = [5,1,6]
 * Output: 28
 * Explanation: The 8 subsets of [5,1,6] are:
 * - The empty subset has an XOR total of 0.
 * - [5] has an XOR total of 5.
 * - [1] has an XOR total of 1.
 * - [6] has an XOR total of 6.
 * - [5,1] has an XOR total of 5 XOR 1 = 4.
 * - [5,6] has an XOR total of 5 XOR 6 = 3.
 * - [1,6] has an XOR total of 1 XOR 6 = 7.
 * - [5,1,6] has an XOR total of 5 XOR 1 XOR 6 = 2.
 * 0 + 5 + 1 + 6 + 4 + 3 + 7 + 2 = 28
 * Example 3:
 *
 * Input: nums = [3,4,5,6,7,8]
 * Output: 480
 * Explanation: The sum of all XOR totals for every subset is 480.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 12
 * 1 <= nums[i] <= 20
 */
/**
 * O(n) solution. Bit of hard to grasp.
 * IDEA:
 * consider ith bit for the final result.
 * If there is no element whose ith bit is 1, then all xor subset sums has 0 in ith bit;
 * If there are k (k>=1) elements whose ith bits are 1, then there are in total
 * comb(k, 1) + comb(k,3) + .. = 2**(k-1) ways to select odd number our of these k elements to make
 * the subset xor sum's ith bit to be 1, and there are 2**(n-k) ways to choose from the remaining
 * elements whose ith bits are 0s. Therefore, we have in total 2**(k-1) * 2**(n-k) = 2**(n-1) subsets
 * whose xor sums have 1 in their ith bit, which means the contribution to the final sum is
 * 2**(n-1) * 2**i. Notice this result is irrelevant to k.
 *
 * So we only need to determine whether there is any element whose ith bit is 1. We use bitwise OR sum
 * to do this.
 */
class Solution {
public:
    int subsetXORSum(vector<int>& nums) {
        return accumulate(begin(nums), end(nums), 0, bit_or<int>()) << (nums.size() - 1);
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int subsetXORSum(vector<int>& nums) {
        dfs(nums, 0, 0);
        return sum;
    }

private:
    void dfs(vector<int> const& nums, int idx, int cur)
    {
        if (idx == nums.size()) {
            sum += cur;
            return;
        }

        dfs(nums, idx + 1, cur ^ nums[idx]);
        dfs(nums, idx + 1, cur);
    }

    int sum = 0;
};
