/**
 * Longest Substring Without Repeating Characters
 *
 * Given a string s, find the length of the longest substring without repeating characters.
 *
 * Example 1:
 *
 * Input: s = "abcabcbb"
 * Output: 3
 * Explanation: The answer is "abc", with the length of 3.
 * Example 2:
 *
 * Input: s = "bbbbb"
 * Output: 1
 * Explanation: The answer is "b", with the length of 1.
 * Example 3:
 *
 * Input: s = "pwwkew"
 * Output: 3
 * Explanation: The answer is "wke", with the length of 3.
 * Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.
 * Example 4:
 *
 * Input: s = ""
 * Output: 0
 *
 * Constraints:
 *
 * 0 <= s.length <= 5 * 104
 * s consists of English letters, digits, symbols and spaces.
 */
/**
 * Even faster.
 */
class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        array<int, 256> char_pos;
        char_pos.fill(-1);

        int ans = 0;
        for (int L = -1, R = 0; R < s.length(); ++R) {
            if (char_pos[s[R]] > L) {
                L = char_pos[s[R]];
            }
            char_pos[s[R]] = R;
            ans = max(ans, R - L);
        }

        return ans;
    }
};

/**
 * More efficient one.
 */
class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        unordered_map<char, int> char_count;
        int duplicates = 0;
        int ans = 0;
        for (int L = 0, R = 0; R < s.length(); ++R) {
            ++char_count[s[R]];
            duplicates += char_count[s[R]] > 1;

            if (duplicates) {
                duplicates -= char_count[s[L]] > 1;
                --char_count[s[L]];
                ++L;
            }
            else {
                ans = max(ans, R - L + 1);
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        map<char, int> char_count;
        int ans = 0;
        for (int L = 0, R = 0; R < s.length(); ++R) {
            ++char_count[s[R]];
            bool has_duplicate = false;
            for (auto [ch, cnt] : char_count) {
                if (cnt > 1) {
                    has_duplicate = true;
                    break;
                }
            }

            if (has_duplicate) {
                --char_count[s[L]];
                ++L;
            }
            else {
                ans = max(ans, R - L + 1);
            }
        }

        return ans;
    }
};
