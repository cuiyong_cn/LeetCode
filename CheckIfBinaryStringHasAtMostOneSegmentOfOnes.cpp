/**
 * Check if Binary String Has at Most One Segment of Ones
 *
 * Given a binary string s without leading zeros, return true if s contains at most one contiguous
 * segment of ones. Otherwise, return
 * false.
 *
 * Example 1:
 *
 * Input: s = "1001"
 * Output: false
 * Explanation: The ones do not form a contiguous segment.
 * Example 2:
 *
 * Input: s = "110"
 * Output: true
 *
 * Constraints:
 *
 * 1 <= s.length <= 100
 * s[i] is either '0' or '1'.
 * s[0] is '1'.
 */
/**
 * Improvement.
 */
class Solution {
public:
    bool checkOnesSegment(string s) {
        return s.find("01") == string::npos;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool checkOnesSegment(string s) {
        auto pos = s.find('0');
        if (string::npos == pos) {
            return true;
        }

        return s.find('1', pos + 1) == string::npos;
    }
};
