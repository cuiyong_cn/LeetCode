/**
 * Combinations
 *
 * Given two integers n and k, return all possible combinations of k numbers out of 1 ... n.
 *
 * Example:
 *
 * Input: n = 4, k = 2
 * Output:
 * [
 *   [2,4],
 *   [3,4],
 *   [2,3],
 *   [1,2],
 *   [1,3],
 *   [1,4],
 * ]
 */
/**
 * Found this amazing iterative version from dicussion. Also some users use DP solution.  The idea is simple, if the
 * combination k out of n (select k elements from [1,n]) is combine(k, n).
 *
 * Let's consider how can we get combine(k, n) by adding the last element n to something we already have (combine(k - 1,
 * n
 * - 1) and combine(k, n - 1)). Actually, the combine(k, n) has two parts, one part is all combinations without n, it's
 *   combine(k, n - 1), another is all combinations with n, which can be gotten by appending n to every element in
 *   combine(k
 * - 1, n - 1). Note, the combine(i, i) is what we can get directly.
 */
class Solution
{
public:
    vector<vector<int>> combine(int n, int k)
    {
        vector<vector<int>> res;
        vector<int> curr(k, 0);
        int i = 0;
        while (i >= 0) {
            curr[i]++;
            if (curr[i] > (n - k + 1 + i)) {
                --i;
            }
            else if (i == k - 1) {
                res.push_back(curr);
            }
            else {
                i++;
                curr[i] = curr[i - 1];
            }
        }
        return res;
    }
};

/**
 * original solution. dfs solution.
 */
class Solution
{
public:
    vector<vector<int>> ans;
    int K;
    int N;

    vector<vector<int>> combine(int n, int k)
    {
        K = k;
        N = n;
        for (int i = 1; i <= (n - k + 1); ++i) {
            vector<int> combo{i};
            dfs(combo, 1, i + 1);
        }

        return ans;
    }

    void dfs(vector<int>& cur, int count, int start)
    {
        if (count == K) {
            ans.push_back(cur);
            return;
        }

        if (start > N) return;

        dfs(cur, count, start + 1);
        cur.push_back(start);
        dfs(cur, count + 1, start + 1);
        cur.pop_back();
    }
};

