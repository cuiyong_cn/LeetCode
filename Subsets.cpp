/**
 * Subsets
 *
 * Given a set of distinct integers, nums, return all possible subsets (the power set).
 *
 * Note: The solution set must not contain duplicate subsets.
 *
 * Example:
 *
 * Input: nums = [1,2,3]
 * Output:
 * [
 *   [3],
 *   [1],
 *   [2],
 *   [1,2,3],
 *   [1,3],
 *   [2,3],
 *   [1,2],
 *   []
 * ]
 */
/**
 * My original solution. using dfs.
 */
class Solution {
public:
    vector<vector<int>> subsets(vector<int>& nums) {
        list<int> subset;
        dfs(nums, subset, 0);
        return ans;
    }
private:
    void dfs(vector<int>& nums, list<int>& subset, int i) {
        if (i < nums.size()) {
            subset.push_back(nums[i]);
            dfs(nums, subset, i + 1);
            subset.pop_back();
            dfs(nums, subset, i + 1);
        }
        else {
            ans.push_back(vector<int>(subset.begin(), subset.end()));
        }
    }
    vector<vector<int> > ans;
};

/**
 * Iterative method. The idea is:
 * Using [1, 2, 3] as an example, the iterative process is like:
 *
 *     Initially, one empty subset [[]]
 *     Adding 1 to []: [[], [1]];
 *     Adding 2 to [] and [1]: [[], [1], [2], [1, 2]];
 *     Adding 3 to [], [1], [2] and [1, 2]: [[], [1], [2], [1, 2], [3], [1, 3], [2, 3], [1, 2, 3]].
 */
class Solution {
public:
    vector<vector<int>> subsets(vector<int>& nums) {
        vector<vector<int>> subs = {{}};
        for (int num : nums) {
            int n = subs.size();
            for (int i = 0; i < n; i++) {
                subs.push_back(subs[i]);
                subs.back().push_back(num);
            }
        }
        return subs;
    }
};

/**
 * Intresting solution using bitmask.
 *
 * Take [1, 2, 3] as an example. each number has only two possibilities: either in or not in a subset.
 *       0  0  0  ---->  []
 *       0  0  1  ---->  [3]
 *       0  1  0  ---->  [2]
 *       0  1  1  ---->  [2, 3]
 *       1  0  0  ---->  [1]
 *       1  0  1  ---->  [1, 3]
 *       1  1  0  ---->  [1, 2]
 *       1  1  1  ---->  [1, 2, 3]
 *
 * The following solution can not handle the situation when nums's size > 32;
 */
class Solution {
public:
    vector<vector<int>> subsets(vector<int>& nums) {
        int n = nums.size(), p = 1 << n;
        vector<vector<int>> subs(p);
        for (int i = 0; i < p; i++) {
            for (int j = 0; j < n; j++) {
                if ((i >> j) & 1) {
                    subs[i].push_back(nums[j]);
                }
            }
        }
        return subs;
    }
};
