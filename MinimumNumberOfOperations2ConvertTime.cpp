/**
 * Minimum Number of Operations to Convert Time
 *
 * You are given two strings current and correct representing two 24-hour times.
 *
 * 24-hour times are formatted as "HH:MM", where HH is between 00 and 23, and MM is between 00 and
 * 59. The earliest 24-hour time is 00:00, and the latest is 23:59.
 *
 * In one operation you can increase the time current by 1, 5, 15, or 60 minutes. You can perform
 * this operation any number of times.
 *
 * Return the minimum number of operations needed to convert current to correct.
 *
 * Example 1:
 *
 * Input: current = "02:30", correct = "04:35"
 * Output: 3
 * Explanation:
 * We can convert current to correct in 3 operations as follows:
 * - Add 60 minutes to current. current becomes "03:30".
 * - Add 60 minutes to current. current becomes "04:30".
 * - Add 5 minutes to current. current becomes "04:35".
 * It can be proven that it is not possible to convert current to correct in fewer than 3 operations.
 * Example 2:
 *
 * Input: current = "11:00", correct = "11:01"
 * Output: 1
 * Explanation: We only have to add one minute to current, so the minimum number of operations needed is 1.
 *
 * Constraints:
 *
 * current and correct are in the format "HH:MM"
 * current <= correct*
 */
/**
 * Original solution.
 */
int to_minutes(string const& time)
{
    return ((time[0] - '0') * 10 + (time[1] - '0')) * 60
         + ((time[3] - '0') * 10 + (time[4] - '0'));
}

int time_diff_in_minutes(string const& from, string const& to)
{
    auto minutes_from = to_minutes(from);
    auto minutes_to = to_minutes(to);
    return minutes_to >= minutes_from ?
        minutes_to - minutes_from : (24 * 60 - minutes_from) + minutes_to;
}

class Solution {
public:
    int convertTime(string current, string correct) {
        auto minutes_diff = time_diff_in_minutes(current, correct);
        auto ans = 0;
        for (auto s : {60, 15, 5, 1}) {
            auto cnt = minutes_diff / s;
            ans += cnt;
            minutes_diff -= cnt * s;
        }

        return ans;
    }
};
