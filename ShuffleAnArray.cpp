/**
 * Shuffle an Array
 *
 * Shuffle a set of numbers without duplicates.
 *
 * Example:
 *
 * // Init an array with set 1, 2, and 3.
 * int[] nums = {1,2,3};
 * Solution solution = new Solution(nums);
 *
 * // Shuffle the array [1,2,3] and return its result. Any permutation of [1,2,3] must equally likely to be returned.
 * solution.shuffle();
 *
 * // Resets the array back to its original configuration [1,2,3].
 * solution.reset();
 *
 * // Returns the random shuffling of array [1,2,3].
 * solution.shuffle();
 */
/**
 * Fisher-yates solution. Probably the right solution.
 */
class Solution {
public:
    vector<int> backup;
    vector<int> nums;
    pair<int, int> lastSwapIndex;

    Solution(vector<int>& nums) : nums{nums}, lastSwapIndex{0, 0} {
        backup = nums;
        srand(time(0));
    }

    /** Resets the array to its original configuration and return it. */
    vector<int> reset() {
        nums = backup;
        return backup;
    }

    /** Returns a random shuffling of the array. */
    vector<int> shuffle() {
        for (int i = nums.size() - 1; i > 0; --i) {
            int j = rand() % (i + 1);
            swap(nums[i], nums[j]);
        }

        return nums;
    }
};

/**
 * original solution. I think this is a wrong solution but passed. (test case does not handle well)
 * The distribution of the permutation is clearly not normal.
 */
class Solution {
public:
    vector<int> backup;
    vector<int> nums;
    pair<int, int> lastSwapIndex;

    Solution(vector<int>& nums) : nums{nums}, lastSwapIndex{0, 0} {
        backup = nums;
        srand(0);
    }

    /** Resets the array to its original configuration and return it. */
    vector<int> reset() {
        return backup;
    }

    /** Returns a random shuffling of the array. */
    vector<int> shuffle() {
        if (nums.empty()) return {};

        int n = lastSwapIndex.first;
        int m = lastSwapIndex.second;

        n = (n + rand()) % nums.size();
        m = (m + rand()) % nums.size();

        swap(nums[n], nums[m]);

        lastSwapIndex = {n, m};
        return nums;
    }
};

/**
 * Your Solution object will be instantiated and called as such:
 * Solution* obj = new Solution(nums);
 * vector<int> param_1 = obj->reset();
 * vector<int> param_2 = obj->shuffle();
 */
