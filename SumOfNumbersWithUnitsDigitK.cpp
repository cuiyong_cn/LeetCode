/**
 * Sum of Numbers With Units Digit K
 *
 * Given two integers num and k, consider a set of positive integers with the following properties:
 *
 * The units digit of each integer is k.
 * The sum of the integers is num.
 * Return the minimum possible size of such a set, or -1 if no such set exists.
 *
 * Note:
 *
 * The set can contain multiple instances of the same integer, and the sum of an empty set is considered 0.
 * The units digit of a number is the rightmost digit of the number.
 *
 * Example 1:
 *
 * Input: num = 58, k = 9
 * Output: 2
 * Explanation:
 * One valid set is [9,49], as the sum is 58 and each integer has a units digit of 9.
 * Another valid set is [19,39].
 * It can be shown that 2 is the minimum possible size of a valid set.
 * Example 2:
 *
 * Input: num = 37, k = 2
 * Output: -1
 * Explanation: It is not possible to obtain a sum of 37 using only integers that have a units digit of 2.
 * Example 3:
 *
 * Input: num = 0, k = 7
 * Output: 0
 * Explanation: The sum of an empty set is considered 0.
 *
 * Constraints:
 *
 * 0 <= num <= 3000
 * 0 <= k <= 9
 */
/**
 * Math Solution.
 * Intuition.
 *  assume there is a set satify:
 *      num = a1 + a2 + a3 + ... + an
 *
 *      since a1, a2, ..., an all have k as their right most digit.
 *
 *      num = n * k + 10 * (k1 + k2 + k3 + ... kn)
 *      num - (n * k) =  10 * (k1 + k2 + k3 + ... kn)
 */
class Solution {
public:
    int minimumNumbers(int sum, int k) {
        if (sum == 0) {
            return 0;
        }

        for (int i = 1; i <= 10; ++i) {
            auto diff = num - i * k;
            if (diff < 0) {
                break;
            }
            if (0 == (diff % 10)) {
                return i;
            }
        }

        return -1;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int minimumNumbers(int num, int k) {
        if (0 == num) {
            return 0;
        }

        auto right_digit = num % 10;
        if (0 == k) {
            return 0 == right_digit ? 1 : -1;
        }

        if (0 == (k % 2)) {
            if (1 == (num % 2)) {
                return -1;
            }
        }

        if (5 == k) {
            if (0 != right_digit && 5 != right_digit) {
                return -1;
            }
        }

        return dfs(num, k);
    }

private:
    int dfs(int num, int k)
    {
        if (num < k) {
            return -1;
        }

        auto right_digit = num % 10;
        if (right_digit == k) {
            return 1;
        }

        auto integer = num - right_digit + k + (right_digit > k ? 0 : -10);
        while (integer >= k) {
            auto size = dfs(num - integer, k);
            if (size > 0) {
                return 1 + size;
            }
            else {
                integer -= 10;
            }
        }

        return -1;
    }
};
