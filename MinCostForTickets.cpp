/**
 * Minimum Cost For Tickets
 *
 * In a country popular for train travel, you have planned some train travelling one
 * year in advance.  The days of the year that you will travel is given as an array days.
 * Each day is an integer from 1 to 365.
 *
 * Train tickets are sold in 3 different ways:
 *
 *     a 1-day pass is sold for costs[0] dollars;
 *     a 7-day pass is sold for costs[1] dollars;
 *     a 30-day pass is sold for costs[2] dollars.
 *
 * The passes allow that many days of consecutive travel.  For example, if we get a 7-day
 * pass on day 2, then we can travel for 7 days: day 2, 3, 4, 5, 6, 7, and 8.
 *
 * Return the minimum number of dollars you need to travel every day in the given list of days.
 *
 * Example 1:
 *
 * Input: days = [1,4,6,7,8,20], costs = [2,7,15]
 * Output: 11
 * Explanation:
 * For example, here is one way to buy passes that lets you travel your travel plan:
 * On day 1, you bought a 1-day pass for costs[0] = $2, which covered day 1.
 * On day 3, you bought a 7-day pass for costs[1] = $7, which covered days 3, 4, ..., 9.
 * On day 20, you bought a 1-day pass for costs[0] = $2, which covered day 20.
 * In total you spent $11 and covered all the days of your travel.
 *
 * Example 2:
 *
 * Input: days = [1,2,3,4,5,6,7,8,9,10,30,31], costs = [2,7,15]
 * Output: 17
 * Explanation:
 * For example, here is one way to buy passes that lets you travel your travel plan:
 * On day 1, you bought a 30-day pass for costs[2] = $15 which covered days 1, 2, ..., 30.
 * On day 31, you bought a 1-day pass for costs[0] = $2 which covered day 31.
 * In total you spent $17 and covered all the days of your travel.
 *
 * Note:
 *
 *     1 <= days.length <= 365
 *     1 <= days[i] <= 365
 *     days is in strictly increasing order.
 *     costs.length == 3
 *     1 <= costs[i] <= 1000
 */
/**
 * Improved version
 */
class Solution {
private:
    int dp(int i, vector<int>& days, vector<int>& memo, vector<int>& costs) {
        if (i >= days.size()) return 0;
        if (memo[i] > 0) return memo[i];

        int ans = INT_MAX;
        int j = i;
        vector<int> durations{1, 7, 30};
        for (int k = 0; k < 3; ++k) {
            while (j < days.size() && days[j] < days[i] + durations[k]) {
                ++j;
            }
            ans = min(ans, dp(j, days, memo, costs) + costs[k]);
        }
        memo[i] = ans;
        return ans;
    }
public:
    int mincostTickets(vector<int>& days, vector<int>& costs) {
        vector<int> memo(days.size());

        return dp(0, days, memo, costs);
    }
};
/**
 * Dynamic Programming
 */
class Solution {
public:
    int dp(int day, set<int>& dayset, vector<int>& memo, vector<int>& costs) {
        if (day > 365) return 0;

        if (memo[day] > 0) return memo[day];

        int ans = 0;
        if (dayset.count(day)) {
            ans = min(dp(day + 1, dayset, memo, costs) + costs[0], dp(day + 7, dayset, memo, costs) + costs[1]);
            ans = min(ans, dp(day + 30, dayset, memo, costs) + costs[2]);
        }
        else {
            ans = dp(day + 1, dayset, memo, costs);
        }
        memo[day] = ans;
        return ans;
    }
public:
    int mincostTickets(vector<int>& days, vector<int>& costs) {
        vector<int> memo(366, 0);
        set<int> dayset(days.begin(), days.end());
        return dp(1, dayset, memo, costs);
    }
};
