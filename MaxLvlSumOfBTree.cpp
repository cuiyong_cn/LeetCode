/**
 * Given the root of a binary tree, the level of its root is 1, the level of its
 * children is 2, and so on.
 * 
 * Return the smallest level X such that the sum of all the values of nodes at level
 * X is maximal.
 *
 * Example 1:
 *             1
 *            / \
 *           7   0
 *          / \
 *         7   -8
 * 
 * Input: [1,7,0,7,-8,null,null]
 * Output: 2
 * Explanation: 
 * Level 1 sum = 1.
 * Level 2 sum = 7 + 0 = 7.
 * Level 3 sum = 7 + -8 = -1.
 * So we return the level with the maximum sum which is level 2.
 * 
 * Note:
 * 
 *     1. The number of nodes in the given tree is between 1 and 10^4.
 *     2. -10^5 <= node.val <= 10^5
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/**
 * Breadth first search mechanism
 *
 * class Solution {
 * public:
 *     int maxLevelSum(TreeNode* root) {
 *         if (!root) return 0;
 *
 *         int res = 0, level = 0, global_sum = 0;
 *         queue<TreeNode*> q{root};
 *         while (!q.empty()) {
 *             int count = q.size(), tmp_sum = 0;
 *             while (count--) {
 *                 TreeNode* tmp = q.front();
 *                 q.pop();
 *                 tmp_sum += tmp->val;
 *                 if (tmp->left) q.push(tmp->left);
 *                 if (tmp->right) q.push(tmp->right);
 *             }
 *             level++;
 *             if (tmp_sum > global_sum) res = level;
 *             global_sum = max(tmp_sum, global_sum);
 *         }
 *         return res;
 *     }
 * };
 *
 *
 * Depth first search mechanism
 */
class Solution {
public:
    void dfsLvlSum(TreeNode* t, map<int, int>& m, int lvl)
    {
        if (t == nullptr) return;
        m[lvl] += t->val;
        dfsLvlSum(t->left, m, lvl + 1);
        dfsLvlSum(t->right, m, lvl + 1);
    }
    
    int maxLevelSum(TreeNode* root) {
        map<int, int> lvlSum;
        dfsLvlSum(root, lvlSum, 1);
        auto it = lvlSum.begin();
        int max = it->second;
        int level = 0;
        for (; it != lvlSum.end(); ++it) {
            if (it->second > max) {
                max = it->second;
                level = it->first;
            }
        }
        
        return level;
    }
};
