/**
 * Find Valid Matrix Given Row and Column Sums
 *
 * You are given two arrays rowSum and colSum of non-negative integers where rowSum[i] is the sum of
 * the elements in the ith row and colSum[j] is the sum of the elements of the jth column of a 2D
 * matrix. In other words, you do not know the elements of the matrix, but you do know the sums of
 * each row and column.
 *
 * Find any matrix of non-negative integers of size rowSum.length x colSum.length that satisfies the
 * rowSum and colSum requirements.
 *
 * Return a 2D array representing any matrix that fulfills the requirements. It's guaranteed that at
 * least one matrix that fulfills the requirements exists.
 *
 * Example 1:
 *
 * Input: rowSum = [3,8], colSum = [4,7]
 * Output: [[3,0],
 *          [1,7]]
 * Explanation:
 * 0th row: 3 + 0 = 3 == rowSum[0]
 * 1st row: 1 + 7 = 8 == rowSum[1]
 * 0th column: 3 + 1 = 4 == colSum[0]
 * 1st column: 0 + 7 = 7 == colSum[1]
 * The row and column sums match, and all matrix elements are non-negative.
 * Another possible matrix is: [[1,2],
 *                              [3,5]]
 * Example 2:
 *
 * Input: rowSum = [5,7,10], colSum = [8,6,8]
 * Output: [[0,5,0],
 *          [6,1,0],
 *          [2,0,8]]
 *
 * Constraints:
 *
 * 1 <= rowSum.length, colSum.length <= 500
 * 0 <= rowSum[i], colSum[i] <= 108
 * sum(rows) == sum(columns)
 */
/**
 * priority_queue not needed.
 */
class Solution {
public:
    vector<vector<int>> restoreMatrix(vector<int>& rs, vector<int>& cs)
    {
        vector<vector<int>> m(rs.size(), vector<int>(cs.size()));
        for (int x = 0, y = 0; x < rs.size() && y < cs.size(); ) {
            m[x][y] = min(rs[x], cs[y]);
            rs[x] -= m[x][y];
            cs[y] -= m[x][y];
            x += rs[x] == 0;
            y += cs[y] == 0;
        }

        return m;
    }
};

/**
 * Original solution.
 */
class Solution {
    using sum_idx = pair<int, int>;
public:
    vector<vector<int>> restoreMatrix(vector<int>& rowSum, vector<int>& colSum) {
        int const M = rowSum.size();
        int const N = colSum.size();

        priority_queue<sum_idx, vector<sum_idx>, greater<sum_idx>> row_q;
        priority_queue<sum_idx, vector<sum_idx>, greater<sum_idx>> col_q;

        for (int i = 0; i < M; ++i) {
            row_q.emplace(rowSum[i], i);
        }
        for (int i = 0; i < N; ++i) {
            col_q.emplace(colSum[i], i);
        }

        vector<vector<int>> ans(M, vector<int>(N, 0));
        while (!row_q.empty() && !col_q.empty()) {
            auto [rval, ridx] = row_q.top(); row_q.pop();
            auto [cval, cidx] = col_q.top(); col_q.pop();

            int val = min(rval, cval);
            ans[ridx][cidx] = val;

            if (rval != val) {
                row_q.emplace(rval - val, ridx);
            }
            if (cval != val) {
                col_q.emplace(cval - val, cidx);
            }
        }

        return ans;
    }
};
