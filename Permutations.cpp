/**
 * Permutations
 *
 * Given a collection of distinct integers, return all possible permutations.
 *
 * Example:
 *
 * Input: [1,2,3]
 * Output:
 * [
 *   [1,2,3],
 *   [1,3,2],
 *   [2,1,3],
 *   [2,3,1],
 *   [3,1,2],
 *   [3,2,1]
 * ]
 *
 */
/**
 * More elegant approach. No extra space required.
 */
class Solution {
public:
    vector<vector<int> > permute(vector<int> &num) {
	    vector<vector<int> > result;
	    permuteRecursive(num, 0, result);
	    return result;
    }

	void permuteRecursive(vector<int> &num, int begin, vector<vector<int> > &result)	{
		if (begin >= num.size()) {
		    result.push_back(num);
		    return;
		}

		for (int i = begin; i < num.size(); i++) {
		    swap(num[begin], num[i]);
		    permuteRecursive(num, begin + 1, result);
		    // reset
		    swap(num[begin], num[i]);
		}
    }
};

/**
 * Backtracking approach.
 */
class Solution {
public:
    vector<vector<int>> permute(vector<int>& nums) {
        vector<vector<int>> perms;
        permute(nums, 0, perms);
        return perms;
    }
private:
    void permute(vector<int>& nums, int i, vector<vector<int>>& perms) {
        if (i == nums.size()) {
            perms.push_back(nums);
        } else {
            for (int j = i; j < nums.size(); j++) {
                swap(nums[i], nums[j]);
                permute(nums, i + 1, perms);
            }
        }
    }
};

/**
 * My original version.
 */
class Solution {
private:
    void dfs(vector<int>& nums, vector<int>& seen, vector<int>& perm, vector<vector<int>>& ans) {
        if (perm.size() == nums.size()) {
            ans.push_back(perm);
            return;
        }

        for (int i = 0; i < nums.size(); ++i) {
            if (0 == seen[i]) {
                vector<int> p = perm;
                seen[i] = 1;
                p.push_back(nums[i]);
                dfs(nums, seen, p, ans);
                seen[i] = 0;
            }
        }
    }
public:
    vector<vector<int>> permute(vector<int>& nums) {
        int size = nums.size();
        vector<int> seen(size, 0);
        vector<vector<int>> ans;
        for (int i = 0; i < size; ++i) {
            vector<int> perm{nums[i]};
            seen[i] = 1;
            dfs(nums, seen, perm, ans);
            seen[i] = 0;
        }
        return ans;
    }
};
