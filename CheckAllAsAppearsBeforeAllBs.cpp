/**
 * Check if All A's Appears Before All B's
 *
 * Given a string s consisting of only the characters 'a' and 'b', return true if every 'a' appears
 * before every 'b' in the string. Otherwise, return false.
 *
 * Example 1:
 *
 * Input: s = "aaabbb"
 * Output: true
 * Explanation:
 * The 'a's are at indices 0, 1, and 2, while the 'b's are at indices 3, 4, and 5.
 * Hence, every 'a' appears before every 'b' and we return true.
 * Example 2:
 *
 * Input: s = "abab"
 * Output: false
 * Explanation:
 * There is an 'a' at index 2 and a 'b' at index 1.
 * Hence, not every 'a' appears before every 'b' and we return false.
 * Example 3:
 *
 * Input: s = "bbb"
 * Output: true
 * Explanation:
 * There are no 'a's, hence, every 'a' appears before every 'b' and we return true.
 *
 * Constraints:
 *
 * 1 <= s.length <= 100
 * s[i] is either 'a' or 'b'.
 */
/**
 * Other solutions from discussion.
 */
bool checkString(string s)
{
    return s.find("ba")==string::npos;
}

bool checkString(string s)
{
    return is_sorted(s.begin(),s.end());
}

/**
 * Original solution.
 */
class Solution {
public:
    bool checkString(string s) {
        auto first_b = s.find('b');

        return string::npos == first_b ? true
            : all_of(begin(s) + first_b, end(s), [](auto c) { return 'b' == c; });
    }
};
