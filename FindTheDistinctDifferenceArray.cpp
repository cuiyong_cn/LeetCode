/**
 * Find the Distinct Difference Array
 *
 * You are given a 0-indexed array nums of length n.
 *
 * The distinct difference array of nums is an array diff of length n such that
 * diff[i] is equal to the number of distinct elements in the suffix nums[i + 1,
 * ..., n - 1] subtracted from the number of distinct elements in the prefix
 * nums[0, ..., i].
 *
 * Return the distinct difference array of nums.
 *
 * Note that nums[i, ..., j] denotes the subarray of nums starting at index i
 * and ending at index j inclusive. Particularly, if i > j then nums[i, ..., j]
 * denotes an empty subarray.
 *
 * Example 1:
 *
 * Input: nums = [1,2,3,4,5]
 * Output: [-3,-1,1,3,5]
 * Explanation: For index i = 0, there is 1 element in the prefix and 4 distinct elements in the suffix. Thus, diff[0] = 1 - 4 = -3.
 * For index i = 1, there are 2 distinct elements in the prefix and 3 distinct elements in the suffix. Thus, diff[1] = 2 - 3 = -1.
 * For index i = 2, there are 3 distinct elements in the prefix and 2 distinct elements in the suffix. Thus, diff[2] = 3 - 2 = 1.
 * For index i = 3, there are 4 distinct elements in the prefix and 1 distinct element in the suffix. Thus, diff[3] = 4 - 1 = 3.
 * For index i = 4, there are 5 distinct elements in the prefix and no elements in the suffix. Thus, diff[4] = 5 - 0 = 5.
 * Example 2:
 *
 * Input: nums = [3,2,3,4,2]
 * Output: [-2,-1,0,2,3]
 * Explanation: For index i = 0, there is 1 element in the prefix and 3 distinct elements in the suffix. Thus, diff[0] = 1 - 3 = -2.
 * For index i = 1, there are 2 distinct elements in the prefix and 3 distinct elements in the suffix. Thus, diff[1] = 2 - 3 = -1.
 * For index i = 2, there are 2 distinct elements in the prefix and 2 distinct elements in the suffix. Thus, diff[2] = 2 - 2 = 0.
 * For index i = 3, there are 3 distinct elements in the prefix and 1 distinct element in the suffix. Thus, diff[3] = 3 - 1 = 2.
 * For index i = 4, there are 3 distinct elements in the prefix and no elements in the suffix. Thus, diff[4] = 3 - 0 = 3.
 *
 * Constraints:
 *
 * 1 <= n == nums.length <= 50
 * 1 <= nums[i] <= 50
 */
/**
 * Original solution.
 */
template<typename Iter>
auto distinct_cnt(Iter b, Iter e)
{
    using value_type = typename Iter::value_type;
    auto cnt = vector<value_type>{};
    auto s = unordered_set<value_type>{};
    for (; b != e; ++b) {
        s.insert(*b);
        cnt.push_back(s.size());
    }
    return cnt;
}

auto prefix_distinct(vector<int> const& nums)
{
    return distinct_cnt(begin(nums), end(nums));
}

auto suffix_distinct(vector<int> const& nums)
{
    auto cnt = distinct_cnt(rbegin(nums), rend(nums));
    reverse(begin(cnt), end(cnt));
    return cnt;
}

class Solution {
public:
    vector<int> distinctDifferenceArray(vector<int>& nums) {
        auto n = static_cast<int>(nums.size());
        auto prefix_cnt = prefix_distinct(nums);
        auto suffix_cnt = suffix_distinct(nums);
        auto ans = vector<int>(n, 0);
        for (auto i = 0; i < (n - 1); ++i) {
            ans[i] = prefix_cnt[i] - suffix_cnt[i + 1];
        }
        ans.back() = prefix_cnt.back();
        return ans;
    }
};
