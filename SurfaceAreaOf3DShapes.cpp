/**
 * Surface Area of 3D Shapes
 *
 * On a N * N grid, we place some 1 * 1 * 1 cubes.
 *
 * Each value v = grid[i][j] represents a tower of v cubes placed on top of grid cell (i, j).
 *
 * Return the total surface area of the resulting shapes.
 *
 * Example 1:
 *
 * Input: [[2]]
 * Output: 10
 *
 * Example 2:
 *
 * Input: [[1,2],[3,4]]
 * Output: 34
 *
 * Example 3:
 *
 * Input: [[1,0],[0,2]]
 * Output: 16
 *
 * Example 4:
 *
 * Input: [[1,1,1],[1,0,1],[1,1,1]]
 * Output: 32
 *
 * Example 5:
 *
 * Input: [[2,2,2],[2,1,2],[2,2,2]]
 * Output: 46
 *
 * Note:
 *
 *     1 <= N <= 50
 *     0 <= grid[i][j] <= 50
 */
/**
 * Improved my original solution, we only have to consider left, and back side.
 */
class Solution {
public:
    int surfaceArea(vector<vector<int>>& grid) {
        int area = 0;
        int rows = grid.size();
        int cols = rows;
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                if (grid[i][j] <= 0) continue;

                area += 4 * grid[i][j] + 2;
                // left overlap
                if (j > 0) {
                    area -= min(grid[i][j], grid[i][j - 1]) << 1;
                }
                // back overlap
                if (i > 0) {
                    area -= min(grid[i][j], grid[i - 1][j]) << 1;
                }
            }
        }
        return area;
    }
};

/**
 * My original solution. Using overlap concept. We could eliminate this.
 */
class Solution {
public:
    int surfaceArea(vector<vector<int>>& grid) {
        int area = 0;
        int rows = grid.size();
        int cols = rows;
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                if (grid[i][j] <= 0) continue;

                area += 6 * grid[i][j] - 2 * max(grid[i][j] - 1, 0);
                // left overlap
                if ((j - 1) >= 0) {
                    area -= min(grid[i][j], grid[i][j - 1]);
                }
                // back overlap
                if ((i - 1) >= 0) {
                    area -= min(grid[i][j], grid[i - 1][j]);
                }
                // right overlap
                if ((j + 1) < cols) {
                    area -= min(grid[i][j], grid[i][j + 1]);
                }
                // front overlap
                if ((i + 1) < rows) {
                    area -= min(grid[i][j], grid[i + 1][j]);
                }
            }
        }
        return area;
    }
};

/**
 * Without consider overlap. But seems nesting too deep.
 */
class Solution {
private:
    enum Direction
    {
        WEST,
        NORTH,
        EAST,
        SOUTH
    };
public:
    int surfaceArea(vector<vector<int>>& grid) {
        int area = 0;
        int rows = grid.size();
        int cols = rows;
        vector<int> dir{0, -1, 0, 1, 0};
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                if (grid[i][j] > 0) {
                    area += 2;
                    for (int d = WEST; d < 4; ++d) {
                        int nr = i + dir[d];
                        int nc = j + dir[d + 1];
                        int nv = 0;
                        if (0 <= nr && nr < rows && 0 <= nc && nc < cols) {
                            nv = grid[nr][nc];
                        }
                        area += max(grid[i][j] - nv, 0);
                    }
                }
            }
        }
        return area;
    }
};
