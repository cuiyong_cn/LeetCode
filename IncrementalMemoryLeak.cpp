/**
 * Incremental Memory Leak
 *
 * You are given two integers memory1 and memory2 representing the available memory in bits on two
 * memory sticks. There is currently a faulty program running that consumes an increasing amount of
 * memory every second.
 *
 * At the ith second (starting from 1), i bits of memory are allocated to the stick with more
 * available memory (or from the first memory stick if both have the same available memory). If
 * neither stick has at least i bits of available memory, the program crashes.
 *
 * Return an array containing [crashTime, memory1crash, memory2crash], where crashTime is the time
 * (in seconds) when the program crashed and memory1crash and memory2crash are the available bits of
 * memory in the first and second sticks respectively.
 *
 * Example 1:
 *
 * Input: memory1 = 2, memory2 = 2
 * Output: [3,1,0]
 * Explanation: The memory is allocated as follows:
 * - At the 1st second, 1 bit of memory is allocated to stick 1. The first stick now has 1 bit of available memory.
 * - At the 2nd second, 2 bits of memory are allocated to stick 2. The second stick now has 0 bits of available memory.
 * - At the 3rd second, the program crashes. The sticks have 1 and 0 bits available respectively.
 * Example 2:
 *
 * Input: memory1 = 8, memory2 = 11
 * Output: [6,0,4]
 * Explanation: The memory is allocated as follows:
 * - At the 1st second, 1 bit of memory is allocated to stick 2. The second stick now has 10 bit of available memory.
 * - At the 2nd second, 2 bits of memory are allocated to stick 2. The second stick now has 8 bits of available memory.
 * - At the 3rd second, 3 bits of memory are allocated to stick 1. The first stick now has 5 bits of available memory.
 * - At the 4th second, 4 bits of memory are allocated to stick 2. The second stick now has 4 bits of available memory.
 * - At the 5th second, 5 bits of memory are allocated to stick 1. The first stick now has 0 bits of available memory.
 * - At the 6th second, the program crashes. The sticks have 0 and 4 bits available respectively.
 *
 * Constraints:
 *
 * 0 <= memory1, memory2 <= 231 - 1
 */
/**
 * From discussion. O(1) solution.
 */
class Solution {
public:
    vector<int> memLeak(int memory1, int memory2) {
		// first step:
		// make m1 and m2 close
        int d = abs(memory1 - memory2);
        int k = sqrt(2L*d+0.25)-0.5;
        int total = (long)k*(k+1)/2;
        if (memory1 > memory2) {
            memory1 -= total;
        } else {
            memory2 -= total;
        }

		// swap m1 and m2 if necessary
        bool changed = false;
        if (memory2 > memory1) {
            swap(memory2, memory1);
            changed = true;
        }

		// get tail1 and tail2 from our equation
        int start = k+1;
        int t1 = sqrt(4L*memory1 + (long)start*(start-2) + 1L) - 1.0;
        int t2 = sqrt(4L*memory2 + (long)start*start) - 1.0;

		// checking parity
        if ((start&1) ^ (t1&1)) --t1;
        if (((start+1)&1) ^ (t2&1)) --t2;

		// get crash time, as well as the final stage of m1/m2
        int time = max(t1, t2)+1;
        memory1 -= (long)(start+t1)*(t1-start+2)/4;
        memory2 -= (long)(start+t2+1)*(t2-start+1)/4;

		// swap m1, m2 if needed
        if (changed) {
            return {time, memory2, memory1};
        }
        return {time, memory1, memory2};
    }
};

/**
 * Original solution.
 */
bool program_crash(int seconds, int mem1, int mem2)
{
    if (mem1 >= mem2) {
        return mem1 < seconds;
    }

    return mem2 < seconds;
}

class Solution {
public:
    vector<int> memLeak(int memory1, int memory2) {
        int seconds = 1;
        while (!program_crash(seconds, memory1, memory2)) {
            auto& mem = memory1 >= memory2 ? memory1 : memory2;
            mem -= seconds;
            ++seconds;
        }

        return {seconds, memory1, memory2};
    }
};
