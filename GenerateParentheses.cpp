/**
 * Generate Parentheses
 *
 * Given n pairs of parentheses, write a function to generate all combinations of
 * well-formed parentheses.
 *
 * For example, given n = 3, a solution set is:
 *
 * [
 *   "((()))",
 *   "(()())",
 *   "(())()",
 *   "()(())",
 *   "()()()"
 * ]
 */
/**
 * Improved version
 */
class Solution {
private:
    void dfs(vector<string>& ans, string s, int left, int right) {
        if (0 == left && 0 == right) {
            ans.push_back(s);
            return;
        }
        
        if (right > 0) { dfs(ans, s + ')', left, right - 1); }
        if (left > 0) { dfs(ans, s + '(', left - 1, right + 1); }
    }
public:
    vector<string> generateParenthesis(int n) {
        vector<string> ans;
        dfs(ans, "", n, 0);
        return ans;
    }
};
/**
 * My original verison. Simple and easy to understand. But leetcode report it
 * "Memory Limit Exceed"
 */
class Solution {
private:
    void dfs(vector<string>& ans, string& s, int num, int totalNum) {
        string s1 = '(' + s + ')';
        string s2 = s + "()";
        string s3 = "()" + s;

        if (num == totalNum) {
            ans.push_back(s1);
            ans.push_back(s2);
            if (s2 != s3) {
                ans.push_back(s3);
            }
        }
        else {
            dfs(ans, s1, num + 1, totalNum);
            dfs(ans, s2, num + 1, totalNum);
            if (s2 != s3) {
                dfs(ans, s3, num + 1, totalNum);
            }
        }
    }
public:
    vector<string> generateParenthesis(int n) {
        vector<string> ans;
        string s = "()";
        int num = 1;
        dfs(ans, s, num + 1, n);
        return ans;
    }
};
/**
 * Other approches from leetcode
 */
class Solution {
private:
    void dfs(vector<string>& ans, string& s, int pos) {
        if (pos == s.size()) {
            if (valid(s)) ans.push_back(s);
        }
        else {
            s[pos] = '(';
            dfs(ans, s, pos + 1);
            s[pos] = ')';
            dfs(ans, s, pos + 1);
        }
    }
    
    bool valid(string& s) {
        int balance = 0;
        for (auto& c : s) {
            if ('(' == c) ++balance;
            else --balance;
            if (balance < 0) return false;
        }
        return 0 == balance;
    }
public:
    vector<string> generateParenthesis(int n) {
        vector<string> ans;
        string current(2 * n, '\0');
        dfs(ans, current, 0);
        return ans;
    }
};
