/**
 * Hand of Straights
 *
 * Alice has a hand of cards, given as an array of integers.
 *
 * Now she wants to rearrange the cards into groups so that each group is size W, and consists of W consecutive cards.
 *
 * Return true if and only if she can.
 *
 * Example 1:
 *
 * Input: hand = [1,2,3,6,2,3,4,7,8], W = 3
 * Output: true
 * Explanation: Alice's hand can be rearranged as [1,2,3],[2,3,4],[6,7,8].
 *
 * Example 2:
 *
 * Input: hand = [1,2,3,4,5], W = 4
 * Output: false
 * Explanation: Alice's hand can't be rearranged into groups of 4.
 *
 * Note:
 *     1 <= hand.length <= 10000
 *     0 <= hand[i] <= 10^9
 *     1 <= W <= hand.length
 */
/**
 * Improved solution. But I think this solution is not 100% correct. See the comment.
 * Test case should be improved.
 */
class Solution {
public:
    bool isNStraightHand(vector<int>& hand, int W) {
        bool straight = true;

        if ((hand.size() % W) != 0) {
            straight = false;
        }
        else if (W > 1) {
            sort(hand.begin(), hand.end());
            int groupNum = hand.size() / W;
            bool swapped = false;

            for (int i = 0; i < groupNum; ++i) {
                int start = i * W, end = (i + 1) * W;
                if (swapped) {
                    /** If we want the 100% correctness, end should be hand.end() */
                    sort(hand.begin() + start, hand.begin() + (end == hand.size() ? end : end + W));
                    swapped = false;
                }

                for (int j = start + 1; j < end; ++j) {
                    if (swapped || (hand[j] == hand[j - 1])) {
                        swapped = true;
                        int target = hand[j - 1] + 1;
                        bool found = false;
                        for (int k = j + 1; k < hand.size(); ++k) {
                            if (hand[k] == target) {
                                swap(hand[j], hand[k]);
                                found = true;
                                break;
                            }
                        }

                        if (false == found) straight = false;
                    }
                    else if ((hand[j] - hand[j - 1]) != 1) {
                        straight = false;
                    }

                    if (false == straight) break;
                }

                if (false == straight) break;
            }
        }

        return straight;
    }
};

/**
 * original solution. Ugly one.
 */
class Solution {
public:
    bool isNStraightHand(vector<int>& hand, int W) {
        bool straight = true;

        if ((hand.size() % W) != 0) {
            straight = false;
        }
        else if (W > 1) {
            map<int, int> numCount;
            for (auto n : hand) ++numCount[n];
            int count = hand.size() / W;
            while (count > 0) {
                int start;
                for (auto& p : numCount) {
                    if (p.second > 0) {
                        start = p.first;
                        break;
                    }
                }

                for (int i = 0; i < W; ++i) {
                    auto it = numCount.find(start);
                    if (it == numCount.end()) {
                        straight = false;
                        break;
                    }
                    if (it->second == 0) {
                        straight = false;
                        break;
                    }
                    else {
                        --it->second;
                    }
                    ++start;
                }

                if (false == straight) break;
                --count;
            }
        }

        return straight;
    }
};

/**
 * Efficient map solution.
 */
class Solution {
public:
    bool isNStraightHand(vector<int>& hand, int W) {
        int n = hand.size();

        if (n == 0) return false;
        if (n % W != 0) return false;
        if (1 == W) return true;

        map<int,int>mp;

        for (auto num : hand) mp[num] ++;

        while (mp.size() >= W) {
            auto it = mp.begin();
            int cur_num = it->first, times = it->second;
            it = mp.erase(it);

            for(int i = 1; i < W; ++i) {
                int next = cur_num + i;
                if ((it->first != next)
                 || (it->second < times)) {
                    return false;
                }

                if (it->second > times) {
                    it->second = it->second - times;
                    it++;
                }
                else if (it->second == times) {
                    it = mp.erase(it);
                }
            }
        }

        return mp.empty();
    }
};

/**
 * short version from user lee215
 */
class Solution
{
public:
    bool isNStraightHand(vector<int> hand, int W) {
        map<int, int> c;
        for (int i : hand) c[i]++;
        for (auto it : c)
            if (c[it.first] > 0)
                for (int i = W - 1; i >= 0; --i)
                    if ((c[it.first + i] -= c[it.first]) < 0)
                        return false;
        return true;
    }
};
