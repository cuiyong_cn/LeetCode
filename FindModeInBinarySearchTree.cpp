/**
 * Find Mode in Binary Search Tree
 *
 * Given a binary search tree (BST) with duplicates, find all the mode(s) (the most frequently
 * occurred element) in the given BST.
 *
 * Assume a BST is defined as follows:
 *
 * The left subtree of a node contains only nodes with keys less than or equal to the node's key.
 * The right subtree of a node contains only nodes with keys greater than or equal to the node's key.
 * Both the left and right subtrees must also be binary search trees.
 *
 * For example:
 * Given BST [1,null,2,2],
 *
 *    1
 *     \
 *      2
 *     /
 *    2
 *
 * return [2].
 *
 * Note: If a tree has more than one mode, you can return them in any order.
 *
 * Follow up: Could you do that without using any extra space? (Assume that the implicit stack space
 * incurred due to recursion does not count).
 */
/**
 * O(1) space
 */
class Solution {
public:

    vector<int> findMode(TreeNode *root)
    {
        inorderTraversal(root);
        return ans;
    }

    void inorderTraversal(TreeNode *node)
    {
        if (!node) {
            return;
        }

        inorderTraversal(node->left);
        if (init) {
            currFreq = preval == node->val ? currFreq + 1 : 1;
        }
        else {
            init = true;
            currFreq = 1;
        }

        if (currFreq > maxFreq) {
            ans.clear();
            maxFreq = currFreq;
            ans.push_back(node->val);
        }
        else if (currFreq == maxFreq) {
            ans.push_back(node->val);
        }

        preval = node->val;
        inorderTraversal(node->right);
    }

private:
    bool init = false;
    int maxFreq = 0;
    int currFreq = 0;
    int preval = 0;
    vector<int> ans;
};

/**
 * Original solution.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> findMode(TreeNode* root) {
        std::unordered_map<int, int> data_freq;
        std::deque<TreeNode*> q{root};
        auto max_freq = 0;
        while (!q.empty()) {
            std::deque<TreeNode*> nq;
            for (auto node : q) {
                if (node) {
                    ++data_freq[node->val];
                    max_freq = std::max(max_freq, data_freq[node->val]);

                    nq.push_back(node->left);
                    nq.push_back(node->right);
                }
            }

            std::swap(q, nq);
        }

        vector<int> ans;
        for (auto df : data_freq) {
            if (max_freq == df.second) {
                ans.push_back(df.first);
            }
        }

        return ans;
    }
};
