/**
 * Iterator for Combination
 *
 * Design the CombinationIterator class:
 *
 * CombinationIterator(string characters, int combinationLength) Initializes the object with a
 * string characters of sorted distinct lowercase English letters and a number combinationLength as
 * arguments.  next() Returns the next combination of length combinationLength in lexicographical
 * order.  hasNext() Returns true if and only if there exists a next combination.
 *
 * Example 1:
 *
 * Input
 * ["CombinationIterator", "next", "hasNext", "next", "hasNext", "next", "hasNext"]
 * [["abc", 2], [], [], [], [], [], []]
 * Output
 * [null, "ab", true, "ac", true, "bc", false]
 *
 * Explanation
 * CombinationIterator itr = new CombinationIterator("abc", 2);
 * itr.next();    // return "ab"
 * itr.hasNext(); // return True
 * itr.next();    // return "ac"
 * itr.hasNext(); // return True
 * itr.next();    // return "bc"
 * itr.hasNext(); // return False
 *
 * Constraints:
 *
 * 1 <= combinationLength <= characters.length <= 15
 * All the characters of characters are unique.
 * At most 104 calls will be made to next and hasNext.
 * It's guaranteed that all calls of the function next are valid.
 */
/**
 * Optimize.
 */
class CombinationIterator {
public:
    CombinationIterator(string characters, int combinationLength) : m_combo_len{combinationLength}
    {
        auto m1 = (1 << characters.size()) - 1;
        auto m2 = ~((1 << (characters.size() - combinationLength)) - 1);
        m_mask = m1 & m2;
        m_chars = characters;
    }

    string next() {
        string ans(m_combo_len, '\0');
        int m = 0;
        for (int i = m_chars.size() - 1; i >= 0; --i) {
            if (m_mask & (1 << i)) {
                auto idx = m_chars.size() - i - 1;
                ans[m] = m_chars[idx];
                ++m;
            }
        }

        while (--m_mask > 0) {
            bitset<8 * sizeof(int)> bits(m_mask);
            if (bits.count() == m_combo_len) {
                break;
            }
        }

        return ans;
    }

    bool hasNext() {
        return m_mask != 0;
    }

private:
    int m_mask;
    int const m_combo_len;
    string m_chars;
};

/**
 * Original solution.
 */
class CombinationIterator {
public:
    CombinationIterator(string characters, int combinationLength)
        : m_pos{0}, m_combo_len{combinationLength}, m_chars{characters} {
        string com;
        collect_combos(com, 0);
    }

    string next() {
        return m_combos[m_pos++];
    }

    bool hasNext() {
        return m_pos != m_combos.size();
    }

private:
    void collect_combos(string& com, int i)
    {
        if (com.length() == m_combo_len) {
            m_combos.push_back(com);
            return;
        }

        for (int j = i; j < m_chars.length(); ++j) {
            com.push_back(m_chars[j]);
            collect_combos(com, j + 1);
            com.pop_back();
        }
    }

private:
    int m_pos;
    int const m_combo_len;
    string m_chars;
    vector<string> m_combos;
};

/**
 * Your CombinationIterator object will be instantiated and called as such:
 * CombinationIterator* obj = new CombinationIterator(characters, combinationLength);
 * string param_1 = obj->next();
 * bool param_2 = obj->hasNext();
 */
