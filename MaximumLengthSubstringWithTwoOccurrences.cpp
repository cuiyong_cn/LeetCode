/**
 * Maximum Length Substring With Two Occurrences
 *
 * Given a string s, return the maximum length of a substring
 *  such that it contains at most two occurrences of each character.
 *
 * Example 1:
 *
 * Input: s = "bcbbbcba"
 *
 * Output: 4
 *
 * Explanation:
 * The following substring has a length of 4 and contains at most two occurrences of each character: "bcbbbcba".
 *
 * Example 2:
 *
 * Input: s = "aaaa"
 *
 * Output: 2
 *
 * Explanation:
 * The following substring has a length of 2 and contains at most two occurrences of each character: "aaaa".
 *
 * Constraints:
 *     2 <= s.length <= 100
 *     s consists only of lowercase English letters.
 */
/**
 * Optimization
 */
bool good_substr(string_view s)
{
    auto cnt = array<int, 26>{ 0, };
    for (auto c : s) {
        auto n = ++cnt[c - 'a'];
        if (n > 2) {
            return false;
        }
    }

    return true;
}

class Solution {
public:
    int maximumLengthSubstring(string s) {
        auto ans = 0;
        int const n = s.length();

        for (auto L = n; L > 2; --L) {
            for (auto [i, end] = pair(0, n - L); i <= end; ++i) {
                if (good_substr({s.data() + i, static_cast<unsigned long>(L)})) {
                    return L;
                }
            }
        }

        return 2;
    }
};

/**
 * Original solution.
 */
bool good_substr(string const& s)
{
    auto cnt = array<int, 26>{ 0, };
    for (auto c : s) {
        ++cnt[c - 'a'];
    }

    return all_of(cnt.begin(), cnt.end(), [](int n) { return n < 3; });
}

class Solution {
public:
    int maximumLengthSubstring(string s) {
        auto ans = 0;
        int const n = s.length();

        for (auto L = n; L > 2; --L) {
            for (auto [i, end] = pair(0, n - L); i <= end; ++i) {
                auto sub = s.substr(i, L);
                if (good_substr(sub)) {
                    return L;
                }
            }
        }

        return 2;
    }
};
