/**
 * Best Team With No Conflicts
 *
 * You are the manager of a basketball team. For the upcoming tournament, you want to choose the
 * team with the highest overall score. The score of the team is the sum of scores of all the
 * players in the team.
 *
 * However, the basketball team is not allowed to have conflicts. A conflict exists if a younger
 * player has a strictly higher score than an older player. A conflict does not occur between
 * players of the same age.
 *
 * Given two lists, scores and ages, where each scores[i] and ages[i] represents the score and age
 * of the ith player, respectively, return the highest overall score of all possible basketball
 * teams.
 *
 * Example 1:
 *
 * Input: scores = [1,3,5,10,15], ages = [1,2,3,4,5]
 * Output: 34
 * Explanation: You can choose all the players.
 * Example 2:
 *
 * Input: scores = [4,5,6,5], ages = [2,1,2,1]
 * Output: 16
 * Explanation: It is best to choose the last 3 players. Notice that you are allowed to choose
 * multiple people of the same age.
 * Example 3:
 *
 * Input: scores = [1,2,3,5], ages = [8,9,10,1]
 * Output: 6
 * Explanation: It is best to choose the first 3 players.
 *
 * Constraints:
 *
 * 1 <= scores.length, ages.length <= 1000
 * scores.length == ages.length
 * 1 <= scores[i] <= 106
 * 1 <= ages[i] <= 1000
 */
/**
 * Bottom up solution. But TLE.
 */
class Solution {
public:
    int bestTeamScore(vector<int>& scores, vector<int>& ages) {
        vector<pair<int, int>> players(scores.size());
        for (size_t i = 0; i < scores.size(); ++i) {
            players[i] = {ages[i], scores[i]};
        }

        map<pair<int, int>, int> max_with_initial_socre;
        sort(begin(players), end(players));

        for (int i = players.size() - 1; i >= 0; --i) {
            accumulate(players, i, players[i].second, max_with_initial_socre);
        }

        int ans = 0;
        for (auto [init, score] : max_with_initial_socre) {
            ans = max(ans, score);
        }

        return ans;
    }

private:
    int accumulate(vector<pair<int, int>> const& players, int start, int init, map<pair<int, int>, int>& max_with_initial_socre)
    {
        if (start >= players.size()) {
            return 0;
        }

        if (max_with_initial_socre.count({start, init}) > 0) {
            return max_with_initial_socre[{start, init}];
        }

        int score_take_cur = 0;
        int score_skip_cur = 0;

        if (players[start].second >= init) {
            score_take_cur = players[start].second + accumulate(players, start + 1, players[start].second, max_with_initial_socre);
            max_with_initial_socre[{start, players[start].second}] = score_take_cur;
        }

        score_skip_cur = accumulate(players, start + 1, init, max_with_initial_socre);

        auto score = max(score_take_cur, score_skip_cur);
        max_with_initial_socre[{start, init}] = score;

        return score;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int bestTeamScore(vector<int>& scores, vector<int>& ages) {
        const int n = scores.size();
        vector<int> dp(n, 0);
        vector<pair<int, int>> arr(n, {0, 0});

        for(int i = 0; i < n; ++i){
            arr[i] = { ages[i], scores[i] };
        }
        sort(begin(arr), end(arr));

        int team_score = 0;
        for (int i = 0; i < n; ++i) {
            dp[i] = arr[i].second;
            for (int j = i - 1; j >= 0; --j){
                if (arr[i].second >= arr[j].second){
                    dp[i] = max(dp[i], dp[j] + arr[i].second);
                }
            }
            team_score = max(team_score, dp[i]);
        }

        return team_score;
    }
};
