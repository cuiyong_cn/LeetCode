/**
 * Decode String
 *
 * Given an encoded string, return its decoded string.
 *
 * The encoding rule is: k[encoded_string], where the encoded_string inside the square brackets is being repeated
 * exactly k times. Note that k is guaranteed to be a positive integer.
 *
 * You may assume that the input string is always valid; No extra white spaces, square brackets are well-formed, etc.
 *
 * Furthermore, you may assume that the original data does not contain any digits and that digits are only for those
 * repeat numbers, k. For example, there won't be input like 3a or 2[4].
 *
 * Example 1:
 *
 * Input: s = "3[a]2[bc]"
 * Output: "aaabcbc"
 *
 * Example 2:
 *
 * Input: s = "3[a2[c]]"
 * Output: "accaccacc"
 *
 * Example 3:
 *
 * Input: s = "2[abc]3[cd]ef"
 * Output: "abcabccdcdcdef"
 *
 * Example 4:
 *
 * Input: s = "abc3[cd]xyz"
 * Output: "abccdcdcdxyz"
 */
/**
 * Solution from discussion. More compact.
 */
class Solution {
public:
    string decodeString(const string& s, int& i) {
        string res;
        
        while (i < s.length() && s[i] != ']') {
            if (!isdigit(s[i]))
                res += s[i++];
            else {
                int n = 0;
                while (i < s.length() && isdigit(s[i]))
                    n = n * 10 + s[i++] - '0';
                    
                i++; // '['
                string t = decodeString(s, i);
                i++; // ']'
                
                while (n-- > 0)
                    res += t;
            }
        }
        
        return res;
    }

    string decodeString(string s) {
        int i = 0;
        return decodeString(s, i);
    }
};

/**
 * Original recursive solution. using the c++17's structured binding (just for practice)
 */
class Solution {
public:
    struct PairInfo
    {
        size_t count;
        string enc_str;
        size_t next;
    };

    string decodeString(string s) {
        if (s.empty()) return "";

        if (isdigit(s[0])) {
            auto [count, enc_str, next] = get_pair(s);
            return num_dup(count, decodeString(enc_str)) + decodeString(s.substr(next));
        }

        auto [str, next] = get_str(s);
        return str + decodeString(s.substr(next));
    }

private:
    PairInfo get_pair(const string& s) {
        size_t sb_count = 0;
        auto [num_str, n1] = get_num(s); // here we can also use stoul(string, size_t*, int)
        auto [enc_str, n2] = get_enc_str(s.substr(n1));

        return {stoul(num_str), enc_str, n1 + n2 + 1};
    }

    pair<string, size_t> get_num(const string& s) {
        string num_str;
        size_t i = 0;
        for (; i < s.length(); ++i) {
            if (isdigit(s[i])) {
                num_str += s[i];
            }
            else {
                break;
            }
        }
        return {num_str, i};
    }

    pair<string, size_t> get_enc_str(const string& s) {
        string enc_str;
        size_t i = 0;
        int sb_count = 0;
        for (; i < s.length(); ++i) {
            if (isalnum(s[i])) {
                enc_str += s[i];
            }
            else if (']' == s[i]) {
                --sb_count;
                if (0 == sb_count) break;
                enc_str += s[i];
            }
            else if ('[' == s[i]) {
                ++sb_count;
                if (sb_count > 1) {
                    enc_str += s[i];
                }
            }
        }

        return {enc_str, i};
    }

    string num_dup(size_t count, const string& s) {
        string res;
        for (size_t i = 0; i < count; ++i) {
            res += s;
        }

        return res;
    }

    pair<string, size_t> get_str(const string& s) {
        string res;
        size_t i = 0;
        for (; i < s.length(); ++i) {
            if (isalpha(s[i])) {
                res += s[i];
            }
            else {
                break;
            }
        }

        return {res, i};
    }
};

