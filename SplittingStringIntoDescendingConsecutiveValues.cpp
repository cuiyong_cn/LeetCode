/**
 * Splitting a String Into Descending Consecutive Values
 *
 * You are given a string s that consists of only digits.
 *
 * Check if we can split s into two or more non-empty substrings such that the numerical values of
 * the substrings are in descending order and the difference between numerical values of every two
 * adjacent substrings is equal to 1.
 *
 * For example, the string s = "0090089" can be split into ["0090", "089"] with numerical values
 * [90,89]. The values are in descending order and adjacent values differ by 1, so this way is
 * valid.  Another example, the string s = "001" can be split into ["0", "01"], ["00", "1"], or
 * ["0", "0", "1"]. However all the ways are invalid because they have numerical values [0,1],
 * [0,1], and [0,0,1] respectively, all of which are not in descending order.  Return true if it is
 * possible to split s as described above, or false otherwise.
 *
 * A substring is a contiguous sequence of characters in a string.
 *
 * Example 1:
 *
 * Input: s = "1234"
 * Output: false
 * Explanation: There is no valid way to split s.
 * Example 2:
 *
 * Input: s = "050043"
 * Output: true
 * Explanation: s can be split into ["05", "004", "3"] with numerical values [5,4,3].
 * The values are in descending order with adjacent values differing by 1.
 * Example 3:
 *
 * Input: s = "9080701"
 * Output: false
 * Explanation: There is no valid way to split s.
 *
 * Constraints:
 *
 * 1 <= s.length <= 20
 * s only consists of digits.
 */
/**
 * Compare with other solution. My original solution stinks (contains so many edge cases)
 *
 * Way better solution. Actually MX can be limited by the s.length(). (refer my original solution)
 */
const int64_t MX = 10'000'000'000 - 1;
bool dfs(string const& s, int64_t previous, size_t idx, int part)
{
    int64_t SL = s.length();
    if (idx >= SL) {
        return part > 1;
    }

    int64_t value = 0;
    for (int i = idx; i < SL; ++i) {
        value = 10 * value + (s[i] - '0');
        if ((value > previous && previous > 0) || (value > MX)) {
            break;
        }

        if ((previous - 1) == value || -1 == previous) {
            if (dfs(s, value, i + 1, part + 1)) {
                return true;
            }
        }
    }

    return false;
}

class Solution {
public:
    bool splitString(string s) {
        return dfs(s, -1, 0, 0);
    }
};

/**
 * Original solution.
 */
bool dfs(string const& s, int64_t target, size_t idx, int part)
{
    if (target < 0) {
        return false;
    }

    int const L = s.length();
    if (idx >= L) {
        return part > 1;
    }

    auto i = s.find_first_not_of('0', idx);
    if (0 == target) {
        return string::npos == i;
    }

    if (string::npos == i) {
        return false;
    }

    auto target_str = to_string(target);
    auto send = i + target_str.length();
    if (send > L) {
        return false;
    }

    if (0 != s.compare(i, target_str.length(), target_str)) {
        return false;
    }

    return dfs(s, target - 1, send, part + 1);
}

class Solution {
public:
    bool splitString(string s) {
        auto i = s.find_first_not_of('0');
        if (string::npos == i) {
            return false;
        }

        auto const L = s.length() - i;
        auto const send = i + ((L / 2) + (L % 2));
        for (auto j = i; j < send; ++j) {
            auto target = stoll(s.substr(i, j - i + 1));
            if (dfs(s, target - 1, j + 1, 1)) {
                return true;
            }
        }

        return false;
    }
};
