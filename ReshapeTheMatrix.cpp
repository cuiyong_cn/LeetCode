/**
 * Reshape the Matrix
 *
 * In MATLAB, there is a very useful function called 'reshape', which can reshape a
 * matrix into a new one with different size but keep its original data.
 *
 * You're given a matrix represented by a two-dimensional array, and two positive
 * integers r and c representing the row number and column number of the wanted reshaped
 * matrix, respectively.
 *
 * The reshaped matrix need to be filled with all the elements of the original matrix in
 * the same row-traversing order as they were.
 *
 * If the 'reshape' operation with given parameters is possible and legal, output the new
 * reshaped matrix; Otherwise, output the original matrix.
 *
 * Example 1:
 *
 * Input:
 * nums =
 * [[1,2],
 *  [3,4]]
 * r = 1, c = 4
 * Output:
 * [[1,2,3,4]]
 * Explanation:
 * The row-traversing of nums is [1,2,3,4]. The new reshaped matrix is a 1 * 4 matrix, fill it row by row by using the previous list.
 *
 * Example 2:
 *
 * Input:
 * nums =
 * [[1,2],
 *  [3,4]]
 * r = 2, c = 4
 * Output:
 * [[1,2],
 *  [3,4]]
 * Explanation:
 * There is no way to reshape a 2 * 2 matrix to a 2 * 4 matrix. So output the original matrix.
 *
 * Note:
 *
 *     The height and width of the given matrix is in range [1, 100].
 *     The given r and c are all positive.
 */
/**
 * My original version. The idea behind this is:
 *
 * 2D array to 1D array :  cols * i + j = num
 * 1D array to 2D array :  r = num / cols, c = num % cols
 *
 * Though this method is clear and intuitive, but I prefer the simple count version,
 * cause the divsion and modulus are pretty expensive operations.
 */
class Solution {
public:
    vector<vector<int>> matrixReshape(vector<vector<int>>& nums, int r, int c) {
        int rows = nums.size();
        int cols = nums[0].size();
        int origNum = rows * cols;
        int newNum = r * c;
        if (newNum != origNum) return nums;

        vector<vector<int>> newMatrix(r, vector<int>(c, 0));
        for (int i = 0; i < r; ++i) {
            for (int j = 0; j < c; ++j) {
                int index = c * i + j;
                int ox = index / cols;
                int oy = index - ox * cols;
                newMatrix[i][j] = nums[ox][oy];
            }
        }
        return newMatrix;
    }
};

/**
 * Simple count version
 */
class Solution {
public:
    vector<vector<int>> matrixReshape(vector<vector<int>>& nums, int r, int c) {
        int rows = nums.size();
        int cols = nums[0].size();
        int origNum = rows * cols;
        int newNum = r * c;
        if (newNum != origNum) return nums;

        int ox = 0;
        int oy = 0;
        vector<vector<int>> newMatrix(r, vector<int>(c, 0));
        for (int i = 0; i < r; ++i) {
            for (int j = 0; j < c; ++j) {
                if (oy == cols) {
                    ++ox;
                    oy = 0;
                }
                newMatrix[i][j] = nums[ox][oy];
                ++oy;
            }
        }
        return newMatrix;
    }
};
