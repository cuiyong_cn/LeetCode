/**
 * Move Pieces to Obtain a String
 *
 * You are given two strings start and target, both of length n. Each string consists only of the
 * characters 'L', 'R', and '_' where:
 *
 * The characters 'L' and 'R' represent pieces, where a piece 'L' can move to the left only if there
 * is a blank space directly to its left, and a piece 'R' can move to the right only if there is a
 * blank space directly to its right.
 * The character '_' represents a blank space that can be occupied by any of the 'L' or 'R' pieces.
 * Return true if it is possible to obtain the string target by moving the pieces of the string
 * start any number of times. Otherwise, return false.
 *
 * Example 1:
 *
 * Input: start = "_L__R__R_", target = "L______RR"
 * Output: true
 * Explanation: We can obtain the string target from start by doing the following moves:
 * - Move the first piece one step to the left, start becomes equal to "L___R__R_".
 * - Move the last piece one step to the right, start becomes equal to "L___R___R".
 * - Move the second piece three steps to the right, start becomes equal to "L______RR".
 * Since it is possible to get the string target from start, we return true.
 * Example 2:
 *
 * Input: start = "R_L_", target = "__LR"
 * Output: false
 * Explanation: The 'R' piece in the string start can move one step to the right to obtain "_RL_".
 * After that, no pieces can move anymore, so it is impossible to obtain the string target from start.
 * Example 3:
 *
 * Input: start = "_R", target = "R_"
 * Output: false
 * Explanation: The piece in the string start can move only to the right, so it is impossible to obtain the string target from start.
 *
 * Constraints:
 *
 * n == start.length == target.length
 * 1 <= n <= 105
 * start and target consist of the characters 'L', 'R', and '_'.*
 */
/**
 * Did not come up a solution. So here is from the discussion.
 */
class Solution {
public:
    bool canChange(string start, string target) {
        auto const len = start.length();
        auto i = 0 * len, j = 0 * len;
        while (i <= len && j <= len) {
            i = start.find_first_not_of('_', i);
            j = target.find_first_not_of('_', j);

            if (string::npos == i || string::npos == j) {
                return string::npos == i && string::npos == j;
            }

            if (start[i] != target[j]) {
                return false;
            }

            if ('L' == target[j]) {
                if (i < j) {
                    return false;
                }
            }
            else {
                if (i > j) {
                    return false;
                }
            }

            ++i;
            ++j;
        }

        return true;
    }
};

/**
 * Original solution. Brute force, and no surprise TLE.
 */
class Solution {
public:
    bool canChange(string start, string target) {
        auto const n = static_cast<int>(start.length());
        for (auto i = 0; i < n; ++i) {
            if (start[i] == target[i]) {
                continue;
            }

            switch (target[i]) {
                case '_':
                {
                    auto space_pos = i;
                    for (; space_pos < n; ++space_pos) {
                        if ('L' == start[space_pos]) {
                            return false;
                        }
                        if ('_' == start[space_pos]) {
                            break;
                        }
                    }
                    if (n == space_pos) {
                        return false;
                    }
                    //cout << "sp: " << space_pos << "\n";
                    swap(start[i], start[space_pos]);
                }
                break;

                case 'L':
                {
                    auto L_pos = i;
                    for (; L_pos < n; ++L_pos) {
                        if ('R' == start[L_pos]) {
                            return false;
                        }
                        if ('L' == start[L_pos]) {
                            break;
                        }
                    }
                    if (n == L_pos) {
                        return false;
                    }
                    swap(start[i], start[L_pos]);
                }
                break;

                case 'R':
                default:
                {
                    return false;
                }
                break;
            }
            //cout << start << '\n';
        }
        return true;
    }
};
