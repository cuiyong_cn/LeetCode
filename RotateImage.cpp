/**
 * Rotate Image
 *
 * You are given an n x n 2D matrix representing an image.
 *
 * Rotate the image by 90 degrees (clockwise).
 *
 * Note:
 *
 * You have to rotate the image in-place, which means you have to modify the input 2D matrix directly. DO NOT allocate
 * another 2D matrix and do the rotation.
 *
 * Example 1:
 *
 * Given input matrix =
 * [
 *   [1,2,3],
 *   [4,5,6],
 *   [7,8,9]
 * ],
 *
 * rotate the input matrix in-place such that it becomes:
 * [
 *   [7,4,1],
 *   [8,5,2],
 *   [9,6,3]
 * ]
 * Example 2:
 *
 * Given input matrix =
 * [
 *   [ 5, 1, 9,11],
 *   [ 2, 4, 8,10],
 *   [13, 3, 6, 7],
 *   [15,14,12,16]
 * ],
 *
 * rotate the input matrix in-place such that it becomes:
 * [
 *   [15,13, 2, 5],
 *   [14, 3, 4, 1],
 *   [12, 6, 8, 9],
 *   [16, 7,10,11]
 * ]
 */
/**
 * Transpose and reverse solution.
 */
class Solution {
public:
    void rotate(vector<vector<int>>& m) {
        int n = m.size();

        for(int i=0; i<n; i++)
        for(int j=0; j<i; j++)
            swap(m[i][j], m[j][i]);

        for(int i=0; i<n; i++)
            reverse(m[i].begin(), m[i].end());
    }
};

/**
 * Same ring by ring idea, but more clean;
 */
class Solution {
public:
    void rotate(vector<vector<int>>& matrix) {
        int n = matrix.size();
        int a = 0;
        int b = n-1;
        while(a<b){
            for(int i=0;i<(b-a);++i){
                swap(matrix[a][a+i], matrix[a+i][b]);
                swap(matrix[a][a+i], matrix[b][b-i]);
                swap(matrix[a][a+i], matrix[b-i][a]);
            }
            ++a;
            --b;
        }
    }
};

/**
 * My original non-allocate solution. Can be visualized like this:
 *              ------
 *              |\--/|
 *              ||\/||
 *              ||/\||
 *              |/--\|
 *              ------
 *  Rotate the up triangle element ring by ring.
 */
class Solution {
public:
    void rotate(vector<vector<int>>& matrix) {
        int dim = matrix.size();
        int half = dim / 2;
        for (int i = 0; i < half; ++i) {
        for (int j = i; j < dim - i - 1; ++j) {
            int tmp = matrix[i][j];
            int nx = i, ny = j;
            for (int k = 0; k < 4; ++k) {
                int ty = dim - 1 - nx;
                nx = ny; ny = ty;
                int t = matrix[nx][ny];
                matrix[nx][ny] = tmp;
                tmp = t;
            }
        }
        }
    }
};

/**
 * Original solution. Violate the restriction. But this is intuitive and simple though consume more memory.
 */
class Solution {
public:
    void rotate(vector<vector<int>>& matrix) {
        int dim = matrix.size();
        vector<vector<int>> rotatedImg(dim, vector<int>(dim));
        for (int i = 0; i < dim; ++i) {
            auto& row = matrix[i];
            int c = dim - i - 1;
            for (int j = 0; j < dim; ++j) {
                rotatedImg[j][c] = matrix[i][j];
            }
        }
        matrix = rotatedImg;
    }
};
