/**
 * Range Sum Query 2D - Immutable
 *
 * Given a 2D matrix matrix, find the sum of the elements inside the rectangle defined by its upper
 * left corner (row1, col1) and lower right corner (row2, col2).
 *
 * Implement the NumMatrix class:
 *
 * NumMatrix(int[][] matrix) initializes the object with the integer matrix matrix.
 * int sumRegion(int row1, int col1, int row2, int col2) returns the sum of the elements of the
 * matrix array inside the rectangle defined by its upper left corner (row1, col1) and lower right
 * corner (row2, col2).
 *
 * Example 1:
 *
 * Input
 * ["NumMatrix", "sumRegion", "sumRegion", "sumRegion"]
 * [[[[3, 0, 1, 4, 2], [5, 6, 3, 2, 1], [1, 2, 0, 1, 5], [4, 1, 0, 1, 7], [1, 0, 3, 0, 5]]], [2, 1,
 * 4, 3], [1, 1, 2, 2], [1, 2, 2, 4]]
 * Output
 * [null, 8, 11, 12]
 *
 * Explanation
 * NumMatrix numMatrix = new NumMatrix([[3, 0, 1, 4, 2], [5, 6, 3, 2, 1], [1, 2, 0, 1, 5], [4, 1, 0,
 * 1, 7], [1, 0, 3, 0, 5]]);
 * numMatrix.sumRegion(2, 1, 4, 3); // return 8 (i.e sum of the red rectangele).
 * numMatrix.sumRegion(1, 1, 2, 2); // return 11 (i.e sum of the green rectangele).
 * numMatrix.sumRegion(1, 2, 2, 4); // return 12 (i.e sum of the blue rectangele).
 *
 * Constraints:
 *
 * m == matrix.length
 * n == matrix[i].length
 * 1 <= m, n <= 200
 * -105 <= matrix[i][j] <= 105
 * 0 <= row1 <= row2 < m
 * 0 <= col1 <= col2 < n
 * At most 104 calls will be made to sumRegion.
 */
/**
 * More efficient one.
 */
class NumMatrix {
public:
    using Matrix = vector<vector<int>>;
    NumMatrix(vector<vector<int>>& matrix) : m_matrix{matrix} {
        for (auto&& r : matrix) {
            partial_sum(begin(r), end(r), begin(r));
        }

        auto const M = matrix.size();
        auto const N = matrix.front().size();
        for (int r = 1; r < M; ++r) {
            for (int c = 0; c < N; ++c) {
                matrix[r][c] += matrix[r -1][c];
            }
        }
    }

    int sumRegion(int row1, int col1, int row2, int col2) {
        auto sum = m_matrix[row2][col2];
        if (row1 > 0) {
            sum -= m_matrix[row1 - 1][col2];
        }
        if (col1 > 0) {
            sum -= m_matrix[row2][col1 - 1];
        }
        if (row1 > 0 && col1 > 0) {
            sum += m_matrix[row1 - 1][col1 - 1];
        }
        return sum;
    }
private:
    Matrix& m_matrix;
};

/**
 * Your NumMatrix object will be instantiated and called as such:
 * NumMatrix* obj = new NumMatrix(matrix);
 * int param_1 = obj->sumRegion(row1,col1,row2,col2);
 */

/**
 * Original solution.
 */
class NumMatrix {
public:
    using Matrix = vector<vector<int>>;
    NumMatrix(vector<vector<int>>& matrix) : m_matrix{matrix} {
        for (auto&& r : matrix) {
            partial_sum(begin(r), end(r), begin(r));
        }
    }

    int sumRegion(int row1, int col1, int row2, int col2) {
        int sum = 0;
        for (int r = row1; r <= row2; ++r) {
            sum += m_matrix[r][col2] - (col1 > 0 ? m_matrix[r][col1 - 1] : 0);
        }
        return sum;
    }
private:
    Matrix& m_matrix;
};

/**
 * Your NumMatrix object will be instantiated and called as such:
 * NumMatrix* obj = new NumMatrix(matrix);
 * int param_1 = obj->sumRegion(row1,col1,row2,col2);
 */
