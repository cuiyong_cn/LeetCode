/**
 * Robot Bounded In Circle
 *
 * On an infinite plane, a robot initially stands at (0, 0) and faces north.  The robot can receive one of three instructions:
 *     "G": go straight 1 unit;
 *     "L": turn 90 degrees to the left;
 *     "R": turn 90 degress to the right.
 *
 * The robot performs the instructions given in order, and repeats them forever.
 *
 * Return true if and only if there exists a circle in the plane such that the robot never leaves the circle.
 *
 * Example 1:
 *
 * Input: "GGLLGG"
 * Output: true
 * Explanation:
 * The robot moves from (0,0) to (0,2), turns 180 degrees, and then returns to (0,0).
 * When repeating these instructions, the robot remains in the circle of radius 2 centered at the origin.
 *
 * Example 2:
 *
 * Input: "GG"
 * Output: false
 * Explanation:
 * The robot moves north indefinitely.
 *
 * Example 3:
 *
 * Input: "GL"
 * Output: true
 * Explanation:
 * The robot moves from (0, 0) -> (0, 1) -> (-1, 1) -> (-1, 0) -> (0, 0) -> ...
 *
 * Note:
 *     1 <= instructions.length <= 100
 *     instructions[i] is in {'G', 'L', 'R'}
 */
/**
 * Original solution.
 */
struct Point
{
    int x = 0;
    int y = 0;
    Point(int ix, int iy) : x{ix}, y{iy} {}
};

struct Accelerate
{
    int dx;
    int dy;
    Accelerate(int ix, int iy) : dx{ix}, dy{iy} {}
};

void operator+(Point& p, const Accelerate& d)
{
    p.x += d.dx;
    p.y += d.dy;
}

bool operator==(Point p1, Point p2)
{
    return p1.x == p2.x && p1.y == p2.y;
}

class Solution {
public:
    bool isRobotBounded(string instructions) {
        Point pos{0, 0};
        array<Accelerate, 4> acc{ Accelerate{0, 1}, Accelerate{1, 0}, Accelerate{0, -1}, Accelerate{-1, 0} };
        int dir = 0;

        for (auto c : instructions) {
            if (c == 'G') pos + acc[dir];
            else if (c == 'R') dir = (dir + 1) % 4;
            else if (c == 'L') dir = (dir + 3) % 4;
        }

        return pos == Point{0, 0} || dir != 0;
    }
};
