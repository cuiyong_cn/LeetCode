/**
 * Four Divisors
 *
 * Given an integer array nums, return the sum of divisors of the integers in that array that have
 * exactly four divisors.
 *
 * If there is no such integer in the array, return 0.
 *
 * Example 1:
 *
 * Input: nums = [21,4,7]
 * Output: 32
 * Explanation:
 * 21 has 4 divisors: 1, 3, 7, 21
 * 4 has 3 divisors: 1, 2, 4
 * 7 has 2 divisors: 1, 7
 * The answer is the sum of divisors of 21 only.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 10^4
 * 1 <= nums[i] <= 10^5
 */
/**
 * More efficient one.
 */
class Solution {
public:
    int sumFourDivisors(vector<int>& nums) {
        int sum = 0;
        for (auto n : nums) {
            int last_d = 0;
            for (auto d = 2; (d * d) <= n; ++d) {
                if ((n % d) == 0) {
                    if (last_d > 0) {
                        last_d = 0;
                        break;
                    }
                    last_d = d;
                }
            }

            if (last_d > 0 && last_d != (n / last_d)) {
                sum += 1 + n + last_d + n / last_d;
            }
        }

        return sum;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int sumFourDivisors(vector<int>& nums) {
        int sum = 0;
        unordered_map<int, pair<int, int>> cache;
        for (auto n : nums) {
            if (cache.count(n)) {
                auto [size, divsum] = cache[n];
                if (4 == size) {
                    sum += divsum;
                }
            }
            else {
                int size = 0;
                int divsum = 0;
                int end = sqrt(n) + 1;
                for (int i = 1; i < end; ++i) {
                    if (0 == (n % i)) {
                        auto div1 = i;
                        auto div2 = n / i;

                        if (div1 == div2) {
                            ++size;
                            divsum += div1;
                        }
                        else {
                            size += 2;
                            divsum += div1 + div2;
                        }
                    }
                }

                if (4 == size) {
                    sum += divsum;
                }
                cache[n] = {size, divsum};
            }
        }

        return sum;
    }
};
