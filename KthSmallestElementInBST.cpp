/**
 * Kth Smallest Element in a BST
 *
 * Given a binary search tree, write a function kthSmallest to find the kth smallest element in it.
 *
 * Note:
 * You may assume k is always valid, 1 ≤ k ≤ BST's total elements.
 *
 * Example 1:
 *
 * Input: root = [3,1,4,null,2], k = 1
 *    3
 *   / \
 *  1   4
 *   \
 *    2
 * Output: 1
 *
 * Example 2:
 *
 * Input: root = [5,3,6,2,4,null,null,1], k = 3
 *        5
 *       / \
 *      3   6
 *     / \
 *    2   4
 *   /
 *  1
 * Output: 3
 *
 * Follow up:
 * What if the BST is modified (insert/delete operations) often and you need to find the kth smallest frequently? How
 * would you optimize the kthSmallest routine?
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/**
 * DFS solution. Flat the binary tree using inorder traversal.
 */
class Solution {
public:
    int kthSmallest(TreeNode* root, int k) {
        inorder(root);

        return flatBST[k - 1];
    }
private:
    void inorder(TreeNode* root) {
        if (nullptr == root) return;
        inorder(root->left);
        flatBST.push_back(root->val);
        inorder(root->right);
    }
    vector<int> flatBST;
};

/**
 * More clean solution.
 */
class Solution {
public:
    int kthSmallest(TreeNode* root, int k) {
        stack<TreeNode*> stk;
        TreeNode* tn = root;
        int ans = 0;
        while (k > 0) {
            while (nullptr != tn) {
                stk.push(tn);
                tn = tn->left;
            }

            tn = stk.top();
            if (k == 1) {
                ans = tn->val;
                break;
            }
            stk.pop(); --k;
            tn = tn->right;
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int kthSmallest(TreeNode* root, int k) {
        stack<TreeNode*> stk;
        stk.push(root);
        if (nullptr != root->left) {
            TreeNode* tn = root->left;
            while (nullptr != tn) {
                stk.push(tn);
                tn = tn->left;
            }
        }

        while (k > 1) {
            TreeNode* top = stk.top();
            stk.pop();
            if (nullptr != top->right) {
                TreeNode* tn = top->right;
                while (nullptr != tn) {
                    stk.push(tn);
                    tn = tn->left;
                }
            }
            --k;
        }

        return stk.top()->val;
    }
};
