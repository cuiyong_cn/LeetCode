/**
 * Number of Ways to Split a String
 *
 * Given a binary string s, you can split s into 3 non-empty strings s1, s2, and s3 where s1 + s2 +
 * s3 = s.
 *
 * Return the number of ways s can be split such that the number of ones is the same in s1, s2, and
 * s3. Since the answer may be too large, return it modulo 109 + 7.
 *
 * Example 1:
 *
 * Input: s = "10101"
 * Output: 4
 * Explanation: There are four ways to split s in 3 parts where each part contain the same number of letters '1'.
 * "1|010|1"
 * "1|01|01"
 * "10|10|1"
 * "10|1|01"
 * Example 2:
 *
 * Input: s = "1001"
 * Output: 0
 * Example 3:
 *
 * Input: s = "0000"
 * Output: 3
 * Explanation: There are three ways to split s in 3 parts.
 * "0|0|00"
 * "0|00|0"
 * "00|0|0"
 *
 * Constraints:
 *
 * 3 <= s.length <= 105
 * s[i] is either '0' or '1'.
 */
/**
 * Original solution.
 */
class Solution {
public:
    int numWays(string s) {
        auto const one_cnt = count(s.begin(), s.end(), '1');
        if (one_cnt % 3) {
            return 0;
        }

        uint64_t const L = s.length();
        int const mod = 1'000'000'007;
        if (0 == one_cnt) {
            return ((L - 2) * (L - 1) / 2) % mod;
        }

        auto const one_third = one_cnt / 3;
        uint64_t prefix1_cnt = 0;
        uint64_t suffix3_cnt = 0;

        for (int i = 0, cnt = one_third; i < L && cnt >= 0; ++i) {
            if ('1' == s[i]) {
                --cnt;
            }
            if (0 == cnt) {
                ++prefix1_cnt;
            }
        }

        for (int i = s.length() - 1, cnt = one_third; i >= 0 && cnt >= 0; --i) {
            if ('1' == s[i]) {
                --cnt;
            }
            if (0 == cnt) {
                ++suffix3_cnt;
            }
        }

        return (prefix1_cnt * suffix3_cnt) % mod;
    }
};
