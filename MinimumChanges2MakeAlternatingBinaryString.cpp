/**
 * Minimum Changes To Make Alternating Binary String
 *
 * You are given a string s consisting only of the characters '0' and '1'. In one operation, you can
 * change any '0' to '1' or vice versa.
 *
 * The string is called alternating if no two adjacent characters are equal. For example, the string
 * "010" is alternating, while the string "0100" is not.
 *
 * Return the minimum number of operations needed to make s alternating.
 *
 * Example 1:
 *
 * Input: s = "0100"
 * Output: 1
 * Explanation: If you change the last character to '1', s will be "0101", which is alternating.
 * Example 2:
 *
 * Input: s = "10"
 * Output: 0
 * Explanation: s is already alternating.
 * Example 3:
 *
 * Input: s = "1111"
 * Output: 2
 * Explanation: You need two operations to reach "0101" or "1010".
 *
 * Constraints:
 *
 * 1 <= s.length <= 104
 * s[i] is either '0' or '1'.
 */
/**
 * Simplify.
 */
class Solution {
public:
    int minOperations(string s) {
        int f1 = 1, f0 = 0, cnt1 = 0, cnt2 = 0;

        for (auto ch : s) {
            cnt1 += ch - '0' != f1;
            cnt2 += ch - '0' != f0;
            swap(f1, f0);
        }

        return min(cnt1, cnt2);
    }
};

/**
 * Original solution
 */
class Solution {
public:
    int minOperations(string s) {
        return min(
            ops_to_make_alternating_binary(s, "01"),
            ops_to_make_alternating_binary(s, "10"));
    }

private:
    int ops_to_make_alternating_binary(string const& source, string const& pattern)
    {
        int ans = 0;
        int const N = source.length();
        for (int i = 0; i < N; i+=2) {
            if (source[i] != pattern[0]) {
                ++ans;
            }
        }

        for (int i = 1; i < N; i += 2) {
            if (source[i] != pattern[1]) {
                ++ans;
            }
        }

        return ans;
    }
};
