/**
 * String Without AAA or BBB
 *
 * Given two integers a and b, return any string s such that:
 *
 * s has length a + b and contains exactly a 'a' letters, and exactly b 'b' letters,
 * The substring 'aaa' does not occur in s, and
 * The substring 'bbb' does not occur in s.
 *
 * Example 1:
 *
 * Input: a = 1, b = 2
 * Output: "abb"
 * Explanation: "abb", "bab" and "bba" are all correct answers.
 * Example 2:
 *
 * Input: a = 4, b = 1
 * Output: "aabaa"
 *
 * Constraints:
 *
 * 0 <= a, b <= 100
 * It is guaranteed such an s exists for the given a and b.
 */
/**
 * Original solution.
 */
class Solution {
public:
    string strWithout3a3b(int a, int b) {
        string ans;
        while (a > b && b > 0) {
            ans += "aab";
            a -= 2;
            b -= 1;
        }

        while (b > a && a > 0) {
            ans += "bba";
            b -= 2;
            a -= 1;
        }

        while (a > 0 && b > 0) {
            ans += "ab";
            a-=1;
            b-=1;
        }

        if (a > 0) {
            ans += string(a, 'a');
            a = 0;
        }
        
        if (b > 0) {
            ans += string(b, 'b');
            b = 0;
        }

        return ans;
    }
};
