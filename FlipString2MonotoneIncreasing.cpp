/**
 * Flip String to Monotone Increasing
 *
 * A string of '0's and '1's is monotone increasing if it consists of some number of '0's (possibly 0), followed by some
 * number of '1's (also possibly 0.)
 *
 * We are given a string S of '0's and '1's, and we may flip any '0' to a '1' or a '1' to a '0'.
 *
 * Return the minimum number of flips to make S monotone increasing.
 *
 * Example 1:
 *
 * Input: "00110" Output: 1 Explanation: We flip the last digit to get 00111.
 *
 * Example 2:
 *
 * Input: "010110" Output: 2 Explanation: We flip to get 011111, or alternatively 000111.
 *
 * Example 3:
 *
 * Input: "00011000" Output: 2 Explanation: We flip to get 00000000.
 *
 * Note:
 *
 *     1 <= S.length <= 20000 S only consists of '0' and '1' characters.
 */
/**
 * Improved version. Eliminate the vector and reduce one round traversal.
 * Idea is:
 *
 * For string s, we use flipCount to make s monotone and origian string has ones '1'.
 * What would be the flipCount if we append a new char to s.
 *
 * If '1' is appended, no more flip needed.
 * If '0' is appended, we have 2 choices:
 *  1. flip original ones '1' to '0'
 *  2. flip the new added '0' to '1', flipCount + 1
 */
class Solution {
public:
    int minFlipsMonoIncr(string S) {
        int ones = 0;
        int flipCount = 0;

        for (int i = 0; i < S.length(); ++i) {
            if ('0' == S[i]) {
                ++flipCount;
            }
            else {
                ++ones;
            }
            flipCount = min(ones, flipCount);
        }

        return flipCount;
    }
};

/**
 * Original solution. Intuitive. O(n) complexity, O(n) space
 */
class Solution {
public:
    int minFlipsMonoIncr(string S) {
        vector<pair<int, int>> memo(S.length(), {0, 0});
        int zeroes = 0;
        int ones = 0;
        for (int i = 0; i < S.length(); ++i) {
            if ('0' == S[i]) ++zeroes;
            else ++ones;
            memo[i] = {zeroes, ones};
        }

        int totalZeroes = memo[memo.size() - 1].first;
        int totalOnes = memo[memo.size() - 1].second;
        int ans = min(totalZeroes, totalOnes);

        for (int i = 0; i < memo.size(); ++i) {
            int zeroCount = memo[i].first;
            int oneCount = memo[i].second;
            ans = min(ans, oneCount + totalZeroes - zeroCount);
        }

        return ans;
    }
};
