/**
 * Different Ways to Add Parentheses
 *
 * Given a string of numbers and operators, return all possible results from computing all the different possible ways
 * to group numbers and operators. The valid operators are +, - and *.
 *
 * Example 1:
 *
 * Input: "2-1-1"
 * Output: [0, 2]
 * Explanation:
 * ((2-1)-1) = 0
 * (2-(1-1)) = 2
 *
 * Example 2:
 *
 * Input: "2*3-4*5"
 * Output: [-34, -14, -10, -10, 10]
 * Explanation:
 * (2*(3-(4*5))) = -34
 * ((2*3)-(4*5)) = -14
 * ((2*(3-4))*5) = -10
 * (2*((3-4)*5)) = -10
 * (((2*3)-4)*5) = 10
 */
/**
 * using memo to speed it up.
 */
class Solution {
public:
    int GetResult(int n1, int n2, char op) {
        int ans = 0;
        switch (op) {
        case '+':
            ans = n1 + n2; break;
        case '-':
            ans = n1 - n2; break;
        case '*':
            ans = n1 * n2; break;
        default:
            break;
        }
        return ans;
    }

    vector<int> diffWaysToCompute(string input) {
        if (memo.count(input)) return memo[input];

        vector<int> result;
        int size = input.size();
        for (int i = 0; i < size; i++) {
            char cur = input[i];
            if (!isdigit(cur)) {
                vector<int> result1 = diffWaysToCompute(input.substr(0, i));
                vector<int> result2 = diffWaysToCompute(input.substr(i+1));
                for (auto n1 : result1)
                for (auto n2 : result2)
                    result.push_back(GetResult(n1, n2, cur));
            }
        }

        if (result.empty())
            result.push_back(stoi(input));

        memo[input] = result;

        return result;
    }
private:
    unordered_map<string, vector<int>> memo;
};


/**
 * Recursive solution.
 */
class Solution {
public:
    int GetResult(int n1, int n2, char op) {
        int ans = 0;
        switch (op) {
        case '+':
            ans = n1 + n2; break;
        case '-':
            ans = n1 - n2; break;
        case '*':
            ans = n1 * n2; break;
        default:
            break;
        }
        return ans;
    }

    vector<int> diffWaysToCompute(string input) {
        vector<int> result;
        int size = input.size();
        for (int i = 0; i < size; i++) {
            char cur = input[i];
            if (!isdigit(cur)) {
                vector<int> result1 = diffWaysToCompute(input.substr(0, i));
                vector<int> result2 = diffWaysToCompute(input.substr(i+1));
                for (auto n1 : result1)
                for (auto n2 : result2)
                    result.push_back(GetResult(n1, n2, cur));
            }
        }

        if (result.empty())
            result.push_back(stoi(input));
        return result;
    }
};
