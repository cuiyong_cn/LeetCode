/**
 * Longest Palindromic Substring
 *
 * Given a string s, return the longest palindromic substring in s.
 *
 * Example 1:
 *
 * Input: s = "babad"
 * Output: "bab"
 * Note: "aba" is also a valid answer.
 * Example 2:
 *
 * Input: s = "cbbd"
 * Output: "bb"
 * Example 3:
 *
 * Input: s = "a"
 * Output: "a"
 * Example 4:
 *
 * Input: s = "ac"
 * Output: "a"
 *
 * Constraints:
 *
 * 1 <= s.length <= 1000
 * s consist of only digits and English letters (lower-case and/or upper-case),
 */
/**
 * search for Manacher's Algorithm
 */

/**
 * Using constant space.
 */
class Solution {
public:
    string longestPalindrome(string s) {
        int const N = s.length();
        int start = 0, end = 0;
        for (int i = 0; i < N; i++) {
            int len1 = expandAroundCenter(s, i, i);
            int len2 = expandAroundCenter(s, i, i + 1);
            int len = max(len1, len2);
            if (len > end - start) {
                start = i - (len - 1) / 2;
                end = i + len / 2;
            }
        }

        return s.substr(start, end - start + 1);
    }

private:
    int expandAroundCenter(string const& s, int left, int right) {
        int L = left, R = right;
        while (L >= 0 && R < s.length() && s[L] == s[R]) {
            L--;
            R++;
        }
        return R - L - 1;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    string longestPalindrome(string s) {
        int const N = s.length();

        vector<vector<bool>> is_palindrome(N, vector<bool>(N,  false));
        for (int i = 0; i < N; ++i) {
            is_palindrome[i][i] = true;
        }

        int start = 0;
        int max_len = 1;
        for (int L = 2; L <= N; ++L) {
            for (int i = 0, end = N - L + 1; i < end; ++i) {
                int j = i + L - 1;
                if (s[i] == s[j]) {
                    if (2 == L || is_palindrome[i + 1][j - 1]) {
                        start = i;
                        max_len = L;
                        is_palindrome[i][j] = true;
                    }
                }
            }
        }

        return s.substr(start, max_len);
    }
};
