/**
 * Partition to K Equal Sum Subsets
 *Given an array of integers nums and a positive integer k, find whether it's possible to divide this array into k
 non-empty subsets whose sums are all equal.
 *
 * Example 1:
 *
 * Input: nums = [4, 3, 2, 3, 5, 2, 1], k = 4
 * Output: True
 * Explanation: It's possible to divide it into 4 subsets (5), (1, 4), (2,3), (2,3) with equal sums.
 *
 * Note:
 *
 * 1 <= k <= len(nums) <= 16.
 * 0 < nums[i] < 10000.
 */
/**
 * From the discussion. Well exlained, and I think this should be the top rated.
 *
 *The best solution for this problem is definitely bit manipulation DP with complexity O(n2^n). Whereas dfs will render
 a expotential complexity and it is hard to analyze.
 *
 * Here is my idea: We use mask to determine the state, the state will tell you "which numbers are selected already".
 * For example nums = [2,1,4,3,5,6,2], mask = "1100101" , then that means we have already chosen 2,1,5,2.
 *
 * Now, how do we consider the conversion between states? Consider the example above, it is clear that we have 4,3,6
 * unchosen so we can choose any of them. But, as explained in the leetcode official solution, we can't choose a number
 * such that it crosses our target. And dp[mask] will represent sum(chosen number) % target. Hence the state conversion
 * will become:
 *
 * dp[mask|(1<<i)] = (dp[mask]+nums[i]) % tar;
 *
 * An Example will be: nums = [4, 3, 2, 3, 5, 2, 1], k = 4, tar = 5 dp["1100101"] represents we have chosen 4,3,5,1, the
 * sum is 4+3+5+1 = 13, 13%5 = 3, hence dp["1100101"] = 3
 *
 * If dp["11111...1111"] == 0 then that means we can find the solution.
 */
class Solution {
public:
    int dp[(1<<16)+2];
    bool canPartitionKSubsets(vector<int>& nums, int k) {
        int n = nums.size(), sum = 0;
        fill(dp, dp+(1<<16)+2, -1);
        dp[0] = 0;
        for (int i = 0; i < n; i++) sum += nums[i];
        if (sum % k) return false;
        int tar = sum/k;

        for (int mask = 0; mask < (1<<n); mask++) {
            if (dp[mask] == -1) continue;  // if current state is illegal, simply ignore it
            for (int i = 0; i < n; i++) {
                if (!(mask&(1<<i)) && dp[mask]+nums[i] <= tar) {  // if nums[i] is unchosen && choose nums[i] would not cross the target
                    dp[mask|(1<<i)] = (dp[mask]+nums[i]) % tar;
                }
            }
        }
        return dp[(1<<n)-1] == 0;
    }
};
/**
 * Original solution.
 */
class Solution {
public:
    bool canPartitionKSubsets(vector<int>& nums, int k) {
        int total = accumulate(nums.begin(), nums.end(), 0);

        if (total % k) {
            return false;
        }

        int sub_sum = total / k;
        sort(nums.begin(), nums.end());
        if (nums.back() > sub_sum) {
            return false;
        }

        int idx = nums.size() - 1;
        while (idx >= 0 && nums[idx] == sub_sum) {
            --idx;
            --k;
        }

        vector<int> groups(k, 0);

        return search(groups, idx, nums, sub_sum);
    }

private:
    bool search(vector<int>& groups, int st, const vector<int>& nums, int target)
    {
        if (st < 0) return true;
        int v = nums[st--];

        for (int i = 0; i < groups.size(); ++i) {
            if ((groups[i] + v) <= target) {
                groups[i] += v;
                if (search(groups, st, nums, target)) {
                    return true;
                }
                groups[i] -= v;
            }

            if (0 == groups[i]) {
                break;
            }
        }
        return false;
    }
};
