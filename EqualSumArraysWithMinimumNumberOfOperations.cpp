/**
 * Equal Sum Arrays With Minimum Number of Operations
 *
 * You are given two arrays of integers nums1 and nums2, possibly of different lengths. The values
 * in the arrays are between 1 and 6, inclusive.
 *
 * In one operation, you can change any integer's value in any of the arrays to any value between 1
 * and 6, inclusive.
 *
 * Return the minimum number of operations required to make the sum of values in nums1 equal to the
 * sum of values in nums2. Return -1 if it is not possible to make the
 * sum of the two arrays equal.
 *
 * Example 1:
 *
 * Input: nums1 = [1,2,3,4,5,6], nums2 = [1,1,2,2,2,2]
 * Output: 3
 * Explanation: You can make the sums of nums1 and nums2 equal with 3 operations. All indices are 0-indexed.
 * - Change nums2[0] to 6. nums1 = [1,2,3,4,5,6], nums2 = [6,1,2,2,2,2].
 * - Change nums1[5] to 1. nums1 = [1,2,3,4,5,1], nums2 = [6,1,2,2,2,2].
 * - Change nums1[2] to 2. nums1 = [1,2,2,4,5,1], nums2 = [6,1,2,2,2,2].
 * Example 2:
 *
 * Input: nums1 = [1,1,1,1,1,1,1], nums2 = [6]
 * Output: -1
 * Explanation: There is no way to decrease the sum of nums1 or to increase the sum of nums2 to make them equal.
 * Example 3:
 *
 * Input: nums1 = [6,6], nums2 = [1]
 * Output: 3
 * Explanation: You can make the sums of nums1 and nums2 equal with 3 operations. All indices are 0-indexed.
 * - Change nums1[0] to 2. nums1 = [2,6], nums2 = [1].
 * - Change nums1[1] to 2. nums1 = [2,2], nums2 = [1].
 * - Change nums2[0] to 4. nums1 = [2,2], nums2 = [4].
 *
 * Constraints:
 *
 * 1 <= nums1.length, nums2.length <= 105
 * 1 <= nums1[i], nums2[i] <= 6
 */
/**
 * Same idea, but speed it up using counting sort.
 */
class Solution {
public:
     int minOperations(vector<int>& n1, vector<int>& n2) {
         if (n2.size() * 6 < n1.size() || n1.size() * 6 < n2.size()) {
             return -1;
         }

         auto sum1 = accumulate(begin(n1), end(n1), 0);
         auto sum2 = accumulate(begin(n2), end(n2), 0);
         if (sum1 > sum2) {
             swap(n1, n2);
             swap(sum1, sum2);
         }

         auto cnt[6] = {};
         auto diff = sum2 - sum1;
         auto res = 0;

         for (auto n : n1) {
             ++cnt[6 - n];
         }

         for (auto n : n2) {
             ++cnt[n - 1];
         }

         for (int i = 5; i > 0 && diff > 0; --i) {
             int take = min(cnt[i], diff / i + (diff % i != 0));
             diff -= take * i;
             res += take;
         }

         return res;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int minOperations(vector<int>& nums1, vector<int>& nums2) {
        auto sum1 = accumulate(nums1.begin(), nums1.end(), 0);
        auto sum2 = accumulate(nums2.begin(), nums2.end(), 0);
        if (sum1 > sum2) {
            swap(sum1, sum2);
            swap(nums1, nums2);
        }

        int const N1 = nums1.size();
        int const N2 = nums2.size();

        sort(nums1.begin(), nums1.end());
        sort(nums2.begin(), nums2.end());

        int i = 0, j = N2 - 1;
        int ans = 0;
        while (true) {
            auto diff = sum2 - sum1;
            if (0 == diff) {
                break;
            }

            ++ans;

            auto max_incr = i < N1 ? 6 - nums1[i] : 0;
            auto max_decr = j >= 0 ? nums2[j] - 1 : 0;
            if (0 == max_incr & max_incr == max_decr) {
                ans = -1;
                break;
            }

            if (max_incr >= max_decr) {
                sum1 += min(max_incr, diff);
                ++i;
            }
            else {
                sum2 -= min(max_decr, diff);
                --j;
            }
        }

        return ans;
    }
};
