/**
 * Remove Nth Node From End of List
 *
 * Given the head of a linked list, remove the nth node from the end of the list and return its head.
 *
 * Example 1:
 *
 * Input: head = [1,2,3,4,5], n = 2
 * Output: [1,2,3,5]
 * Example 2:
 *
 * Input: head = [1], n = 1
 * Output: []
 * Example 3:
 *
 * Input: head = [1,2], n = 1
 * Output: [1]
 *
 * Constraints:
 *
 * The number of nodes in the list is sz.
 * 1 <= sz <= 30
 * 0 <= Node.val <= 100
 * 1 <= n <= sz
 *
 * Follow up: Could you do this in one pass?
 */
/**
 * A little improvement.
 */
class Solution {
public:
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        ListNode dummy;
        dummy.next = head;
        ListNode* first = &dummy;
        ListNode* second = &dummy;
        for (int i = 0; i <= n; ++i) {
            first = first->next;
        }

        while (first) {
            first = first->next;
            second = second->next;
        }

        second->next = second->next->next;

        return dummy.next;
    }
};

/**
 * Original solution.
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        int N = length(head);

        ListNode dummy;
        ListNode* cur = &dummy;
        int nth = N - n;
        while (nth > 0) {
            cur->next = head;
            cur = head;
            head = head->next;
            --nth;
        }

        cur->next = head->next;

        return dummy.next;
    }

private:
    int length(ListNode* node)
    {
        int ans = 0;
        while (node) {
            ++ans;
            node = node->next;
        }
        return ans;
    }
};
