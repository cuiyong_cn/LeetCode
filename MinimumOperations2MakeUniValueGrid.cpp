/**
 * Minimum Operations to Make a Uni-Value Grid
 *
 * You are given a 2D integer grid of size m x n and an integer x. In one operation, you can add x
 * to or subtract x from any element in the grid.
 *
 * A uni-value grid is a grid where all the elements of it are equal.
 *
 * Return the minimum number of operations to make the grid uni-value. If it is not possible, return
 * -1.
 *
 * Example 1:
 *
 * Input: grid = [[2,4],[6,8]], x = 2
 * Output: 4
 * Explanation: We can make every element equal to 4 by doing the following:
 * - Add x to 2 once.
 * - Subtract x from 6 once.
 * - Subtract x from 8 twice.
 * A total of 4 operations were used.
 * Example 2:
 *
 * Input: grid = [[1,5],[2,3]], x = 1
 * Output: 5
 * Explanation: We can make every element equal to 3.
 * Example 3:
 *
 *
 * Input: grid = [[1,2],[3,4]], x = 2
 * Output: -1
 * Explanation: It is impossible to make every element equal.
 *
 * Constraints:
 *
 * m == grid.length
 * n == grid[i].length
 * 1 <= m, n <= 105
 * 1 <= m * n <= 105
 * 1 <= x, grid[i][j] <= 104
 */
/**
 * Original solution. Non mathematical way.
 *
 * But we can do the mathematical way using the middle value to converge.
 */
class Solution {
public:
    int minOperations(vector<vector<int>>& grid, int x) {
        int const total_cnt = grid.size() * grid[0].size();

        vector<int> num_cnt(10001, 0);
        int min_num = 10001;
        int sum = 0;
        for (auto const& row : grid) {
            for (auto const& n : row) {
                min_num = min(min_num, n);
                sum += n;
                ++num_cnt[n];
            }
        }

        int ans = numeric_limits<int>::max();
        int cnt = 0;
        int current_sum = 0;
        for (int i = min_num; i < num_cnt.size(); i += x) {
            for (int k = 1; k < x; ++k) {
                if ((i + k) < num_cnt.size() && num_cnt[i + k] > 0) {
                    return -1;
                }
            }

            cnt += num_cnt[i];
            current_sum += i * num_cnt[i];

            ans = min(ans, (cnt * i - current_sum) / x + (sum - current_sum - (total_cnt - cnt) * i) / x);
        }

        return ans;
    }
};
