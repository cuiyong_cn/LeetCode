/**
 * Factorial Trailing Zeroes
 *
 * Given an integer n, return the number of trailing zeroes in n!.
 *
 * Follow up: Could you write a solution that works in logarithmic time complexity?
 *
 * Example 1:
 *
 * Input: n = 3
 * Output: 0
 * Explanation: 3! = 6, no trailing zero.
 * Example 2:
 *
 * Input: n = 5
 * Output: 1
 * Explanation: 5! = 120, one trailing zero.
 * Example 3:
 *
 * Input: n = 0
 * Output: 0
 *
 * Constraints:
 *
 * 0 <= n <= 104
 */
/**
 * Faster one. Idea is. 10 = 2 * 5.
 * In n!, we need to know how many 2 and 5, and the number of zeros is the minimum of the number of
 * 2 and the number of 5.
 * Since multiple of 2 is more than multiple of 5, the number of zeros is dominant by the number of
 * 5.
 * E.g  2147483647!
 * =2 * 3 * ...* 5 ... *10 ... 15* ... * 25 ... * 50 ... * 125 ... * 250...
 * =2 * 3 * ...* 5 ... * (5^1*2)...(5^1*3)...*(5^2*1)...*(5^2*2)...*(5^3*1)...*(5^3*2)... (Equation 1)
 *
 * Problem become to find the count of number 5.
 *
 * And the result is: n / 5 + n / 25 + n / 125 + ... n / 5^i
 */
class Solution {
public:
    int trailingZeroes(int n) { 
        int count = 0;

        for (long long i = 5; n / i; i *= 5) {
            count += n / i;
        }

        return count;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int trailingZeroes(int n) {
        int ans = 0;
        unsigned long factorial = 1;
        for (int i = 2; i <= n; ++i) {
            factorial *= i;
            while (0 == (factorial % 10)) {
                ++ans;
                factorial /= 10;
            }
            factorial %= 1000000UL;
        }

        return ans;
    }
};
