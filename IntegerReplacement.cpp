/**
 * Integer Replacement
 *
 * Given a positive integer n, you can apply one of the following operations:
 *
 * If n is even, replace n with n / 2.
 * If n is odd, replace n with either n + 1 or n - 1.
 * Return the minimum number of operations needed for n to become 1.
 *
 * Example 1:
 *
 * Input: n = 8
 * Output: 3
 * Explanation: 8 -> 4 -> 2 -> 1
 * Example 2:
 *
 * Input: n = 7
 * Output: 4
 * Explanation: 7 -> 8 -> 4 -> 2 -> 1
 * or 7 -> 6 -> 3 -> 2 -> 1
 * Example 3:
 *
 * Input: n = 4
 * Output: 2
 *
 * Constraints:
 *
 * 1 <= n <= 231 - 1
 */
/**
 * Convert this to recursive one.
 */
class Solution {
private:
    unordered_map<int, int> visited;

public:
    int integerReplacement(int n) {
        if (n == 1) { return 0; }
        if (visited.count(n) == 0) {
            if (n & 1 == 1) {
                visited[n] = 2 + min(integerReplacement(n / 2), integerReplacement(n / 2 + 1));
            } else {
                visited[n] = 1 + integerReplacement(n / 2);
            }
        }
        return visited[n];
    }
};

/**
 * DP solution. But this one trigger TLE because every time you need to calculate the repeated one,
 * or unneeded ones.
 */
class Solution {
public:
    int integerReplacement(int n) {
        vector<int> dp(n + 1, 0);
        for (int i = 2; i <= n; i++) {
            dp[i] += 1;
            if (i & 1) {
                dp[i] += min(dp[i - 1], 1 + dp[i / 2 + 1]);
            }
            else {
                dp[i] += dp[i / 2];
            }
        }

        return dp[n];
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int integerReplacement(int n) {
        vector<int> anses;
        deque<pair<uint32_t, int>> q = {{n, 0}};
        while (!q.empty()) {
            auto [num, steps] = q.front(); q.pop_front();
            if (1 == num) {
                anses.push_back(steps);
            }
            else if (num & 1) {
                // odd number
                q.push_back({num + 1, steps + 1});
                q.push_back({num - 1, steps + 1});
            }
            else {
                q.push_back({num / 2, steps + 1});
            }
        }

        return *min_element(begin(anses), end(anses));
    }
};
