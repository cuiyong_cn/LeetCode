/**
 * Find All Numbers Disappeared in an Array
 *
 * Given an array of integers where 1 ≤ a[i] ≤ n (n = size of array), some elements appear twice and others appear once.
 *
 * Find all the elements of [1, n] inclusive that do not appear in this array.
 *
 * Could you do it without extra space and in O(n) runtime? You may assume the returned list does not count as extra space.
 *
 * Example:
 *
 * Input:
 * [4,3,2,7,8,2,3,1]
 *
 * Output:
 * [5,6]
 */
/**
 * Remove the extra space.
 */
class Solution {
public:
    vector<int> findDisappearedNumbers(vector<int>& nums) {
        vector<int> ans;
        for (int i = 0; i < nums.size(); ++i) {
            int n = abs(nums[i]) - 1;
            if (nums[n] > 0) {
                nums[n] = -nums[n];
            }
        }
        for (int i = 0; i < nums.size(); ++i) {
            if (nums[i] > 0) {
                ans.push_back(i + 1);
            }
        }

        return ans;
    }
};

/**
 * Using extra space to make it more efficient.
 */
class Solution {
public:
    vector<int> findDisappearedNumbers(vector<int>& nums) {
        vector<int> counter(nums.size() + 1);
        vector<int> ans;
        for (auto& n : nums) {
            ++counter[n];
        }
        for (int i = 1; i < counter.size(); ++i) {
            if (0 == counter[i]) {
                ans.push_back(i);
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<int> findDisappearedNumbers(vector<int>& nums) {
        vector<int> ans;
        sort(nums.begin(), nums.end());
        int i = 1;
        for (auto& n : nums) {
            if (n >= i) {
                for (int j = i; j < n; ++j) {
                    ans.push_back(j);
                }
                i = n + 1;
            }
        }

        for (int n = i; n <= nums.size(); ++n) {
            ans.push_back(n);
        }
        return ans;
    }
};
