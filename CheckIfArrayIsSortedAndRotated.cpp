/**
 * Check if Array Is Sorted and Rotated
 *
 * Given an array nums, return true if the array was originally sorted in non-decreasing order, then
 * rotated some number of positions (including zero). Otherwise, return false.
 *
 * There may be duplicates in the original array.
 *
 * Note: An array A rotated by x positions results in an array B of the same length such that A[i]
 * == B[(i+x) % A.length], where % is the modulo operation.
 *
 * Example 1:
 *
 * Input: nums = [3,4,5,1,2]
 * Output: true
 * Explanation: [1,2,3,4,5] is the original sorted array.
 * You can rotate the array by x = 3 positions to begin on the the element of value 3: [3,4,5,1,2].
 * Example 2:
 *
 * Input: nums = [2,1,3,4]
 * Output: false
 * Explanation: There is no sorted array once rotated that can make nums.
 * Example 3:
 *
 * Input: nums = [1,2,3]
 * Output: true
 * Explanation: [1,2,3] is the original sorted array.
 * You can rotate the array by x = 0 positions (i.e. no rotation) to make nums.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 100
 * 1 <= nums[i] <= 100
 */
/**
 * Another one solution. Idea is: all adjacent elements, the relation a > b can happen at most once.
 */
class Solution {
public:
    bool check(vector<int>& nums) {
        int const N = nums.size();
        int gt_cnt = 0;
        for (int i = 0; i < N; ++i) {
            int next = (i + 1) % N;
            if (nums[i] > nums[next]) {
                ++gt_cnt;
            }
        }

        return gt_cnt < 2;
    }
};

/**
 * Simplify.
 */
class Solution {
public:
    bool check(vector<int>& nums) {
        auto it = is_sorted_until(nums.begin(), nums.end());
        if (it == nums.end()) {
            return true;
        }

        return nums.back() <= nums.front() && is_sorted(it, nums.end());
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool check(vector<int>& nums) {
        int const N = nums.size();
        int i = 1;
        for (; i < N; ++i) {
            if (nums[i] < nums[i - 1]) {
                break;
            }
        }

        if (N == i) {
            return true;
        }

        for (int j = 1; j < N; ++j) {
            auto const next = (i + 1) % N;
            if (nums[i] > nums[next]) {
                return false;
            }
            i = next;
        }

        return true;
    }
};
