/**
 * Vowel Spellchecker
 *
 * Given a wordlist, we want to implement a spellchecker that converts a query word into a correct word.
 *
 * For a given query word, the spell checker handles two categories of spelling mistakes:
 *
 * Capitalization: If the query matches a word in the wordlist (case-insensitive), then the query word is returned with
 * the same case as the case in the wordlist.
 * Example: wordlist = ["yellow"], query = "YellOw": correct = "yellow"
 * Example: wordlist = ["Yellow"], query = "yellow": correct = "Yellow"
 * Example: wordlist = ["yellow"], query = "yellow": correct = "yellow"
 * Vowel Errors: If after replacing the vowels ('a', 'e', 'i', 'o', 'u') of the query word with any vowel individually,
 * it matches a word in the wordlist (case-insensitive), then the query word is returned with the same case as the match
 * in the wordlist.
 *
 * Example: wordlist = ["YellOw"], query = "yollow": correct = "YellOw"
 * Example: wordlist = ["YellOw"], query = "yeellow": correct = "" (no match)
 * Example: wordlist = ["YellOw"], query = "yllw": correct = "" (no match)
 * In addition, the spell checker operates under the following precedence rules:
 *
 * When the query exactly matches a word in the wordlist (case-sensitive), you should return the same word back.
 * When the query matches a word up to capitlization, you should return the first such match in the wordlist.
 * When the query matches a word up to vowel errors, you should return the first such match in the wordlist.
 * If the query has no matches in the wordlist, you should return the empty string.
 * Given some queries, return a list of words answer, where answer[i] is the correct word for query = queries[i].
 *
 * Example 1:
 *
 * Input: wordlist = ["KiTe","kite","hare","Hare"], queries = ["kite","Kite","KiTe","Hare","HARE","Hear","hear","keti","keet","keto"]
 * Output: ["kite","KiTe","KiTe","Hare","hare","","","KiTe","","KiTe"]
 *
 * Note:
 * 1 <= wordlist.length <= 5000
 * 1 <= queries.length <= 5000
 * 1 <= wordlist[i].length <= 7
 * 1 <= queries[i].length <= 7
 * All strings in wordlist and queries consist only of english letters.
 */
/**
 * Original solution. Need optimize.
 * From discussions. They all use three map to solve this one.
 * org_map, lower_map, match_map
 * and for each query word, match against the tree map in defined order.
 */
class Solution {
public:
    struct TriStr
    {
        int seq_no;
        string org;
        string lower;
        string pattern;
    };

    vector<string> spellchecker(vector<string>& wordlist, vector<string>& queries) {
        vector<vector<TriStr>> words_start_with(128);

        for (int i = 0; i < wordlist.size(); ++i) {
            const auto& w = wordlist[i];
            TriStr pack = {i, w, to_lower(w), pattern(w)};

            words_start_with[tolower(w.front())].push_back(pack);
            if (is_vowel(w.front())) {
                words_start_with['*'].push_back(pack);
            }
        }

        vector<string> ans;

        for (const auto& q : queries) {
            string match = pattern(q);
            string lowq = to_lower(q);
            const string* org = nullptr;
            const string* lower = nullptr;
            const string* pat = nullptr;
            int pat_seq_no = -1;

            if (is_vowel(q.front())) {
                for (const auto& w : words_start_with['*']) {
                    if (w.pattern == match) {
                        pat = &w.org;
                        pat_seq_no = w.seq_no;
                        break;
                    }
                }
            }

            for (const auto& w : words_start_with[tolower(q.front())]) {
                if (w.org == q) {
                    org = &w.org;
                    break;
                }
                else if (!lower && w.lower == lowq) {
                    lower = &w.org;
                }
                else if (!lower) {
                    if (!pat || pat_seq_no > w.seq_no) {
                        if (w.pattern == match) {
                            pat = &w.org;
                        }
                    }
                }
            }

            if (org) {
                ans.push_back(*org);
            }
            else if (lower) {
                ans.push_back(*lower);
            }
            else if (pat) {
                ans.push_back(*pat);
            }
            else {
                ans.push_back("");
            }
        }

        return ans;
    }

private:
    string pattern(const string& w)
    {
        string clone = w;
        for (auto& c : clone) {
            if (is_vowel(c)) {
                c = '*';
            }
            else {
                c = tolower(c);
            }
        }

        return clone;
    }

    bool is_vowel(char c)
    {
        switch (tolower(c)) {
        case 'a':
        case 'e':
        case 'i':
        case 'o':
        case 'u':
            return true;
        default:
            break;
        }

        return false;
    }

    string to_lower(const string& w)
    {
        string clone = w;
        for (auto& c : clone) {
            c = tolower(c);
        }

        return clone;
    }
};
