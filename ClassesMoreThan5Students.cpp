/**
 * Classes More Than 5 Students
 *
 * There is a table courses with columns: student and class
 *
 * Please list out all classes which have more than or equal to 5 students.
 *
 * For example, the table:
 *
 * +---------+------------+
 * | student | class      |
 * +---------+------------+
 * | A       | Math       |
 * | B       | English    |
 * | C       | Math       |
 * | D       | Biology    |
 * | E       | Math       |
 * | F       | Computer   |
 * | G       | Math       |
 * | H       | Math       |
 * | I       | Math       |
 * +---------+------------+
 * Should output:
 *
 * +---------+
 * | class   |
 * +---------+
 * | Math    |
 * +---------+
 *
 * Note:
 * The students should not be counted duplicate in each course.
 */
/**
 * Group by then Having, but now efficient as subquery one.
 */
SELECT
    class
FROM
    courses
GROUP BY class
HAVING COUNT(DISTINCT student) >= 5
;

/**
 * Original solution.
 */
# Write your MySQL query statement below
select
  b.class
from
  (
    select
      a.class,
      count(distinct a.student) as students
    from
      courses a
    group by
      a.class
  ) b
where
  b.students > 4;
