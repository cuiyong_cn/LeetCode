/**
 * Add Strings
 *
 * Given two non-negative integers num1 and num2 represented as string, return the sum of num1 and num2.
 *
 * Note:
 *
 * The length of both num1 and num2 is < 5100.
 * Both num1 and num2 contains only digits 0-9.
 * Both num1 and num2 does not contain any leading zero.
 * You must not use any built-in BigInteger library or convert the inputs to integer directly.
 */
/**
 * Improve the following one. But I still prefer the original one.
 */
class Solution {
public:
    string addStrings(string num1, string num2) {
        int carry = 0;
        string ans;

        for (int i = num1.length() - 1, j = num2.length() - 1; i >= 0 || j >= 0; --i, --j) {
            char add = 0;
            if (i >= 0) {
                add += num1[i] - '0';
            }
            if (j >= 0) {
                add += num2[j] - '0';
            }

            add += carry;

            if (add > 9) {
                add -= 10;
                carry = 1;
            }
            else {
                carry = 0;
            }

            ans += '0' + add;
        }

        if (carry) {
            ans += '1';
        }

        reverse(ans.begin(), ans.end());

        return ans;
    }
};

/**
 * without reverse. But really slow because the it needs allocation every time.
 */
class Solution {
public:
    string addStrings(string num1, string num2) {
        int carry = 0;
        string ans;
        for (int i = num1.length() - 1, j = num2.length() - 1; i >= 0 || j >= 0; --i, --j) {
            char add = 0;
            if (i >= 0) {
                add += num1[i] - '0';
            }
            if (j >= 0) {
                add += num2[j] - '0';
            }

            add += carry;

            if (add > 9) {
                add -= 10;
                carry = 1;
            }
            else {
                carry = 0;
            }

            ans = string(1, '0' + add) + ans;
        }

        if (carry) {
            ans = "1" + ans;
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    string addStrings(string num1, string num2) {
        reverse(num1.begin(), num1.end());
        reverse(num2.begin(), num2.end());

        int carry = 0;
        string ans;
        size_t i = 0;
        while (i < num1.length() || i < num2.length()) {
            int add = 0;
            if (i < num1.length()) {
                add += num1[i] - '0';
            }
            if (i < num2.length()) {
                add += num2[i] - '0';
            }

            add += carry;

            if (add > 9) {
                add -= 10;
                carry = 1;
            }
            else {
                carry = 0;
            }

            ans += '0' + add;
            ++i;
        }

        if (carry) {
            ans += '1';
        }

        reverse(ans.begin(), ans.end());
        return ans;
    }
};
