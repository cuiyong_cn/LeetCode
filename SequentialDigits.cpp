/**
 * Sequential Digits
 *
 * An integer has sequential digits if and only if each digit in the number is one more than the previous digit.
 *
 * Return a sorted list of all the integers in the range [low, high] inclusive that have sequential digits.
 *
 * Example 1:
 *
 * Input: low = 100, high = 300 Output: [123,234]
 *
 * Example 2:
 *
 * Input: low = 1000, high = 13000 Output: [1234,2345,3456,4567,5678,6789,12345]
 *
 * Constraints:
 *     10 <= low <= high <= 10^9
 */
/**
 * Original solution.
 */
class Solution
{
public:
    int length(int num)
    {
        int count = 0;

        while (num > 0) {
            ++count;
            num = num / 10;
        }

        return count;
    }

    int getSeqNum(int d, int len)
    {
        if (len > (9 - d + 1)) return -1;
        stringstream ss;

        for (; len > 0; --len) {
            ss << d;
            ++d;
        }

        return stoi(ss.str());
    }

    vector<int> sequentialDigits(int low, int high)
    {
        int minLen = length(low);
        int maxLen = length(high);
        vector<int> ans;
        int digit = low / (pow(10, minLen));
        for (int L = minLen; L <= maxLen; ++L) {
            for (int d = digit; d < 9; ++d) {
                int num = getSeqNum(d, L);
                if (low <= num && num <= high) ans.push_back(num);
            }
            digit = 1;
        }

        return ans;
    }
};

/**
 * Iterate through all the possibles ones.
 */
class Solution
{
public:
    vector<int> sequentialDigits(int low, int high)
    {
        vector<int> allNums = {12, 23, 34, 45, 56, 67, 78, 89,
                               123, 234, 345, 456, 567, 678, 789,
                               1234, 2345, 3456, 4567, 5678, 6789,
                               12345, 23456, 34567, 45678, 56789,
                               123456, 234567, 345678, 456789,
                               1234567, 2345678, 3456789,
                               12345678, 23456789,
                               123456789
                              };
        vector<int> ans;
        for (auto n : allNums) {
            if (low <= n && n <= high) {
                ans.push_back(n);
            }
        }
        return ans;
    }
};

