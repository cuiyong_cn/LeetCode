/**
 * Implement Trie (Prefix Tree)
 *
 * Implement a trie with insert, search, and startsWith methods.
 *
 * Example:
 *
 * Trie trie = new Trie();
 *
 * trie.insert("apple");
 * trie.search("apple");   // returns true
 * trie.search("app");     // returns false
 * trie.startsWith("app"); // returns true
 * trie.insert("app");
 * trie.search("app");     // returns true
 *
 * Note:
 *     You may assume that all inputs are consist of lowercase letters a-z.
 *     All inputs are guaranteed to be non-empty strings.
 */
/**
 * Original solution. Pretty basic.
 */
class Trie {
public:
    /** Initialize your data structure here. */
    Trie() : children(26, nullptr), leaf{false} {

    }

    /** Inserts a word into the trie. */
    void insert(string word) {
        Trie* cur = this;
        for (auto c : word) {
            if (!cur->children[c - 'a']) {
                cur->children[c - 'a'] = new Trie{};
            }
            cur = cur->children[c - 'a'];
        }
        cur->leaf = true;
    }

    /** Returns if the word is in the trie. */
    bool search(string word) {
        Trie* cur = this;
        for (auto c : word) {
            if (!cur->children[c - 'a']) {
                return false;
            }
            cur = cur->children[c - 'a'];
        }

        return cur->leaf;
    }

    /** Returns if there is any word in the trie that starts with the given prefix. */
    bool startsWith(string prefix) {
        Trie* cur = this;
        for (auto c : prefix) {
            if (!cur->children[c - 'a']) {
                return false;
            }
            cur = cur->children[c - 'a'];
        }

        return true;
    }

private:
    vector<Trie*> children;
    bool leaf;
};

/**
 * Your Trie object will be instantiated and called as such:
 * Trie* obj = new Trie();
 * obj->insert(word);
 * bool param_2 = obj->search(word);
 * bool param_3 = obj->startsWith(prefix);
 */
