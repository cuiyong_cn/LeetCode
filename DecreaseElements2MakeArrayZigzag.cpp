/**
 * Decrease Elements To Make Array Zigzag
 *
 * Given an array nums of integers, a move consists of choosing any element and decreasing it by 1.
 *
 * An array A is a zigzag array if either:
 *
 * Every even-indexed element is greater than adjacent elements, ie. A[0] > A[1] < A[2] > A[3] < A[4] > ...
 * OR, every odd-indexed element is greater than adjacent elements, ie. A[0] < A[1] > A[2] < A[3] > A[4] < ...
 * Return the minimum number of moves to transform the given array nums into a zigzag array.
 *
 * Example 1:
 *
 * Input: nums = [1,2,3]
 * Output: 2
 * Explanation: We can decrease 2 to 0 or 3 to 1.
 * Example 2:
 *
 * Input: nums = [9,6,1,6,2]
 * Output: 4
 *
 * Constraints:
 *
 * 1 <= nums.length <= 1000
 * 1 <= nums[i] <= 1000
 */
/**
 * Improve the code.
 */
class Solution {
public:
    int movesToMakeZigzag(vector<int>& A) {
        int res[2] = {0, 0};
        int  n = A.size(), left = 0, right = 0;
        for (int i = 0; i < n; ++i) {
            left = i > 0 ? A[i - 1] : 1001;
            right = i + 1 < n ? A[i + 1] : 1001;
            res[i & 1] += max(0, A[i] - min(left, right) + 1);
        }
        return min(res[0], res[1]);
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int movesToMakeZigzag(vector<int>& nums) {
        auto even_version = nums;
        int even_move = 0;
        for (int i = 1; i < even_version.size(); i += 2) {
            int min_num = nums[i - 1];
            if ((i + 1) < nums.size()) {
                min_num = std::min(min_num, nums[i + 1]);
            }

            if (nums[i] >= min_num) {
                even_move += nums[i] - min_num + 1;
            }
        }

        auto odd_version = nums;
        int odd_move = 0;
        for (int i = 0; i < even_version.size(); i += 2) {
            int min_num = i > 0 ? nums[i - 1] : numeric_limits<int>::max();
            if ((i + 1) < nums.size()) {
                min_num = std::min(min_num, nums[i + 1]);
            }
            if (nums[i] >= min_num) {
                odd_move += nums[i] - min_num + 1;
            }
        }

        return std::min(even_move, odd_move);
    }
};
