/**
 * Bitwise ORs of Subarrays
 *
 * We have an array arr of non-negative integers.
 *
 * For every (contiguous) subarray sub = [arr[i], arr[i + 1], ..., arr[j]] (with i <= j), we take
 * the bitwise OR of all the elements in sub, obtaining a result arr[i] | arr[i + 1] | ... | arr[j].
 *
 * Return the number of possible results. Results that occur more than once are only counted once in
 * the final answer
 *
 * Example 1:
 *
 * Input: arr = [0]
 * Output: 1
 * Explanation: There is only one possible result: 0.
 * Example 2:
 *
 * Input: arr = [1,1,2]
 * Output: 3
 * Explanation: The possible subarrays are [1], [1], [2], [1, 1], [1, 2], [1, 1, 2].
 * These yield the results 1, 1, 2, 1, 3, 3.
 * There are 3 unique values, so the answer is 3.
 * Example 3:
 *
 * Input: arr = [1,2,4]
 * Output: 6
 * Explanation: The possible results are 1, 2, 3, 4, 6, and 7.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 5 * 104
 * 0 <= nums[i] <= 109
 */
/**
 * Optimize.
 */
class Solution {
public:
    int subarrayBitwiseORs(vector<int> A) {
        vector<int> res;
        int left = 0, right;
        for (int a: A) {
            right = res.size();
            res.push_back(a);
            for (int i = left; i < right; ++i) {
                if (res.back() != (res[i] | a)) {
                    res.push_back(res[i] | a);
                }
            }
            left = right;
        }
        return unordered_set(res.begin(), res.end()).size();
    }
};

/**
 * Optimize.
 */
class Solution {
public:
    int subarrayBitwiseORs(vector<int>& A) {
        unordered_set<int> s;
        set<int> t;

        for (auto i : A) {
            set<int> r = {i};

            for (auto j : t) {
                r.insert(i | j);
            }

            swap(t, r);
            s.insert(t.begin(), t.end());
        }

        return s.size();
    }
};

/**
 * Original solution. Idea is
 * For example, an array is [001, 011, 100, 110, 101] (in binary).
 *
 * All the subarrays are:
 *
 * [001]
 * [001 011] [011]
 * [001 011 100] [011 100] [100]
 * [001 011 100 110] [011 100 110] [100 110] [110]
 * [001 011 100 110 101] [011 100 110 101] [100 110 101] [110 101] [101]
 *
 */
class Solution {
public:
    int subarrayBitwiseORs(vector<int>& A) {
        unordered_set<int> s;
        unordered_set<int> t;

        for (auto i : A) {
            unordered_set<int> r = {i};

            for (auto j : t) {
                r.insert(i | j);
            }

            swap(t, r);
            s.insert(t.begin(), t.end());
        }

        return s.size();
    }
};
