##
 # Transpose File
 #
 # Given a text file file.txt, transpose its content.
 #
 # You may assume that each row has the same number of columns, and each field is separated by the
 # ' ' character.
 #
 # Example:
 #
 # If file.txt has the following content:
 #
 # name age
 # alice 21
 # ryan 30
 # Output the following:
 #
 # name alice ryan
 # age 21 30
 #
##
 # pure awk script
 #
awk '
{
    for (i = 1; i <= NF; i++) {
        if(NR == 1) {
            s[i] = $i;
        } else {
            s[i] = s[i] " " $i;
        }
    }
}
END {
    for (i = 1; s[i] != ""; i++) {
        print s[i];
    }
}' file.txt

##
 # one line solutoin
 #
head -1 file.txt | wc -w | xargs seq 1 | xargs -I{} -n 1 sh -c "cut -d ' ' -f{} file.txt | paste -sd ' ' -"

##
 # Original solution.
 #
# Read from the file file.txt and print its transposed content to stdout.
nr=$(head -1 file.txt | tr ' ' '\n' | wc -l)

i=1
while [ $i -le ${nr} ]
do
    awk '{ if (FNR == 1) printf "%s", $'"$i;"' else printf " %s", $'"$i}" file.txt
    printf "\n"
    i=$((i + 1))
done
