/**
 * Can Place Flowers
 *
 * You have a long flowerbed in which some of the plots are planted, and some are not. However,
 * flowers cannot be planted in adjacent plots.
 *
 * Given an integer array flowerbed containing 0's and 1's, where 0 means empty and 1 means not
 * empty, and an integer n, return if n new flowers can be planted in the flowerbed without
 * violating the no-adjacent-flowers rule.
 *
 * Example 1:
 *
 * Input: flowerbed = [1,0,0,0,1], n = 1
 * Output: true
 * Example 2:
 *
 * Input: flowerbed = [1,0,0,0,1], n = 2
 * Output: false
 *
 * Constraints:
 *
 * 1 <= flowerbed.length <= 2 * 104
 * flowerbed[i] is 0 or 1.
 * There are no two adjacent flowers in flowerbed.
 * 0 <= n <= flowerbed.length
 */
/**
 * Original solution.
 * First we try to plant the flower at the back and front which is cheap.(since we only need to
 * check one position instead of two)
 * Then we start from 1 to N - 2  to plant the rest.
 *
 * The only thing left is some edge cases, like when there is only one element or we don't need to
 * plant any new flower.
 */
class Solution {
public:
    bool canPlaceFlowers(vector<int>& flowerbed, int n) {
        if (0 == n) {
            return true;
        }

        int const N = flowerbed.size();
        if (1 == N) {
            return 0 == flowerbed[0] && n < 2;
        }

        if (0 == flowerbed[0] && N > 1 && 0 == flowerbed[1]) {
            flowerbed[0] = 1;
            --n;
        }

        if (0 == flowerbed.back() && N > 1 && 0 == flowerbed[N - 2]) {
            flowerbed.back() = 1;
            --n;
        }

        int const last = N - 1;
        int i = 1;
        while (i < last) {
            if (0 == flowerbed[i] && 0 == flowerbed[i - 1] && 0 == flowerbed[i + 1]) {
                flowerbed[i] = 1;
                --n;
                ++i;
            }
            ++i;
        }

        return n <= 0;
    }
};
