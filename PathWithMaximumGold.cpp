/**
 * Path with Maximum Gold
 *
 * In a gold mine grid of size m * n, each cell in this mine has an integer representing
 * the amount of gold in that cell, 0 if it is empty.
 *
 * Return the maximum amount of gold you can collect under the conditions:
 *
 *     Every time you are located in a cell you will collect all the gold in that cell.
 *     From your position you can walk one step to the left, right, up or down.
 *     You can't visit the same cell more than once.
 *     Never visit a cell with 0 gold.
 *     You can start and stop collecting gold from any position in the grid that has some gold.
 *
 * Example 1:
 *
 * Input: grid = [[0,6,0],[5,8,7],[0,9,0]]
 * Output: 24
 * Explanation:
 * [[0,6,0],
 *  [5,8,7],
 *  [0,9,0]]
 * Path to get the maximum gold, 9 -> 8 -> 7.
 *
 * Example 2:
 *
 * Input: grid = [[1,0,7],[2,0,6],[3,4,5],[0,3,0],[9,0,20]]
 * Output: 28
 * Explanation:
 * [[1,0,7],
 *  [2,0,6],
 *  [3,4,5],
 *  [0,3,0],
 *  [9,0,20]]
 * Path to get the maximum gold, 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7.
 *
 * Constraints:
 *
 *     1 <= grid.length, grid[i].length <= 15
 *     0 <= grid[i][j] <= 100
 *     There are at most 25 cells containing gold.
 */
/**
 * Original version. If the grid is really large, we should use some cache mechanism to
 * memorize every location's max gold amount for four direcitons.
 */
class Solution {
private:
    vector<pair<int, int>> m_dirs = { {-1, 0}, {0, 1}, {1, 0}, {0, -1} };
    int m_rows;
    int m_cols;

    int walkAround(vector<vector<int>>& grid, int x, int y) {
        if (x < 0 || x >= m_rows || y < 0 || y >= m_cols) return 0;
        if (0 >= grid[x][y]) return 0;

        int m = 0;
        // Cause the gold amount is positive, flip to mark it visited.
        grid[x][y] = -grid[x][y];
        for (auto& d : m_dirs) {
            m = max(m, walkAround(grid, x + d.first, y + d.second));
        }
        grid[x][y] = -grid[x][y];

        return m + grid[x][y];
    }
public:
    int getMaximumGold(vector<vector<int>>& grid) {
        m_rows = grid.size();
        m_cols = grid[0].size();
        int m = 0;
        for (int i = 0; i < m_rows; ++i) {
            for (int j = 0; j < m_cols; ++j) {
                m = max(m, walkAround(grid, i, j));
            }
        }
        return m;
    }
};
