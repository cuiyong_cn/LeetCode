/**
 * Repeated Substring Pattern
 *
 * Given a non-empty string check if it can be constructed by taking a substring of it and appending
 * multiple copies of the substring together. You may assume the given string consists of lowercase
 * English letters only and its length will not exceed 10000.
 *
 * Example 1:
 *
 * Input: "abab"
 * Output: True
 * Explanation: It's the substring "ab" twice.
 * Example 2:
 *
 * Input: "aba"
 * Output: False
 * Example 3:
 *
 * Input: "abcabcabcabc"
 * Output: True
 * Explanation: It's the substring "abc" four times. (And the substring "abcabc" twice.)
 */
/**
 * KMP solution.
 */
class Solution {
public:
    bool repeatedSubstringPattern(string str) {
        int i = 1, j = 0, n = str.size();

        vector<int> dp(n + 1, 0);
        while (i < str.size()){
            if (str[i] == str[j]) {
                dp[++i] = ++j;
            }
            else if (0 == j) {
                i++;
            }
            else {
                j = dp[j];
            }
        }

        return dp[n] && !(dp[n] % (n - dp[n]));
    }
};

/**
 * More compact and efficient I think.
 * Observations:
 * First char of input string is first char of repeated substring
 * Last char of input string is last char of repeated substring
 * Let S1 = S + S (where S in input string)
 * Remove 1 and last char of S1. Let this be S2
 * If S exists in S2 then return true else false
 * Let i be index in S2 where S starts then repeated substring length i + 1
 * and repeated substring S[0: i+1]
 */
class Solution {
public:
    bool repeatedSubstringPattern(string s) {
        auto s2 = s + s;
        return s2.substr(1, s2.length() - 2).find(s) != string::npos;
    }
};

/**
 * Original solution. Brute force. I'm suprised that this one got passed.
 */
class Solution {
public:
    bool repeatedSubstringPattern(string s) {
        int length = s.length();

        for (int i = 2; i <= length; ++i) {
            if (length % i) {
                continue;
            }

            bool ans = true;
            auto sublen = length / i;
            auto sub = s.substr(0, sublen);
            for (int j = 0; j <= (length - sublen); j += sublen) {
                if (s.compare(j, sublen, sub, 0, sublen)) {
                    ans = false;
                    break;
                }
            }

            if (ans) {
                return true;
            }
        }

        return false;
    }
};
