/**
 * Partition List
 *
 * Given the head of a linked list and a value x, partition it such that all nodes less than x come
 * before nodes greater than or equal to x.
 *
 * You should preserve the original relative order of the nodes in each of the two partitions.
 *
 * Example 1:
 *
 * Input: head = [1,4,3,2,5,2], x = 3
 * Output: [1,2,2,4,3,5]
 * Example 2:
 *
 * Input: head = [2,1], x = 2
 * Output: [1,2]
 *
 * Constraints:
 *
 * The number of nodes in the list is in the range [0, 200].
 * -100 <= Node.val <= 100
 * -200 <= x <= 200
 */
/**
 * Simplify the below solution.
 */
class Solution {
public:
    ListNode* partition(ListNode* head, int x) {
        ListNode before_head;
        ListNode* before = &before_head;
        ListNode after_head;
        ListNode* after = &after_head;

        while (head) {
            ListNode*& ref = head->val < x ? before : after;
            ref->next = head;
            ref = head;

            head = head->next;
        }

        after->next = nullptr;
        before->next = after_head.next;

        return before_head.next;
    }
};

/**
 * Original solution.
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* partition(ListNode* head, int x) {
        ListNode* greater_head = nullptr;
        ListNode* greater_tail = nullptr;
        ListNode* cur = head;
        ListNode* tail = nullptr;
        ListNode* prev = nullptr;
        while (cur) {
            if (cur->val >= x) {
                if (nullptr == greater_head) {
                    greater_head = cur;
                    greater_tail = cur;
                }
                else {
                    greater_tail->next = cur;
                    greater_tail = cur;
                }

                if (prev) {
                    prev->next = cur->next;
                }
                else {
                    head = cur->next;
                }
            }
            else {
                prev = cur;
                tail = cur;
            }

            cur = cur->next;
        }

        if (tail) {
            tail->next = greater_head;
        }
        
        if (greater_tail) {
            greater_tail->next = nullptr;
        }

        return head ? head : greater_head;
    }
};
