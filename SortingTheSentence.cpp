/**
 * Sorting the Sentence
 *
 * A sentence is a list of words that are separated by a single space with no leading or trailing
 * spaces. Each word consists of lowercase and uppercase English letters.
 *
 * A sentence can be shuffled by appending the 1-indexed word position to each word then rearranging
 * the words in the sentence.
 *
 * For example, the sentence "This is a sentence" can be shuffled as "sentence4 a3 is2 This1" or
 * "is2 sentence4 This1 a3".  Given a shuffled sentence s containing no more than 9 words,
 * reconstruct and return the original sentence.
 *
 * Example 1:
 *
 * Input: s = "is2 sentence4 This1 a3"
 * Output: "This is a sentence"
 * Explanation: Sort the words in s to their original positions "This1 is2 a3 sentence4", then remove the numbers.
 * Example 2:
 *
 * Input: s = "Myself2 Me1 I4 and3"
 * Output: "Me Myself and I"
 * Explanation: Sort the words in s to their original positions "Me1 Myself2 and3 I4", then remove the numbers.
 *
 * Constraints:
 *
 * 2 <= s.length <= 200
 * s consists of lowercase and uppercase English letters, spaces, and digits from 1 to 9.
 * The number of words in s is between 1 and 9.
 * The words in s are separated by a single space.
 * s contains no leading or trailing spaces.*
 */
/**
 * Using algo.
 */

int index(string const& s)
{
    return s.back() - '0' - 1;
}

class Solution {
public:
    string sortSentence(string s) {
        istringstream istream{s};
        vector<string> words{istream_iterator<string>{istream}, istream_iterator<string>{}};

        sort(words.begin(), words.end(),
             [](auto const& w1, auto const& w2) {
                 return index(w1) < index(w2);
             });

        for_each(words.begin(), words.end(), [](auto& w) { w.pop_back(); });

        ostringstream oss;
        copy(words.begin(), words.end(), ostream_iterator<string>{oss, " "});

        auto ans = oss.str();
        ans.pop_back();

        return ans;
    }
};

/**
 * More efficient one.
 */
class Solution {
public:
    string sortSentence(string s) {
        vector<string> words(9);
        s += ' ';
        int const slen = s.length();
        for (int i = 0, j = 1; j < slen; ++j) {
            if (' ' == s[j]) {
                auto idx = s[j - 1] - '0' - 1;
                words[idx] = s.substr(i, j - i - 1);
                i = j + 1;
            }
        }

        string ans;
        for (auto const& w : words) {
            if (w.empty()) {
                break;
            }
            ans += w;
            ans += ' ';
        }

        ans.pop_back();

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    string sortSentence(string s) {
        istringstream istream{s};
        vector<string> words{istream_iterator<string>{istream}, istream_iterator<string>{}};

        int const wl = words.size();
        for (int i = 0; i < wl;) {
            auto idx = words[i].back() - '0' - 1;
            if (i != idx) {
                swap(words[i], words[idx]);
            }
            else {
                ++i;
            }
        }

        string ans;
        for (auto const& w : words) {
            ans += w.substr(0, w.length() - 1) + ' ';
        }

        ans.pop_back();

        return ans;
    }
};
