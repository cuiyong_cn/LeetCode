/**
 * Evaluate Reverse Polish Notation
 *
 * Evaluate the value of an arithmetic expression in Reverse Polish Notation.
 *
 * Valid operators are +, -, *, and /. Each operand may be an integer or another expression.
 *
 * Note that division between two integers should truncate toward zero.
 *
 * It is guaranteed that the given RPN expression is always valid. That means the expression would
 * always evaluate to a result, and there will not be any division by zero operation.
 *
 * Example 1:
 *
 * Input: tokens = ["2","1","+","3","*"]
 * Output: 9
 * Explanation: ((2 + 1) * 3) = 9
 * Example 2:
 *
 * Input: tokens = ["4","13","5","/","+"]
 * Output: 6
 * Explanation: (4 + (13 / 5)) = 6
 * Example 3:
 *
 * Input: tokens = ["10","6","9","3","+","-11","*","/","*","17","+","5","+"]
 * Output: 22
 * Explanation: ((10 * (6 / ((9 + 3) * -11))) + 17) + 5
 * = ((10 * (6 / (12 * -11))) + 17) + 5
 * = ((10 * (6 / -132)) + 17) + 5
 * = ((10 * 0) + 17) + 5
 * = (0 + 17) + 5
 * = 17 + 5
 * = 22
 *
 * Constraints:
 *
 * 1 <= tokens.length <= 104
 * tokens[i] is either an operator: "+", "-", "*", or "/", or an integer in the range [-200, 200].
 */
/**
 * More compact one.
 */
class Solution {
public:
    int evalRPN(vector<string>& tokens) {
        vector<int> operands;
        map<char, std::function<int(int,int)>> command =
        {
            {'+', [](int a, int b) { return a + b; }},
            {'-', [](int a, int b) { return a - b; }},
            {'*', [](int a, int b) { return a * b; }},
            {'/', [](int a, int b) { return a / b; }}
        };
        for (auto const& t : tokens) {
            if (auto it = command.find(t.back()); it != command.end()) {
                auto op1 = operands.back(); operands.pop_back();
                auto op2 = operands.back(); operands.pop_back();
                operands.push_back(it->second(op2, op1));
            }
            else {
                operands.push_back(stoi(t));
            }
        }

        return operands.back();
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int evalRPN(vector<string>& tokens) {
        vector<int> operands;
        for (auto const& t : tokens) {
            auto ch = t.back();
            if ('+' == ch) {
                auto op1 = operands.back(); operands.pop_back();
                auto op2 = operands.back(); operands.pop_back();
                operands.push_back(op1 + op2);
            }
            else if ('-' == ch) {
                auto op1 = operands.back(); operands.pop_back();
                auto op2 = operands.back(); operands.pop_back();
                operands.push_back(op2 - op1);
            }
            else if ('*' == ch) {
                auto op1 = operands.back(); operands.pop_back();
                auto op2 = operands.back(); operands.pop_back();
                operands.push_back(op1 * op2);
            }
            else if ('/' == ch) {
                auto op1 = operands.back(); operands.pop_back();
                auto op2 = operands.back(); operands.pop_back();
                operands.push_back(op2 / op1);
            }
            else {
                operands.push_back(stoi(t));
            }
        }

        return operands.back();
    }
};
