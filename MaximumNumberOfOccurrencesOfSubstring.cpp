/**
 * Maximum Number of Occurrences of a Substring
 *
 * Given a string s, return the maximum number of ocurrences of any substring under the following rules:
 *
 * The number of unique characters in the substring must be less than or equal to maxLetters.
 * The substring size must be between minSize and maxSize inclusive.
 *
 * Example 1:
 *
 * Input: s = "aababcaab", maxLetters = 2, minSize = 3, maxSize = 4
 * Output: 2
 * Explanation: Substring "aab" has 2 ocurrences in the original string.
 * It satisfies the conditions, 2 unique letters and size 3 (between minSize and maxSize).
 * Example 2:
 *
 * Input: s = "aaaa", maxLetters = 1, minSize = 3, maxSize = 3
 * Output: 2
 * Explanation: Substring "aaa" occur 2 times in the string. It can overlap.
 * Example 3:
 *
 * Input: s = "aabcabcab", maxLetters = 2, minSize = 2, maxSize = 3
 * Output: 3
 * Example 4:
 *
 * Input: s = "abcde", maxLetters = 2, minSize = 3, maxSize = 3
 * Output: 0
 *
 * Constraints:
 *
 * 1 <= s.length <= 10^5
 * 1 <= maxLetters <= 26
 * 1 <= minSize <= maxSize <= min(26, s.length)
 * s only contains lowercase English letters.
 */
/**
 * Improvement. Actually we don't need to consider maxSize.
 */
class Solution {
public:
    int maxFreq(string s, int maxLetters, int minSize, int maxSize) {
        unordered_map<string, int> sub_freq;

        int letters[26] = { 0, };
        int unique = 0;
        for (size_t i = 0; (i + minSize) <= s.length(); ++i) {
            if (i > 0) {
                auto a = --letters[s[i - 1] - 'a'];
                auto b = ++letters[s[i + minSize - 1] - 'a'];
                if (!a) --unique;
                if (1 == b) ++ unique;
            }
            else {
                for (size_t j = i; j < (i + minSize); ++j) {
                    auto a = ++letters[s[j] - 'a'];
                    if (1 == a) ++unique;
                }
            }

            if (unique <= maxLetters) {
                ++sub_freq[s.substr(i, minSize)];
            }
        }

        auto it = max_element(sub_freq.begin(), sub_freq.end(), [](auto a, auto b) {
            return a.second < b.second;
        });


        return it != sub_freq.end() ? it->second : 0;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int maxFreq(string s, int maxLetters, int minSize, int maxSize) {
        unordered_map<string, int> sub_freq;

        for (size_t i = 0; i < s.length(); ++i) {
            for (size_t L = minSize; L <= maxSize && (i + L) <= s.length(); ++L) {
                if (unique_letters(s, i, i + L) <= maxLetters) {
                    ++sub_freq[s.substr(i, L)];
                }
                else {
                    break;
                }
            }
        }

        auto it = max_element(sub_freq.begin(), sub_freq.end(), [](auto a, auto b) {
            return a.second < b.second;
        });


        return it != sub_freq.end() ? it->second : 0;
    }

private:
    int unique_letters(const string& s, int start, int end)
    {
        int letters[26] = { 0 };
        int unique = 0;
        for (size_t i = start; i < end; ++i) {
            if (!letters[s[i] - 'a']) {
                letters[s[i] - 'a'] = 1;
                ++unique;
            }
        }

        return unique;
    }
};
