/**
 * Search Suggestions System
 *
 * Given an array of strings products and a string searchWord. We want to design a system that suggests at most three
 * product names from products after each character of searchWord is typed. Suggested products should have common
 * prefix with the searchWord. If there are more than three products with a common prefix return the three
 * lexicographically minimums products.
 *
 * Return list of lists of the suggested products after each character of searchWord is typed.
 *
 * Example 1:
 *
 * Input: products = ["mobile","mouse","moneypot","monitor","mousepad"], searchWord = "mouse"
 * Output: [
 * ["mobile","moneypot","monitor"],
 * ["mobile","moneypot","monitor"],
 * ["mouse","mousepad"],
 * ["mouse","mousepad"],
 * ["mouse","mousepad"]
 * ]
 * Explanation: products sorted lexicographically = ["mobile","moneypot","monitor","mouse","mousepad"]
 * After typing m and mo all products match and we show user ["mobile","moneypot","monitor"]
 * After typing mou, mous and mouse the system suggests ["mouse","mousepad"]
 *
 * Example 2:
 *
 * Input: products = ["havana"], searchWord = "havana"
 * Output: [["havana"],["havana"],["havana"],["havana"],["havana"],["havana"]]
 *
 * Example 3:
 *
 * Input: products = ["bags","baggage","banner","box","cloths"], searchWord = "bags"
 * Output: [["baggage","bags","banner"],["baggage","bags","banner"],["baggage","bags"],["bags"]]
 *
 * Example 4:
 *
 * Input: products = ["havana"], searchWord = "tatiana"
 * Output: [[],[],[],[],[],[],[]]
 *
 * Constraints:
 *
 *     1 <= products.length <= 1000
 *     There are no repeated elements in products.
 *     1 <= Σ products[i].length <= 2 * 10^4
 *     All characters of products[i] are lower-case English letters.
 *     1 <= searchWord.length <= 1000
 *     All characters of searchWord are lower-case English letters.
 */
/**
 * Orignal solution. Brute force. Pretty intuitive.
 */
class Solution {
public:
    vector<vector<string>> suggestedProducts(vector<string>& products, string searchWord) {
        vector<vector<string> > ans(searchWord.length());
        sort(products.begin(), products.end());

        for (auto& s : products) {
            int len = min(s.length(), searchWord.length());
            for (int i = 0; i < len; ++i) {
                if (s[i] != searchWord[i]) break;

                if (ans[i].size() < 3) {
                    ans[i].push_back(s);
                }
            }
        }
        return ans;
    }
};

/**
 * Binary Search Solution.
 */
class Solution {
public:
    vector<vector<string>> suggestedProducts(vector<string>& A, string searchWord) {
        auto it = A.begin();
        sort(it, A.end());
        vector<vector<string> > res;
        string cur = "";

        for (char c : searchWord) {
            cur += c;
            vector<string> suggested;
            it = lower_bound(it, A.end(), cur);
            for (int i = 0; i < 3 && it + i != A.end(); i++) {
                string& s = *(it + i);
                if (s.find(cur)) break;
                suggested.push_back(s);
            }
            res.push_back(suggested);
        }
        return res;
    }
};

/**
 * Trie method.
 */
class Solution {
private:
    class Trie
    {
    public:
        Trie() : sub(26), sorted{false} {}

        vector<shared_ptr<Trie> > sub;
        vector<string> suggestions;
        bool sorted;
    };
public:
    vector<vector<string>> suggestedProducts(vector<string>& A, string searchWord) {
        shared_ptr<Trie> root{new Trie};
        sort(A.begin(), A.end());
        for (auto& s : A) {
            shared_ptr<Trie> t = root;
            for (auto& c : s) {
                if (nullptr == t->sub[c - 'a']) {
                    t->sub[c - 'a'] = shared_ptr<Trie>(new Trie());
                }
                t = t->sub[c - 'a'];
                if (t->suggestions.size() < 3) {
                    t->suggestions.push_back(s);
                }
            }
        }
        
        vector<vector<string> > ans(searchWord.length());
        shared_ptr<Trie> t = root;
        for (int i = 0; i < searchWord.length(); ++i) {
            if (nullptr != t) {
                t = t->sub[searchWord[i] - 'a'];
            }
            if (nullptr != t) {
                ans[i] = t->suggestions;
            }
        }
        return ans;
    }
};
