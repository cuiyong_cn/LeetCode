/**
 * Coin Change 2
 *
 * You are given coins of different denominations and a total amount of money. Write a function to compute the number of
 * combinations that make up that amount. You may assume that you have infinite number of each kind of coin.
 *
 * Example 1:
 *
 * Input: amount = 5, coins = [1, 2, 5]
 * Output: 4
 * Explanation: there are four ways to make up the amount:
 * 5=5
 * 5=2+2+1
 * 5=2+1+1+1
 * 5=1+1+1+1+1
 *
 * Example 2:
 *
 * Input: amount = 3, coins = [2]
 * Output: 0
 * Explanation: the amount of 3 cannot be made up just with coins of 2.
 *
 * Example 3:
 *
 * Input: amount = 10, coins = [10]
 * Output: 1
 *
 * Note:
 *
 * You can assume that
 *     0 <= amount <= 5000
 *     1 <= coin <= 5000
 *     the number of coins is less than 500
 *     the answer is guaranteed to fit into signed 32-bit integer
 */
/**
 * Simplify the below solution.
 */
class Solution {
public:
    int change(int amount, vector<int>& coins) {

        vector<int> dp(amount + 1, 0);
        dp[0] = 1;

        for (auto coin : coins) {
            for (size_t j = 1; j <= amount; j++) {
                dp[j] += j < coin ? 0 : dp[j - coin];
            }
        }

        return dp[amount];
    }
};

/**
 * In order to save time, the solution from user tankztc.
 * This is a classic knapsack problem. Honestly, I'm not good at knapsack problem, it's really tough for me.

 * dp[i][j] : the number of combinations to make up amount j by using the first i types of coins.
 * dp[i][j] = dp[i - 1][j] + dp[i][j - coin[i - 1]]
 *
 * 1   not using the ith coin, only using the first i-1 coins to make up amount j, then we have dp[i-1][j] ways.
 * 2   using the ith coin, since we can use unlimited same coin, we need to know how many ways to make up amount
 *     j - coins[i-1] by using first i coins(including ith), which is dp[i][j-coins[i-1]]
 *
 * Initialization: dp[i][0] = 1
 */
class Solution {
public:
    int change(int amount, vector<int>& coins) {

        vector<vector<int>> dp(coins.size() + 1, vector<int>(amount + 1, 0));
        dp[0][0] = 1;

        for (size_t i = 1; i <= coins.size(); i++) {
            dp[i][0] = 1;
            for (size_t j = 1; j <= amount; j++) {
                dp[i][j] = dp[i-1][j] + (j >= coins[i-1] ? dp[i][j-coins[i-1]] : 0);
            }
        }

        return dp[coins.size()][amount];
    }
};

/**
 * Improved version. But still TLE
 */
class Solution {
public:
    int change(int amount, vector<int>& coins) {
        ans = 0;
        target = amount;

        dfs(0, 0, coins);

        return ans;
    }

private:
    void dfs(int cur, int index, const vector<int>& coins) {
        if (cur > target) return;
        if (cur == target) {
            ++ans;
            return;
        }

        for (size_t i = index; i < coins.size(); ++i) {
            dfs(cur + coins[i], i, coins);
        }
    }

private:
    int target;
    int ans;
};

/**
 * Original TLE solution.
 */
class Solution {
public:
    int change(int amount, vector<int>& coins) {
        ans = 0;
        target = amount;

        dfs(0, coins);

        return ans;
    }

private:
    void dfs(int cur, const vector<int>& coins) {
        if (cur > target) return;
        if (cur == target) {
            string combination_str;
            for (auto& p : memo) {
                int count = p.second;
                int coin = p.first;
                for (int i = 0; i < count; ++i) {
                    combination_str += to_string(coin) + "#";
                }
            }

            if (combins.count(combination_str) == 0) {
                ++ans;
                combins.insert(combination_str);
            }

            return;
        }

        for (size_t i = 0; i < coins.size(); ++i) {
            ++memo[coins[i]];
            dfs(cur + coins[i], coins);
            --memo[coins[i]];
        }
    }

private:
    int target;
    int ans;
    unordered_map<int, int> memo;
    set<string> combins;
};
