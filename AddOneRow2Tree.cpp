/**
 * Add One Row to Tree
 *
 * Given the root of a binary tree, then value v and depth d, you need to add a row of nodes with value v at the given
 * depth d. The root node is at depth 1.
 *
 * The adding rule is: given a positive integer depth d, for each NOT null tree nodes N in depth d-1, create two tree
 * nodes with value v as N's left subtree root and right subtree root. And N's original left subtree should be the left
 * subtree of the new left subtree root, its original right subtree should be the right subtree of the new right subtree
 * root. If depth d is 1 that means there is no depth d-1 at all, then create a tree node with value v as the new root
 * of the whole original tree, and the original tree is the new root's left subtree.
 *
 * Example 1:
 *
 * Input:
 * A binary tree as following:
 *        4
 *      /   \
 *     2     6
 *    / \   /
 *   3   1 5
 *
 * v = 1
 *
 * d = 2
 *
 * Output:
 *        4
 *       / \
 *      1   1
 *     /     \
 *    2       6
 *   / \     /
 *  3   1   5
 *
 * Example 2:
 *
 * Input:
 * A binary tree as following:
 *       4
 *      /
 *     2
 *    / \
 *   3   1
 *
 * v = 1
 *
 * d = 3
 *
 * Output:
 *       4
 *      /
 *     2
 *    / \
 *   1   1
 *  /     \
 * 3       1
 *
 * Note:
 *     The given d is in range [1, maximum depth of the given tree + 1].
 *     The given binary tree has at least one tree node.
 */
/**
 * Iterative solution. Java solution. But can easily converted into c++ version.
 */
public class Solution {
    class Node{
        Node(TreeNode n,int d){
            node=n;
            depth=d;
        }
        TreeNode node;
        int depth;
    }

    public TreeNode addOneRow(TreeNode t, int v, int d) {
        if (d == 1) {
            TreeNode n = new TreeNode(v);
            n.left = t;
            return n;
        }
        Stack<Node> stack=new Stack<>();
        stack.push(new Node(t,1));
        while(!stack.isEmpty())
        {
            Node n=stack.pop();
            if(n.node==null)
                continue;
            if (n.depth == d - 1 ) {
                TreeNode temp = n.node.left;
                n.node.left = new TreeNode(v);
                n.node.left.left = temp;
                temp = n.node.right;
                n.node.right = new TreeNode(v);
                n.node.right.right = temp;

            } else{
                stack.push(new Node(n.node.left, n.depth + 1));
                stack.push(new Node(n.node.right, n.depth + 1));
            }
        }
        return t;
    }
}


/**
 * Original recursive solution.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* addOneRow(TreeNode* root, int v, int d) {
        if (1 == d) {
            auto node = new TreeNode(v);
            node->left = root;
            return node;
        }

        return dfs(root, v, d, 1);
    }

private:
    TreeNode* dfs(TreeNode* root, int v, int d, int cur_depth)
    {
        if (root) {
            root->left = handleLeftTree(root->left, v, d, cur_depth + 1);
            root->right = handleRightTree(root->right, v, d, cur_depth + 1);
        }

        return root;
    }

    TreeNode* handleLeftTree(TreeNode* root, int v, int d, int cur_depth)
    {
        if (cur_depth == d) {
            auto node = new TreeNode(v);
            node->left = root;
            return node;
        }

        return dfs(root, v, d, cur_depth);
    }

    TreeNode* handleRightTree(TreeNode* root, int v, int d, int cur_depth)
    {
        if (cur_depth == d) {
            auto node = new TreeNode(v);
            node->right = root;
            return node;
        }

        return dfs(root, v, d, cur_depth);;
    }
};
