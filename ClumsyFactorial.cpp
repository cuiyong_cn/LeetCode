/**
 * Clumsy Factorial
 *
 * Normally, the factorial of a positive integer n is the product of all positive integers less than or equal to n.
 * For example, factorial(10) = 10 * 9 * 8 * 7 * 6 * 5 * 4 * 3 * 2 * 1.
 *
 * We instead make a clumsy factorial: using the integers in decreasing order, we swap out the multiply operations for
 * a fixed rotation of operations: multiply (*), divide (/), add (+) and subtract (-) in this order.
 *
 * For example, clumsy(10) = 10 * 9 / 8 + 7 - 6 * 5 / 4 + 3 - 2 * 1.  However, these operations are still applied using
 * the usual order of operations of arithmetic: we do all multiplication and division steps before any addition or
 * subtraction steps, and multiplication and division steps are processed left to right.
 *
 * Additionally, the division that we use is floor division such that 10 * 9 / 8 equals 11.  This guarantees the result
 * is an integer.
 *
 * Implement the clumsy function as defined above: given an integer N, it returns the clumsy factorial of N.
 *
 * Example 1:
 *
 * Input: 4
 * Output: 7
 * Explanation: 7 = 4 * 3 / 2 + 1
 * Example 2:
 *
 * Input: 10
 * Output: 12
 * Explanation: 12 = 10 * 9 / 8 + 7 - 6 * 5 / 4 + 3 - 2 * 1
 *
 * Note:
 *
 * 1 <= N <= 10000
 * -2^31 <= answer <= 2^31 - 1  (The answer is guaranteed to fit within a 32-bit integer.)
 */
/**
 * Math solution. The idea is as:
 *
 * Formula: i * (i-1) / (i-2) = i+1 when i >= 5
 * So factorial
 * f(N) = N * (N - 1) / (N - 2) + (N - 3) - (N - 4) * (N - 5) /(N - 6) + (N - 7) - ...
 * f(N) = N + 1 + (N - 3) - (N - 3) + (N - 7) - (N - 7) + ...
 * f(N) = N + 1 + ...
 *
 * we can call each 4 numbers a chunk, so from N // 4 we can know how many chunks there are,
 * then the rest 0, 1, 2 and 3 elements will influence our final result.
 *
 * 0 left: N + 1
 * 1 left: N + 1 + ... + 6 - 5*4/3 + 2 - 1         --> N + 2
 * 2 left: N + 1 + ... + 7 - 6*5/4 + 3 - 2 * 1     --> N + 2
 * 3 left: N + 1 + ... + 8 - 7*6/5 + 4 - 3 * 2 / 1 --> N - 1
 */
class Solution {
public:
    int clumsy(int N) {
        vector<int> magic = { 1, 2, 2, -1, 0, 0, 3, 3};
        int ans = N;
        if (N > 4) {
            ans += magic[N % 4];
        }
        else {
            ans += magic[N + 3];
        }

        return ans;
    }
};

/**
 * Original solution. Brute force solution.
 */
class Solution {
public:
    int base(int N) {
        if (N < 3) return N;
        if (3 == N) return 6;
        return 7;
    }

    int clumsy(int N) {
        if (N < 5) return base(N);

        int ans = N * (N - 1) / (N - 2) + (N - 3);
        N -= 4;

        while (N > 3) {
            int p = - N * (N - 1) / (N - 2) + (N - 3);
            ans += p;
            N -= 4;
        }

        if (N > 0) {
            ans -= base(N);
        }

        return ans;
    }
};
