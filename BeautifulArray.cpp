/**
 * Beautiful Array
 *
 * For some fixed N, an array A is beautiful if it is a permutation of the integers 1, 2, ..., N, such that:
 *
 * For every i < j, there is no k with i < k < j such that A[k] * 2 = A[i] + A[j].
 *
 * Given N, return any beautiful array A.  (It is guaranteed that one exists.)
 *
 * Example 1:
 *
 * Input: 4
 * Output: [2,1,4,3]
 *
 * Example 2:
 *
 * Input: 5
 * Output: [3,1,2,5,4]
 *
 * Note:
 *
 *     1 <= N <= 1000
 */
/**
 * Really confusing solution. But the core idea is the same.
 */
class Solution {
public:
    int partition(vector<int> &v, int start, int end, int mask)
    {
        int j = start;
        for(int i = start; i <= end; i++)
        {
            if((v[i] & mask) != 0)
            {
                swap(v[i], v[j]);
                j++;
            }
        }
        return j;
    }
    
    void sort(vector<int> & v, int start, int end, int mask)
    {
        if(start >= end) return;
        int mid = partition(v, start, end, mask);
        sort(v, start, mid - 1, mask << 1);
        sort(v, mid, end, mask << 1);
    }
    
    vector<int> beautifulArray(int N) {
        vector<int> ans;
        for(int i = 0; i < N; i++) ans.push_back(i + 1);
        sort(ans, 0, N - 1, 1);
        return ans;
    }
};
/**
 * Iterative version.
 */
class Solution
{
public:
    vector<int> beautifulArray(int N) {
        vector<int> res = {1};
        while (res.size() < N) {
            vector<int> tmp;
            for (int i : res) if (i * 2 - 1 <= N) tmp.push_back(i * 2 - 1);
            for (int i : res) if (i * 2 <= N) tmp.push_back(i * 2);
            res = tmp;
        }
        return res;
    }
};

/**
 * Divide and conquer. The idea is:
 *
 * I. Given a beautiful array A, the following properties holds:
 * (1) A' = A*c is a Beautiful array
 * (2) A' = A + c is a Beautiful array
 * (3) if A' is an array obtained by deleting some element in A, then A' is still Beautiful.
 *
 * II. Given two Beautiful array A and B, whose elements are odd and even respectively, then concatenation array A + B is still Beautiful.
 */
class Solution {
public:
    vector<int> beautifulArray(int N) {
        return f(N);
    }
private:
    vector<int>& f(int N)
    {
        if (memo.count(N) == 1) {
            return memo[N];
        }

        vector<int> ans(N);
        if (N == 1) {
            ans[0] = 1;
        }
        else {
            int t = 0;
            for (auto& n : f((N + 1) >> 1)) {
                ans[t++] = 2 * n - 1;
            }
            for (auto& n : f(N >> 1)) {
                ans[t++] = 2 * n;
            }
        }

        memo[N] = std::move(ans);
        return memo[N];
    }

    map<int, vector<int> > memo;
};

/**
 * Mathematic one. But don't knnow how to fill the odd and even.
 * So still use another dumb mehtod to sort it out. Better than below of course. But if N > 24 then TLE again.
 *          [ odd , even ]
 */
class Solution {
public:
    vector<int> beautifulArray(int N) {
        vector<int> ans(N);
        int next = 1;
        for (int i = 0; i < N; ++i) {
            ans[i] = next; next += 2;
            if (next > N) next = 2;
        }

        int mid = N >> 1;
        if (N & 1) ++mid;

        correctify(ans, 0, mid);
        correctify(ans, mid, N);

        return ans;
    }
private:
    void correctify(vector<int>& a, int s, int e)
    {
        int odd = a[s] & 1;
        bool beauty = false;
        while (false == beauty) {
            int b = true;
            for (int i = s; i < e - 2; ++i) {
                for (int j = i + 2; j < e; ++j) {
                    int t = (a[i] + a[j]) >> 1;
                    if ((t & 1) == odd) {
                        for (int m = i + 1; m < j; ++m) {
                            if (a[m] == t) {
                                swap(a[m], a[j]);
                                m = j;
                                b = false;
                            }
                        }
                    }
                }
            }
            beauty = b;
        }
    }
};

/**
 * Brute force. check all the permutations to see if we get one.
 * But obviously, this is TLE solution.
 */
class Solution {
public:
    vector<int> beautifulArray(int N) {
        vector<int> ans(N, 0);
        for (int i = 0; i < N; ++i) {
            ans[i] = i + 1;
        }

        dfs(ans, 0);
        return ans;
    }
private:
    bool dfs(vector<int>& a, int i)
    {
        if (i == a.size()) {
            return isBeatifulArray(a);
        }

        for (int k = i; k < a.size(); ++k) {
            swap(a[i], a[k]);
            if (true == dfs(a, i + 1)) {
                return true;
            }
            swap(a[i], a[k]);
        }
        return false;
    }

    bool isBeatifulArray(vector<int>& a)
    {
        if (a.size() < 2) {
            return true;
        }

        for (int i = 0; i < (a.size() - 2); ++i) {
            for (int j = i + 2; j < a.size(); ++j) {
                int ht = a[i] + a[j];
                for (int k = i + 1; k < j; ++k) {
                    int k2 = a[k] << 1;
                    if (k2 == ht) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
};
