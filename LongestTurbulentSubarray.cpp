/**
 * Longest Turbulent Subarray
 *
 * A subarray A[i], A[i+1], ..., A[j] of A is said to be turbulent if and only if:
 *
 * For i <= k < j, A[k] > A[k+1] when k is odd, and A[k] < A[k+1] when k is even;
 * OR, for i <= k < j, A[k] > A[k+1] when k is even, and A[k] < A[k+1] when k is odd.
 * That is, the subarray is turbulent if the comparison sign flips between each adjacent pair of elements in the subarray.
 *
 * Return the length of a maximum size turbulent subarray of A.
 *
 * Example 1:
 *
 * Input: [9,4,2,10,7,8,8,1,9]
 * Output: 5
 * Explanation: (A[1] > A[2] < A[3] > A[4] < A[5])
 * Example 2:
 *
 * Input: [4,8,12,16]
 * Output: 2
 * Example 3:
 *
 * Input: [100]
 * Output: 1
 *
 * Note:
 *
 * 1 <= A.length <= 40000
 * 0 <= A[i] <= 10^9
 */
/**
 * Flip the sign. but I prefer the inc and dec one.
 */
class Solution {
public:
    int maxTurbulenceSize(vector<int>& A, int res = 0) {
        for (auto i = 0, cnt = 0; i + 1 < A.size(); ++i, cnt *= -1) {
            if (A[i] > A[i + 1]) {
                cnt = cnt > 0 ? cnt + 1 : 1;
            }
            else if (A[i] < A[i + 1]) {
                cnt = cnt < 0 ? cnt - 1 : -1;
            }
            else {
                cnt = 0;
            }

            res = max(res, abs(cnt));
        }
        return res + 1;
    }
};

/**
 * A dp one.
 */
class Solution {
public:
    int maxTurbulenceSize(vector<int>& A) {
        vector<vector<int>> dp(A.size(), vector<int>(2, 1));
        int res = 1;

        for (int i = 1; i < A.size(); i++) {
            if (A[i] > A[i - 1]) {
                dp[i][0] = dp[i - 1][1] + 1;
            }
            else if (A[i] < A[i - 1]) {
                dp[i][1] = dp[i - 1][0] + 1;
            }
            res = max(res, max(dp[i][0], dp[i][1]));   
        }
        return res;
    }
};

/**
 * Same idea but more readable.
 */
class Solution {
public:
    int maxTurbulenceSize(vector<int>& A) {
        int inc = 1, dec = 1, ans = 1;

        for (int i = 1; i < A.size(); ++i) {
            if (A[i] < A[i - 1]) {
                dec = inc + 1;
                inc = 1;
            } else if (A[i] > A[i - 1]) {
                inc = dec + 1;
                dec = 1;
            } else {
                inc = 1;
                dec = 1;
            }

            ans = std::max(ans, std::max(dec, inc));
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int maxTurbulenceSize(vector<int>& A) {
        if (1 == A.size()) {
            return 1;
        }

        int ans = A[0] == A[1] ? 1 : 2;
        int prev = A[0] < A[1] ? -1 : A[0] > A[1] ? 1 : 0;
        int length = ans;

        for (int i = 2; i < A.size(); ++i) {
            int cur = A[i - 1] < A[i] ? -1 : A[i - 1] > A[i] ? 1 : 0;
            if ((prev < 0 && cur > 0) || (prev > 0 && cur < 0)) {
                ++length;
            }
            else {
                ans = std::max(length, ans);
                length = 0 == cur ? 1 : 2;
            }
            prev = cur;
        }

        return std::max(length, ans);
    }
};
