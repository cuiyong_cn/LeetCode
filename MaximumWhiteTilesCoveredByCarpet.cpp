/**
 * Maximum White Tiles Covered by a Carpet
 *
 * You are given a 2D integer array tiles where tiles[i] = [li, ri] represents that every tile j in
 * the range li <= j <= ri is colored white.
 *
 * You are also given an integer carpetLen, the length of a single carpet that can be placed anywhere.
 *
 * Return the maximum number of white tiles that can be covered by the carpet.
 *
 * Example 1:
 *
 * Input: tiles = [[1,5],[10,11],[12,18],[20,25],[30,32]], carpetLen = 10
 * Output: 9
 * Explanation: Place the carpet starting on tile 10.
 * It covers 9 white tiles, so we return 9.
 * Note that there may be other places where the carpet covers 9 white tiles.
 * It can be shown that the carpet cannot cover more than 9 white tiles.
 * Example 2:
 *
 * Input: tiles = [[10,11],[1,1]], carpetLen = 2
 * Output: 2
 * Explanation: Place the carpet starting on tile 10.
 * It covers 2 white tiles, so we return 2.
 *
 * Constraints:
 *
 * 1 <= tiles.length <= 5 * 104
 * tiles[i].length == 2
 * 1 <= li <= ri <= 109
 * 1 <= carpetLen <= 109
 * The tiles are non-overlapping.
 */
/**
 * Original solution.
 */
class Solution {
public:
    int maximumWhiteTiles(vector<vector<int>>& tiles, int carpetLen) {
        sort(begin(tiles), end(tiles));

        auto front = 0;
        auto back = 0;
        auto cover = 0;
        auto ans = 0;
        while (ans < carpetLen && back < tiles.size()) {
            auto p_back = tiles[front][0] + carpetLen;
            if (p_back <= tiles[back][1]) {
                auto extra = max(0, p_back - tiles[back][0]);
                ans = max(ans, cover + extra);
                cover -= tiles[front][1] - tiles[front][0] + 1;
                ++front;
            }
            else {
                cover += tiles[back][1] - tiles[back][0] + 1;
                ans = max(ans, cover);
                ++back;
            }
        }
        return ans;
    }
};
