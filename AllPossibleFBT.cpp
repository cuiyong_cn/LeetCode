/**
 * All Possible Full Binary Trees
 *
 * A full binary tree is a binary tree where each node has exactly 0 or 2 children.
 *
 * Return a list of all possible full binary trees with N nodes.  Each element
 * of the answer is the root node of one possible tree.
 *
 * Each node of each tree in the answer must have node.val = 0.
 *
 * You may return the final list of trees in any order.
 *
 * Example 1:
 *
 * Input: 7
 * Output: [
 * [0,0,0,null,null,0,0,null,null,0,0],
 * [0,0,0,null,null,0,0,0,0],
 * [0,0,0,0,0,0,0],
 * [0,0,0,0,0,null,null,null,null,0,0],
 * [0,0,0,0,0,null,null,0,0]]
 * Explanation:
 *
 * Note:
 *
 *     1 <= N <= 20
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* clone(TreeNode* root)
    {
        TreeNode* new_root = new TreeNode(0);
        new_root->left = (root->left) ? clone(root->left) : nullptr;
        new_root->right = (root->right) ? clone(root->right) : nullptr;
        return new_root;
    }

    vector<TreeNode*> allPossibleFBT(int N)
    {
        std::vector<TreeNode*> ret;
        if (1 == N) {
            ret.emplace_back(new TreeNode{0});
        } else if (N % 2) {
            for (int i = 2; i <= N; i += 2) {
                auto left = allPossibleFBT(i - 1);
                auto right = allPossibleFBT(N - i);
                for (int l_idx = 0; l_idx < left.size(); ++l_idx) {
                    for (int r_idx = 0; r_idx < right.size(); ++r_idx) {
                      ret.emplace_back(new TreeNode(0));

                      ret.back()->left = (r_idx == right.size() - 1) ? left[l_idx] : clone(left[l_idx]);
                      ret.back()->right = (l_idx == left.size() - 1) ? right[r_idx] : clone(right[r_idx]);
                    }
                }
            }
        }
        return ret;
    }
};
