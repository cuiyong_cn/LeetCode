/**
 * Matrix Similarity After Cyclic Shifts
 *
 * You are given a 0-indexed m x n integer matrix mat and an integer k. You have to cyclically right
 * shift odd indexed rows k times and cyclically left shift even indexed rows k times.
 *
 * Return true if the initial and final matrix are exactly the same and false otherwise.
 *
 * Example 1:
 *
 * Input: mat = [[1,2,1,2],[5,5,5,5],[6,3,6,3]], k = 2
 * Output: true
 * Explanation:
 *
 * Initially, the matrix looks like the first figure.
 * Second figure represents the state of the matrix after one right and left cyclic shifts to even and odd indexed rows.
 * Third figure is the final state of the matrix after two cyclic shifts which is similar to the initial matrix.
 * Therefore, return true.
 *
 * Example 2:
 *
 * Input: mat = [[2,2],[2,2]], k = 3
 * Output: true
 * Explanation: As all the values are equal in the matrix, even after performing cyclic shifts the matrix will remain the same. Therefeore, we return true.
 *
 * Example 3:
 *
 * Input: mat = [[1,2]], k = 1
 * Output: false
 * Explanation: After one cyclic shift, mat = [[2,1]] which is not equal to the initial matrix. Therefore we return false.
 *
 * Constraints:
 *     1 <= mat.length <= 25
 *     1 <= mat[i].length <= 25
 *     1 <= mat[i][j] <= 25
 *     1 <= k <= 50
 */
/**
 * Improved one. Here is why left or right does not matter.
 *     For example, say k = 3, then we need to compare:
 *         For odd index, mat[i][0] == mat[i][3] , mat[i][1] == mat[i][4], mat[i][2] == mat[i][5]
 *         For even index, mat[i][3] == mat[i][0], mat[i][4] == mat[i][1], mat[i][5] == mat[i][2]
 *     We can see, they are pair-wise symmetry.
 */
class Solution {
public:
    bool areSimilar(vector<vector<int>>& mat, int k) {
        int const cols = mat[0].size();

        for (auto const& row : mat) {
            for (auto i = 0; i < cols; ++i) {
                if (row[i] != row[(i + k) % cols]) {
                    return false;
                }
            }
        }

        return true;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool areSimilar(vector<vector<int>>& mat, int k) {
        int const cols = mat[0].size();
        int const rows = mat.size();

        k = k % cols;

        for (auto i = 0; i < rows; ++i) {
            auto const upper = (i % 2) ? k : cols - k;

            for (auto j = 0; j < upper; ++j) {
                if (mat[i][(j + k) % cols] != mat[i][(k + j + k) % cols]) {
                    return false;
                }
            }
        }

        return true;
    }
};
