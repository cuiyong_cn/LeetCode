/**
 * Subarray Sum Equals K
 *
 * Given an array of integers nums and an integer k, return the total number of continuous subarrays
 * whose sum equals to k.
 *
 * Example 1:
 *
 * Input: nums = [1,1,1], k = 2
 * Output: 2
 * Example 2:
 *
 * Input: nums = [1,2,3], k = 3
 * Output: 2
 *
 * Constraints:
 *
 * 1 <= nums.length <= 2 * 104
 * -1000 <= nums[i] <= 1000
 * -107 <= k <= 107
 */
/**
 * solution from leetcode.
 *
 * The idea behind this approach is as follows:
 *
 * If the cumulative sum upto two indices is the same, the sum of the elements lying in between
 * those indices is zero. Extending the same thought further, if the cumulative sum upto two indices,
 * say i and j is at a difference of k i.e. if sum[i] - sum[j] = k, the sum of elements lying
 * between indices i and j is k.
 *
 * Based on these thoughts, we make use of a hashmap mapmap which is used to store the cumulative
 * sum upto all the indices possible along with the number of times the same sum occurs. We store
 * the data in the form: (sum_i, count of occurences of sum_i)
 * We traverse over the array nums and keep on finding the cumulative sum. Every time we encounter
 * a new sum, we make a new entry in the hashmap corresponding to that sum. If the same sum occurs
 * again, we increment the count corresponding to that sum in the hashmap. Further, for every sum
 * encountered, we also determine the number of times the sum - k has occured already, since it
 * will determine the number of times a subarray with sum k has occured upto the current index. We
 * increment the count by the same amount.
 *
 * After the complete array has been traversed, the countcount gives the required result.
 */
class Solution {
public:
    int subarraySum(vector<int>& nums, int k) {
        int ans = 0;
        unordered_map<int, int> sum_count;

        sum_count[0] = 1;
        int sum = 0;
        for (auto n : nums) {
            sum += n;
            if (sum_count.count(sum - k)) {
                ans += sum_count[sum - k];
            }
            ++sum_count[sum];
        }

        return ans;
    }
};

/**
 * The only solution I can think of. But sadly TLE.
 * First I thought about sliding window, but as it contains
 * negative number, this solution does not work.
 */
class Solution {
public:
    int subarraySum(vector<int>& nums, int k) {
        int ans = 0;
        for (int i = 0; i < nums.size(); ++i) {
            int sum = 0;
            for (int j = i; j < nums.size(); ++j) {
                sum += nums[j];
                if (k == sum) {
                    ++ans;
                }
            }
        }

        return ans;
    }
};
