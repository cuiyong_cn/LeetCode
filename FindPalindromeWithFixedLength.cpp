/**
 * Find Palindrome With Fixed Length
 *
 * Given an integer array queries and a positive integer intLength, return an array answer where
 * answer[i] is either the queries[i]th smallest positive palindrome of length intLength or -1 if no
 * such palindrome exists.
 *
 * A palindrome is a number that reads the same backwards and forwards. Palindromes cannot have leading zeros.
 *
 * Example 1:
 *
 * Input: queries = [1,2,3,4,5,90], intLength = 3
 * Output: [101,111,121,131,141,999]
 * Explanation:
 * The first few palindromes of length 3 are:
 * 101, 111, 121, 131, 141, 151, 161, 171, 181, 191, 202, ...
 * The 90th palindrome of length 3 is 999.
 * Example 2:
 *
 * Input: queries = [2,4,6], intLength = 4
 * Output: [1111,1331,1551]
 * Explanation:
 * The first six palindromes of length 4 are:
 * 1001, 1111, 1221, 1331, 1441, and 1551.
 *
 * Constraints:
 *
 * 1 <= queries.length <= 5 * 104
 * 1 <= queries[i] <= 109
 * 1 <= intLength <= 15
 */
/**
 * Original solution.
 */
long long make_palindrome(int half, int len)
{
    if (1 == len) {
        return half;
    }

    auto left = to_string(half);
    auto right = len & 1 ? to_string(half / 10) : left;
    reverse(begin(right), end(right));
    return stoll(left + right);
}

class Solution {
public:
    vector<long long> kthPalindrome(vector<int>& queries, int intLength) {
        auto ans = vector<long long>(queries.size(), -1);
        auto half_len = (int)ceil(intLength * 1.0 / 2);
        auto max_half = stoll(string(half_len, '9'));
        auto base = (int)pow(10, half_len - 1) - 1;

        for (auto i = 0; i < queries.size(); ++i) {
            auto const palin_half = base + queries[i];
            if (max_half < palin_half) {
                continue;
            }

            ans[i] = make_palindrome(palin_half, intLength);
        }

        return ans;
    }
};
