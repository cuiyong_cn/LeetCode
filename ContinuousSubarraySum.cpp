/**
 * Continuous Subarray Sum
 *
 * Given an integer array nums and an integer k, return true if nums has a continuous subarray of
 * size at least two whose elements sum up to a multiple of k, or false otherwise.
 *
 * An integer x is a multiple of k if there exists an integer n such that x = n * k. 0 is always a
 * multiple of k.
 *
 * Example 1:
 *
 * Input: nums = [23,2,4,6,7], k = 6
 * Output: true
 * Explanation: [2, 4] is a continuous subarray of size 2 whose elements sum up to 6.
 * Example 2:
 *
 * Input: nums = [23,2,6,4,7], k = 6
 * Output: true
 * Explanation: [23, 2, 6, 4, 7] is an continuous subarray of size 5 whose elements sum up to 42.
 * 42 is a multiple of 6 because 42 = 7 * 6 and 7 is an integer.
 * Example 3:
 *
 * Input: nums = [23,2,6,4,7], k = 13
 * Output: false
 *
 * Constraints:
 *
 * 1 <= nums.length <= 105
 * 0 <= nums[i] <= 109
 * 0 <= sum(nums[i]) <= 231 - 1
 * 1 <= k <= 231 - 1
 */
/**
 * Math solution. The idea is:
 * calc running sum mod at every index i
 * if there exists two same mod and their distance is greater than 1. The subarray exists.
 *
 * for example.
 * say at index i, running sum is a, mod c = a % k
 *     at index j, running sum is b, mod c = b % k
 *     the difference of a and b is b - a
 *     a = a1 * k + c, b = b1 * k + c
 *     b - a = (b1 - a1) * k  --> multiple of k --> means subarray s[i, j], its sum is multiple of k
 */
class Solution {
public:
    bool checkSubarraySum(vector<int>& nums, int k) {
        int run_sum = 0;
        unordered_map<int, int> mod_pos = {{0, -1}};
        for (int i = 0; i < nums.size(); ++i) {
            run_sum += nums[i];
            auto mod = run_sum % k;
            if (mod_pos.count(mod)) {
                if ((i - mod_pos[mod]) > 1) {
                    return true;
                }
            }
            else {
                mod_pos[mod] = i;
            }
        }

        return false;
    }
};

/**
 * Original solution. partial_sum solution.
 */
class Solution {
public:
    bool checkSubarraySum(vector<int>& nums, int k) {
        vector<int> psum(nums.size() + 1, 0);
        partial_sum(nums.begin(), nums.end(), psum.begin() + 1);

        if (nums.size() >= 2 && sub_sum_for_len(psum, 2, k)) {
            return true;
        }

        int max_k = numeric_limits<int>::max() / 2;
        if (k > max_k) {
            return false;
        }

        for (int L = 3; L <= nums.size(); ++L) {
            if (sub_sum_for_len(psum, L, k)) {
                return true;
            }
        }

        return false;
    }

private:
    bool sub_sum_for_len(vector<int> const& psum, int len, int k)
    {
        int last = psum.size() - len;
        for (int i = 0; i < last; ++i) {
            auto sum = psum[i + len] - psum[i];
            if (0 == sum || (0 == (sum % k))) {
                return true;
            }
        }

        return false;
    }
};
