/**
 * Increasing Triplet Subsequence
 *
 * Given an integer array nums, return true if there exists a triple of indices (i, j, k) such that
 * i < j < k and nums[i] < nums[j] < nums[k]. If no such indices exists, return false.
 *
 * Example 1:
 *
 * Input: nums = [1,2,3,4,5]
 * Output: true
 * Explanation: Any triplet where i < j < k is valid.
 * Example 2:
 *
 * Input: nums = [5,4,3,2,1]
 * Output: false
 * Explanation: No triplet exists.
 * Example 3:
 *
 * Input: nums = [2,1,5,0,4,6]
 * Output: true
 * Explanation: The triplet (3, 4, 5) is valid because nums[3] == 0 < nums[4] == 4 < nums[5] == 6.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 105
 * -231 <= nums[i] <= 231 - 1
 *
 * Follow up: Could you implement a solution that runs in O(n) time complexity and O(1) space complexity?
 */
/**
 * Simplify below solution.
 */
class Solution {
public:
    bool increasingTriplet(vector<int>& nums) {
        auto low = numeric_limits<int>::max();
        auto mid = low;

        for (auto n : nums) {
            if (n <= low) {
                low = n;
            }
            else if (n <= mid) {
                mid = n;
            }
            else {
                return true;
            }
        }

        return false;
    }
};

/**
 * Original solution. A bit of ugly but works though not efficient.
 */
class Solution {
public:
    bool increasingTriplet(vector<int>& nums) {
        vector<vector<int>> incr_seq;
        for (auto n : nums) {
            for (auto&& seq : incr_seq) {
                if (2 == seq.size()) {
                    if (n > seq.back()) {
                        return true;
                    }
                    
                    if (n > seq.front()) {
                        seq.back() = n;
                    }
                }
                else {
                    if (n > seq.back()) {
                        seq.push_back(n);
                    }
                    else {
                        seq.back() = n;
                    }
                }
            }
            incr_seq.push_back({n});
        }

        return false;
    }
};
