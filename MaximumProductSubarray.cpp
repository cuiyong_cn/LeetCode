/**
 * Maximum Product Subarray
 *
 * Given an integer array nums, find a contiguous non-empty subarray within the array that has the
 * largest product, and return the product.
 *
 * It is guaranteed that the answer will fit in a 32-bit integer.
 *
 * A subarray is a contiguous subsequence of the array.
 *
 * Example 1:
 *
 * Input: nums = [2,3,-2,4]
 * Output: 6
 * Explanation: [2,3] has the largest product 6.
 * Example 2:
 *
 * Input: nums = [-2,0,-1]
 * Output: 0
 * Explanation: The result cannot be 2, because [-2,-1] is not a subarray.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 2 * 104
 * -10 <= nums[i] <= 10
 * The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit integer.
 */
/**
 * I was stuck in thinking prefix_prod. Actually we should know that a num times another non-zero
 * number will result a bigger absolute value.
 */
class Solution {
public:
    int maxProduct(vector<int>& A) {
        // store the result that is the max we have found so far
        int r = A[0];
        int n = A.size();

        // imax/imin stores the max/min product of
        // subarray that ends with the current number A[i]
        for (int i = 1, imax = r, imin = r; i < n; i++) {
            // multiplied by a negative makes big number smaller, small number bigger
            // so we redefine the extremums by swapping them
            if (A[i] < 0) {
                swap(imax, imin);
            }

            // max/min product for the current number is either the current number itself
            // or the max/min by the previous number times the current one
            imax = max(A[i], imax * A[i]);
            imin = min(A[i], imin * A[i]);

            // the newly computed max value is a candidate for our global result
            r = max(r, imax);
        }

        return r;
    }
};

/**
 * Original solution. TLE.
 */
class Solution {
public:
    int maxProduct(vector<int>& nums) {
        vector<pair<int*, int>> subs;

        int ans = numeric_limits<int>::min();
        int i = 0, j = 0, len = nums.size();
        int* data = nums.data();        
        while (j < len) {
            while (j < len && 0 != nums[j]) {
                ++j;
            }

            if (j < len) {
                ans = 0;
            }

            if ((j - i) >= 0) {
                subs.emplace_back(data + i, j - i);
            }

            i = ++j;
        }

        
        for (auto sub : subs) {
            ans = max(ans, maxProd(sub.first, sub.second));
        }

        return ans;
    }

private:
    int maxProd(int* nums, int len)
    {
        vector<int> prefix_prod(len + 1, 1);
        for (int i = 0; i < len; ++i) {
            prefix_prod[i + 1] = prefix_prod[i] * nums[i];
        }

        int ans = numeric_limits<int>::min();
        for (int L = 1; L <= len; ++L) {
            for (int s = 0; (s + L) <= len; ++s) {
                ans = max(ans, prefix_prod[s + L] / prefix_prod[s]);
            }
        }

        return ans;
    }
};
