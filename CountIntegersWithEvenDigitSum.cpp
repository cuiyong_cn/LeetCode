/**
 * Count Integers With Even Digit Sum
 *
 * Given a positive integer num, return the number of positive integers less than or equal to num
 * whose digit sums are even.
 *
 * The digit sum of a positive integer is the sum of all its digits.
 *
 * Example 1:
 *
 * Input: num = 4
 * Output: 2
 * Explanation:
 * The only integers less than or equal to 4 whose digit sums are even are 2 and 4.
 * Example 2:
 *
 * Input: num = 30
 * Output: 14
 * Explanation:
 * The 14 integers less than or equal to 30 whose digit sums are even are
 * 2, 4, 6, 8, 11, 13, 15, 17, 19, 20, 22, 24, 26, and 28.
 *
 * Constraints:
 *
 * 1 <= num <= 1000
 */
/**
 * Further optimize. Observe every 20 range, we get 10 numbers satisfy the requirement.
 */
bool digits_sum_even(int n)
{
    auto ans = 0;
    auto sum = 0;
    while (n > 0) {
        sum += n % 10;
        n /= 10;
    }

    return 0 == (sum % 2);
}

class Solution {
public:
    int countEven(int num) {
        return digits_sum_even(num) ? num / 2 : (num - 1) / 2;
    }
};

/**
 * Optimize.
 */
bool digits_sum_even(int n)
{
    auto ans = 0;
    auto sum = 0;
    while (n > 0) {
        sum += n % 10;
        n /= 10;
    }

    return 0 == (sum % 2);
}

class Solution {
public:
    int countEven(int num) {
        auto ans = 0;
        for (int i = 2, up = 10; i <= num; up+=10) {
            if (!digits_sum_even(i)) {
                ++i;
            }

            auto upper = min(up, num);
            for (; i < upper; i+=2, ++ans) {
                ;
            }
            i = up;
        }

        return ans + digits_sum_even(num);
    }
};

/**
 * Original solution.
 */
bool digits_sum_even(int n)
{
    auto ans = 0;
    auto sum = 0;
    while (n > 0) {
        sum += n % 10;
        n /= 10;
    }

    return 0 == (sum % 2);
}

class Solution {
public:
    int countEven(int num) {
        auto ans = 0;
        for (int i = 2; i <= num; ++i) {
            if (digits_sum_even(i)) {
                ++ans;
            }
        }
        return ans;
    }
};
