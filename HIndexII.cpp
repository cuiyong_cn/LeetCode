/**
 * H-Index II
 *
 * Given an array of integers citations where citations[i] is the number of citations a researcher
 * received for their ith paper and citations is sorted in an ascending order, return compute the
 * researcher's h-index.
 *
 * According to the definition of h-index on Wikipedia: A scientist has an index h if h of their n
 * papers have at least h citations each, and the other n − h papers have no more than h citations
 * each.
 *
 * If there are several possible values for h, the maximum one is taken as the h-index.
 *
 * You must write an algorithm that runs in logarithmic time.
 *
 * Example 1:
 *
 * Input: citations = [0,1,3,5,6]
 * Output: 3
 * Explanation: [0,1,3,5,6] means the researcher has 5 papers in total and each of them had received 0, 1, 3, 5, 6 citations respectively.
 * Since the researcher has 3 papers with at least 3 citations each and the remaining two with no
 * more than 3 citations each, their h-index is 3.
 * Example 2:
 *
 * Input: citations = [1,2,100]
 * Output: 2
 *
 * Constraints:
 *
 * n == citations.length
 * 1 <= n <= 105
 * 0 <= citations[i] <= 1000
 * citations is sorted in ascending order.
 */
/**
 * From discussion. Rework.
 */
class Solution {
public:
    int hIndex(vector<int>& citations) {
        int const N = citations.size();
        int left = 0, right = N - 1;

        while (left <= right) {
            int mid = (left + right) / 2;
            int h = N - mid;
            if (citations[mid] == h) {
                return h;
            }
            else if (citations[mid] > h) {
                right = mid - 1;
            }
            else {
                left = mid + 1;
            }
        }

        return N - right - 1;
    }
};

/**
 * Original solution. I hate this solution though it passed the test.
 */
class Solution {
public:
    int hIndex(vector<int>& citations) {
        int const N = citations.size();
        int left = 0, right = N;
        if (citations[0] >= N) {
            return N;
        }

        while (left < right) {
            int len = (left + right) / 2;
            if (citations[N - 1 - len] >= len) {
                left = len + 1;
            }
            else {
                right = len;
            }
        }

        return N == left ? left - 1 :
               0 == left ? left :
               citations[N - left] >= left ? left : left - 1;
    }
};
