/**
 * Two City Scheduling
 *
 * There are 2N people a company is planning to interview. The cost of flying the i-th person to city A is costs[i][0],
 * and the cost of flying the i-th person to city B is costs[i][1].
 *
 * Return the minimum cost to fly every person to a city such that exactly N people arrive in each city.
 *
 * Example 1:
 *
 * Input: [[10,20],[30,200],[400,50],[30,20]]
 * Output: 110
 * Explanation:
 * The first person goes to city A for a cost of 10.
 * The second person goes to city A for a cost of 30.
 * The third person goes to city B for a cost of 50.
 * The fourth person goes to city B for a cost of 20.
 *
 * The total minimum cost is 10 + 30 + 50 + 20 = 110 to have half the people interviewing in each city.
 *
 * Note:
 *
 *     1 <= costs.length <= 100
 *     It is guaranteed that costs.length is even.
 *     1 <= costs[i][0], costs[i][1] <= 1000
 */
/**
 * Solution from leetcode user vobtuc. The idea is we should save as much as possible. So we sort the array by how much
 * we can save by deciding whether we fly person to A or B.
 */
class Solution {
public:
    int twoCitySchedCost(vector<vector<int>>& costs) {
        sort(costs.begin(), costs.end(), [](vector<int>& a, vector<int>& b) { return (a[0] - a[1]) < (b[0] - b[1]); });

        int N = costs.size() >> 1;
        int ans = 0;
        for (int i = 0; i < N; ++i) {
            ans += costs[i][0] + costs[i + N][1];
        }
        return ans;
    }
};

/**
 * Calculate the costs of possible permutation. Guess what? TLE...
 */
class Solution {
public:
    int twoCitySchedCost(vector<vector<int>>& costs) {
        minCosts = numeric_limits<int>::max();
        N = costs.size() >> 1;
        vector<bool> toACity(costs.size(), false);
        dfs(costs, toACity, 0, 0);
        return minCosts;
    }

    void dfs(vector<vector<int> >& costs, vector<bool>& aCity, int idx, int n) {
        if (idx > (costs.size() -N + n)) return;
        if (n == N) {
            int ACosts = 0;
            int BCosts = 0;
            for (int i = 0; i < costs.size(); ++i) {
                if (true == aCity[i]) {
                    ACosts += costs[i][0];
                }
                else {
                    BCosts += costs[i][1];
                }
            }
            minCosts = min(minCosts, ACosts + BCosts);
            return;
        }

        aCity[idx] = true;
        dfs(costs, aCity, idx + 1, n + 1);
        aCity[idx] = false;
        dfs(costs, aCity, idx + 1, n);
    }
private:
    int N;
    int minCosts;
};
