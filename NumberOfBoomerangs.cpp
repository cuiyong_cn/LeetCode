/**
 * Number of Boomerangs
 *
 * Given n points in the plane that are all pairwise distinct, a "boomerang" is a tuple of points (i, j, k) such that the distance between i and j equals the distance between i and k (the order of the tuple matters).
 *
 * Find the number of boomerangs. You may assume that n will be at most 500 and coordinates of points are all in the range [-10000, 10000] (inclusive).
 *
 * Example:
 *
 * Input:
 * [[0,0],[1,0],[2,0]]
 *
 * Output:
 * 2
 *
 * Explanation:
 * The two boomerangs are [[1,0],[0,0],[2,0]] and [[1,0],[2,0],[0,0]]
 */
/**
 * Improved version.
 */
class Solution {
public:
    int numberOfBoomerangs(vector<vector<int>>& points) {
        int ans = 0;
        unordered_map<int, int> memo;

        for (int i = 0; i < points.size(); ++i) {
            for (int j = 0; j < points.size(); ++j) {
                int x1 = points[i][0], x2 = points[j][0];
                int y1 = points[i][1], y2 = points[j][1];
                int dx = x1 - x2;
                int dy = y1 - y2;
                int distance = dx * dx + dy * dy;
                ++memo[distance];
            }
            for (auto& p : memo) {
                ans += p.second * (p.second - 1);
            }
            memo.clear();
        }

        return ans;
    }
};

/**
 * original solution. very intuitive. But efficieny... don't think about it.
 * Time O(n^2)
 * Space O(n)
 */
class Solution {
public:
    struct Point
    {
        int x;
        int y;
        bool operator==(const Point& p) const{
            return x == p.x && y == p.y;
        }
    };

    bool haveSamePoint(const pair<Point, Point>& p1, const pair<Point, Point>& p2) {
        return p1.first == p2.first
            || p1.first == p2.second
            || p1.second == p2.first
            || p1.second == p2.second;
    }

    int numberOfBoomerangs(vector<vector<int>>& points) {
        int ans = 0;
        map<int, vector<pair<Point, Point>>> memo;

        for (int i = 0; i < points.size(); ++i) {
            for (int j = i + 1; j < points.size(); ++j) {
                int x1 = points[i][0], x2 = points[j][0];
                int y1 = points[i][1], y2 = points[j][1];
                int dx = x1 - x2;
                int dy = y1 - y2;
                int distance = dx * dx + dy * dy;
                memo[distance].push_back({{x1, y1}, {x2, y2}});
            }
        }

        for (auto& p : memo) {
            auto& vec = p.second;
            for (int i = 0; i < vec.size(); ++i) {
                for (int j = i + 1; j < vec.size(); ++j) {
                    if (haveSamePoint(vec[i], vec[j])) {
                        ++ans;
                    }
                }
            }
        }

        return ans * 2;
    }
};
