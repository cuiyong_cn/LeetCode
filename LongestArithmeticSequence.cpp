/**
 * Longest Arithmetic Sequence
 *
 * Given an array A of integers, return the length of the longest arithmetic subsequence in A.
 *
 * Recall that a subsequence of A is a list A[i_1], A[i_2], ..., A[i_k] with 0 <= i_1 < i_2 < ... < i_k <= A.length - 1,
 * and that a sequence B is arithmetic if B[i+1] - B[i] are all the same value (for 0 <= i < B.length - 1).
 *
 * Example 1:
 *
 * Input: [3,6,9,12]
 * Output: 4
 * Explanation:
 * The whole array is an arithmetic sequence with steps of length = 3.
 *
 * Example 2:
 *
 * Input: [9,4,7,2,10]
 * Output: 3
 * Explanation:
 * The longest arithmetic subsequence is [4,7,10].
 *
 * Example 3:
 *
 * Input: [20,1,15,3,10,5,8]
 * Output: 4
 * Explanation:
 * The longest arithmetic subsequence is [20,15,10,5].
 *
 * Note:
 *
 *     2 <= A.length <= 2000
 *     0 <= A[i] <= 10000
 */
/**
 * DP solution.
 */
class Solution {
public:
    int longestArithSeqLength(vector<int>& A) {
        unordered_map<int, unordered_map<int, int>> dp;
        int res = 2, n = A.size();
        for (int i = 0; i < n; ++i)
        for (int j = i + 1; j < n; ++j)  {
            int d = A[j] - A[i];
            dp[d][j] = dp[d].count(i) ? dp[d][i] + 1 : 2;
            res = max(res, dp[d][j]);
        }

        return res;
    }
};
/**
 * Same idea but fast.
 */
class Solution {
public:
    int longestArithSeqLength(vector<int>& A, int res = 2) {
        unordered_map<int, vector<int>> m;

        for (auto i = 0; i < A.size(); ++i) {
             m[A[i]].push_back(i);
        }

        for (auto i = 0; i < A.size() - res; ++i)
        for (auto j = i + 1; j < A.size() - res; ++j) {
            int seq = 2;
            int step = A[j] - A[i];
            int k = j;
            while (1) {
                auto it = m.find(A[k] + step);
                if (it == end(m)) break;
                auto it1 = upper_bound(begin(it->second), end(it->second), k);
                if (it1 == end(it->second)) break;

                k = *it1;
            }
            res = max(res, seq);
        }
        return res;
    }
};


/**
 * Original solution. Brute force, using memo to speed it up.
 */
class Solution {
public:
    unordered_map<int, vector<int>> memo;
    int dfs(const vector<int>& A, int s1, int s2) {
        int diff = A[s2] - A[s1];

        if (memo.count(diff)) {
            for (auto& c : memo[diff]) {
                if (c == s1) return 0;
            }
        }

        vector<int> path = {s1, s2};
        int next = A[s2] + diff;
        for (int i = s2 + 1; i < A.size(); ++i) {
            if (A[i] == next) {
                path.push_back(i);
                next += diff;
            }
        }

        memo[diff] = path;

        return path.size();
    }

    int longestArithSeqLength(vector<int>& A) {
        int ans = 2;
        for (int i = 0; i < A.size(); ++i) {
            if (ans > (A.size() - i) / 2) break;
            for (int j = i + 1; j < A.size(); ++j) {
                ans = max(ans, dfs(A, i, j));
            }
        }
        return ans;
    }
};
