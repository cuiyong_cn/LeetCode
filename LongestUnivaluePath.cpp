/**
 * Longest Univalue Path
 *
 * Given the root of a binary tree, return the length of the longest path, where each node in the
 * path has the same value. This path may or may not pass through the root.
 *
 * The length of the path between two nodes is represented by the number of edges between them.
 *
 * Example 1:
 *
 * Input: root = [5,4,5,1,1,5]
 * Output: 2
 * Example 2:
 *
 * Input: root = [1,4,5,4,4,5]
 * Output: 2
 *
 * Constraints:
 *
 * The number of nodes in the tree is in the range [0, 104].
 * -1000 <= Node.val <= 1000
 * The depth of the tree will not exceed 1000
 */
/**
 * Make it shorter.
 */
class Solution {
public:
    int longestUnivaluePath(TreeNode* root) {
        dfs(root);

        return ans;
    }

private:
    int dfs(TreeNode* root)
    {
        if (nullptr == root) {
            return 0;
        }

        int left = dfs(root->left);
        int right = dfs(root->right);
        int to_left = 0;
        int to_right = 0;
        if (root->left && root->left->val == root->val) {
            to_left = left + 1;
        }

        if (root->right && root->right->val == root->val) {
            to_right = right + 1;
        }

        ans = max(ans, to_left + to_right);

        return max(to_left, to_right);
    }

private:
    int ans = 0;
};

/**
 * Optimize, we don't need the map.
 */
class Solution {
public:
    int longestUnivaluePath(TreeNode* root) {
        dfs(root);

        return ans > 0 ? ans - 1 : 0;
    }

private:
    int dfs(TreeNode* root)
    {
        if (nullptr == root) {
            return 0;
        }

        int left = 0;
        if (root->left) {
            if (root->left->val == root->val) {
                left = dfs(root->left);
            }
            else {
                dfs(root->left);
            }
        }

        int right = 0;
        if (root->right) {
            if (root->right->val == root->val) {
                right = dfs(root->right);
            }
            else {
                dfs(root->right);
            }
        }

        int left_right = 0;
        if (root->left && root->right && root->left->val == root->val && root->val == root->right->val) {
            left_right = left + right + 1;
        }

        int max_length = max(max(left, right) + 1, left_right);

        if (max_length > ans) {
            ans = max_length;
        }

        return max(left, right) + 1;
    }

private:
    int ans = 0;
};

/**
 * Original solution.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int longestUnivaluePath(TreeNode* root) {
        dfs(root);
        int ans = 0;
        for (auto const& upl : unival_path_max) {
            if (upl.second > ans) {
                ans = upl.second;
            }
        }

        return ans > 0 ? ans - 1 : 0;
    }

private:
    int dfs(TreeNode* root)
    {
        if (nullptr == root) {
            return 0;
        }

        int left = 0;
        if (root->left) {
            if (root->left->val == root->val) {
                left = dfs(root->left);
            }
            else {
                dfs(root->left);
            }
        }

        int right = 0;
        if (root->right) {
            if (root->right->val == root->val) {
                right = dfs(root->right);
            }
            else {
                dfs(root->right);
            }
        }

        int left_right = 0;
        if (root->left && root->right && root->left->val == root->val && root->val == root->right->val) {
            left_right = left + right + 1;
        }

        int max_length = max(max(left, right) + 1, left_right);

        if (max_length > unival_path_max[root->val]) {
            unival_path_max[root->val] = max_length;
        }

        return max(left, right) + 1;
    }

private:
    unordered_map<int, int> unival_path_max;
};
