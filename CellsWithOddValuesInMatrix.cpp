/**
 * Cells with Odd Values in a Matrix
 *
 * There is an m x n matrix that is initialized to all 0's. There is also a 2D array indices where
 * each indices[i] = [ri, ci] represents a 0-indexed location to perform some increment operations
 * on the matrix.
 *
 * For each location indices[i], do both of the following:
 *
 * Increment all the cells on row ri.
 * Increment all the cells on column ci.
 * Given m, n, and indices, return the number of odd-valued cells in the matrix after applying the
 * increment to all locations in indices.
 *
 * Example 1:
 *
 * Input: m = 2, n = 3, indices = [[0,1],[1,1]]
 * Output: 6
 * Explanation: Initial matrix = [[0,0,0],[0,0,0]].
 * After applying first increment it becomes [[1,2,1],[0,1,0]].
 * The final matrix is [[1,3,1],[1,3,1]], which contains 6 odd numbers.
 * Example 2:
 *
 * Input: m = 2, n = 2, indices = [[1,1],[0,0]]
 * Output: 0
 * Explanation: Final matrix = [[2,2],[2,2]]. There are no odd numbers in the final matrix.
 *
 * Constraints:
 *
 * 1 <= m, n <= 50
 * 1 <= indices.length <= 100
 * 0 <= ri < m
 * 0 <= ci < n
 *
 * Follow up: Could you solve this in O(n + m + indices.length) time with only O(n + m) extra space?
 */
/**
 * Optimise. Idea:
 * Let's assume there are
 *      row_odds rows are incremented odd times
 *      col_odds columns are incremented odd times
 * then
 *      there are row_odds * n elements are odd
 *      there are col_odds * m elements are odd
 *      there are row_odds * col_odds we counted twice which we need to exclude
 *      there aer row_odds * col_ddds elements which are even (odd + odd = even) we also need to exclude
 * hence
 *      total = row_odds * n + col_odds * m - 2 * row_odds * col_odds
 */
class Solution {
public:
    int oddCells(int m, int n, vector<vector<int>>& indices) {
        bitset<50> row;
        bitset<50> col;
        for (auto const& id : indices) {
            row.flip(id[0]);
            col.flip(id[1]);
        }

        int row_odds = row.count();
        int col_odds = col.count();
        return row_odds * n + col_odds * m - 2 * row_odds * col_odds;
    }
};

/**
 * Reduce time and space usage.
 */
class Solution {
public:
    int oddCells(int m, int n, vector<vector<int>>& indices) {
        vector<int> row(m, 0);
        vector<int> column(n, 0);
        for (auto const& id : indices) {
            ++row[id[0]];
            ++column[id[1]];
        }

        int ans = 0;
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                auto count = row[i] + column[j];
                if (count & 1) {
                    ++ans;
                }
            }
        }

        return ans;
    }
};

/**
 * Original solution. Simulation.
 */
class Solution {
public:
    int oddCells(int m, int n, vector<vector<int>>& indices) {
        vector<vector<int>> matrix(m, vector<int>(n, 0));
        for (auto const& id : indices) {
            auto r = id[0];
            auto c = id[1];
            for (int i = 0; i < n; ++i) {
                ++matrix[r][i];
            }
            for (int i = 0; i < m; ++i) {
                ++matrix[i][c];
            }
        }

        int ans = 0;
        for (auto const& row : matrix) {
            for (auto const& v : row) {
                if (v & 1) {
                    ++ans;
                }
            }
        }

        return ans;
    }
};
