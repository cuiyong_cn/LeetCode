/**
 * Count Primes
 *
 * Count the number of prime numbers less than a non-negative number, n.
 *
 * Example 1:
 *
 * Input: n = 10
 * Output: 4
 * Explanation: There are 4 prime numbers less than 10, they are 2, 3, 5, 7.
 * Example 2:
 *
 * Input: n = 0
 * Output: 0
 * Example 3:
 *
 * Input: n = 1
 * Output: 0
 *
 * Constraints:
 *
 * 0 <= n <= 5 * 106
 */
/**
 * Passed one.
 */
class Solution {
public:
    int countPrimes(int n) {
        vector<bool> prime(n, true);
        for (int i = 2; (i * i) < n; ++i) {
            if (false == prime[i]) {
                continue;
            }

            for (int j = i * i; j < n; j += i) {
                prime[j] = false;
            }
        }

        int ans = 0;
        for (int i = 2; i < n; ++i) {
            if (prime[i]) {
                ++ans;
            }
        }

        return ans;
    }
};

/**
 * Original solution. TLE.
 */
class Solution {
public:
    int countPrimes(int n) {
        int ans = 0;
        for (int i = 2; i < n; ++i) {
            if (is_prime(i)) {
                ++ans;
            }
        }

        return ans;
    }

private:
    bool is_prime(int num)
    {
        if (num < 2) {
            return false;
        }
        
        for (int i = 2; (i * i) <= num; ++i) {
            if (0 == (num % i)) {
                return false;
            }
        }
        return true;
    }
};
