/**
 * Minimum Number of Valid Strings to Form Target I
 *
 * You are given an array of strings words and a string target.
 *
 * A string x is called valid if x is a prefix of any string in words.
 *
 * Return the minimum number of valid strings that can be concatenated to form target. If it is not
 * possible to form target, return -1.
 *
 * Example 1:
 *
 * Input: words = ["abc","aaaaa","bcdef"], target = "aabcdabc"
 *
 * Output: 3
 *
 * Explanation:
 *
 * The target string can be formed by concatenating:
 *     Prefix of length 2 of words[1], i.e. "aa".
 *     Prefix of length 3 of words[2], i.e. "bcd".
 *     Prefix of length 3 of words[0], i.e. "abc".
 *
 * Example 2:
 *
 * Input: words = ["abababab","ab"], target = "ababaababa"
 *
 * Output: 2
 *
 * Explanation:
 *
 * The target string can be formed by concatenating:
 *     Prefix of length 5 of words[0], i.e. "ababa".
 *     Prefix of length 5 of words[0], i.e. "ababa".
 *
 * Example 3:
 *
 * Input: words = ["abcdef"], target = "xyz"
 *
 * Output: -1
 *
 * Constraints:
 *     1 <= words.length <= 100
 *     1 <= words[i].length <= 5 * 103
 *     The input is generated such that sum(words[i].length) <= 105.
 *     words[i] consists only of lowercase English letters.
 *     1 <= target.length <= 5 * 103
 *     target consists only of lowercase English letters.
 */
/**
 * Passed one.
 */
constexpr int invalid = 50001;

struct Trie
{
    Trie* next[26] = { nullptr, };
};

void build(Trie& t, string const& str)
{
    auto p = &t;

    for (auto c : str) {
        if (nullptr == p->next[c - 'a']) {
            p->next[c - 'a'] = new Trie{};
        }

        p = p->next[c - 'a'];
    }
}

bool is_prefix(string_view str, Trie const& t)
{
    auto p = &t;

    for (auto c : str) {
        if (nullptr == p->next[c - 'a']) {
            return false;
        }
        p = p->next[c - 'a'];
    }

    return true;
}

class Solution {
public:
    int minValidStrings(vector<string>& words, string target) {
        for (auto const& w : words) {
            build(trie, w);
        }

        tar = &target;
        dp = vector<int>(target.length(), -1);

        if (auto ans = dfs(0); ans != invalid) {
            return ans;
        }

        return -1;
    }

private:
    int dfs(int n)
    {
        auto const& target = *tar;
        auto const size = static_cast<int>(target.size());

        if (n == size) {
            return 0;
        }

        if (dp[n] != -1) {
            return dp[n];
        }

        int ans = invalid;
        auto p = &trie;

        for (auto i = n; i < size; ++i) {
            if (nullptr == p->next[target[i] - 'a']) {
                break;
            }
            p = p->next[target[i] - 'a'];
            ans = min(ans, dfs(i + 1) + 1);
        }

        return dp[n] = ans;
    }

private:
    vector<int> dp;
    Trie trie;
    string* tar;
};


/**
 * Original solution. TLE.
 */
constexpr int invalid = 50001;

struct Trie
{
    vector<Trie*> next{26, nullptr};
};

void build(Trie& t, string const& str)
{
    auto p = &t;

    for (auto c : str) {
        if (nullptr == p->next[c - 'a']) {
            p->next[c - 'a'] = new Trie{};
        }

        p = p->next[c - 'a'];
    }
}

bool is_prefix(string_view str, Trie const& t)
{
    auto p = &t;

    for (auto c : str) {
        if (nullptr == p->next[c - 'a']) {
            return false;
        }
        p = p->next[c - 'a'];
    }

    return true;
}

class Solution {
public:
    int minValidStrings(vector<string>& words, string target) {
        for (auto const& w : words) {
            build(trie, w);
        }

        tar = &target;
        dp = vector<int>(target.length(), invalid);

        if (auto ans = dfs(0); ans != invalid) {
            return ans;
        }

        return -1;
    }

private:
    int dfs(int n)
    {
        auto const& target = *tar;
        auto const size = static_cast<int>(target.size());

        if (n == size) {
            return 0;
        }

        if (dp[n] != invalid) {
            return dp[n];
        }

        int ans = invalid;
        auto p = &trie;

        for (auto i = n; i < size; ++i) {
            if (nullptr == p->next[target[i] - 'a']) {
                break;
            }
            p = p->next[target[i] - 'a'];
            ans = min(ans, dfs(i + 1) + 1);
        }

        return dp[n] = ans;
    }

private:
    vector<int> dp;
    Trie trie;
    string* tar;
};
