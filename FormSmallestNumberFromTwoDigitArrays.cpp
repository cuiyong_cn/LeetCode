/**
 * Form Smallest Number From Two Digit Arrays
 *
 * Given two arrays of unique digits nums1 and nums2, return the smallest number that contains at least one digit from each array.
 *
 *
 * Example 1:
 *
 * Input: nums1 = [4,1,3], nums2 = [5,7]
 * Output: 15
 * Explanation: The number 15 contains the digit 1 from nums1 and the digit 5 from nums2. It can be proven that 15 is the smallest number we can have.
 * Example 2:
 *
 * Input: nums1 = [3,5,2,6], nums2 = [3,1,7]
 * Output: 3
 * Explanation: The number 3 contains the digit 3 which exists in both arrays.
 *
 *
 * Constraints:
 *
 * 1 <= nums1.length, nums2.length <= 9
 * 1 <= nums1[i], nums2[i] <= 9
 * All digits in each array are unique.
 */
/**
 * Same idea.
 */
class Solution {
public:
    int minNumber(vector<int>& nums1, vector<int>& nums2) {
        for (auto i = 1; i < 10; ++i) {
            auto cnt1 = count(begin(nums1), end(nums1), i);
            if (1 == cnt1) {
                auto cnt2 = count(begin(nums2), end(nums2), i);
                if (1 == cnt2) {
                    return i;
                }
            }
        }

        auto m1 = *min_element(begin(nums1), end(nums1));
        auto m2 = *min_element(begin(nums2), end(nums2));
        return min(10 * m1 + m2, 10 * m2 + m1);
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int minNumber(vector<int>& nums1, vector<int>& nums2) {
        auto cnt1 = array<int, 10>{ 0, };
        auto cnt2 = cnt1;
        for (auto&& n : nums1) {
            ++cnt1[n];
        }
        for (auto&& n : nums2) {
            ++cnt2[n];
        }

        for (auto i = 1; i < 10; ++i) {
            if (cnt1[i] > 0 && cnt2[i] > 0) {
                return i;
            }
        }

        for (auto i = 1; i < 10; ++i) {
            if (0 == cnt1[i]) {
                continue;
            }
            for (auto j = 1; j < 10; ++j) {
                if (cnt2[j] > 0) {
                    return min(10 * i + j, 10 * j + i);
                }
            }
        }

        return 0;
    }
};
