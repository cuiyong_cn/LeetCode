/**
 * Swap Adjacent in LR String
 *
 * In a string composed of 'L', 'R', and 'X' characters, like "RXXLRXRXL", a move consists of either
 * replacing one occurrence of "XL" with "LX", or replacing one occurrence of "RX" with "XR". Given
 * the starting string start and the ending string end, return True if and only if there exists a
 * sequence of moves to transform one string to the other.
 *
 * Example 1:
 *
 * Input: start = "RXXLRXRXL", end = "XRLXXRRLX"
 * Output: true
 * Explanation: We can transform start to end following these steps:
 * RXXLRXRXL ->
 * XRXLRXRXL ->
 * XRLXRXRXL ->
 * XRLXXRRXL ->
 * XRLXXRRLX
 * Example 2:
 *
 * Input: start = "X", end = "L"
 * Output: false
 * Example 3:
 *
 * Input: start = "LLR", end = "RRL"
 * Output: false
 * Example 4:
 *
 * Input: start = "XL", end = "LX"
 * Output: true
 * Example 5:
 *
 * Input: start = "XLLR", end = "LXLX"
 * Output: false
 *
 * Constraints:
 *
 * 1 <= start.length <= 104
 * start.length == end.length
 * Both start and end will only consist of characters in 'L', 'R', and 'X'.
 */
/**
 * Optimize.
 * Frist, we don't need to swap and amke s become e.
 * Second, observe that L can only move left, R can only move right.
 * And L and R can not cross each other.
 * So basically X does not matter, we only need to consider their relative order.
 */
class Solution {
public:
    bool canTransform(string s, string e) {
        int const N = s.length();
        int i = 0;
        int j = 0;

        while (true) {
            while (i < N && 'X' == s[i]) {
                i++;
            }
            while (j < N && 'X' == e[j]) {
                j++;
            }

            if (N == i || N == j) {
                break;
            }

            if (s[i] != e[j]) {
                return false;
            }

            if (s[i] == 'R' && i > j) {
                return false;
            }
            if (s[i] == 'L' && i < j) {
                return false;
            }

            i++;
            j++;
        }

        return i == j;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool canTransform(string start, string end) {
        int const N = start.length();
        for (int i = 0; i < N; ++i) {
            if (start[i] != end[i]) {
                if ('L' == start[i]) {
                    return false;
                }

                if ('R' == start[i]) {
                    if ('L' == end[i]) {
                        return false;
                    }

                    int j = i + 1;
                    for (; j < N; ++j) {
                        if ('L' == start[j]) {
                            return false;
                        }

                        if ('X' == start[j]) {
                            swap(start[i], start[j]);
                            break;
                        }
                    }
                    if (N == j) {
                        return false;
                    }
                }
                else {
                    if ('R' == end[i]) {
                        return false;
                    }

                    int j = i + 1;
                    for (; j < N; ++j) {
                        if ('R' == start[j]) {
                            return false;
                        }
                        if ('L' == start[j]) {
                            swap(start[i], start[j]);
                            break;
                        }
                    }
                    if (N == j) {
                        return false;
                    }
                }
            }
        }

        return start.back() == end.back();
    }
};
