/**
 * LRU Cache
 *
 * Design a data structure that follows the constraints of a Least Recently Used (LRU) cache.
 *
 * Implement the LRUCache class:
 *
 * LRUCache(int capacity) Initialize the LRU cache with positive size capacity.
 * int get(int key) Return the value of the key if the key exists, otherwise return -1.
 * void put(int key, int value) Update the value of the key if the key exists. Otherwise, add the
 * key-value pair to the cache. If the number of keys exceeds the capacity from this operation,
 * evict the least recently used key.
 * The functions get and put must each run in O(1) average time complexity.
 *
 * Example 1:
 *
 * Input
 * ["LRUCache", "put", "put", "get", "put", "get", "put", "get", "get", "get"]
 * [[2], [1, 1], [2, 2], [1], [3, 3], [2], [4, 4], [1], [3], [4]]
 * Output
 * [null, null, null, 1, null, -1, null, -1, 3, 4]
 *
 * Explanation
 * LRUCache lRUCache = new LRUCache(2);
 * lRUCache.put(1, 1); // cache is {1=1}
 * lRUCache.put(2, 2); // cache is {1=1, 2=2}
 * lRUCache.get(1);    // return 1
 * lRUCache.put(3, 3); // LRU key was 2, evicts key 2, cache is {1=1, 3=3}
 * lRUCache.get(2);    // returns -1 (not found)
 * lRUCache.put(4, 4); // LRU key was 1, evicts key 1, cache is {4=4, 3=3}
 * lRUCache.get(1);    // return -1 (not found)
 * lRUCache.get(3);    // return 3
 * lRUCache.get(4);    // return 4
 *
 * Constraints:
 *
 * 1 <= capacity <= 3000
 * 0 <= key <= 104
 * 0 <= value <= 105
 * At most 2 * 105 calls will be made to get and put.
 */
/**
 * Hash + List
 */
class LRUCache {
public:
    LRUCache(int capacity) : _capacity(capacity) {}

    int get(int key) {
        auto it = cache.find(key);
        if (it == cache.end()) {
            return -1;
        }

        touch(it);
        return it->second.first;
    }

    void put(int key, int value) {
        auto it = cache.find(key);
        if (it != cache.end()) {
            touch(it);
        }
        else {
			if (cache.size() == _capacity) {
				cache.erase(used.back());
				used.pop_back();
			}
            used.push_front(key);
        }
        cache[key] = { value, used.begin() };
    }

private:
    typedef int _key;
    typedef int _value;
    typedef list<_key> LI;
    typedef pair<_value, LI::iterator> PII;
    typedef unordered_map<_key, PII> HIPII;

    void touch(HIPII::iterator it) {
        int key = it->first;
        used.erase(it->second.second);
        used.push_front(key);
        it->second.second = used.begin();
    }

    HIPII cache;
    LI used;
    int _capacity;
};

/**
 * Original solution.
 */
class LRUCache {
public:
    LRUCache(int cnt) : capacity{cnt}, count{0}, seq{0} {

    }

    int get(int key) {
        auto kv = key_value.find(key);
        if (kv != key_value.end()) {
            kv->second.second = ++seq;
            least_queue.push({-seq, key});
            return kv->second.first;
        }

        return -1;
    }

    void put(int key, int value) {
        ++seq;
        auto kv = key_value.find(key);
        if (kv == key_value.end()) {
            if (count == capacity) {
                while (!least_queue.empty()) {
                    auto [s, key] = least_queue.top(); least_queue.pop();
                    auto tmp = key_value.find(key);
                    if (tmp != key_value.end()) {
                        if ((s + tmp->second.second) == 0) {
                            key_value.erase(key);
                            break;
                        }
                    }
                }
            }
            else {
                ++count;
            }

            kv = key_value.insert({key, {value, seq}}).first;
        }

        kv->second.first = value;
        kv->second.second = seq;

        least_queue.push({-seq, key});
    }

private:
    int capacity;
    int count;
    int seq;
    unordered_map<int, pair<int, int>> key_value;
    priority_queue<pair<int, int>> least_queue;
};

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache* obj = new LRUCache(capacity);
 * int param_1 = obj->get(key);
 * obj->put(key,value);
 */
