/**
 * Count the Number of Good Nodes
 *
 * There is an undirected tree with n nodes labeled from 0 to n - 1, and rooted at node 0. You are
 * given a 2D integer array edges of length n - 1, where edges[i] = [ai, bi] indicates that there is
 * an edge between nodes ai and bi in the tree.
 *
 * A node is good if all the subtrees rooted at its children have the same size.
 *
 * Return the number of good nodes in the given tree.
 *
 * A subtree of treeName is a tree consisting of a node in treeName and all of its descendants.
 *
 * Example 1:
 *
 * Input: edges = [[0,1],[0,2],[1,3],[1,4],[2,5],[2,6]]
 *
 * Output: 7
 *
 * Explanation:
 *
 * All of the nodes of the given tree are good.
 *
 * Example 2:
 *
 * Input: edges = [[0,1],[1,2],[2,3],[3,4],[0,5],[1,6],[2,7],[3,8]]
 *
 * Output: 6
 *
 * Explanation:
 *
 * There are 6 good nodes in the given tree. They are colored in the image above.
 *
 * Example 3:
 *
 * Input: edges = [[0,1],[1,2],[1,3],[1,4],[0,5],[5,6],[6,7],[7,8],[0,9],[9,10],[9,12],[10,11]]
 *
 * Output: 12
 *
 * Explanation:
 *
 * All nodes except node 9 are good.
 *
 * Constraints:
 *     2 <= n <= 105
 *     edges.length == n - 1
 *     edges[i].length == 2
 *     0 <= ai, bi < n
 *     The input is generated such that edges represents a valid tree.
 */
/**
 * Get rid of the visited array.
 */
auto build_tree(vector<vector<int>> const& edges)
{
    auto const n = edges.size() + 1;
    auto tree = vector<vector<int>>(n);

    for (auto const& e : edges) {
        auto [u, v] = tuple(e[0], e[1]);
        tree[u].push_back(v);
        tree[v].push_back(u);
    }

    return tree;
}

auto dfs(int node, int parent, vector<vector<int>> const& tree) -> pair<int, int>
{
    auto size = 1;
    auto prev_size = -1;
    auto good = 0;

    for (auto const n : tree[node]) {
        if (n != parent) {
            auto [sub_size, sub_good] = dfs(n, node, tree);
            if (-1 == prev_size) {
                prev_size = sub_size;
            }
            else {
                if (sub_size != prev_size) {
                    prev_size = -2;
                }
            }

            good += sub_good;
            size += sub_size;
        }
    }

    if (-2 != prev_size) {
        good += 1;
    }

    return pair{size, good};
}

class Solution {
public:
    int countGoodNodes(vector<vector<int>>& edges) {
        auto const n = edges.size() + 1;
        auto const tree = build_tree(edges);
        auto [size, good_nodes] = dfs(0, -1, tree);

        return good_nodes;
    }
};

/**
 * Original solution. DFS.
 */
auto build_tree(vector<vector<int>> const& edges)
{
    auto const n = edges.size() + 1;
    auto tree = vector<vector<int>>(n);

    for (auto const& e : edges) {
        auto [u, v] = tuple(e[0], e[1]);
        tree[u].push_back(v);
        tree[v].push_back(u);
    }

    return tree;
}

auto dfs(int node, vector<vector<int>> const& tree, vector<bool>& visited) -> pair<int, int>
{
    visited[node] = true;

    auto size = 1;
    auto prev_size = -1;
    auto good = 0;

    for (auto n : tree[node]) {
        if (!visited[n]) {
            auto [sub_size, sub_good] = dfs(n, tree, visited);
            if (-1 == prev_size) {
                prev_size = sub_size;
            }
            else {
                if (sub_size != prev_size) {
                    prev_size = -2;
                }
            }

            good += sub_good;
            size += sub_size;
        }
    }

    if (-2 != prev_size) {
        good += 1;
    }

    return pair{size, good};
}

class Solution {
public:
    int countGoodNodes(vector<vector<int>>& edges) {
        auto const n = edges.size() + 1;
        auto const tree = build_tree(edges);
        auto visited = vector<bool>(n, false);
        auto [size, good_nodes] = dfs(0, tree, visited);

        return good_nodes;
    }
};
