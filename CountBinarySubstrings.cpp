/**
 * Count Binary Substrings
 *
 * Give a string s, count the number of non-empty (contiguous) substrings that have the same number of 0's and 1's,
 * and all the 0's and all the 1's in these substrings are grouped consecutively.
 *
 * Substrings that occur multiple times are counted the number of times they occur.
 *
 * Example 1:
 *
 * Input: "00110011"
 * Output: 6
 * Explanation: There are 6 substrings that have equal number of consecutive 1's and 0's: "0011", "01", "1100", "10",
 * "0011", and "01".
 *
 * Notice that some of these substrings repeat and are counted the number of times they occur.
 *
 * Also, "00110011" is not a valid substring because all the 0's (and 1's) are not grouped together.
 *
 * Example 2:
 *
 * Input: "10101"
 * Output: 4
 * Explanation: There are 4 substrings: "10", "01", "10", "01" that have equal number of consecutive 1's and 0's.
 *
 * Note:
 * s.length will be between 1 and 50,000.
 * s will only consist of "0" or "1" characters.
 */
/**
 * Improved one.
 */
class Solution {
public:
    int countBinarySubstrings(string s) {
        int cur = 1, pre = 0, res = 0;
        for (int i = 1; i < s.size(); i++) {
            if (s[i] == s[i - 1]) cur++;
            else {
                res += min(cur, pre);
                pre = cur;
                cur = 1;
            }
        }
        res += min(cur, pre);
        return res;
    }
};
/**
 * Original solution. Pretty nasty.
 */
class Solution {
public:
    int countBinarySubstrings(string s) {
        int ans = 0;
        int zeros = 0;
        int ones = 0;
        int switches = 0;

        if ('0' == s[0]) {
            ++zeros;
        }
        else {
            ++ones;
        }

        for (int i = 1; i < s.length(); ++i) {
            if (s[i - 1] == s[i]) {
                if ('0' == s[i]) {
                    ++zeros;
                }
                else {
                    ++ones;
                }
            }
            else {
                ++switches;
                if (switches >= 2) {
                    ans += min(zeros, ones);
                    if ('0' == s[i]) {
                        zeros = 0;
                    }
                    else {
                        ones = 0;
                    }
                }
                if ('0' == s[i]) {
                    ++zeros;
                }
                else {
                    ++ones;
                }
            }
        }

        ans += min(zeros, ones);
        return ans;
    }
};
