/**
 * Shortest Subarray With OR at Least K II
 *
 * You are given an array nums of non-negative integers and an integer k.
 *
 * An array is called special if the bitwise OR of all of its elements is at least k.
 *
 * Return the length of the shortest special non-empty
 * subarray
 * of nums, or return -1 if no special subarray exists.
 *
 * Example 1:
 *
 * Input: nums = [1,2,3], k = 2
 *
 * Output: 1
 *
 * Explanation:
 *
 * The subarray [3] has OR value of 3. Hence, we return 1.
 *
 * Example 2:
 *
 * Input: nums = [2,1,8], k = 10
 *
 * Output: 3
 *
 * Explanation:
 *
 * The subarray [2,1,8] has OR value of 11. Hence, we return 3.
 *
 * Example 3:
 *
 * Input: nums = [1,2], k = 0
 *
 * Output: 1
 *
 * Explanation:
 *
 * The subarray [1] has OR value of 1. Hence, we return 1.
 *
 * Constraints:
 *     1 <= nums.length <= 2 * 105
 *     0 <= nums[i] <= 109
 *     0 <= k <= 109
 */
/**
 * Original solution.
 */
auto update_bit_cnt(vector<int>& cnt_bit, int num, int val)
{
    constexpr auto bits_int = sizeof(int) * 8;
    auto ored = 0;

    for (auto b = 0; b < bits_int; ++b) {
        if (num & (1 << b)) {
            cnt_bit[b] += val;
        }

        if (cnt_bit[b] > 0) {
            ored = ored | (1 << b);
        }
    }

    return ored;
}

class Solution {
public:
    int minimumSubarrayLength(vector<int>& nums, int k) {
        constexpr auto int_max = numeric_limits<int>::max();
        constexpr auto bits_int = sizeof(int) * 8;
        auto ans = int_max;
        auto cnt_bit = vector<int>(bits_int, 0);
        int const n = nums.size();

        for (auto [i, j, ored] = tuple(0, 0, 0); i < n; ++i) {
            ored = update_bit_cnt(cnt_bit, nums[i], 1);

            for (; ored >= k && j <= i; ++j) {
                ans = min(ans, i - j + 1);
                ored = update_bit_cnt(cnt_bit, nums[j], -1);
            }
        }

        return int_max == ans ? -1 : ans;
    }
};
