/**
 * Number of Pairs of Interchangeable Rectangles
 *
 * You are given n rectangles represented by a 0-indexed 2D integer array rectangles, where
 * rectangles[i] = [widthi, heighti] denotes the width and height of the ith rectangle.
 *
 * Two rectangles i and j (i < j) are considered interchangeable if they have the same
 * width-to-height ratio. More formally, two rectangles are interchangeable if widthi/heighti ==
 * widthj/heightj (using decimal division, not integer division).
 *
 * Return the number of pairs of interchangeable rectangles in rectangles.
 *
 * Example 1:
 *
 * Input: rectangles = [[4,8],[3,6],[10,20],[15,30]]
 * Output: 6
 * Explanation: The following are the interchangeable pairs of rectangles by index (0-indexed):
 * - Rectangle 0 with rectangle 1: 4/8 == 3/6.
 * - Rectangle 0 with rectangle 2: 4/8 == 10/20.
 * - Rectangle 0 with rectangle 3: 4/8 == 15/30.
 * - Rectangle 1 with rectangle 2: 3/6 == 10/20.
 * - Rectangle 1 with rectangle 3: 3/6 == 15/30.
 * - Rectangle 2 with rectangle 3: 10/20 == 15/30.
 * Example 2:
 *
 * Input: rectangles = [[4,5],[7,8]]
 * Output: 0
 * Explanation: There are no interchangeable pairs of rectangles.
 *
 * Constraints:
 *
 * n == rectangles.length
 * 1 <= n <= 105
 * rectangles[i].length == 2
 * 1 <= widthi, heighti <= 105
 */
/**
 * Original solution.
 */
class Solution {
public:
    long long interchangeableRectangles(vector<vector<int>>& rectangles) {
        unordered_map<int64_t, int64_t> ratio_cnt;
        long long ans = 0;

        for (auto const& rect : rectangles) {
            auto width = rect[0];
            auto height = rect[1];
            int64_t gc_divisor = gcd(width, height);
            int64_t ratio = ((width / gc_divisor) << 32) | (height / gc_divisor);
            ++ratio_cnt[ratio];
        }

        for (auto const& rc : ratio_cnt) {
            ans += rc.second * (rc.second - 1) / 2;
        }

        return ans;
    }
};
