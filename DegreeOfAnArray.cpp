/** Degree of an Array
 *
 * Given a non-empty array of non-negative integers nums, the degree of this array is defined as the maximum frequency
 * of any one of its elements.
 *
 * Your task is to find the smallest possible length of a (contiguous) subarray of nums, that has the same degree as
 * nums.
 *
 * Example 1:
 *
 * Input: [1, 2, 2, 3, 1] Output: 2 Explanation: The input array has a degree of 2 because both elements 1 and 2 appear
 * twice.  Of the subarrays that have the same degree: [1, 2, 2, 3, 1], [1, 2, 2, 3], [2, 2, 3, 1], [1, 2, 2], [2, 2,
 * 3], [2, 2] The shortest length is 2. So return 2.
 *
 * Example 2:
 *
 * Input: [1,2,2,3,1,4,2] Output: 6
 *
 * Note: nums.length will be between 1 and 50,000.  nums[i] will be an integer between 0 and 49,999.
 */
/**
 * More efficient one.
 */
class Solution {
public:
    int findShortestSubArray(vector<int>& nums) {
        unordered_map<int, vector<int>> valueIndexMap;

        for (int i = 0; i < nums.size(); ++i) {
            valueIndexMap[nums[i]].push_back(i);
        }

        int maxFreq = 0;
        int ans = nums.size();
        int count = nums.size();
        for (auto& p : valueIndexMap) {
            if (p.second.size() > maxFreq) {
                maxFreq = p.second.size();
                ans = min(count, p.second[maxFreq - 1] - p.second[0] + 1);
            }
            if (p.second.size() == maxFreq) {
                ans = min(ans, p.second[maxFreq - 1] - p.second[0] + 1);
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int findShortestSubArray(vector<int>& nums) {
        unordered_map<int, int> freq;
        for (auto& n : nums) ++freq[n];

        int maxFreq = 0;
        for (auto& p : freq) {
            if (p.second > maxFreq) maxFreq = p.second;
        }

        vector<int> sameFreqNums;
        for (auto& p : freq) {
            if (p.second == maxFreq) sameFreqNums.push_back(p.first);
        }

        int ans = numeric_limits<int>::max();
        for (auto& n : sameFreqNums) {
            auto start = find(nums.begin(), nums.end(), n);
            auto end = find(nums.rbegin(), nums.rend(), n);
            int len = distance(start, nums.end()) - distance(nums.rbegin(), end);
            ans = min(ans, len);
        }

        return ans;
    }
};
