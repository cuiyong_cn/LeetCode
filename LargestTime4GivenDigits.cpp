/**
 * Largest Time for Given Digits
 *
 * Given an array arr of 4 digits, find the latest 24-hour time that can be made using each digit
 * exactly once.
 *
 * 24-hour times are formatted as "HH:MM", where HH is between 00 and 23, and MM is between 00 and
 * 59. The earliest 24-hour time is 00:00, and the latest is 23:59.
 *
 * Return the latest 24-hour time in "HH:MM" format.  If no valid time can be made, return an empty
 * string.
 *
 * Example 1:
 *
 * Input: arr = [1,2,3,4]
 * Output: "23:41"
 * Explanation: The valid 24-hour times are "12:34", "12:43", "13:24", "13:42", "14:23", "14:32",
 * "21:34", "21:43", "23:14", and "23:41". Of these times, "23:41" is the latest.
 * Example 2:
 *
 * Input: arr = [5,5,5,5]
 * Output: ""
 * Explanation: There are no valid 24-hour times as "55:55" is not valid.
 * Example 3:
 *
 * Input: arr = [0,0,0,0]
 * Output: "00:00"
 * Example 4:
 *
 * Input: arr = [0,0,1,0]
 * Output: "10:00"
 *
 * Constraints:
 *
 * arr.length == 4
 * 0 <= arr[i] <= 9
 */
/**
 * Next_permutation solution
 */
class Solution {
public:
    string largestTimeFromDigits(vector<int>& A) {

        int max_time = -1;
        // prepare for the generation of permutations next.
        std::sort(A.begin(), A.end());

        do {
            int hour = A[0] * 10 + A[1];
            int minute = A[2] * 10 + A[3];

            if (hour < 24 && minute < 60) {
                int new_time = hour * 60 + minute;
                max_time = new_time > max_time ? new_time : max_time;
            }
        } while(next_permutation(A.begin(), A.end()));

        if (max_time == -1) {
            return "";
        }

        std::ostringstream strstream;
        strstream << std::setw(2) << std::setfill('0') << max_time / 60
           << ":" << std::setw(2) << std::setfill('0') << max_time % 60;
        return strstream.str();
    }
};

/**
 * Original solution. Backtracking which is long ass solution. This is not an elegant solution.
 */
class Solution {
public:
    string largestTimeFromDigits(vector<int>& arr) {
        array<int, 10> digit_cnt = { 0, };
        for (auto d : arr) {
            ++digit_cnt[d];
        }

        string ans;
        return h1(digit_cnt, ans) ? ans : "";
    }

private:
    bool h1(array<int, 10>& digit_cnt, string& ans)
    {
        for (int h = 2; h >= 0; --h) {
            if (digit_cnt[h] > 0) {
                ans.push_back('0' + h);
                --digit_cnt[h];
                if (h2(digit_cnt, ans)) {
                    return true;
                }
                ++digit_cnt[h];
                ans.pop_back();
            }
        }

        return 5 == ans.length();
    }

    bool h2(array<int, 10>& digit_cnt, string& ans)
    {
        for (int h = '2' == ans[0] ? 3 : 9; h >= 0; --h) {
            if (digit_cnt[h] > 0) {
                ans.push_back('0' + h);
                --digit_cnt[h];
                if (m1(digit_cnt, ans)) {
                    return true;
                }
                ++digit_cnt[h];
                ans.pop_back();
            }
        }

        return 5 == ans.length();
    }

    bool m1(array<int, 10>& digit_cnt, string& ans)
    {
        ans += ':';
        for (int m = 5; m >= 0; --m) {
            if (digit_cnt[m] > 0) {
                ans.push_back('0' + m);
                --digit_cnt[m];
                if (m2(digit_cnt, ans)) {
                    return true;
                }
                ++digit_cnt[m];
                ans.pop_back();
            }
        }

        if (5 != ans.length()) {
            ans.pop_back();
            return false;
        }

        return true;
    }

    bool m2(array<int, 10>& digit_cnt, string& ans)
    {
        for (int m = 9; m >= 0; --m) {
            if (digit_cnt[m] > 0) {
                ans += '0' + m;
                --digit_cnt[m];
                return true;
            }
        }

        return false;
    }
};
