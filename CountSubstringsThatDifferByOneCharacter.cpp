/**
 * Count Substrings That Differ by One Character
 *
 * Given two strings s and t, find the number of ways you can choose a non-empty substring of s and
 * replace a single character by a different character such that the resulting substring is a
 * substring of t. In other words, find the number of substrings in s that differ from some
 * substring in t by exactly one character.
 *
 * For example, the underlined substrings in "computer" and "computation" only differ by the
 * 'e'/'a', so this is a valid way.
 *
 * Return the number of substrings that satisfy the condition above.
 *
 * A substring is a contiguous sequence of characters within a string.
 *
 * Example 1:
 *
 * Input: s = "aba", t = "baba"
 * Output: 6
 * Explanation: The following are the pairs of substrings from s and t that differ by exactly 1 character:
 * ("aba", "baba")
 * ("aba", "baba")
 * ("aba", "baba")
 * ("aba", "baba")
 * ("aba", "baba")
 * ("aba", "baba")
 * The underlined portions are the substrings that are chosen from s and t.
 * Example 2:
 * Input: s = "ab", t = "bb"
 * Output: 3
 * Explanation: The following are the pairs of substrings from s and t that differ by 1 character:
 * ("ab", "bb")
 * ("ab", "bb")
 * ("ab", "bb")
 * The underlined portions are the substrings that are chosen from s and t.
 *
 * Constraints:
 *
 * 1 <= s.length, t.length <= 100
 * s and t consist of lowercase English letters only.
 */
/**
 * Sliding window.
 */
class Solution {
public:
    int countSubstrings(string const& s, string const& t) {
        int res = 0 ;
        for (int i = 0; i < s.length(); ++i)
            res += helper(s, t, i, 0);
        for (int j = 1; j < t.length(); ++j)
            res += helper(s, t, 0, j);
        return res;
    }

    int helper(string const& s, string const& t, int i, int j) {
        int res = 0, pre = 0, cur = 0;
        for (int n = s.length(), m = t.length(); i < n && j < m; ++i, ++j) {
            cur++;
            if (s[i] != t[j])
                pre = cur, cur = 0;
            res += pre;
        }
        return res;
    }
};

/**
 * Precompute the mismatches.
 */
class Solution {
public:
    int countSubstrings(string &s, string &t) {
        int res = 0;
        int dpl[101][101] = {}, dpr[101][101] = {};
        for (int i = 1; i <= s.size(); ++i)
            for (int j = 1; j <= t.size(); ++j)
                if (s[i - 1] == t[j - 1])
                    dpl[i][j] = 1 + dpl[i - 1][j - 1];
        for (int i = s.size(); i > 0; --i)
            for (int j = t.size(); j > 0; --j)
                if (s[i - 1] == t[j - 1])
                    dpr[i - 1][j - 1] = 1 + dpr[i][j];
        for (int i = 0; i < s.size(); ++i)
            for (int j = 0; j < t.size(); ++j)
                if (s[i] != t[j])
                    res += (dpl[i][j] + 1) * (dpr[i + 1][j + 1] + 1);
        return res;
    }
};

/**
 * Same as below one.
 */
class Solution {
public:
    int countSubstrings(string &s, string &t) {
        int res = 0;
        for (int i = 0; i < s.size(); ++i)
            for (int j = 0; j < t.size(); ++j) {
                if (s[i] != t[j]) {
                    int l = 1, r = 1;
                    while (min(i - l, j - l) >= 0 && s[i - l] == t[j - l])
                        ++l;
                    while (i + r < s.size() && j + r < t.size() && s[i + r] == t[j + r])
                        ++r;
                    res += l * r;
                }
            }
        return res;
    }
};

/**
 * Count mismatched char
 */
class Solution {
public:
    int countSubstrings(string &s, string &t) {
        int res = 0;
        for (int i = 0; i < s.size(); ++i) {
            for (int j = 0; j < t.size(); ++j) {
                int miss = 0;
                for (int pos = 0; i + pos < s.size() && j + pos < t.size(); ++pos) {
                    if (s[i + pos] != t[j + pos] && ++miss > 1)
                        break;
                    res += miss;
                }
            }
        }
        return res;
    }
};

/**
 * string_view is not suitable to be used as a key for how == operator implemented.
 */
class Solution {
public:
    int countSubstrings(string s, string t) {
        unordered_map<string, int> str_cnt;
        int const len_t = t.length();
        for (int L = 1; L <= len_t; ++L) {
            int const idx = len_t - L;
            for (int i = 0; i <= idx; ++i) {
                ++str_cnt[t.substr(i, L)];
            }
        }

        unordered_map<string, int> sub_cnt;
        int const len_s = s.length();
        for (int L = 1; L <= len_s; ++L) {
            int const idx = len_s - L;
            for (int i = 0; i <= idx; ++i) {
                ++sub_cnt[s.substr(i, L)];
            }
        }

        int ans = 0;
        for (auto const& sc : sub_cnt) {
            //cout << sc.first << ", " << sc.second << "\n";
            string str = sc.first;
            for (auto& c : str) {
                char const oc = c;
                for (char r = 'a'; r <= 'z'; ++r) {
                    if (oc != r) {
                        c = r;
                        if (str_cnt.count(str)) {
                            ans += sc.second * str_cnt[str];
                        }
                    }
                }
                c = oc;
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int countSubstrings(string s, string t) {
        unordered_map<string_view, int> str_cnt;
        int const len_t = t.length();
        for (int L = 1; L <= len_t; ++L) {
            int const idx = len_t - L;
            for (int i = 0; i <= idx; ++i) {
                ++str_cnt[string_view(t.c_str() + i, L)];
            }
        }

        unordered_map<string, int> sub_cnt;
        int const len_s = s.length();
        for (int L = 1; L <= len_s; ++L) {
            int const idx = len_s - L;
            for (int i = 0; i <= idx; ++i) {
                ++sub_cnt[s.substr(i, L)];
            }
        }

        int ans = 0;
        for (auto const& sc : sub_cnt) {
            string str = sc.first;
            for (auto& c : str) {
                char const oc = c;
                for (char r = 'a'; r <= 'z'; ++r) {
                    if (oc != r) {
                        c = r;
                        if (str_cnt.count(str)) {
                            ans += sc.second * str_cnt[str];
                        }
                    }
                }
                c = oc;
            }
        }

        return ans;
    }
};
