/**
 * Bulls and Cows
 *
 * You are playing the Bulls and Cows game with your friend.
 *
 * You write down a secret number and ask your friend to guess what the number is. When your friend makes a guess, you
 * provide a hint with the following info:
 *
 * The number of "bulls", which are digits in the guess that are in the correct position.  The number of "cows", which
 * are digits in the guess that are in your secret number but are located in the wrong position. Specifically, the
 * non-bull digits in the guess that could be rearranged such that they become bulls.  Given the secret number secret
 * and your friend's guess guess, return the hint for your friend's guess.
 *
 * The hint should be formatted as "xAyB", where x is the number of bulls and y is the number of cows. Note that both
 * secret and guess may contain duplicate digits.
 *
 * Example 1:
 *
 * Input: secret = "1807", guess = "7810"
 * Output: "1A3B"
 * Explanation: Bulls are connected with a '|' and cows are underlined:
 * "1807"
 *   |
 * "7810"
 * Example 2:
 *
 * Input: secret = "1123", guess = "0111"
 * Output: "1A1B"
 * Explanation: Bulls are connected with a '|' and cows are underlined:
 * "1123"        "1123"
 *   |      or     |
 * "0111"        "0111"
 * Note that only one of the two unmatched 1s is counted as a cow since the non-bull digits can only be rearranged to
 * allow one 1 to be a bull.
 * Example 3:
 *
 * Input: secret = "1", guess = "0"
 * Output: "0A0B"
 * Example 4:
 *
 * Input: secret = "1", guess = "1"
 * Output: "1A0B"
 *
 * Constraints:
 *
 * 1 <= secret.length, guess.length <= 1000
 * secret.length == guess.length
 * secret and guess consist of digits only.
 */
/**
 * One pass, and more space efficient
 */
class Solution {
public:
    string getHint(string secret, string guess) {
        int bulls = 0, cows = 0;
        int digit_count[10] = { 0, };

        for (int i = 0; i < guess.length(); ++i) {
            if (guess[i] == secret[i]) {
                ++bulls;
            }
            else {
                if (digit_count[guess[i] - '0']-- > 0) {
                    ++cows;
                }
                if (digit_count[secret[i] - '0']++ < 0) {
                    ++cows;
                }
            }
        }

        return std::to_string(bulls) + "A" + std::to_string(cows) + "B";
    }
};

/**
 * Improve the below code.
 */
class Solution {
public:
    string getHint(string secret, string guess) {
        int bulls = 0, cows = 0;
        int gdigit_count[10] = { 0, };
        int sdigit_count[10] = { 0, };

        for (int i = 0; i < guess.length(); ++i) {
            if (guess[i] == secret[i]) {
                ++bulls;
            }
            else {
                ++gdigit_count[guess[i] - '0'];
                ++sdigit_count[secret[i] - '0'];
            }
        }

        for (int i = 0; i < 10; ++i) {
            cows += std::min(sdigit_count[i], gdigit_count[i]);
        }

        return std::to_string(bulls) + "A" + std::to_string(cows) + "B";
    }
};

/**
 * Original solution. One pass count solution.
 */
class Solution {
public:
    string getHint(string secret, string guess) {
        int bulls = 0, cows = 0;
        int gdigit_count[10] = { 0, };
        int sdigit_count[10] = { 0, };

        for (int i = 0; i < guess.length(); ++i) {
            if (guess[i] == secret[i]) {
                ++bulls;
            }
            else {
                int gdigit = guess[i] - '0';
                if (sdigit_count[gdigit]) {
                    ++cows;
                    --sdigit_count[gdigit];
                }
                else {
                    ++gdigit_count[gdigit];
                }
                int sdigit = secret[i] - '0';
                if (gdigit_count[sdigit]) {
                    ++cows;
                    --gdigit_count[sdigit];
                }
                else {
                    ++sdigit_count[sdigit];
                }
            }
        }

        return std::to_string(bulls) + "A" + std::to_string(cows) + "B";
    }
};
