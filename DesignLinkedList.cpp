/**
 * Design Linked List
 *
 * Design your implementation of the linked list. You can choose to use a singly or doubly linked
 * list.  A node in a singly linked list should have two attributes: val and next. val is the value
 * of the current node, and next is a pointer/reference to the next node.  If you want to use the
 * doubly linked list, you will need one more attribute prev to indicate the previous node in the
 * linked list. Assume all nodes in the linked list are 0-indexed.
 *
 * Implement the MyLinkedList class:
 *
 * MyLinkedList() Initializes the MyLinkedList object.  int get(int index) Get the value of the
 * indexth node in the linked list. If the index is invalid, return -1.  void addAtHead(int val) Add
 * a node of value val before the first element of the linked list. After the insertion, the new
 * node will be the first node of the linked list.  void addAtTail(int val) Append a node of value
 * val as the last element of the linked list.  void addAtIndex(int index, int val) Add a node of
 * value val before the indexth node in the linked list. If index equals the length of the linked
 * list, the node will be appended to the end of the linked list. If index is greater than the
 * length, the node will not be inserted.  void deleteAtIndex(int index) Delete the indexth node in
 * the linked list, if the index is valid.
 *
 * Example 1:
 *
 * Input
 * ["MyLinkedList", "addAtHead", "addAtTail", "addAtIndex", "get", "deleteAtIndex", "get"]
 * [[], [1], [3], [1, 2], [1], [1], [1]]
 * Output
 * [null, null, null, null, 2, null, 3]
 *
 * Explanation
 * MyLinkedList myLinkedList = new MyLinkedList();
 * myLinkedList.addAtHead(1);
 * myLinkedList.addAtTail(3);
 * myLinkedList.addAtIndex(1, 2);    // linked list becomes 1->2->3
 * myLinkedList.get(1);              // return 2
 * myLinkedList.deleteAtIndex(1);    // now the linked list is 1->3
 * myLinkedList.get(1);              // return 3
 *
 * Constraints:
 *
 * 0 <= index, val <= 1000
 * Please do not use the built-in LinkedList library.
 * At most 2000 calls will be made to get, addAtHead, addAtTail, addAtIndex and deleteAtIndex.
 */
/**
 * Original solution.
 */
class MyLinkedList {
    struct Node {
        int val;
        Node* next;
        Node(int val, Node* next) : val{val}, next{next} {}
    };

    Node* head;
    Node* tail;
    int len;

public:
    /** Initialize your data structure here. */
    MyLinkedList() : head{nullptr}, tail{nullptr}, len{0} {

    }

    /** Get the value of the index-th node in the linked list. If the index is invalid, return -1. */
    int get(int index) {
        if (index >= len || index < 0) {
            return -1;
        }

        auto cur = head;
        while (index > 0) {
            --index;
            cur = cur->next;
        }

        return cur->val;
    }

    /** Add a node of value val before the first element of the linked list. After the insertion, the new node will be the first node of the linked list. */
    void addAtHead(int val) {
        auto new_head = new Node(val, head);
        if (0 == len) {
            tail = new_head;
        }

        head = new_head;
        ++len;
    }

    /** Append a node of value val to the last element of the linked list. */
    void addAtTail(int val) {
        auto new_tail = new Node(val, nullptr);
        if (0 == len) {
            head = new_tail;
        }
        else {
            tail->next = new_tail;
        }

        tail = new_tail;
        ++len;
    }

    /** Add a node of value val before the index-th node in the linked list. If index equals to the length of linked list, the node will be appended to the end of linked list. If index is greater than the length, the node will not be inserted. */
    void addAtIndex(int index, int val) {
        if (index > len || index < 0) {
            return;
        }

        if (0 == index) {
            return addAtHead(val);
        }

        if (index == len) {
            return addAtTail(val);
        }

        Node dummy(0, head);
        auto cur = &dummy;
        while (index > 0) {
            --index;
            cur = cur->next;
        }

        auto new_node = new Node(val, cur->next);
        cur->next = new_node;
        ++len;
    }

    /** Delete the index-th node in the linked list, if the index is valid. */
    void deleteAtIndex(int index) {
        if (index >= len || index < 0) {
            return;
        }

        Node dummy(0, head);
        auto cur = &dummy;
        auto idx = index;
        while (idx > 0) {
            --idx;
            cur = cur->next;
        }

        auto node_to_del = cur->next;
        cur->next = cur->next->next;
        if (0 == index) {
            head = head->next;
        }

        if (index == (len - 1)) {
            tail = len > 1 ? cur : nullptr;
        }

        delete node_to_del;
        --len;
    }
};

/**
 * Your MyLinkedList object will be instantiated and called as such:
 * MyLinkedList* obj = new MyLinkedList();
 * int param_1 = obj->get(index);
 * obj->addAtHead(val);
 * obj->addAtTail(val);
 * obj->addAtIndex(index,val);
 * obj->deleteAtIndex(index);
 */
