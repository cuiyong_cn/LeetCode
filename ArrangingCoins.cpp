/**
 * Arranging Coins
 *
 * You have a total of n coins that you want to form in a staircase shape, where every k-th row must
 * have exactly k coins.
 *
 * Given n, find the total number of full staircase rows that can be formed.
 * n is a non-negative integer and fits within the range of a 32-bit signed integer.
 *
 * Example 1:
 *
 * n = 5
 *
 * The coins can form the following rows:
 * ¤
 * ¤ ¤
 * ¤ ¤
 *
 * Because the 3rd row is incomplete, we return 2.
 * Example 2:
 *
 * n = 8
 *
 * The coins can form the following rows:
 * ¤
 * ¤ ¤
 * ¤ ¤ ¤
 * ¤ ¤
 *
 * Because the 4th row is incomplete, we return 3.
 */
/**
 * Using math formula
 */
class Solution {
public:
    int arrangeCoins(int n) {
        return std::sqrt(2L * n + 0.25) - 0.5;
    }
};

/**
 * Binary search. We use long long because when mid * (mid + 1) may
 * cause signed integer overflow.
 */
class Solution {
public:
    int arrangeCoins(int n) {
        long long left = 0, right = n;
        
        while (left <= right) {
            auto mid = (left + right) >> 1;
            auto total = (mid * (mid + 1)) >> 1;
            
            if (n == total) {
                return mid;
            }

            if (n > total) {
                left = mid + 1;
            }
            else {
                right = mid - 1;
            }
        }

        return right;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int arrangeCoins(int n) {
        int ans = 0;
        for (int i = 1; n >= i; ++i) {
            ++ans;
            n -= i;
        }

        return ans;
    }
};
