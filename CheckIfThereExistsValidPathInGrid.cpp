/**
 * Check if There is a Valid Path in a Grid
 *
 * Given a m x n grid. Each cell of the grid represents a street. The street of grid[i][j] can be:
 * 1 which means a street connecting the left cell and the right cell.
 * 2 which means a street connecting the upper cell and the lower cell.
 * 3 which means a street connecting the left cell and the lower cell.
 * 4 which means a street connecting the right cell and the lower cell.
 * 5 which means a street connecting the left cell and the upper cell.
 * 6 which means a street connecting the right cell and the upper cell.
 *
 * You will initially start at the street of the upper-left cell (0,0). A valid path in the grid is a path which starts
 * from the upper left cell (0,0) and ends at the bottom-right cell (m - 1, n - 1). The path should only follow the
 * streets.
 *
 * Notice that you are not allowed to change any street.
 *
 * Return true if there is a valid path in the grid or false otherwise.
 *
 * Example 1:
 *
 * Input: grid = [[2,4,3],[6,5,2]]
 * Output: true
 * Explanation: As shown you can start at cell (0, 0) and visit all the cells of the grid to reach (m - 1, n - 1).
 * Example 2:
 *
 * Input: grid = [[1,2,1],[1,2,1]]
 * Output: false
 * Explanation: As shown you the street at cell (0, 0) is not connected with any street of any other cell and you will get stuck at cell (0, 0)
 * Example 3:
 *
 * Input: grid = [[1,1,2]]
 * Output: false
 * Explanation: You will get stuck at cell (0, 1) and you cannot reach cell (0, 2).
 * Example 4:
 *
 * Input: grid = [[1,1,1,1,1,1,3]]
 * Output: true
 * Example 5:
 *
 * Input: grid = [[2],[2],[2],[2],[2],[2],[6]]
 * Output: true
 *
 * Constraints:
 *
 * m == grid.length
 * n == grid[i].length
 * 1 <= m, n <= 300
 * 1 <= grid[i][j] <= 6
 */
/**
 * Really intresting solution using upscaling method.
 */
class Solution {
    bool dfs(vector<vector<bool>> &g, int i, int j) {
        if (min(i, j) < 0 || i >= g.size() || j >= g[i].size() || !g[i][j]) {
            return false;
        }

        if (i == g.size() - 2 && j == g[i].size() - 2) {
            return true;
        }

        g[i][j] = false;

        return dfs(g, i - 1, j) || dfs(g, i + 1, j) || dfs(g, i, j + 1) || dfs(g, i, j - 1);
    }

    bool hasValidPath(vector<vector<int>>& grid) {
        int m = grid.size(), n = grid[0].size();
        vector<vector<bool>> g(m * 3, vector<bool>(n * 3)); // upscale

        for (auto i = 0; i < m; ++i) {
            for (auto j = 0; j < n; ++j) {
                auto r = grid[i][j];
                g[i * 3 + 1][j * 3 + 1] = true;
                g[i * 3 + 1][j * 3 + 0] = r == 1 || r == 3 || r == 5;
                g[i * 3 + 1][j * 3 + 2] = r == 1 || r == 4 || r == 6;
                g[i * 3 + 0][j * 3 + 1] = r == 2 || r == 5 || r == 6;
                g[i * 3 + 2][j * 3 + 1] = r == 2 || r == 3 || r == 4;
            }
        }

        return dfs(g, 1, 1);    
    }
};

/**
 * no dfs used.
 */
class Solution {
public:
    vector<pair<int, int>> dirs{{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
    unordered_map<int, int> turns[7] = {
        {}, 
        {{0, 0}, {1, 1}},
        {{2, 2}, {3, 3}},
        {{0, 2}, {3, 1}}, 
        {{3, 0}, {1, 2}},
        {{0, 3}, {2, 1}},
        {{2, 0}, {1, 3}}
    };

    bool trace(vector<vector<int>> &g, int dir) {
        int i = 0, j = 0, m = g.size(), n = g[0].size();

        while(min(i, j) >= 0 && i < m && j < n) {
            auto road = g[i][j];
            if (turns[road].count(dir) == 0) {
                return false;
            }

            if (i == m - 1 && j == n - 1) {
                return true;
            }

            i += dirs[dir].first, j += dirs[dir].second;
            if (i == 0 && j == 0) {
                return false;
            }
            dir = turns[road][dir];
        }

        return false;        
    }

    bool hasValidPath(vector<vector<int>>& g) {
        return trace(g, 0) || trace(g, 1) || trace(g, 2) || trace(g, 3);
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    enum class Direction {
        north,
        east,
        south,
        west
    };

    bool hasValidPath(vector<vector<int>>& grid) {
        bool valid = false;
        auto street_type = grid[0][0];
        grid[0][0] = -street_type;

        switch (street_type) {
            case 1:
            case 6:
                valid = dfs(grid, 0, 1, Direction::west);
                break;

            case 2:
            case 3:
                valid = dfs(grid, 1, 0, Direction::north);
                break;

            case 4:
                valid = dfs(grid, 0, 1, Direction::west) || dfs(grid, 1, 0, Direction::north);
                break;

            case 5:
            default:
                break;
        }
        grid[0][0] = street_type;

        if (!valid) {
            valid = (0 == (grid.size() - 1) && 0 == (grid[0].size() - 1));
        }

        return valid;
    }

private:
    bool dfs(vector<vector<int>>& grid, int r, int c, Direction from)
    {
        if (r < 0 || c < 0 || r >= grid.size() || c >= grid[0].size()) {
            return false;
        }

        if (grid[r][c] < 0) {
            return false;
        }

        auto street_type = grid[r][c];
        auto valid_path = false;

        if (can_reach_current_street(from, street_type)) {
            grid[r][c] = -street_type;
            switch (head_for(from, street_type)) {
                case Direction::north:
                    valid_path = dfs(grid, r - 1, c, Direction::south);
                    break;
                case Direction::south:
                    valid_path = dfs(grid, r + 1, c, Direction::north);
                    break;
                case Direction::west:
                    valid_path = dfs(grid, r, c - 1, Direction::east);
                    break;
                case Direction::east:
                    valid_path = dfs(grid, r, c + 1, Direction::west);
                    break;
                default:
                    break;
            }

            grid[r][c] = street_type;

            if (!valid_path) {
                valid_path = (r == (grid.size() - 1) && c == (grid[0].size() - 1));
            }
        }

        return valid_path;
    }

    bool can_reach_current_street(Direction from, int street_type)
    {
        auto valid = false;

        switch (from) {
            case Direction::north:
                if (2 == street_type || 5 == street_type || 6 == street_type) {
                    valid = true;
                }
                break;

            case Direction::south:
                if (2 == street_type || 3 == street_type || 4 == street_type) {
                    valid = true;
                }
                break;

            case Direction::west:
                if (1 == street_type || 3 == street_type || 5 == street_type) {
                    valid = true;
                }
                break;

            case Direction::east:
                if (1 == street_type || 4 == street_type || 6 == street_type) {
                    valid = true;
                }
                break;

            default:
                break;
        }

        return valid;
    }

    Direction head_for(Direction from, int street_type)
    {
        Direction next = from;
        if (1 == street_type) {
            next = Direction::west == from ? Direction::east : Direction::west;
        }
        else if (2 == street_type) {
            next = Direction::north == from ? Direction::south : Direction::north;
        }
        else if (3 == street_type) {
            next = Direction::west == from ? Direction::south : Direction::west;
        }
        else if (4 == street_type) {
            next = Direction::east == from ? Direction::south : Direction::east;
        }
        else if (5 == street_type) {
            next = Direction::west == from ? Direction::north : Direction::west;
        }
        else if (6 == street_type) {
            next = Direction::east == from ? Direction::north : Direction::east;
        }

        return next;
    }
};
