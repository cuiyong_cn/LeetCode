/**
 * Can Make Arithmetic Progression From Sequence
 *
 * A sequence of numbers is called an arithmetic progression if the difference between any two
 * consecutive elements is the same.
 *
 * Given an array of numbers arr, return true if the array can be rearranged to form an arithmetic
 * progression. Otherwise, return false.
 *
 * Example 1:
 *
 * Input: arr = [3,5,1]
 * Output: true
 * Explanation: We can reorder the elements as [1,3,5] or [5,3,1] with differences 2 and -2 respectively, between each consecutive elements.
 * Example 2:
 *
 * Input: arr = [1,2,4]
 * Output: false
 * Explanation: There is no way to reorder the elements to obtain an arithmetic progression.
 *
 * Constraints:
 *
 * 2 <= arr.length <= 1000
 * -106 <= arr[i] <= 106
 */
/**
 * Another solution.
 */
class Solution {
public:
    bool canMakeArithmeticProgression(vector<int>& arr) {
        auto mm = std::minmax_element(arr.begin(), arr.end());
        int const N = arr.size();
        int const diff = *mm.second - *mm.first;
        if (0 == diff) {
            return true;
        }
        if (diff % (N - 1)) {
            return false;
        }

        unordered_set<int> unique{arr.begin(), arr.end()};
        if (N != unique.size()) {
            return false;
        }

        int const step =  diff / (N - 1);
        return all_of(arr.begin(), arr.end(),
                [min = *mm.first, step](auto const& n) {
                    return 0 == ((n - min) % step);
                });
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool canMakeArithmeticProgression(vector<int>& arr) {
        sort(arr.begin(), arr.end());
        auto const diff = arr[1] - arr[0];
        int const N = arr.size();
        for (int i = 2; i < N; ++i) {
            if (diff != (arr[i] - arr[i - 1])) {
                return false;
            }
        }

        return true;
    }
};
