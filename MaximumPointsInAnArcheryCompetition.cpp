/**
 * Maximum Points in an Archery Competition
 *
 * Alice and Bob are opponents in an archery competition. The competition has set the following rules:
 *
 * Alice first shoots numArrows arrows and then Bob shoots numArrows arrows.
 * The points are then calculated as follows:
 * The target has integer scoring sections ranging from 0 to 11 inclusive.
 * For each section of the target with score k (in between 0 to 11), say Alice and Bob have shot ak
 * and bk arrows on that section respectively. If ak >= bk, then Alice takes k points. If ak < bk,
 * then Bob takes k points.
 * However, if ak == bk == 0, then nobody takes k points.
 * For example, if Alice and Bob both shot 2 arrows on the section with score 11, then Alice takes
 * 11 points. On the other hand, if Alice shot 0 arrows on the section with score 11 and Bob shot 2
 * arrows on that same section, then Bob takes 11 points.
 *
 * You are given the integer numArrows and an integer array aliceArrows of size 12, which represents
 * the number of arrows Alice shot on each scoring section from 0 to 11. Now, Bob wants to maximize
 * the total number of points he can obtain.
 *
 * Return the array bobArrows which represents the number of arrows Bob shot on each scoring section
 * from 0 to 11. The sum of the values in bobArrows should equal numArrows.
 *
 * If there are multiple ways for Bob to earn the maximum total points, return any one of them.
 *
 * Example 1:
 *
 * Input: numArrows = 9, aliceArrows = [1,1,0,1,0,0,2,1,0,1,2,0]
 * Output: [0,0,0,0,1,1,0,0,1,2,3,1]
 * Explanation: The table above shows how the competition is scored.
 * Bob earns a total point of 4 + 5 + 8 + 9 + 10 + 11 = 47.
 * It can be shown that Bob cannot obtain a score higher than 47 points.
 * Example 2:
 *
 * Input: numArrows = 3, aliceArrows = [0,0,1,0,0,0,0,0,0,0,0,2]
 * Output: [0,0,0,0,0,0,0,0,1,1,1,0]
 * Explanation: The table above shows how the competition is scored.
 * Bob earns a total point of 8 + 9 + 10 = 27.
 * It can be shown that Bob cannot obtain a score higher than 27 points.
 *
 * Constraints:
 *
 * 1 <= numArrows <= 105
 * aliceArrows.length == bobArrows.length == 12
 * 0 <= aliceArrows[i], bobArrows[i] <= numArrows
 * sum(aliceArrows[i]) == numArrows
 */
/**
 * From discussion. DP solution.
 */
class Solution {
public:
    int memo[12][100001];
    int dp(int k, int numArrows, vector<int>& aliceArrows) {
        if (k == 12 || numArrows <= 0)
            return 0;

        if (memo[k][numArrows] != -1) return memo[k][numArrows];
        int maxScore = dp(k+1, numArrows, aliceArrows); // Bob loses
        if (numArrows > aliceArrows[k])
            maxScore = max(maxScore, dp(k+1, numArrows-aliceArrows[k]-1, aliceArrows) + k); // Bob wins
        return memo[k][numArrows] = maxScore;
    }

    vector<int> maximumBobPoints(int numArrows, vector<int>& aliceArrows) {
        memset(memo, -1, sizeof(memo));
        vector<int> ans(12, 0);
        int remainBobArrows = numArrows;
        for (int k = 0; k < 12; ++k) {
            if (dp(k, numArrows, aliceArrows) != dp(k+1, numArrows, aliceArrows)) { // if Bob wins
                ans[k] = aliceArrows[k] + 1;
                numArrows -= ans[k];
                remainBobArrows -= ans[k];
            }
        }

        // In case of having remain arrows then it means in all sections Bob always win
        //   then we can distribute the remain to any section, here we simple choose first section.
        ans[0] += remainBobArrows;
        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<int> maximumBobPoints(int numArrows, vector<int>& aliceArrows) {
        dfs(numArrows, aliceArrows, 0, 0, 0);

        auto ans = vector<int>(12, 0);
        for (auto i = 0; i < 12; ++i) {
            if ((bits >> i) & 1) {
                ans[i] = aliceArrows[i] + 1;
            }
        }

        ans[0] += numArrows - accumulate(begin(ans), end(ans), 0);

        return ans;
    }

private:
    void dfs(int arrows_left, vector<int> const& alice_arrows, int k, int points, int pattern)
    {
        if (arrows_left <= 0 || k > 11) {
            if (points > max_points) {
                max_points = points;
                bits = pattern;
            }
            return;
        }

        if (arrows_left > alice_arrows[k]) {
            dfs(arrows_left - alice_arrows[k] - 1, alice_arrows, k + 1, points + k, pattern | (1 << k));
        }
        dfs(arrows_left, alice_arrows, k + 1, points, pattern);
    }

private:
    int max_points = 0;
    int bits = 0;
};
