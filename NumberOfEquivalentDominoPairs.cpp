/**
 * Number of Equivalent Domino Pairs
 *
 * Given a list of dominoes, dominoes[i] = [a, b] is equivalent to dominoes[j] = [c, d] if and only if either (a==c and
 * b==d), or (a==d and b==c) - that is, one domino can be rotated to be equal to another domino.
 *
 * Return the number of pairs (i, j) for which 0 <= i < j < dominoes.length, and dominoes[i] is equivalent to
 * dominoes[j].
 *
 * Example 1:
 *
 * Input: dominoes = [[1,2],[2,1],[3,4],[5,6]]
 * Output: 1
 *
 * Constraints:
 *
 * 1 <= dominoes.length <= 40000
 * 1 <= dominoes[i][j] <= 9
 */
/**
 * From discussion. Note that 1 <= dominoes[i][j] <= 9
 */
class Solution {
public:
    int numEquivDominoPairs(vector<vector<int>>& dominoes) {
        unordered_map<int, int> count;
        int res = 0;
        for (auto& d : dominoes) {
            res += count[min(d[0], d[1]) * 10 + max(d[0], d[1])]++;
        }
        return res;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int numEquivDominoPairs(vector<vector<int>>& dominoes) {
        int ans = 0;
        map<std::pair<int, int>, int> seen;
        for (const auto& dom : dominoes) {
            ans += equivalent_to({dom[0], dom[1]}, seen);
            if (dom[0] != dom[1]) {
                ans += equivalent_to({dom[1], dom[0]}, seen);
            }

            ++seen[{dom[0], dom[1]}];
        }

        return ans;
    }

private:
    int equivalent_to(std::pair<int, int> dom, const map<std::pair<int, int>, int>& seen)
    {
        int count = 0;
        auto it = seen.find(dom);
        if (it != seen.end()) {
            count = it->second;
        }

        return count;
    }
};
