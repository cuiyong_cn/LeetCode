/** Two Sum II - Input array is sorted
 *
Given an array of integers that is already sorted in ascending order, find two numbers such that they add up to a
specific target number.

The function twoSum should return indices of the two numbers such that they add up to the target, where index1 must be
less than index2.

Note:

    Your returned answers (both index1 and index2) are not zero-based.  You may assume that each input would have
    exactly one solution and you may not use the same element twice.

Example:

Input: numbers = [2,7,11,15], target = 9 Output: [1,2] Explanation: The sum of 2 and 7 is 9. Therefore index1 = 1,
index2 = 2.

 */
/** Original solution.
 */
class Solution
{
public:
    vector<int> twoSum(vector<int>& numbers, int target)
    {
        int front = 1;
        int end = numbers.size();

        while (front < end) {
            int sum = numbers[front - 1] + numbers[end - 1];
            if (sum == target) return {front, end};
            if (sum > target) --end;
            else ++front;
        }

        return {};
    }
};

/**
 * Another one, using binary search.
 */
public int[] twoSum(int[] numbers, int target)
{
    int left = 0;
    int right = numbers.length - 1;
    while (left + 1 < right) {
        int cur = numbers[left] + numbers[right];
        if (cur == target) {
            return new int[] {left + 1, right + 1};
        }
        if (cur < target) {
            left = smallestLargerOrEqual(numbers, target - numbers[right], left + 1, right);
        } else {
            right = largestSmallerOrEqual(numbers, target - numbers[left], left, right - 1);
        }
    }
    return new int[] {left + 1, right + 1};
}
private int smallestLargerOrEqual(int[] num, int target, int left, int right)
{
    while (left < right) {
        int mid = left + (right - left) / 2;
        if (num[mid] == target) {
            return mid;
        }
        if (num[mid] > target) {
            right = mid;
        } else {
            left = mid + 1;
        }
    }
    return left;
}
private int largestSmallerOrEqual(int[] num, int target, int left, int right)
{
    while (left < right) {
        int mid = left + (right + 1 - left) / 2;
        if (num[mid] == target) {
            return mid;
        }
        if (num[mid] > target) {
            right = mid - 1;
        } else {
            left = mid;
        }
    }
    return left;
}

