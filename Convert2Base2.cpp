/**
 * Convert to Base -2
 *
 * Given a number N, return a string consisting of "0"s and "1"s that represents its value in base -2 (negative two).
 *
 * The returned string must have no leading zeroes, unless the string is "0".
 *
 * Example 1:
 *
 * Input: 2
 * Output: "110"
 * Explantion: (-2) ^ 2 + (-2) ^ 1 = 2
 *
 * Example 2:
 *
 * Input: 3
 * Output: "111"
 * Explantion: (-2) ^ 2 + (-2) ^ 1 + (-2) ^ 0 = 3
 *
 * Example 3:
 *
 * Input: 4
 * Output: "100"
 * Explantion: (-2) ^ 2 = 4
 *
 * Note:
 *
 *     0 <= N <= 10^9
 */
/**
 * General one, works for any negative base.
 */
class Solution {
public:
    string baseNeg2(int N) {
        if (0 == N) return "0";
        string ans;
        const int negBase2 = -2;
        while (N != 0) {
            int rem = N % negBase2;
            N /= -2;
            if (rem < 0) rem += 2, N += 1;
            ans += rem > 0 ? '1' : '0';
        }
        reverse(ans.begin(), ans.end());
        return ans;
    }
};
/**
 * The simple solution. Similar to base-2. But this is the specialized one.
 * Refs:
 *   1. https://stackoverflow.com/questions/11720656/modulo-operation-with-negative-numbers
 *   2. https://www.geeksforgeeks.org/convert-number-negative-base-representation/
 */
class Solution {
public:
    string baseNeg2(int N) {
        if (0 == N) return "0";
        string ans = "";
        while (N) {
            ans += N & 1 ? '1' : '0';
            N = -(N >> 1);
        }
        reverse(ans.begin(), ans.end());
        return ans;
    }
};

/**
 * My original brute force solution. As u can guess it, it is time limit exceeded.
 */
class Solution {
public:
    string baseNeg2(int N) {
        char bin[32] = { '0', '\0' };
        target = N;
        bitNum.resize(32);
        bitNum[0] = 1;
        for (int i = 1; i < 31; ++i) {
            if (bitNum[i - 1] > 0) {
                bitNum[i] = -(bitNum[i - 1] << 1);
            }
            else {
                bitNum[i] = (-bitNum[i - 1]) << 1;
            }
        }

        dfs(bin, 0, 0);
        string ans(bin);
        reverse(ans.begin(), ans.end());

        return ans;
    }
private:
    bool dfs(char* bin, int i, int num) {
        if (i > 30) return false;
        if (num >= target) {
            if (num > target) {
                return false;
            }
            else {
                return true;
            }
        }
        else {
            bin[i] = '1';
            if (true == dfs(bin, i + 1, num + bitNum[i])) {
                return true;
            }
            bin[i] = '0';
            if (true == dfs(bin, i + 1, num)) {
                return true;
            }
        }
        bin[i] = '\0';
        return false;
    }

    int target;
    vector<int> bitNum;
};
