/**
 * Reorder List
 *
 * You are given the head of a singly linked-list. The list can be represented as:
 *
 * L0 → L1 → … → Ln - 1 → Ln
 * Reorder the list to be on the following form:
 *
 * L0 → Ln → L1 → Ln - 1 → L2 → Ln - 2 → …
 * You may not modify the values in the list's nodes. Only nodes themselves may be changed.
 *
 * Example 1:
 *
 * Input: head = [1,2,3,4]
 * Output: [1,4,2,3]
 * Example 2:
 *
 * Input: head = [1,2,3,4,5]
 * Output: [1,5,2,4,3]
 *
 * Constraints:
 *
 * The number of nodes in the list is in the range [1, 5 * 104].
 * 1 <= Node.val <= 1000
 */
/**
 * Interesting recursive solution.
 */
class Solution {
public:
    int getListLength(ListNode const* head)
    {
        int len = 0;
        while (head) {
            ++len;
            head = head->next;
        }
        return len;
    }

    ListNode* reorderList(ListNode* head, int len){
        if (0 == len) {
            return nullptr;
        }
        if (1 == len) {
            return head;
        }
        if (2 == len) {
            return head->next;
        }

        auto tail = reorderList(head->next, len - 2);
        auto tmp = tail->next;
        tail->next = tail->next->next;
        tmp->next = head->next;
        head->next = tmp;

        return tail;
    }

    void reorderList(ListNode *head)
    {
        reorderList(head, getListLength(head));
    }
};

/**
 * Without extra space.
 */
class Solution {
public:
    void reorderList(ListNode* head) {
        auto slow = head;
        auto fast = head;
        while (fast && fast->next) {
            slow = slow->next;
            fast = fast->next->next;
        }

        auto back = reverse(slow->next);
        slow->next = nullptr;

        auto front = head;
        while (front && back) {
            auto fnext = front->next;
            auto bnext = back->next;

            front->next = back;
            back->next = fnext;

            front = fnext;
            back = bnext;
        }
    }

private:
    ListNode* reverse(ListNode* node)
    {
        if (!node) {
            return nullptr;
        }

        auto prev = node;
        auto cur = node->next;
        prev->next = nullptr;

        while (cur) {
            auto next = cur->next;
            cur->next = prev;
            prev = cur;
            cur = next;
        }

        return prev;
    }
};

/**
 * Original solution.
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    void reorderList(ListNode* head) {
        vector<ListNode*> nodes_list;
        for (auto cur = head; cur; cur = cur->next) {
            nodes_list.push_back(cur);
        }

        ListNode dummy;
        ListNode* cur = &dummy;
        size_t front = 0;
        size_t back = nodes_list.empty() ? 0 : nodes_list.size() - 1;
        while (front < back) {
            nodes_list[front]->next = nodes_list[back];
            cur->next = nodes_list[front];
            cur = nodes_list[back];
            ++front;
            --back;
        }

        if (front == back) {
            cur->next = nodes_list[front];
            cur = nodes_list[front];
        }
        cur->next = nullptr;
    }
};
