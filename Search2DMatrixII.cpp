/** Search a 2D Matrix II
 *
 * Write an efficient algorithm that searches for a target value in an m x n
 * integer matrix. The matrix has the following properties:
 *
 * Integers in each row are sorted in ascending from left to right.  Integers in
 * each column are sorted in ascending from top to bottom.
 *
 * Example 1:
 *
 * Input: matrix =
 * [[1,4,7,11,15],[2,5,8,12,19],[3,6,9,16,22],[10,13,14,17,24],[18,21,23,26,30]],
 * target = 5 Output: true Example 2:
 *
 * Input: matrix =
 * [[1,4,7,11,15],[2,5,8,12,19],[3,6,9,16,22],[10,13,14,17,24],[18,21,23,26,30]],
 * target = 20 Output: false
 *
 * Constraints:
 *
 * 1. m == matrix.length
 * 2. n == matrix[i].length
 * 3. 1 <= n, m <= 300
 * 4. -109 <= matix[i][j] <= 109
 * 5. All the integers in each row are sorted in ascending order.
 * 6. All the integers in each column are sorted in ascending order.
 * 7. -109 <= target <= 109
 */
/**
 * Same idea, but search from bottom left.
 */
class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        int n = matrix.size();
        if (0 == n) {
            return false;
        }

        int m = matrix[0].size();
        if (0 == m) {
            return false;
        }

        int r = n - 1;
        while (r >= 0 and matrix[r][0] > target) {
            r--;
        }

        int j = 0;
        while (r >= 0 and j < m) {
            auto value = matrix[r][j];
            if (value == target) {
                return true;
            }
            else if (value < target) {
                j++;
            }
            else {
                r--;
            }
        }

        return false;
    }
};

/**
 * From discussion. Search from top right corner.
 * And reduce one colume or one row at a time.
 */
class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        int m = matrix.size();
        int n = matrix[0].size();
        int r = 0, c = n - 1;

        while (r < m && c >= 0) {
            auto value = matrix[r][c];
            if (value == target) {
                return true;
            }

            if (value > target) {
                --c;
            }
            else {
                ++r;
            }
        }

        return false;
    }
};

/**
 * Original solution. Really simple and clear. But I think we can use binary
 * search directly on 2D Matrix
 */
class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        if (target < matrix.front().front() || target > matrix.back().back()) {
            return false;
        }

        for (auto const& row : matrix) {
            if (binary_search(row.begin(), row.end(), target)) {
                return true;
            }
        }

        return false;
    }
};
