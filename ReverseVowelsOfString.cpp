/**
 * Reverse Vowels of a String
 *
 * Write a function that takes a string as input and reverse only the vowels of a string.
 *
 * Example 1:
 *
 * Input: "hello"
 * Output: "holle"
 * Example 2:
 *
 * Input: "leetcode"
 * Output: "leotcede"
 * Note:
 * The vowels does not include the letter "y".
 */
/**
 * Front discussion. using std lib
 */
class Solution {
public:
    string reverseVowels(string s) {
        const string vowels = "aeiouAEIOU";
        int i = 0, j = s.size() - 1;
        while (i < j) {
            i = s.find_first_of(vowels, i);
            j = s.find_last_of(vowels, j);
            if (i < j) {
                swap(s[i++], s[j--]);
            }
        }

        return s;
    }
};

/**
 * original solution.
 */
class Solution {
public:
    string reverseVowels(string s) {
        const string vowels = "aeiouAEIOU";
        int front = 0, back = s.length() - 1;

        while (front < back) {
            while (front < back) {
                if (vowels.find(s[front]) != string::npos) {
                    break;
                }
                ++front;
            }

            while (back > front) {
                if (vowels.find(s[back]) != string::npos) {
                    break;
                }
                --back;
            }

            swap(s[front], s[back]);

            ++front; --back;
        }

        return s;
    }
};
