/**
 * Equal Row and Column Pairs
 *
 * Given a 0-indexed n x n integer matrix grid, return the number of pairs (ri, cj) such that row ri
 * and column cj are equal.
 *
 * A row and column pair is considered equal if they contain the same elements in the same order
 * (i.e., an equal array).
 *
 * Example 1:
 *
 * Input: grid = [[3,2,1],[1,7,6],[2,7,7]]
 * Output: 1
 * Explanation: There is 1 equal row and column pair:
 * - (Row 2, Column 1): [2,7,7]
 * Example 2:
 *
 * Input: grid = [[3,1,2,2],[1,4,4,5],[2,4,2,2],[2,4,2,2]]
 * Output: 3
 * Explanation: There are 3 equal row and column pairs:
 * - (Row 0, Column 0): [3,1,2,2]
 * - (Row 2, Column 2): [2,4,2,2]
 * - (Row 3, Column 2): [2,4,2,2]
 *
 * Constraints:
 *
 * n == grid.length == grid[i].length
 * 1 <= n <= 200
 * 1 <= grid[i][j] <= 105
 */
/**
 * More efficient one.
 */
vector<int> column_to_row(size_t col, vector<vector<int>> const& grid)
{
    auto n = grid.size();
    auto row = vector<int>(n, 0);
    for (auto r = 0; r < n; ++r) {
        row[r] = grid[r][col];
    }
    return row;
}

class Solution {
public:
    int equalPairs(vector<vector<int>>& grid) {
        auto ans = 0;
        map<vector<int>, int> vec_cnt;
        auto n = grid.size();
        for (auto c = 0 * n; c < n; ++c) {
            auto row = column_to_row(c, grid);
            ++vec_cnt[row];
        }

        for (auto const& row : grid) {
            auto it = vec_cnt.find(row);
            if (it != vec_cnt.end()) {
                ans += it->second;
            }
        }
        return ans;
    }
};

/**
 * Original solution.
 */
bool is_row_col_equal(int row, int col, vector<vector<int>> const& grid)
{
    auto n = static_cast<int>(grid.size());
    for (int i = 0; i < n; ++i) {
        if (grid[row][i] != grid[i][col]) {
            return false;
        }
    }
    return true;
}

class Solution {
public:
    int equalPairs(vector<vector<int>>& grid) {
        auto ans = 0;
        auto n = static_cast<int>(grid.size());
        for (auto i = 0; i < n; ++i) {
            for (auto j = 0; j < n; ++j) {
                ans += is_row_col_equal(i, j, grid);
            }
        }
        return ans;
    }
};
