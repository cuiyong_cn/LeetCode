/**
 * Sum of Square Numbers
 *
 * Given a non-negative integer c, decide whether there're two integers a and b such that a2 + b2 = c.
 *
 * Example 1:
 *
 * Input: c = 5
 * Output: true
 * Explanation: 1 * 1 + 2 * 2 = 5
 * Example 2:
 *
 * Input: c = 3
 * Output: false
 * Example 3:
 *
 * Input: c = 4
 * Output: true
 * Example 4:
 *
 * Input: c = 2
 * Output: true
 * Example 5:
 *
 * Input: c = 1
 * Output: true
 *
 * Constraints:
 *
 * 0 <= c <= 231 - 1
 */
/**
 * Original solution.
 * How to check if a integer is perfect square or not can also be implemented by following facts:
 *
 * The square of nth positive integer can be represented as a sum of the first n odd positive
 * integers.
 *
 * n * n = 1 + 3 + 5 + 7 + ... (2 * n - 1)
 *
 * prove:
 * n * n = 1 + 3 + 5 + ... (2 * n - 1)
 *       = (2 * 1 - 1) + (2 * 2 - 1) + (2 * 3 - 1) + ... (2 * n - 1)
 *       = 2 * (1 + 2 + 3 + ... + n) - (1 + 1 + .. + 1)
 *       = 2 * (n * (n + 1) / 2) - n
 *       = n * n + n - n
 *       = n * n
 *
 * Also we check the perfect square number using binary search.
 */
class Solution {
public:
    bool judgeSquareSum(int c) {
        int max_n = sqrt(c) + 1;
        for (int b = 0; b < max_n; ++b) {
            int a = c - (b * b);
            if (is_perfect_square(a)) {
                return true;
            }
        }

        return false;
    }

private:
    bool is_perfect_square(int n)
    {
        int sr = sqrt(n);
        return (sr * sr) == n;
    }
};
