/**
 * Range Sum of Sorted Subarray Sums
 *
 * You are given the array nums consisting of n positive integers. You computed the sum of all
 * non-empty continuous subarrays from the array and then sorted them in non-decreasing order,
 * creating a new array of n * (n + 1) / 2 numbers.
 *
 * Return the sum of the numbers from index left to index right (indexed from 1), inclusive, in the
 * new array. Since the answer can be a huge number return it modulo 109 + 7.
 *
 * Example 1:
 *
 * Input: nums = [1,2,3,4], n = 4, left = 1, right = 5
 * Output: 13
 * Explanation: All subarray sums are 1, 3, 6, 10, 2, 5, 9, 3, 7, 4. After sorting them in non-decreasing order we have the new array [1, 2, 3, 3, 4, 5, 6, 7, 9, 10]. The sum of the numbers from index le = 1 to ri = 5 is 1 + 2 + 3 + 3 + 4 = 13.
 * Example 2:
 *
 * Input: nums = [1,2,3,4], n = 4, left = 3, right = 4
 * Output: 6
 * Explanation: The given array is the same as example 1. We have the new array [1, 2, 3, 3, 4, 5, 6, 7, 9, 10]. The sum of the numbers from index le = 3 to ri = 4 is 3 + 3 = 6.
 * Example 3:
 *
 * Input: nums = [1,2,3,4], n = 4, left = 1, right = 10
 * Output: 50
 *
 * Constraints:
 *
 * n == nums.length
 * 1 <= nums.length <= 1000
 * 1 <= nums[i] <= 100
 * 1 <= left <= right <= n * (n + 1) / 2
 */
/***
 * Most efficient so far.
 */
class Solution {
public:
    int rangeSum(vector<int>& nums, int n, int left, int right) {
        partial_sum(begin(nums), end(nums), begin(nums));
        return (aggregate_k_sums(nums, right) - aggregate_k_sums(nums, left - 1)) % 1000000007;
    }

    long long aggregate_k_sums(vector<int>& nums, int k) {
        int max_sum = kth_sum(nums, k);
        long long sum = 0, sum_ij = 0, cnt = 0;
        for (int i = 0, j = 0; i < nums.size(); ++i) {
            int nums_i = i > 0 ? nums[i - 1] : 0;
            sum_ij -= nums_i;
            for (; j < nums.size() && nums[j] - nums_i <= max_sum; ++j)
                sum_ij += nums[j];
            sum += sum_ij - nums_i * (j - i);
            cnt += (j - i);
        }
        return sum - (cnt - k) * max_sum;
    }

    int kth_sum(vector<int>& nums, int k) {
        int l = 0, r = 100 * 1000;
        while (l < r) {
            int m = (l + r) / 2, cnt = 0;
            for (int i = 0, j = 0; i < nums.size(); ++i) {
                j = upper_bound(begin(nums) + j, end(nums), m + (i > 0 ? nums[i - 1] : 0)) - begin(nums);
                cnt += (j - i);
            }
            if (cnt < k)
                l = m + 1;
            else
                r = m;
        }
        return l;
    }
};

/**
 * More efficient based on original solution.
 */
class Solution {
public:
    int rangeSum(vector<int>& nums, int n, int left, int right) {
        vector<int> subs;
        for (auto i = 0; i < nums.size(); ++i) {
            for (auto j = i, sum = 0; j < nums.size(); ++j) {
                subs.push_back(sum += nums[j]);
            }
        }

        nth_element(begin(subs), begin(subs) + left - 1, end(subs));
        nth_element(begin(subs) + left, begin(subs) + right, end(subs));

        return accumulate(begin(subs) + left - 1, begin(subs) + right, 0,
                [](int s, int i) {
                    return (s + i) % 1000000007;
                });
    }
};

/**
 * Optimize.
 */
class Solution {
public:
    using data_type = pair<int, int>;
    using container_type = vector<pair<int, int>>;
    using sorting_method = greater<pair<int, int>>;

    int rangeSum(vector<int>& nums, int n, int left, int right) {
        priority_queue<data_type, container_type, sorting_method> mqueue;
        for (int i = 0; i < n; ++i) {
            mqueue.emplace(nums[i], i+1);
        }

        int ans = 0;
        int const mod = 1e9+7;
        for (int i = 1; i <= right; ++i) {
            auto p = mqueue.top(); mqueue.pop();
            if (i >= left) {
                ans = (ans + p.first) % mod;
            }
            if (p.second < n) {
                p.first += nums[p.second++];
                mqueue.push(p);
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int rangeSum(vector<int>& nums, int n, int left, int right) {
        vector<int> sub_sums;

        for (int i = 0; i < n; ++i) {
            int sum = 0;
            for (int j = i; j < n; ++j) {
                sum += nums[j];
                sub_sums.push_back(sum);
            }
        }

        sort(sub_sums.begin(), sub_sums.end());

        int sum = 0;
        int const mod = 1000000007;
        for (int i = left - 1; i < right; ++i) {
            sum = (sum + sub_sums[i]) % mod;
        }

        return sum;
    }
};
