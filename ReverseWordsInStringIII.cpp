/**
 * Reverse Words in a String III
 *
 * Given a string, you need to reverse the order of characters in each word within a
 * sentence while still preserving whitespace and initial word order.
 *
 * Example 1:
 *
 * Input: "Let's take LeetCode contest"
 * Output: "s'teL ekat edoCteeL tsetnoc"
 *
 * Note: In the string, each word is separated by single space and there will not
 * be any extra space in the string.
 */
/**
 * More efficient one
 */
class Solution {
public:
    string reverseWords(string s) {
        size_t start = 0;
        size_t pos = 0;
        while ((pos = s.find(' ', start)) != string::npos) {
            // same as reverse(&s[start], &s[pos]);
            reverse(s.begin() + start, s.begin() + pos);
            start = pos + 1;
        }
        reverse(s.begin() + start, s.end());
        return s;
    }
};

/**
 * istringstream method
 */
class Solution
{
public:
    string reverseWords(string s) {
        istringstream iss{s};
        auto iter = s.begin();
        for (string word; iss >> word;) {
            iter = copy(word.rbegin(), word.rend(), iter);
            if (iter != s.end())
                ++iter;
        }
        return s;
    }
};

/**
 * Mine original version
 */
class Solution {
public:
    string reverseWords(string s) {
        vector<string> vs;
        char sep = ' ';
        size_t start = 0;
        size_t pos = 0;
        while ((pos = s.find(sep, start)) != string::npos) {
            vs.push_back(s.substr(start, pos - start));
            start = pos + 1;
        }
        vs.push_back(s.substr(start));

        stringstream ss;
        for (int i = 0; i < vs.size(); ++i) {
            reverse(vs[i].begin(), vs[i].end());
            if (i == (vs.size() - 1)) {
                ss << vs[i];
            }
            else {
                ss << vs[i] << " ";
            }
        }

        return ss.str();
    }
};
