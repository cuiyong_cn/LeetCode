/**
 * Largest Values From Labels
 *
 * We have a set of items: the i-th item has value values[i] and label labels[i].
 *
 * Then, we choose a subset S of these items, such that:
 *
 *     |S| <= num_wanted
 *     For every label L, the number of items in S with label L is <= use_limit.
 *
 * Return the largest possible sum of the subset S.
 *
 * Example 1:
 *
 * Input: values = [5,4,3,2,1], labels = [1,1,2,2,3], num_wanted = 3, use_limit = 1
 * Output: 9
 * Explanation: The subset chosen is the first, third, and fifth item.
 *
 * Example 2:
 *
 * Input: values = [5,4,3,2,1], labels = [1,3,3,3,2], num_wanted = 3, use_limit = 2
 * Output: 12
 * Explanation: The subset chosen is the first, second, and third item.
 *
 * Example 3:
 *
 * Input: values = [9,8,8,7,6], labels = [0,0,0,1,1], num_wanted = 3, use_limit = 1
 * Output: 16
 * Explanation: The subset chosen is the first and fourth item.
 *
 * Example 4:
 *
 * Input: values = [9,8,8,7,6], labels = [0,0,0,1,1], num_wanted = 3, use_limit = 2
 * Output: 24
 * Explanation: The subset chosen is the first, second, and fourth item.
 *
 * Note:
 *
 *     1 <= values.length == labels.length <= 20000
 *     0 <= values[i], labels[i] <= 20000
 *     1 <= num_wanted, use_limit <= values.length
 */
/**
 * My original solution. The idea is values and labels uses the same sort index seqence.
 */
class Solution {
private:
    class MyComparator
    {
    public:
        MyComparator(vector<int>& values) : values{values} {}
        bool operator()(int i, int j) {
            return values[i] > values[j];
        }
    private:
        vector<int>& values;
    };
public:
    int largestValsFromLabels(vector<int>& values, vector<int>& labels, int num_wanted, int use_limit) {
        int ans = 0;
        vector<int> idxSeq(values.size(), 0);
        unordered_map<int, int> labelUsedTimes;
        for (int i = 0; i < values.size(); ++i) {
            idxSeq[i] = i;
            labelUsedTimes.insert({labels[i], 0});
        }
        sort(idxSeq.begin(), idxSeq.end(), MyComparator(values));

        int numOfSubset = 0;
        for (auto& i : idxSeq) {
            if (numOfSubset >= num_wanted) break;
            if (++labelUsedTimes[labels[i]] <= use_limit) {
                ans += values[i];
                ++numOfSubset;
            }
        }
        return ans;
    }
};

/**
 * Here is another version from leetcode. More compact. But I think my own version is more
 * intresting.
 */
class Solution {
public:
    int largestValsFromLabels(vector<int>& vs, vector<int>& ls, int wanted, int limit) {
        int ans = 0;
        multimap<int, int> s;
        unordered_map<int, int> m;
        for (auto i = 0; i < vs.size(); ++i) s.insert({vs[i], ls[i]});
        for (auto it = rbegin(s); it != rend(s) && wanted > 0; ++it) {
            if (++m[it->second] <= limit) {
              ans += it->first;
              --wanted;
            }
        }
        return ans;
    }
};
