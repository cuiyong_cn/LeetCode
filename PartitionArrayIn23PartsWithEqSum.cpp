/**
 * Partition Array Into Three Parts With Equal Sum
 *
 * Given an array A of integers, return true if and only if we can partition the array into three non-empty parts
 * with equal sums.
 *
 * Formally, we can partition the array if we can find indexes i+1 < j with (A[0] + A[1] + ... + A[i] == A[i+1] + A[i+2]
 * + ... + A[j-1] == A[j] + A[j-1] + ... + A[A.length - 1])
 *
 * Example 1:
 *
 * Input: [0,2,1,-6,6,-7,9,1,2,0,1]
 * Output: true
 * Explanation: 0 + 2 + 1 = -6 + 6 - 7 + 9 + 1 = 2 + 0 + 1
 *
 * Example 2:
 *
 * Input: [0,2,1,-6,6,7,9,-1,2,0,1]
 * Output: false
 *
 * Example 3:
 *
 * Input: [3,3,6,5,-2,2,5,1,-9,4]
 * Output: true
 * Explanation: 3 + 3 = 6 = 5 - 2 + 2 + 5 + 1 - 9 + 4
 *
 * Note:
 *
 *     3 <= A.length <= 50000
 *     -10000 <= A[i] <= 10000
 */
/**
 * Uisng only stl method.
 */
class Solution {
public:
    bool canThreePartsEqualSum(vector<int>& A) {
        
        vector<int> sum;
        partial_sum(begin(A), end(A), back_inserter(sum));
        int total = sum[sum.size()-1];
        if(total%3) 
            return false;
        auto it1 = find(begin(sum), end(sum), total/3);
        auto it2 = find(it1+1, end(sum), 2*total/3) ;
        
        return it1 != end(sum) && it2 != end(sum);

    }
};

/**
 * Improved one. more secure.
 */
class Solution {
public:
    bool canThreePartsEqualSum(vector<int>& A) {
        int total = accumulate(begin(A), end(A), 0);
        if (total % 3 != 0) return false;
        int partitionSum = total / 3;
        int eqSumCount = 0;
        int stopCount = total != 0 ? 2 : 3;
        for (auto i = 0, sum = 0; i < A.size(); ++i) {
            sum += A[i];
            if (sum == partitionSum) {
                sum = 0;
                ++eqSumCount;
                if (eqSumCount == stopCount) {
                    break;
                }
            }
        }
        return eqSumCount == stopCount;
    }
};
/**
 * My original solution. But this is not correct. Some edge cases are not considered.
 */
class Solution {
public:
    bool canThreePartsEqualSum(vector<int>& A) {
        int sum = accumulate(A.begin(), A.end(), 0);
        if (sum % 3 != 0) return false;

        int partitionSum = sum / 3;
        int subSum = 0;
        int eqSumCount = 0;
        for (int i = 0; i < A.size(); ++i) {
            subSum += A[i];
            if (subSum == partitionSum) {
                ++eqSumCount;
                subSum = 0;
            }
        }

        return 3 == eqSumCount ? true : false;
    }
};
