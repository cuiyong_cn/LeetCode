/**
 * Maximum Number of Operations With the Same Score II
 *
 * Given an array of integers called nums, you can perform any of the following operation while nums
 * contains at least 2 elements:
 *     Choose the first two elements of nums and delete them.
 *     Choose the last two elements of nums and delete them.
 *     Choose the first and the last elements of nums and delete them.
 *
 * The score of the operation is the sum of the deleted elements.
 * Your task is to find the maximum number of operations that can be performed, such that all operations have the same score.
 * Return the maximum number of operations possible that satisfy the condition mentioned above.
 *
 * Example 1:
 *
 * Input: nums = [3,2,1,2,3,4]
 * Output: 3
 * Explanation: We perform the following operations:
 * - Delete the first two elements, with score 3 + 2 = 5, nums = [1,2,3,4].
 * - Delete the first and the last elements, with score 1 + 4 = 5, nums = [2,3].
 * - Delete the first and the last elements, with score 2 + 3 = 5, nums = [].
 * We are unable to perform any more operations as nums is empty.
 *
 * Example 2:
 *
 * Input: nums = [3,2,6,1,4]
 * Output: 2
 * Explanation: We perform the following operations:
 * - Delete the first two elements, with score 3 + 2 = 5, nums = [6,1,4].
 * - Delete the last two elements, with score 1 + 4 = 5, nums = [6].
 * It can be proven that we can perform at most 2 operations.
 *
 * Constraints:
 *     2 <= nums.length <= 2000
 *     1 <= nums[i] <= 1000
 */
/**
 * Using cache to speed it up.
 */


class Solution {
public:
    int maxOperations(vector<int>& nums) {
        int const n = nums.size() - 1;

        return 1 + max({solve(nums, 2, n, nums[0] + nums[1])
                      , solve(nums, 1, n - 1, nums[0] + nums[n])
                      , solve(nums, 0, n - 2, nums[n - 1] + nums[n])});
    }

private:
    int solve(vector<int> const& nums, int f, int e, int sum)
    {
        if ((e - f) < 1) {
            return 0;
        }

        auto key = tuple(f, e, sum);

        if (m_cache.count(key)) {
            return m_cache[key];
        }

        auto f2 = (nums[f] + nums[f + 1]) == sum ? 1 + solve(nums, f + 2, e, sum) : 0;
        auto fe = (nums[f] + nums[e]) == sum ? 1 + solve(nums, f + 1, e - 1, sum) : 0;
        auto e2 = (nums[e - 1] + nums[e]) == sum ? 1 + solve(nums, f, e - 2, sum) : 0;
        auto len = max({f2, fe, e2});

        m_cache[key] = len;

        return len;
    }

    map<tuple<int, int, int>, int> m_cache;
};

/**
 * Original solution but TLE.
 */
auto solve(vector<int> const& nums, int f, int e, int sum)
{
    if ((e - f) < 1) {
        return 0;
    }

    auto f2 = (nums[f] + nums[f + 1]) == sum ? 1 + solve(nums, f + 2, e, sum) : 0;
    auto fe = (nums[f] + nums[e]) == sum ? 1 + solve(nums, f + 1, e - 1, sum) : 0;
    auto e2 = (nums[e - 1] + nums[e]) == sum ? 1 + solve(nums, f, e - 2, sum) : 0;

    return max({f2, fe, e2});
}

class Solution {
public:
    int maxOperations(vector<int>& nums) {
        int const n = nums.size() - 1;

        return 1 + max({solve(nums, 2, n, nums[0] + nums[1])
                      , solve(nums, 1, n - 1, nums[0] + nums[n])
                      , solve(nums, 0, n - 2, nums[n - 1] + nums[n])});
    }
};
