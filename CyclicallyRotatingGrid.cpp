/**
 * Cyclically Rotating a Grid
 *
 * You are given an m x n integer matrix grid, where m and n are both even
 * integers, and an integer k.
 *
 * The matrix is composed of several layers, which is shown in the below image, where each color is
 * its own layer:
 *
 * A cyclic rotation of the matrix is done by cyclically rotating each layer in the matrix. To
 * cyclically rotate a layer once, each element in the layer will take the place of the adjacent
 * element in the counter-clockwise direction. An example rotation is shown below:
 *
 * Return the matrix after applying k cyclic rotations to it.
 *
 * Example 1:
 *
 * Input: grid = [[40,10],[30,20]], k = 1
 * Output: [[10,20],[40,30]]
 * Explanation: The figures above represent the grid at every state.
 * Example 2:
 *
 * Input: grid = [[1,2,3,4],[5,6,7,8],[9,10,11,12],[13,14,15,16]], k = 2
 * Output: [[3,4,8,12],[2,11,10,16],[1,7,6,15],[5,9,13,14]]
 * Explanation: The figures above represent the grid at every state.
 *
 * Constraints:
 *
 * m == grid.length
 * n == grid[i].length
 * 2 <= m, n <= 50
 * Both m and n are even integers.
 * 1 <= grid[i][j] <= 5000
 * 1 <= k <= 109
 */
/**
 * Original solution.
 */
void flat(int L, vector<vector<int>> const& grid, vector<int>& layer_)
{
    int const rows = grid.size();
    int const cols = grid[0].size();
    int idx = 0;
    // top line
    for (int c = L; c < (cols - 1 - L); ++c, ++idx) {
        layer_[idx] = grid[L][c];
    }
    // right line
    for (int r = L; r < (rows - 1 - L); ++r, ++idx) {
        layer_[idx] = grid[r][cols - 1 - L];
    }
    // bottom line
    for (int c = cols - 1 - L; c > L; --c, ++idx) {
        layer_[idx] = grid[rows - 1 - L][c];
    }
    // left line
    for (int r = rows - 1 - L; r > L; --r, ++idx) {
        layer_[idx] = grid[r][L];
    }
}

void assign(int L, vector<vector<int>>& grid, vector<int> const& layer_)
{
    int const rows = grid.size();
    int const cols = grid[0].size();
    int idx = 0;
    // top line
    for (int c = L; c < (cols - 1 - L); ++c, ++idx) {
        grid[L][c] = layer_[idx];
    }
    // right line
    for (int r = L; r < (rows - 1 - L); ++r, ++idx) {
        grid[r][cols - 1 - L] = layer_[idx];
    }
    // bottom line
    for (int c = cols - 1 - L; c > L; --c, ++idx) {
        grid[rows - 1 - L][c] = layer_[idx];
    }
    // left line
    for (int r = rows - 1 - L; r > L; --r, ++idx) {
        grid[r][L] = layer_[idx];
    }
}

class Solution {
public:
    vector<vector<int>> rotateGrid(vector<vector<int>>& grid, int k) {
        int const rows = grid.size();
        int const cols = grid[0].size();

        int layers = min(rows, cols) / 2;
        for (int L = 0; L < layers; ++L) {
            int const count = (rows - 2 * L) * 2 + (cols - 2 * L) * 2 - 4;
            if (0 == k % count) {
                continue;
            }

            vector<int> layer_(count, 0);
            flat(L, grid, layer_);

            rotate(layer_.begin(), layer_.begin() + (k % count), layer_.end());

            assign(L, grid, layer_);
        }

        return grid;
    }
};
