/**
 * Replace the Substring for Balanced String
 *
 * You are given a string containing only 4 kinds of characters 'Q', 'W', 'E' and 'R'.
 *
 * A string is said to be balanced if each of its characters appears n/4 times where n is the length
 * of the string.
 *
 * Return the minimum length of the substring that can be replaced with any other string of the same
 * length to make the original string s balanced.
 *
 * Return 0 if the string is already balanced.
 *
 * Example 1:
 *
 * Input: s = "QWER"
 * Output: 0
 * Explanation: s is already balanced.
 * Example 2:
 *
 * Input: s = "QQWE"
 * Output: 1
 * Explanation: We need to replace a 'Q' to 'R', so that "RQWE" (or "QRWE") is balanced.
 * Example 3:
 *
 * Input: s = "QQQW"
 * Output: 2
 * Explanation: We can replace the first "QQ" to "ER".
 * Example 4:
 *
 * Input: s = "QQQQ"
 * Output: 3
 * Explanation: We can replace the last 3 'Q' to make s = "QWER".
 *
 * Constraints:
 *
 * 1 <= s.length <= 10^5
 * s.length is a multiple of 4
 * s contains only 'Q', 'W', 'E' and 'R'.
 */
/**
 * A little improve for readability.
 */
class Solution {
public:
    int balancedString(string s) {
        unordered_map<char, int> cnt;
        int const slen = s.size(), c4 = s.size() / 4;
        int ans = numeric_limits<int>::max();
        for (auto ch : s) {
            ++cnt[ch];
        }

        for (int i = 0, j = 0; i < slen; ++i) {
            --cnt[s[i]];
            while (j < s.size()) {
                if (any_of(begin(cnt), end(cnt),
                          [c4](auto const &p) {
                              return p.second > c4;
                          })) {
                    break;
                }
                ans = min(ans, i - j + 1);
                ++cnt[s[j++]];
            }
        }

        return ans;
    }
};

/**
 * Sliding window solution.
 */
class Solution {
public:
    int balancedString(string s) {
        unordered_map<char, int> cnt;
        int const slen = s.size(), c4 = s.size() / 4;
        int ans = numeric_limits<int>::max();
        for (auto ch : s) {
            ++cnt[ch];
        }

        for (int i = 0, j = 0; i < slen; ++i) {
            --cnt[s[i]];
            while (j < s.size()) {
                if (!all_of(begin(cnt), end(cnt),
                          [c4](auto const& p) {
                              return p.second <= c4;
                          })) {
                    break;
                }
                ans = min(ans, i - j + 1);
                ++cnt[s[j++]];
            }
        }

        return ans;
    }
};

/**
 * Original solution. TLE.
 */
class Solution {
public:
    int balancedString(string s) {
        int const slen = s.length();
        int const qidx = 0, widx = 1, eidx = 2, ridx = 3;
        unordered_map<char, int> ch_idx = { {'Q', qidx}, {'W', widx}, {'E', eidx}, {'R', ridx} };
        vector<vector<int>> ch_cnt_prefix(slen + 1, vector<int>(4, 0));
        for (int i = 0; i < slen; ++i) {
            int idx = ch_idx[s[i]];
            ch_cnt_prefix[i + 1] = ch_cnt_prefix[i];
            ch_cnt_prefix[i + 1][idx] += 1;
        }

        int const q_cnt = ch_cnt_prefix[slen][qidx];
        int const w_cnt = ch_cnt_prefix[slen][widx];
        int const e_cnt = ch_cnt_prefix[slen][eidx];
        int const r_cnt = ch_cnt_prefix[slen][ridx];
        if (q_cnt == w_cnt && w_cnt == e_cnt && e_cnt == r_cnt) {
            return 0;
        }

        int const c4 = slen / 4;
        for (int L = 0; L <= slen; ++L) {
            for (int i = 0; (i + L) < slen; ++i) {
                int const q = ch_cnt_prefix[i][qidx] + q_cnt - ch_cnt_prefix[i + L + 1][qidx];
                int const w = ch_cnt_prefix[i][widx] + w_cnt - ch_cnt_prefix[i + L + 1][widx];
                int const e = ch_cnt_prefix[i][eidx] + e_cnt - ch_cnt_prefix[i + L + 1][eidx];
                int const r = ch_cnt_prefix[i][ridx] + r_cnt - ch_cnt_prefix[i + L + 1][ridx];
                if (q <= c4 && w <= c4 && e <= c4 && r <= c4) {
                    return L + 1;
                }
            }
        }

        return slen;
    }
};
