/**
 * Flip Binary Tree To Match Preorder Traversal
 *
 * Given a binary tree with N nodes, each node has a different value from {1, ..., N}.
 *
 * A node in this binary tree can be flipped by swapping the left child and the right child of that node.
 *
 * Consider the sequence of N values reported by a preorder traversal starting from the root.  Call such a sequence of N
 * values the voyage of the tree.
 *
 * (Recall that a preorder traversal of a node means we report the current node's value, then preorder-traverse the left
 * child, then preorder-traverse the right child.)
 *
 * Our goal is to flip the least number of nodes in the tree so that the voyage of the tree matches the voyage we are given.
 *
 * If we can do so, then return a list of the values of all nodes flipped.  You may return the answer in any order.
 *
 * If we cannot do so, then return the list [-1].
 *
 * Example 1:
 *
 * Input: root = [1,2], voyage = [2,1]
 * Output: [-1]
 * Example 2:
 *
 * Input: root = [1,2,3], voyage = [1,3,2]
 * Output: [1]
 * Example 3:
 *
 * Input: root = [1,2,3], voyage = [1,2,3]
 * Output: []
 *
 * Note:
 *
 * 1 <= N <= 100
 */
/**
 * Original solution. From the discussion. The user an_shuman777 post a interesting query.
 *      flip the least number of nodes in the problem statement is just to mislead.
 * And user ps_sneak's reply is worth noted:
 *
 * Yes and no.
 *
 * When I first tried this problem, I missed the part where nodes are DISTINCT. In that case, this problem needs O(N^3)
 * time to solve and the least number of nodes requirement makes a difference.
 *
 * Only when the nodes are DISTINCT does the least number of nodes requirement not make a difference (and the problem is
 * solvable in O(N)).
 *
 * It seems to me the author initially intended the generic non-distinct version but probably changed it. Labelling the
 * full version an LC med is a stretch though.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> flipMatchVoyage(TreeNode* root, vector<int>& voyage) {
        vector<int> ans;

        int i = 0;
        if (!dfs(root, voyage, i, ans)) {
            return {-1};
        }

        return ans;
    }

private:
    bool dfs(TreeNode* root, const vector<int>& voyage, int& i, vector<int>& ans)
    {
        if (!root) {
            return true;
        }

        if (root->val != voyage[i++]) {
            return false;
        }

        if (root->left) {
            if (root->left->val != voyage[i]) {
                swap(root->left, root->right);
                ans.push_back(root->val);
            }
        }

        if (!dfs(root->left, voyage, i, ans)) {
            return false;
        }

        return dfs(root->right, voyage, i, ans);
    }
};
