/**
 * Find the Largest Area of Square Inside Two Rectangles
 *
 * There exist n rectangles in a 2D plane. You are given two 0-indexed 2D integer arrays bottomLeft
 * and topRight, both of size n x 2, where bottomLeft[i] and topRight[i] represent the bottom-left
 * and top-right coordinates of the ith rectangle respectively.
 *
 * You can select a region formed from the intersection of two of the given rectangles. You need to
 * find the largest area of a square that can fit inside this region if you select the region
 * optimally.
 *
 * Return the largest possible area of a square, or 0 if there do not exist any intersecting regions between the rectangles.
 *
 * Example 1:
 *
 * Input: bottomLeft = [[1,1],[2,2],[3,1]], topRight = [[3,3],[4,4],[6,6]]
 * Output: 1
 * Explanation: A square with side length 1 can fit inside either the intersecting region of rectangle 0 and rectangle 1, or the intersecting region of rectangle 1 and rectangle 2. Hence the largest area is side * side which is 1 * 1 == 1.
 * It can be shown that a square with a greater side length can not fit inside any intersecting region.
 *
 * Example 2:
 *
 * Input: bottomLeft = [[1,1],[2,2],[1,2]], topRight = [[3,3],[4,4],[3,4]]
 * Output: 1
 * Explanation: A square with side length 1 can fit inside either the intersecting region of rectangle 0 and rectangle 1, the intersecting region of rectangle 1 and rectangle 2, or the intersection region of all 3 rectangles. Hence the largest area is side * side which is 1 * 1 == 1.
 * It can be shown that a square with a greater side length can not fit inside any intersecting region.
 * Note that the region can be formed by the intersection of more than 2 rectangles.
 *
 * Example 3:
 *
 * Input: bottomLeft = [[1,1],[3,3],[3,1]], topRight = [[2,2],[4,4],[4,2]]
 * Output: 0
 * Explanation: No pair of rectangles intersect, hence, we return 0.
 *
 * Constraints:
 *     n == bottomLeft.length == topRight.length
 *     2 <= n <= 103
 *     bottomLeft[i].length == topRight[i].length == 2
 *     1 <= bottomLeft[i][0], bottomLeft[i][1] <= 107
 *     1 <= topRight[i][0], topRight[i][1] <= 107
 *     bottomLeft[i][0] < topRight[i][0]
 *     bottomLeft[i][1] < topRight[i][1]
 */
/**
 * It seems the function call consumes quite a lot.
 */
class Solution {
public:
    long long largestSquareArea(vector<vector<int>>& bottomLeft, vector<vector<int>>& topRight) {
        auto ans = 0LL;
        int const n = bottomLeft.size();

        for (auto i = 0; i < n; ++i) {
            for (auto j = i + 1; j < n; ++j) {
                auto const& r1bl = bottomLeft[i];
                auto const& r1tr = topRight[i];
                auto const& r2bl = bottomLeft[j];
                auto const& r2tr = topRight[j];

                if ((r1tr[0] <= r2bl[0]) || (r2tr[0] <= r1bl[0])
                 || (r1tr[1] <= r2bl[1]) || (r2tr[1] <= r1bl[1])) {
                    continue;
                }

                auto [bx, by] = tuple{max(r1bl[0], r2bl[0]), max(r1bl[1], r2bl[1])};
                auto [tx, ty] = tuple{min(r1tr[0], r2tr[0]), min(r1tr[1], r2tr[1])};
                auto L = 1LL * min(abs(tx - bx), abs(ty - by));
                ans = max(ans, L * L);
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
auto intersect(vector<int> const& r1bl, vector<int> const& r1tr
             , vector<int> const& r2bl, vector<int> const& r2tr)
{
    if ((r1tr[0] <= r2bl[0]) || (r2tr[0] <= r1bl[0])
     || (r1tr[1] <= r2bl[1]) || (r2tr[1] <= r1bl[1])) {
        return tuple{false, 0};
    }

    auto [bx, by] = tuple{max(r1bl[0], r2bl[0]), max(r1bl[1], r2bl[1])};
    auto [tx, ty] = tuple{min(r1tr[0], r2tr[0]), min(r1tr[1], r2tr[1])};

    return tuple{true, min(abs(tx - bx), abs(ty - by))};
}

class Solution {
public:
    long long largestSquareArea(vector<vector<int>>& bottomLeft, vector<vector<int>>& topRight) {
        auto ans = 0LL;
        int const n = bottomLeft.size();

        for (auto i = 0; i < n; ++i) {
            for (auto j = i + 1; j < n; ++j) {
                auto [overlap, L] = intersect(bottomLeft[i], topRight[i], bottomLeft[j], topRight[j]);
                if (overlap) {
                    ans = max(ans, 1LL * L * L);
                }
            }
        }

        return ans;
    }
};
