/**
 * Minimize Result by Adding Parentheses to Expression
 *
 * You are given a 0-indexed string expression of the form "<num1>+<num2>" where <num1> and <num2>
 * represent positive integers.
 *
 * Add a pair of parentheses to expression such that after the addition of parentheses, expression
 * is a valid mathematical expression and evaluates to the smallest possible value. The left
 * parenthesis must be added to the left of '+' and the right parenthesis must be added to the right
 * of '+'.
 *
 * Return expression after adding a pair of parentheses such that expression evaluates to the
 * smallest possible value. If there are multiple answers that yield the same result, return any of
 * them.
 *
 * The input has been generated such that the original value of expression, and the value of
 * expression after adding any pair of parentheses that meets the requirements fits within a signed
 * 32-bit integer.
 *
 * Example 1:
 *
 * Input: expression = "247+38"
 * Output: "2(47+38)"
 * Explanation: The expression evaluates to 2 * (47 + 38) = 2 * 85 = 170.
 * Note that "2(4)7+38" is invalid because the right parenthesis must be to the right of the '+'.
 * It can be shown that 170 is the smallest possible value.
 * Example 2:
 *
 * Input: expression = "12+34"
 * Output: "1(2+3)4"
 * Explanation: The expression evaluates to 1 * (2 + 3) * 4 = 1 * 5 * 4 = 20.
 * Example 3:
 *
 * Input: expression = "999+999"
 * Output: "(999+999)"
 * Explanation: The expression evaluates to 999 + 999 = 1998.
 *
 * Constraints:
 *
 * 3 <= expression.length <= 10
 * expression consists of digits from '1' to '9' and '+'.
 * expression starts and ends with digits.
 * expression contains exactly one '+'.
 * The original value of expression, and the value of expression after adding any pair of parentheses that meets the requirements fits within a signed 32-bit integer.
 */
/**
 * Same idea. Might be more efficient.
 */
class Solution {
public:
    string minimizeResult(string expression) {
        auto plus = expression.find('+');
        auto min_value = numeric_limits<int>::max();
        auto v = tuple(0, 0, 0, 0);

        for (auto num1 = stoi(expression.substr(0, plus)), mul = 10; num1 * 10 >= mul; mul *= 10) {
            auto a = num1 / mul;
            auto b = num1 % mul;
            for (auto num2 = stoi(expression.substr(plus + 1)), mul = 1; num2 / mul > 0; mul *= 10) {
                auto c = num2 / mul;
                auto d = num2 % mul;
                auto value = max(1, a) * (b + c) * max(1, d);
                if (value < min_value) {
                    v = {a, b, c, d};
                    min_value = value;
                }
            }
        }

        auto [a, b, c, d] = v;
        return (0 == a ? "" : to_string(a)) + '(' + to_string(b) + '+'
            + to_string(c) + ')' + (0 == d ? "" : to_string(d));
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    string minimizeResult(string expression) {
        auto plus = expression.find('+');
        auto num1 = expression.substr(0, plus);
        auto num2 = expression.substr(plus + 1);

        auto min_value = numeric_limits<int>::max();
        auto left = 0;
        auto right = 0;

        for (auto pl = 0; pl < num1.length(); ++pl) {
            auto a = 0 == pl ? 1 : stoi(num1.substr(0, pl));
            auto b = stoi(num1.substr(pl));
            for (auto pr = 1; pr <= num2.length(); ++pr) {
                auto c = stoi(num2.substr(0, pr));
                auto d = num2.length() == pr ? 1 : stoi(num2.substr(pr));
                auto value = a * (b + c) * d;
                if (value < min_value) {
                    left = pl;
                    right = pr;
                    min_value = value;
                }
            }
        }

        return num1.substr(0, left) + '(' + num1.substr(left) + '+'
            +  num2.substr(0, right) + ')' + num2.substr(right);
    }
};
