/**
 * Count Number of Distinct Integers After Reverse Operations
 *
 * You are given an array nums consisting of positive integers.
 *
 * You have to take each integer in the array, reverse its digits, and add it to the end of the
 * array. You should apply this operation to the original integers in nums.
 *
 * Return the number of distinct integers in the final array.
 *
 * Example 1:
 *
 * Input: nums = [1,13,10,12,31]
 * Output: 6
 * Explanation: After including the reverse of each number, the resulting array is [1,13,10,12,31,1,31,1,21,13].
 * The reversed integers that were added to the end of the array are underlined. Note that for the
 * integer 10, after reversing it, it becomes 01 which is just 1.
 * The number of distinct integers in this array is 6 (The numbers 1, 10, 12, 13, 21, and 31).
 * Example 2:
 *
 * Input: nums = [2,2,2]
 * Output: 1
 * Explanation: After including the reverse of each number, the resulting array is [2,2,2,2,2,2].
 * The number of distinct integers in this array is 1 (The number 2).
 *
 * Constraints:
 *
 * 1 <= nums.length <= 105
 * 1 <= nums[i] <= 106
 */
/**
 * Another one.
 */

int reverse(int n)
{
    auto str = to_string(n);
    return stoi(string{rbegin(str), rend(str)});
}

class Solution {
public:
    int countDistinctIntegers(vector<int>& nums) {
        auto seen = unordered_set<int>{begin(nums), end(nums)};
        auto ans = 0;
        for (auto n : nums) {
            if (0 == seen.count(n)) {
                auto reversed = reverse(n);
                if (n == reversed || seen.count(reversed)) {
                    ++ans;
                }
                else {
                    ans += 2;
                    seen.insert(reversed);
                }
                seen.insert(n)
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
int reverse(int n)
{
    auto str = to_string(n);
    return stoi(string{rbegin(str), rend(str)});
}

class Solution {
public:
    int countDistinctIntegers(vector<int>& nums) {
        auto orig_set = unordered_set<int>{begin(nums), end(nums)};
        auto reverse_set = unordered_set<int>{};
        for (auto n : orig_set) {
            reverse_set.insert(reverse(n));
        }

        orig_set.merge(reverse_set);
        return orig_set.size();
    }
};
