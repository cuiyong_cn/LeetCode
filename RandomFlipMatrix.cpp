/**
 * Random Flip Matrix
 *
 * There is an m x n binary grid matrix with all the values set 0 initially. Design an algorithm to
 * randomly pick an index (i, j) where matrix[i][j] == 0 and flips it to 1. All the indices (i, j)
 * where matrix[i][j] == 0 should be equally likely to be returned.
 *
 * Optimize your algorithm to minimize the number of calls made to the built-in random function of
 * your language and optimize the time and space complexity.
 *
 * Implement the Solution class:
 *
 * Solution(int m, int n) Initializes the object with the size of the binary matrix m and n.
 * int[] flip() Returns a random index [i, j] of the matrix where matrix[i][j] == 0 and flips it to 1.
 * void reset() Resets all the values of the matrix to be 0.
 *
 * Example 1:
 *
 * Input
 * ["Solution", "flip", "flip", "flip", "reset", "flip"]
 * [[3, 1], [], [], [], [], []]
 * Output
 * [null, [1, 0], [2, 0], [0, 0], null, [2, 0]]
 *
 * Explanation
 * Solution solution = new Solution(3, 1);
 * solution.flip();  // return [1, 0], [0,0], [1,0], and [2,0] should be equally likely to be returned.
 * solution.flip();  // return [2, 0], Since [1,0] was returned, [2,0] and [0,0]
 * solution.flip();  // return [0, 0], Based on the previously returned indices, only [0,0] can be returned.
 * solution.reset(); // All the values are reset to 0 and can be returned.
 * solution.flip();  // return [2, 0], [0,0], [1,0], and [2,0] should be equally likely to be returned.
 *
 * Constraints:
 *
 * 1 <= m, n <= 104
 * There will be at least one free cell for each call to flip.
 * At most 1000 calls will be made to flip and reset.
 */
/**
 * In order to reduce the number of rand calls. We use fisher-Yates shuffle
 */
class Solution {
public:
    Solution(int n_rows, int n_cols) : n_rows(n_rows), n_cols(n_cols), size(n_rows * n_cols) {
        srand(time(nullptr));
    }

    vector<int> flip() {
        int id = rand() % size;
        int rid = id;

        --size;

        if (holes.count(id)) {
           id = holes[id];
        }

        // Create or update whole at real index rid. Since the
        // virtual space is shrinking move the hole at it boundary
        // to rid or point to boundary
        holes[rid] = holes.count(size) ? holes[size] : size;

        return {id / n_cols, id % n_cols};
    }

    void reset() {
        holes.clear();
        size = n_rows * n_cols;
    }

private:
    int n_rows;
    int n_cols;
    int size;
    unordered_map<int, int> holes;
};

/**
 * Ok, I changed the vector to unordered_set, and it passed. So I assume memory allocatioin
 * consumes too much time.
 */
class Solution {
public:
    Solution(int m, int n) : row{m}, col{n} {
        srand (time(0));
    }

    vector<int> flip() {
        int range = row * col;
        while (1) {
            int rd = rand() % range;
            if (!flipped.count(rd)) {
                flipped.insert(rd);
                return {rd / col, rd % col};
            }
        }
    }

    void reset() {
        flipped.clear();
    }

private:
    int row;
    int col;
    unordered_set<int> flipped;
};

/**
 * Original solution. TLE.
 */
class Solution {
public:
    Solution(int m, int n) : row{m}, col{n}, bin_matrix(m * n, false) {
        srand (time(0));
    }

    vector<int> flip() {
        int range = row * col;
        while (1) {
            int rd = rand() % range;
            if (!bin_matrix[rd]) {
                bin_matrix[rd] = true;
                return {rd / col, rd % col};
            }
        }
    }

    void reset() {
        fill(bin_matrix.begin(), bin_matrix.end(), 0);
    }

private:
    int row;
    int col;
    vector<bool> bin_matrix;
};

/**
 * Your Solution object will be instantiated and called as such:
 * Solution* obj = new Solution(m, n);
 * vector<int> param_1 = obj->flip();
 * obj->reset();
 */
