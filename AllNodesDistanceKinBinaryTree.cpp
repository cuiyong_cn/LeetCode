/** All Nodes Distance K in Binary Tree
 *
 * We are given a binary tree (with root node root), a target node, and an integer value K.
 *
 * Return a list of the values of all nodes that have a distance K from the target node.  The answer can be returned in
 * any order.
 *
 * Example 1:
 *
 * Input: root = [3,5,1,6,2,0,8,null,null,7,4], target = 5, K = 2
 *
 * Output: [7,4,1]
 *
 * Explanation: The nodes that are a distance 2 from the target node (with value 5) have values 7, 4, and 1.
 *                                 3
 *                                / \
 *                               /   \
 *                              /     \
 *                             5       1
 *                            / \     / \
 *                           6   2   0   9
 *                              / \
 *                             7   4
 *
 * Note that the inputs "root" and "target" are actually TreeNodes.  The descriptions of the inputs above are just
 * serializations of these objects.
 *
 * Note:
 *     The given tree is non-empty.  Each node in the tree has unique values 0 <= node.val <= 500.  The target node is a
 *     node in the tree.  0 <= K <= 1000.
 */
/**
 * Improved the below solution. First we build the path to target from root node, and then we walking through this
 * path to find all the K distance nodes.
 *
 * This solution better than below one in 2 ways.
 * 1. we may not need to traversal all the nodes to build the path (meaning to find where the target node is),
 *      (if the target in left subtree, we only walk half of them tops)
 * 2. Now we don't need to flat the tree, save lots of space.
 */
class Solution {
public:
    unordered_map<TreeNode*, int> pathToTarget;
    TreeNode* targetNode;
    vector<int> ans;

    vector<int> distanceK(TreeNode* root, TreeNode* target, int K) {
        targetNode = target;
        buildPathToTargetFrom(root);

        for (auto& p : pathToTarget) {
            TreeNode* node = p.first;
            int distance = p.second;
            if (distance == K) {
                ans.push_back(node->val);
            }
            else if (distance < K) {
                TreeNode* left = node->left;
                TreeNode* right = node->right;
                if (0 == pathToTarget.count(left)) {
                    dfs(left, distance + 1, K);
                }
                if (0 == pathToTarget.count(right)) {
                    dfs(right, distance + 1, K);
                }
            }
        }

        return ans;
    }

    int buildPathToTargetFrom(TreeNode* node) {
        if (nullptr == node) return -1;
        if (node == targetNode) {
            pathToTarget[node] = 0;
            return 0;
        }

        int L = buildPathToTargetFrom(node->left);
        if (L >= 0) {
            pathToTarget[node] = L + 1;
            return L + 1;
        }

        int R = buildPathToTargetFrom(node->right);
        if (R >= 0) {
            pathToTarget[node] = R + 1;
            return R + 1;
        }

        return -1;
    }

    void dfs(TreeNode* node, int distance, int K) {
        if (nullptr == node) return;
        if (distance == K) {
            ans.push_back(node->val);
            return;
        }

        dfs(node->left, distance + 1, K);
        dfs(node->right, distance + 1, K);
    }
};

/**
 * Original method. O(n) method, O(n^2) space. The idea is:
 * Flat the binary tree in array. And backtrace the node and add the
 * node K distance from target node.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<vector<TreeNode*>> layers;
    vector<int> ans;
    int treeHeight;

    vector<int> distanceK(TreeNode* root, TreeNode* target, int K) {
        layers = vector<vector<TreeNode*>>(1, vector<TreeNode*>(1, nullptr));
        int L = 0, tl = 0, ti = 0;
        layers[0][0] = root;

        while (1) {
            ++L;
            int count = 1 << L;
            int nodeCount = 0;
            layers.push_back(vector<TreeNode*>(count, nullptr));
            for (int i = 0; i < layers[L - 1].size(); ++i) {
                TreeNode* cur = layers[L - 1][i];
                if (nullptr != cur) {
                    layers[L][i * 2] = cur->left;
                    layers[L][i * 2 + 1] = cur->right;
                    if (cur->left) ++nodeCount;
                    if (cur->right) ++nodeCount;
                    if (cur == target) {
                        tl = L - 1;
                        ti = i;
                    }
                }
            }
            if (0 == nodeCount) break;
        }

        treeHeight = layers.size() - 1;

        directlyBelowKDistance(tl, ti, K);

        int tk = K;
        int pre = ti;
        while (tl > 0) {
            --tl; --tk;
            ti = ti >> 1;
            if (tk > 0) {
                if ((ti * 2) != pre) {
                    directlyBelowKDistance(tl + 1, ti * 2, tk - 1);
                }
                else {
                    directlyBelowKDistance(tl + 1, ti * 2 + 1, tk - 1);
                }
            }
            pre = ti;

            if (tk == 0) break;
        }

        if (0 == tk) {
            ans.push_back(layers[tl][ti]->val);
        }

        return ans;
    }

    void directlyBelowKDistance(int tl, int ti, int K) {
        if ((tl + K) < treeHeight) {
            int nl = tl + K;
            int block = 1 << K;
            int end = (ti + 1) * block;

            for (int s = ti * block; s < end; ++s) {
                TreeNode* cur = layers[nl][s];
                if (nullptr != cur) {
                    ans.push_back(cur->val);
                }
            }
        }
    }
};
