/**
 * You're given strings J representing the types of stones that are jewels,
 * and S representing the stones you have.  Each character in S is a type of stone
 * you have.  You want to know how many of the stones you have are also jewels.
 *
 * The letters in J are guaranteed distinct, and all characters in J and S are letters.
 * Letters are case sensitive, so "a" is considered a different type of stone from "A".
 *
 * Example 1:
 *
 * Input: J = "aA", S = "aAAbbbb"
 * Output: 3
 *
 * Example 2:
 * 
 * Input: J = "z", S = "ZZ"
 * Output: 0
 * 
 * Note:
 * 
 *     S and J will consist of letters and have length at most 50.
 *     The characters in J are distinct.
 */

/**
 * Better solution is using hashset.
 * Time complexity is O(J+S) -> O(n)
 *
 *  int numJewelsInStones(string J, string S) {
 *      int res = 0;
 *      unordered_set<char> setJ(J.begin(), J.end());
 *      for (char s : S)
 *          if (setJ.count(s)) res++;
 *      return res;
 *  }
 *
 *  int numJewelsInStones(string J, string S) {
 *      map<char,int> M;
 *      int res = 0;
 *      
 *      //Create a Hashmap of all the stones
 *      for (char c : S){
 *          M[c] += 1;
 *      }
 *      
 *      //Iterate through the jewels & find total number of jewels from the map of stones
 *      for (char c: J){
 *          res += M[c];
 *      }
 *
 *      return res;
 *  }
 */

class Solution {
public:
    /**
     * Though this solution is crystal clear.
     * The complexity is O(JS)
     */
    int numJewelsInStones(string J, string S) {
        int numOfJewels = 0;
        for (auto j : J) {
            for (auto s : S) {
                if (j == s) {
                    numOfJewels++;
                }
            }
        }
        
        return numOfJewels;
    }
};
