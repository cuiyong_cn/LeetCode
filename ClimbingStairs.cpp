/**
 * Climbing Stairs
 *
 * You are climbing a stair case. It takes n steps to reach to the top.
 *
 * Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?
 *
 * Example 1:
 *
 * Input: 2
 * Output: 2
 * Explanation: There are two ways to climb to the top.
 * 1. 1 step + 1 step
 * 2. 2 steps
 * Example 2:
 *
 * Input: 3
 * Output: 3
 * Explanation: There are three ways to climb to the top.
 * 1. 1 step + 1 step + 1 step
 * 2. 1 step + 2 steps
 * 3. 2 steps + 1 step
 *
 * Constraints:
 *
 * 1 <= n <= 45
 */
/**
 * using the math formular.
 */
public class Solution {
    public int climbStairs(int n) {
        double sqrt5=Math.sqrt(5);
        double fibn=Math.pow((1+sqrt5)/2,n+1)-Math.pow((1-sqrt5)/2,n+1);
        return (int)(fibn/sqrt5);
    }
}

/**
 * From the leetcode. This can be solve by Binets methods.
 *
 * We can get the nth Fibonacci number using the following method.
 *
 *             F(n + 1)   F(n)          1     1
 *     Q   =                        =
 *             F(n)       F(n - 1)      1     0
 *
 * F(n) = Q^(n - 1);
 */
using Matrix2D = std::vector<std::vector<int>>;

Matrix2D operator*(const Matrix2D& a, const Matrix2D& b)
{
    if (!a.empty() && !a[0].empty() && a[0].size() != b.size()) {
        throw runtime_error("Matrix2D dimension mismatch.");
    }

    if (!b.empty() && b[0].size() == 0) {
        throw runtime_error("second matrix the dimension is n x 0");
    }

    Matrix2D ans = Matrix2D(a.size(), vector<int>(b[0].size(), 0));

    for (size_t i = 0; i < ans.size(); ++i) {
        for (size_t j = 0; j < ans[0].size(); ++j) {
            int sum = 0;
            for (size_t k = 0; k < a[0].size(); ++k) {
                sum += a[i][k] * b[k][j];
            }

            ans[i][j] = sum;
        }
    }

    return ans;
}

class Solution {
public:
    int climbStairs(int n) {
        Matrix2D q = { {1, 1}, {1, 0} };

        auto ans = pow(q, n);
        return ans[0][0];
    }

    Matrix2D pow(Matrix2D q, int n)
    {
        Matrix2D ans = { {1, 0}, {0, 1} };

        while (n > 0) {
            if (n & 1) {
                ans = ans * q;
            }
            n >>= 1;
            if (n == 0) {
                break;
            }
            q = q * q;
        }

        return ans;
    }
};

/**
 * dp solution.
 */
class Solution {
public:
    int climbStairs(int n) {
        if (n <= 3) {
            return n;
        }

        int a = 2;
        int b = 3;
        int ans = 0;
        for (size_t i = 4; i <= n; ++i) {
            ans = a + b;
            a = b;
            b = ans;
        }

        return ans;
    }
};

/**
 * Improved one using cache mechanism.
 */
class Solution {
public:
    int climbStairs(int n) {
        if (n <= 3) {
            return n;
        }

        if (cache.count(n)) {
            return cache[n];
        }

        auto combos = climbStairs(n - 1) + climbStairs(n - 2);
        cache[n] = combos;
        return combos;
    }

    unordered_map<int, int> cache;
};

/**
 * Original solution. Recursive method but TLE though correct.
 * There are some computation redundency.
 */
class Solution {
public:
    int climbStairs(int n) {
        if (n <= 3) {
            return n;
        }

        return climbStairs(n - 1) + climbStairs(n - 2);
    }
};
