/**
 * Two Best Non-Overlapping Events
 *
 * You are given a 0-indexed 2D integer array of events where events[i] = [startTimei, endTimei,
 * valuei]. The ith event starts at startTimei and ends at endTimei, and if you attend this event,
 * you will receive a value of valuei. You can choose at most two non-overlapping events to attend
 * such that the sum of their values is maximized.
 *
 * Return this maximum sum.
 *
 * Note that the start time and end time is inclusive: that is, you cannot attend two events where
 * one of them starts and the other ends at the same time. More specifically, if you attend an event
 * with end time t, the next event must start at or after t + 1.
 *
 * Example 1:
 *
 * Input: events = [[1,3,2],[4,5,2],[2,4,3]]
 * Output: 4
 * Explanation: Choose the green events, 0 and 1 for a sum of 2 + 2 = 4.
 * Example 2:
 *
 * Example 1 Diagram
 * Input: events = [[1,3,2],[4,5,2],[1,5,5]]
 * Output: 5
 * Explanation: Choose event 2 for a sum of 5.
 * Example 3:
 *
 * Input: events = [[1,5,3],[1,5,1],[6,6,5]]
 * Output: 8
 * Explanation: Choose events 0 and 2 for a sum of 3 + 5 = 8.
 *
 * Constraints:
 *
 * 2 <= events.length <= 105
 * events[i].length == 3
 * 1 <= startTimei <= endTimei <= 109
 * 1 <= valuei <= 106
 */
/**
 * Sort and map.
 */
class Solution {
public:
    int maxTwoEvents(vector<vector<int>>& events) {
        sort(events.begin(), events.end());

        int ans = 0;
        int max_value = 0;
        map<int, int> max_value_after;

        for (int i = events.size() - 1; i >= 0; --i) {
            auto mva = max_value_after.upper_bound(events[i][1]);
            max_value = max(max_value, events[i][2]);
            max_value_after[events[i][0]] = max_value;

            if (mva != max_value_after.end()) {
                ans = max(ans, events[i][2] + mva->second);
            }
            else {
                ans = max(ans, max_value);
            }
        }

        return ans;
    }
};

/**
 * Sort and memo
 */
class Solution {
public:
    int maxTwoEvents(vector<vector<int>>& events) {
        int const n = events.size();
        auto dp = vector<vector<int>>(n, vector<int>{-1, -1});

        //Sorting because since we need to find an event that has starting time > ending time
        //of previous event selected, so applying binary search would help there.
        sort(events.begin(),events.end());

        return solve(events, 0, 0, dp);
    }

    int solve(vector<vector<int>>&nums,int idx,int k,vector<vector<int>>&dp)
    {
        // Base case
        if(2 == k) {
            return 0;
        }

        if (idx == nums.size()) {
            return 0;
        }

        // Memoization check
        if (-1 == dp[idx][k]) {
            return dp[idx][k];
        }

        //Basically ending times of the events
        auto basic = vector<int>{nums[idx][1], INT_MAX, INT_MAX};

        //Searching the event whose starting time > ending time of previous event selected
        int nextindex = upper_bound(begin(nums), end(nums), basic) - begin(nums);

        //Pick event
        int include = nums[idx][2] + solve(nums, nextindex, k+1, dp);

        //Don't Pick event
        int exclude = solve(nums, idx+1, k, dp);

        return dp[idx][k] = max(include, exclude); //Max of(Pick, Not Pick)
    }
};

/**
 * Original solution. Brute force, code is simple but TLE of course.
 */
bool overlapp(vector<int> const& evt1, vector<int> const& evt2)
{
    return (evt1[0] <= evt2[0] && evt2[0] <= evt1[1])
    || (evt2[0] <= evt1[0] && evt1[0] <= evt2[1]);
}

int score(vector<int> const& evt1, vector<int> const& evt2)
{
    if (overlapp(evt1, evt2)) {
        return max(evt1[2], evt2[2]);
    }

    return evt1[2] + evt2[2];
}

class Solution {
public:
    int maxTwoEvents(vector<vector<int>>& events) {
        int ans = 0;

        int const size = events.size();
        for (int i = 0; i < size; ++i) {
            for (int j = i + 1; j < size; ++j) {
                ans = max(ans, score(events[i], events[j]));
            }
        }

        return ans;
    }
};
