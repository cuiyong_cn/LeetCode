/**
 * N-th Tribonacci Number
 *
 * The Tribonacci sequence Tn is defined as follows:
 *
 * T0 = 0, T1 = 1, T2 = 1, and Tn+3 = Tn + Tn+1 + Tn+2 for n >= 0.
 *
 * Given n, return the value of Tn.
 *
 * Example 1:
 *
 * Input: n = 4
 * Output: 4
 * Explanation:
 * T_3 = 0 + 1 + 1 = 2
 * T_4 = 1 + 1 + 2 = 4
 *
 * Example 2:
 *
 * Input: n = 25
 * Output: 1389537
 *
 * Constraints:
 *
 *     0 <= n <= 37
 *     The answer is guaranteed to fit within a 32-bit integer, ie. answer <= 2^31 - 1.
 */
/**
 * My favorite solution. And there exists another solution which use a formular.
 */
class Solution {
public:
    int tribonacci(int n) {
        int dp[3] = {0, 1, 1};
        for (int i = 3; i <= n; ++i) {
            dp[i%3] += dp[(i+1)%3] + dp[(i+2)%3];
        }
        return dp[n%3];
    }
};

/**
 * More fun solution using matrix.
 *
 * We have 3 equations:
 *        f(n)   = f(n-1) + f(n-2) + f(n-3)
 *        f(n-1) = f(n-1)
 *        f(n-2) =          f(n-2)
 *
 * By turning them into matrix realtion. we get:
 *        | f(n)   |     | 1 1 1 |     | f(n-1) |
 *        | f(n-1) |  =  | 1 0 0 |  *  | f(n-2) |
 *        | f(n-2) |     | 0 1 0 |     | f(n-3) |
 *
 * Since we can compute an matrix exponent by O(log(n)), Simplify the relation into exponents
 *        | f(n)   |     | 1 1 1 |^(n-2)     | f(2) |
 *        | f(n-1) |  =  | 1 0 0 |       *   | f(1) |
 *        | f(n-2) |     | 0 1 0 |           | f(0) |
 *
 *The matrix multiplication cost is k^3, k=3. So the total cost is O(k^3log(n))
 */
class Solution {
private:
    typedef vector<vector<int>> Matrix2D;

    Matrix2D matrixMul(const Matrix2D &A, const Matrix2D &B) {
        Matrix2D C(A.size(), vector<int>(B[0].size(), 0));

        for(int i=0; i<A.size(); ++i)
            for(int k=0; k<B.size(); ++k)
                for(int j=0; j<B[0].size();++j) {
                    C[i][j] += (A[i][k] * B[k][j]); 
                }
        return C;
    }

    Matrix2D matrixPow(const Matrix2D &A, int k) {
        if(k == 0) {
            Matrix2D C(A.size(), vector<int>(A.size(), 0));
            for(int i=0; i < A.size(); ++i) C[i][i] = 1;
            return C;
        }
        if(k == 1) return A;

        Matrix2D C = matrixPow(A, k/2);
        C = matrixMul(C, C);
        if(k%2 == 1) return matrixMul(C,A);
        return C;
    }

public:
    int tribonacci(int n) {
        if(n == 0) return 0;
        if(n==1 || n==2) return 1;

        int f_0 = 0;
        int f_1 = 1;
        int f_2 = 1;

        Matrix2D C = matrixPow({
            {1,1,1},
            {1,0,0},
            {0,1,0}
        }, n-2);
        return matrixMul(C, {
            {f_2},
            {f_1},
            {f_0}
        })[0][0];
    }
};

/**
 * My original simple solution.
 */
class Solution {
public:
    int tribonacci(int n) {
        vector<int> trib(38, 0);
        trib[1] = 1; trib[2] = 1;

        for (int i = 3; i <= n; ++i) {
            trib[i] = trib[i - 1] + trib[i - 2] + trib[i - 3];
        }
        return trib[n];
    }
};

/**
 * Another memory friendily approach.
 */
class Solution {
public:
    int tribonacci(int n) {
        if (n < 2) return n;
        int a = 0, b = 1, c = 1, d = a + b + c;
        while (n-- > 2) {
            d = a + b + c, a = b, b = c, c = d;
        }
        return c;
    }
};
