/** Ransom Note
 *
 * Given an arbitrary ransom note string and another string containing letters from all the magazines, write a function
 * that will return true if the ransom note can be constructed from the magazines ; otherwise, it will return false.
 *
 * Each letter in the magazine string can only be used once in your ransom note.
 *
 * Note: You may assume that both strings contain only lowercase letters.
 *
 * canConstruct("a", "b") -> false canConstruct("aa", "ab") -> false canConstruct("aa", "aab") -> true
 */
/**
 * original solution.
 */
class Solution {
public:
    bool canConstruct(string ransomNote, string magazine) {
        if (ransomNote.length() > magazine.length()) return false;

        vector<int> chars(26, 0);
        for (auto c : ransomNote) ++chars[c - 'a'];

        for (auto c : magazine) {
            if (chars[c - 'a'] > 0) {
                --chars[c - 'a'];
            }
        }

        bool ans = true;
        for (auto n : chars) {
            if (n > 0) {
                ans = false;
            }
        }
        return ans;
    }
};
