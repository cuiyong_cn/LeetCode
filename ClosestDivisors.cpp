/**
 * Closest Divisors
 *
 * Given an integer num, find the closest two integers in absolute difference whose product equals num + 1 or num + 2.
 *
 * Return the two integers in any order.
 *
 * Example 1:
 *
 * Input: num = 8 Output: [3,3] Explanation: For num + 1 = 9, the closest divisors are 3 & 3, for num + 2 = 10, the
 * closest divisors are 2 & 5, hence 3 & 3 is chosen.
 *
 * Example 2:
 *
 * Input: num = 123 Output: [5,25]
 *
 * Example 3:
 *
 * Input: num = 999 Output: [40,25]
 *
 * Constraints:
 *
 *     1 <= num <= 10^9
 */
/**
 * More short and efficient one. Mind blower
 * The idea behind this is.
 *          a * b = c * d  if a < c < d < b then b - a > d - c
 */
class Solution {
public:
    vector<int> closestDivisors(int x) {
        for (int a = sqrt(x + 2); a > 0; --a) 
            for(auto i: {x+1, x+2})
                if (i % a == 0) return {a, i / a};            
        return {};
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<int> closestDivisors(int num) {
        int num1 = num + 1;
        int num2 = num + 2;
        
        int n1 = sqrt(num1);
        if (n1 * n1 == num1) return {n1, n1};
        
        int n2 = sqrt(num2);
        if (n2 * n2 == num2) return {n2, n2};
        
        int diff = num;
        std::pair<int, int> ans{1, num1};
        for (int i = 2; i <= n1; ++i) {
            if (num1 % i == 0) {
                std::pair t{i, num1/i};
                int d = abs(t.first - t.second);
                if (d < diff) {
                    diff = d;
                    ans = t;
                }
            }
        }
        
        if (diff == 1) return {ans.first, ans.second};

        for (int i = 2; i <= n2; ++i) {
            if (num2 % i == 0) {
                std::pair t{i, num2/i};
                int d = abs(t.first - t.second);
                if (d < diff) {
                    diff = d;
                    ans = t;
                }
            }
        }
        
        return {ans.first, ans.second};
    }
};
