/**
 * Check If It Is a Straight Line
 *
 * You are given an array coordinates, coordinates[i] = [x, y], where [x, y] represents the
 * coordinate of a point. Check if these points make a straight line in the XY plane.
 *
 * Example 1:
 *
 * Input: coordinates = [[1,2],[2,3],[3,4],[4,5],[5,6],[6,7]]
 * Output: true
 * Example 2:
 *
 * Input: coordinates = [[1,1],[2,2],[3,4],[4,5],[5,6],[7,7]]
 * Output: false
 *
 * Constraints:
 *
 * 2 <= coordinates.length <= 1000
 * coordinates[i].length == 2
 * -10^4 <= coordinates[i][0], coordinates[i][1] <= 10^4
 * coordinates contains no duplicate point.
 */
/**
 * Actually, sorting is totally unnecessary.
 */
class Solution {
public:
    bool checkStraightLine(vector<vector<int>>& coordinates) {
        auto fx = coordinates[0][0], fy = coordinates[0][1];
        auto dx = coordinates[1][0] - fx, dy = coordinates[1][1] - fy;

        for (int i = 2; i < coordinates.size(); ++i) {
            auto ndx = coordinates[i][0] - fx, ndy = coordinates[i][1] - fy;
            if (dy * ndx != dx * ndy) {
                return false;
            }
        }

        return true;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool checkStraightLine(vector<vector<int>>& coordinates) {
        sort(coordinates.begin(), coordinates.end(),
             [](auto const& a, auto const& b) {
                 if (a[0] < b[0]) {
                     return true;
                 }
                 if (a[0] == b[0]) {
                     return a[1] < b[1];
                 }

                 return false;
             });

        auto dx = coordinates[1][0] - coordinates[0][0];
        auto dy = coordinates[1][1] - coordinates[0][1];

        for (int i = 2; i < coordinates.size(); ++i) {
            auto ndx = coordinates[i][0] - coordinates[i - 1][0];
            auto ndy = coordinates[i][1] - coordinates[i - 1][1];
            if (dy * ndx != dx * ndy) {
                return false;
            }
        }

        return true;
    }
};
