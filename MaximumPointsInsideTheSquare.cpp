/**
 * Maximum Points Inside the Square
 *
 * You are given a 2D array points and a string s where, points[i] represents the coordinates of
 * point i, and s[i] represents the tag of point i.
 *
 * A valid square is a square centered at the origin (0, 0), has edges parallel to the axes, and
 * does not contain two points with the same tag.
 *
 * Return the maximum number of points contained in a valid square.
 * Note:
 *     A point is considered to be inside the square if it lies on or within the square's boundaries.
 *     The side length of the square can be zero.
 *
 * Example 1:
 *
 * Input: points = [[2,2],[-1,-2],[-4,4],[-3,1],[3,-3]], s = "abdca"
 *
 * Output: 2
 *
 * Explanation:
 *
 * The square of side length 4 covers two points points[0] and points[1].
 *
 * Example 2:
 *
 * Input: points = [[1,1],[-2,-2],[-2,2]], s = "abb"
 *
 * Output: 1
 *
 * Explanation:
 *
 * The square of side length 2 covers one point, which is points[0].
 *
 * Example 3:
 *
 * Input: points = [[1,1],[-1,-1],[2,-2]], s = "ccd"
 *
 * Output: 0
 *
 * Explanation:
 *
 * It's impossible to make any valid squares centered at the origin such that it covers only one point among points[0] and points[1].
 *
 * Constraints:
 *     1 <= s.length, points.length <= 105
 *     points[i].length == 2
 *     -109 <= points[i][0], points[i][1] <= 109
 *     s.length == points.length
 *     points consists of distinct coordinates.
 *     s consists only of lowercase English letters.
 */
/**
 * Original solution
 */
class Solution {
public:
    int maxPointsInsideSquare(vector<vector<int>>& points, string s) {
        int const cnt = points.size();
        auto index = vector<int>(cnt, 0);

        std::ranges::iota(index, 0);

        std::ranges::sort(index, [&points](auto const& i, auto const& j) {
            auto ea = max(abs(points[i][0]), abs(points[i][1]));
            auto eb = max(abs(points[j][0]), abs(points[j][1]));

            return ea < eb;
        });

        auto seen = array<bool, 26>{ false, };

        for (auto p = 0; p < cnt;) {
            auto i = index[p];
            if (seen[s[i] - 'a']) {
                return p;
            }

            seen[s[i] - 'a'] = true;

            auto const expected = max(abs(points[i][0]), abs(points[i][1]));
            auto np = p + 1;

            for (; np < cnt; ++np) {
                auto j = index[np];
                auto edge_len = max(abs(points[j][0]), abs(points[j][1]));
                if (edge_len != expected) {
                    break;
                }

                auto ch = s[j] - 'a';
                if (seen[ch]) {
                    return p;
                }

                seen[ch] = true;
            }

            p = np;
        }

        return cnt;
    }
};
