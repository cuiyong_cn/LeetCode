/**
 * Day of the Week
 *
 * Given a date, return the corresponding day of the week for that date.
 *
 * The input is given as three integers representing the day, month and year respectively.
 *
 * Return the answer as one of the following values {"Sunday", "Monday", "Tuesday",
 * "Wednesday", "Thursday", "Friday", "Saturday"}.
 *
 * Example 1:
 *
 * Input: day = 31, month = 8, year = 2019
 * Output: "Saturday"
 *
 * Example 2:
 *
 * Input: day = 18, month = 7, year = 1999
 * Output: "Sunday"
 *
 * Example 3:
 *
 * Input: day = 15, month = 8, year = 1993
 * Output: "Sunday"
 *
 * Constraints:
 *
 *     The given dates are valid dates between the years 1971 and 2100.
 */
/**
 * Zellor's algorithm
 */
class Solution
{
public:
    vector<string> days = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday",
                           "Friday", "Saturday"
                          };
    string dayOfTheWeek(int d, int m, int y, int c = 0)
    {
        if (m < 3) m += 12, y -= 1;
        c = y / 100, y = y % 100;
        int w = (c / 4 - 2 * c + y + y / 4 + 13 * (m + 1) / 5 + d - 1) % 7;
        return days[(w + 7) % 7];
    }
};
/**
 * Another solution base on the today's day of week.
 */
class Solution
{
public:
    int daysOfMonth[2][12] = {
        31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31,
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    };
    string weekName[7] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
    inline bool isleapyear(int year)
    {
        return ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0));
    }

    int daystill1971(int year, int month, int day)
    {
        int count = 0;
        int year1 = 1970, month1 = 1, day1 = 1;
        while (year1 != year) {
            bool b = isleapyear(year1);
            if (b) count += 366;
            else count += 365;
            year1++;
        }
        int b = isleapyear(year1) ? 0 : 1;
        for (int i = 0; i < month - 1; i++) count += daysOfMonth[b][i];
        count += day - 1;
        return count;
    }

    string dayOfTheWeek(int day, int month, int year)
    {
        int days1 = daystill1971(2019, 9, 8);//today(2019,9,8) is Sunday
        int days2 = daystill1971(year, month, day);
        int days = (((days2 - days1) % 7) + 7) % 7;//Number of days off
        return weekName[days];
    }
};

