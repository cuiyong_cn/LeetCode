/**
 * Sqrt(x)
 *
 * Given a non-negative integer x, compute and return the square root of x.
 *
 * Since the return type is an integer, the decimal digits are truncated, and only the integer part
 * of the result is returned.
 *
 * Note: You are not allowed to use any built-in exponent function or operator, such as pow(x, 0.5)
 * or x ** 0.5.
 *
 * Example 1:
 *
 * Input: x = 4
 * Output: 2
 * Example 2:
 *
 * Input: x = 8
 * Output: 2
 * Explanation: The square root of 8 is 2.82842..., and since the decimal part is truncated, 2 is
 * returned.
 *
 * Constraints:
 *
 * 0 <= x <= 231 - 1
 **/
/**
 * Improve below solution. We don't need long long type.
 */
class Solution {
public:
    int mySqrt(int x) {
        if (0 == x) {
            return 0;
        }

        int left = 1, right = x;

        while (left <= right) {
            auto mid = left + (right - left) / 2;
            auto mid2 = x / mid;
            if (mid <= mid2) {
                left = mid + 1;
            }
            else {
                right = mid - 1;
            }
        }

        return left - 1;
    }
};

/**
 * Binary search.
 */
class Solution {
public:
    int mySqrt(int x) {
        long long left = 0, right = x;

        while (left <= right) {
            auto mid = left + (right - left) / 2;
            auto mid2 = mid * mid;
            if (mid2 <= x) {
                left =  mid + 1;
            }
            else {
                right = mid - 1;
            }
        }

        return left - 1;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int mySqrt(int x) {
        int ans = 0;
        for (long long i = 1; i <= x; ++i) {
            long long x2 = i * i;
            if (x2 > x) {
                break;
            }
            ans = i;
        }

        return ans;
    }
};
