/**
 * Flip Columns For Maximum Number of Equal Rows
 *
 * Given a matrix consisting of 0s and 1s, we may choose any number of columns in the
 * matrix and flip every cell in that column.  Flipping a cell changes the value of that
 * cell from 0 to 1 or from 1 to 0.
 *
 * Return the maximum number of rows that have all values equal after some number of flips.
 *
 * Example 1:
 *
 * Input: [[0,1],[1,1]]
 * Output: 1
 * Explanation: After flipping no values, 1 row has all values equal.
 *
 * Example 2:
 *
 * Input: [[0,1],[1,0]]
 * Output: 2
 * Explanation: After flipping values in the first column, both rows have equal values.
 *
 * Example 3:
 *
 * Input: [[0,0,0],[0,0,1],[1,1,0]]
 * Output: 2
 * Explanation: After flipping values in the first two columns, the last two rows have equal values.
 *
 * Note:
 *
 *     1 <= matrix.length <= 300
 *     1 <= matrix[i].length <= 300
 *     All matrix[i].length's are equal
 *     matrix[i][j] is 0 or 1
 */
/**
 * My original version, the middle part can be improved with some map approach.
 * The idea behind this is find the same or opposite columns, because they can be
 * same within some flips.
 */
class Solution {
public:
    int maxEqualRowsAfterFlips(vector<vector<int>>& matrix) {
        int ans = 0;
        int rows = matrix.size();
        int cols = matrix[0].size();
        vector<vector<int>> rmax(rows);
        for (int i = 0; i < rows; ++i) {
            for (int j = i + 1; j < rows; ++j) {
                bool sOrOp = true;
                int prev = matrix[i][0] ^ matrix[j][0];
                for (int k = 1; k < cols; ++k) {
                    if ((matrix[i][k] ^ matrix[j][k]) != prev) {
                        sOrOp = false;
                        break;
                    }
                }
                if (true == sOrOp) {
                    rmax[i].push_back(j);
                    rmax[j].push_back(i);
                }
            }
        }

        for (int i = 0; i < rows; ++i) {
            int size = rmax[i].size() + 1;
            ans = max(ans, size);
        }
        return ans;
    }
};

/**
 * More efficient version. We put the same or opposite row pairs continuesly.
 */
class Solution {
public:
    int maxEqualRowsAfterFlips(vector<vector<int>>& matrix) {
        int ans = 1;
        int rows = matrix.size();
        int cols = matrix[0].size();
        int i = 0;
        int left = rows;
        while (i < rows) {
            int count = 1;
            for (int j = i + 1; j < rows; ++j) {
                int sOrOp = true;
                int prev = matrix[i][0] ^ matrix[j][0];
                for (int k = 1; k < cols; ++k) {
                    if ((matrix[i][k] ^ matrix[j][k]) != prev) {
                        sOrOp = false;
                        break;
                    }
                }
                if (true == sOrOp) {
                    ++count;
                    swap(matrix[j], matrix[i + 1]);
                    ++i;
                }
            }
            ans = max(ans, count);
            left -= count;
            if (ans >= left) break;
            ++i;
        }

        return ans;
    }
};
