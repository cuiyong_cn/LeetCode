/**
 * Max Consecutive Ones
 *
 * Given a binary array, find the maximum number of consecutive 1s in this array.
 *
 * Example 1:
 *
 * Input: [1,1,0,1,1,1]
 * Output: 3
 * Explanation: The first two digits or the last three digits are consecutive 1s.
 *     The maximum number of consecutive 1s is 3.
 *
 * Note:
 *
 *     The input array will only contain 0 and 1.
 *     The length of input array is a positive integer and will not exceed 10,000
 */
/**
 * Original solution.
 */
class Solution {
public:
    int findMaxConsecutiveOnes(vector<int>& nums) {
        int ans = 0;
        int max = 0;
        for (auto& n : nums) {
            if (0 == n) {
                ans = std::max(max, ans);
                max = 0;
            }
            else {
                ++max;
            }
        }
        ans = std::max(max, ans);
        return ans;
    }
};
