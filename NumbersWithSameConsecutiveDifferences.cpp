/**
 * Numbers With Same Consecutive Differences
 *
 * Return all non-negative integers of length n such that the absolute difference between every two consecutive digits
 * is k.
 *
 * Note that every number in the answer must not have leading zeros. For example, 01 has one leading zero and is invalid.
 *
 * You may return the answer in any order.
 *
 * Example 1:
 *
 * Input: n = 3, k = 7
 * Output: [181,292,707,818,929]
 * Explanation: Note that 070 is not a valid number, because it has leading zeroes.
 * Example 2:
 *
 * Input: n = 2, k = 1
 * Output: [10,12,21,23,32,34,43,45,54,56,65,67,76,78,87,89,98]
 * Example 3:
 *
 * Input: n = 2, k = 0
 * Output: [11,22,33,44,55,66,77,88,99]
 * Example 4:
 *
 * Input: n = 2, k = 1
 * Output: [10,12,21,23,32,34,43,45,54,56,65,67,76,78,87,89,98]
 * Example 5:
 *
 * Input: n = 2, k = 2
 * Output: [13,20,24,31,35,42,46,53,57,64,68,75,79,86,97]
 *
 * Constraints:
 *
 * 2 <= n <= 9
 * 0 <= k <= 9
 */
/**
 * Of course we can do it using BFS.
 */
class Solution
{
public:
    vector<int> numsSameConsecDiff(int n, int k)
    {
        vector<int> cur = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        for (int i = 2; i <= n; ++i) {
            vector<int> cur2;
            for (auto x : cur) {
                int y = x % 10;
                if ((y + k) < 10) {
                    cur2.push_back(x * 10 + y + k);
                }

                if (k > 0 && (y - k) >= 0) {
                    cur2.push_back(x * 10 + y - k);
                }
            }

            std::swap(cur2, cur);
        }

        return cur;
    }
};

/**
 * Improve below code. We don't need the count argument.
 */
class Solution {
public:
    vector<int> numsSameConsecDiff(int n, int k) {
        for (int i = 1; i < 10; ++i) {
            dfs(i, n - 1, k);
        }

        return vector<int>(ans.begin(), ans.end());
    }

private:
    void dfs(int num, int n, int k)
    {
        if (0 == n) {
            ans.insert(num);
            return;
        }

        int last = num % 10;
        if ((last + k) < 10) {
            dfs(num * 10 + (last + k), n - 1, k);
        }

        if ((last - k) >= 0) {
            dfs(num * 10 + (last - k), n - 1, k);
        }
    }

    set<int> ans;
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<int> numsSameConsecDiff(int n, int k) {
        for (int i = 1; i < 10; ++i) {
            dfs(i, 1, n, k);
        }

        return vector<int>(ans.begin(), ans.end());
    }

private:
    void dfs(int num, int count, int n, int k)
    {
        if (count == n) {
            ans.insert(num);
            return;
        }

        int last = num % 10;
        if ((last + k) < 10) {
            dfs(num * 10 + (last + k), count + 1, n, k);
        }

        if ((last - k) >= 0) {
            dfs(num * 10 + (last - k), count + 1, n, k);
        }
    }

    set<int> ans;
};
