/**
 * Score After Flipping Matrix
 *
 * We have a two dimensional matrix A where each value is 0 or 1.
 *
 * A move consists of choosing any row or column, and toggling each value in
 * that row or column: changing all 0s to 1s, and all 1s to 0s.
 *
 * After making any number of moves, every row of this matrix is interpreted as
 * a binary number, and the score of the matrix is the sum of these numbers.
 *
 * Return the highest possible score.
 *
 * Example 1:
 *
 * Input: [[0,0,1,1],[1,0,1,0],[1,1,0,0]]
 * Output: 39
 * Explanation:
 * Toggled to [[1,1,1,1],[1,0,0,1],[1,1,1,1]].
 * 0b1111 + 0b1001 + 0b1111 = 15 + 9 + 15 = 39
 *
 * Note:
 *
 *  1  1 <= A.length <= 20
 *  2  1 <= A[0].length <= 20
 *  3  A[i][j] is 0 or 1.
 *
 */
/**
 * more simplified version
 *
 * 1. A[i][0] is worth 1 << (N - 1) points, more than the sum of
 * (A[i][1] + .. + A[i][N-1]).
 * 2. We need to toggle all A[i][0] to 1, here I toggle all lines for A[i][0] = 0.
 * 3. A[i][j] is worth 1 << (N - 1 - j)
 *
 * For every col, I count the current number of 1s.
 * After step 1, A[i][j] becomes 1 if A[i][j] == A[i][0].
 * if M - cur > cur, we can toggle this column to get more 1s.
 * max(M, M - cur) will be the maximum number of 1s that we can get.
 *  int matrixScore(vector<vector<int>> A) {
 *      int M = A.size(), N = A[0].size(), res = (1 << (N - 1)) * M;
 *      for (int j = 1; j < N; j++) {
 *          int cur = 0;
 *          for (int i = 0; i < M; i++) cur += A[i][j] == A[i][0];
 *          res += max(cur, M - cur) * (1 << (N - j - 1));
 *      }
 *      return res;
 *  }
 */
class Solution {
public:
    int matrixScore(vector<vector<int>>& A) {
        int height = A.size();
        int width = height > 0 ? A[0].size() : 0;
        /** first toggle the all A[i][0] to 1 */
        for (int h = 0; h < height; ++h) {
            if (A[h][0] == 0) {
                for (int w = 0; w < width; ++w) {
                    A[h][w] ^= 1;
                }
            }
        }

        /** for all col, if ones < zeros we toggle the column */
        for (int w = 1; w < width; ++w) {
            int ones = 0;
            for (int h = 0; h < height; ++h) {
                ones += A[h][w];
            }
            int zeros = height - ones;
            if (ones < zeros) {
                for (int h = 0; h < height; ++h) {
                    A[h][w] ^= 1;
                }
            }
        }

        /** cal the sum */
        int sum = 0;
        for (int h = 0; h < height; ++h) {
            int v = 0;
            for (int w = 0; w < width; ++w) {
                v = (v << 1) | A[h][w];
            }
            sum += v;
        }

        return sum;
    }
};
