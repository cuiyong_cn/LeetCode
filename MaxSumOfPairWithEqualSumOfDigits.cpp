/**
 * Max Sum of a Pair With Equal Sum of Digits
 *
 * You are given a 0-indexed array nums consisting of positive integers. You can choose two indices
 * i and j, such that i != j, and the sum of digits of the number nums[i] is equal to that of
 * nums[j].
 *
 * Return the maximum value of nums[i] + nums[j] that you can obtain over all possible indices i and
 * j that satisfy the conditions.
 *
 * Example 1:
 *
 * Input: nums = [18,43,36,13,7]
 * Output: 54
 * Explanation: The pairs (i, j) that satisfy the conditions are:
 * - (0, 2), both numbers have a sum of digits equal to 9, and their sum is 18 + 36 = 54.
 * - (1, 4), both numbers have a sum of digits equal to 7, and their sum is 43 + 7 = 50.
 * So the maximum sum that we can obtain is 54.
 * Example 2:
 *
 * Input: nums = [10,12,19,14]
 * Output: -1
 * Explanation: There are no two numbers that satisfy the conditions, so we return -1.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 105
 * 1 <= nums[i] <= 109
 */
/**
 * Optimize.
 */
auto sum_digits(int n)
{
    auto ans = 0;
    for (; n > 0; n /= 10) {
        ans += n % 10;
    }
    return ans;
}

class Solution {
public:
    int maximumSum(vector<int>& nums) {
        auto max_num_of_digits_sum = vector<int>(100, 0);
        auto ans = -1;
        for (auto n : nums) {
            auto sd = sum_digits(n);
            auto& max_num = max_num_of_digits_sum[sd];
            if (max_num > 0) {
                ans = max(ans, max_num + n);
            }
            max_num = max(max_num, n);
        }

        return ans;
    }
};

/**
 * Original solution.
 */
auto sum_digits(int n)
{
    auto ans = 0;
    for (; n > 0; n /= 10) {
        ans += n % 10;
    }
    return ans;
}

auto top2sum(vector<int> const& nums)
{
    auto t1 = 0, t2 = 0;
    for (auto n : nums) {
        if (n > t1) {
            t2 = t1;
            t1 = n;
        }
        else if (n > t2) {
            t2 = n;
        }
    }
    return t1 + t2;
}

class Solution {
public:
    int maximumSum(vector<int>& nums) {
        unordered_map<int, vector<int>> nums_same_sum(100);
        for (auto n : nums) {
            auto sd = sum_digits(n);
            nums_same_sum[sd].push_back(n);
        }

        auto ans = -1;
        for (auto const& [_, nums] : nums_same_sum) {
            if (nums.size() < 2) {
                continue;
            }
            ans = max(ans, top2sum(nums));
        }
        return ans;
    }
};
