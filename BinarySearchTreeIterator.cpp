/**
 * Binary Search Tree Iterator
 *
 * Implement an iterator over a binary search tree (BST). Your iterator will be initialized with the root node of a BST.
 *
 * Calling next() will return the next smallest number in the BST.
 *
 * Example:
 *
 * BSTIterator iterator = new BSTIterator(root);
 * iterator.next();    // return 3
 * iterator.next();    // return 7
 * iterator.hasNext(); // return true
 * iterator.next();    // return 9
 * iterator.hasNext(); // return true
 * iterator.next();    // return 15
 * iterator.hasNext(); // return true
 * iterator.next();    // return 20
 * iterator.hasNext(); // return false
 *
 * Note:
 *
 *     next() and hasNext() should run in average O(1) time and uses O(h) memory, where h is the height of the tree.
 *     You may assume that next() call will always be valid, that is, there will be at least a next smallest number in
 *     the BST when next() is called.
 */
/**
 * original solution. We have met this problem before.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class BSTIterator {
public:
    stack<TreeNode*> stk;

    BSTIterator(TreeNode* root) {
        pushAllLeft(root);
    }

    /** @return the next smallest number */
    int next() {
        TreeNode* cur = stk.top(); stk.pop();
        pushAllLeft(cur->right);

        return cur->val;
    }

    /** @return whether we have a next smallest number */
    bool hasNext() {
        return stk.empty() == false;
    }
private:
    void pushAllLeft(TreeNode* n) {
        while (nullptr != n) {
            stk.push(n);
            n = n->left;
        }
    }
};

/**
 * Your BSTIterator object will be instantiated and called as such:
 * BSTIterator* obj = new BSTIterator(root);
 * int param_1 = obj->next();
 * bool param_2 = obj->hasNext();
 */
