/**
 * Reformat The Strin
 *
 * You are given an alphanumeric string s. (Alphanumeric string is a string consisting of lowercase
 * English letters and digits).
 *
 * You have to find a permutation of the string where no letter is followed by another letter and no
 * digit is followed by another digit. That is, no two adjacent characters have the same type.
 *
 * Return the reformatted string or return an empty string if it is impossible to reformat the
 * string.
 *
 * Example 1:
 *
 * Input: s = "a0b1c2"
 * Output: "0a1b2c"
 * Explanation: No two adjacent characters have the same type in "0a1b2c". "a0b1c2", "0a1b2c",
 * "0c2a1b" are also valid permutations.
 * Example 2:
 *
 * Input: s = "leetcode"
 * Output: ""
 * Explanation: "leetcode" has only characters so we cannot separate them by digits.
 * Example 3:
 *
 * Input: s = "1229857369"
 * Output: ""
 * Explanation: "1229857369" has only digits so we cannot separate them by characters.
 *
 * Constraints:
 *
 * 1 <= s.length <= 500
 * s consists of only lowercase English letters and/or digits.
 */
/**
 * Original solution.
 */
class Solution {
public:
    string reformat(string s) {
        int letter_cnt = count_if(s.begin(), s.end(), ::isalpha);
        int digit_cnt = s.length() - letter_cnt;
        if (labs(letter_cnt - digit_cnt) > 1) {
            return "";
        }

        if (digit_cnt >= letter_cnt) {
            sort(s.begin(), s.end());
        }
        else {
            sort(s.begin(), s.end(), greater<int>());
        }

        int i = 1, big = max(digit_cnt, letter_cnt), j = big + (big & 1);
        while (j < s.length()) {
            swap(s[i], s[j]);
            i += 2;
            j += 2;
        }

        return s;
    }
};
