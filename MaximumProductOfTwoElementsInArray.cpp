/**
 * Maximum Product of Two Elements in an Array
 *
 * Given the array of integers nums, you will choose two different indices i and j of that array.
 * Return the maximum value of (nums[i]-1)*(nums[j]-1).
 *
 * Example 1:
 *
 * Input: nums = [3,4,5,2]
 * Output: 12
 * Explanation: If you choose the indices i=1 and j=2 (indexed from 0), you will get the maximum value, that is, (nums[1]-1)*(nums[2]-1) = (4-1)*(5-1) = 3*4 = 12.
 * Example 2:
 *
 * Input: nums = [1,5,4,5]
 * Output: 16
 * Explanation: Choosing the indices i=1 and j=3 (indexed from 0), you will get the maximum value of (5-1)*(5-1) = 16.
 * Example 3:
 *
 * Input: nums = [3,7]
 * Output: 12
 *
 * Constraints:
 *
 * 2 <= nums.length <= 500
 * 1 <= nums[i] <= 10^3
 */
/**
 * A little longer. O(n) time solution.
 */
class Solution {
public:
    int maxProduct(vector<int>& nums) {
        int max_a = 0, max_b = 0;
        for (auto n : nums) {
            if (n >= max_a) {
                max_b = max_a;
                max_a = n;
            }
            else if (n > max_b) {
                max_b = n;
            }
        }

        return (max_a - 1) * (max_b - 1);
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int maxProduct(vector<int>& nums) {
        sort(nums.begin(), nums.end());
        
        const int N = nums.size();
        return (nums[N - 1] - 1) * (nums[N - 2] - 1);
    }
};
