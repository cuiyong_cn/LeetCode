/**
 * Spiral Matrix II
 *
 * Given a positive integer n, generate a square matrix filled with elements from 1 to n2 in spiral order.
 *
 * Example:
 *
 * Input: 3
 * Output:
 * [
 *  [ 1, 2, 3 ],
 *  [ 8, 9, 4 ],
 *  [ 7, 6, 5 ]
 * ]
 */
/**
 * Original solution. Basic idea is:
 *
 *  -------------------
 *  |                 |
 *  |  -------------  |
 *  |  |  -------  |  |
 *  |  |  |     |  |  |
 *  |  |  -------  |  |
 *  |  -------------  |
 *  |                 |
 *  -------------------
 *
 *  fill the matrix from outside into inside square
 */
class Solution {
public:
    vector<vector<int>> generateMatrix(int n) {
        vector<vector<int>> ans(n, vector<int>(n, 0));
        int num = 1;
        int squares = n / 2;
        for (int i = 0; i <= squares; ++i) {
            for (int j = i; j < (n - i - 1); ++j) {
                ans[i][j] = num++;
            }
            for (int h = i; h < (n - i - 1); ++h) {
                ans[h][n - i - 1] = num++;
            }
            for (int k = n - i - 1; k > i; --k) {
                ans[n - i - 1][k] = num++;
            }
            for (int m = n - i - 1; m > i; --m) {
                ans[m][i] = num++;
            }
        }

        if (n % 2) ans[squares][squares] = num;

        return ans;
    }
};

