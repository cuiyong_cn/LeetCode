/**
 * Count Numbers with Unique Digits
 *
 * Given a non-negative integer n, count all numbers with unique digits, x, where 0 ≤ x < 10n.
 *
 * Example:
 *
 * Input: 2
 * Output: 91
 * Explanation: The answer should be the total numbers in the range of 0 ≤ x < 100,
 *              excluding 11,22,33,44,55,66,77,88,99
 */
/**
 * Original solution. This is math combination problem.
 */
class Solution {
public:
    int countNumbersWithUniqueDigits(int n) {
        auto ans = 0 == n ? 1 : 10;
        auto cand = 9;
        auto prev_comb = 9;

        for (int i = 2; i <= n; ++i) {
            auto cur_comb = prev_comb * cand;
            ans +=  cur_comb;
            prev_comb = cur_comb;
            --cand;
        }

        return ans;
    }
};
