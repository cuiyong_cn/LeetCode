/**
 * Maximum Depth of N-ary Tree
 *
 * Given a n-ary tree, find its maximum depth.
 *
 * The maximum depth is the number of nodes along the longest path from the
 * root node down to the farthest leaf node.
 *
 * For example, given a 3-ary tree:
 *                      1
 *                     /|\
 *                    / | \
 *                   3  2  4
 *                  / \
 *                 5   6
 *
 * We should return its max depth, which is 3.
 *
 * Note:
 *     The depth of the tree is at most 1000.
 *     The total number of nodes is at most 5000.
 */
/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/
/**
 * DFS Recursive method.
 */
class Solution {
public:
    int maxDepth(Node* root) {
        if (!root) return 0;
        int max_depth = 0;
        for (auto n : root->children) {
            int depth = maxDepth(n);
            max_depth = max(max_depth, depth);
        }
        return 1 + max_depth;
    }
};
/**
 * BFS method
 */
class Solution {
public:
    int maxDepth(Node* root) {
        if (root == nullptr) return 0;
        queue<Node*> q; q.push(root);
        int depth = 0;
        while (!q.empty()) {
            depth += 1;
            int breadth = q.size();
            for (int _ = 0; _ < breadth; ++_) {
                auto node = q.front(); q.pop();
                for (auto child : node->children) if (child) q.push(child);
            }
        }
        return depth;
    }
};
