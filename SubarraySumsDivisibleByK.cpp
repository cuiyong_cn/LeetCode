/**
 * Subarray Sums Divisible by K
 *
 * Given an array A of integers, return the number of (contiguous, non-empty) subarrays that have a sum divisible by K.
 *
 * Example 1:
 *
 * Input: A = [4,5,0,-2,-3,1], K = 5
 * Output: 7
 * Explanation: There are 7 subarrays with a sum divisible by K = 5:
 * [4, 5, 0, -2, -3, 1], [5], [5, 0], [5, 0, -2, -3], [0], [0, -2, -3], [-2, -3]
 *
 * Note:
 *     1 <= A.length <= 30000
 *     -10000 <= A[i] <= 10000
 *     2 <= K <= 10000
 */
/**
 * A more clear one. The idea is:
 * sum[0, i] % K == sum[0, j] % K, sum[i + 1, j] is divisible by by K.
 * Then it's a combination problem.
 */
class Solution {
public:
    int subarraysDivByK(vector<int>& A, int K) {
        vector<int> count(K, 0);
        partial_sum(A.begin(), A.end(), A.begin());

        count[0] = 1;
        for (auto s : A) {
            ++count[(s % K + K) % K];
        }

        for_each(count.begin(), count.end(), [](auto& n) { n = n * (n - 1) / 2; });
        return accumulate(count.begin(), count.end(), 0);
    }
};

class Solution {
public:
    int subarraysDivByK(vector<int>& A, int K) {
        int ans = 0;
        vector<int> count(K, 0);
        partial_sum(A.begin(), A.end(), A.begin());

        count[0] = 1;
        for (auto s : A) {
            ++count[(s % K + K) % K];
        }

        for_each(count.begin(), count.end(), [&ans](auto n) { ans += n * (n - 1) / 2; });
        return ans;
    }
};

/**
 * Improve the following solution.
 */
class Solution {
public:
    int subarraysDivByK(vector<int>& A, int K) {
        vector<int> count(K);

        count[0] = 1;
        int prefix = 0, res = 0;
        for (int a : A) {
            prefix = (prefix + a % K + K) % K;
            res += count[prefix]++;
        }

        return res;
    }
};

/**
 * Original solution. But TLE. Time(O(n^2))
 */
class Solution {
public:
    int subarraysDivByK(vector<int>& A, int K) {
        int ans = 0;
        vector<int> pre_sum(A.size() + 1, 0);
        partial_sum(A.begin(), A.end(), pre_sum.begin() + 1);

        for (int d = 1; d <= A.size(); ++d) {
            for (int i = 0; (i + d) < pre_sum.size(); ++i) {
                int sum = pre_sum[i + d] - pre_sum[i];
                if (sum % K == 0) {
                    ++ans;
                }
            }
        }

        return ans;
    }
};
