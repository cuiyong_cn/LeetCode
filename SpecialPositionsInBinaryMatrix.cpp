/**
 * Special Positions in a Binary Matrix
 *
 * Given an m x n binary matrix mat, return the number of special positions in mat.
 *
 * A position (i, j) is called special if mat[i][j] == 1 and all other elements in row i and column
 * j are 0 (rows and columns are 0-indexed).
 *
 * Example 1:
 *
 * Input: mat = [[1,0,0],[0,0,1],[1,0,0]]
 * Output: 1
 * Explanation: (1, 2) is a special position because mat[1][2] == 1 and all other elements in row 1 and column 2 are 0.
 * Example 2:
 *
 * Input: mat = [[1,0,0],[0,1,0],[0,0,1]]
 * Output: 3
 * Explanation: (0, 0), (1, 1) and (2, 2) are special positions.
 *
 * Constraints:
 *
 * m == mat.length
 * n == mat[i].length
 * 1 <= m, n <= 100
 * mat[i][j] is either 0 or 1.
 */
/**
 * A little shorter.
 */
class Solution {
public:
    int numSpecial(vector<vector<int>>& mat) {
        int const M = mat.size();
        int const N = mat[0].size();
        vector<int> one_cnt_row(M, 0);
        vector<int> one_cnt_col(N, 0);

        for (int i = 0; i < M; ++i) {
            for (int j = 0; j < N; ++j) {
                if (1 == mat[i][j]) {
                    ++one_cnt_row[i];
                    ++one_cnt_col[j];
                }
            }
        }

        int ans = 0;
        for (int i = 0; i < M; ++i) {
            if (one_cnt_row[i] == 1) {
                for (int j = 0; j < N; ++j) {
                    if (1 == mat[i][j] && 1 == one_cnt_col[j]) {
                        ++ans;
                    }
                }
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int numSpecial(vector<vector<int>>& mat) {
        int const M = mat.size();
        int const N = mat[0].size();
        vector<int> one_cnt_row(M, 0);
        vector<int> one_cnt_col(N, 0);

        for (int i = 0; i < M; ++i) {
            one_cnt_row[i] = count(mat[i].begin(), mat[i].end(), 1);
        }

        for (int i = 0; i < N; ++i) {
            int one_cnt = 0;
            for (int j = 0; j < M; ++j) {
                if (1 == mat[j][i]) {
                    ++one_cnt;
                }
            }
            one_cnt_col[i] = one_cnt;
        }

        int ans = 0;
        for (int i = 0; i < M; ++i) {
            if (one_cnt_row[i] == 1) {
                for (int j = 0; j < N; ++j) {
                    if (1 == mat[i][j] && 1 == one_cnt_col[j]) {
                        ++ans;
                    }
                }
            }
        }

        return ans;
    }
};
