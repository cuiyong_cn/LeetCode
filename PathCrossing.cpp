/**
 * Path Crossing
 *
 * Given a string path, where path[i] = 'N', 'S', 'E' or 'W', each representing moving one unit
 * north, south, east, or west, respectively. You start at the origin (0, 0) on a 2D plane and walk
 * on the path specified by path.
 *
 * Return true if the path crosses itself at any point, that is, if at any time you are on a
 * location you have previously visited. Return false otherwise.
 *
 * Example 1:
 *
 * Input: path = "NES"
 * Output: false
 * Explanation: Notice that the path doesn't cross any point more than once.
 * Example 2:
 *
 * Input: path = "NESWW"
 * Output: true
 * Explanation: Notice that the path visits the origin twice.
 *
 * Constraints:
 *
 * 1 <= path.length <= 104
 * path[i] is either 'N', 'S', 'E', or 'W'.
 */
/**
 * Shorten below solution.
 */
class Solution {
public:
    bool isPathCrossing(string path) {
        int x = 10000, y = 10000;
        unordered_set<int> positions{(x << 14) | y};
        unordered_map<char, pair<int, int>> delta{ {'N', {0, 1}}, {'S', {0, -1}}, {'E', {1, 0}}, {'W', {-1, 0}} };
        for (auto d : path) {
            auto [dx, dy] = delta[d];
            x += dx; y += dy;

            int p = (x << 14) | y;
            if (positions.count(p)) {
                return true;
            }
            positions.insert(p);
        }

        return false;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool isPathCrossing(string path) {
        int x = 10000, y = 10000;
        unordered_set<int> positions{(x << 14) | y};
        for (auto d : path) {
            if ('N' == d) {
                ++y;
            }
            else if ('S' == d) {
                --y;
            }
            else if ('E' == d) {
                ++x;
            }
            else {
                --x;
            }

            int p = (x << 14) | y;
            if (positions.count(p)) {
                return true;
            }
            positions.insert(p);
        }

        return false;
    }
};
