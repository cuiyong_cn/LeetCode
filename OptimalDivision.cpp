/**
 * Optimal Division
 *
 * Given a list of positive integers, the adjacent integers will perform the float division. For example,
 * [2,3,4] -> 2 / 3 / 4.
 *
 * However, you can add any number of parenthesis at any position to change the priority of operations. You should find
 * out how to add parenthesis to get the maximum result, and return the corresponding expression in string format.
 * Your expression should NOT contain redundant parenthesis.
 *
 * Example:
 *
 * Input: [1000,100,10,2]
 * Output: "1000/(100/10/2)"
 * Explanation:
 * 1000/(100/10/2) = 1000/((100/10)/2) = 200
 * However, the bold parenthesis in "1000/((100/10)/2)" are redundant,
 * since they don't influence the operation priority. So you should return "1000/(100/10/2)".
 *
 * Other cases:
 * 1000/(100/10)/2 = 50
 * 1000/(100/(10/2)) = 50
 * 1000/100/10/2 = 0.5
 * 1000/100/(10/2) = 2
 *
 * Note:
 *
 *     The length of the input array is [1, 10].
 *     Elements in the given array will be in range [2, 1000].
 *     There is only one optimal division for each test case.
 */
/**
 * Math solution.
 */
class Solution {
public:
    string optimalDivision(vector<int>& nums) {
        stringstream ss;
        if (nums.size() == 1) {
            ss << nums[0];
        }
        else if (nums.size() == 2) {
            ss << nums[0] << '/' << nums[1];
        }
        else {
            ss << nums[0] << "/(";
            for (int i = 1; i < nums.size() - 1; ++i) {
                ss << nums[i] << "/";
            }
            ss << nums[nums.size() - 1] << ')';
        }
        return ss.str();
    }
};

/**
 * Brute force solution. But using memorization to speed it up.
 */
class Solution {
private:
    struct T
    {
        float max_val;
        float min_val;
        string max_str;
        string min_str;
    };

public:
    string optimalDivision(vector<int>& nums) {
        vector<vector<shared_ptr<T> > > memo(nums.size(), vector<shared_ptr<T> >(nums.size()));
        T* t = optimal(nums, 0, nums.size() - 1, memo);
        return t->max_str;
    }
private:
    T* optimal(vector<int>& nums, int start, int end, vector<vector<shared_ptr<T> > >& memo)
    {
        if (nullptr != memo[start][end]) {
            return memo[start][end].get();
        }
        T* t = new T;
        if (start == end) {
            t->max_val = t->min_val = nums[start];
            t->max_str = t->min_str = to_string(nums[start]);
            memo[start][end] = shared_ptr<T>(t);
            return t;
        }
        t->max_val = numeric_limits<float>::min();
        t->min_val = numeric_limits<float>::max();
        t->max_str = t->min_str = "";
        for (int i = start; i < end; ++i) {
            T* left = optimal(nums, start, i, memo);
            T* right = optimal(nums, i + 1, end, memo);
            if (t->min_val > left->min_val / right->max_val) {
                t->min_val = left->min_val / right->max_val;
                t->min_str = left->min_str + "/" + (i + 1 != end ? "(" : "") + right->max_str + (i + 1 != end ? ")" : "");
            }
            if (t->max_val < left->max_val / right->min_val) {
                t->max_val = left->max_val / right->min_val;
                t->max_str = left->max_str + "/" + (i + 1 != end ? "(" : "") + right->min_str + (i + 1 != end ? ")" : "");
            }
        }
        memo[start][end] = shared_ptr<T>(t);
        return t;
    }
};

/**
 * Brute force solution. Directly copied from answers. Not very efficient but intuitive.
 */
class Solution {
private:
    struct T
    {
        float max_val;
        float min_val;
        string max_str;
        string min_str;
    };
public:
    string optimalDivision(vector<int>& nums) {
        T t = optimal(nums, 0, nums.size() - 1);
        return t.max_str;
    }
private:
    T optimal(vector<int>& nums, int start, int end)
    {
        T t;
        if (start == end) {
            t.max_val = t.min_val = nums[start];
            t.max_str = t.min_str = to_string(nums[start]);
            return t;
        }
        t.max_val = numeric_limits<float>::min();
        t.min_val = numeric_limits<float>::max();
        t.max_str = t.min_str = "";
        for (int i = start; i < end; ++i) {
            T left = optimal(nums, start, i);
            T right = optimal(nums, i + 1, end);
            if (t.min_val > left.min_val / right.max_val) {
                t.min_val = left.min_val / right.max_val;
                t.min_str = left.min_str + "/" + (i + 1 != end ? "(" : "") + right.max_str + (i + 1 != end ? ")" : "");
            }
            if (t.max_val < left.max_val / right.min_val) {
                t.max_val = left.max_val / right.min_val;
                t.max_str = left.max_str + "/" + (i + 1 != end ? "(" : "") + right.min_str + (i + 1 != end ? ")" : "");
            }
        }
        return t;
    }
};
