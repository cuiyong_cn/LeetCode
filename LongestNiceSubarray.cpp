/**
 * Longest Nice Subarray
 *
 * You are given an array nums consisting of positive integers.
 *
 * We call a subarray of nums nice if the bitwise AND of every pair of elements that are in
 * different positions in the subarray is equal to 0.
 *
 * Return the length of the longest nice subarray.
 *
 * A subarray is a contiguous part of an array.
 *
 * Note that subarrays of length 1 are always considered nice.
 *
 * Example 1:
 *
 * Input: nums = [1,3,8,48,10]
 * Output: 3
 * Explanation: The longest nice subarray is [3,8,48]. This subarray satisfies the conditions:
 * - 3 AND 8 = 0.
 * - 3 AND 48 = 0.
 * - 8 AND 48 = 0.
 * It can be proven that no longer nice subarray can be obtained, so we return 3.
 * Example 2:
 *
 * Input: nums = [3,1,5,11,13]
 * Output: 1
 * Explanation: The length of the longest nice subarray is 1. Any subarray of length 1 can be chosen.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 105
 * 1 <= nums[i] <= 109
 */
/**
 * This is the optimiztion.
 */
class Solution {
public:
    int longestNiceSubarray(vector<int>& nums) {
        int used = 0, j = 0, res = 0;
        for (int i = 0; i < nums.size(); ++i) {
            while ((used & nums[i]) != 0)
                used ^= nums[j++];
            used |= nums[i];
            res = max(res, i - j + 1);
        }
        return res;
    }
};

/**
 * I thought it as a optimization. But it actually got worse.
 */
template<typename Func>
void bit_cnt_update(array<int, 30>& bit_cnt, int num, Func f)
{
    for (auto i = size_t{0}; i < bit_cnt.size(); ++i) {
        if (num & (1 << i)) {
            bit_cnt[i] = f(bit_cnt[i]);
        }
    }
}

class Solution {
public:
    int longestNiceSubarray(vector<int>& nums) {
        auto nsize = nums.size();
        auto bit_cnt = array<int, 30>{ 0, };

        auto ans = min<int>(30, nsize);
        for (; ans > 1; --ans) {
            bit_cnt.fill(0);
            for (auto i = size_t{0}; i < nsize; ++i) {
                bit_cnt_update(bit_cnt, nums[i], [](int n) { return ++n; });
                if ((i + 1) >= ans) {
                    if (i >= ans) {
                        bit_cnt_update(bit_cnt, nums[i - ans], [](int n) { return --n; });
                    }
                    if (all_of(begin(bit_cnt), end(bit_cnt),
                        [](int n) { return n < 2; })) {
                        return ans;
                    }
                }
            }
        }

        return 1;
    }
};

/**
 * Prefix solution.
 */
class Solution {
public:
    int longestNiceSubarray(vector<int>& nums) {
        auto nsize = nums.size();
        auto bit_cnt_prefix = vector<vector<int>>(30, vector<int>(nsize + 1, 0));
        for (auto i = size_t{0}; i < nsize; ++i) {
            for (auto j = size_t{0}; j < 30U; ++j) {
                bit_cnt_prefix[j][i + 1] = bit_cnt_prefix[j][i];
            }
            for (auto k = size_t{0}; k < 30U; ++k) {
                if (nums[i] & (1 << k)) {
                    ++bit_cnt_prefix[k][i + 1];
                }
            }
        }

        auto ans = min<int>(30, nsize);
        for (; ans > 1; --ans) {
            for (auto i = size_t{0}; (i + ans) <= nsize; ++i) {
                auto nice = true;
                for (auto k = size_t{0}; k < 30U; ++k) {
                    auto bit_cnt = bit_cnt_prefix[k][i + ans] - bit_cnt_prefix[k][i];
                    if (bit_cnt > 1) {
                        nice = false;
                        break;
                    }
                }
                if (nice) {
                    return ans;
                }
            }
        }

        return 1;
    }
};
