/**
 * Find the Pivot Integer
 *
 * Given a positive integer n, find the pivot integer x such that:
 *
 * The sum of all elements between 1 and x inclusively equals the sum of all elements between x and n inclusively.
 * Return the pivot integer x. If no such integer exists, return -1. It is
 * guaranteed that there will be at most one pivot index for the given input.
 *
 * Example 1:
 *
 * Input: n = 8
 * Output: 6
 * Explanation: 6 is the pivot integer since: 1 + 2 + 3 + 4 + 5 + 6 = 6 + 7 + 8 = 21.
 * Example 2:
 *
 * Input: n = 1
 * Output: 1
 * Explanation: 1 is the pivot integer since: 1 = 1.
 * Example 3:
 *
 * Input: n = 4
 * Output: -1
 * Explanation: It can be proved that no such integer exist.
 *
 * Constraints:
 *
 * 1 <= n <= 1000
 */
/**
 * O(1) solution.
 *   assume the pivot number is x.
 *   sum of [1..x] = x * (x + 1) / 2
 *   sum of [x..n] = n * (n + 1) / 2 - x * (x + 1) / 2 + x
 *   so x * x = n * (n + 1) / 2
 */
class Solution {
public:
    int pivotInteger(int n) {
        auto const sum = n * (n + 1) / 2;
        auto x = static_cast<int>(sqrt(sum));
        return (x * x) == sum ? x : -1;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int pivotInteger(int n) {
        auto const sum = n * (n + 1) / 2;
        for (auto i = 1, acc = 0; i <= n; ++i) {
            acc += i;
            if ((sum - acc + i) == acc) {
                return i;
            }
        }
        return -1;
    }
};
