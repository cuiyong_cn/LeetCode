/**
 * Surrounded Regions
 *
 * Given an m x n matrix board containing 'X' and 'O', capture all regions that are 4-directionally
 * surrounded by 'X'.
 *
 * A region is captured by flipping all 'O's into 'X's in that surrounded region.
 *
 * Example 1:
 *
 * Input: board = [["X","X","X","X"],["X","O","O","X"],["X","X","O","X"],["X","O","X","X"]]
 * Output: [["X","X","X","X"],["X","X","X","X"],["X","X","X","X"],["X","O","X","X"]]
 * Explanation: Surrounded regions should not be on the border, which means that any 'O' on the
 * border of the board are not flipped to 'X'. Any 'O' that is not on the border and it is not
 * connected to an 'O' on the border will be flipped to 'X'. Two cells are connected if they are
 * adjacent cells connected horizontally or vertically.
 *
 * Example 2:
 *
 * Input: board = [["X"]]
 * Output: [["X"]]
 *
 * Constraints:
 *
 * m == board.length
 * n == board[i].length
 * 1 <= m, n <= 200
 * board[i][j] is 'X' or 'O'.
 */
/**
 * We don't need the visited vector actually.
 */
class Solution {
public:
    void solve(vector<vector<char>>& board) {
        // find all 'O' on the edge
        int M = board.size();
        int N = board.front().size();

        vector<pair<int, int>> q;
        for (int c = 0; c < N; ++c) {
            if ('O' == board[0][c]) {
                q.emplace_back(0, c);
            }
            if ('O' == board[M - 1][c]) {
                q.emplace_back(M - 1, c);
            }
        }

        for (int r = 1; r < M; ++r) {
            if ('O' == board[r][0]) {
                q.emplace_back(r, 0);
            }
            if ('O' == board[r][N - 1]) {
                q.emplace_back(r, N - 1);
            }
        }

        while (!q.empty()) {
            auto [r, c] = q.back(); q.pop_back();
            if ('#' == board[r][c]) {
                continue;
            }
            board[r][c] = '#';

            if (c > 0 && 'O' == board[r][c - 1]) {
                q.emplace_back(r, c - 1);
            }
            if (c < (N - 1) && 'O' == board[r][c + 1]) {
                q.emplace_back(r, c + 1);
            }
            if (r > 0 && 'O' == board[r - 1][c]) {
                q.emplace_back(r - 1, c);
            }
            if (r < (M - 1) && 'O' == board[r + 1][c]) {
                q.emplace_back(r + 1, c);
            }
        }

        for (int r = 0; r < M; ++r) {
            for (int c = 0; c < N; ++c) {
                if ('O' == board[r][c]) {
                    board[r][c] = 'X';
                }
                else if ('#' == board[r][c]) {
                    board[r][c] = 'O';
                }
            }
        }
    }
};

/**
 * Original solution. Here is the idea.
 *
 * All 'O' expanded from the edge can not be surround. So we marks these block.
 * and flip the other 'O' and we are done.
 */
class Solution {
public:
    void solve(vector<vector<char>>& board) {
        // find all 'O' on the edge
        int M = board.size();
        int N = board.front().size();

        vector<vector<bool>> visited(M, vector<bool>(N, false));

        vector<pair<int, int>> q;
        for (int c = 0; c < N; ++c) {
            if ('O' == board[0][c]) {
                q.emplace_back(0, c);
            }
            if ('O' == board[M - 1][c]) {
                q.emplace_back(M - 1, c);
            }
        }

        for (int r = 1; r < M; ++r) {
            if ('O' == board[r][0]) {
                q.emplace_back(r, 0);
            }
            if ('O' == board[r][N - 1]) {
                q.emplace_back(r, N - 1);
            }
        }

        // mark the 'O' expanded from the edge
        while (!q.empty()) {
            auto [r, c] = q.back(); q.pop_back();
            if (visited[r][c]) {
                continue;
            }
            visited[r][c] = true;

            if (c > 0 && 'O' == board[r][c - 1]) {
                q.emplace_back(r, c - 1);
            }
            if (c < (N - 1) && 'O' == board[r][c + 1]) {
                q.emplace_back(r, c + 1);
            }
            if (r > 0 && 'O' == board[r - 1][c]) {
                q.emplace_back(r - 1, c);
            }
            if (r < (M - 1) && 'O' == board[r + 1][c]) {
                q.emplace_back(r + 1, c);
            }
        }

        // flip the other 'O'
        for (int r = 0; r < M; ++r) {
            for (int c = 0; c < N; ++c) {
                if ('O' == board[r][c] && !visited[r][c]) {
                    board[r][c] = 'X';
                }
            }
        }
    }
};
