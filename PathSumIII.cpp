/**
 * Path Sum III
 *
 * You are given a binary tree in which each node contains an integer value.
 * Find the number of paths that sum to a given value.
 *
 * The path does not need to start or end at the root or a leaf, but it must go downwards (traveling only from parent
 * nodes to child nodes).
 * The tree has no more than 1,000 nodes and the values are in the range -1,000,000 to 1,000,000.
 *
 * Example:
 *
 * root = [10,5,-3,3,2,null,11,3,-2,null,1], sum = 8
 *
 *       10
 *      /  \
 *     5   -3
 *    / \    \
 *   3   2   11
 *  / \   \
 * 3  -2   1
 *
 * Return 3. The paths that sum to 8 are:
 *
 * 1.  5 -> 3
 * 2.  5 -> 2 -> 1
 * 3. -3 -> 11
 */
/**
 * Improved one. use a hashmap to avoid the recalculate the prefix sum.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int pathSum(TreeNode* root, int sum) {
        prefix_sum[0] = 1;

        return dfs(root, 0, sum);
    }

private:
    int dfs(TreeNode* node, int cur_sum, int target)
    {
        if (!node) {
            return 0;
        }

        cur_sum += node->val;

        auto count = prefix_sum_eq(cur_sum - target);
        prefix_sum[cur_sum] += 1;

        count += dfs(node->left, cur_sum, target) + dfs(node->right, cur_sum, target);

        prefix_sum[cur_sum] -= 1;

        return count;
    }

    int prefix_sum_eq(int target)
    {
        int count = 0;

        auto it = prefix_sum.find(target);
        if (it != prefix_sum.end()) {
            count = it->second;
        }

        return count;
    }

    unordered_map<int, int> prefix_sum;
};

/**
 * Original solution.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int pathSum(TreeNode* root, int sum) {
        return dfs(root, sum, sum);
    }

private:
    int dfs(TreeNode* node, int left, int sum)
    {
        if (!node) {
            return 0;
        }

        int ans = 0;
        if (left == node->val) {
            ans += 1;
        }

        left = left - node->val;

        ans += dfs(node->left, left, sum);
        ans += dfs(node->right, left, sum);

        ans += dfs2(node->left, sum, sum);
        ans += dfs2(node->right, sum, sum);

        return ans;
    }

    int dfs2(TreeNode* node, int left, int sum)
    {
        if (!node) {
            return 0;
        }

        int ans = 0;
        if (left == node->val) {
            ans += 1;
        }

        left = left - node->val;

        ans += dfs2(node->left, left, sum);
        ans += dfs2(node->right, left, sum);

        return ans;
    }
};
