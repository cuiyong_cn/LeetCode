/**
 * Maximum Swap
 *
 * Given a non-negative integer, you could swap two digits at most once to get the maximum valued number. Return the
 * maximum valued number you could get.
 *
 * Example 1:
 * Input: 2736
 * Output: 7236
 * Explanation: Swap the number 2 and the number 7.
 * Example 2:
 * Input: 9973
 * Output: 9973
 * Explanation: No swap.
 * Note:
 * The given number is in the range [0, 108]
 */
/**
 * Bounded solution. Written in Java
 */
class Solution {
    public int maximumSwap(int num) {
        char[] A = Integer.toString(num).toCharArray();
        int[] last = new int[10];
        for (int i = 0; i < A.length; i++) {
            last[A[i] - '0'] = i;
        }

        for (int i = 0; i < A.length; i++) {
            for (int d = 9; d > A[i] - '0'; d--) {
                if (last[d] > i) {
                    char tmp = A[i];
                    A[i] = A[last[d]];
                    A[last[d]] = tmp;
                    return Integer.valueOf(new String(A));
                }
            }
        }
        return num;
    }
}

/**
 * Same as below without using string.
 */
class Solution {
public:
    int maximumSwap(int num) {
        int base = 1, base2 = 1, digit2 = -1, n0 = num;
        int delta = 0, b1 = -1, b2 = -1; // optimal swap if delta >0
        while (num) {
            int digit = num % 10;
            if (digit > digit2) {
                digit2 = digit; // could be a better candidate once we find a digit
                                // which is less than digit2
                base2 = base;
            } else if (digit < digit2) {
                delta = (digit2 - digit); // update best solution
                b1 = base;
                b2 = base2;
            }

            base *= 10;
            num /= 10;
        }

        if (delta > 0) {
            n0 += delta * (b1 - b2);
        }
        return n0;
    }
};
/**
 * From discussion. O(n) solution.
 */
class Solution {
public:
    int maximumSwap(int num) {
        string numstr = std::to_string(num);

        int maxidx = -1; int maxdigit = -1;
        int leftidx = -1; int rightidx = -1;

        for (int i = numstr.size() - 1; i >= 0; --i) {
            // current digit is the largest by far
            if (numstr[i] > maxdigit) {
                maxdigit = numstr[i];
                maxidx = i;
                continue;
            }

            // best candidate for max swap if there is no more
            // such situation on the left side
            if (numstr[i] < maxdigit) {
                leftidx = i;
                rightidx = maxidx;
            }
        }

        // num is already in order
        if (-1 == leftidx) {
            return num;
        }

        std::swap(numstr[leftidx], numstr[rightidx]);

        return std::stoi(numstr);
    }
};
/**
 * Below solution complicates things. Simplify it as more "stupid" solution.
 */
class Solution {
public:
    int maximumSwap(int num) {
        string str_num = to_string(num);

        for (int i = 0; i < str_num.length(); ++i) {
            char max_digit = str_num[i];
            int index = i;
            for (int j = i + 1; j < str_num.length(); ++j) {
                if (str_num[j] >= max_digit) {
                    max_digit = str_num[j];
                    index = j;
                }
            }

            if (i < index && str_num[i] < str_num[index]) {
                swap(str_num[i], str_num[index]);
                break;
            }
        }

        return stoi(str_num);
    }
};
/**
 * Original solution.
 */
class Solution {
public:
    int maximumSwap(int num) {
        string str_num = to_string(num);
        vector<pair<char, int>> sorted;
        for (int i = 0; i < str_num.length(); ++i) {
            sorted.push_back({str_num[i], i});
        }

        sort(sorted.begin(), sorted.end(),
            [](auto a, auto b) {
                int diff = a.first - b.first;
                if (diff > 0) {
                    return true;
                }
                else if (0 == diff) {
                    return a.second > b.second;
                }

                return false;
            });

        int j = 0;
        for (int i = 0; i < str_num.length(); ++i) {
            int diff = str_num[i] - sorted[j].first;
            if (diff < 0) {
                if (i < sorted[j].second) {
                    swap(str_num[i], str_num[sorted[j].second]);
                    break;
                }
                --i; ++j;
            }
            else if (0 == diff) {
                if (i >= sorted[j].second) {
                    ++j;
                }
            }
        }

        return stoi(str_num);
    }
};
