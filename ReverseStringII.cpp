/**
 * Reverse String II
 *
 * Given a string and an integer k, you need to reverse the first k characters for every 2k characters counting from the
 * start of the string. If there are less than k characters left, reverse all of them. If there are less than 2k but
 * greater than or equal to k characters, then reverse the first k characters and left the other as original.
 *
 * Example:
 *
 * Input: s = "abcdefg", k = 2
 * Output: "bacdfeg"
 *
 * Restrictions:
 *     The string consists of lower English letters only.
 *     Length of the given string and k will in the range [1, 10000]
 */
/**
 * More compact one.
 */
class Solution {
public:
    string reverseStr(string s, int k) {
        const int DK = 2 * k;

        for (size_t pos = 0; pos < s.length(); pos += DK) {
            size_t left = pos;
            size_t right = std::min(pos + k, s.length());
            reverse(s, pos, right - left);
        }

        return s;
    }
private:
    void reverse(string& s, size_t pos, size_t len = string::npos)
    {
        size_t left = pos;
        size_t right = len == string::npos ? (s.length() - 1) : (pos + len - 1);

        while (left < right) {
            std::swap(s[left], s[right]);
            ++left;
            --right;
        }
    }
};

/**
 * More efficient one.
 */
class Solution {
public:
    string reverseStr(string s, int k) {
        const int DK = 2 * k;
        size_t len = s.length();
        size_t pos = 0;

        while (len >= DK) {
            reverse(s, pos, k);
            pos += DK;
            len -= DK;
        }

        if (len >= k) {
            reverse(s, pos, k);
            len = 0;
        }

        if (len > 0) {
            reverse(s, pos);
        }

        return s;
    }
private:
    void reverse(string& s, size_t pos, size_t len = string::npos)
    {
        size_t left = pos;
        size_t right = len == string::npos ? (s.length() - 1) : (pos + len - 1);

        while (left < right) {
            std::swap(s[left], s[right]);
            ++left;
            --right;
        }
    }
};

/**
 * Original solution. Pretty clear.
 */
class Solution {
public:
    string reverseStr(string s, int k) {
        const int DK = 2 * k;
        size_t len = s.length();
        size_t pos = 0;
        string ans;

        while (len >= DK) {
            string sub = s.substr(pos, k);
            ans += string(sub.rbegin(), sub.rend()) + s.substr(pos + k, k);
            len -= DK;
            pos += DK;
        }

        if (len >= k) {
            string sub = s.substr(pos, k);
            ans += string(sub.rbegin(), sub.rend()) + s.substr(pos +k);
            len = 0;
        }

        if (len > 0) {
            string sub = s.substr(pos);
            ans += string(sub.rbegin(), sub.rend());
        }

        return ans;
    }
};
