/**
 * Prime Pairs With Target Sum
 *
 * You are given an integer n. We say that two integers x and y form a prime number pair if:
 *
 * 1 <= x <= y <= n
 * x + y == n
 * x and y are prime numbers
 * Return the 2D sorted list of prime number pairs [xi, yi]. The list should be sorted in increasing order of xi. If there are no prime number pairs at all, return an empty array.
 *
 * Note: A prime number is a natural number greater than 1 with only two factors, itself and 1.
 *
 * Example 1:
 *
 * Input: n = 10
 * Output: [[3,7],[5,5]]
 * Explanation: In this example, there are two prime pairs that satisfy the criteria.
 * These pairs are [3,7] and [5,5], and we return them in the sorted order as described in the problem statement.
 * Example 2:
 *
 * Input: n = 2
 * Output: []
 * Explanation: We can show that there is no prime number pair that gives a sum of 2, so we return an empty array.
 *
 * Constraints:
 *
 * 1 <= n <= 106
 */
/**
 * Original solution.
 */
static auto s_primes = vector<bool>{};

void pre_compute_primes()
{
    constexpr auto const size = 1000001;
    s_primes = vector<bool>(size, true);
    s_primes[0] = false;
    s_primes[1] = false;

    auto upto = static_cast<int64_t>(ceil(sqrt(size)));

    for (auto i = int64_t(2); i <= upto; ++i) {
        if (s_primes[i]) {
            for (auto j = i*i; j <= size; j += i) {
                s_primes[j] = false;
            }
        }
    }
}

class Solution {
public:
    vector<vector<int>> findPrimePairs(int n) {
        if (s_primes.empty()) {
            pre_compute_primes();
        }

        auto const upper = n / 2;
        auto ans = vector<vector<int>>{};

        for (auto i = 2; i <= upper; ++i) {
            if (s_primes[i] && s_primes[n - i]) {
                ans.push_back({i, n - i});
            }
        }

        return ans;
    }
};
