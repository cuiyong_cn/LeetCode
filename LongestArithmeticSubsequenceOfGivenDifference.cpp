/**
 * Longest Arithmetic Subsequence of Given Difference
 *
 * Given an integer array arr and an integer difference, return the length of the longest subsequence in arr which is an
 * arithmetic sequence such that the difference between adjacent elements in the subsequence equals difference.
 *
 * Example 1:
 *
 * Input: arr = [1,2,3,4], difference = 1
 * Output: 4
 * Explanation: The longest arithmetic subsequence is [1,2,3,4].
 * Example 2:
 *
 * Input: arr = [1,3,5,7], difference = 1
 * Output: 1
 * Explanation: The longest arithmetic subsequence is any single element.
 * Example 3:
 *
 * Input: arr = [1,5,7,8,5,3,4,2,1], difference = -2
 * Output: 4
 * Explanation: The longest arithmetic subsequence is [7,5,3,1].
 *
 * Constraints:
 *
 * 1 <= arr.length <= 10^5
 * -10^4 <= arr[i], difference <= 10^4
 */
/**
 * DP solution.
 */
class Solution {
public:
    int longestSubsequence(vector<int>& arr, int difference) {
        vector<int> dp(1e5 + 1, 0);

        for (auto n : arr) {
            int val = n + 2e4;
            int prev = val - difference;
            dp[val] = dp[prev] + 1;
        }

        return *std::max_element(dp.begin(), dp.end());
    }
};

/**
 * Improved one. But still TLE. Looks like binary search still
 * can not satisfy the situation.
 */
class Solution {
public:
    int longestSubsequence(vector<int>& arr, int difference) {
        vector<pair<int, int>> sorted;
        vector<bool> visited(arr.size(), false);

        for (int i = 0; i < arr.size(); ++i) {
            sorted.push_back({arr[i], i});
        }

        if (difference > 0) {
            sort(sorted.begin(), sorted.end(), [](auto a, auto b) {
                if (a.first < b.first) {
                    return true;
                }
                else if (a.first == b.first) {
                    return a.second < b.second;
                }
                return false;
            });
        }
        else {
            sort(sorted.begin(), sorted.end(), [](auto a, auto b) {
                if (a.first > b.first) {
                    return true;
                }
                else if (a.first == b.first) {
                    return a.second < b.second;
                }
                return false;
            });
        }

        int ans = 1;
        for (int i = 0; i < sorted.size(); ++i) {
            if (visited[i]) {
                continue;
            }

            visited[i] = true;
            auto [expected, idx] = sorted[i];
            expected += difference;

            int len = 1;
            for (int j = i + 1; j < sorted.size(); ++j) {
                if (visited[j]) {
                    continue;
                }

                int pos = binary_find(sorted, {j, difference > 0}, expected);
                if (pos < 0) {
                    break;
                }

                for (int k = pos; k < sorted.size(); ++k) {
                    if (sorted[k].first == expected) {
                        if (idx < sorted[k].second) {
                            visited[k] = true;
                            ++len;
                            expected += difference;
                            idx = sorted[k].second;
                        }
                    }
                    else {
                        break;
                    }
                }
            }
            ans = std::max(ans, len);
        }

        return ans;
    }

private:
    int binary_find(const vector<pair<int, int>>& arr, pair<int, int> start, int target)
    {
        int pos = -1;
        int L = start.first, R = arr.size();
        bool asc = start.second;

        while (L <= R) {
            int mid = (L + R) / 2;
            if (arr[mid].first > target) {
                if (asc) {
                    R = mid - 1;
                }
                else {
                    L = mid + 1;
                }
            }
            else if (arr[mid].first < target) {
                if (asc) {
                    L = mid + 1;
                }
                else {
                    R = mid - 1;
                }
            }
            else {
                pos = mid;
                R = mid - 1;
            }
        }

        return pos;
    }
};

/**
 * Original solution. This solution I think definitely gonna work.
 * But TLE :) so typical.
 */
class Solution {
public:
    int longestSubsequence(vector<int>& arr, int difference) {
        vector<pair<int, int>> sorted;
        vector<bool> visited(arr.size(), false);

        for (int i = 0; i < arr.size(); ++i) {
            sorted.push_back({arr[i], i});
        }

        if (difference > 0) {
            sort(sorted.begin(), sorted.end(), [](auto a, auto b) {
                if (a.first < b.first) {
                    return true;
                }
                else if (a.first == b.first) {
                    return a.second < b.second;
                }
                return false;
            });
        }
        else {
            sort(sorted.begin(), sorted.end(), [](auto a, auto b) {
                if (a.first > b.first) {
                    return true;
                }
                else if (a.first == b.first) {
                    return a.second < b.second;
                }
                return false;
            });
        }

        int ans = 1;
        for (int i = 0; i < sorted.size(); ++i) {
            if (visited[i]) {
                continue;
            }

            auto [expected, idx] = sorted[i];
            int len = 0;
            for (int j = i; j < sorted.size(); ++j) {
                if (visited[j]) {
                    continue;
                }

                if (sorted[j].first == expected) {
                    if (idx <= sorted[j].second) {
                        visited[j] = true;
                        expected += difference;
                        idx = sorted[j].second;
                        ++len;
                    }
                }
                else {
                    if ((difference > 0 && sorted[j].first > expected)
                        || (difference < 0 && sorted[j].first < expected)){
                        break;
                    }
                }
            }
            ans = std::max(ans, len);
        }

        return ans;
    }
};
