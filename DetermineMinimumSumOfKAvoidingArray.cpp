/**
 * Determine the Minimum Sum of a k-avoiding Array
 *
 * You are given two integers, n and k.
 *
 * An array of distinct positive integers is called a k-avoiding array if there does not exist any
 * pair of distinct elements that sum to k.
 *
 * Return the minimum possible sum of a k-avoiding array of length n.
 *
 * Example 1:
 *
 * Input: n = 5, k = 4
 * Output: 18
 * Explanation: Consider the k-avoiding array [1,2,4,5,6], which has a sum of 18.
 * It can be proven that there is no k-avoiding array with a sum less than 18.
 *
 * Example 2:
 *
 * Input: n = 2, k = 6
 * Output: 3
 * Explanation: We can construct the array [1,2], which has a sum of 3.
 * It can be proven that there is no k-avoiding array with a sum less than 3.
 *
 * Constraints:
 *
 *     1 <= n, k <= 50
 */
/**
 * Greedy solution.
 */
int sum_from(int start, int count)
{
    auto end = start + count - 1;

    return count * (start + end) / 2;
}

class Solution {
public:
    int minimumSum(int n, int k) {
        auto midk = k / 2;

        return n <= midk ? sum_from(1, n) : sum_from(1, midk) + sum_from(k, n - midk);
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int minimumSum(int n, int k) {
        auto ans = 0;
        auto num_used = unordered_set<int>{};

        for (auto i = 1; n > 0; ++i) {
            if (!num_used.count(k - i)) {
                --n;
                ans += i;
                num_used.insert(i);
            }
        }

        return ans;
    }
};
