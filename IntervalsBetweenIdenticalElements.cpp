/**
 * Intervals Between Identical Elements
 *
 * You are given a 0-indexed array of n integers arr.
 *
 * The interval between two elements in arr is defined as the absolute difference between their
 * indices. More formally, the interval between arr[i] and arr[j] is |i - j|.
 *
 * Return an array intervals of length n where intervals[i] is the sum of intervals between arr[i]
 * and each element in arr with the same value as arr[i].
 *
 * Note: |x| is the absolute value of x.
 *
 * Example 1:
 *
 * Input: arr = [2,1,3,1,2,3,3]
 * Output: [4,2,7,2,4,4,5]
 * Explanation:
 * - Index 0: Another 2 is found at index 4. |0 - 4| = 4
 * - Index 1: Another 1 is found at index 3. |1 - 3| = 2
 * - Index 2: Two more 3s are found at indices 5 and 6. |2 - 5| + |2 - 6| = 7
 * - Index 3: Another 1 is found at index 1. |3 - 1| = 2
 * - Index 4: Another 2 is found at index 0. |4 - 0| = 4
 * - Index 5: Two more 3s are found at indices 2 and 6. |5 - 2| + |5 - 6| = 4
 * - Index 6: Two more 3s are found at indices 2 and 5. |6 - 2| + |6 - 5| = 5
 * Example 2:
 *
 * Input: arr = [10,5,10,10]
 * Output: [5,0,3,4]
 * Explanation:
 * - Index 0: Two more 10s are found at indices 2 and 3. |0 - 2| + |0 - 3| = 5
 * - Index 1: There is only one 5 in the array, so its sum of intervals to identical elements is 0.
 * - Index 2: Two more 10s are found at indices 0 and 3. |2 - 0| + |2 - 3| = 3
 * - Index 3: Two more 10s are found at indices 0 and 2. |3 - 0| + |3 - 2| = 4
 *
 * Constraints:
 *
 * n == arr.length
 * 1 <= n <= 105
 * 1 <= arr[i] <= 105*
 */
/**
 * Solution from discussion. Core idea is same but different appearance.
 */
vector<long long> getDistances(vector<int>& arr) {
    unordered_map<int, int> m;
    for (int i = 0; i < arr.size(); ++i) {
        if (m.count(arr[i]) == 0)
            m[arr[i]] = m.size();
        arr[i] = m[arr[i]];
    }
    vector<vector<int>> idx(m.size());
    vector<long long> sum(m.size()), last(m.size(), -1), res;
    for (int i = 0; i < arr.size(); ++i) {
        idx[arr[i]].push_back(i);
        sum[arr[i]] += idx[arr[i]].back() - idx[arr[i]][0];
    }
    for (int i = 0; i < arr.size(); ++i) {
        long long p = last[arr[i]], v = arr[i];
        if (p >= 0) {
            long long diff = idx[v][p + 1] - idx[v][p];
            sum[v] += diff * p - diff * (idx[v].size() - 1 - (p + 1));
        }
        res.push_back(sum[v]);
        ++last[v];
    }
    return res;
}

/**
 * Original solution.
 */
class Solution {
public:
    vector<long long> getDistances(vector<int>& arr) {
        auto same_value_indices = unordered_map<int, vector<int>>{};
        auto asize = static_cast<int>(arr.size());
        for (auto i = int{0}; i < asize; ++i) {
            same_value_indices[arr[i]].emplace_back(i);
        }

        auto ans = vector<long long>(asize, 0);
        for (auto const& svi : same_value_indices) {
            auto pre_sum = vector<int64_t>{begin(svi.second), end(svi.second)};
            auto suf_sum = pre_sum;
            partial_sum(begin(pre_sum), end(pre_sum), begin(pre_sum));
            partial_sum(rbegin(suf_sum), rend(suf_sum), rbegin(suf_sum));

            auto svi_size = static_cast<int64_t>(pre_sum.size());
            for (auto i = int64_t{0}; i < svi_size; ++i) {
                auto idx = svi.second[i];
                ans[idx] = ((i + 1) * idx - pre_sum[i])
                         + (suf_sum[i] - (svi_size - i) * idx);
            }
        }
        return ans;
    }
};
