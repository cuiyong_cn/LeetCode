/**
 * Next Greater Numerically Balanced Number
 *
 * An integer x is numerically balanced if for every digit d in the number x, there are exactly d
 * occurrences of that digit in x.
 *
 * Given an integer n, return the smallest numerically balanced number strictly greater than n.
 *
 * Example 1:
 *
 * Input: n = 1
 * Output: 22
 * Explanation:
 * 22 is numerically balanced since:
 * - The digit 2 occurs 2 times.
 * It is also the smallest numerically balanced number strictly greater than 1.
 * Example 2:
 *
 * Input: n = 1000
 * Output: 1333
 * Explanation:
 * 1333 is numerically balanced since:
 * - The digit 1 occurs 1 time.
 * - The digit 3 occurs 3 times.
 * It is also the smallest numerically balanced number strictly greater than 1000.
 * Note that 1022 cannot be the answer because 0 appeared more than 0 times.
 * Example 3:
 *
 * Input: n = 3000
 * Output: 3133
 * Explanation:
 * 3133 is numerically balanced since:
 * - The digit 1 occurs 1 time.
 * - The digit 3 occurs 3 times.
 * It is also the smallest numerically balanced number strictly greater than 3000.
 *
 * Constraints:
 *
 * 0 <= n <= 106
 */
/**
 * Original solution. Permutation solution.
 */
auto sorted_beautiful_numbers = vector<int>{};
auto basic_beautiful_numbers = vector<string>{"1", "22", "333", "4444", "55555", "666666", "7777777"};

void dfs(int pos, string combo)
{
    if (pos == basic_beautiful_numbers.size()) {
        return;
    }

    if (combo.length() > 7) {
        return;
    }

    auto new_combo = combo + basic_beautiful_numbers[pos];
    if (new_combo.length() < 8) {
        do {
            sorted_beautiful_numbers.emplace_back(stoi(new_combo));
        } while (next_permutation(new_combo.begin(), new_combo.end()));
    }

    dfs(pos + 1, combo);
    dfs(pos + 1, new_combo);
}

void build_beautiful_numbers()
{
    if (!sorted_beautiful_numbers.empty()) {
        return;
    }

    dfs(0, "");
    sort(sorted_beautiful_numbers.begin(), sorted_beautiful_numbers.end());
}

class Solution {
public:
    int nextBeautifulNumber(int n) {
        build_beautiful_numbers();

        return *upper_bound(sorted_beautiful_numbers.begin(), sorted_beautiful_numbers.end(), n);
    }
};
