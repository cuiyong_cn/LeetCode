/**
 * Sum of Number and Its Reverse
 *
 * Given a non-negative integer num, return true if num can be expressed as the sum of any
 * non-negative integer and its reverse, or false otherwise.
 *
 * Example 1:
 *
 * Input: num = 443
 * Output: true
 * Explanation: 172 + 271 = 443 so we return true.
 * Example 2:
 *
 * Input: num = 63
 * Output: false
 * Explanation: 63 cannot be expressed as the sum of a non-negative integer and its reverse so we return false.
 * Example 3:
 *
 * Input: num = 181
 * Output: true
 * Explanation: 140 + 041 = 181 so we return true. Note that when a number is reversed, there may be leading zeros.
 *
 * Constraints:
 *
 * 0 <= num <= 105
 */
/**
 * Rework reverse implementation. More speed observed.
 */
int reverse(int n)
{
    auto ans = 0;
    for (; n > 0; n /= 10) {
        ans = ans * 10 + n % 10;
    }
    return ans;
}

class Solution {
public:
    bool sumOfNumberAndReverse(int num) {
        for (auto i = num/2; i <= num; ++i) {
            if (num == (i + reverse(i))) {
                return true;
            }
        }
        return false;
    }
};

/**
 * Original solution. Brute force.
 */
int reverse(int n)
{
    auto str = to_string(n);
    return stoi(string{rbegin(str), rend(str)});
}

class Solution {
public:
    bool sumOfNumberAndReverse(int num) {
        for (auto i = 0; i <= num; ++i) {
            if (num == (i + reverse(i))) {
                return true;
            }
        }
        return false;
    }
};
