/**
 * Image Smoother
 *
 * Given a 2D integer matrix M representing the gray scale of an image, you need to design a smoother to make the gray
 * scale of each cell becomes the average gray scale (rounding down) of all the 8 surrounding cells and itself. If a
 * cell has less than 8 surrounding cells, then use as many as you can.
 *
 * Example 1:
 *
 * Input:
 * [[1,1,1],
 *  [1,0,1],
 *  [1,1,1]]
 * Output:
 * [[0, 0, 0],
 *  [0, 0, 0],
 *  [0, 0, 0]]
 * Explanation:
 * For the point (0,0), (0,2), (2,0), (2,2): floor(3/4) = floor(0.75) = 0
 * For the point (0,1), (1,0), (1,2), (2,1): floor(5/6) = floor(0.83333333) = 0
 * For the point (1,1): floor(8/9) = floor(0.88888889) = 0
 *
 * Note:
 *     The value in the given matrix is in the range of [0, 255].
 *     The length and width of the given matrix are in the range of [1, 150].
 */
/**
 * more compact version, but theorectically slow than the below one.
 */
class Solution {
public:
    vector<vector<int>> imageSmoother(vector<vector<int>>& M) {
        auto imgCopy = M;

        int ir = M.size();
        int ic = M[0].size();

        for (int i = 0; i < ir; ++i) {
        for (int j = 0; j < ic; ++j) {
            int pixs = 0, count = 0;

            for (int nr = i - 1; nr <= (i + 1); ++nr) {
            for (int nc = j - 1; nc <= (j + 1); ++nc) {
                if (0 <= nr && nr < ir && 0 <= nc && nc < ic) {
                    pixs += M[nr][nc];
                    ++count;
                }
            }
            }

            imgCopy[i][j] = pixs / count;
        }
        }

        return imgCopy;
    }
};

/**
 * Original solution
 */
class Solution {
public:
    vector<vector<int>> imageSmoother(vector<vector<int>>& M) {
        auto imgCopy = M;

        int ir = M.size();
        int ic = M[0].size();

        for (int i = 0; i < ir; ++i) {
            for (int j = 0; j < ic; ++j) {
                int pixs = 0, count = 0;
                int pr = i - 1, pc = j - 1, nr = i + 1, nc = j + 1;
                if (pr >= 0 && pc >= 0) {
                    pixs += M[pr][pc];
                    ++count;
                }

                if (pr >= 0) {
                    pixs += M[pr][j];
                    ++count;
                }

                if (pr >= 0 && nc < ic) {
                    pixs += M[pr][nc];
                    ++count;
                }

                if (pc >= 0) {
                    pixs += M[i][pc];
                    ++count;
                }

                pixs += M[i][j];
                ++count;

                if (nc < ic) {
                    pixs += M[i][nc];
                    ++count;
                }

                if (nr < ir && pc >= 0) {
                    pixs += M[nr][pc];
                    ++count;
                }

                if (nr < ir) {
                    pixs += M[nr][j];
                    ++count;
                }

                if (nr < ir && nc < ic) {
                    pixs += M[nr][nc];
                    ++count;
                }

                imgCopy[i][j] = pixs / count;
            }
        }

        return imgCopy;
    }
};
