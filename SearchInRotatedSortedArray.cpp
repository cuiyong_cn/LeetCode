/**
 * Search in Rotated Sorted Array
 *
 * There is an integer array nums sorted in ascending order (with distinct values).
 *
 * Prior to being passed to your function, nums is rotated at an unknown pivot index k (0 <= k <
 * nums.length) such that the resulting array is [nums[k], nums[k+1], ..., nums[n-1], nums[0],
 * nums[1], ..., nums[k-1]] (0-indexed). For example, [0,1,2,4,5,6,7] might be rotated at pivot
 * index 3 and become [4,5,6,7,0,1,2].
 *
 * Given the array nums after the rotation and an integer target, return the index of target if it
 * is in nums, or -1 if it is not in nums.
 *
 * You must write an algorithm with O(log n) runtime complexity.
 *
 * Example 1:
 *
 * Input: nums = [4,5,6,7,0,1,2], target = 0
 * Output: 4
 * Example 2:
 *
 * Input: nums = [4,5,6,7,0,1,2], target = 3
 * Output: -1
 * Example 3:
 *
 * Input: nums = [1], target = 0
 * Output: -1
 *
 * Constraints:
 *
 * 1 <= nums.length <= 5000
 * -104 <= nums[i] <= 104
 * All values of nums are unique.
 * nums is guaranteed to be rotated at some pivot.
 * -104 <= target <= 104
 */
/**
 * Intresting one.
 */
class Solution {
public:
    int search(vector<int>& nums, int target) {
        int lo = 0, hi = nums.size();
        while (lo < hi) {
            int mid = (lo + hi) / 2;

            double num = (nums[mid] < nums[0]) == (target < nums[0])
                       ? nums[mid]
                       : target < nums[0] ? -INFINITY : INFINITY;

            if (num < target)
                lo = mid + 1;
            else if (num > target)
                hi = mid;
            else
                return mid;
        }
        return -1;
    }
};

/**
 * iterative version.
 */
class Solution {
public:
    int search(vector<int>& nums, int target) {
        int l = 0, r = nums.size()-1;
        while (l<=r) {
            int mid = (r-l)/2+l;
            if (nums[mid] == target)
                return mid;
            if (nums[mid] < nums[r]) {
                if (nums[mid]<target && target<=nums[r])
                    l = mid+1;
                else
                    r = mid-1;
            } else {
                if(nums[l]<=target && target<nums[mid])
                    r = mid-1;
                else
                    l = mid+1;
            }
        }
        return -1;
    }
};

/**
 * Without finding the pivot.
 */
class Solution {
public:
    int search(vector<int>& nums, int target) {
        return search_helper(nums, 0, nums.size() - 1, target);
    }

private:
    int search_helper(vector<int> const& nums, int start, int end, int target)
    {
        if (nums[start] <= nums[end]) {
            if (nums[start] <= target && target <= nums[end]) {
                auto sit = begin(nums) + start;
                auto eit = begin(nums) + end + 1;
                auto it = lower_bound(sit, eit, target);
                return it == eit ? -1 :
                (*it == target ? distance(begin(nums), it) : -1);
            }

            return -1;
        }

        int mid = (start + end) / 2;
        if (auto pos = search_helper(nums, start, mid, target); pos >= 0) {
            return pos;
        }

        return search_helper(nums, mid + 1, end, target);
    }
};

/**
 * Intresting version. Find the pivot then using cycling fact to find the target.
 */
class Solution {
public:
    int search(int A[], int n, int target) {
        int lo=0,hi=n-1;
        while(lo<hi){
            int mid=(lo+hi)/2;
            if(A[mid]>A[hi]) lo=mid+1;
            else hi=mid;
        }

        // lo==hi is the index of the smallest value and also the number of places rotated.
        int rot=lo;
        lo=0;hi=n-1;
        // The usual binary search and accounting for rotation.
        while(lo<=hi){
            int mid=(lo+hi)/2;
            int realmid=(mid+rot)%n;
            if(A[realmid]==target)return realmid;
            if(A[realmid]<target)lo=mid+1;
            else hi=mid-1;
        }
        return -1;
    }
};

/**
 * More clear one. First we find the pivot. Then search the two part.
 */
class Solution {
public:
    int search(vector<int>& nums, int target) {
        int L = 0, R = nums.size() - 1;
        int pivot = -1;
        while (L < R) {
            int mid = (L + R) / 2;
            if (nums[mid] > nums[mid + 1]) {
                pivot = mid + 1;
                break;
            }

            if (nums[L] < nums[mid]) {
                L = mid;
            }
            else {
                R = mid;
            }
        }

        using Iter = vector<int>::iterator;

        Iter sit, eit;
        if (pivot >= 0) {
            if (target >= nums[0]) {
                sit = begin(nums);
                eit = begin(nums) + pivot;
            }
            else if (target >= nums[pivot]) {
                sit = begin(nums) + pivot;
                eit = end(nums);
            }
            else {
                sit = eit = end(nums);
            }
        }
        else {
            sit = begin(nums);
            eit = end(nums);
        }

        auto it = lower_bound(sit, eit, target);

        return it == eit ? -1 :
        (*it == target ? distance(begin(nums), it) : - 1);
    }
};

/**
 * Original solution. Binary search. Not quite intuitive.
 */
class Solution {
public:
    int search(vector<int>& nums, int target) {
        int L = 0, R = nums.size() - 1;
        while (L <= R) {
            int mid = (L + R) / 2;
            if (target == nums[mid]) {
                return mid;
            }
            else if (target >= nums[L]) {
                if (nums[L] <= nums[mid]) {
                    if (target < nums[mid]) {
                        R = mid - 1;
                    }
                    else {
                        L = mid + 1;
                    }
                }
                else {
                    R = mid - 1;
                }
            }
            else {
                if (nums[L] <= nums[mid]) {
                    L = mid + 1;
                }
                else {
                    if (target < nums[mid]) {
                        R = mid - 1;
                    }
                    else {
                        L = mid + 1;
                    }
                }
            }
        }

        return -1;
    }
};
