/**
 * Employees With Missing Information
 *
 * Table: Employees
 *
 * +-------------+---------+
 * | Column Name | Type    |
 * +-------------+---------+
 * | employee_id | int     |
 * | name        | varchar |
 * +-------------+---------+
 * employee_id is the primary key for this table.
 * Each row of this table indicates the name of the employee whose ID is employee_id.
 *
 * Table: Salaries
 *
 * +-------------+---------+
 * | Column Name | Type    |
 * +-------------+---------+
 * | employee_id | int     |
 * | salary      | int     |
 * +-------------+---------+
 * employee_id is the primary key for this table.
 * Each row of this table indicates the salary of the employee whose ID is employee_id.
 *
 * Write an SQL query to report the IDs of all the employees with missing information. The
 * information of an employee is missing if:
 *
 * The employee's name is missing, or
 * The employee's salary is missing.
 * Return the result table ordered by employee_id in ascending order.
 *
 * The query result format is in the following example.
 *
 * Example 1:
 *
 * Input:
 * Employees table:
 * +-------------+----------+
 * | employee_id | name     |
 * +-------------+----------+
 * | 2           | Crew     |
 * | 4           | Haven    |
 * | 5           | Kristian |
 * +-------------+----------+
 * Salaries table:
 * +-------------+--------+
 * | employee_id | salary |
 * +-------------+--------+
 * | 5           | 76071  |
 * | 1           | 22517  |
 * | 4           | 63539  |
 * +-------------+--------+
 * Output:
 * +-------------+
 * | employee_id |
 * +-------------+
 * | 1           |
 * | 2           |
 * +-------------+
 * Explanation:
 * Employees 1, 2, 4, and 5 are working at this company.
 * The name of employee 1 is missing.
 * The salary of employee 2 is missing.
 */
/**
 * Similar orignal one.
 */
SELECT T.employee_id
FROM
  (SELECT * FROM Employees LEFT JOIN Salaries USING(employee_id)
   UNION
   SELECT * FROM Employees RIGHT JOIN Salaries USING(employee_id))
AS T
WHERE T.salary IS NULL OR T.name IS NULL
ORDER BY employee_id;

/**
 * More simple one.
 */
SELECT employee_id FROM Employees WHERE employee_id NOT IN (SELECT employee_id FROM Salaries)
UNION
SELECT employee_id FROM Salaries WHERE employee_id NOT IN (SELECT employee_id FROM Employees)
ORDER BY 1 ASC

/**
 * Original solution.
 */
# Write your MySQL query statement below
select * from (
    select e.employee_id
      from Employees e
     where not exists (select 1 from Salaries s where s.employee_id = e.employee_id)
     union
    select s.employee_id
      from Salaries s
     where not exists (select 1 from Employees e where s.employee_id = e.employee_id)
) sets
 order by employee_id;
