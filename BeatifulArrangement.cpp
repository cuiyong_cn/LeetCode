/**
 * Beautiful Arrangement
 *
 * Suppose you have N integers from 1 to N. We define a beautiful arrangement as an array that is constructed by
 * these N numbers successfully if one of the following is true for the ith position (1 <= i <= N) in this array:
 *
 *     The number at the ith position is divisible by i.
 *     i is divisible by the number at the ith position.
 *
 * Now given N, how many beautiful arrangements can you construct?
 *
 * Example 1:
 *
 * Input: 2
 * Output: 2
 * Explanation:
 *
 * The first beautiful arrangement is [1, 2]:
 *
 * Number at the 1st position (i=1) is 1, and 1 is divisible by i (i=1).
 *
 * Number at the 2nd position (i=2) is 2, and 2 is divisible by i (i=2).
 *
 * The second beautiful arrangement is [2, 1]:
 *
 * Number at the 1st position (i=1) is 2, and 2 is divisible by i (i=1).
 *
 * Number at the 2nd position (i=2) is 1, and i (i=2) is divisible by 1.
 *
 * Note:
 *
 *     N is a positive integer and will not exceed 15.
 */
/**
 * Same idea. But with backtracking thought. Less efficient than below solution.
 */
class Solution {
public:
    int countArrangement(int N) {
        ans = 0;
        vector<bool> visited(N + 1, false);

        permute(visited, 1);

        return ans;
    }
private:
    void permute(vector<bool>& visited, int pos) {
        if (pos == visited.size()) {
            ++ans;
        }

        for (int k = 1; k < visited.size(); ++k) {
            if (false == visited[k] && ((pos % k == 0) || (k % pos == 0))) {
                visited[k] = true;
                permute(visited, pos + 1);
                visited[k] = false;
            }
        }
    }
    int ans;
};
/**
 * Improved version. Before we do permute, we check if the new swap number violate the rule.
 */
class Solution {
public:
    int countArrangement(int N) {
        ans = 0;
        vector<int> nums(N + 1);
        for (int i = 0; i < nums.size(); ++i) {
            nums[i] = i;
        }

        permute(nums, 1);

        return ans;
    }
private:
    void permute(vector<int>& nums, int i) {
        if (i == nums.size()) {
            ++ans;
        }

        for (int k = i; k < nums.size(); ++k) {
            swap(nums[i], nums[k]);
            if ((nums[i] % i == 0) || (i % nums[i] == 0)) {
                permute(nums, i + 1);
            }
            swap(nums[i], nums[k]);
        }
    }
    int ans;
};
/**
 * Brute force, but again, time limit exceeded.
 */
class Solution {
public:
    int countArrangement(int N) {
        ans = 0;
        vector<int> nums(N + 1);
        for (int i = 0; i < nums.size(); ++i) {
            nums[i] = i;
        }

        permute(nums, 1);

        return ans;
    }
private:
    void permute(vector<int>& nums, int i) {
        if (i == nums.size()) {
            int k = 1;
            for (; k < nums.size(); ++k) {
                if (((nums[k] % k) != 0) && ((k % nums[k]) != 0)) {
                    break;
                }
            }
            if (k == nums.size()) {
                ++ans;
            }
        }

        for (int k = i; k < nums.size(); ++k) {
            swap(nums[i], nums[k]);
            permute(nums, i + 1);
            swap(nums[i], nums[k]);
        }
    }
    int ans;
};
