/**
 * Check if the Sentence Is Pangram
 *
 * A pangram is a sentence where every letter of the English alphabet appears at least once.
 *
 * Given a string sentence containing only lowercase English letters, return true if sentence is a
 * pangram, or false otherwise.
 *
 * Example 1:
 *
 * Input: sentence = "thequickbrownfoxjumpsoverthelazydog"
 * Output: true
 * Explanation: sentence contains at least one of every letter of the English alphabet.
 * Example 2:
 *
 * Input: sentence = "leetcode"
 * Output: false
 *
 * Constraints:
 *
 * 1 <= sentence.length <= 1000
 * sentence consists of lowercase English letters.
 */
/**
 * More efficient solution.
 */
class Solution {
public:
    bool checkIfPangram(string sentence) {
        int seen = 0;
        for (auto const& c : sentence) {
            seen |= 1 << (c - 'a');
        }

        return ((1 << 26) - 1) == seen;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool checkIfPangram(string sentence) {
        unordered_set<char> chars_seen;
        for (auto const& c : sentence) {
            chars_seen.emplace(c);
        }

        return 26 == chars_seen.size();
    }
};
