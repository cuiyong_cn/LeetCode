/**
 * Swap Nodes in Pairs
 *
 * Given a linked list, swap every two adjacent nodes and return its head.
 *
 * You may not modify the values in the list's nodes, only nodes itself may be changed.
 *
 * Example:
 *
 * Given 1->2->3->4, you should return the list as 2->1->4->3.
 */
/**
 * Original solution. Stack based solution.
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* swapPairs(ListNode* head) {
        vector<ListNode*> vec;

        while (head) {
            vec.push_back(head);
            head = head->next;
        }

        if (vec.size() % 2) {
            head = vec.back();
            vec.pop_back();
        }

        while (vec.size() > 1) {
            ListNode* n1 = vec.back(); vec.pop_back();
            ListNode* n2 = vec.back(); vec.pop_back();
            n1->next = n2;
            n2->next = head;
            head = n1;
        }

        return head;
    }
};

/**
 * Other solutions from discussions. Recursive approach.
 */
class Solution
{
public:
    ListNode* swapPairs1(ListNode* head) {
        if (nullptr == head || nullptr == head->next) return head;

        ListNode *res = head->next;
        head->next = swapPairs(res->next);
        res->next = head;
        return res;
    }
};

/**
 * Iterative approach
 */
class Solution
{
public:
    ListNode *swapPairs(ListNode *head) {
        ListNode tmp(0);
        tmp.next = head;

        ListNode* pre = &tmp;
        ListNode* cur = head;

        while (cur && cur->next) {
            ListNode* next = cur->next;
            cur->next = next->next;
            next->next = cur;
            pre->next = next;
            pre = cur;
            cur = cur->next;
        }

        return tmp->next;
    }
};
