/**
 * Number of Operations to Make Network Connected
 *
 * There are n computers numbered from 0 to n-1 connected by ethernet cables connections forming a network where
 * connections[i] = [a, b] represents a connection between computers a and b. Any computer can reach any other computer
 * directly or indirectly through the network.
 *
 * Given an initial computer network connections. You can extract certain cables between two directly connected
 * computers, and place them between any pair of disconnected computers to make them directly connected. Return the
 * minimum number of times you need to do this in order to make all the computers connected. If it's not possible,
 * return -1.
 *
 * Example 1:
 *
 * Input: n = 4, connections = [[0,1],[0,2],[1,2]]
 * Output: 1
 * Explanation: Remove cable between computer 1 and 2 and place between computers 1 and 3.
 *
 * Example 2:
 *
 * Input: n = 6, connections = [[0,1],[0,2],[0,3],[1,2],[1,3]]
 * Output: 2
 *
 * Example 3:
 *
 * Input: n = 6, connections = [[0,1],[0,2],[0,3],[1,2]]
 * Output: -1
 * Explanation: There are not enough cables.
 *
 * Example 4:
 *
 * Input: n = 5, connections = [[0,1],[0,2],[3,4],[2,3]]
 * Output: 0
 *
 * Constraints:
 *
 *     1 <= n <= 10^5
 *     1 <= connections.length <= min(n*(n-1)/2, 10^5)
 *     connections[i].length == 2
 *     0 <= connections[i][0], connections[i][1] < n
 *     connections[i][0] != connections[i][1]
 *     There are no repeated connections.
 *     No two computers are connected by more than one cable.
 */
/**
 * Found this interesting solution from user votrubac.
 * This is union-find solution. I thought about this method, but did not write it.
 */
class Solution {
public:
    int find(vector<int> &ds, int i) {
        return ds[i] < 0 ? i : ds[i] = find(ds, ds[i]);
    }

    int makeConnected(int n, vector<vector<int>>& connections) {
        if (connections.size() < n - 1) return -1;
        vector<int> ds(n, -1);
        for (auto &c : connections) {
            auto i = find(ds, c[0]), j = find(ds, c[1]);
            if (i != j) {
                if (ds[j] < ds[i])
                    swap(i, j);
                ds[i] += ds[j];
                ds[j] = i;
                --n;
            }
        }
        return n - 1;
    }
};
/**
 * original solution. Basic idea is, connections less than n - 1, directly return -1;
 * If cables are enough, then we only have to count the isolated items.
 * How do we count isolated items? We count how many connected circles.
 */
class Solution {
public:
    int makeConnected(int n, vector<vector<int>>& connections) {
        if (connections.size() < (n - 1)) return -1;

        for (auto& c : connections) {
            memo[c[0]].push_back(c[1]);
            memo[c[1]].push_back(c[0]);
        }

        visited = vector<int>(n, 0);
        int connected = 0;
        for (int i = 0; i < n; ++i) connected += dfs(i);

        return connected - 1;
    }

private:
    int dfs(int i)
    {
        if (visited[i]) return 0;
        visited[i] = 1;
        if (memo.count(i)) {
            for (auto p : memo[i]) {
                dfs(p);
            }
        }

        return 1;
    }

    vector<int> visited;
    unordered_map<int, vector<int>> memo;
};
