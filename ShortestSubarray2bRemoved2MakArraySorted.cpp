/**
 * Shortest Subarray to be Removed to Make Array Sorted
 *
 * Given an integer array arr, remove a subarray (can be empty) from arr such that the remaining
 * elements in arr are non-decreasing.
 *
 * Return the length of the shortest subarray to remove.
 *
 * A subarray is a contiguous subsequence of the array.
 *
 * Example 1:
 *
 * Input: arr = [1,2,3,10,4,2,3,5]
 * Output: 3
 * Explanation: The shortest subarray we can remove is [10,4,2] of length 3. The remaining elements after that will be [1,2,3,3,5] which are sorted.
 * Another correct solution is to remove the subarray [3,10,4].
 * Example 2:
 *
 * Input: arr = [5,4,3,2,1]
 * Output: 4
 * Explanation: Since the array is strictly decreasing, we can only keep a single element. Therefore we need to remove a subarray of length 4, either [5,4,3,2] or [4,3,2,1].
 * Example 3:
 *
 * Input: arr = [1,2,3]
 * Output: 0
 * Explanation: The array is already non-decreasing. We do not need to remove any elements.
 *
 * Constraints:
 *
 * 1 <= arr.length <= 105
 * 0 <= arr[i] <= 109
 */
/**
 * Same idea, shorter version. First start from right.
 */
class Solution {
public:
    int findLengthOfShortestSubarray(vector<int>& n)
    {
        int sz = n.size(), r = sz - 1;
        for (; r > 0 && n[r - 1] <= n[r]; --r) {
            ;
        }

        auto res = r;
        for (int l = 0; l < r && (l == 0 || n[l - 1] <= n[l]); ++l) {
            while (r < sz && n[r] < n[l]) {
                ++r;
            }

            res = min(res, r - l - 1);
        }

        return res;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int findLengthOfShortestSubarray(vector<int>& arr) {
        int const N = arr.size();
        int i = 1;
        while (i < N) {
            if (arr[i] < arr[i - 1]) {
                break;
            }
            ++i;
        }

        if (N == i) {
            return 0;
        }

        int j = N - 1;
        while (j > 0) {
            if (arr[j] < arr[j - 1]) {
                break;
            }
            --j;
        }

        int ans = min(N - i, j);
        for (int m = i - 1; m >= 0; --m) {
            if (arr[m] <= arr[j]) {
                ans = min(ans, j - m - 1);
                break;
            }
        }

        for (int m = j; m < N; ++m) {
            if (arr[i - 1] <= arr[m]) {
                ans = min(ans, m - i);
                break;
            }
        }

        for (int m = i - 1, n = j; m >= 0 && n < N; --m, ++n) {
            if (arr[m] <= arr[n]) {
                ans = min(ans, n - m - 1);
                break;
            }
        }

        return ans;
    }
};
