/**
 * Perfect Number
 *
 * A perfect number is a positive integer that is equal to the sum of its positive divisors,
 * excluding the number itself. A divisor of an integer x is an integer that can divide x evenly.
 *
 * Given an integer n, return true if n is a perfect number, otherwise return false.
 *
 * Example 1:
 *
 * Input: num = 28
 * Output: true
 * Explanation: 28 = 1 + 2 + 4 + 7 + 14
 * 1, 2, 4, 7, and 14 are all divisors of 28.
 * Example 2:
 *
 * Input: num = 6
 * Output: true
 * Example 3:
 *
 * Input: num = 496
 * Output: true
 * Example 4:
 *
 * Input: num = 8128
 * Output: true
 * Example 5:
 *
 * Input: num = 2
 * Output: false
 *
 * Constraints:
 *
 * 1 <= num <= 108
 */
/**
 * Funny but valid solution.
 */
class Solution:
    def checkPerfectNumber(self, num):
        """
        :type num: int
        :rtype: bool
        """
        return num in (6, 28, 496, 8128, 33550336)

/**
 * As it turns out, this is a math problem.
 */
public class Solution {
    public int pn(int p) {
        return (1 << (p - 1)) * ((1 << p) - 1);
    }

    public boolean checkPerfectNumber(int num) {
        int[] primes=new int[]{2,3,5,7,13,17,19,31};
        for (int prime: primes) {
            if (pn(prime) == num)
                return true;
        }
        return false;
    }
}

/**
 * Original solution.
 */
class Solution {
public:
    bool checkPerfectNumber(int num) {
        if (num < 6) {
            return false;
        }

        set<int> divisors = { 1 };
        auto S = sqrt(num) + 1;
        for (int i = 2; i < S; ++i) {
            if (0 == (num % i)) {
                divisors.insert(i);
                divisors.insert(num / i);
            }
        }

        return accumulate(divisors.begin(), divisors.end(), 0) == num;
    }
};
