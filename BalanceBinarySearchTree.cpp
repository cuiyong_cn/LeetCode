/**
 * Balance a Binary Search Tree
 *
 * Given the root of a binary search tree, return a balanced binary search tree with the same node
 * values. If there is more than one answer, return any of them.
 *
 * A binary search tree is balanced if the depth of the two subtrees of every node never differs by
 * more than 1.
 *
 * Example 1:
 *
 * Input: root = [1,null,2,null,3,null,4,null,null]
 * Output: [2,1,3,null,null,null,4]
 * Explanation: This is not the only correct answer, [3,1,4,null,2] is also correct.
 * Example 2:
 *
 * Input: root = [2,1,3]
 * Output: [2,1,3]
 *
 * Constraints:
 *
 * The number of nodes in the tree is in the range [1, 104].
 * 1 <= Node.val <= 105
 */
/**
 * DSW algorithm. Learned from discussioin forum.
 */
class Solution {
    public TreeNode balanceBST(TreeNode root) {
        TreeNode pseudoRoot = new TreeNode(0);
        pseudoRoot.right = root;

        int nodeCount = flattenTreeWithRightRotations(pseudoRoot);
        int heightOfTree = (int)(Math.log(nodeCount + 1) / Math.log(2));
        int numOfNodesInTree = (int) Math.pow(2, heightOfTree) - 1;

        createBalancedTreeWithLeftRotation(pseudoRoot, nodeCount - numOfNodesInTree);
        for (int numOfNodesInSubTree = (numOfNodesInTree/2); numOfNodesInSubTree > 0; numOfNodesInSubTree /= 2)
            createBalancedTreeWithLeftRotation(pseudoRoot, numOfNodesInSubTree);
        return pseudoRoot.right;
    }


    public int flattenTreeWithRightRotations(TreeNode root) {
        int nodeCount = 0;
        TreeNode pseudoRoot = root.right;
        while (pseudoRoot != null) {
            if (pseudoRoot.left != null) {
                TreeNode oldPseudoRoot = pseudoRoot;
                pseudoRoot = pseudoRoot.left;
                oldPseudoRoot.left = pseudoRoot.right;
                pseudoRoot.right = oldPseudoRoot;
                root.right = pseudoRoot;
            } else {
                ++nodeCount;
                root = pseudoRoot;
                pseudoRoot = pseudoRoot.right;
            }
        }
        return nodeCount;
    }

    void createBalancedTreeWithLeftRotation(TreeNode root, int numOfNodesInSubTree) {
        TreeNode pseudoRoot = root.right;
        while (numOfNodesInSubTree-- > 0) {
            TreeNode oldPseudoRoot = pseudoRoot;
            pseudoRoot = pseudoRoot.right;

            root.right = pseudoRoot;
            oldPseudoRoot.right = pseudoRoot.left;
            pseudoRoot.left = oldPseudoRoot;
            root = pseudoRoot;
            pseudoRoot = pseudoRoot.right;
        }
    }
}

/**
 * Original solution.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* balanceBST(TreeNode* root) {
        sort(root);
        return construct_balanced();
    }

private:
    void sort(TreeNode* node) {
        if (nullptr == node) {
            return;
        }
        sort(node->left);
        m_sorted.push_back(node);
        sort(node->right);
    }

    TreeNode* construct_balanced() {
        return helper(0, m_sorted.size() - 1);
    }

    TreeNode* helper(int L, int R) {
        if (L > R) {
            return nullptr;
        }

        int mid = L + (R - L) / 2;
        auto node = m_sorted[mid];
        node->left = helper(L, mid - 1);
        node->right = helper(mid + 1, R);
        return node;
    }

    vector<TreeNode*> m_sorted;
};
