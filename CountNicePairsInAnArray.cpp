/**
 * Count Nice Pairs in an Array
 *
 * You are given an array nums that consists of non-negative integers. Let us define rev(x) as the
 * reverse of the non-negative integer x. For example, rev(123) = 321, and rev(120) = 21. A pair of
 * indices (i, j) is nice if it satisfies all of the following conditions:
 *
 * 0 <= i < j < nums.length
 * nums[i] + rev(nums[j]) == nums[j] + rev(nums[i])
 * Return the number of nice pairs of indices. Since that number can be too large, return it modulo
 * 109 + 7.
 *
 * Example 1:
 *
 * Input: nums = [42,11,1,97]
 * Output: 2
 * Explanation: The two pairs are:
 *  - (0,3) : 42 + rev(97) = 42 + 79 = 121, 97 + rev(42) = 97 + 24 = 121.
 *  - (1,2) : 11 + rev(1) = 11 + 1 = 12, 1 + rev(11) = 1 + 11 = 12.
 * Example 2:
 *
 * Input: nums = [13,10,35,24,76]
 * Output: 4
 *
 * Constraints:
 *
 * 1 <= nums.length <= 105
 * 0 <= nums[i] <= 109
 */
/**
 * same performance, another method.
 */
class Solution {
public:
    int countNicePairs(vector<int>& nums) {
        long ans = 0;
        int const mod = 1e9 + 7;
        unordered_map<long, long> num_cnt;
        for (auto n : nums) {
            ++num_cnt[n - reverse(n)];
        }

        for (auto [n, cnt] : num_cnt) {
            ans += cnt * (cnt - 1) / 2;
            ans %= mod;
        }

        return ans;
    }

private:
    int reverse(int n)
    {
        int ans = 0;
        while (n) {
            ans = ans * 10 + n % 10;
            n /= 10;
        }
        return ans;
    }
};

/**
 * Improve the code. Performance gain.
 */
class Solution {
public:
    int countNicePairs(vector<int>& nums) {
        int ans = 0;
        int const mod = 1e9 + 7;
        unordered_map<int, int> num_cnt;
        for (auto n : nums) {
            auto revn = n - reverse(n);
            ++num_cnt[revn];
            if (num_cnt[revn] > 1) {
                ans += num_cnt[revn] - 1;
                ans %= mod;
            }
        }

        return ans;
    }

private:
    int reverse(int n)
    {
        int ans = 0;
        while (n) {
            ans = ans * 10 + n % 10;
            n /= 10;
        }
        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int countNicePairs(vector<int>& nums) {
        for (auto& n : nums) {
            n = n - reverse(n);
        }

        int ans = 0;
        int const mod = 1e9 + 7;
        unordered_map<int, int> num_cnt;
        for (auto n : nums) {
            ++num_cnt[n];
            if (num_cnt[n] > 1) {
                ans += num_cnt[n] - 1;
                ans %= mod;
            }
        }

        return ans;
    }

private:
    int reverse(int n)
    {
        auto nstr = to_string(n);
        std::reverse(begin(nstr), end(nstr));
        return stoi(nstr);
    }
};
