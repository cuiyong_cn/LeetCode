/**
 * Longest Word in Dictionary
 *
 * Given a list of strings words representing an English Dictionary, find the longest word in words that can be built
 * one character at a time by other words in words. If there is more than one possible answer, return the longest word
 * with the smallest lexicographical order.
 *
 * If there is no answer, return the empty string.
 * Example 1:
 * Input:
 * words = ["w","wo","wor","worl", "world"]
 * Output: "world"
 * Explanation:
 * The word "world" can be built one character at a time by "w", "wo", "wor", and "worl".
 * Example 2:
 * Input:
 * words = ["a", "banana", "app", "appl", "ap", "apply", "apple"]
 * Output: "apple"
 * Explanation:
 * Both "apply" and "apple" can be built from other words in the dictionary. However, "apple" is lexicographically
 * smaller than "apply".
 *
 * Note:
 *
 * All the strings in the input will only contain lowercase letters.
 * The length of words will be in the range [1, 1000].
 * The length of words[i] will be in the range [1, 30].
 */
/**
 * same idea, diff implementation
 */
class Solution {
public:
    string longestWord(vector<string>& words) {
        sort(words.begin(), words.end());
        unordered_set<string> built;
        string res;
        for (string w : words) {
            if (w.size() == 1 || built.count(w.substr(0, w.size() - 1))) {
                res = w.size() > res.size() ? w : res;
                built.insert(w);
            }
        }
        return res;
    }
};

/**
 * Improved one. using unordered_set to speed it up.
 *
 * Also we can use the trie structure to solve this one.
 */
class Solution {
public:
    string longestWord(vector<string>& words) {
        unordered_set<string> words_set(words.begin(), words.end());

        string ans;

        for (auto& w : words_set) {
            if ((w.length() < ans.length())
                || (w.length() == ans.length() && ans < w)) {
                continue;
            }

            bool valid = true;
            for (size_t i = 1; i < w.length(); ++i) {
                if (!words_set.count(w.substr(0, i))) {
                    valid = false;
                    break;
                }
            }

            if (valid) {
                ans = w;
            }
        }

        return ans;
    }
};

/**
 * Original solution. Though not very efficient, but clear and straightforward.
 */
class Solution {
public:
    string longestWord(vector<string>& words) {
        set<string, greater<string>> words_set(words.begin(), words.end());

        string ans;

        for (auto& w : words_set) {
            if ((w.length() < ans.length())
                || (w.length() == ans.length() && ans < w)) {
                continue;
            }

            bool valid = true;
            for (size_t i = 1; i < w.length(); ++i) {
                if (!words_set.count(w.substr(0, i))) {
                    valid = false;
                    break;
                }
            }

            if (valid) {
                ans = w;
            }
        }

        return ans;
    }
};
