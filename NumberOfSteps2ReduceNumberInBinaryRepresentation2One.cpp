/**
 * Number of Steps to Reduce a Number in Binary Representation to One
 *
 * Given a number s in their binary representation. Return the number of steps to reduce it to 1 under the following rules:
 *     If the current number is even, you have to divide it by 2.
 *     If the current number is odd, you have to add 1 to it.
 *
 * It's guaranteed that you can always reach to one for all testcases.
 *
 * Example 1:
 *
 * Input: s = "1101"
 * Output: 6
 * Explanation: "1101" corressponds to number 13 in their decimal representation.
 * Step 1) 13 is odd, add 1 and obtain 14.
 * Step 2) 14 is even, divide by 2 and obtain 7.
 * Step 3) 7 is odd, add 1 and obtain 8.
 * Step 4) 8 is even, divide by 2 and obtain 4.
 * Step 5) 4 is even, divide by 2 and obtain 2.
 * Step 6) 2 is even, divide by 2 and obtain 1.
 *
 * Example 2:
 *
 * Input: s = "10"
 * Output: 1
 * Explanation: "10" corressponds to number 2 in their decimal representation.
 * Step 1) 2 is even, divide by 2 and obtain 1.
 *
 * Example 3:
 *
 * Input: s = "1"
 * Output: 0
 *
 * Constraints:
 *     1 <= s.length <= 500
 *     s consists of characters '0' or '1'
 *     s[0] == '1'
 */
/**
 * From user votrubac. The idea is:
 *
 * 1  We haven't encountered any 1. Every char adds one operation.
 * 2  We encounter our first 1. We set carry to 1 and add two operations.
 * 3  The rest:
 *    3A. Every 1 needs one operation (carry makes it 0). carry is still 1 due to addition.
 *    3B. Every 0 needs two operations (carry makes it 1). carry is still 1 as we need to add 1 in this case.
 */
int numSteps(string &s) {
    int res = 0, carry = 0;
    for (auto i = s.size() - 1; i > 0; --i) {
        ++res;
        if (s[i] - '0' + carry == 1) {
            carry = 1;
            ++res;
        }
    }
    return res + carry;
}

/**
 * And here comes a equivalent one.
 */
int numSteps(string &s) {
    int ones = 0, last_one = 0;
    for (auto i = 1; i < s.size(); ++i)
        if (s[i] == '1') {
            last_one = i;
            ++ones;
        }
    return s.size() + (last_one - ones) /* zeros count */ + (last_one ? 1 : 0);
}

/**
 * Original solution. Simulate the process.
 */
class Solution {
public:
    int numSteps(string s) {
        s = "0" + s;
        int i = s.length() - 1;
        int step = 0;

        while (i > 1) {
            if ('0' == s[i]) {
                step += 1;
            }
            else {
                add_one(s, i);
                step += 2;
            }
            --i;
        }

        if ('1' == s[0]) ++step;

        return step;
    }

private:
    void add_one(string& s, int pos) {
        for (int i = pos; i >= 0; --i) {
            if ('1' == s[i]) {
                s[i] = '0';
            }
            else {
                s[i] = '1';
                break;
            }
        }
    }
};

/**
 * Another solution from discussion. Not very efficient, but also intuitive.
 */
int numSteps(string s) {
	int step = 0;

	while (s != "1") {
		if (s.back() == '0') s.pop_back();
		else {
			while (!s.empty() && s.back() == '1') {
				s.pop_back();
				++step;
			}

			if (s.empty()) return step + 1;
			else s.back() = '1';
		}
		++step;
	}

	return step;
}
