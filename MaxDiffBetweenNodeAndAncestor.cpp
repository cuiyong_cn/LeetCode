/**
 * Maximum Difference Between Node and Ancestor
 *
 * Given the root of a binary tree, find the maximum value V for which there exists
 * different nodes A and B where V = |A.val - B.val| and A is an ancestor of B.
 *
 * (A node A is an ancestor of B if either: any child of A is equal to B, or any child of
 * A is an ancestor of B.)
 *
 * Example 1:
 *                      8
 *                     / \
 *                    /   \
 *                   3    10
 *                  / \     \
 *                 1   6    14
 *                    / \   /
 *                   4   7 13
 *
 * Input: [8,3,10,1,6,null,14,null,null,4,7,13]
 * Output: 7
 * Explanation:
 *
 * We have various ancestor-node differences, some of which are given below :
 * |8 - 3| = 5
 * |3 - 7| = 4
 * |8 - 1| = 7
 * |10 - 13| = 3
 * Among all possible differences, the maximum value of 7 is obtained by |8 - 1| = 7.
 *
 * Note:
 *
 *     The number of nodes in the tree is between 2 and 5000.
 *     Each node will have value between 0 and 100000.
 */
/**
 * To make sure min/max values belong to an ancestor, we track min/max from the root till
 * the leaf, and pick the biggest difference among all leaves.
 *
 * Or think like this, we sink the min/max value down to the leaf node, and cal the
 * differences at the leaf node, return the max one.
 */
class Solution {
public:
    int maxAncestorDiff(TreeNode* root, int mn = 100000, int mx = 0) {
        if (root == nullptr) return mx - mn;

        return max(maxAncestorDiff(root->left, min(mn, root->val), max(mx, root->val)),
        maxAncestorDiff(root->right, min(mn, root->val), max(mx, root->val)));
    }
};

/**
 * Original ugly code.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    int getMax(TreeNode* root, int val = 0) {
        if (root == nullptr) return val;

        val = max(val, root->val);
        return max(getMax(root->left, val), getMax(root->right, val));
    }

    int getMin(TreeNode* root, int val = 100001) {
        if (root == nullptr) return val;

        val = min(val, root->val);
        return min(getMin(root->left, val), getMin(root->right, val));
    }

public:
    int maxAncestorDiff(TreeNode* root) {
        if (root == nullptr) return 0;
        int lmax = root->left ? getMax(root->left, root->left->val) : root->val;
        int lmin = root->left ? getMin(root->left, root->left->val) : root->val;
        int rmax = root->right ? getMax(root->right, root->right->val) : root->val;
        int rmin = root->right ? getMin(root->right, root->right->val) : root->val;

        int l1 = max(abs(root->val - lmax), abs(root->val - lmin));
        int l2 = max(abs(root->val - rmax), abs(root->val - rmin));
        int m = max(l1, l2);
        return max(max(m, maxAncestorDiff(root->left)), max(m, maxAncestorDiff(root->right)));
    }
};
