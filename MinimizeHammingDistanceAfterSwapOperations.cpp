/**
 * Minimize Hamming Distance After Swap Operations
 *
 * You are given two integer arrays, source and target, both of length n. You are also given an
 * array allowedSwaps where each allowedSwaps[i] = [ai, bi] indicates that you are allowed to swap
 * the elements at index ai and index bi (0-indexed) of array source. Note that you can swap
 * elements at a specific pair of indices multiple times and in any order.
 *
 * The Hamming distance of two arrays of the same length, source and target, is the number of
 * positions where the elements are different. Formally, it is the number of indices i for 0 <= i <=
 * n-1 where source[i] != target[i] (0-indexed).
 *
 * Return the minimum Hamming distance of source and target after performing any amount of swap
 * operations on array source.
 *
 * Example 1:
 *
 * Input: source = [1,2,3,4], target = [2,1,4,5], allowedSwaps = [[0,1],[2,3]]
 * Output: 1
 * Explanation: source can be transformed the following way:
 * - Swap indices 0 and 1: source = [2,1,3,4]
 * - Swap indices 2 and 3: source = [2,1,4,3]
 * The Hamming distance of source and target is 1 as they differ in 1 position: index 3.
 * Example 2:
 *
 * Input: source = [1,2,3,4], target = [1,3,2,4], allowedSwaps = []
 * Output: 2
 * Explanation: There are no allowed swaps.
 * The Hamming distance of source and target is 2 as they differ in 2 positions: index 1 and index 2.
 * Example 3:
 *
 * Input: source = [5,1,2,4,3], target = [1,5,4,2,3], allowedSwaps = [[0,4],[4,2],[1,3],[1,4]]
 * Output: 0
 *
 * Constraints:
 *
 * n == source.length == target.length
 * 1 <= n <= 105
 * 1 <= source[i], target[i] <= 105
 * 0 <= allowedSwaps.length <= 105
 * allowedSwaps[i].length == 2
 * 0 <= ai, bi <= n - 1
 * ai != bi
 */
/**
 * Union disjoint set solution from discussion. I like my original better.
 */
class Solution {
public:
    int find(vector<int> &ds, int i) {
        return ds[i] < 0 ? i : ds[i] = find(ds, ds[i]);
    }

    int minimumHammingDistance(vector<int>& s, vector<int>& t, vector<vector<int>>& allowedSwaps) {
        int sz = s.size(), res = 0;
        vector<int> ds(sz, -1);
        for (auto &sw: allowedSwaps) {
            int i = find(ds, sw[0]), j = find(ds, sw[1]);
            if (i != j) {
                if (ds[i] > ds[j])
                    swap(i, j);
                ds[i] += ds[j];
                ds[j] = i;
            }
        }

        vector<unordered_multiset<int>> cycles(sz);
        for (auto i = 0; i < sz; ++i)
            if (s[i] != t[i])
                cycles[find(ds, i)].insert(s[i]);
        for (auto i = 0; i < sz; ++i) {
            if (s[i] != t[i]) {
                auto &cycle = cycles[find(ds, i)];
                auto it = cycle.find(t[i]);
                if (it != end(cycle)) {
                    cycle.erase(it);
                } else
                    ++res;
            }
        }
        return res;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int minimumHammingDistance(vector<int>& source, vector<int>& target, vector<vector<int>>& allowedSwaps) {
        int const N = source.size();
        char const visited = 1;
        char const not_visited = 0;
        vector<char> guest(N, not_visited);

        unordered_map<int, unordered_set<int>> connected;
        int ans = 0;

        for (auto const& as : allowedSwaps) {
            connected[as[0]].insert(as[1]);
            connected[as[1]].insert(as[0]);
        }

        for (int i = 0; i < N; ++i) {
            if (visited == guest[i]) {
                continue;
            }

            unordered_set<int> group;
            deque<int> q = {i};

            while (!q.empty()) {
                int const S = q.size();
                for (int j = 0; j < S; ++j) {
                    auto g = q.front(); q.pop_front();
                    if (visited == guest[g]) {
                        continue;
                    }

                    guest[g] = visited;
                    group.insert(g);

                    copy(connected[g].begin(), connected[g].end(), back_inserter(q));
                }
            }

            unordered_map<int, int> cnt;
            for (auto const& idx : group) {
                ++cnt[source[idx]];
                --cnt[target[idx]];
            }

            int diff = 0;
            for (auto const& c : cnt) {
                diff += abs(c.second);
            }
            ans += diff / 2;
        }

        return ans;
    }
};
