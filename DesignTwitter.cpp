/**
 * Design Twitter
 *
 * Design a simplified version of Twitter where users can post tweets, follow/unfollow another user,
 * and is able to see the 10 most recent tweets in the user's news feed.
 *
 * Implement the Twitter class:
 *
 * Twitter() Initializes your twitter object.  void postTweet(int userId, int tweetId) Composes a
 * new tweet with ID tweetId by the user userId. Each call to this function will be made with a
 * unique tweetId.  List<Integer> getNewsFeed(int userId) Retrieves the 10 most recent tweet IDs in
 * the user's news feed. Each item in the news feed must be posted by users who the user followed or
 * by the user themself. Tweets must be ordered from most recent to least recent.  void follow(int
 * followerId, int followeeId) The user with ID followerId started following the user with ID
 * followeeId.  void unfollow(int followerId, int followeeId) The user with ID followerId started
 * unfollowing the user with ID followeeId.
 *
 * Example 1:
 *
 * Input
 * ["Twitter", "postTweet", "getNewsFeed", "follow", "postTweet", "getNewsFeed", "unfollow", "getNewsFeed"]
 * [[], [1, 5], [1], [1, 2], [2, 6], [1], [1, 2], [1]]
 * Output
 * [null, null, [5], null, null, [6, 5], null, [5]]
 *
 * Explanation
 * Twitter twitter = new Twitter();
 * twitter.postTweet(1, 5); // User 1 posts a new tweet (id = 5).
 * twitter.getNewsFeed(1);  // User 1's news feed should return a list with 1 tweet id -> [5]. return [5]
 * twitter.follow(1, 2);    // User 1 follows user 2.
 * twitter.postTweet(2, 6); // User 2 posts a new tweet (id = 6).
 * twitter.getNewsFeed(1);  // User 1's news feed should return a list with 2 tweet ids -> [6, 5].
 * Tweet id 6 should precede tweet id 5 because it is posted after tweet id 5.
 * twitter.unfollow(1, 2);  // User 1 unfollows user 2.
 * twitter.getNewsFeed(1);  // User 1's news feed should return a list with 1 tweet id -> [5], since
 * user 1 is no longer following user 2.
 *
 * Constraints:
 *
 * 1 <= userId, followerId, followeeId <= 500
 * 0 <= tweetId <= 104
 * All the tweets have unique IDs.
 * At most 3 * 104 calls will be made to postTweet, getNewsFeed, follow, and unfollow.
 */
/**
 * From dicussion.
 */
class Twitter {
private:
    vector<pair<int,int>>posts;
    unordered_map<int, unordered_map<int, int>>follows;
public:
    Twitter() {}

    void postTweet(int userId, int tweetId) {
        posts.push_back(make_pair(userId, tweetId));
    }

    vector<int> getNewsFeed(int userId) {
        vector<int>feed;
        int count = 0;
        for(int i = posts.size() - 1; i >= 0 && count < 10; i--)
            if(posts[i].first == userId || follows[userId][posts[i].first])
                feed.push_back(posts[i].second), count++;
        return feed;
    }

    void follow(int followerId, int followeeId) {
        follows[followerId][followeeId] = 1;
    }

    void unfollow(int followerId, int followeeId) {
        follows[followerId][followeeId] = 0;
    }
};

/**
 * Original solution. We can impove this by make this more OO
 */
class Twitter {
public:
    struct User
    {
        User() : userId{0} {}
        User(int uid) : userId{uid} {}
        int userId;
        unordered_set<int> followers;
        deque<vector<int>> recent_tweets;
    };

    /** Initialize your data structure here. */
    Twitter() {

    }

    /** Compose a new tweet. */
    void postTweet(int userId, int tweetId) {
        if (0 == user_tweets.count(userId)) {
            user_tweets[userId] = User(userId);
        }

        user_tweets[userId].recent_tweets.push_front({seq, userId, tweetId});
        for (auto const& u : user_tweets[userId].followers) {
            user_tweets[u].recent_tweets.push_front({seq, userId, tweetId});
        }
        ++seq;
    }

    /** Retrieve the 10 most recent tweet ids in the user's news feed. Each item in the news feed must be posted by users who the user followed or by the user herself. Tweets must be ordered from most recent to least recent. */
    vector<int> getNewsFeed(int userId) {
        if (0 == user_tweets.count(userId)) {
            return {};
        }

        vector<int> ans;
        auto size = user_tweets[userId].recent_tweets.size();
        auto const& recent_tweets = user_tweets[userId].recent_tweets;
        for (int i = 0; i < 10 && i < size; ++i) {
            ans.push_back(recent_tweets[i][2]);
        }

        return ans;
    }

    /** Follower follows a followee. If the operation is invalid, it should be a no-op. */
    void follow(int followerId, int followeeId) {
        if (0 == user_tweets.count(followerId)) {
            user_tweets[followerId] = User(followerId);
        }
        if (0 == user_tweets.count(followeeId)) {
            user_tweets[followeeId] = User(followeeId);
        }

        auto& followers = user_tweets[followeeId].followers;
        if (followers.count(followerId)) {
            return;
        }

        followers.insert(followerId);

        auto& follower_tweets = user_tweets[followerId].recent_tweets;
        for (auto const& tw : user_tweets[followeeId].recent_tweets) {
            if (followeeId == tw[1]) {
                follower_tweets.push_back(tw);
            }
        }

        sort(begin(follower_tweets), end(follower_tweets),
            [](auto const& a, auto const& b) {
                return a > b;
            });
    }

    /** Follower unfollows a followee. If the operation is invalid, it should be a no-op. */
    void unfollow(int followerId, int followeeId) {
        if (0 == user_tweets.count(followerId)) {
            user_tweets[followerId] = User(followerId);
        }
        if (0 == user_tweets.count(followeeId)) {
            user_tweets[followeeId] = User(followeeId);
        }

        auto& followers = user_tweets[followeeId].followers;
        followers.erase(followerId);

        auto& recent_tweets = user_tweets[followerId].recent_tweets;
        auto new_end = remove_if(begin(recent_tweets), end(recent_tweets),
                 [followeeId](auto const& tw) {
                     return followeeId == tw[1];
                 });
        recent_tweets.erase(new_end, end(recent_tweets));
    }

private:
    unordered_map<int, User> user_tweets;
    int seq = 0;
};

/**
 * Your Twitter object will be instantiated and called as such:
 * Twitter* obj = new Twitter();
 * obj->postTweet(userId,tweetId);
 * vector<int> param_2 = obj->getNewsFeed(userId);
 * obj->follow(followerId,followeeId);
 * obj->unfollow(followerId,followeeId);
 */
