/**
 * Maximum Subarray Min-Product
 *
 * The min-product of an array is equal to the minimum value in the array multiplied by the array's
 * sum.
 *
 * For example, the array [3,2,5] (minimum value is 2) has a min-product of 2 * (3+2+5) = 2 * 10 =
 * 20.  Given an array of integers nums, return the maximum min-product of any non-empty subarray of
 * nums. Since the answer may be large, return it modulo 109 + 7.
 *
 * Note that the min-product should be maximized before performing the modulo operation. Testcases
 * are generated such that the maximum min-product without modulo will fit in a 64-bit signed
 * integer.
 *
 * A subarray is a contiguous part of an array.
 *
 * Example 1:
 *
 * Input: nums = [1,2,3,2]
 * Output: 14
 * Explanation: The maximum min-product is achieved with the subarray [2,3,2] (minimum value is 2).
 * 2 * (2+3+2) = 2 * 7 = 14.
 * Example 2:
 *
 * Input: nums = [2,3,3,1,2]
 * Output: 18
 * Explanation: The maximum min-product is achieved with the subarray [3,3] (minimum value is 3).
 * 3 * (3+3) = 3 * 6 = 18.
 * Example 3:
 *
 * Input: nums = [3,1,5,6,4,2]
 * Output: 60
 * Explanation: The maximum min-product is achieved with the subarray [5,6,4] (minimum value is 4).
 * 4 * (5+6+4) = 4 * 15 = 60.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 105
 * 1 <= nums[i] <= 107
 */
/**
 * The direction is correct. But we need a more efficient to find out the boundary of the subarray.
 * Therefore we use monostack.
 */
class Solution {
public:
    int maxSumMinProduct(vector<int>& nums) {
        int const mod = 1'000'000'007;
        int const NS = nums.size();
        int64_t ans = 0;
        stack<int> stk;
        vector<int64_t> psum(NS + 1, 0);
        copy(nums.begin(), nums.end(), psum.begin() + 1);
        partial_sum(psum.begin(), psum.end(), psum.begin());

        for (int i = 0; i <= NS; ++i) {
            while (!stk.empty() && (i == NS || nums[stk.top()] > nums[i])) {
                int const min_idx = stk.top();
                stk.pop();
                int const L = stk.empty() ? 0 : stk.top() + 1;
                ans = max(ans, nums[min_idx] * (psum[i] - psum[L]));
            }

            stk.push(i);
        }

        return ans % mod;
    }
};

/**
 * Original solution.
 * Idea is, we treat each element of nums as the minimum value of the subarray.
 * Expand the subarray to its max length.
 *
 * But sadly TLE.
 */
class Solution {
public:
    int maxSumMinProduct(vector<int>& nums) {
        int64_t const vec_sum = accumulate(nums.begin(), nums.end(), 0LL);
        int const mod = 1'000'000'007;
        int64_t ans = 0;
        int const NS = nums.size();
        for (int i = 0; i < NS; ++i) {
            if (1 == nums[i]) {
                ans = max(ans, vec_sum);
                continue;
            }

            int L = i - 1;
            int R = i + 1;
            int64_t subsum = nums[i];

            while (L >= 0) {
                if (nums[L] < nums[i]) {
                    break;
                }
                subsum += nums[L];
                --L;
            }

            while (R < NS) {
                if (nums[R] < nums[i]) {
                    break;
                }
                subsum += nums[R];
                ++R;
            }

            subsum *= nums[i];

            ans = max(ans, subsum);
        }

        return ans % mod;
    }
};
