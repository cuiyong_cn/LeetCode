/**
 * Merge Strings Alternately
 *
 * You are given two strings word1 and word2. Merge the strings by adding letters in alternating
 * order, starting with word1. If a string is longer than the other, append the additional letters
 * onto the end of the merged string.
 *
 * Return the merged string.
 *
 * Example 1:
 *
 * Input: word1 = "abc", word2 = "pqr"
 * Output: "apbqcr"
 * Explanation: The merged string will be merged as so:
 * word1:  a   b   c
 * word2:    p   q   r
 * merged: a p b q c r
 * Example 2:
 *
 * Input: word1 = "ab", word2 = "pqrs"
 * Output: "apbqrs"
 * Explanation: Notice that as word2 is longer, "rs" is appended to the end.
 * word1:  a   b
 * word2:    p   q   r   s
 * merged: a p b q   r   s
 * Example 3:
 *
 * Input: word1 = "abcd", word2 = "pq"
 * Output: "apbqcd"
 * Explanation: Notice that as word1 is longer, "cd" is appended to the end.
 * word1:  a   b   c   d
 * word2:    p   q
 * merged: a p b q c   d
 *
 * Constraints:
 *
 * 1 <= word1.length, word2.length <= 100
 * word1 and word2 consist of lowercase English letters.
 */
/**
 * Shorter version.
 */
class Solution {
public:
    string mergeAlternately(string w1, string w2) {
        string res = "";
        for (int i = 0; i < w1.size() || i < w2.size(); ++i) {
            if (i < w1.size())
                res.push_back(w1[i]);
            if (i < w2.size())
                res.push_back(w2[i]);
        }
        return res;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    string mergeAlternately(string word1, string word2) {
        int const L1 = word1.length();
        int const L2 = word2.length();

        string ans(L1 + L2, 'a');
        int const L = min(L1, L2);
        int i = 0;
        for (; i < L; ++i) {
            int const slot = 2 << i;
            ans[slot] = word1[i];
            ans[slot + 1] = word2[i];
        }

        if (i < L1) {
            copy(word1.begin() + i, word1.end(), ans.begin() + 2 * i);
        }
        if (i < L2) {
            copy(word2.begin() + i, word2.end(), ans.begin() + 2 * i);
        }

        return ans;
    }
};
