/**
 * Battleships in a Board
 *
 * Given an 2D board, count how many battleships are in it. The battleships are
 * represented with 'X's, empty slots are represented with '.'s. You may
 * assume the following rules:
 *
 * You receive a valid board, made of only battleships or empty slots.
 * Battleships can only be placed horizontally or vertically. In other words,
 * they can only be made of the shape 1xN (1 row, N columns) or Nx1 (N rows, 1 column),
 * where N can be of any size.
 * At least one horizontal or vertical cell separates between two battleships
 * - there are no adjacent battleships.
 *
 * Example:
 *
 * X..X
 * ...X
 * ...X
 *
 * In the above board there are 2 battleships.
 *
 * Invalid Example:
 *
 * ...X
 * XXXX
 * ...X
 *
 * This is an invalid board that you will not receive - as battleships will always
 * have a cell separating between them.
 *
 * Follow up:
 * Could you do it in one-pass, using only O(1) extra memory and without modifying
 * the value of the board?
 */
class Solution {
public:
    int countBattleships(vector<vector<char>>& board) {
            if (board.empty() || board[0].empty()) { return 0; }
            int m = board.size(), n = board[0].size(), cnt = 0;

            for (int r = 0; r < m; r++)
                for (int c = 0; c < n; c++)
                    cnt += board[r][c] == 'X' && (r == 0 || board[r - 1][c] != 'X')
                        && (c == 0 || board[r][c - 1] != 'X');

            return cnt;
    }
};

/**
 * Same asa the previous one, just more clear.
 */
class Solution {
public:
    int countBattleships(vector<vector<char>>& board)
    {
        if(board.empty() || board.front().empty()) return 0;

        const size_t rowCount = board.size();
        const size_t colCount = board.front().size();

        int count{0};
        for(size_t y = 0; y < rowCount; ++y) {
            for(size_t x = 0; x < colCount; ++x) {
                if(board[y][x] == 'X') {
                    if(y > 0 && board[y - 1][x] == 'X') continue;
                    if(x > 0 && board[y][x - 1] == 'X') continue;
                    ++count;
                }
            }
        }
        return count;
    }
};
