/**
 * Reorganize String
 *
 * Given a string S, check if the letters can be rearranged so that two characters that are adjacent to each other are
 * not the same.
 *
 * If possible, output any possible result.  If not possible, return the empty string.
 *
 * Example 1:
 *
 * Input: S = "aab"
 * Output: "aba"
 *
 * Example 2:
 *
 * Input: S = "aaab"
 * Output: ""
 *
 * Note:
 *     S will consist of lowercase letters and have length in range [1, 500].
 */
/**
 * Intresting method from discussion.
 */
class Solution {
public:
    string reorganizeString(string S) {
        vector<int> mp(26);
        int n = S.size();

        for (auto c: S) {
            ++mp[c-'a'];
        }

        int limit = (n + 1) / 2;
        vector<pair<int, char>> charCounts;
        for (int i = 0; i < 26; ++i) {
            if (mp[i] > limit) {
                return "";
            }

            if (mp[i]) {
                charCounts.push_back({mp[i], i+'a'});
            }
        }

        sort(S.begin(), S.end());

        string ans;
        for (int i = 0, j = limit; i <= (limit - 1); ++i, ++j) {
            ans += S[i];
            if (j < n) ans += S[j];
        }

        return ans;
    }
};

/**
 * Slightly different compared to the solution below.
 */
class Solution {
public:
    string reorganizeString(string S) {
        using CharCount = std::pair<int, char>;
        std::array<int, 26> count{0};
        for (auto c : S) {
            ++count[c - 'a'];
        }

        std::priority_queue<CharCount, std::vector<CharCount>, std::greater<CharCount>> pq;
        for (size_t i = 0; i < count.size(); ++i) {
            if (count[i] > 0) {
                pq.push({count[i], 'a' + i});
            }
        }

        int N = S.size();
        int limit = (N + 1) / 2;
        int t = 1;
        while (!pq.empty()) {
            auto c = pq.top(); pq.pop();
            if (c.first > limit) {
                return "";
            }

            for (size_t i = 0; i < c.first; ++i) {
                if (t >= N) {
                    t = 0;
                }
                S[t] = c.second;
                t += 2;
            }
        }

        return S;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    string reorganizeString(string S) {
        std::array<int, 26> count{0};
        for (auto c : S) {
            ++count[c - 'a'];
        }

        std::priority_queue<std::pair<int, char>> pq;
        for (size_t i = 0; i < count.size(); ++i) {
            if (count[i]) {
                pq.push({count[i], 'a' + i});
            }
        }

        string ans;
        while (pq.size() > 1) {
            auto a = pq.top(); pq.pop();
            auto b = pq.top(); pq.pop();
            ans += a.second; --a.first;
            ans += b.second; --b.first;

            if (a.first > 0) {
                pq.push(a);
            }
            if (b.first > 0) {
                pq.push(b);
            }
        }

        if (!pq.empty()) {
            auto c = pq.top(); pq.pop();
            for (size_t i = 0; i < c.first; ++i) {
                ans += c.second;
            }
        }

        return ans.size() > 1 ?
            (ans[ans.size() - 1] == ans[ans.size() - 2] ? "" : ans) : ans;
    }
};
