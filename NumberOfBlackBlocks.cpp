/**
 * Number of Black Blocks
 *
 * You are given two integers m and n representing the dimensions of a 0-indexed m x n grid.
 *
 * You are also given a 0-indexed 2D integer matrix coordinates, where
 * coordinates[i] = [x, y] indicates that the cell with coordinates [x, y] is
 * colored black. All cells in the grid that do not appear in coordinates are
 * white.
 *
 * A block is defined as a 2 x 2 submatrix of the grid. More formally, a block
 * with cell [x, y] as its top-left corner where 0 <= x < m - 1 and 0 <= y < n -
 * 1 contains the coordinates [x, y], [x + 1, y], [x, y + 1], and [x + 1, y +
 * 1].
 *
 * Return a 0-indexed integer array arr of size 5 such that arr[i] is the number
 * of blocks that contains exactly i black cells.
 *
 * Example 1:
 *
 * Input: m = 3, n = 3, coordinates = [[0,0]]
 * Output: [3,1,0,0,0]
 * Explanation: The grid looks like this:
 *
 * There is only 1 block with one black cell, and it is the block starting with cell [0,0].
 * The other 3 blocks start with cells [0,1], [1,0] and [1,1]. They all have zero black cells.
 * Thus, we return [3,1,0,0,0].
 * Example 2:
 *
 * Input: m = 3, n = 3, coordinates = [[0,0],[1,1],[0,2]]
 * Output: [0,2,2,0,0]
 * Explanation: The grid looks like this:
 *
 * There are 2 blocks with two black cells (the ones starting with cell coordinates [0,0] and [0,1]).
 * The other 2 blocks have starting cell coordinates of [1,0] and [1,1]. They both have 1 black cell.
 * Therefore, we return [0,2,2,0,0].
 *
 * Constraints:
 *
 * 2 <= m <= 105
 * 2 <= n <= 105
 * 0 <= coordinates.length <= 104
 * coordinates[i].length == 2
 * 0 <= coordinates[i][0] < m
 * 0 <= coordinates[i][1] < n
 * It is guaranteed that coordinates contains pairwise distinct coordinates.
 */
/**
 * Original solution.
 */
class Solution {
public:
    vector<long long> countBlackBlocks(int m, int n, vector<vector<int>>& coordinates) {
        auto BB_of_block = unordered_map<int64_t, int32_t>{};
        auto blkid = [m = m - 1, n = n - 1](int64_t x, int64_t y) {
            if (x < 0 || x >= m || y < 0 || y >= n) {
                return int64_t{-1};
            }

            return (x << 32) | y;
        };

        for (auto const& coord : coordinates) {
            auto [x, y] = make_tuple(coord[0], coord[1]);
            ++BB_of_block[blkid(x, y)];
            ++BB_of_block[blkid(x, y - 1)];
            ++BB_of_block[blkid(x - 1, y)];
            ++BB_of_block[blkid(x - 1, y - 1)];
        }

        BB_of_block.erase(-1);

        auto ans = vector<long long>(5, 0);

        for (auto const& [bid, cnt] : BB_of_block) {
            ++ans[cnt];
        }

        auto const total_blocks = 1LL * (m - 1) * (n - 1);

        ans[0] = total_blocks - BB_of_block.size();

        return ans;
    }
};
