/**
 * Minimum Operations to Make the Array Alternating
 *
 * You are given a 0-indexed array nums consisting of n positive integers.
 *
 * The array nums is called alternating if:
 *
 * nums[i - 2] == nums[i], where 2 <= i <= n - 1.
 * nums[i - 1] != nums[i], where 1 <= i <= n - 1.
 * In one operation, you can choose an index i and change nums[i] into any positive integer.
 *
 * Return the minimum number of operations required to make the array alternating.
 *
 * Example 1:
 *
 * Input: nums = [3,1,3,2,4,3]
 * Output: 3
 * Explanation:
 * One way to make the array alternating is by converting it to [3,1,3,1,3,1].
 * The number of operations required in this case is 3.
 * It can be proven that it is not possible to make the array alternating in less than 3 operations.
 * Example 2:
 *
 * Input: nums = [1,2,2,2,2]
 * Output: 2
 * Explanation:
 * One way to make the array alternating is by converting it to [1,2,1,2,1].
 * The number of operations required in this case is 2.
 * Note that the array cannot be converted to [2,2,2,2,2] because in this case nums[0] == nums[1] which violates the conditions of an alternating array.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 105
 * 1 <= nums[i] <= 105
 */
/**
 * Only use top2 element.
 */
auto count(vector<int> const& nums, int s)
{
    auto ans = unordered_map<int, int>{};
    for (auto i = s; i < nums.size(); i += 2) {
        ++ans[nums[i]];
    }
    return ans;
}

auto top2_freq(unordered_map<int, int> const& freq)
{
    auto first = pair(0, 0);
    auto second = first;

    for (auto const& f : freq) {
        if (f.second > first.second) {
            second = first;
            first = f;
        }
        else if (f.second > second.second){
            second = f;
        }
    }
    return pair(first, second);
}

class Solution {
public:
    int minimumOperations(vector<int>& nums) {
        auto odd_idx_num_cnt = count(nums, 0);
        auto even_idx_num_cnt = count(nums, 1);

        auto top2_odd = top2_freq(odd_idx_num_cnt);
        auto top2_even = top2_freq(even_idx_num_cnt);

        if (top2_odd.first.first != top2_even.first.first) {
            return nums.size() - (top2_odd.first.second + top2_even.first.second);
        }

        return nums.size() - max(top2_odd.first.second + top2_even.second.second,
                                 top2_odd.second.second + top2_even.first.second);
    }
};


/**
 * Original solution. This is correct one but TLE.
 */
unordered_map<int, int> count(vector<int> const& nums, int s)
{
    auto ans = unordered_map<int, int>{};
    for (auto i = s; i < nums.size(); i += 2) {
        ++ans[nums[i]];
    }
    return ans;
}

class Solution {
public:
    int minimumOperations(vector<int>& nums) {
        auto odd_idx_num_cnt = count(nums, 0);
        auto even_idx_num_cnt = count(nums, 1);
        auto cmp = [](auto const& a, auto const& b) {
            return a.second < b.second;
        };

        auto max_keep = 0;
        for (auto const& a : odd_idx_num_cnt) {
            for (auto const& b : even_idx_num_cnt) {
                if (a.first != b.first) {
                    max_keep = max(max_keep, a.second + b.second);
                }
            }
        }

        return nums.size() - max_keep;
    }
};
