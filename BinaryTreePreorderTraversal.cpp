/**
 * Binary Tree Preorder Traversal
 *
 * Given a binary tree, return the preorder traversal of its nodes' values.
 *
 * Example:
 *
 * Input: [1,null,2,3]
 *    1
 *     \
 *      2
 *     /
 *    3
 *
 * Output: [1,2,3]
 *
 * Follow up: Recursive solution is trivial, could you do it iteratively?
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/**
 * Iterative version.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<int> ans;
    vector<int> preorderTraversal(TreeNode* root) {
        stack<TreeNode*> stk;
        if (nullptr != root) {
            stk.push(root);
        }

        while (false == stk.empty()) {
            TreeNode* t = stk.top();
            stk.pop();
            ans.push_back(t->val);
            if (nullptr != t->right) {
                stk.push(t->right);
            }
            if (nullptr != t->left) {
                stk.push(t->left);
            }
        }
        return ans;
    }
};

/**
 * Original solution. Recursive solution.
 */
class Solution {
public:
    vector<int> ans;
    void preorder(TreeNode* r) {
        if (nullptr == r) return;
        ans.push_back(r->val);
        preorder(r->left);
        preorder(r->right);
    }

    vector<int> preorderTraversal(TreeNode* root) {
        preorder(root);
        return ans;
    }
};
