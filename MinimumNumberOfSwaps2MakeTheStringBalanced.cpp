/**
 * Minimum Number of Swaps to Make the String Balanced
 *
 * You are given a 0-indexed string s of even length n. The string consists of exactly n / 2 opening
 * brackets '[' and n / 2 closing brackets ']'.
 *
 * A string is called balanced if and only if:
 *
 * It is the empty string, or
 * It can be written as AB, where both A and B are balanced strings, or
 * It can be written as [C], where C is a balanced string.
 * You may swap the brackets at any two indices any number of times.
 *
 * Return the minimum number of swaps to make s balanced.
 *
 * Example 1:
 *
 * Input: s = "][]["
 * Output: 1
 * Explanation: You can make the string balanced by swapping index 0 with index 3.
 * The resulting string is "[[]]".
 * Example 2:
 *
 * Input: s = "]]][[["
 * Output: 2
 * Explanation: You can do the following to make the string balanced:
 * - Swap index 0 with index 4. s = "[]][][".
 * - Swap index 1 with index 5. s = "[[][]]".
 * The resulting string is "[[][]]".
 * Example 3:
 *
 * Input: s = "[]"
 * Output: 0
 * Explanation: The string is already balanced.
 *
 * Constraints:
 *
 * n == s.length
 * 2 <= n <= 106
 * n is even.
 * s[i] is either '[' or ']'.
 * The number of opening brackets '[' equals n / 2, and the number of closing brackets ']' equals n / 2.
 */
/**
 * Optimize. Idea is:
 * We can fist eliminate out the already matched pair.
 * Then what left is somthing like "]]]]]]]]]]]]][[[[[[[[[[[[["
 * And we can swap length / 2 time to make it balanced.
 */
class Solution {
public:
    int minSwaps(string s) {
        int stack_size = 0;
        for (auto const& c : s) {
            if ('[' == c) {
                ++stack_size;
            }
            else {
                stack_size -= stack_size > 0 ? 1 : 0;
            }
        }

        return (stack_size + 1) >> 1;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int minSwaps(string s) {
        int head = 0;
        int tail = 0;
        int ans = 0;

        for (int i = 0, j = s.length() - 1; i < j; ++i) {
            head += '[' == s[i] ? 1 : -1;
            if (head < 0) {
                for (; j > i; --j) {
                    tail += '[' == s[j] ? 1 : -1;
                    if (tail > 0) {
                        break;
                    }
                }

                swap(s[i], s[j]);
                head = 1;
                tail = 0;
                ++ans;
            }
        }

        return ans;
    }
};
