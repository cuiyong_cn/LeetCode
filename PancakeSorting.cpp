/**
 * Pancake Sorting
 *
 * Given an array A, we can perform a pancake flip: We choose some positive integer
 * k <= A.length, then reverse the order of the first k elements of A.  We want to
 * perform zero or more pancake flips (doing them one after another in succession)
 * to sort the array A.

 * Return the k-values corresponding to a sequence of pancake flips that sort A.
 * Any valid answer that sorts the array within 10 * A.length flips will
 * be judged as correct.
 *
 * Example 1:
 *
 * Input: [3,2,4,1]
 * Output: [4,2,4,3]
 * Explanation:
 * We perform 4 pancake flips, with k values 4, 2, 4, and 3.
 * Starting state: A = [3, 2, 4, 1]
 * After 1st flip (k=4): A = [1, 4, 2, 3]
 * After 2nd flip (k=2): A = [4, 1, 2, 3]
 * After 3rd flip (k=4): A = [3, 2, 1, 4]
 * After 4th flip (k=3): A = [1, 2, 3, 4], which is sorted.
 *
 * Example 2:
 *
 * Input: [1,2,3]
 * Output: []
 * Explanation: The input is already sorted, so there is no need to flip anything.
 * Note that other answers, such as [3, 3], would also be accepted.
 *
 * Note:
 *
 *     1 <= A.length <= 100
 *     A[i] is a permutation of [1, 2, ..., A.length]
 */
/**
 * 1. Find the index i of the largest element in subarray A[0] .. A[n]
 * 2. Flip the Array A from index 0 to index i. We put the largest element in A[0]
 * 3. Flip the Array A from index 0 to n. We put the largest element in correct position
 * 4. subtract n by 1 and Repeat this process with subarray A[0] .. A[n - 1] unti the
 *    array is sorted.
 *
 * We use 2 flips to put one element in correct location. So the total flips will be 2n
 */
class Solution {
    bool ArraySorted(vector<int>& A, int endIndex) {
        for (int i = 0; i < endIndex; ++i) {
            if (A[i] > A[i + 1]) return false;
        }
        return true;
    }

    int FindMaxElementIndex(vector<int>& A, int endIndex) {
        int max = A[0];
        int index = 0;
        for (int i = 1; i < endIndex; ++i) {
            if (max < A[i]) {
                max = A[i];
                index = i;
            }
        }
        return index;
    }

    void Flip(vector<int>& A, int endIndex) {
        for (int i = 0, j = endIndex; i < j; ++i, --j) {
            swap(A[i], A[j]);
        }
    }
public:
    vector<int> pancakeSort(vector<int>& A) {
        vector<int> ans;
        int n = A.size();
        while (ArraySorted(A, n - 1) == false) {
            int index = FindMaxElementIndex(A, n);
            ans.push_back(index + 1);
            ans.push_back(n);
            Flip(A, index);
            Flip(A, n - 1);
            --n;
        }
        return ans;
    }
};
