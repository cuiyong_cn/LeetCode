/**
 * Repeated DNA Sequences
 *
 * The DNA sequence is composed of a series of nucleotides abbreviated as 'A', 'C', 'G', and 'T'.
 *
 * For example, "ACGAATTCCG" is a DNA sequence.
 * When studying DNA, it is useful to identify repeated sequences within the DNA.
 *
 * Given a string s that represents a DNA sequence, return all the 10-letter-long sequences
 * (substrings) that occur more than once in a DNA molecule. You may return the answer in any order.
 *
 * Example 1:
 *
 * Input: s = "AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT"
 * Output: ["AAAAACCCCC","CCCCCAAAAA"]
 * Example 2:
 *
 * Input: s = "AAAAAAAAAAAAA"
 * Output: ["AAAAAAAAAA"]
 *
 * Constraints:
 *
 * 1 <= s.length <= 105
 * s[i] is either 'A', 'C', 'G', or 'T'.
 */
/**
 * The key is to encode the length 10 dna sub into more compact format to avoid memory excess.
 * check the octal of 'A', 'C', 'G', 'T', we can see that the last digit are different.
 * So length 10 dna can be encode in a single int. (3 bits * 10 = 30 bits)
 */
class Solution {
public:
    vector<string> findRepeatedDnaSequences(string s) {
        unordered_map<unsigned int, int> m;
        vector<string> r;

        for (unsigned int t = 0, i = 0; i < s.size(); i++) {
            if (m[t = t << 3 & 0x3FFFFFFF | s[i] & 7]++ == 1) {
                r.push_back(s.substr(i - 9, 10));
            }
        }

        return r;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<string> findRepeatedDnaSequences(string s) {
        if (s.length() <= 10) {
            return {};
        }

        unordered_map<string_view, int> dna_seq_count;

        size_t const sub_len = 10;
        auto const end = s.length() - 10 + 1;
        for (size_t i = 0; i < end; ++i) {
            string_view dna_sub(&s[i], sub_len);
            ++dna_seq_count[dna_sub];
        }

        vector<string> ans;
        for (auto const& p : dna_seq_count) {
            if (p.second > 1) {
                ans.push_back(string{p.first});
            }
        }

        return ans;
    }
};
