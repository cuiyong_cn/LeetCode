/**
 * Longest Nice Substring
 *
 * A string s is nice if, for every letter of the alphabet that s contains, it appears both in
 * uppercase and lowercase. For example, "abABB" is nice because 'A' and 'a' appear, and 'B' and 'b'
 * appear. However, "abA" is not because 'b' appears, but 'B' does not.
 *
 * Given a string s, return the longest substring of s that is nice. If there are multiple, return
 * the substring of the earliest occurrence. If there are none, return an empty string.
 *
 * Example 1:
 *
 * Input: s = "YazaAay"
 * Output: "aAa"
 * Explanation: "aAa" is a nice string because 'A/a' is the only letter of the alphabet in s, and
 * both 'A' and 'a' appear.
 * "aAa" is the longest nice substring.
 * Example 2:
 *
 * Input: s = "Bb"
 * Output: "Bb"
 * Explanation: "Bb" is a nice string because both 'B' and 'b' appear. The whole string is a substring.
 * Example 3:
 *
 * Input: s = "c"
 * Output: ""
 * Explanation: There are no nice substrings.
 *
 * Constraints:
 *
 * 1 <= s.length <= 100
 * s consists of uppercase and lowercase English letters.
 */
/**
 * O(n) solution. Idea is:
 * We split the string at mismatched character.
 * And check the string between consecutive split position.
 */
class Solution {
public:
    string longestNiceSubstring(string s) {
        return dfs(s);
    }

private:
    string dfs(string_view s)
    {
        int const N = s.length();
        if (N < 2) {
            return "";
        }

        array<int, 26> upper = { 0, };
        array<int, 26> lower = { 0, };

        for (auto const& c : s) {
            if (isupper(c)) {
                upper[c - 'A'] = 1;
            }
            else {
                lower[c - 'a'] = 1;
            }
        }

        vector<int> parts = { -1 };
        for (int i = 0; i < N; ++i) {
            auto c = s[i];
            if (1 == (upper[toupper(c) - 'A'] + lower[tolower(c) - 'a'])) {
                parts.emplace_back(i);
            }
        }

        if (parts.size() < 2) {
            return {s.begin(), s.end()};
        }

        parts.emplace_back(N);

        string ans;
        for (int i = 1; i < parts.size(); ++i) {
            auto r = dfs(s.substr(parts[i - 1] + 1, parts[i] - parts[i - 1] - 1));
            if (r.length() > ans.length()) {
                swap(r, ans);
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    string longestNiceSubstring(string s) {
        if (s.length() < 1) {
            return "";
        }

        int const N = s.length();
        for (int L = N; L > 1; --L) {
            int const Left = N - L;
            for (int i = 0; i <= Left; ++i) {
                if (is_nice_string(string_view(s.c_str() + i, L))) {
                    return s.substr(i, L);
                }
            }
        }

        return "";
    }

private:
    bool is_nice_string(string_view s) {
        array<int, 26> upper = { 0, };
        array<int, 26> lower = { 0, };

        for (auto const& c : s) {
            if (isupper(c)) {
                ++upper[c - 'A'];
            }
            else {
                ++lower[c - 'a'];
            }
        }

        int const L = 26;
        for (int i = 0; i < L; ++i) {
            if ((0 == upper[i] && lower[i] > 0)
                || (0 == lower[i] && upper[i] > 0)) {
                return false;
            }
        }

        return true;
    }
};
