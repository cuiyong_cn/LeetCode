/**
 * Reverse Words in a String
 *
 * Given an input string s, reverse the order of the words.
 *
 * A word is defined as a sequence of non-space characters. The words in s will be separated by at
 * least one space.
 *
 * Return a string of the words in reverse order concatenated by a single space.
 *
 * Note that s may contain leading or trailing spaces or multiple spaces between two words. The
 * returned string should only have a single space separating the words. Do not include any extra
 * spaces.
 *
 * Example 1:
 *
 * Input: s = "the sky is blue"
 * Output: "blue is sky the"
 * Example 2:
 *
 * Input: s = "  hello world  "
 * Output: "world hello"
 * Explanation: Your reversed string should not contain leading or trailing spaces.
 * Example 3:
 *
 * Input: s = "a good   example"
 * Output: "example good a"
 * Explanation: You need to reduce multiple spaces between two words to a single space in the reversed string.
 * Example 4:
 *
 * Input: s = "  Bob    Loves  Alice   "
 * Output: "Alice Loves Bob"
 * Example 5:
 *
 * Input: s = "Alice does not even like bob"
 * Output: "bob like even not does Alice"
 *
 * Constraints:
 *
 * 1 <= s.length <= 104
 * s contains English letters (upper-case and lower-case), digits, and spaces ' '.
 * There is at least one word in s.
 *
 * Follow-up: If the string data type is mutable in your language, can you solve it in-place with
 * O(1) extra space?
 */
/**
 * O(1) in-place solution.
 */
class Solution {
public:
    string reverseWords(string s) {
        // compact the string. Remove leading and trailing space. turn multi spaces into one space
        int L = 0, R = 0;
        while (R < s.length()) {
            while (R < s.length() && ' ' == s[R]) {
                ++R;
            }

            while (R < s.length() && ' ' != s[R]) {
                s[L] = s[R];
                ++L; ++R;
            }

            if (R < s.length()) {
                s[L] = s[R];
                ++L; ++R;
            }
        }

        while (L > 0 && ' ' == s[L - 1]) {
            --L;
        }

        s.erase(L);

        // reverse the whole string
        R = L - 1;
        L = 0;
        while (L < R) {
            swap(s[L], s[R]);
            ++L; --R;
        }

        // reverse each word
        L = 0; R = 0;
        while (R < s.length()) {
            while (R < s.length() && ' ' != s[R]) {
                ++R;
            }

            for (int i = L, j = R - 1; i < j; ++i, --j) {
                swap(s[i], s[j]);
            }

            L = ++R;
        }

        return s;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    string reverseWords(string s) {
        istringstream iss{s};
        vector<string> words{istream_iterator<string>(iss), istream_iterator<string>()};

        reverse(begin(words), end(words));

        ostringstream oss;
        copy(begin(words), end(words), ostream_iterator<string>(oss, " "));

        auto ans = oss.str();
        ans.pop_back();

        return ans;
    }
};
