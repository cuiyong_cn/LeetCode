/**
 * Solve the Equation
 *
 * Solve a given equation and return the value of x in the form of string "x=#value". The equation
 * contains only '+', '-' operation, the variable x and its coefficient.
 *
 * If there is no solution for the equation, return "No solution".
 *
 * If there are infinite solutions for the equation, return "Infinite solutions".
 *
 * If there is exactly one solution for the equation, we ensure that the value of x is an integer.
 *
 * Example 1:
 * Input: "x+5-3+x=6+x-2"
 * Output: "x=2"
 * Example 2:
 * Input: "x=x"
 * Output: "Infinite solutions"
 * Example 3:
 * Input: "2x=x"
 * Output: "x=0"
 * Example 4:
 * Input: "2x+3x-6x=x+2"
 * Output: "x=-1"
 * Example 5:
 * Input: "x=x+2"
 * Output: "No solution"
 */
/**
 * Original solution.
 */
class Solution {
public:
    enum EType
    {
        eNum,
        eVar,
        eCount
    };

    string solveEquation(string equation) {
        int eq_pos = equation.find('=');
        auto left = parse(equation.substr(0, eq_pos));
        auto right = parse(equation.substr(eq_pos + 1));

        int num_coef = right[eNum] - left[eNum];
        int var_coef = left[eVar] - right[eVar];

        if (0 == var_coef) {
            return 0 == num_coef ? "Infinite solutions" : "No solution";
        }

        return string("x=") + to_string(num_coef / var_coef);
    }

private:
    vector<int> parse(string equation)
    {
        std::vector<int> items(eCount, 0);
        string element;
        for (auto ch : equation) {
            if ('x' == ch) {
                if (element.empty() || string("+-").find(element.back()) != string::npos) {
                    element += '1';
                }
                items[eVar] += stoi(element);
                element.clear();
            }
            else if ('+' == ch || '-' == ch) {
                if (!element.empty()) {
                    items[eNum] += stoi(element);
                    element.clear();
                }

                element += ch;
            }
            else {
                element += ch;
            }
        }
        
        if (!element.empty()) {
            items[eNum] += stoi(element);
        }

        return items;
    }
};
