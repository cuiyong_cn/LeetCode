/**
 * Number of Enclaves
 *
 * Given a 2D array A, each cell is 0 (representing sea) or 1 (representing land)
 *
 * A move consists of walking from one land square 4-directionally to another land square, or off the boundary of the grid.
 *
 * Return the number of land squares in the grid for which we cannot walk off the boundary of the grid in any number of moves.
 *
 * Example 1:
 *
 * Input: [[0,0,0,0],[1,0,1,0],[0,1,1,0],[0,0,0,0]]
 * Output: 3
 * Explanation:
 * There are three 1s that are enclosed by 0s, and one 1 that isn't enclosed because its on the boundary.
 *
 * Example 2:
 *
 * Input: [[0,1,1,0],[0,0,1,0],[0,0,1,0],[0,0,0,0]]
 * Output: 0
 * Explanation:
 * All 1s are either on the boundary or can reach the boundary.
 *
 * Note:
 *
 *     1 <= A.length <= 500
 *     1 <= A[i].length <= 500
 *     0 <= A[i][j] <= 1
 *     All rows have the same size.
 */
/**
 * Same idea using bfs method.
 */
class Solution {
public:
    int numEnclaves(vector<vector<int>>& A) {
        int ans = 0;
        queue<pair<int, int> > q;
        vector<int> dir = { -1, 0, 1, 0, -1 };
        int rows = A.size();
        int cols = A[0].size();
        for (auto i = 0; i < rows; ++i)
        for (auto j = 0; j < cols; ++j) {
            ans += A[i][j];
            if (0 == i || 0 == j || i == rows - 1 || j == cols - 1) {
                if (1 == A[i][j]) q.push({i, j});
            }
        }

        while (false == q.empty()) {
            auto x = q.front().first, y = q.front().second;
            q.pop();

            if (1 == A[x][y]) {
                A[x][y] = 0;
                --ans;
                for (int d = 0; d < 4; ++d) {
                    int nr = x + dir[d];
                    int nc = y + dir[d + 1];
                    if (0 <= nr && nr < rows && 0 <= nc && nc < cols) {
                        if (1 == A[nr][nc]) q.push({ nr, nc });
                    }
                }
            }
        }

        return ans;
    }
};


/**
 * My original solution. The idea is:
 *      1. Walking along the boundary, and mark the connected land as "walk off land"
 *      2. Then count the rest connected land as the answer.
 */
class Solution {
public:
    Solution() : dir{ -1, 0, 1, 0, -1 } {}

    int numEnclaves(vector<vector<int>>& A) {
        rows = A.size();
        cols = A[0].size();
        vector<int> rowsVec = { 0, rows - 1 };
        vector<int> colsVec = { 0, cols - 1 };

        for (auto& r : rowsVec) {
            for (int c = 0; c < cols; ++c) {
                if (1 == A[r][c]) {
                    A[r][c] = 0;
                    for (int i = 0; i < 4; ++i) {
                        dfs(A, r + dir[i], c + dir[i + 1], 0);
                    }
                }
            }
        }

        for (auto& c : colsVec) {
            for (int r = 0; r < rows; ++r) {
                if (1 == A[r][c]) {
                    A[r][c] = 0;
                    for (int i = 0; i < 4; ++i) {
                        dfs(A, r + dir[i], c + dir[i + 1], 0);
                    }
                }
            }
        }
        int sum = 0;
        for (int r = 1; r < rows - 1; ++r) {
            for (int c = 1; c < cols - 1; ++c) {
                if (1 == A[r][c]) {
                    ++sum;
                }
            }
        }
        return sum;
    }
private:
    int dfs(vector<vector<int> >& A, int r, int c, int sum) {
        if (0 <= r && r < rows && 0 <= c && c < cols) {
            if (1 == A[r][c]) {
                A[r][c] = 0;
                ++sum;
                for (int i = 0; i < 4; ++i) {
                    sum += dfs(A, r + dir[i], c + dir[i + 1], 0);
                }
            }
        }
        return sum;
    }
    vector<int> dir;
    int rows;
    int cols;
};
