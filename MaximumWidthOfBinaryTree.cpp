/**
 * Maximum Width of Binary Tree
 *
 * Given the root of a binary tree, return the maximum width of the given tree.
 *
 * The maximum width of a tree is the maximum width among all levels.
 *
 * The width of one level is defined as the length between the end-nodes (the leftmost and rightmost
 * non-null nodes), where the null nodes between the end-nodes are also counted into the length
 * calculation.
 *
 * It is guaranteed that the answer will in the range of 32-bit signed integer.
 *
 * Example 1:
 *
 * Input: root = [1,3,2,5,3,null,9]
 * Output: 4
 * Explanation: The maximum width existing in the third level with the length 4 (5,3,null,9).
 * Example 2:
 *
 * Input: root = [1,3,null,5,3]
 * Output: 2
 * Explanation: The maximum width existing in the third level with the length 2 (5,3).
 * Example 3:
 *
 * Input: root = [1,3,2,5]
 * Output: 2
 * Explanation: The maximum width existing in the second level with the length 2 (3,2).
 * Example 4:
 *
 * Input: root = [1,3,2,5,null,null,9,6,null,null,7]
 * Output: 8
 * Explanation: The maximum width existing in the fourth level with the length 8
 * (6,null,null,null,null,null,null,7).
 *
 * Constraints:
 *
 * The number of nodes in the tree is in the range [1, 3000].
 * -100 <= Node.val <= 100
 */
/**
 * reduce the noise.
 */

class Solution {
public:
    int widthOfBinaryTree(TreeNode* root) {
        if (nullptr == root) {
            return 0;
        }

        int max_width = 1;
        vector<pair<unsigned long long, TreeNode*>> cur_level{{1, root}};
        int lvl = 0;
        while (!cur_level.empty()) {
            vector<pair<unsigned long long, TreeNode*>> next_level;
            for (auto [len, node] : cur_level) {
                if (node) {
                    if (node->left) {
                        next_level.push_back({1, node->left});
                    }
                    else {
                        append_or_update_back(next_level, 1);
                    }
                    if (node->right) {
                        next_level.push_back({1, node->right});
                    }
                    else {
                        append_or_update_back(next_level, 1);
                    }
                }
                else {
                    append_or_update_back(next_level, 2 * len);
                }
            }

            if (!next_level.empty()) {
                while (nullptr == next_level.back().second) {
                    next_level.pop_back();
                }
            }

            int cur_len = 0;
            for (auto [len, node] : next_level) {
                cur_len += len;
            }

            max_width = max(max_width, cur_len);
            swap(cur_level, next_level);
        }

        return max_width;
    }

private:
    void append_or_update_back(vector<pair<unsigned long long, TreeNode*>>& next_level, int len)
    {
        if (!next_level.empty()) {
            if (nullptr == next_level.back().second) {
                next_level.back().first += len;
            }
            else {
                next_level.push_back({len, nullptr});
            }
        }
    }
};


/**
 * Avoid using idx, can be fit for any solution.
 * Idea is merge the consecutie nullptr into one.
 * Very nosiy code. Could be simplified.
 */
class Solution {
public:
    int widthOfBinaryTree(TreeNode* root) {
        if (nullptr == root) {
            return 0;
        }

        int max_width = 1;
        vector<pair<int, TreeNode*>> cur_level{{1, root}};
        int lvl = 0;
        while (!cur_level.empty()) {
            vector<pair<int, TreeNode*>> next_level;
            for (auto [len, node] : cur_level) {
                if (node) {
                    if (node->left) {
                        next_level.push_back({1, node->left});
                    }
                    else if (!next_level.empty()) {
                        if (nullptr == next_level.back().second) {
                            ++next_level.back().first;
                        }
                        else {
                            next_level.push_back({1, nullptr});
                        }
                    }
                    if (node->right) {
                        next_level.push_back({1, node->right});
                    }
                    else if (!next_level.empty()) {
                        if (nullptr == next_level.back().second) {
                            ++next_level.back().first;
                        }
                        else {
                            next_level.push_back({1, nullptr});
                        }
                    }
                }
                else if (!next_level.empty()) {
                    if (nullptr == next_level.back().second) {
                        next_level.back().first += 2 * len;
                    }
                    else {
                        next_level.push_back({2 * len, nullptr});
                    }
                }
            }

            if (!next_level.empty()) {
                while (nullptr == next_level.back().second) {
                    next_level.pop_back();
                }
            }

            int cur_len = 0;
            for (auto [len, node] : next_level) {
                cur_len += len;
            }

            max_width = max(max_width, cur_len);
            swap(cur_level, next_level);
        }

        return max_width;
    }
};

/**
 * Offset the idx to avoid the idx overflow
 */
class Solution {
public:
    int widthOfBinaryTree(TreeNode* root) {
        queue<pair<TreeNode*, int>> q{{{root, 1}}};
        int res = 0;
        while(!q.empty())
        {
            int qs = q.size();
            int l = INT_MAX, r = INT_MIN;
            int offset = q.front().second;
            while(qs--)
            {
                auto t = q.front(); q.pop();
                l = min(l, t.second - offset);
                r = max(r, t.second - offset);
                if (t.first->left) q.push({t.first->left, 2 * (t.second - offset)});
                if (t.first->right) q.push({t.first->right, 2 * (t.second - offset) + 1});
            }
            res = max(res, r - l + 1);
        }
        return res;
    }
};

/**
 * idx overflow one.
 */
class Solution {
public:
    int widthOfBinaryTree(TreeNode* root) {
        if (nullptr == root) {
            return 0;
        }

        int max_width = 1;
        vector<pair<long, TreeNode*>> cur_level{{0, root}};
        while (!cur_level.empty()) {
            vector<pair<long, TreeNode*>> next_level;
            for (auto [idx, node] : cur_level) {
                if (node->left) {
                    next_level.push_back({idx * 2, node->left});
                }
                if (node->right) {
                    next_level.push_back({idx * 2 + 1, node->right});
                }
            }

            if (next_level.size() > 1) {
                max_width = max<int>(max_width, next_level.back().first - next_level.front().first + 1);
            }
            swap(cur_level, next_level);
        }

        return max_width;
    }
};

/**
 * Original solution. TLE solution.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int widthOfBinaryTree(TreeNode* root) {
        if (nullptr == root) {
            return 0;
        }

        int max_width = 1;
        vector<TreeNode*> cur_level{root};
        while (!cur_level.empty()) {
            vector<TreeNode*> next_level;
            for (auto node : cur_level) {
                if (nullptr != node) {
                    next_level.push_back(node->left);
                    next_level.push_back(node->right);
                }
                else if (!next_level.empty()) {
                    next_level.push_back(nullptr);
                    next_level.push_back(nullptr);
                }
            }

            while (!next_level.empty() && nullptr == next_level.back()) {
                next_level.pop_back();
            }

            while (!next_level.empty() && nullptr == next_level.front()) {
                next_level.erase(next_level.begin());
            }

            max_width = max<int>(max_width, next_level.size());
            swap(cur_level, next_level);
        }
        
        return max_width;
    }
};
