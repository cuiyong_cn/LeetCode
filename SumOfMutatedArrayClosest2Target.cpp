/**
 * Sum of Mutated Array Closest to Target
 *
 * Given an integer array arr and a target value target, return the integer value such that when we
 * change all the integers larger than value in the given array to be equal to value, the sum of the
 * array gets as close as possible (in absolute difference) to target.
 *
 * In case of a tie, return the minimum such integer.
 *
 * Notice that the answer is not neccesarilly a number from arr.
 *
 * Example 1:
 *
 * Input: arr = [4,9,3], target = 10
 * Output: 3
 * Explanation: When using 3 arr converts to [3, 3, 3] which sums 9 and that's the optimal answer.
 * Example 2:
 *
 * Input: arr = [2,3,5], target = 10
 * Output: 5
 * Example 3:
 *
 * Input: arr = [60864,25176,27249,21296,20204], target = 56803
 * Output: 11361
 *
 * Constraints:
 *
 * 1 <= arr.length <= 10^4
 * 1 <= arr[i], target <= 10^5
 */
/**
 * From discussion. More simpler version.
 */
class Solution {
public:
    int findBestValue(vector<int>& A, int target) {
        sort(A.begin(), A.end());
        int n = A.size(), i = 0;
        while (i < n && target > A[i] * (n - i)) {
            target -= A[i++];
        }

        return i == n ? A[n - 1] : round((target - 0.0001) / (n - i));
    }
};

/**
 * Original solution. Binary search.
 */
class Solution {
public:
    int findBestValue(vector<int>& arr, int target) {
        sort(std::begin(arr), std::end(arr));
        int avg = (target * 1.0 / arr.size()) + 0.5;
        if (avg <= arr.front()) {
            return avg;
        }

        vector<int> presum(arr.size(), 0);
        partial_sum(std::begin(arr), std::end(arr), std::begin(presum));
        int arr_avg = (presum.back() * 1.0 / arr.size()) + 0.5;
        if (avg >= arr_avg) {
            return arr.back();
        }

        int ans = 0;
        int diff = arr_avg - avg;
        int left = avg, right = arr.back();
        while (left <= right) {
            int mid = (left + right) >> 1;
            auto it = lower_bound(std::begin(arr), std::end(arr), mid);
            int pos = std::distance(std::begin(arr), it);

            int navg = ((presum[pos - 1] + (arr.size() - pos) * mid) * 1.0 / arr.size()) + 0.5;
            int ndiff = abs(navg - avg);
            if (ndiff <= diff) {
                ans = mid;
                diff = ndiff;
            }

            if (navg <= avg) {
                left = mid + 1;
            }
            else {
                right = mid - 1;
            }
        }

        return ans;
    }
};
