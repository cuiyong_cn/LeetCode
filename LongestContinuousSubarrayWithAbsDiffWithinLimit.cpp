/**
 * Longest Continuous Subarray With Absolute Diff Less Than or Equal to Limit
 *
 * Given an array of integers nums and an integer limit, return the size of the longest non-empty subarray such that the
 * absolute difference between any two elements of this subarray is less than or equal to limit.
 *
 * Example 1:
 *
 * Input: nums = [8,2,4,7], limit = 4
 * Output: 2
 * Explanation: All subarrays are:
 * [8] with maximum absolute diff |8-8| = 0 <= 4.
 * [8,2] with maximum absolute diff |8-2| = 6 > 4.
 * [8,2,4] with maximum absolute diff |8-2| = 6 > 4.
 * [8,2,4,7] with maximum absolute diff |8-2| = 6 > 4.
 * [2] with maximum absolute diff |2-2| = 0 <= 4.
 * [2,4] with maximum absolute diff |2-4| = 2 <= 4.
 * [2,4,7] with maximum absolute diff |2-7| = 5 > 4.
 * [4] with maximum absolute diff |4-4| = 0 <= 4.
 * [4,7] with maximum absolute diff |4-7| = 3 <= 4.
 * [7] with maximum absolute diff |7-7| = 0 <= 4.
 * Therefore, the size of the longest subarray is 2.
 * Example 2:
 *
 * Input: nums = [10,1,2,4,7,2], limit = 5
 * Output: 4
 * Explanation: The subarray [2,4,7,2] is the longest since the maximum absolute diff is |2-7| = 5 <= 5.
 * Example 3:
 *
 * Input: nums = [4,2,2,2,4,4,2,2], limit = 0
 * Output: 3
 *
 * Constraints:
 *
 * 1 <= nums.length <= 10^5
 * 1 <= nums[i] <= 10^9
 * 0 <= limit <= 10^9
 */
/**
 * The lazy update of i can be somewhat hard to understand, we can make it more intuitive.
 * But below one I think will be a little more efficient.
 */
class Solution {
public:
    int longestSubarray(vector<int>& A, int limit) {
        deque<int> maxd, mind;
        int ans = 0;

        for (int i =0, j = 0; j < A.size(); ++j) {
            while (!maxd.empty() && A[j] > maxd.back()) {
                maxd.pop_back();
            }

            while (!mind.empty() && A[j] < mind.back()) {
                mind.pop_back();
            }

            maxd.push_back(A[j]);
            mind.push_back(A[j]);

            while (maxd.front() - mind.front() > limit) {
                if (maxd.front() == A[i]) {
                    maxd.pop_front();
                }
                if (mind.front() == A[i]) {
                    mind.pop_front();
                }
                ++i;
            }
            ans = std::max(ans, j - i + 1);
        }

        return ans;
    }
};

/**
 * Two deque solution. Much more efficient and not long.
 */
class Solution {
public:
    int longestSubarray(vector<int>& A, int limit) {
        deque<int> maxd, mind;
        int i = 0, j = 0;
        for (j = 0; j < A.size(); ++j) {
            while (!maxd.empty() && A[j] > maxd.back()) {
                maxd.pop_back();
            }

            while (!mind.empty() && A[j] < mind.back()) {
                mind.pop_back();
            }

            maxd.push_back(A[j]);
            mind.push_back(A[j]);

            if (maxd.front() - mind.front() > limit) {
                if (maxd.front() == A[i]) {
                    maxd.pop_front();
                }
                if (mind.front() == A[i]) {
                    mind.pop_front();
                }
                ++i;
            }
        }

        return j - i;
    }
};

/**
 * From discussion user lee215. Using multiset, but it is slower. Maybe it's because every operation
 * took log(n) time causing this. But the code is incredible shorter.
 */
class Solution {
public:
    int longestSubarray(vector<int>& nums, int limit) {
        int i = 0, j = 0;
        std::multiset<int> m;

        for (; j < nums.size(); ++j) {
            m.insert(nums[j]);
            if ((*m.rbegin() - *m.begin()) > limit) {
                m.erase(m.find(nums[i++]));
            }
        }
        return j - i;
    }
};

/**
 * Original solution. Sliding window.
 */
class Solution {
public:
    int longestSubarray(vector<int>& nums, int limit) {
        int left = 0, right = 0;
        int max_num = nums.front(), min_num = nums.front();
        int ans = 0;

        while (right < nums.size()) {
            while (right < nums.size()) {
                max_num = std::max(max_num, nums[right]);
                min_num = std::min(min_num, nums[right]);

                if ((max_num - min_num) > limit) {
                    break;
                }
                ++right;
            }

            ans = std::max(ans, right - left);

            if (right < nums.size()) {
                max_num = nums[right]; min_num = nums[right];

                for (int i = right - 1; i >= left; --i) {
                    auto max_value = std::max(max_num, nums[i]);
                    auto min_value = std::min(min_num, nums[i]);

                    if ((max_value - min_value) > limit) {
                        left = i + 1;
                        break;
                    }
                    max_num = max_value;
                    min_num = min_value;
                }
            }
        }

        return ans;
    }
};
