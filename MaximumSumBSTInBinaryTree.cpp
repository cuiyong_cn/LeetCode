/**
 * Maximum Sum BST in Binary Tree
 *
 * Given a binary tree root, return the maximum sum of all keys of any sub-tree which is also a
 * Binary Search Tree (BST).
 *
 * Assume a BST is defined as follows:
 *
 * The left subtree of a node contains only nodes with keys less than the node's key.  The right
 * subtree of a node contains only nodes with keys greater than the node's key.
 * Both the left and right subtrees must also be binary search trees.
 *
 * Example 1:
 *
 * Input: root = [1,4,3,2,4,2,5,null,null,null,null,null,null,4,6]
 * Output: 20
 * Explanation: Maximum sum in a valid Binary search tree is obtained in root node with key equal to 3.
 * Example 2:
 *
 * Input: root = [4,3,null,1,2]
 * Output: 2
 * Explanation: Maximum sum in a valid Binary search tree is obtained in a single root node with key equal to 2.
 * Example 3:
 *
 * Input: root = [-4,-2,-5]
 * Output: 0
 * Explanation: All values are negatives. Return an empty BST.
 * Example 4:
 *
 * Input: root = [2,1,3]
 * Output: 6
 * Example 5:
 *
 * Input: root = [5,4,8,3,null,6,3]
 * Output: 7
 *
 * Constraints:
 *
 * The number of nodes in the tree is in the range [1, 4 * 104].
 * -4 * 104 <= Node.val <= 4 * 104
 */
/**
 * More clearer one.
 */
class Solution {
    struct S {
        bool isBST;
        int minVal;
        int maxVal;
        int sum;
    };

    int ans = 0;

    S helper(TreeNode *node) {
        if (nullptr == node) {
            return {true, numeric_limits<int>::max(), numeric_limits<int>::min(), 0};
        }

        auto l = helper(node->left);
        auto r = helper(node->right);
        if (l.isBST && r.isBST && l.maxVal < node->val && r.minVal > node->val) {
            auto sum = l.sum + r.sum + node->val;
            ans = max(ans, sum);
            return {true, min(l.minVal, node->val), max(r.maxVal, node->val), sum};
        }

        return {false, 0, 0, 0};
    }

public:
    int maxSumBST(TreeNode* root) {
        helper(root);
        return ans;
    }
};

/**
 * Original solution. DFS solution. Key points are blow:
 * 1. How to determine a binary tree is also a binary search tree?
 * Ans: max value of left subtree is less the root value.
 *      min value of right subtree is greater thean the root value
 * 2. If a node is a binary search tree root node, we have to return the sum of the tree,
 *    not the maximum sum of the subtree. Cause its parent may reverse the situation.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int maxSumBST(TreeNode* root) {
        dfs(root);
        return ans;
    }
private:
    pair<int, bool> dfs(TreeNode* node) {
        if (nullptr == node) {
            return {0, true};
        }

        auto left = dfs(node->left);
        auto right = dfs(node->right);
        auto max_sum = max(left.first, right.first);
        ans = max(ans, max_sum);

        if (true == left.second && true == right.second && is_binary_search_root(node)) {
            ans = max(ans, left.first + right.first + node->val);
            return {left.first + right.first + node->val, true};
        }

        return {max_sum, false};
    }

    bool is_binary_search_root(TreeNode* node) {
        if (nullptr != node->left) {
            if (node->left->val >= node->val) {
                return false;
            }
            auto lr = node->left->right;
            if (nullptr != lr && lr->val >= node->val) {
                return false;
            }
        }

        if (nullptr != node->right) {
            if (node->right->val <= node->val) {
                return false;
            }
            auto rl = node->right->left;
            if (nullptr != rl && rl->val <= node->val) {
                return false;
            }
        }

        return true;
    }

    int ans = 0;
};
