/**
 * Number of Ways Where Square of Number Is Equal to Product of Two Numbers
 *
 * Given two arrays of integers nums1 and nums2, return the number of triplets formed (type 1 and
 * type 2) under the following rules:
 *
 * Type 1: Triplet (i, j, k) if nums1[i]2 == nums2[j] * nums2[k] where 0 <= i < nums1.length and 0
 * <= j < k < nums2.length.  Type 2: Triplet (i, j, k) if nums2[i]2 == nums1[j] * nums1[k] where 0
 * <= i < nums2.length and 0 <= j < k < nums1.length.
 *
 * Example 1:
 *
 * Input: nums1 = [7,4], nums2 = [5,2,8,9]
 * Output: 1
 * Explanation: Type 1: (1, 1, 2), nums1[1]2 = nums2[1] * nums2[2]. (42 = 2 * 8).
 * Example 2:
 *
 * Input: nums1 = [1,1], nums2 = [1,1,1]
 * Output: 9
 * Explanation: All Triplets are valid, because 12 = 1 * 1.
 * Type 1: (0,0,1), (0,0,2), (0,1,2), (1,0,1), (1,0,2), (1,1,2).  nums1[i]2 = nums2[j] * nums2[k].
 * Type 2: (0,0,1), (1,0,1), (2,0,1). nums2[i]2 = nums1[j] * nums1[k].
 * Example 3:
 *
 * Input: nums1 = [7,7,8,3], nums2 = [1,2,9,7]
 * Output: 2
 * Explanation: There are 2 valid triplets.
 * Type 1: (3,0,2).  nums1[3]2 = nums2[0] * nums2[2].
 * Type 2: (3,0,1).  nums2[3]2 = nums1[0] * nums1[1].
 *
 * Constraints:
 *
 * 1 <= nums1.length, nums2.length <= 1000
 * 1 <= nums1[i], nums2[i] <= 105
 */
/**
 * O(m*n) solution.
 */
class Solution {
public:
    int numTriplets(vector<int>& n1, vector<int>& n2) {
        return countForArray(n1, n2) + countForArray(n2, n1);
    }

    int countForArray(vector<int>& n1, vector<int>& n2) {
        int res = 0, last_res = 0, last_n = 0;
        sort(begin(n1), end(n1));
        for (auto i = 0; i < n1.size(); last_n = n1[i++])
            if (n1[i] == last_n)
                res += last_res;
            else
                res += last_res = twoProduct((long)n1[i] * n1[i], n2);
        return res;
    }

    int twoProduct(long i, vector<int> &n) {
        unordered_map<int, int> m;
        int cnt = 0;
        for (auto v : n) {
            if (i % v == 0)
                cnt += m[i / v];
            ++m[v];
        }
        return cnt;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int numTriplets(vector<int>& nums1, vector<int>& nums2) {
        return type(nums1, nums2) + type(nums2, nums1);
    }

private:
    int type(vector<int> const& n1, vector<int> const& n2)
    {
        int ans = 0;
        unordered_map<int64_t, int> sq_cnt;
        for (int64_t n : n1) {
            ++sq_cnt[n * n];
        }

        int const N = n2.size();
        for (int i = 0; i < N; ++i) {
            int64_t F = n2[i];
            for (int j = i + 1; j < N; ++j) {
                auto mult = F * n2[j];
                if (sq_cnt.count(mult)) {
                    ans += sq_cnt[mult];
                }
            }
        }

        return ans;
    }
};
