/**
 * Odd String Difference
 *
 * You are given an array of equal-length strings words. Assume that the length of each string is n.
 *
 * Each string words[i] can be converted into a difference integer array difference[i] of length n -
 * 1 where difference[i][j] = words[i][j+1] - words[i][j] where 0 <= j <= n - 2. Note that the
 * difference between two letters is the difference between their positions in the alphabet i.e. the
 * position of 'a' is 0, 'b' is 1, and 'z' is 25.
 *
 * For example, for the string "acb", the difference integer array is [2 - 0, 1 - 2] = [2, -1].
 * All the strings in words have the same difference integer array, except one. You should find that string.
 *
 * Return the string in words that has different difference integer array.
 *
 * Example 1:
 *
 * Input: words = ["adc","wzy","abc"]
 * Output: "abc"
 * Explanation:
 * - The difference integer array of "adc" is [3 - 0, 2 - 3] = [3, -1].
 * - The difference integer array of "wzy" is [25 - 22, 24 - 25]= [3, -1].
 * - The difference integer array of "abc" is [1 - 0, 2 - 1] = [1, 1].
 * The odd array out is [1, 1], so we return the corresponding string, "abc".
 * Example 2:
 *
 * Input: words = ["aaa","bob","ccc","ddd"]
 * Output: "bob"
 * Explanation: All the integer arrays are [0, 0] except for "bob", which corresponds to [13, -13].
 *
 * Constraints:
 *
 * 3 <= words.length <= 100
 * n == words[i].length
 * 2 <= n <= 20
 * words[i] consists of lowercase English letters.
 */
/**
 * Compare every diff of every position.
 */
class Solution {
public:
    string oddString(vector<string>& words) {
        auto wlen = words.front().length();
        for (auto n = wlen - 1, i = n * 0; i < n; ++i) {
            auto diff_cnt = map<int, vector<int>>{};
            for (auto wn = words.size(), j = i * 0; j < wn; ++j) {
                auto diff = words[j][i + 1] - words[j][i];
                if (diff_cnt[diff].size() < 2) {
                    diff_cnt[diff].push_back(j);
                }
            }

            if (diff_cnt.size() > 1) {
                for (auto const& dc : diff_cnt) {
                    if (1 == dc.second.size()) {
                        return words[dc.second.front()];
                    }
                }
            }
        }

        return words.front();
    }
};

/**
 * Original solution.
 */
auto diff_array(string const& word)
{
    auto da = vector<int>(word.size() - 1, 0);
    for (auto n = word.size() - 1, i = n * 0; i < n; ++i) {
        da[i] = word[i + 1] - word[i];
    }
    return da;
}

class Solution {
public:
    string oddString(vector<string>& words) {
        auto diff_array_cnt = map<vector<int>, vector<int>>{};
        for (auto n = words.size(), i = n*0; i < n; ++i) {
            auto da = diff_array(words[i]);
            diff_array_cnt[da].push_back(i);
        }

        for (auto const& dac : diff_array_cnt) {
            if (1 == dac.second.size()) {
                return words[dac.second.front()];
            }
        }

        return words.front();
    }
};
