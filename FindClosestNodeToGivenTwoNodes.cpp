/**
 * Find Closest Node to Given Two Nodes
 *
 * You are given a directed graph of n nodes numbered from 0 to n - 1, where each node has at most
 * one outgoing edge.
 *
 * The graph is represented with a given 0-indexed array edges of size n, indicating that there is a
 * directed edge from node i to node edges[i]. If there is no outgoing edge from i, then edges[i] ==
 * -1.
 *
 * You are also given two integers node1 and node2.
 *
 * Return the index of the node that can be reached from both node1 and node2, such that the maximum
 * between the distance from node1 to that node, and from node2 to that node is minimized. If there
 * are multiple answers, return the node with the smallest index, and if no possible answer exists,
 * return -1.
 *
 * Note that edges may contain cycles.
 *
 * Example 1:
 *
 * Input: edges = [2,2,3,-1], node1 = 0, node2 = 1
 * Output: 2
 * Explanation: The distance from node 0 to node 2 is 1, and the distance from node 1 to node 2 is 1.
 * The maximum of those two distances is 1. It can be proven that we cannot get a node with a smaller maximum distance than 1, so we return node 2.
 * Example 2:
 *
 * Input: edges = [1,2,-1], node1 = 0, node2 = 2
 * Output: 2
 * Explanation: The distance from node 0 to node 2 is 2, and the distance from node 2 to itself is 0.
 * The maximum of those two distances is 2. It can be proven that we cannot get a node with a smaller maximum distance than 2, so we return node 2.
 *
 * Constraints:
 *
 * n == edges.length
 * 2 <= n <= 105
 * -1 <= edges[i] < n
 * edges[i] != i
 * 0 <= node1, node2 < n
 */
/**
 * Original solution.
 */
vector<int> distance_from_node(vector<int> const& edges, int node)
{
    auto n = edges.size();
    auto distance = vector<int>(n, -1);
    auto dist = 0;
    auto org = node;
    while (node >= 0) {
        ++dist;
        auto to_node = edges[node];
        if (-1 == to_node || distance[to_node] > 0) {
            break;
        }

        distance[to_node] = dist;
        node = to_node;
    }

    distance[org] = 0;

    return distance;
}

class Solution {
public:
    int closestMeetingNode(vector<int>& edges, int node1, int node2) {
        auto distance_from_node1 = distance_from_node(edges, node1);
        auto distance_from_node2 = distance_from_node(edges, node2);

        auto ans = -1;
        auto min_distance = numeric_limits<int>::max();
        auto n = edges.size();
        for (auto i = 0; i < n; ++i) {
            if (distance_from_node1[i] >= 0 && distance_from_node2[i] >= 0) {
                auto max_distance = max(distance_from_node1[i], distance_from_node2[i]);
                if (max_distance < min_distance) {
                    ans = i;
                    min_distance = max_distance;
                }
            }
        }
        return ans;
    }
};
