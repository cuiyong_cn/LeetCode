/**
 * Maximum Rows Covered by Columns
 *
 * You are given a 0-indexed m x n binary matrix matrix and an integer numSelect, which denotes the
 * number of distinct columns you must select from matrix.
 *
 * Let us consider s = {c1, c2, ...., cnumSelect} as the set of columns selected by you. A row row is covered by s if:
 *
 * For each cell matrix[row][col] (0 <= col <= n - 1) where matrix[row][col] == 1, col is present in s or,
 * No cell in row has a value of 1.
 * You need to choose numSelect columns such that the number of rows that are covered is maximized.
 *
 * Return the maximum number of rows that can be covered by a set of numSelect columns.
 *
 * Example 1:
 *
 * Input: matrix = [[0,0,0],[1,0,1],[0,1,1],[0,0,1]], numSelect = 2
 * Output: 3
 * Explanation: One possible way to cover 3 rows is shown in the diagram above.
 * We choose s = {0, 2}.
 * - Row 0 is covered because it has no occurrences of 1.
 * - Row 1 is covered because the columns with value 1, i.e. 0 and 2 are present in s.
 * - Row 2 is not covered because matrix[2][1] == 1 but 1 is not present in s.
 * - Row 3 is covered because matrix[2][2] == 1 and 2 is present in s.
 * Thus, we can cover three rows.
 * Note that s = {1, 2} will also cover 3 rows, but it can be shown that no more than three rows can be covered.
 * Example 2:
 *
 *
 * Input: matrix = [[1],[0]], numSelect = 1
 * Output: 2
 * Explanation: Selecting the only column will result in both rows being covered since the entire matrix is selected.
 * Therefore, we return 2.
 *
 * Constraints:
 *
 * m == matrix.length
 * n == matrix[i].length
 * 1 <= m, n <= 12
 * matrix[i][j] is either 0 or 1.
 * 1 <= numSelect <= n
 */
/**
 * Original solution.
 */
bool row_covered(vector<vector<int>> const& matrix, vector<int> const& col_selected, int row)
{
    if (all_of(begin(matrix[row]), end(matrix[row]),
            [](auto n) {
                return 0 == n;
            })) {
        return true;
    }

    auto const cols = matrix[0].size();
    auto c = size_t{0};

    for (auto p = 0; c < cols; ++c) {
        if (1 == matrix[row][c]) {
            while (p < col_selected.size() && c > col_selected[p]) {
                ++p;
            }

            if (p == col_selected.size() || c != col_selected[p]) {
                break;
            }
        }
    }

    return c == cols;
}

int rows_covered(vector<vector<int>> const& matrix, vector<int> const& cols_selected)
{
    auto rows = matrix.size();
    auto cnt = 0;
    for (auto r = size_t{0}; r < rows; ++r) {
        cnt += row_covered(matrix, cols_selected, r) ? 1 : 0;
    }

    return cnt;
}

class Solution {
public:
    int maximumRows(vector<vector<int>>& matrix, int numSelect) {
        auto cols_selected = vector<int>{};

        dfs(matrix, cols_selected, 0, numSelect);

        return ans;
    }

private:
    void dfs(vector<vector<int>> const& matrix, vector<int>& cols_selected, int col, int col_cnt_limit)
    {
        if (cols_selected.size() == col_cnt_limit) {
            ans = max(ans, rows_covered(matrix, cols_selected));
            return;
        }

        if (col < matrix[0].size()) {
            cols_selected.push_back(col);
            dfs(matrix, cols_selected, col + 1, col_cnt_limit);
            cols_selected.pop_back();
            dfs(matrix, cols_selected, col + 1, col_cnt_limit);
        }
    }

    int ans = 0;
};
