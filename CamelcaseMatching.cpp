/**
 * Camelcase Matching
 *
 * A query word matches a given pattern if we can insert lowercase letters to the pattern word so that it equals the
 * query. (We may insert each character at any position, and may insert 0 characters.)
 *
 * Given a list of queries, and a pattern, return an answer list of booleans, where answer[i] is true if and only if
 * queries[i] matches the pattern.
 *
 * Example 1:
 *
 * Input: queries = ["FooBar","FooBarTest","FootBall","FrameBuffer","ForceFeedBack"], pattern = "FB"
 * Output: [true,false,true,true,false]
 * Explanation:
 * "FooBar" can be generated like this "F" + "oo" + "B" + "ar".
 * "FootBall" can be generated like this "F" + "oot" + "B" + "all".
 * "FrameBuffer" can be generated like this "F" + "rame" + "B" + "uffer".
 *
 * Example 2:
 *
 * Input: queries = ["FooBar","FooBarTest","FootBall","FrameBuffer","ForceFeedBack"], pattern = "FoBa"
 * Output: [true,false,true,false,false]
 * Explanation:
 * "FooBar" can be generated like this "Fo" + "o" + "Ba" + "r".
 * "FootBall" can be generated like this "Fo" + "ot" + "Ba" + "ll".
 *
 * Example 3:
 *
 * Input: queries = ["FooBar","FooBarTest","FootBall","FrameBuffer","ForceFeedBack"], pattern = "FoBaT"
 * Output: [false,true,false,false,false]
 * Explanation:
 * "FooBarTest" can be generated like this "Fo" + "o" + "Ba" + "r" + "T" + "est".
 *
 * Note:
 *
 *     1 <= queries.length <= 100
 *     1 <= queries[i].length <= 100
 *     1 <= pattern.length <= 100
 *     All strings consists only of lower and upper case English letters.
 */
/**
 * Same idea, but using find and all_of algorithm method
 */
class Solution {
public:
    vector<bool> camelMatch(vector<string>& qs, string pattern, vector<bool> res = {}) {
        for (auto i = 0; i < qs.size(); ++i) {
            for (auto p = -1, j = 0; j < pattern.size(); ++j) {
                p = qs[i].find(pattern[j], p + 1);
                if (p == string::npos) {
                    res.push_back(false);
                    break;
                }
                qs[i][p] = 'a';
            }
            if (res.size() <= i) {
                res.push_back(all_of(begin(qs[i]), end(qs[i]), [](char ch) { return islower(ch); }));
            }
        }
        return res;
    }
};
/**
 * Regex solution. But efficiency is a big issue.
 */
class Solution {
public:
    vector<bool> camelMatch(vector<string>& queries, string pattern) {
        vector<bool> res;
        string p = "[a-z]*";
        for (char &c: pattern)
            p += string(1, c) + "[a-z]*";
        regex r(p);
        for (string &query : queries)
            res.push_back(regex_match(query, r) );
        return res;
    }
};

/**
 * More clean solution. But I think this solution is less efficient than original solution.
 */
class Solution {
public:
    vector<bool> camelMatch(vector<string>& queries, string pattern) {
        vector<bool> res;
        for (string &query : queries) {
            res.push_back(isMatch(query, pattern));
        }
        return res;
    }

    bool isMatch(string& query, string& pattern) {
        int i = 0;
        for (char & c : query)
            if (i < pattern.length() && c == pattern[i]) i++;
            else if (c < 'a') return false;
        return i == pattern.length();
    }
};

/**
 * Original solution. Brute force idea:
 *      1. Traverse the queries.
 *      2. Judge the word matches the pattern or not.
 */
class Solution {
public:
    vector<bool> camelMatch(vector<string>& queries, string pattern) {
        vector<bool> ans(queries.size(), false);

        for (int i = 0; i < queries.size(); ++i) {
            if (queries[i].size() >= pattern.size()) {
                int j = 0;
                int k = 0;
                bool match = true;
                while (j < queries[i].size() && k < pattern.size()) {
                    if (isupper(pattern[k])) {
                        if (isupper(queries[i][j])) {
                            if (pattern[k] != queries[i][j]) {
                                match = false;
                                break;
                            }
                            else {
                                ++j;
                                ++k;
                            }
                        }
                        else {
                            ++j;
                        }
                    }
                    else {
                        if (isupper(queries[i][j])) {
                            match = false;
                            break;
                        }
                        else {
                            if (queries[i][j] == pattern[k]) {
                                ++j;
                                ++k;
                            }
                            else {
                                ++j;
                            }
                        }
                    }
                }
                if (k < pattern.size()) {
                    match = false;
                }
                else {
                    for (int m = j; m < queries[i].size(); ++m) {
                        if (isupper(queries[i][m])) {
                            match = false;
                            break;
                        }
                    }
                }
                ans[i] = match;
            }
        }
        return ans;
    }
};
