/**
 * Subject line (try to keep under 50 characters)
 *
 * A square triple (a,b,c) is a triple where a, b, and c are integers and a2 + b2 = c2.
 *
 * Given an integer n, return the number of square triples such that 1 <= a, b, c <= n.
 *
 * Example 1:
 *
 * Input: n = 5
 * Output: 2
 * Explanation: The square triples are (3,4,5) and (4,3,5).
 * Example 2:
 *
 * Input: n = 10
 * Output: 4
 * Explanation: The square triples are (3,4,5), (4,3,5), (6,8,10), and (8,6,10).
 *
 * Constraints:
 *
 * 1 <= n <= 250
 */
/**
 * Optimized.
 */
bool is_sqrt_interger(int n)
{
    int left = 1;
    int right = n;

    while (left <= right) {
        auto mid = (left + right) >> 1;
        auto const sqr = mid * mid;
        if (sqr == n) {
            return true;
        }

        if (sqr > n) {
            right = mid - 1;
        }
        else {
            left = mid + 1;
        }
    }

    return false;
}

class Solution {
public:
    int countTriples(int n) {
        int ans = 0;
        int max_sum = n * n;
        for (int a = 1; a <= n; ++a) {
            for (int b = a; b <= n; ++b) {
                auto sum = a * a + b * b;
                if (sum > max_sum) {
                    break;
                }

                if (is_sqrt_interger(sum)) {
                    ans += 2;
                }
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
bool is_sqrt_interger(int n)
{
    int left = 1;
    int right = n;

    while (left <= right) {
        auto mid = (left + right) >> 1;
        auto const sqr = mid * mid;
        if (sqr == n) {
            return true;
        }

        if (sqr > n) {
            right = mid - 1;
        }
        else {
            left = mid + 1;
        }
    }

    return false;
}

class Solution {
public:
    int countTriples(int n) {
        int ans = 0;
        int max_sum = n * n;
        for (int a = 1; a <= n; ++a) {
            for (int b = 1; b <= n; ++b) {
                auto sum = a * a + b * b;
                if (sum > max_sum) {
                    break;
                }

                if (is_sqrt_interger(sum)) {
                    ++ans;
                }
            }
        }

        return ans;
    }
};
