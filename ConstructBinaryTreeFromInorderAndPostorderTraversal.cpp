/**
 * Construct Binary Tree from Inorder and Postorder Traversal
 *
 * Given inorder and postorder traversal of a tree, construct the binary tree.
 *
 * Note:
 * You may assume that duplicates do not exist in the tree.
 *
 * For example, given
 *
 * inorder = [9,3,15,20,7]
 * postorder = [9,15,7,20,3]
 * Return the following binary tree:
 *
 *     3
 *    / \
 *   9  20
 *     /  \
 *    15   7
 */
/**
 * And the always recursive one.
 */
public TreeNode buildTreePostIn(int[] inorder, int[] postorder) {
	if (inorder == null || postorder == null || inorder.length != postorder.length)
		return null;
	HashMap<Integer, Integer> hm = new HashMap<Integer,Integer>();
	for (int i=0;i<inorder.length;++i)
		hm.put(inorder[i], i);
	return buildTreePostIn(inorder, 0, inorder.length-1, postorder, 0,
                          postorder.length-1,hm);
}

private TreeNode buildTreePostIn(int[] inorder, int is, int ie, int[] postorder, int ps, int pe,
                                 HashMap<Integer,Integer> hm){
	if (ps>pe || is>ie) return null;
	TreeNode root = new TreeNode(postorder[pe]);
	int ri = hm.get(postorder[pe]);
	root.left = buildTreePostIn(inorder, is, ri-1, postorder, ps, ps+ri-is-1, hm);
	root.right = buildTreePostIn(inorder,ri+1, ie, postorder, ps+ri-is, pe-1, hm);
	return root;
}

/**
 * Same idea, but recursive solution. This is from discussion and written in java.
 */
int pInorder;   // index of inorder array
int pPostorder; // index of postorder array

private TreeNode buildTree(int[] inorder, int[] postorder, TreeNode end) {
	if (pPostorder < 0) {
		return null;
	}

	// create root node
	TreeNode n = new TreeNode(postorder[pPostorder--]);

	// if right node exist, create right subtree
	if (inorder[pInorder] != n.val) {
		n.right = buildTree(inorder, postorder, n);
	}

	pInorder--;

	// if left node exist, create left subtree
	if ((end == null) || (inorder[pInorder] != end.val)) {
		n.left = buildTree(inorder, postorder, end);
	}

	return n;
}

public TreeNode buildTree(int[] inorder, int[] postorder) {
	pInorder = inorder.length - 1;
	pPostorder = postorder.length - 1;

	return buildTree(inorder, postorder, null);
}

/**
 * Original solution. Same as ConstructBinaryTreeFromPreorderAndInorderTraversal.cpp
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* buildTree(vector<int>& inorder, vector<int>& postorder) {
        if (inorder.empty()) {
            return nullptr;
        }

        TreeNode* root = new TreeNode(postorder.back());
        stack<TreeNode*> stk;

        stk.push(root);
        int index = inorder.size() - 1;

        for (int i = postorder.size() - 2; i >= 0; --i) {
            TreeNode* cur = stk.top();

            if (cur->val != inorder[index]) {
                cur->right = new TreeNode(postorder[i]);
                stk.push(cur->right);
            }
            else {
                while (!stk.empty() && stk.top()->val == inorder[index]) {
                    cur = stk.top();
                    stk.pop();
                    --index;
                }

                cur->left = new TreeNode(postorder[i]);
                stk.push(cur->left);
            }
        }

        return root;
    }
};
