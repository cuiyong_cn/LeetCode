/**
 * Knight Probability in Chessboard
 *
 * On an NxN chessboard, a knight starts at the r-th row and c-th column and attempts to make exactly K moves. The rows
 * and columns are 0 indexed, so the top-left square is (0, 0), and the bottom-right square is (N-1, N-1).
 * 
 * A chess knight has 8 possible moves it can make, as illustrated below. Each move is two squares in a cardinal
 * direction, then one square in an orthogonal direction.
 * 
 * Each time the knight is to move, it chooses one of eight possible moves uniformly at random (even if the piece would
 * go off the chessboard) and moves there.
 * 
 * The knight continues moving until it has made exactly K moves or has moved off the chessboard. Return the probability
 * that the knight remains on the board after it has stopped moving.
 * 
 * Example:
 * 
 * Input: 3, 2, 0, 0
 * Output: 0.0625
 * Explanation: There are two moves (to (1,2), (2,1)) that will keep the knight on the board.
 * From each of those positions, there are also two moves that will keep the knight on the board.
 * The total probability the knight stays on the board is 0.0625.
 * 
 * Note:
 *     N will be between 1 and 25.
 *     K will be between 0 and 100.
 *     The knight always initially starts on the board.
 */
/**
 * Solution from leetcode using dp. The idea is:
 * Let f[r][c][steps] be the probability of being on square(r, c) after steps steps. Based on how a knight moves, we
 * have the following recursion:
 *
 * f[r][c][steps]=∑f[r+dr][c+dc][steps−1]/8.0
 */
struct Pos
{
    int r;
    int c;
};

struct Dir
{
    int dr;
    int dc;
};

class Solution {
public:
    double knightProbability(int N, int K, int r, int c) {
        if (0 == K) return 1.0;

        vector<Dir> dir{{-2, 1}, {-1, 2}, {1, 2}, {2, 1},
                        {2, -1}, {1, -2}, {-1, -2}, {-2, -1}};

        vector<vector<double>> dp(N, vector<double>(N, 0));
        dp[r][c] = 1;

        for (int turn = 0; turn < K; ++turn) {
            vector<vector<double>> dp2(N, vector<double>(N, 0));
            for (int sr = 0; sr < N; ++sr) {
                for (int sc = 0; sc < N; ++sc) {
                    for (size_t d = 0; d < dir.size(); ++d) {
                        Pos npos = {sr + dir[d].dr, sc + dir[d].dc};
                        if (0 <= npos.r && npos.r < N && 0 <= npos.c && npos.c < N) {
                            dp2[npos.r][npos.c] += dp[sr][sc] / 8.0;
                        }
                    }
                }
            }
            swap(dp, dp2);
        }
        
        double ans = 0.0;
        for_each(dp.begin(), dp.end(), [&ans](auto& row) {
            ans += accumulate(row.begin(), row.end(), 0.0);
        });
        
        return ans;
    }
};

/**
 * Original solution. TLE. (we need a proper cache mechanism)
 */
struct Pos
{
    int r;
    int c;
};

struct Dir
{
    int dr;
    int dc;
};

class Solution {
public:
    double knightProbability(int N, int K, int r, int c) {
        if (0 == K) return 1.0;

        vector<Dir> dir{{-2, 1}, {-1, 2}, {1, 2}, {2, 1},
                        {2, -1}, {1, -2}, {-1, -2}, {-2, -1}};

        vector<double> probability(K, 0);
        deque<Pos> on_board{{r, c}};

        for (int turn = 0; turn < K; ++turn) {
            int count = on_board.size();
            for (int i = 0; i < count; ++i) {
                Pos knight = on_board.front();
                on_board.pop_front();

                for (size_t d = 0; d < dir.size(); ++d) {
                    Pos new_pos = {knight.r + dir[d].dr, knight.c + dir[d].dc};
                    if (0 <= new_pos.r && new_pos.r < N
                       && 0 <= new_pos.c && new_pos.c < N) {
                        on_board.push_back(new_pos);
                    }
                }
                
                probability[turn] = on_board.size() * 1.0 / count / dir.size();
            }
        }
        
        double ans = 1;
        for_each(probability.begin(), probability.end(), [&ans](auto p) {
            ans *= p;
        });

        return ans;
    }
};
