/**
 * Array of Doubled Pairs
 *
 * Given an integer array of even length arr, return true if it is possible to reorder arr such that
 * arr[2 * i + 1] = 2 * arr[2 * i] for every 0 <= i < len(arr) / 2, or false otherwise.
 *
 * Example 1:
 *
 * Input: arr = [3,1,3,6]
 * Output: false
 * Example 2:
 *
 * Input: arr = [2,1,2,6]
 * Output: false
 * Example 3:
 *
 * Input: arr = [4,-2,2,-4]
 * Output: true
 * Explanation: We can take two groups, [-2,-4] and [2,4] to form [-2,-4,2,4] or [2,4,-2,-4].
 * Example 4:
 *
 * Input: arr = [1,2,4,16,8,4]
 * Output: false
 *
 * Constraints:
 *
 * 2 <= arr.length <= 3 * 104
 * arr.length is even.
 * -105 <= arr[i] <= 105
 */
/**
 * Original solution.
 */
class Solution {
public:
    bool canReorderDoubled(vector<int>& arr) {
        map<int, int> int_cnt;
        for (auto n : arr) {
            ++int_cnt[n];
        }

        if (int_cnt.count(0)) {
            if (int_cnt[0] & 1) {
                return false;
            }
            int_cnt.erase(0);
        }

        while (int_cnt.size() > 1) {
            auto const [first, cnt] = *int_cnt.begin();
            if (first < 0 && ((-first) & 1)) {
                return false;
            }

            auto second = first < 0 ? first / 2 : 2 * first;
            if (0 == int_cnt.count(second) || cnt > int_cnt[second]) {
                return false;
            }

            int_cnt.erase(first);
            if (cnt == int_cnt[second]) {
                int_cnt.erase(second);
            }
            else {
                int_cnt[second] -= cnt;
            }
        }

        return int_cnt.empty();
    }
};
