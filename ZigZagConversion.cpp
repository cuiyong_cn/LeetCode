/**
 * ZigZag Conversion
 *
 * The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this:
 * (you may want to display this pattern in a fixed font for better legibility)
 *
 * P   A   H   N
 * A P L S I I G
 * Y   I   R
 * And then read line by line: "PAHNAPLSIIGYIR"
 *
 * Write the code that will take a string and make this conversion given a number of rows:
 *
 * string convert(string s, int numRows);
 *
 * Example 1:
 *
 * Input: s = "PAYPALISHIRING", numRows = 3
 * Output: "PAHNAPLSIIGYIR"
 * Example 2:
 *
 * Input: s = "PAYPALISHIRING", numRows = 4
 * Output: "PINALSIGYAHRPI"
 * Explanation:
 * P     I    N
 * A   L S  I G
 * Y A   H R
 * P     I
 * Example 3:
 *
 * Input: s = "A", numRows = 1
 * Output: "A"
 *
 * Constraints:
 *
 * 1 <= s.length <= 1000
 * s consists of English letters (lower-case and upper-case), ',' and '.'.
 * 1 <= numRows <= 1000
 */
/**
 * More space efficient.
 */
class Solution {
public:
    string convert(string s, int numRows) {
        if (1 == numRows) {
            return s;
        }

        string ret;
        int n = s.size();
        int cycleLen = 2 * numRows - 2;

        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j + i < n; j += cycleLen) {
                ret += s[j + i];
                if (i != 0 && i != numRows - 1 && j + cycleLen - i < n) {
                    ret += s[j + cycleLen - i];
                }
            }
        }

        return ret;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    string convert(string s, int numRows) {
        vector<string> zigzag(numRows);

        for (int i = 0; i < s.length();) {
            for (int k = 0; k < numRows && i < s.length(); ++k, ++i) {
                zigzag[k] += s[i];
            }

            for (int k = numRows - 2; k > 0 && i < s.length(); --k, ++i) {
                zigzag[k] += s[i];
            }
        }

        return accumulate(begin(zigzag), end(zigzag), string{});
    }
};
