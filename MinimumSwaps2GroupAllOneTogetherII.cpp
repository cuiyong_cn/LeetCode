/**
 * Minimum Swaps to Group All 1's Together II
 *
 * A swap is defined as taking two distinct positions in an array and swapping the values in them.
 *
 * A circular array is defined as an array where we consider the first element and the last element
 * to be adjacent.
 *
 * Given a binary circular array nums, return the minimum number of swaps required to group all 1's
 * present in the array together at any location.
 *
 * Example 1:
 *
 * Input: nums = [0,1,0,1,1,0,0]
 * Output: 1
 * Explanation: Here are a few of the ways to group all the 1's together:
 * [0,0,1,1,1,0,0] using 1 swap.
 * [0,1,1,1,0,0,0] using 1 swap.
 * [1,1,0,0,0,0,1] using 2 swaps (using the circular property of the array).
 * There is no way to group all 1's together with 0 swaps.
 * Thus, the minimum number of swaps required is 1.
 * Example 2:
 *
 * Input: nums = [0,1,1,1,0,0,1,1,0]
 * Output: 2
 * Explanation: Here are a few of the ways to group all the 1's together:
 * [1,1,1,0,0,0,0,1,1] using 2 swaps (using the circular property of the array).
 * [1,1,1,1,1,0,0,0,0] using 2 swaps.
 * There is no way to group all 1's together with 0 or 1 swaps.
 * Thus, the minimum number of swaps required is 2.
 * Example 3:
 *
 * Input: nums = [1,1,0,0,1]
 * Output: 0
 * Explanation: All the 1's are already grouped together due to the circular property of the array.
 * Thus, the minimum number of swaps required is 0.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 105
 * nums[i] is either 0 or 1.*
 */
/**
 * Original solution.
 */
vector<int> repeat(vector<int> const& arr, int times)
{
    auto new_size = times * arr.size();
    auto ans = vector<int>(new_size, 0);
    auto b = begin(ans);
    while (times > 0) {
        --times;
        copy(begin(arr), end(arr), b);
        advance(b, arr.size());
    }

    return ans;
}

class Solution {
public:
    int minSwaps(vector<int>& nums) {
        auto sub_size = count(begin(nums), end(nums), 1);
        auto const arr = repeat(nums, 2);
        auto one_cnt = count(begin(nums), begin(nums) + sub_size, 1);
        auto ans = sub_size - one_cnt;
        for (auto i = sub_size; i < arr.size(); ++i) {
            one_cnt += 1 == arr[i];
            one_cnt -= 1 == arr[i - sub_size];
            ans = min(ans, sub_size - one_cnt);
        }

        return ans;
    }
};
