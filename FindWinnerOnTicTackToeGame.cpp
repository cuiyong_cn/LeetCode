/**
 * Find Winner on a Tic Tac Toe Game
 *
 * Tic-tac-toe is played by two players A and B on a 3 x 3 grid.
 *
 * Here are the rules of Tic-Tac-Toe:
 *
 *     Players take turns placing characters into empty squares (" ").
 *     The first player A always places "X" characters, while the second player B always places "O" characters.
 *     "X" and "O" characters are always placed into empty squares, never on filled ones.
 *     The game ends when there are 3 of the same (non-empty) character filling any row, column, or diagonal.
 *     The game also ends if all squares are non-empty.
 *     No more moves can be played if the game is over.
 *
 * Given an array moves where each element is another array of size 2 corresponding to the row and column of the grid
 * where they mark their respective character in the order in which A and B play.
 *
 * Return the winner of the game if it exists (A or B), in case the game ends in a draw return "Draw", if there are
 * still movements to play return "Pending".
 *
 * You can assume that moves is valid (It follows the rules of Tic-Tac-Toe), the grid is initially empty and A will play first.
 *
 * Example 1:
 *
 * Input: moves = [[0,0],[2,0],[1,1],[2,1],[2,2]]
 * Output: "A"
 * Explanation: "A" wins, he always plays first.
 * "X  "    "X  "    "X  "    "X  "    "X  "
 * "   " -> "   " -> " X " -> " X " -> " X "
 * "   "    "O  "    "O  "    "OO "    "OOX"
 *
 * Example 2:
 *
 * Input: moves = [[0,0],[1,1],[0,1],[0,2],[1,0],[2,0]]
 * Output: "B"
 * Explanation: "B" wins.
 * "X  "    "X  "    "XX "    "XXO"    "XXO"    "XXO"
 * "   " -> " O " -> " O " -> " O " -> "XO " -> "XO "
 * "   "    "   "    "   "    "   "    "   "    "O  "
 *
 * Example 3:
 *
 * Input: moves = [[0,0],[1,1],[2,0],[1,0],[1,2],[2,1],[0,1],[0,2],[2,2]]
 * Output: "Draw"
 * Explanation: The game ends in a draw since there are no moves to make.
 * "XXO"
 * "OOX"
 * "XOX"
 *
 * Example 4:
 *
 * Input: moves = [[0,0],[1,1]]
 * Output: "Pending"
 * Explanation: The game has not finished yet.
 * "X  "
 * " O "
 * "   "
 *
 * Constraints:
 *
 *     1 <= moves.length <= 9
 *     moves[i].length == 2
 *     0 <= moves[i][j] <= 2
 *     There are no repeated elements on moves.
 *     moves follow the rules of tic tac toe.
 */
/**
 * Shorter one.
 */
class Solution {
public:
    string tictactoe(vector<vector<int>>& moves) {
        vector<int> A(8,0), B(8,0); // 3 rows, 3 cols, 2 diagonals

        for(int i=0; i<moves.size(); i++) {
            int r = moves[i][0];
            int c = moves[i][1];
            vector<int>& player = (i%2 == 0) ? A : B;
            player[r]++;
            player[c+3]++;
            if (r == c) player[6]++;
            if (r == 2 - c) player[7]++;
        }

        for(int i=0; i<8; i++) {
            if(A[i]==3) return "A";
            if(B[i]==3) return "B";
        }

        return moves.size()==9 ? "Draw":"Pending";
    }
};

/**
 * Original solution. Brute force. The idea is:
 *      1. If moves < 5, it is "Pending" without doubt.
 *      2. 3 < Moves <= 9; we check rows, cols and diagonal to see if there is a winner. If no winner found,
 *         when moves == 9, it is Draw, or it is pending.
 */
class Solution {
public:
    string tictactoe(vector<vector<int>>& moves) {
        string ans;
        vector<vector<char>> board(3, vector<char>(3, 0));
        if (moves.size() < 5) return "Pending";

        for (int i = 0; i < moves.size(); ++i) {
            int r = moves[i][0];
            int c = moves[i][1];
            if (0 == (i % 2)) {
                board[r][c] = 'X';
            }
            else {
                board[r][c] = 'O';
            }
        }

        for (int i = 0; i < 3; ++i) {
            int j = 1;
            /* check rows */
            for (; j < 3; ++j) {
                if (board[i][j] != board[i][j - 1]) break;
            }
            if (3 == j) {
                if ('X' == board[i][0]) {
                    ans = "A";
                }
                else if ('O' == board[i][0]) {
                    ans = "B";
                }
                break;
            }

            j = 1;
            /* check cols */
            for (; j < 3; ++j) {
                if (board[j][i] != board[j - 1][i]) break;
            }
            if (3 == j) {
                if ('X' == board[0][i]) {
                    ans = "A";
                }
                else if ('O' == board[0][i]) {
                    ans = "B";
                }
                break;
            }
        }

        if (ans.length() == 0) {
            int i = 1;
            for (; i < 3; ++i) {
                if (board[i][i] != board[i - 1][i - 1]) break;
            }
            if (3 == i) {
                if ('X' == board[1][1]) {
                    ans = "A";
                }
                else if ('O' == board[1][1]) {
                    ans = "B";
                }
            }
        }

        if (ans.length() == 0) {
            int i = 1;
            int j = 1;
            for (; i < 3; ++i, --j) {
                if (board[i][j] != board[i - 1][j + 1]) break;
            }
            if (3 == i) {
                if ('X' == board[1][1]) {
                    ans = "A";
                }
                else if ('O' == board[1][1]) {
                    ans = "B";
                }
            }
        }

        if (ans.length() == 0) {
            if (moves.size() == 9) {
                ans = "Draw";
            }
            else {
                ans = "Pending";
            }
        }

        return ans;
    }
};
