/**
 * Sum of Subarray Ranges
 *
 * You are given an integer array nums. The range of a subarray of nums is the difference between
 * the largest and smallest element in the subarray.
 *
 * Return the sum of all subarray ranges of nums.
 *
 * A subarray is a contiguous non-empty sequence of elements within an array.
 *
 * Example 1:
 *
 * Input: nums = [1,2,3]
 * Output: 4
 * Explanation: The 6 subarrays of nums are the following:
 * [1], range = largest - smallest = 1 - 1 = 0
 * [2], range = 2 - 2 = 0
 * [3], range = 3 - 3 = 0
 * [1,2], range = 2 - 1 = 1
 * [2,3], range = 3 - 2 = 1
 * [1,2,3], range = 3 - 1 = 2
 * So the sum of all ranges is 0 + 0 + 0 + 1 + 1 + 2 = 4.
 * Example 2:
 *
 * Input: nums = [1,3,3]
 * Output: 4
 * Explanation: The 6 subarrays of nums are the following:
 * [1], range = largest - smallest = 1 - 1 = 0
 * [3], range = 3 - 3 = 0
 * [3], range = 3 - 3 = 0
 * [1,3], range = 3 - 1 = 2
 * [3,3], range = 3 - 3 = 0
 * [1,3,3], range = 3 - 1 = 2
 * So the sum of all ranges is 0 + 0 + 0 + 2 + 0 + 2 = 4.
 * Example 3:
 *
 * Input: nums = [4,-2,-3,4,1]
 * Output: 59
 * Explanation: The sum of all subarray ranges of nums is 59.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 1000
 * -109 <= nums[i] <= 109
 *
 * Follow-up: Could you find a solution with O(n) time complexity?
 */
/**
 * O(n) solution using monostack.
 */
class Solution {
public:
    long long subArrayRanges(vector<int>& n) {
        return sumSubarrayComp(n, less<int>()) - sumSubarrayComp(n, greater<int>());
    }

    long long sumSubarrayComp(vector<int>& n, function<bool (int, int)> comp) {
        long long res = 0;
        vector<int> s;
        for (int i = 0; i <= n.size(); ++i) {
            while (!s.empty() && (i == n.size() || comp(n[s.back()], n[i]))) {
                int j = s.back(), k = s.size() < 2 ? -1 : s[s.size() - 2];
                res += (long long)(i - j) * (j - k) * n[j];
                s.pop_back();
            }
            s.push_back(i);
        }
        return res;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    long long subArrayRanges(vector<int>& nums) {
        auto nsize = static_cast<int>(nums.size());
        auto ans = int64_t{0};
        for (auto i = int{0}; i < nsize; ++i) {
            auto min_num = nums[i];
            auto max_num = min_num;
            for (auto j = i + 1; j < nsize; ++j) {
                if (nums[j] < min_num) {
                    min_num = nums[j];
                }
                else if (max_num < nums[j]) {
                    max_num = nums[j];
                }
                ans += max_num - min_num;
            }
        }

        return ans;
    }
};
