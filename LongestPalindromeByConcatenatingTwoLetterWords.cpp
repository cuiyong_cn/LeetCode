/**
 * Longest Palindrome by Concatenating Two Letter Words
 *
 * You are given an array of strings words. Each element of words consists of two lowercase English
 * letters.
 *
 * Create the longest possible palindrome by selecting some elements from words and concatenating
 * them in any order. Each element can be selected at most once.
 *
 * Return the length of the longest palindrome that you can create. If it is impossible to create
 * any palindrome, return 0.
 *
 * A palindrome is a string that reads the same forward and backward.
 *
 * Example 1:
 *
 * Input: words = ["lc","cl","gg"]
 * Output: 6
 * Explanation: One longest palindrome is "lc" + "gg" + "cl" = "lcggcl", of length 6.
 * Note that "clgglc" is another longest palindrome that can be created.
 * Example 2:
 *
 * Input: words = ["ab","ty","yt","lc","cl","ab"]
 * Output: 8
 * Explanation: One longest palindrome is "ty" + "lc" + "cl" + "yt" = "tylcclyt", of length 8.
 * Note that "lcyttycl" is another longest palindrome that can be created.
 * Example 3:
 *
 * Input: words = ["cc","ll","xx"]
 * Output: 2
 * Explanation: One longest palindrome is "cc", of length 2.
 * Note that "ll" is another longest palindrome that can be created, and so is "xx".
 *
 * Constraints:
 *
 * 1 <= words.length <= 105
 * words[i].length == 2
 * words[i] consists of lowercase English letters.
 */
/**
 * Original solution.
 */
bool palindrome(string const& word)
{
    return equal(begin(word), end(word), rbegin(word));
}

string reverse(string const& word)
{
    return string(rbegin(word), rend(word));
}

class Solution {
public:
    int longestPalindrome(vector<string>& words) {
        auto word_cnt = unordered_map<string, int>{};
        for (auto const& w : words) {
            ++word_cnt[w];
        }

        auto ans = int{0};
        auto odd_self_panlindrome = int{0};
        for (auto& wc : word_cnt) {
            auto& [word, cnt] = wc;
            if (palindrome(word)) {
                if (cnt & 1) {
                    odd_self_panlindrome = 2;
                    --cnt;
                }
                ans += cnt * 2;
            }
            else if (cnt > 0) {
                auto rev = reverse(word);
                if (word_cnt.count(rev)) {
                    auto pairs = min(cnt, word_cnt[rev]);
                    ans +=  pairs * 4;
                    cnt -= pairs;
                    word_cnt[rev] -= pairs;
                }
            }
        }
        return ans + odd_self_panlindrome;
    }
};
