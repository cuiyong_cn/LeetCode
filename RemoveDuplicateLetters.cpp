/**
 * Remove Duplicate Letters
 *
 * Given a string s, remove duplicate letters so that every letter appears once and only once. You
 * must make sure your result is the smallest in lexicographical order among all possible results.
 *
 * Note: This question is the same as 1081:
 * https://leetcode.com/problems/smallest-subsequence-of-distinct-characters/
 *
 * Example 1:
 *
 * Input: s = "bcabc"
 * Output: "abc"
 * Example 2:
 *
 * Input: s = "cbacdcbc"
 * Output: "acdb"
 *
 * Constraints:
 *
 * 1 <= s.length <= 104
 * s consists of lowercase English letters.*
 */
/**
 * Non recusive version. More efficient.
 */
class Solution {
public:
    string removeDuplicateLetters(string s) {
        vector<int> cnt(256, 0);
        vector<bool> visited(256, false);

        for (auto c : s) {
            cnt[c]++;
        }

        auto ans = string(1, 'a' - 1);
        for (auto c : s) {
            --cnt[c];

            if (visited[c]) {
                continue;
            }

            while (ans.back() > c && cnt[ans.back()]) {
                visited[ans.back()] = false;
                ans.pop_back();
            }

            ans += c;
            visited[c] = true;
        }

        return ans.substr(1);
    }
};

/**
 * Original solution. The idea is, every turn we only choose the one unique character.
 * Because there are at most 26 characters, the time complexity is O(26 * n)
 */
class Solution {
public:
    string removeDuplicateLetters(string s) {
        if (s.empty()) {
            return "";
        }
        vector<int> cnt(26, 0);
        for (auto c : s) {
            ++cnt[c - 'a'];
        }

        size_t min_char_pos = 0;
        for (size_t i = 0; i < s.length(); ++i) {
            int idx = s[i] - 'a';
            if (s[i] < s[min_char_pos]) {
                min_char_pos = i;
            }

            if (0 == --cnt[idx]) {
                break;
            }
        }

        string rest;
        auto ch = s[min_char_pos];
        for (size_t i = min_char_pos + 1; i < s.length(); ++i) {
            if (ch != s[i]) {
                rest += s[i];
            }
        }
        return string(1, ch) + removeDuplicateLetters(std::move(rest));
    }
};
