/**
 * Valid Tic-Tac-Toe State
 *
 * Given a Tic-Tac-Toe board as a string array board, return true if and only if it is possible to
 * reach this board position during the course of a valid tic-tac-toe game.
 *
 * The board is a 3 x 3 array that consists of characters ' ', 'X', and 'O'. The ' ' character
 * represents an empty square.
 *
 * Here are the rules of Tic-Tac-Toe:
 *
 * Players take turns placing characters into empty squares ' '.
 * The first player always places 'X' characters, while the second player always places 'O'
 * characters.  'X' and 'O' characters are always placed into empty squares, never filled ones.  The
 * game ends when there are three of the same (non-empty) character filling any row, column, or
 * diagonal.
 * The game also ends if all squares are non-empty.
 * No more moves can be played if the game is over.
 *
 * Example 1:
 *
 * Input: board = ["O  ","   ","   "]
 * Output: false
 * Explanation: The first player always plays "X".
 * Example 2:
 *
 * Input: board = ["XOX"," X ","   "]
 * Output: false
 * Explanation: Players take turns making moves.
 * Example 3:
 *
 * Input: board = ["XXX","   ","OOO"]
 * Output: false
 * Example 4:
 *
 * Input: board = ["XOX","O O","XOX"]
 * Output: true
 *
 * Constraints:
 *
 * board.length == 3
 * board[i].length == 3
 * board[i][j] is either 'X', 'O', or ' '.
 */
/**
 * Make it more clear.
 */
class Solution {
public:
    bool validTicTacToe(vector<string>& board) {
        int o_cnt = 0;
        int x_cnt = 0;
        for (auto const& row : board) {
            for (auto c : row) {
                if ('X' == c) {
                    ++x_cnt;
                }
                else if ('O' == c) {
                    ++o_cnt;
                }
            }
        }

        if (!(x_cnt == o_cnt || 1 == (x_cnt - o_cnt))) {
            return false;
        }
        else if (x_cnt == o_cnt) {
            return false == is_first_player_win(board);
        }
        else {
            return false == is_second_player_win(board);
        }

        return true;
    }

private:
    bool is_first_player_win(vector<string> const& board)
    {
        return is_player_win(board, 'X');
    }
    
    bool is_second_player_win(vector<string> const& board)
    {
        return is_player_win(board, 'O');
    }

    bool is_player_win(vector<string> const& board, char player)
    {
        for (auto const& row : board) {
            if (row == string(3, player)) {
                return true;
            }
        }

        for (int i = 0; i < 2; ++i) {
            if (player == board[0][i] && player == board[1][i] && player == board[2][i]) {
                return true;
            }
        }

        if (player == board[0][0] && player == board[1][1] && player == board[2][2]) {
            return true;
        }

        if (player == board[0][2] && player == board[1][1] && player == board[2][0]) {
            return true;
        }

        return false;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool validTicTacToe(vector<string>& board) {
        int o_cnt = 0;
        int x_cnt = 0;
        for (auto const& row : board) {
            for (auto c : row) {
                if ('X' == c) {
                    ++x_cnt;
                }
                else if ('O' == c) {
                    ++o_cnt;
                }
            }
        }

        if ((x_cnt - o_cnt) > 1 || (o_cnt - x_cnt) > 0) {
            return false;
        }

        if (is_first_player_win(board)) {
            return 1 == (x_cnt - o_cnt) && !is_second_player_win(board);
        }

        if (is_second_player_win(board)) {
            return x_cnt == o_cnt;
        }

        return (x_cnt - o_cnt) <= 1;
    }

private:
    bool is_first_player_win(vector<string> const& board)
    {
        return is_player_win(board, 'X');
    }

    bool is_second_player_win(vector<string> const& board)
    {
        return is_player_win(board, 'O');
    }

    bool is_player_win(vector<string> const& board, char player)
    {
        for (auto const& row : board) {
            if (row == string(3, player)) {
                return true;
            }
        }

        for (int i = 0; i < 2; ++i) {
            if (player == board[0][i] && player == board[1][i] && player == board[2][i]) {
                return true;
            }
        }

        if (player == board[0][0] && player == board[1][1] && player == board[2][2]) {
            return true;
        }

        if (player == board[0][2] && player == board[1][1] && player == board[2][0]) {
            return true;
        }

        return false;
    }
};
