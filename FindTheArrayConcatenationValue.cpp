auto concat(int a, int b)
{
    return to_string(a) + to_string(b);
}

class Solution {
public:
    long long findTheArrayConcVal(vector<int>& nums) {
        auto ans = 0LL;
        auto const N = nums.size();
        for (int i = 0, j = N - 1; i <= j; ++i, --j) {
            if (i != j) {
                ans += stoi(concat(nums[i], nums[j]));
            }
            else {
                ans += nums[i];
            }
        }
        return ans;
    }
};
