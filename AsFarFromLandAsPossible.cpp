/**
 * As Far from Land as Possible
 *
 * Given an n x n grid containing only values 0 and 1, where 0 represents water and 1 represents land, find a water cell
 * such that its distance to the nearest land cell is maximized, and return the distance. If no land or water exists in
 * the grid, return -1.
 *
 * The distance used in this problem is the Manhattan distance: the distance between two cells (x0, y0) and (x1, y1) is |x0 - x1| + |y0 - y1|.
 *
 * Example 1:
 *
 * Input: grid = [[1,0,1],[0,0,0],[1,0,1]]
 * Output: 2
 * Explanation: The cell (1, 1) is as far as possible from all the land with distance 2.
 * Example 2:
 *
 * Input: grid = [[1,0,0],[0,0,0],[0,0,0]]
 * Output: 4
 * Explanation: The cell (2, 2) is as far as possible from all the land with distance 4.
 *
 * Constraints:
 *
 * n == grid.length
 * n == grid[i].length
 * 1 <= n <= 100
 * grid[i][j] is 0 or 1
 */
/**
 * BFS solution.
 */
class Solution {
public:
    int maxDistance(vector<vector<int>>& grid) {
        deque<pair<int, int>> q;
        const int n = grid.size();

        for (int x = 0; x < n; ++x) {
            for (int y = 0; y < n; ++y) {
                if (1 == grid[x][y]) {
                    q.push_back({x - 1, y});
                    q.push_back({x + 1, y});
                    q.push_back({x, y - 1});
                    q.push_back({x, y + 1});
                }
            }
        }

        int step = 0;
        while (!q.empty()) {
            ++step;
            const int size = q.size();
            for (int i = 0; i < size; ++i) {
                auto [x, y] = q.front();
                q.pop_front();

                if (x < 0 || y < 0 || x >= n || y >= n || grid[x][y] != 0) {
                    continue;
                }
                grid[x][y] = step;
                q.push_back({x - 1, y});
                q.push_back({x + 1, y});
                q.push_back({x, y - 1});
                q.push_back({x, y + 1});
            }
        }

        return step <= 1 ? -1 : step - 1;
    }
};

/**
 * DFS solution.
 */
class Solution {
public:
    int maxDistance(vector<vector<int>>& grid) {
        for (int x = 0; x < grid.size(); ++x) {
            for (int y = 0; y < grid.size(); ++y) {
                if (1 == grid[x][y]) {
                    grid[x][y] = 0;
                    expand(grid, x, y, 1);
                }
            }
        }

        int ans = -1;
        for (const auto& line : grid) {
            ans = std::max(ans, *max_element(line.begin(), line.end()));
        }

        return ans < 2 ? -1 : ans - 1;
    }

private:
    void expand(vector<vector<int>>& grid, int x, int y, int distance)
    {
        if (x < 0 || y < 0 || x >= grid.size() || y >= grid.size()) {
            return;
        }

        if (0 != grid[x][y] && distance >= grid[x][y]) {
            return;
        }

        grid[x][y] = distance;
        expand(grid, x, y - 1, distance + 1);
        expand(grid, x, y + 1, distance + 1);
        expand(grid, x - 1, y, distance + 1);
        expand(grid, x + 1, y, distance + 1);
    }
};
