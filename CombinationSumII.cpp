/**
 * Combination Sum II
 *
 * Given a collection of candidate numbers (candidates) and a target number (target), find all unique combinations in
 * candidates where the candidate numbers sums to target.
 *
 * Each number in candidates may only be used once in the combination.
 *
 * Note:
 *
 * All numbers (including target) will be positive integers.
 * The solution set must not contain duplicate combinations.
 * Example 1:
 *
 * Input: candidates = [10,1,2,7,6,1,5], target = 8,
 * A solution set is:
 * [
 *   [1, 7],
 *   [1, 2, 5],
 *   [2, 6],
 *   [1, 1, 6]
 * ]
 * Example 2:
 *
 * Input: candidates = [2,5,2,1,2], target = 5,
 * A solution set is:
 * [
 *   [1,2,2],
 *   [5]
 * ]
 */
/**
 * Original solution. DFS.
 * From the discussion, they all first sort the candidates. The rest is similar.
 * But the skip part is really confusing.
 */
class Solution {
public:
    vector<vector<int>> combinationSum2(vector<int>& candidates, int target) {
        map<int, int> cand_map;

        for (auto n : candidates) {
            if (n <= target) {
                ++cand_map[n];
            }
        }

        vector<int> comb;
        dfs(cand_map.begin(), cand_map.end(), comb, target);

        return ans;
    }

private:
    using Iter = map<int, int>::iterator;

    void dfs(Iter begin, Iter end, vector<int>& comb, int target)
    {
        if (target < 0) {
            return;
        }

        if (0 == target) {
            ans.push_back(comb);
            return;
        }

        for (; begin != end; ++begin) {
            if (begin->second > 0) {
                --begin->second;
                comb.push_back(begin->first);
                dfs(begin, end, comb, target - begin->first);
                comb.pop_back();
                ++begin->second;
            }
        }
    }

    vector<vector<int>> ans;
};
