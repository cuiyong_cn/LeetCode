/**
 * Third Maximum Number
 *
 * Given an integer array nums, return the third distinct maximum number in this array. If the third
 * maximum does not exist, return the maximum number.
 *
 * Example 1:
 *
 * Input: nums = [3,2,1]
 * Output: 1
 * Explanation:
 * The first distinct maximum is 3.
 * The second distinct maximum is 2.
 * The third distinct maximum is 1.
 * Example 2:
 *
 * Input: nums = [1,2]
 * Output: 2
 * Explanation:
 * The first distinct maximum is 2.
 * The second distinct maximum is 1.
 * The third distinct maximum does not exist, so the maximum (2) is returned instead.
 * Example 3:
 *
 * Input: nums = [2,2,3,1]
 * Output: 1
 * Explanation:
 * The first distinct maximum is 3.
 * The second distinct maximum is 2 (both 2's are counted together since they have the same value).
 * The third distinct maximum is 1.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 104
 * -231 <= nums[i] <= 231 - 1
 *
 * Follow up: Can you find an O(n) solution?
 */
/**
 * Using three variable. We could use numeric_limits<long long>::min(), but this is not portable.
 * So I choose to handle the edge case.
 */
class Solution {
public:
    int thirdMax(vector<int>& nums) {
        int a = numeric_limits<int>::min();
        int b = a;
        int c = b;
        auto min_found = false;
        for (int num : nums) {
            if (num <= c ||  b == num || a == num) {
                if (num == numeric_limits<int>::min()) {
                    min_found = true;
                }
                continue;
            }
            c = num;

            if (c > b) {
                swap(b, c);
            }
            if (b > a) {
                swap(a, b);
            }
        }

        return min_found ?
            (numeric_limits<int>::min() == b ? a : c) :
            (numeric_limits<int>::min() == c ? a : c);
    }
};

/**
 * using set.
 */
class Solution {
public:
    int thirdMax(vector<int>& nums) {
        set<int> top3;
        for (int num : nums) {
            top3.insert(num);
            if (top3.size() > 3) {
                top3.erase(top3.begin());
            }
        }

        return top3.size() == 3 ? *top3.begin() : *top3.rbegin();
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int thirdMax(vector<int>& nums) {
        sort(nums.begin(), nums.end());
        int num = nums.back();
        int i = nums.size() - 1;
        int cnt = 1;
        for (; i >= 0; --i) {
            if (nums[i] != num) {
                ++cnt;
                num = nums[i];
            }
            if (3 == cnt) {
                break;
            }
        }

        return 3 != cnt ? nums.back() : nums[i];
    }
};
