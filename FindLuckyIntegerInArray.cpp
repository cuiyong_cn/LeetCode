/**
 * Find Lucky Integer in an Array
 *
 * Given an array of integers arr, a lucky integer is an integer that has a frequency in the array
 * equal to its value.
 *
 * Return the largest lucky integer in the array. If there is no lucky integer return -1.
 *
 * Example 1:
 *
 * Input: arr = [2,2,3,4]
 * Output: 2
 * Explanation: The only lucky number in the array is 2 because frequency[2] == 2.
 * Example 2:
 *
 * Input: arr = [1,2,2,3,3,3]
 * Output: 3
 * Explanation: 1, 2 and 3 are all lucky numbers, return the largest of them.
 * Example 3:
 *
 * Input: arr = [2,2,2,3,3]
 * Output: -1
 * Explanation: There are no lucky numbers in the array.
 *
 * Constraints:
 *
 * 1 <= arr.length <= 500
 * 1 <= arr[i] <= 500
 */
/**
 * Use upper part to store the count.
 */
class Solution {
public:
    int findLucky(vector<int>& arr) {
        for (auto n : arr) {
            n &= 0xFFFF;
            if (n <= arr.size()) {
                arr[n - 1] += 0x10000;
            }
        }

        for (auto i = arr.size(); i > 0; --i) {
            if ((arr[i - 1] >> 16) == i) {
                return i;
            }
        }
        return -1;
    }
};

/**
 * Without the sort.
 */
class Solution {
public:
    int findLucky(vector<int>& arr) {
        for (auto i = 0; i < arr.size(); ++i) {
            auto val = arr[i];
            if (val > 0) {
                arr[i] = 0;
                while (val > 0 && val <= arr.size()) {
                    auto n_val = arr[val - 1];
                    arr[val - 1] = min(0, arr[val - 1]) - 1;
                    val = n_val;
                }
            }
        }

        for (auto i = arr.size(); i > 0; --i) {
            if (-arr[i - 1] == i) {
                return i;
            }
        }

        return -1;
    }
};

/**
 * Without the extra vector.
 */
class Solution {
public:
    int findLucky(vector<int>& arr) {
        sort(arr.begin(), arr.end(), greater<int>{});
        int cnt = 1;
        for (int i = 1; i < arr.size(); ++i) {
            if (arr[i] == arr[i - 1]) {
                ++cnt;
            }
            else {
                if (arr[i - 1] == cnt) {
                    return cnt;
                }
                cnt = 1;
            }
        }

        return cnt == arr.back() ? cnt : -1;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int findLucky(vector<int>& arr) {
        vector<int> num_count(501, 0);

        for (auto n : arr) {
            ++num_count[n];
        }

        for (int i = 500; i > 0; --i) {
            if (num_count[i] == i) {
                return i;
            }
        }

        return -1;
    }
};
