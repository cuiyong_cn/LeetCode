/**
 * Integer Break
 *
 * Given a positive integer n, break it into the sum of at least two positive integers and maximize the product of those
 * integers. Return the maximum product you can get.
 *
 * Example 1:
 *
 * Input: 2
 * Output: 1
 * Explanation: 2 = 1 + 1, 1 × 1 = 1.
 *
 * Example 2:
 *
 * Input: 10
 * Output: 36
 * Explanation: 10 = 3 + 3 + 4, 3 × 3 × 4 = 36.
 *
 * Note: You may assume that n is not less than 2 and not larger than 58.
 */
/**
 * Below solution comes from discussion. Though it works like a charm, but all the explanation do not convince me.
 * Anyway, I put it here.
 */
public class Solution {
    public int integerBreak(int n) {
        if (2 == n) return 1;
        if (3 == n) return 2;
        if (4 == n) return 4;

        int product = 1;
        while (n > 4) {
            product *= 3;
            n -= 3;
        }
        product *= n;

        return product;
    }
}

/**
 * Original solution. This solution works, but sucks, I don't like this.
 */
class Solution {
public:
    int integerBreak(int n, bool first = true) {
        if (n < 11) {
            if (10 == n) return 36;
            if (9 == n) return 27;
            if (8 == n) return 18;
            if (7 == n) return 12;
            if (6 == n) return 9;
            if (5 == n) return 6;
            if (4 == n) return 4;
            if (3 == n) return first ? 2 : 3;
            if (2 == n) return first ? 1 : 2;
        }
        else {
            if (15 == n) return 243;
            if (14 == n) return 162;
            if (13 == n) return 108;
            if (12 == n) return 81;
            if (11 == n) return 54;
        }

        int ans = 0;
        for (int i = 6; i < n / 2; ++i) {
            ans = max(ans, integerBreak(i, false) * integerBreak(n - i, false));
        }

        return ans;
    }
};
