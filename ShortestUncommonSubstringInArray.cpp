/**
 * Shortest Uncommon Substring in an Array
 *
 * You are given an array arr of size n consisting of non-empty strings.
 *
 * Find a string array answer of size n such that:
 *     answer[i] is the shortest
 *     substring
 *     of arr[i] that does not occur as a substring in any other string in arr. If multiple such substrings exist, answer[i] should be the
 *     lexicographically smallest
 *     . And if no such substring exists, answer[i] should be an empty string.
 *
 * Return the array answer.
 *
 * Example 1:
 *
 * Input: arr = ["cab","ad","bad","c"]
 * Output: ["ab","","ba",""]
 * Explanation: We have the following:
 * - For the string "cab", the shortest substring that does not occur in any other string is either "ca" or "ab", we choose the lexicographically smaller substring, which is "ab".
 * - For the string "ad", there is no substring that does not occur in any other string.
 * - For the string "bad", the shortest substring that does not occur in any other string is "ba".
 * - For the string "c", there is no substring that does not occur in any other string.
 *
 * Example 2:
 *
 * Input: arr = ["abc","bcd","abcd"]
 * Output: ["","","abcd"]
 * Explanation: We have the following:
 * - For the string "abc", there is no substring that does not occur in any other string.
 * - For the string "bcd", there is no substring that does not occur in any other string.
 * - For the string "abcd", the shortest substring that does not occur in any other string is "abcd".
 *
 * Constraints:
 *     n == arr.length
 *     2 <= n <= 100
 *     1 <= arr[i].length <= 20
 *     arr[i] consists only of lowercase English letters.
 */
/**
 * Original solution.
 */
auto all_substr(string const& str)
{
    auto subs = unordered_set<string>{};
    int const len = str.length();

    for (auto i = 0; i < len; ++i) {
        auto sub = string{};

        for (auto j = i; j < len; ++j) {
            sub += str[j];
            subs.insert(sub);
        }
    }

    return subs;
}

auto by_length_then_lexico(string const& s1, string const& s2)
{
    return s1.length() == s2.length() ? s1 < s2 : s1.length() < s2.length();
}

class Solution {
public:
    vector<string> shortestSubstrings(vector<string>& arr) {
        int const n = arr.size();
        auto ans = vector<string>(n);
        auto idx_subs = unordered_map<int, unordered_set<string>>{};

        for (auto i = 0; i < n; ++i) {
            idx_subs[i] = all_substr(arr[i]);
        }

        for (auto i = 0; i < n; ++i) {
            auto& subs = idx_subs[i];
            auto uncommon_subs = vector<string>{};

            for (auto const& s : subs) {
                auto uncommon = true;

                for (auto j = 0; j < n; ++j) {
                    if (i == j) {
                        continue;
                    }

                    if (idx_subs[j].count(s)) {
                        uncommon = false;
                        break;
                    }
                }

                if (uncommon) {
                    uncommon_subs.emplace_back(s);
                }
            }

            sort(uncommon_subs.begin(), uncommon_subs.end(), by_length_then_lexico);

            ans[i] = uncommon_subs.empty() ? string{} : uncommon_subs.front();
        }

        return ans;
    }
};
