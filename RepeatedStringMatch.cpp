/**
 * Repeated String Match
 *
 * Given two strings a and b, return the minimum number of times you should repeat string a so that
 * string b is a substring of it. If it is impossible for b​​​​​​ to
 * be a substring of a after repeating it, return -1.
 *
 * Notice: string "abc" repeated 0 times is "",  repeated 1 time is "abc" and repeated 2 times is
 * "abcabc".
 *
 * Example 1:
 *
 * Input: a = "abcd", b = "cdabcdab"
 * Output: 3
 * Explanation: We return 3 because by repeating a three times "abcdabcdabcd", b is a substring of it.
 * Example 2:
 *
 * Input: a = "a", b = "aa"
 * Output: 2
 * Example 3:
 *
 * Input: a = "a", b = "a"
 * Output: 1
 * Example 4:
 *
 * Input: a = "abc", b = "wxyz"
 * Output: -1
 *
 * Constraints:
 *
 * 1 <= a.length <= 104
 * 1 <= b.length <= 104
 * a and b consist of lower-case English letters.
 */
/**
 * Solution from discussion. Fuck me!
 */
class Solution {
public:
    int repeatedStringMatch(string A, string B) {
        for (auto i = 0, j = 0; i < A.size(); ++i) {
            for (j = 0; j < B.size() && A[(i + j) % A.size()] == B[j]; ++j) {
                ;
            }

            if (j == B.size()) {
                return (i + j - 1) / A.size() + 1;
            }
        }
        return -1;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int repeatedStringMatch(string a, string b) {
        vector<int> char_cnt(26, 0);
        for (auto c : a) {
            ++char_cnt[c - 'a'];
        }

        for (auto c : b) {
            if (0 == char_cnt[c - 'a']) {
                return -1;
            }
        }

        if (a.length() >= b.length()) {
            if (a.find(b) != string::npos) {
                return 1;
            }

            if ((a + a).find(b) != string::npos) {
                return 2;
            }

            return -1;
        }

        auto pos = b.find(a);
        if (string::npos == pos) {
            if ((a + a).find(b) != string::npos) {
                return 2;
            }
            return -1;
        }

        if (pos > a.length()) {
            return -1;
        }

        for (int i = pos - 1, j = a.length() - 1; i >= 0; --i, --j) {
            if (b[i] != a[j]) {
                return -1;
            }
        }

        int ans = 0 == pos ? 0 : 1;
        while ((pos + a.length()) < b.length()) {
            for (int i = 0; i < a.length(); ++i) {
                if (a[i] != b[pos + i]) {
                    return -1;
                }
            }

            ++ans;
            pos += a.length();
        }

        if ((b.length() - pos) > a.length()) {
            return -1;
        }

        for (int i = pos, j = 0; i < b.length(); ++i, ++j) {
            if (b[i] != a[j]) {
                return -1;
            }
        }

        return ans + 1;
    }
};
