/**
 * Optimal Partition of String
 *
 * Given a string s, partition the string into one or more substrings such that the characters in
 * each substring are unique. That is, no letter appears in a single substring more than once.
 *
 * Return the minimum number of substrings in such a partition.
 *
 * Note that each character should belong to exactly one substring in a partition.
 *
 * Example 1:
 *
 * Input: s = "abacaba"
 * Output: 4
 * Explanation:
 * Two possible partitions are ("a","ba","cab","a") and ("ab","a","ca","ba").
 * It can be shown that 4 is the minimum number of substrings needed.
 * Example 2:
 *
 * Input: s = "ssssss"
 * Output: 6
 * Explanation:
 * The only valid partition is ("s","s","s","s","s","s").
 *
 * Constraints:
 *
 * 1 <= s.length <= 105
 * s consists of only English lowercase letters.
 */
/**
 * Space efficient.
 */
class Solution {
public:
    int partitionString(string s) {
        auto bits = 0;
        auto ans = 0;
        for (auto c : s) {
            auto mask = 1 << (c - 'a');
            if (bits & mask) {
                ++ans;
                bits = 0;
            }
            bits |= mask;
        }

        return ans + 1;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int partitionString(string s) {
        auto char_cnt = array<int, 26>{ 0, };
        auto ans = 0;
        for (auto c : s) {
            auto c_idx = c - 'a';
            auto cnt = ++char_cnt[c_idx];
            if (cnt > 1) {
                ++ans;
                char_cnt.fill(0);
                ++char_cnt[c_idx];
            }
        }

        return ans + 1;
    }
};
