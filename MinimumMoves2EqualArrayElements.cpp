/**
 * Minimum Moves to Equal Array Elements
 *
 * Given a non-empty integer array of size n, find the minimum number of moves required to make all array elements
 * equal, where a move is incrementing n - 1 elements by 1.
 *
 * Example:
 *
 * Input:
 * [1,2,3]
 *
 * Output:
 * 3
 *
 * Explanation:
 * Only three moves are needed (remember each move increments two elements):
 *
 * [1,2,3]  =>  [2,3,3]  =>  [3,4,3]  =>  [4,4,4]
 */
/**
 * When observe closely, the moves we get is
 * nums[0] - min_n + nums[1] - min_n + nums[2] - min_n + ...
 *
 * The above one can be simplified: sum - n * min_n
 * sum -- the sum of all the numbers of vector
 * n   -- the length of vector
 * min_n -- the min number of the vector
 *
 * what does this equation mean:
 *
 * The final state of the vector is that all numbers are equal to let's say X
 * And we are doing m moves to get this state.
 * Then we get sum + m * (n - 1) = n * X
 * and X = min_n + m, we replace this in above equation and get m = sum - min_n * n
 *
 * or we can think this way:
 * incrementing n - 1 numbers equals to decrement one number
 *
 * So the final state of the vector is all numbers are equal to min_n
 * So the m = sum - min_n * n
 */
class Solution {
public:
    int minMoves(vector<int>& nums) {
        int min_n = numeric_limits<int>::max();
        long sum = 0;

        for (auto n : nums) {
            min_n = std::min(min_n, n);
            sum += n;
        }

        return sum - min_n * nums.size();
    }
};

/**
 * Simplify the below solution.
 */
class Solution {
public:
    int minMoves(vector<int>& nums) {
        sort(nums.begin(), nums.end());
        int moves = 0;
        int min_n = nums[0];
        for (int i = 1; i < nums.size(); ++i) {
            moves += nums[i] - min_n;
        }

        return moves;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int minMoves(vector<int>& nums) {
        sort(nums.begin(), nums.end());
        int min_n = nums[0];
        int max_n = nums[nums.size() - 1];
        int pos = nums.size() - 2;
        int moves = 0;

        while (min_n != max_n && pos >= 0) {
            moves += max_n - min_n;
            max_n = nums[pos] + moves;
            --pos;
            min_n = nums[0] + moves;
        }

        return moves;
    }
};
