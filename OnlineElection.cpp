/**
 * Online Election
 *
 * In an election, the i-th vote was cast for persons[i] at time times[i].
 *
 * Now, we would like to implement the following query function: TopVotedCandidate.q(int t) will return the number of
 * the person that was leading the election at time t.
 *
 * Votes cast at time t will count towards our query.  In the case of a tie, the most recent vote (among tied
 * candidates) wins.
 *
 * Example 1:
 *
 * Input: ["TopVotedCandidate","q","q","q","q","q","q"], [[[0,1,1,0,0,1,0],[0,5,10,15,20,25,30]],[3],[12],[25],[15],[24],[8]]
 * Output: [null,0,1,1,0,0,1]
 * Explanation:
 * At time 3, the votes are [0], and 0 is leading.
 * At time 12, the votes are [0,1,1], and 1 is leading.
 * At time 25, the votes are [0,1,1,0,0,1], and 1 is leading (as ties go to the most recent vote.)
 * This continues for 3 more queries at time 15, 24, and 8.
 *
 * Note:
 *     1 <= persons.length = times.length <= 5000
 *     0 <= persons[i] <= persons.length
 *     times is a strictly increasing array with all elements in [0, 10^9].
 *     TopVotedCandidate.q is called at most 10000 times per test case.
 *     TopVotedCandidate.q(int t) is always called with t >= times[0].
 */
/**
 * Original solution. The idea is:
 * 1. first we construct a vector of winner of corresponding time
 * 2. Using binary search to determin the time slot
 */
class TopVotedCandidate {
public:
    struct Winner {
        int id;
        int votes;
        Winner() : id{-1}, votes{0} {}
    };

    TopVotedCandidate(vector<int>& persons, vector<int>& times) :
            time_winner(times.size(), -1), time_ref{times} {
        Winner wnr;
        map<int, int> p_votes;

        for (size_t i = 0; i < times.size(); ++i) {
            int id = persons[i];
            int votes = ++p_votes[id];
            if (votes >= wnr.votes) {
                wnr.id = id;
                wnr.votes = votes;
            }

            time_winner[i] = wnr.id;
        }
    }

    int q(int t) {
        int left = 0;
        int right = time_ref.size() - 1;
        int pos = 0;
        while (left <= right) {
            int mid = (left + right) / 2;
            if (time_ref[mid] <= t) {
                pos = mid;
                left = mid + 1;
            }
            else {
                right = mid - 1;
            }
        }

        return time_winner[pos];
    }

private:
    vector<int> time_winner;
    vector<int>& time_ref;
};

/**
 * Your TopVotedCandidate object will be instantiated and called as such:
 * TopVotedCandidate* obj = new TopVotedCandidate(persons, times);
 * int param_1 = obj->q(t);
 */
