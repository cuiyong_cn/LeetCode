/**
 * Predict the Winner
 *
 * Given an array of scores that are non-negative integers. Player 1 picks one of the numbers from either end of the
 * array followed by the player 2 and then player 1 and so on. Each time a player picks a number, that number will not
 * be available for the next player. This continues until all the scores have been chosen. The player with the maximum
 * score wins.
 *
 * Given an array of scores, predict whether player 1 is the winner. You can assume each player plays to maximize his
 * score.
 *
 * Example 1:
 *
 * Input: [1, 5, 2] Output: False Explanation: Initially, player 1 can choose between 1 and 2.  If he chooses 2 (or 1),
 * then player 2 can choose from 1 (or 2) and 5. If player 2 chooses 5, then player 1 will be left with 1 (or 2).  So,
 * final score of player 1 is 1 + 2 = 3, and player 2 is 5.  Hence, player 1 will never be the winner and you need to
 * return False.
 *
 * Example 2:
 *
 * Input: [1, 5, 233, 7] Output: True Explanation: Player 1 first chooses 1. Then player 2 have to choose between 5 and
 * 7. No matter which number player 2 choose, player 1 can choose 233.  Finally, player 1 has more score (234) than
 * player 2 (12), so you need to return True representing player1 can win.
 *
 * Constraints:
 *
 * 1 <= length of the array <= 20.  Any scores in the given array are non-negative integers and will not exceed
 * 10,000,000.  If the scores of both players are equal, then player 1 is still the winner.
 */
/**
 * DP solution. Reduce to one dimension.
 */
class Solution {
public:
    bool PredictTheWinner(vector<int>& nums) {
        vector<int> dp(nums.size(), 0);

        for (int s = nums.size() - 1; s >= 0; --s) {
            dp[s] = nums[s];
            for (int e = s + 1; e < nums.size(); ++e) {
                int a = nums[s] - dp[e];
                int b = nums[e] - dp[e - 1];
                dp[e] = std::max(a, b);
            }
        }

        return dp[nums.size() - 1] >= 0;
    }
};

/**
 * DP solution.
 */
class Solution {
public:
    bool PredictTheWinner(vector<int>& nums) {
        vector<vector<int>> dp(nums.size(), vector<int>(nums.size(), 0));

        for (int s = nums.size() - 1; s >= 0; --s) {
            dp[s][s] = nums[s];
            for (int e = s + 1; e < nums.size(); ++e) {
                int a = nums[s] - dp[s + 1][e];
                int b = nums[e] - dp[s][e - 1];
                dp[s][e] = std::max(a, b);
            }
        }

        return dp[0][nums.size() - 1] >= 0;
    }
};

/**
 * Improve the following one using cache.
 */
class Solution {
public:
    bool PredictTheWinner(vector<int>& nums) {
        vector<vector<int>> sub_score(nums.size(), vector<int>(nums.size(), -1));

        return winner(nums, 0, nums.size() - 1, sub_score) >= 0;
    }

private:
    int winner(const vector<int>& nums, int s, int e, vector<vector<int>>& sub_score)
    {
        if (s == e) {
            return nums[s];
        }

        if (sub_score[s][e] == -1) {
            int a = nums[s] - winner(nums, s + 1, e, sub_score);
            int b = nums[e] - winner(nums, s, e - 1, sub_score);
            sub_score[s][e] = std::max(a, b);
        }

        return sub_score[s][e];
    }
};


/**
 * Recusive version. More compact and clear but not efficient as my original solution.
 */
class Solution {
public:
    bool PredictTheWinner(vector<int>& nums) {
        return winner(nums, 0, nums.size() - 1, 1) >= 0;
    }

private:
    int winner(const vector<int>& nums, int s, int e, int turn)
    {
        if (s == e) {
            return turn * nums[s];
        }

        int a = turn * nums[s] + winner(nums, s + 1, e, -turn);
        int b = turn * nums[e] + winner(nums, s, e - 1, -turn);

        return turn * std::max(turn * a, turn * b);
    }
};

/**
 * Original solution. Simulating method. I didn't see this method exists in Leetcode.
 * This is pretty cool I guess.
 */
class Solution {
public:
    bool PredictTheWinner(vector<int>& nums) {
        if (nums.size() == 1) {
            return true;
        }

        vector<Path> path(nums.size());
        path[0].branch[0] = 0;
        path[0].branch[1] = nums.size() - 1;

        bool win = true;
        int i = 0;

        while (0 <= i && i < path.size()) {
            int sign = i % 2 ? -1 : 1;
            if ((i + 1) == path.size()) {
                int idx = path[i].branch[0];
                path[i].sum = (i > 0 ? path[i - 1].sum : 0) + sign * nums[idx];

                if (path[i].sum >= 0) {
                    i -= -1 == sign ? 2 : 1;
                }
                else {
                    i -= 1 == sign ? 2 : 1;
                }
            }
            else if (path[i].cur < 2) {
                int cur = path[i].cur;
                int idx = path[i].branch[cur];
                path[i].sum =  (i > 0 ? path[i - 1].sum : 0) + sign * nums[idx];
                ++path[i].cur;

                path[i + 1].branch[0] = 0 == cur ? idx + 1 : path[i].branch[0];
                path[i + 1].branch[1] = 0 == cur ? path[i].branch[1] : idx - 1;
                path[i + 1].cur = 0;

                ++i;
            }
            else {
                i -= 2;
            }
        }

        return path.back().sum >= 0;
    }

    struct Path
    {
        int branch[2];
        int cur = 0;
        int sum = 0;
    };
};
