/**
 * Longest Square Streak in an Array
 *
 * You are given an integer array nums. A subsequence of nums is called a square streak if:
 *
 * The length of the subsequence is at least 2, and
 * after sorting the subsequence, each element (except the first element) is the square of the previous number.
 * Return the length of the longest square streak in nums, or return -1 if there is no square streak.
 *
 * A subsequence is an array that can be derived from another array by deleting
 * some or no elements without changing the order of the remaining elements.
 *
 * Example 1:
 *
 * Input: nums = [4,3,6,16,8,2]
 * Output: 3
 * Explanation: Choose the subsequence [4,16,2]. After sorting it, it becomes [2,4,16].
 * - 4 = 2 * 2.
 * - 16 = 4 * 4.
 * Therefore, [4,16,2] is a square streak.
 * It can be shown that every subsequence of length 4 is not a square streak.
 * Example 2:
 *
 * Input: nums = [2,3,5,6,7]
 * Output: -1
 * Explanation: There is no square streak in nums so return -1.
 *
 * Constraints:
 *
 * 2 <= nums.length <= 105
 * 2 <= nums[i] <= 105
 */
/**
 * Optimized version from discussion.
 */
class Solution {
public:
    int longestSquareStreak(vector<int>& n) {
	int res = 0;
	set<int> s;
	for(int val : n)
	    if (int sr = sqrt(val); val < 317 || sr * sr == val)
		s.insert(val);
	for (auto it = begin(s); it != end(s) && *it < 317; ++it) {
	    int sz = s.size();
	    for (int i = *it; i < 317 && s.count(i * i); i *= i)
		s.erase(i * i);
	    res = max(res, sz - (int)s.size());
	}
	return res == 0 ? -1 : res + 1;
    }
};
/**
 * Original solution.
 */
class Solution {
public:
    int longestSquareStreak(vector<int>& nums) {
        auto nset = set<int>{begin(nums), end(nums)};
        auto stop_value = int(sqrt(numeric_limits<int>::max()));
        auto ans = -1;

        while (!nset.empty()) {
            auto base = *begin(nset);
            auto nlen = 0;
            while (nset.count(base)) {
                ++nlen;
                nset.erase(base);
                if (base > stop_value) {
                    break;
                }
                base *= base;
            }
            ans = max(ans, nlen > 1 ? nlen : -1);
        }
        return ans;
    }
};
