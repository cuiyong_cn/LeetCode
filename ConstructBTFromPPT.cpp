/**
 * Construct Binary Tree from Preorder and Postorder Traversal
 *
  *Return any binary tree that matches the given preorder and postorder traversals.
 *
 * Values in the traversals pre and post are distinct positive integers.
 *
 *
 *
 * Example 1:
 *
 * Input: pre = [1,2,4,5,3,6,7], post = [4,5,2,6,7,3,1]
 * Output: [1,2,3,4,5,6,7]
 *
 * 
 *
 * Note:
 *
 *    1 <= pre.length == post.length <= 30
 *    pre[] and post[] are both permutations of 1, 2, ..., pre.length.
 *    It is guaranteed an answer exists. If there exists multiple answers, you can return any of them.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/**
 * Take a simple three nodes tree to depict the traversal.
 *
 * Pre-order: root --> left --> right
 * Post-order: left --> right --> root
 *
 * From the above, we know that root is the first one in pre-order,
 * but last in post-order. We can use this to construct every simple subtree.
 */
/**
 * Recursive version. I prefer the itertive one.
 */
class Solution {
    int pri = 0;
    int poi = 0;
public:
    TreeNode* constructFromPrePost(vector<int>& pre, vector<int>& post) {
        TreeNode* root = new TreeNode(pre[pri++]);
        if (root->val != post[poi]) {
            root->left = constructFromPrePost(pre, post);
        }
        if (root->val != post[poi]) {
            root->right = constructFromPrePost(pre, post);
        }
        ++poi;
        return root;
    }
};

/**
 * Iterative one
 */
class Solution {
public:
    TreeNode* constructFromPrePost(vector<int> pre, vector<int> post) {
        vector<TreeNode*> s;
        s.push_back(new TreeNode(pre[0]));
        for (int i = 1, j = 0; i < pre.size(); ++i) {
            TreeNode* node = new TreeNode(pre[i]);
            while (s.back()->val == post[j]) {
                // we complete a simple three nodes substree
                s.pop_back();
                ++j;
            }
            if (s.back()->left == nullptr) s.back()->left = node;
            else s.back()->right = node;
            s.push_back(node);
        }
        return s[0];
    }
};
