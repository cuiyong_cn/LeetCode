/**
 * Longest ZigZag Path in a Binary Tree
 *
 * Given a binary tree root, a ZigZag path for a binary tree is defined as follow:
 *
 *     Choose any node in the binary tree and a direction (right or left).
 *     If the current direction is right then move to the right child of the current node otherwise move to the left child.
 *     Change the direction from right to left or right to left.
 *     Repeat the second and third step until you can't move in the tree.
 *
 * Zigzag length is defined as the number of nodes visited - 1. (A single node has a length of 0).
 *
 * Return the longest ZigZag path contained in that tree.
 *
 * Example 1:
 *
 * Input: root = [1,null,1,1,1,null,null,1,1,null,1,null,null,null,1,null,1]
 * Output: 3
 * Explanation: Longest ZigZag path in blue nodes (right -> left -> right).
 *
 * Example 2:
 *
 * Input: root = [1,1,1,null,1,null,null,1,1,null,1]
 * Output: 4
 * Explanation: Longest ZigZag path in blue nodes (left -> right -> left -> right).
 *
 * Example 3:
 *
 * Input: root = [1]
 * Output: 0
 *
 * Constraints:
 *     Each tree has at most 50000 nodes..
 *     Each node's value is between [1, 100].
 */
/**
 * We eliminate half of unnecessary path exploration. I wonder if we can do better.
 */
class Solution {
public:
    enum class Direction {
        LEFT,
        RIGHT
    };

    struct NodeWrapper {
        int id;
        TreeNode* node;
        NodeWrapper(TreeNode* n, int num) : node{n}, id{num} {}
    };

    int maxZigZag(TreeNode* n, Direction dir) {
        int nodes = 0;

        while (n) {
            ++nodes;
            if (Direction::LEFT == dir) {
                n = n->left;
                dir = Direction::RIGHT;
            }
            else {
                n = n->right;
                dir = Direction::LEFT;
            }
        }
        return nodes - 1;
    }

    int longestZigZag(TreeNode* root) {
        if (nullptr == root) return 0;

        int ans = 0;
        memo = vector<int>(50001, 0);
        deque<NodeWrapper> q;
        q.push_back(NodeWrapper{root, 1});

        int nodeNum = 1;

        while (false == q.empty()) {
            int count = q.size();

            for (int i = 0; i < count; ++i) {
                NodeWrapper nr = q.front(); q.pop_front();
                TreeNode* t = nr.node;
                int lid = 0, rid = 0;
                if (t->left) {
                    lid = ++nodeNum;
                    q.push_back(NodeWrapper{t->left, lid});
                }

                if (t->right) {
                    rid = ++nodeNum;
                    q.push_back(NodeWrapper{t->right, rid});
                }

                memo[lid] = 2;
                memo[rid] = 1;
                if (0 == memo[nr.id]) {
                    ans = max(ans, maxZigZag(t, Direction::LEFT));
                    ans = max(ans, maxZigZag(t, Direction::RIGHT));
                }
                else if (1 == memo[nr.id]) {
                    ans = max(ans, maxZigZag(t, Direction::RIGHT));
                }
                else if (2 == memo[nr.id]) {
                    ans = max(ans, maxZigZag(t, Direction::LEFT));
                }
            }
        }
        
        return ans;
    }
private:
    vector<int> memo;
};

/**
 * I found this solution from disscussion. But it is not efficient compared with my solution above.
 * DFS solution. Pretty neat though.
 */
class Solution {
public:
    int longestZigZag(TreeNode* root) {
        return dfs(root)[2];
    }

    vector<int> dfs(TreeNode* root) {
        if (!root) return { -1, -1, -1};
        vector<int> left = dfs(root->left), right = dfs(root->right);
        int res = max(max(left[1], right[0]) + 1, max(left[2], right[2]));
        return {left[1] + 1, right[0] + 1, res};
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/**
 * Simple solution. But TLE. Looking this issue again, we can find that, if you expore a node from left, then
 * its child right path will have been explored too. We must find a way to take this advantage.
 */
class Solution {
public:
    enum class Direction {
        LEFT,
        RIGHT
    };
    int maxZigZag(TreeNode* n, Direction dir) {
        int nodes = 0;
        while (n) {
            ++nodes;
            if (Direction::LEFT == dir) {
                n = n->left;
                dir = Direction::RIGHT;
            }
            else {
                n = n->right;
                dir = Direction::LEFT;
            }
        }
        return nodes - 1;
    }

    int longestZigZag(TreeNode* root) {
        if (nullptr == root) return 0;

        int ans = 0;
        deque<TreeNode*> q;
        q.push_back(root);
        while (false == q.empty()) {
            int count = q.size();
            for (int i = 0; i < count; ++i) {
                TreeNode* t = q.front(); q.pop_front();
                if (t->left) {
                    q.push_back(t->left);
                }
                if (t->right) {
                    q.push_back(t->right);
                }
                ans = max(ans, maxZigZag(t, Direction::LEFT));
                ans = max(ans, maxZigZag(t, Direction::RIGHT));
            }
        }

        return ans;
    }
};
