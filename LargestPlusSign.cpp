/**
 * Largest Plus Sign
 *
 * In a 2D grid from (0, 0) to (N-1, N-1), every cell contains a 1, except those cells in the given list mines which are
 * 0. What is the largest axis-aligned plus sign of 1s contained in the grid? Return the order of the plus sign. If
 * there is none, return 0.
 *
 * An "axis-aligned plus sign of 1s of order k" has some center grid[x][y] = 1 along with 4 arms of length k-1 going up,
 * down, left, and right, and made of 1s. This is demonstrated in the diagrams below. Note that there could be 0s or 1s
 * beyond the arms of the plus sign, only the relevant area of the plus sign is checked for 1s.
 *
 * Examples of Axis-Aligned Plus Signs of Order k:
 *
 * Order 1:
 * 000
 * 010
 * 000
 *
 * Order 2:
 * 00000
 * 00100
 * 01110
 * 00100
 * 00000
 *
 * Order 3:
 * 0000000
 * 0001000
 * 0001000
 * 0111110
 * 0001000
 * 0001000
 * 0000000
 * Example 1:
 *
 * Input: N = 5, mines = [[4, 2]]
 * Output: 2
 * Explanation:
 * 11111
 * 11111
 * 11111
 * 11111
 * 11011
 * In the above grid, the largest plus sign can only be order 2.  One of them is marked in bold.
 * Example 2:
 *
 * Input: N = 2, mines = []
 * Output: 1
 * Explanation:
 * There is no plus sign of order 2, but there is of order 1.
 * Example 3:
 *
 * Input: N = 1, mines = [[0, 0]]
 * Output: 0
 * Explanation:
 * There is no plus sign, so return 0.
 * Note:
 *
 * N will be an integer in the range [1, 500].
 * mines will have length at most 5000.
 * mines[i] will be length 2 and consist of integers in the range [0, N-1].
 * (Additionally, programs submitted in C, C++, or C# will be judged with a slightly smaller time limit.)
 */
/**
 * From user fun4LeetCode in discussion. Reduce 4 matrix to 1
 */
class Solution {
public:
    int orderOfLargestPlusSign(int N, vector<vector<int>>& mines) {
        vector<vector<int>> grid(N, vector<int>(N, N));

        for (auto& m : mines) {
            grid[m[0]][m[1]] = 0;
        }

        for (int i = 0; i < N; i++) {
            for (int j = 0, k = N - 1, l = 0, r = 0, u = 0, d = 0; j < N; j++, k--) {
                grid[i][j] = min(grid[i][j], l = (grid[i][j] == 0 ? 0 : l + 1));
                grid[i][k] = min(grid[i][k], r = (grid[i][k] == 0 ? 0 : r + 1));
                grid[j][i] = min(grid[j][i], u = (grid[j][i] == 0 ? 0 : u + 1));
                grid[k][i] = min(grid[k][i], d = (grid[k][i] == 0 ? 0 : d + 1));
            }
        }

        int res = 0;

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                res = max(res, grid[i][j]);
            }
        }

        return res;
    }
};

/**
 * Original solution. Bit of terse, but very clear and easy to understand.
 */
class Solution {
public:
    int orderOfLargestPlusSign(int N, vector<vector<int>>& mines) {
        vector< vector<int> > board(N, vector<int>(N, 1));
        for (const auto& pair : mines) {
            board[pair[0]][pair[1]] = 0;
        }

        auto left = get_left(board);
        auto right = get_right(board);
        auto up = get_up(board);
        auto down = get_down(board);

        int ans = 0;
        for (int r = 0; r < N; ++r) {
            for (int c = 0; c < N; ++c) {
                int arms[] = {left[r][c], right[r][c], up[r][c], down[r][c]};
                ans = std::max(ans, *std::min_element(arms, arms + 4));
            }
        }

        return ans;
    }

private:
    vector< vector<int> > get_left(const vector< vector<int> >& board)
    {
        int N = board.size();
        vector< vector<int> > left(N, vector<int>(N, 0));
        for (int r = 0; r < N; ++r) {
            for (int c = 0; c < N; ++c) {
                if (board[r][c]) {
                    if (c > 0) {
                        left[r][c] = left[r][c - 1] + 1;
                    }
                    else {
                        left[r][c] = 1;
                    }
                }
                else {
                    left[r][c] = 0;
                }
            }
        }
        return left;
    }

    vector< vector<int> > get_right(const vector< vector<int> >& board)
    {
        int N = board.size();
        vector< vector<int> > right(N, vector<int>(N, 0));

        for (int r = N - 1; r >= 0; --r) {
            for (int c = N - 1; c >= 0; --c) {
                if (board[r][c]) {
                    if (c < N - 1) {
                        right[r][c] = right[r][c + 1] + 1;
                    }
                    else {
                        right[r][c] = 1;
                    }
                }
                else {
                    right[r][c] = 0;
                }
            }
        }
        return right;
    }

    vector< vector<int> > get_up(const vector< vector<int> >& board)
    {
        int N = board.size();
        vector< vector<int> > up(N, vector<int>(N, 0));

        for (int c = 0; c < N; ++c) {
            for (int r = 0; r < N; ++r) {
                if (board[r][c]) {
                    if (r > 0) {
                        up[r][c] = up[r - 1][c] + 1;
                    }
                    else {
                        up[r][c] = 1;
                    }
                }
                else {
                    up[r][c] = 0;
                }
            }
        }
        return up;
    }

    vector< vector<int> > get_down(const vector< vector<int> >& board)
    {
        int N = board.size();
        vector< vector<int> > down(N, vector<int>(N, 0));

        for (int c = N - 1; c >= 0; --c) {
            for (int r = N - 1; r >= 0; --r) {
                if (board[r][c]) {
                    if (r < N - 1) {
                        down[r][c] = down[r + 1][c] + 1;
                    }
                    else {
                        down[r][c] = 1;
                    }
                }
                else {
                    down[r][c] = 0;
                }
            }
        }

        return down;
    }
};
