/**
 * Excel Sheet Column Title
 *
 * Given an integer columnNumber, return its corresponding column title as it appears in an Excel
 * sheet.
 *
 * For example:
 *
 * A -> 1
 * B -> 2
 * C -> 3
 * ...
 * Z -> 26
 * AA -> 27
 * AB -> 28
 * ...
 *
 * Example 1:
 *
 * Input: columnNumber = 1
 * Output: "A"
 * Example 2:
 *
 * Input: columnNumber = 28
 * Output: "AB"
 * Example 3:
 *
 * Input: columnNumber = 701
 * Output: "ZY"
 * Example 4:
 *
 * Input: columnNumber = 2147483647
 * Output: "FXSHRXW"
 *
 * Constraints:
 *
 * 1 <= columnNumber <= 231 - 1
 */
/**
 * Simplify the code. Also we can rewrite as the recusive one.
 */
class Solution {
public:
    string convertToTitle(int columnNumber) {
        string ans;
        while (--columnNumber >= 0) {
            ans += 'A' + (columnNumber % 26);
            columnNumber = columnNumber / 26;
        }

        reverse(begin(ans), end(ans));

        return ans;
    }
};

/**
 * Original solution. This is seems like a 26 based number system, but it has minor diffrence.
 */
class Solution {
public:
    string convertToTitle(int columnNumber) {
        string ans;
        while (columnNumber > 26) {
            int quotient = columnNumber / 26;
            int rem = columnNumber - quotient * 26;
            ans += 'A' + (rem > 0 ? rem - 1 : 25);
            columnNumber = quotient - (0 == rem);
        }

        ans += 'A' + (columnNumber - 1);

        reverse(begin(ans), end(ans));

        return ans;
    }
};
