/**
 * Subarray Product Less Than K
 *
 * Your are given an array of positive integers nums.
 *
 * Count and print the number of (contiguous) subarrays where the product of all the elements in the
 * subarray is less than k.
 *
 * Example 1:
 * Input: nums = [10, 5, 2, 6], k = 100
 * Output: 8
 * Explanation: The 8 subarrays that have product less than 100 are: [10], [5], [2], [6], [10, 5],
 * [5, 2], [2, 6], [5, 2, 6].
 * Note that [10, 5, 2] is not included as the product of 100 is not strictly less than k.
 * Note:
 *
 * 0 < nums.length <= 50000.
 * 0 < nums[i] < 1000.
 * 0 <= k < 10^6.
 */
/**
 * A little short one.
 */
class Solution {
public:
    int numSubarrayProductLessThanK(vector<int>& nums, int k) {
        if (k < 2) {
            return 0;
        }

        auto product = 1;
        auto ans = 0;
        size_t left = 0;
        for (size_t right = 0; right < nums.size(); ++right) {
            product *= nums[right];
            while (product >= k) {
                product /= nums[left];
                ++left;
            }

            ans += right - left + 1;
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int numSubarrayProductLessThanK(vector<int>& nums, int k) {
        auto product = 1;
        auto ans = 0;
        size_t i = 0, j = 0;
        for (; j < nums.size();) {
            auto new_prod = product * nums[j];
            if (new_prod >= k) {
                if (i < j) {
                    product = product / nums[i];
                }
                else {
                    j = i + 1;
                }
                ++i;
            }
            else {
                ans += j - i + 1;
                product = new_prod;
                ++j;
            }
        }

        return ans;
    }
};
