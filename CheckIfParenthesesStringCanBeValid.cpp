/**
 * Check if a Parentheses String Can Be Valid
 *
 * A parentheses string is a non-empty string consisting only of '(' and ')'. It is valid if any of
 * the following conditions is true:
 *
 * It is ().
 * It can be written as AB (A concatenated with B), where A and B are valid parentheses strings.
 * It can be written as (A), where A is a valid parentheses string.
 * You are given a parentheses string s and a string locked, both of length n. locked is a binary
 * string consisting only of '0's and '1's. For each index i of locked,
 *
 * If locked[i] is '1', you cannot change s[i].
 * But if locked[i] is '0', you can change s[i] to either '(' or ')'.
 * Return true if you can make s a valid parentheses string. Otherwise, return false.
 *
 * Example 1:
 *
 * Input: s = "))()))", locked = "010100"
 * Output: true
 * Explanation: locked[1] == '1' and locked[3] == '1', so we cannot change s[1] or s[3].
 * We change s[0] and s[4] to '(' while leaving s[2] and s[5] unchanged to make s valid.
 * Example 2:
 *
 * Input: s = "()()", locked = "0000"
 * Output: true
 * Explanation: We do not need to make any changes because s is already valid.
 * Example 3:
 *
 * Input: s = ")", locked = "0"
 * Output: false
 * Explanation: locked permits us to change s[0].
 * Changing s[0] to either '(' or ')' will not make s valid.
 *
 * Constraints:
 *
 * n == s.length == locked.length
 * 1 <= n <= 105
 * s[i] is either '(' or ')'.
 * locked[i] is either '0' or '1'.
 */
/**
 * Another one.
 */
bool canBeValid(string s, string locked) {
    auto validate = [&](char op) {
        int bal = 0, wild = 0, sz = s.size();
        int start = op == '(' ? 0 : sz - 1, dir = op == '(' ? 1 : - 1;
        for (int i = start; i >= 0 && i < sz && wild + bal >= 0; i += dir)
            if (locked[i] == '1')
                bal += s[i] == op ? 1 : -1;
            else
                ++wild;
        return abs(bal) <= wild;
    };

    return s.size() % 2 == 0 && validate('(') && validate(')');
}

/**
 * Solution from Dicussion. I need to think about this one.
 * Especially the proof.
 */
class Solution {
public:
    bool canBeValid(string s, string l) {
        if (s.size() % 2 == 1) return false;
        int total = 0, open = 0, closed = 0;
        for(int i = s.size() - 1; i >= 0; i--) {
            if (l[i] == '0')
                total += 1;
            else if (s[i] == '(')
                open += 1;
            else if (s[i] == ')')
                closed += 1;
            if (total - open + closed < 0) return false;
        }
        total = open = closed = 0;
        for(int i = 0; i < s.size(); i++) {
            if (l[i] == '0')
                total += 1;
            else if (s[i] == '(')
                open += 1;
            else if (s[i] == ')')
                closed += 1;
            if (total + open - closed < 0) return false;
        }
        return true;
    }
};

/**
 * Did not pass this one. Below is the most close one but still wrong!
 * Also the code is real messy.
 */
bool impossible_head_tail(string const& s, string const& locked)
{
    return ('1' == locked.front() && ')' == s.front())
        || ('1' == locked.back() && '(' == s.back());
}

class Solution {
public:
    bool canBeValid(string s, string locked) {
        if (s.length() & 1) {
            return false;
        }

        if (impossible_head_tail(s, locked)) {
            return false;
        }

        auto ssize = static_cast<int>(s.length());
        auto free_block = vector<int>{};
        auto pre_frees = int{0};
        auto frees = int{0};

        for (auto i = int{0}; i < ssize; ++i) {
            if ('0' == locked[i]) {
                ++frees;
                continue;
            }

            if (')' == s[i]) {
                if (free_block.empty()) {
                    if (0 == frees) {
                        return false;
                    }
                    --frees;
                }
                else {
                    while (!free_block.empty()) {
                        if (0 == frees) {
                            pre_frees -= free_block.back();
                            frees = free_block.back();
                            free_block.pop_back();
                            break;
                        }
                        else if (pre_frees > 0 || free_block.size() > 1) {
                            pre_frees -= free_block.back();
                            frees += free_block.back() - 1;
                            free_block.pop_back();
                        }
                        else {
                            --frees;
                            break;
                        }
                    }
                }
            }
            else {
                pre_frees += frees;
                free_block.emplace_back(frees);
                frees = 0;
            }
        }

        while (!free_block.empty()) {
            if (0 == frees) {
                return false;
            }
            frees += free_block.back() - 1;
            free_block.pop_back();
        }

        return true;
    }
};
