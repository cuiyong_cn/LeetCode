/**
 * String Compression III
 *
 * Given a string word, compress it using the following algorithm:
 *     Begin with an empty string comp. While word is not empty, use the following operation:
 *         Remove a maximum length prefix of word made of a single character c repeating at most 9 times.
 *         Append the length of the prefix followed by c to comp.
 *
 * Return the string comp.
 *
 * Example 1:
 *
 * Input: word = "abcde"
 *
 * Output: "1a1b1c1d1e"
 *
 * Explanation:
 *
 * Initially, comp = "". Apply the operation 5 times, choosing "a", "b", "c", "d", and "e" as the prefix in each operation.
 *
 * For each prefix, append "1" followed by the character to comp.
 *
 * Example 2:
 *
 * Input: word = "aaaaaaaaaaaaaabb"
 *
 * Output: "9a5a2b"
 *
 * Explanation:
 *
 * Initially, comp = "". Apply the operation 3 times, choosing "aaaaaaaaa", "aaaaa", and "bb" as the prefix in each operation.
 *     For prefix "aaaaaaaaa", append "9" followed by "a" to comp.
 *     For prefix "aaaaa", append "5" followed by "a" to comp.
 *     For prefix "bb", append "2" followed by "b" to comp.
 *
 * Constraints:
 *     1 <= word.length <= 2 * 105
 *     word consists only of lowercase English letters.
 */
/**
 * Single loop.
 */
class Solution {
public:
    string compressedString(string word) {
        auto const wsize = word.length();
        auto ch = word[0];
        auto len = 0;
        auto ans = string{};

        word += '0';

        for (auto c : word) {
            if (len == 9 || c != ch) {
                ans += '0' + len;
                ans += ch;
                len = 1;
                ch = c;
            }
            else {
                ++len;
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    string compressedString(string word) {
        auto const wsize = word.length();
        auto ans = string{};

        for (auto i = 0uz; i < wsize; ) {
            auto const c = word[i];
            auto len = 1;

            for (auto j = i + 1; j < wsize && len < 9; ++j) {
                if (c != word[j]) {
                    break;
                }
                ++len;
            }

            ans += to_string(len) + c;
            i += len;
        }

        return ans;
    }
};
