/**
 * Most Profit Assigning Work
 *
 * We have jobs: difficulty[i] is the difficulty of the ith job, and profit[i] is the profit of the
 * ith job.
 *
 * Now we have some workers. worker[i] is the ability of the ith worker, which means that this
 * worker can only complete a job with difficulty at most worker[i].
 *
 * Every worker can be assigned at most one job, but one job can be completed multiple times.
 *
 * For example, if 3 people attempt the same job that pays $1, then the total profit will be $3.  If
 * a worker cannot complete any job, his profit is $0.
 *
 * What is the most profit we can make?
 *
 * Example 1:
 *
 * Input: difficulty = [2,4,6,8,10], profit = [10,20,30,40,50], worker = [4,5,6,7]
 * Output: 100
 * Explanation: Workers are assigned jobs of difficulty [4,4,6,6] and they get profit of
 * [20,20,30,30] seperately.
 * Notes:
 *
 * 1 <= difficulty.length = profit.length <= 10000
 * 1 <= worker.length <= 10000
 * difficulty[i], profit[i], worker[i]  are in range [1, 10^5]
 */
/**
 * More efficient one.
 */
class Solution {
public:
    int maxProfitAssignment(vector<int>& difficulty, vector<int>& profit, vector<int>& worker) {
        size_t N = difficulty.size();
        vector<pair<int, int>> job_profit(N, {0, 0});
        for (size_t i = 0; i < N; ++i) {
            job_profit[i] = {difficulty[i], profit[i]};
        }

        sort(begin(job_profit), end(job_profit));
        sort(begin(worker), end(worker));

        int ans = 0;
        int cur_max_profit = 0;
        int i = 0;
        for (auto n : worker) {
            while (i < N && n >= job_profit[i].first) {
                if (cur_max_profit < job_profit[i].second) {
                    cur_max_profit = job_profit[i].second;
                }
                ++i;
            }
            ans += cur_max_profit;
        }

        return ans;
    }
};

/**
 * Use vector instead of map to speed it up.
 */
class Solution {
public:
    int maxProfitAssignment(vector<int>& difficulty, vector<int>& profit, vector<int>& worker) {
        vector<int> idx(difficulty.size(), 0);
        for (size_t i = 1; i < idx.size(); ++i) {
            idx[i] = i;
        }

        sort(begin(idx), end(idx),
            [&difficulty](auto const& a, auto const& b) {
                return difficulty[a] <= difficulty[b];
            });

        vector<pair<int, int>> job_profit;
        int cur_max_profit = 0;
        for (auto n : idx) {
            auto job_difficulty = difficulty[n];
            cur_max_profit = max(cur_max_profit, profit[n]);
            job_profit.push_back({job_difficulty, cur_max_profit});
        }

        int ans = 0;
        for (auto n : worker) {
            auto it = upper_bound(begin(job_profit), end(job_profit), make_pair(n, 100001));
            if (it != begin(job_profit)) {
                --it;
                ans += it->second;
            }
        }

        return ans;
    }
};

/**
 * Original solution. TLE.
 */
class Solution {
public:
    int maxProfitAssignment(vector<int>& difficulty, vector<int>& profit, vector<int>& worker) {
        vector<int> idx(difficulty.size(), 0);
        for (size_t i = 1; i < idx.size(); ++i) {
            idx[i] = i;
        }

        sort(begin(idx), end(idx),
            [&difficulty](auto const& a, auto const& b) {
                return difficulty[a] <= difficulty[b];
            });

        map<int, int> job_profit;
        int cur_max_profit = 0;
        for (auto n : idx) {
            auto job_difficulty = difficulty[n];
            cur_max_profit = max(cur_max_profit, profit[n]);
            job_profit[job_difficulty] = cur_max_profit;
        }

        int ans = 0;
        for (auto n : worker) {
            auto it = upper_bound(begin(job_profit), end(job_profit), pair<const int, int>{n, 100001});
            if (it != begin(job_profit)) {
                --it;
                ans += it->second;
            }
        }

        return ans;
    }
};
