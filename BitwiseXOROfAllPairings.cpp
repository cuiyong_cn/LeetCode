/**
 * Bitwise XOR of All Pairings
 *
 * You are given two 0-indexed arrays, nums1 and nums2, consisting of non-negative integers. There
 * exists another array, nums3, which contains the bitwise XOR of all pairings of integers between
 * nums1 and nums2 (every integer in nums1 is paired with every integer in nums2 exactly once).
 *
 * Return the bitwise XOR of all integers in nums3.
 *
 * Example 1:
 *
 * Input: nums1 = [2,1,3], nums2 = [10,2,5,0]
 * Output: 13
 * Explanation:
 * A possible nums3 array is [8,0,7,2,11,3,4,1,9,1,6,3].
 * The bitwise XOR of all these numbers is 13, so we return 13.
 * Example 2:
 *
 * Input: nums1 = [1,2], nums2 = [3,4]
 * Output: 0
 * Explanation:
 * All possible pairs of bitwise XORs are nums1[0] ^ nums2[0], nums1[0] ^ nums2[1], nums1[1] ^ nums2[0],
 * and nums1[1] ^ nums2[1].
 * Thus, one possible nums3 array is [2,5,1,6].
 * 2 ^ 5 ^ 1 ^ 6 = 0, so we return 0.
 *
 * Constraints:
 *
 * 1 <= nums1.length, nums2.length <= 105
 * 0 <= nums1[i], nums2[j] <= 109*
 */
/**
 * More readable.
 */
int xor_all(vector<int> const& nums)
{
    return accumulate(begin(nums), end(nums), 0,
        [](auto acc, auto n) {
            return acc ^ n;
        });
}

int xor_case(vector<int> const& nums1, vector<int> const& nums2)
{
    auto const n1_odd = nums1.size() & 1;
    auto const n2_odd = nums2.size() & 1;
    return n1_odd && n2_odd ? 1 :
           n1_odd ? 2 :
           n2_odd ? 3 : 4;
}

class Solution {
public:
    int xorAllNums(vector<int>& nums1, vector<int>& nums2) {
        auto ans = 0;
        switch (xor_case(nums1, nums2)) {
            case 1:
                ans = xor_all(nums1) ^ xor_all(nums2);
                break;

            case 2:
                ans = xor_all(nums2);
                break;

            case 3:
                ans = xor_all(nums1);
                break;

            default:
                break;
        }
        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int xorAllNums(vector<int>& nums1, vector<int>& nums2) {
        auto const n1_size = nums1.size();
        auto const n2_size = nums2.size();
        if (0 == (n1_size % 2)) {
            if (0 == (n2_size % 2)) {
                return 0;
            }
            return accumulate(begin(nums1), end(nums1), 0,
                [](auto acc, auto n) {
                    return acc ^ n;
                });
        }

        if (0 == (n2_size % 2)) {
            return accumulate(begin(nums2), end(nums2), 0,
                [](auto acc, auto n) {
                    return acc ^ n;
                });
        }

        return accumulate(begin(nums1), end(nums1), 0,
                [](auto acc, auto n) {
                    return acc ^ n;
                })
                ^
                accumulate(begin(nums2), end(nums2), 0,
                [](auto acc, auto n) {
                    return acc ^ n;
                });;
    }
};
