/**
 * Task Scheduler
 *
 * Given a characters array tasks, representing the tasks a CPU needs to do, where each letter
 * represents a different task. Tasks could be done in any order. Each task is done in one unit of
 * time. For each unit of time, the CPU could complete either one task or just be idle.
 *
 * However, there is a non-negative integer n that represents the cooldown period between two same
 * tasks (the same letter in the array), that is that there must be at least n units of time between
 * any two same tasks.
 *
 * Return the least number of units of times that the CPU will take to finish all the given tasks.
 *
 * Example 1:
 *
 * Input: tasks = ["A","A","A","B","B","B"], n = 2
 * Output: 8
 * Explanation:
 * A -> B -> idle -> A -> B -> idle -> A -> B
 * There is at least 2 units of time between any two same tasks.
 * Example 2:
 *
 * Input: tasks = ["A","A","A","B","B","B"], n = 0
 * Output: 6
 * Explanation: On this case any permutation of size 6 would work since n = 0.
 * ["A","A","A","B","B","B"]
 * ["A","B","A","B","A","B"]
 * ["B","B","B","A","A","A"]
 * ...
 * And so on.
 * Example 3:
 *
 * Input: tasks = ["A","A","A","A","A","A","B","C","D","E","F","G"], n = 2
 * Output: 16
 * Explanation:
 * One possible solution is
 * A -> B -> C -> A -> D -> E -> A -> F -> G -> A -> idle -> idle -> A -> idle -> idle -> A
 *
 * Constraints:
 *
 * 1 <= task.length <= 104
 * tasks[i] is upper-case English letter.
 * The integer n is in the range [0, 100].
 */
/**
 * Solution. without simulation.
 */
class Solution {
    using TaskCnt = unordered_map<char, int>;
public:
    int leastInterval(vector<char>& tasks, int n) {
        TaskCnt tc;
        int max_cnt = 0;
        for (auto t : tasks) {
            ++tc[t];
            max_cnt = max(max_cnt, tc[t]);
        }

        int ans = (max_cnt - 1) * (n + 1);
        for (auto t : tc) {
            if (t.second == max_cnt) {
                ++ans;
            }
        }

        return max<int>(tasks.size(), ans);
    }
};

/**
 * Original solution. Simulate the schedule process.
 */
class Solution {
    struct Task
    {
        int cnt = 0;
        int cooldown = 0;
    };

public:
    int leastInterval(vector<char>& tasks, int n) {
        vector<Task> tk(26);

        for (auto t : tasks) {
            tk[t - 'A'].cnt += 1;
        }

        auto have_task = true;
        auto elapse = 0;
        while (have_task) {
            // find the task that has most cnt
            auto finished = true;
            int ti = 0;
            int max_cnt = 0;
            for (int i = 0; i < tk.size(); ++i) {
                if (tk[i].cnt > max_cnt) {
                    finished = false;
                    if (0 == tk[i].cooldown) {
                        ti = i;
                        max_cnt = tk[i].cnt;
                    }
                }
            }

            if (0 == max_cnt) {
                if (finished) {
                    // we have no more tasks
                    have_task = false;
                }
            }
            else {
                tk[ti].cnt -= 1;
                tk[ti].cooldown = n + 1;
            }

            // for other task, cooldown - 1
            for (auto& t : tk) {
                if (t.cooldown > 0) {
                    --t.cooldown;
                }
            }
            ++elapse;
        }

        return elapse - 1;
    }
};
