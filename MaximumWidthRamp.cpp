/**
 * Maximum Width Ramp
 *
 * Given an array A of integers, a ramp is a tuple (i, j) for which i < j and A[i] <= A[j].  The width of such a ramp is
 * j - i.
 *
 * Find the maximum width of a ramp in A.  If one doesn't exist, return 0.
 *
 * Example 1:
 *
 * Input: [6,0,8,2,1,5]
 * Output: 4
 * Explanation:
 * The maximum width ramp is achieved at (i, j) = (1, 5): A[1] = 0 and A[5] = 5.
 * Example 2:
 *
 * Input: [9,8,1,0,1,9,4,0,4,1]
 * Output: 7
 * Explanation:
 * The maximum width ramp is achieved at (i, j) = (2, 9): A[2] = 1 and A[9] = 1.
 *
 * Note:
 *
 * 2 <= A.length <= 50000
 * 0 <= A[i] <= 50000
 */
/**
 * From discussion. The solution beats me. - -!
 * The diea is:
 *      stack's top element always store index that
 *      carry the smallest value
 */
class Solution {
public:
    int maxWidthRamp(vector<int>& A) {
        stack<int> s;
        int res = 0, n = A.size();
        for (int i = 0; i < n; ++i) {
            if (s.empty() || A[s.top()] > A[i]) {
                s.push(i);
            }
        }

        for (int i = n - 1; i > res; --i) {
            while (!s.empty() && A[s.top()] <= A[i]) {
                res = max(res, i - s.top());
                s.pop();
            }
        }

        return res;
    }
};

/**
 * A little difficult to understand than the above one.
 */
class Solution {
public:
    int maxWidthRamp(vector<int>& A)
    {
        vector<pair<int, int>> vec;
        int n = A.size();

        for (int i = 0; i < n; ++i) vec.push_back({A[i], i});
        sort(vec.begin(), vec.end());

        int i = 0, j = 1, res = 0;
        while (j < n)
        {
            if (vec[i].second <= vec[j].second)
            {
                res = max(res, vec[j].second - vec[i].second);
                ++j;
            }
            else {
                ++i;
            }
        }

        return res;
    }
};

/**
 * Again stuck me for a long time. And finnaly got a passed one.
 * I think the difficult part is:
 * how to find to way to skip the redundent comparision and this one
 * hold me for long time.
 */
class Solution {
public:
    int maxWidthRamp(vector<int>& A) {
        vector<int> dp(A.size(), -1);

        dp[0] = -1;
        for (int i = 1; i < A.size(); ++i) {
            int j = i - 1;
            while (j >= 0) {
                if (A[i] >= A[j]) {
                    dp[i] = j;
                    j = dp[j] >= 0 ? dp[j] : j - 1;
                }
                else if (dp[j] < 0) {
                    break;
                }
                else {
                    int k = dp[j];
                    for (; k < j; ++k) {
                        if (A[k] <= A[i]) {
                            dp[i] = k;
                            break;
                        }
                    }
                    break;
                }
            }
        }

        int ans = 0;
        for (int i = 1; i < dp.size(); ++i) {
            if (dp[i] >= 0) {
                ans = std::max(ans, i - dp[i]);
            }
        }

        return ans;
    }
};


/**
 * Original solution. Definitely correct. But as expected, TLE.
 */
class Solution {
public:
    int maxWidthRamp(vector<int>& A) {
        int D = A.size() - 1;

        for (int D = A.size() - 1; D > 0; --D) {
            for (int i = 0; (i + D) < A.size(); ++i) {
                if (A[i] <= A[i + D]) {
                    return D;
                }
            }
        }

        return 0;
    }
};
