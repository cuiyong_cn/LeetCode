/**
 * Maximize the Confusion of an Exam
 *
 * A teacher is writing a test with n true/false questions, with 'T' denoting true and 'F' denoting
 * false. He wants to confuse the students by maximizing the number of consecutive questions with
 * the same answer (multiple trues or multiple falses in a row).
 *
 * You are given a string answerKey, where answerKey[i] is the original answer to the ith question.
 * In addition, you are given an integer k, the maximum number of times you may perform the
 * following operation:
 *
 * Change the answer key for any question to 'T' or 'F' (i.e., set answerKey[i] to 'T' or 'F').
 * Return the maximum number of consecutive 'T's or 'F's in the answer key after performing the
 * operation at most k times.
 *
 * Example 1:
 *
 * Input: answerKey = "TTFF", k = 2
 * Output: 4
 * Explanation: We can replace both the 'F's with 'T's to make answerKey = "TTTT".
 * There are four consecutive 'T's.
 * Example 2:
 *
 * Input: answerKey = "TFFT", k = 1
 * Output: 3
 * Explanation: We can replace the first 'T' with an 'F' to make answerKey = "FFFT".
 * Alternatively, we can replace the second 'T' with an 'F' to make answerKey = "TFFF".
 * In both cases, there are three consecutive 'F's.
 * Example 3:
 *
 * Input: answerKey = "TTFTTFTT", k = 1
 * Output: 5
 * Explanation: We can replace the first 'F' to make answerKey = "TTTTTFTT"
 * Alternatively, we can replace the second 'F' to make answerKey = "TTFTTTTT".
 * In both cases, there are five consecutive 'T's.
 *
 * Constraints:
 *
 * n == answerKey.length
 * 1 <= n <= 5 * 104
 * answerKey[i] is either 'T' or 'F'
 * 1 <= k <= n
 */
/**
 * More compact one. Only one turn.
 */
class Solution {
public:
    int maxConsecutiveAnswers(string s, int k) {
        int maxf = 0, i = 0, n = s.length();
        vector<int> count(26);
        for (int j = 0; j < n; ++j) {
            maxf = max(maxf, ++count[s[j] - 'A']);
            if (j - i + 1 > maxf + k) {
                --count[s[i++] - 'A'];
            }
        }
        return n - i;
    }
};

/**
 * Original solution.
 * This is classic sliding window problem. But I stuck at coding a correct one for about 1.5H.
 */
int maxConsecutive(string const& answerKey, int k, char expected)
{
    int ans = 0;
    int left = 0;
    int right = 0;
    while (right < answerKey.length()) {
        if (expected != answerKey[right]) {
            --k;
        }

        if (k < 0) {
            if (expected != answerKey[left]) {
                ++k;
            }
            ++left;
        }
        ++right;

        if (k >= 0) {
            ans = max(ans, right - left);
        }
    }

    return ans;
}

class Solution {
public:
    int maxConsecutiveAnswers(string answerKey, int k) {
        return max(maxConsecutive(answerKey, k, 'T'), maxConsecutive(answerKey, k, 'F'));
    }
};
