/**
 * Find Largest Value in Each Tree Row
 *
 * You need to find the largest value in each row of a binary tree.
 *
 * Example:
 *
 * Input:
 *
 *           1
 *          / \
 *         3   2
 *        / \   \
 *       5   3   9
 *
 * Output: [1, 3, 9]
 */
/**
 * Orignal bfs approach using deque.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<int> largestValues(TreeNode* root) {
        deque<TreeNode*> q;
        vector<int> ans;
        if (nullptr != root) {
            q.push_back(root);
            while (false == q.empty()) {
                int qSize = q.size();
                int maxVal = INT_MIN;
                for (int i = 0; i < qSize; ++i) {
                    TreeNode* n = q.front();
                    q.pop_front();
                    maxVal = max(maxVal, n->val);
                    if (nullptr != n->left) q.push_back(n->left);
                    if (nullptr != n->right) q.push_back(n->right);
                }
                ans.push_back(maxVal);
            }
        }

        return ans;
    }
};

/**
 * DFS version
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
private:
    void dfs(TreeNode* n, int height, vector<int>& ans)
    {
        if (nullptr != n) {
            if (height >= ans.size()) {
                ans.push_back(n->val);
            }
            else {
                ans[height] = max(ans[height], n->val);
            }

            dfs(n->left, height + 1, ans);
            dfs(n->right, height + 1, ans);
        }
    }
public:
    vector<int> largestValues(TreeNode* root) {
        vector<int> ans;
        dfs(root, 0, ans);

        return ans;
    }
};
