/**
 * Maximum Strength of a Group
 *
 * You are given a 0-indexed integer array nums representing the score of
 * students in an exam. The teacher would like to form one non-empty group of
 * students with maximal strength, where the strength of a group of students of
 * indices i0, i1, i2, ... , ik is defined as nums[i0] * nums[i1] * nums[i2] *
 * ... * nums[ik​].
 *
 * Return the maximum strength of a group the teacher can create.
 *
 * Example 1:
 *
 * Input: nums = [3,-1,-5,2,5,-9]
 * Output: 1350
 * Explanation: One way to form a group of maximal strength is to group the students at indices [0,2,3,4,5]. Their strength is 3 * (-5) * 2 * 5 * (-9) = 1350, which we can show is optimal.
 * Example 2:
 *
 * Input: nums = [-4,-5,-4]
 * Output: 20
 * Explanation: Group the students at indices [0, 1] . Then, we’ll have a resulting strength of 20. We cannot achieve greater strength.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 13
 * -9 <= nums[i] <= 9
 */
/**
 * O(n) solution.
 */
class Solution {
public:
    long long maxStrength(vector<int>& nums) {
        auto ans = 1LL;
        auto leastNegative = numeric_limits<int>::min();
        auto largest = leastNegative;
        auto neg_count = 0;

        for (auto n : nums) {
            if (n) {
                ans *= n;
            }
            if (n < 0) {
                leastNegative = max(leastNegative, n);
                ++neg_count;
            }
            largest = max(largest, n);
        }

        if (0 == largest && neg_count < 2) {
            return 0;
        }

        if (largest < 0 && 1 == neg_count) {
            return leastNegative;
        }

        if (ans > 0) {
            return ans;
        }

        return ans / leastNegative;
    }
};

/**
 * Original solution.
 */
bool negative_number(int n)
{
    return n < 0;
}

bool positive_number(int n)
{
    return n > 0;
}

bool odd_number(int n)
{
    return n % 2;
}

auto product(vector<int> const& nums)
{
    return accumulate(begin(nums), end(nums), 1LL,
                [](auto m, auto n) {
                    return m * n;
                });
}

class Solution {
public:
    long long maxStrength(vector<int>& nums) {
        auto negatives = vector<int>{};
        auto positives = vector<int>{};

        copy_if(begin(nums), end(nums), back_inserter(negatives), negative_number);
        copy_if(begin(nums), end(nums), back_inserter(positives), positive_number);

        auto neg_count = negatives.size();

        if (odd_number(neg_count)) {
            sort(begin(negatives), end(negatives));
            if (neg_count > 1 || !positives.empty() || neg_count < nums.size()) {
                negatives.pop_back();
            }
        }

        if (negatives.empty() && positives.empty()) {
            return 0;
        }

        return product(negatives) * product(positives);
    }
};
