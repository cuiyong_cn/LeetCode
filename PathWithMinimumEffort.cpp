/**
 * Path With Minimum Effort
 *
 * You are a hiker preparing for an upcoming hike. You are given heights, a 2D array of size rows x
 * columns, where heights[row][col] represents the height of cell (row, col). You are situated in
 * the top-left cell, (0, 0), and you hope to travel to the bottom-right cell, (rows-1, columns-1)
 * (i.e., 0-indexed). You can move up, down, left, or right, and you wish to find a route that
 * requires the minimum effort.
 *
 * A route's effort is the maximum absolute difference in heights between two consecutive cells of
 * the route.
 *
 * Return the minimum effort required to travel from the top-left cell to the bottom-right cell.
 *
 * Example 1:
 *
 * Input: heights = [[1,2,2],[3,8,2],[5,3,5]]
 * Output: 2
 * Explanation: The route of [1,3,5,3,5] has a maximum absolute difference of 2 in consecutive cells.
 * This is better than the route of [1,2,2,2,5], where the maximum absolute difference is 3.
 * Example 2:
 *
 * Input: heights = [[1,2,3],[3,8,4],[5,3,5]]
 * Output: 1
 * Explanation: The route of [1,2,3,4,5] has a maximum absolute difference of 1 in consecutive cells, which is better than route [1,3,5,3,5].
 * Example 3:
 *
 * Input: heights = [[1,2,1,1,1],[1,2,1,2,1],[1,2,1,2,1],[1,2,1,2,1],[1,1,1,2,1]]
 * Output: 0
 * Explanation: This route does not require any effort.
 *
 * Constraints:
 *
 * rows == heights.length
 * columns == heights[i].length
 * 1 <= rows, columns <= 100
 * 1 <= heights[i][j] <= 106
 */
/**
 * Dijikstra solution.
 */
class Solution {
public:
    int minimumEffortPath(vector<vector<int>>& heights) {
        int n = heights.size();
        int m = heights[0].size();

        priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> pq;
        vector<vector<bool>> visited(n, vector<bool>(m, false));

        pq.push({0, 0});

        int dir[5] ={-1, 0, 1, 0, -1};

        int ans = -1;

        while(!pq.empty())
        {
            pair<int, int> tp = pq.top();
            pq.pop();

            int x = tp.second/100;
            int y = tp.second%100;

            if(visited[x][y]==true) continue;

            visited[x][y] = true;

            if(x==n-1 && y == m-1)
            {
                ans = tp.first;
                break;
            }

            for(int i=0; i<4; i++)
            {
                int nx = x + dir[i];
                int ny = y + dir[i+1];

                int npos = nx*100+ny;


                if (nx < 0 || nx >= n || ny < 0 || ny >= m || visited[nx][ny]) continue;
                pq.push({max(tp.first, abs(heights[nx][ny]-heights[x][y])), npos});
            }
        }

        return ans;
    }
};

/**
 * Original binary solution.
 */
class Solution {
public:
    int minimumEffortPath(vector<vector<int>>& heights) {
        int left = 0;
        int right = 1'000'000;
        int const R = heights.size();
        int const C = heights.front().size();

        while (left < right) {
            int mid = (left + right) / 2;
            bool feasible = false;
            unordered_set<int> visited;
            vector<pair<int, int>> cells = { {0, 0} };
            auto enqueue_if_feasible = [R, C, mid, &cells, &heights](int x, int y, int h) {
                if (x < 0 || x >= R || y < 0 || y >= C) {
                    return;
                }

                if (abs(heights[x][y] - h) <= mid) {
                    cells.emplace_back(x, y);
                }
            };

            while (!cells.empty()) {
                auto [x, y] = cells.back(); cells.pop_back();
                if ((R - 1) == x && (C - 1) == y) {
                    feasible = true;
                    break;
                }

                if (0 == visited.count((x << 8) | y)) {
                    visited.insert((x << 8) | y);
                    enqueue_if_feasible(x, y - 1, heights[x][y]);
                    enqueue_if_feasible(x, y + 1, heights[x][y]);
                    enqueue_if_feasible(x - 1, y, heights[x][y]);
                    enqueue_if_feasible(x + 1, y, heights[x][y]);
                }
            }

            if (feasible) {
                right = mid;
            }
            else {
                left = mid + 1;
            }
        }

        return left;
    }
};
