/**
 * Valid Boomerang
 *
 * Given an array points where points[i] = [xi, yi] represents a point on the X-Y plane, return true
 * if these points are a boomerang.
 *
 * A boomerang is a set of three points that are all distinct and not in a straight line.
 *
 * Example 1:
 *
 * Input: points = [[1,1],[2,3],[3,2]]
 * Output: true
 * Example 2:
 *
 * Input: points = [[1,1],[2,2],[3,3]]
 * Output: false
 *
 * Constraints:
 *
 * points.length == 3
 * points[i].length == 2
 * 0 <= xi, yi <= 100
 */
/**
 * Another solution. Check if the area of the triangle these three points formed is 0.
 * area =  x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)
 */
class Solution {
public:
    bool isBoomerang(vector<vector<int>>& p) {
        return p[0][0] * (p[1][1] - p[2][1]) + p[1][0] * (p[2][1] - p[0][1]) + p[2][0] * (p[0][1] - p[1][1]) != 0;
    }
}

/**
 * Original solution. using below idea.
 *    (y0 - y1)   (y1 - y2)
 * if --------- = ---------, it means (x0, y0), (x1, y1), (x2, y2) are in a straight line.
 *    (x0 - x1)   (x1 - x2)
 */
class Solution {
public:
    bool isBoomerang(vector<vector<int>>& points) {
        return (points[0][1] - points[1][1]) * (points[1][0] - points[2][0])
            != (points[0][0] - points[1][0]) * (points[1][1] - points[2][1]);
    }
};
