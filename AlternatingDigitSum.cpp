/**
 * Alternating Digit Sum
 * You are given a positive integer n. Each digit of n has a sign according to the following rules:
 *
 * The most significant digit is assigned a positive sign.
 * Each other digit has an opposite sign to its adjacent digits.
 * Return the sum of all digits with their corresponding sign.
 *
 * Example 1:
 *
 * Input: n = 521
 * Output: 4
 * Explanation: (+5) + (-2) + (+1) = 4.
 * Example 2:
 *
 * Input: n = 111
 * Output: 1
 * Explanation: (+1) + (-1) + (+1) = 1.
 * Example 3:
 *
 * Input: n = 886996
 * Output: 0
 * Explanation: (+8) + (-8) + (+6) + (-9) + (+9) + (-6) = 0.
 *
 * Constraints:
 *
 * 1 <= n <= 109
 */
/**
 * Just adjust the sign at the end
 */
class Solution {
public:
    int alternateDigitSum(int n) {
        auto sign = 1;
        auto sum = 0;
        while (n > 0) {
            sign *= -1;
            sum += sign * (n % 10);
            n /= 10;
        }
        return sign * sum;
    }
};

/**
 * Original solution.
 */
auto number_of_digits(int num)
{
    auto n = 1;
    while (num > 9) {
        ++n;
        num /= 10;
    }
    return n;
}

class Solution {
public:
    int alternateDigitSum(int n) {
        auto digits = number_of_digits(n);
        auto sign = digits & 1 ? 1 : -1;
        auto sum = 0;
        while (n > 9) {
            sum += sign * (n % 10);
            n /= 10;
            sign *= -1;
        }
        return sum + sign * n;
    }
};
