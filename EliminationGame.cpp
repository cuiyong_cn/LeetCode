/**
 * Elimination Game
 *
 * There is a list of sorted integers from 1 to n. Starting from left to right, remove the first number and every other
 * number afterward until you reach the end of the list.
 *
 * Repeat the previous step again, but this time from right to left, remove the right most number and every other number
 * from the remaining numbers.
 *
 * We keep repeating the steps again, alternating left to right and right to left, until a single number remains.
 *
 * Find the last number that remains starting with a list of length n.
 *
 * Example:
 *
 * Input:
 * n = 9,
 * 1 2 3 4 5 6 7 8 9
 * 2 4 6 8
 * 2 6
 * 6
 *
 * Output:
 * 6
 */
/**
 * From discussion.
 *
 * 1. remain starts as n, divide by 2 each row, stop the loop when remain=1.
 * 2. Integer step caches the distance the head might need to move, starts as 1, multiply by 2 each row.
 * 3. direction starts as ->, turn around each row.
 * 4. when direction is ->, the head move forward;
 * 5. when direction is <-, the head move forward if remain is odd, else does nothing.
 */
class Solution {
public:
    int lastRemaining(int n) {
      int head = 1, step = 1;
      bool left_to_right = true;
      while (n > 1) {
          if (left_to_right) {
              head += step;
          }
          else {
              head += n % 2 == 0 ? 0 : step;
          }
          step *= 2;
          n /= 2;
          left_to_right = !left_to_right;
      }

      return head;
    }
};
/**
 * Clearly I was wrong, not a math problem. But we have below observation.
 *
 * After first elimination, all the numbers left are even numbers.
 * Divide by 2, we get a continuous new sequence from 1 to n / 2.
 * For this sequence we start from right to left as the first elimination.
 * Then the original result should be two times the mirroring result of lastRemaining(n / 2).
 */
class Solution {
public:
    int lastRemaining(int n) {
        return n == 1 ? 1 : 2 * (1 + n / 2 - lastRemaining(n / 2));
    }
};

/**
 * Original solution. Emulating method. TLE sadly.
 * I think it is a math problem.
 */
class Solution {
public:
    int lastRemaining(int n) {
        vector<int> current(n, 0);
        vector<int> remain(n, 0);

        for (int i = 0; i < current.size(); ++i) {
            current[i] = i + 1;
        }

        bool left_to_right = true;
        for (int len = n; len > 1; len = len / 2) {
            if (left_to_right) {
                left_to_right = false;

                int j = 0;
                for (int i = 1; i < len; i += 2) {
                    remain[j] = current[i];
                    ++j;
                }
            }
            else {
                left_to_right = true;

                int j = (len / 2) - 1;
                for (int i = len - 2; i >= 0; i -= 2) {
                    remain[j] = current[i];
                    --j;
                }
            }

            swap(current, remain);
        }

        return current.front();
    }
};
