/**
 * Largest Perimeter Triangle
 *
 * Given an array A of positive lengths, return the largest perimeter of a triangle with
 * non-zero area, formed from 3 of these lengths.
 *
 * If it is impossible to form any triangle of non-zero area, return 0.
 *
 * Example 1:
 *
 * Input: [2,1,2]
 * Output: 5
 *
 * Example 2:
 *
 * Input: [1,2,1]
 * Output: 0
 *
 * Example 3:
 *
 * Input: [3,2,3,4]
 * Output: 10
 *
 * Example 4:
 *
 * Input: [3,6,2,3]
 * Output: 8
 *
 * Note:
 *
 *     3 <= A.length <= 10000
 *     1 <= A[i] <= 10^6
 */
/**
 * Improved version.
 */
class Solution {
public:
    int largestPerimeter(vector<int>& A) {
        sort(A.begin(), A.end());
        int ans = 0;
        for (int i = A.size() - 3; i >= 0; --i) {
            if ((A[i] + A[i + 1]) > A[i + 2]) {
                ans = A[i] + A[j] + A[k];
                break;
            }
        }
        return ans;
    }
};
/**
 * My original solution. Simple and fast.
 */
class Solution {
public:
    int largestPerimeter(vector<int>& A) {
        sort(A.begin(), A.end(), [](int a, int b) { return a < b; });
        int ans = 0;
        for (int i = A.size() - 1; i >= 0; --i) {
            for (int j = i - 1; j >= 0; --j) {
                for (int k = j - 1; k >=0; --k) {
                    if ((A[i] - A[j]) < A[k]) {
                        ans = max(ans, A[i] + A[j] + A[k]);
                        i = 0;
                    }
                    j = 0; k = 0;
                }
            }
        }
        return ans;
    }
};
