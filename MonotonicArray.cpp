/**
 * Monotonic Array
 *
 * An array is monotonic if it is either monotone increasing or monotone decreasing.
 *
 * An array A is monotone increasing if for all i <= j, A[i] <= A[j].  An array A is monotone decreasing if for all i <= j, A[i] >= A[j].
 *
 * Return true if and only if the given array A is monotonic.
 *
 * Example 1:
 *
 * Input: [1,2,2,3]
 * Output: true
 *
 * Example 2:
 *
 * Input: [6,5,4,4]
 * Output: true
 *
 * Example 3:
 *
 * Input: [1,3,2]
 * Output: false
 *
 * Example 4:
 *
 * Input: [1,2,4,5]
 * Output: true
 *
 * Example 5:
 *
 * Input: [1,1,1]
 * Output: true
 *
 * Note:
 *
 *     1 <= A.length <= 50000
 *     -100000 <= A[i] <= 100000
 */
/**
 * One pass solution. most elegent.
 */
class Solution {
public:
    bool isMonotonic(vector<int>& A) {
        bool incr = true;
        bool decr = true;
        int end = A.size() - 1;
        for (int i = 0; i < end; ++i) {
            if (incr && A[i] > A[i + 1]) {
                incr = false;
            }
            if (decr && A[i] < A[i + 1]) {
                decr = false;
            }

        }
        return incr || decr;
    }
};

/**
 * Improved versioin.
 */
class Solution {
public:
    bool isMonotonic(vector<int>& A) {
        int store = 0;
        int end = A.size() - 1;
        for (int i = 0; i < end; ++i) {
            int c = A[i] < A[i + 1] ? 1 : A[i] > A[i + 1] ? -1 : 0;
            if (c != 0) {
                if (c != store && store != 0) {
                    return false;
                }
                store = c;
            }
        }
        return true;
    }
};

/**
 * My original solution. But not very clean. better using two pass.
 * First pass determine the monotone. Second pass to determine if
 * it is monotone array.
 */
class Solution {
public:
    bool isMonotonic(vector<int>& A) {
        if (A.size() <= 2) return true;
        Monotone prev = Monotone::DECR;
        if (A[1] == A[0]) {
            prev = Monotone::EQ;
        }
        else if (A[1] > A[0]) {
            prev = Monotone::INCR;
        }
        bool mtone = false;
        for (int i = 2; i < A.size(); ++i) {
            Monotone cur = A[i] > A[i - 1] ? Monotone::INCR : A[i] == A[i - 1] ? Monotone::EQ : Monotone::DECR;
            if (cur != prev && cur != Monotone::EQ && prev != Monotone::EQ) {
                return false;
            }
            else {
                if (false == mtone && cur != Monotone::EQ) {
                    prev = cur;
                    mtone = true;
                }
            }
        }
        return true;
    }
private:
    enum class Monotone { DECR, INCR, EQ };
};
