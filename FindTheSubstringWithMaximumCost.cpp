/**
 * Find the Substring With Maximum Cost
 *
 * You are given a string s, a string chars of distinct characters and an
 * integer array vals of the same length as chars.
 *
 * The cost of the substring is the sum of the values of each character in the
 * substring. The cost of an empty string is considered 0.
 *
 * The value of the character is defined in the following way:
 *
 * If the character is not in the string chars, then its value is its
 * corresponding position (1-indexed) in the alphabet.
 *
 * For example, the value of 'a' is 1, the value of 'b' is 2, and so on. The value of 'z' is 26.
 * Otherwise, assuming i is the index where the character occurs in the string chars, then its value is vals[i].
 * Return the maximum cost among all substrings of the string s.
 *
 * Example 1:
 *
 * Input: s = "adaa", chars = "d", vals = [-1000]
 * Output: 2
 * Explanation: The value of the characters "a" and "d" is 1 and -1000 respectively.
 * The substring with the maximum cost is "aa" and its cost is 1 + 1 = 2.
 * It can be proven that 2 is the maximum cost.
 * Example 2:
 *
 * Input: s = "abc", chars = "abc", vals = [-1,-1,-1]
 * Output: 0
 * Explanation: The value of the characters "a", "b" and "c" is -1, -1, and -1 respectively.
 * The substring with the maximum cost is the empty substring "" and its cost is 0.
 * It can be proven that 0 is the maximum cost.
 *
 * Constraints:
 *
 * 1 <= s.length <= 105
 * s consist of lowercase English letters.
 * 1 <= chars.length <= 26
 * chars consist of distinct lowercase English letters.
 * vals.length == chars.length
 * -1000 <= vals[i] <= 1000
 */
/**
 * Reduce the dp space complexity to O(1).
 * Algo called Kadane algo
 */
int char_value(char c, string const& chars, vector<int> const& vals)
{
    auto p = chars.find(c);
    if (string::npos == p) {
        return c - 'a' + 1;
    }

    return vals[p];
}

vector<int> to_value_array(string const& s, array<int, 26> const& vals)
{
    auto size = s.size();
    auto arr = vector<int>(size, 0);
    for (auto i = 0 * size; i < size; ++i) {
        arr[i] = vals[s[i] - 'a'];
    }

    return arr;
}

int max_subarray_sum(vector<int> const& arr)
{
    auto size = arr.size();
    auto max_sum_so_far = 0;
    auto max_sum_ending_here = 0;

    for (auto&& n : arr) {
        max_sum_ending_here = max(max_sum_ending_here + n, n < 0 ? 0 : n);
        max_sum_so_far = max(max_sum_so_far, max_sum_ending_here);
    }

    return max_sum_so_far;
}

class Solution {
public:
    int maximumCostSubstring(string s, string chars, vector<int>& vals) {
        auto char_val = array<int, 26>{ 0, };
        for (auto c = 'a'; c <= 'z'; ++c) {
            auto i = c - 'a';
            char_val[i] = char_value(c, chars, vals);
        }

        auto val_array = to_value_array(s, char_val);
        return max_subarray_sum(val_array);
    }
};

/**
 * Original solution.
 */
int char_value(char c, string const& chars, vector<int> const& vals)
{
    auto p = chars.find(c);
    if (string::npos == p) {
        return c - 'a' + 1;
    }

    return vals[p];
}

vector<int> to_value_array(string const& s, array<int, 26> const& vals)
{
    auto size = s.size();
    auto arr = vector<int>(size, 0);
    for (auto i = 0 * size; i < size; ++i) {
        arr[i] = vals[s[i] - 'a'];
    }

    return arr;
}

int max_subarray_sum(vector<int> const& arr)
{
    auto size = arr.size();
    auto dp = vector<int>(size, 0);
    dp[0] = arr[0] < 0 ? 0 : arr[0];

    for (auto i = 1; i < size; ++i) {
        dp[i] = max(dp[i - 1] + arr[i], arr[i] < 0 ? 0 : arr[i]);
    }

    return *max_element(begin(dp), end(dp));
}

class Solution {
public:
    int maximumCostSubstring(string s, string chars, vector<int>& vals) {
        auto char_val = array<int, 26>{ 0, };
        for (auto c = 'a'; c <= 'z'; ++c) {
            auto i = c - 'a';
            char_val[i] = char_value(c, chars, vals);
        }

        auto val_array = to_value_array(s, char_val);
        return max_subarray_sum(val_array);
    }
};
