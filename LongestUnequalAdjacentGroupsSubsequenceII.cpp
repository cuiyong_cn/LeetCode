/**
 * Longest Unequal Adjacent Groups Subsequence II
 *
 * You are given a string array words, and an array groups, both arrays having length n.
 *
 * The hamming distance between two strings of equal length is the number of positions at which the
 * corresponding characters are different.
 *
 * You need to select the longest
 * subsequence
 * from an array of indices [0, 1, ..., n - 1], such that for the subsequence denoted as [i0, i1,
 * ..., ik-1] having length k, the following holds:
 *
 *     For adjacent indices in the subsequence, their corresponding groups are unequal, i.e.,
 *     groups[ij] != groups[ij+1], for each j where 0 < j + 1 < k.
 *     words[ij] and words[ij+1] are equal in length, and the hamming distance between them is 1,
 *     where 0 < j + 1 < k, for all indices in the subsequence.
 *
 * Return a string array containing the words corresponding to the indices (in order) in the
 * selected subsequence. If there are multiple answers, return any of them.
 *
 * Note: strings in words may be unequal in length.
 *
 * Example 1:
 *
 * Input: words = ["bab","dab","cab"], groups = [1,2,2]
 *
 * Output: ["bab","cab"]
 *
 * Explanation: A subsequence that can be selected is [0,2].
 *     groups[0] != groups[2]
 *     words[0].length == words[2].length, and the hamming distance between them is 1.
 *
 * So, a valid answer is [words[0],words[2]] = ["bab","cab"].
 *
 * Another subsequence that can be selected is [0,1].
 *     groups[0] != groups[1]
 *     words[0].length == words[1].length, and the hamming distance between them is 1.
 *
 * So, another valid answer is [words[0],words[1]] = ["bab","dab"].
 *
 * It can be shown that the length of the longest subsequence of indices that satisfies the conditions is 2.
 *
 * Example 2:
 *
 * Input: words = ["a","b","c","d"], groups = [1,2,3,4]
 *
 * Output: ["a","b","c","d"]
 *
 * Explanation: We can select the subsequence [0,1,2,3].
 *
 * It satisfies both conditions.
 *
 * Hence, the answer is [words[0],words[1],words[2],words[3]] = ["a","b","c","d"].
 *
 * It has the longest length among all subsequences of indices that satisfy the conditions.
 *
 * Hence, it is the only answer.
 *
 * Constraints:
 *     1 <= n == words.length == groups.length <= 1000
 *     1 <= words[i].length <= 10
 *     1 <= groups[i] <= n
 *     words consists of distinct strings.
 *     words[i] consists of lowercase English letters.
 */
/**
 * Original solution.
 */
auto ham_dist(string const& s1, string const& s2)
{
    if (s1.length() != s2.length()) {
        return -1;
    }

    int const n = s1.length();
    auto cnt = 0;

    for (auto i = 0; i < n; ++i) {
        if (s1[i] != s2[i]) {
            ++cnt;
        }
    }

    return cnt;
}

auto condition_met(vector<string> const& words, vector<int> const& groups, int i, int j)
{
    return groups[i] != groups[j]
        && 1 == ham_dist(words[i], words[j]);
}

auto collect_sub(vector<string> const& words, vector<int> const& index, int p)
{
    auto sub = vector<string>{};

    while (p >= 0) {
        sub.push_back(words[p]);
        p = index[p];
    }

    return sub;
}

class Solution {
public:
    vector<string> getWordsInLongestSubsequence(vector<string>& words, vector<int>& groups) {
        int const n = words.size();
        auto dp = vector<int>(n, 0);
        auto index = vector<int>(n, -1);

        dp[0] = 1;

        for (auto i = 1; i < n; ++i) {
            auto max_j = -1;

            for (auto j = i - 1; j >= 0; --j) {
                auto new_len = condition_met(words, groups, i, j) ? dp[j] + 1 : -1;

                if (new_len > dp[i]) {
                    max_j = j;
                    dp[i] = new_len;
                }
            }

            index[i] = max_j;
        }

        auto max_it = max_element(dp.begin(), dp.end());
        auto max_len = dp[distance(dp.begin(), max_it)];
        auto ans = vector<string>{};

        for (auto i = 0; i < n; ++i) {
            if (max_len != dp[i]) {
                continue;
            }

            auto sub = collect_sub(words, index, i);

            if (sub.size() > ans.size()) {
                swap(sub, ans);
            }
        }

        reverse(ans.begin(), ans.end());

        return ans;
    }
};
