/** Minimum Area Rectangle
 *
 * Given a set of points in the xy-plane, determine the minimum area of a rectangle formed from these points, with sides
 * parallel to the x and y axes.
 *
 * If there isn't any rectangle, return 0.
 *
 * Example 1:
 *
 * Input: [[1,1],[1,3],[3,1],[3,3],[2,2]] Output: 4
 *
 * Example 2:
 *
 * Input: [[1,1],[1,3],[3,1],[3,3],[4,1],[4,3]] Output: 2
 *
 * Note:
 *     1 <= points.length <= 500 0 <= points[i][0] <= 40000 0 <= points[i][1] <= 40000 All points are distinct.
 */
/**
 * More clean solution.
 */
class Solution
{
    int minAreaRect(vector<vector<int>>& points, int res = INT_MAX)
    {
        unordered_map<int, set<int>> x;
        for (auto p : points) x[p[0]].insert(p[1]);

        for (auto i = x.begin(); i != x.end(); ++i)
            for (auto j = next(i); j != x.end(); ++j) {
                if (i->second.size() < 2 || j->second.size() < 2) continue;

                vector<int> y;
                set_intersection(begin(i->second), end(i->second),
                                 begin(j->second), end(j->second), back_inserter(y));
                for (int k = 1; k < y.size(); ++k)
                    res = min(res, abs(j->first - i->first) * (y[k] - y[k - 1]));
            }
        return res == INT_MAX ? 0 : res;
    }
};

/**
 * Original solution. This is brute force I think and not elegant at all.
 */
class Solution
{
public:
    map<int, vector<int>> sameX;
    map<int, vector<int>> sameY;

    int minAreaRect(vector<vector<int>>& points)
    {
        sort(points.begin(), points.end(),
        [](const vector<int>& p1, const vector<int>& p2) {
            bool before = false;
            if (p1[0] < p2[0]) {
                before = true;
            } else if (p1[0] == p2[0]) {
                if (p1[1] < p2[1]) {
                    before = true;
                }
            }
            return before;
        });

        for (auto& p : points) {
            sameX[p[0]].push_back(p[1]);
            sameY[p[1]].push_back(p[0]);
        }

        int ans = numeric_limits<int>::max();
        for (auto& x : sameX) {
            auto px = x.first;
            auto& pts = x.second;
            for (int i = 0; i < pts.size(); ++i) {
                for (int j = i + 1; j < pts.size(); ++j) {
                    ans = min(ans, minArea(px, pts[i], pts[j]));
                }
            }
        }

        return ans == numeric_limits<int>::max() ? 0 : ans;
    }

    int minArea(int x, int y1, int y2)
    {
        int ans = numeric_limits<int>::max();
        for (auto& px : sameY[y1]) {
            if (px <= x) continue;
            bool found = false;
            for (auto& py : sameX[px]) {
                if (y2 == py) {
                    found = true;
                    ans = (py - y1) * (px - x);
                    break;
                }
            }
            if (found) break;
        }

        return ans;
    }
};

/**
 * The diagonal approach. Though written in java. Pretty intresting.  For each pair of points in the array, consider
 * them to be the long diagonal of a potential rectangle. We can check if all 4 points are there using a Set.
 *
 * For example, if the points are (1, 1) and (5, 5), we check if we also have (1, 5) and (5, 1). If we do, we have a
 * candidate rectangle.
 */
class Solution
{
    public int minAreaRect(int[][] points)
    {
        Set<Integer> pointSet = new HashSet();
        for (int[] point : points)
            pointSet.add(40001 * point[0] + point[1]);

        int ans = Integer.MAX_VALUE;
        for (int i = 0; i < points.length; ++i)
            for (int j = i + 1; j < points.length; ++j) {
                if (points[i][0] != points[j][0] && points[i][1] != points[j][1]) {
                    if (pointSet.contains(40001 * points[i][0] + points[j][1]) &&
                            pointSet.contains(40001 * points[j][0] + points[i][1])) {
                        ans = Math.min(ans, Math.abs(points[j][0] - points[i][0]) *
                                       Math.abs(points[j][1] - points[i][1]));
                    }
                }
            }

        return ans < Integer.MAX_VALUE ? ans : 0;
    }
};
