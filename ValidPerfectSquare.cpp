/**
 * Valid Perfect Square
 *
 * Given a positive integer num, write a function which returns True if num is a perfect square else
 * False.
 *
 * Follow up: Do not use any built-in library function such as sqrt.
 *
 * Example 1:
 *
 * Input: num = 16
 * Output: true
 * Example 2:
 *
 * Input: num = 14
 * Output: false
 *
 * Constraints:
 *
 * 1 <= num <= 2^31 - 1
 */
/**
 * A square num is 1 + 3 + 5 + 7...
 */
class Solution {
public:
    bool isPerfectSquare(int num) {
        int i = 1;
        while (num > 0) {
            num -= i;
            i += 2;
        }

        return 0 == num;
    }
};

/**
 * Binary solution.
 */
class Solution {
public:
    bool isPerfectSquare(int num) {
        int left = 1;
        int right = num;

        while (left < right) {
            auto mid = (left + right) / 2;
            auto res = num / mid;
            auto remain = num % mid;
            if (res == mid && 0 == remain) {
                return true;
            }
            if (res > mid) {
                left = mid + 1;
            }
            else {
                right = mid - 1;
            }
        }

        return false;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool isPerfectSquare(int num) {
        int sr = sqrt(num);
        return (sr * sr) == num;
    }
};
