/**
 * Count Odd Numbers in an Interval Range
 *
 * Given two non-negative integers low and high. Return the count of odd numbers between low and
 * high (inclusive).
 *
 * Example 1:
 *
 * Input: low = 3, high = 7
 * Output: 3
 * Explanation: The odd numbers between 3 and 7 are [3,5,7].
 * Example 2:
 *
 * Input: low = 8, high = 10
 * Output: 1
 * Explanation: The odd numbers between 8 and 10 are [9].
 *
 * Constraints:
 *
 * 0 <= low <= high <= 10^9
 */
/**
 * one line solution.
 */
class Solution {
public:
    int countOdds(int low, int high) {
        return (high + 1) / 2 - low / 2;
        // or return (high - low) / 2 + ((low | high) & 1);
    }
};

/**
 * O(1) solution.
 */
class Solution {
public:
    int countOdds(int low, int high) {
        int ans = (low & 1) + (high & 1);
        if (low & 1) {
            ++low;
        }

        if (high & 1) {
            --high;
        }

        return ans + (high - low) / 2;
    }
};

/**
 * Original solution. Brute force.
 */
class Solution {
public:
    int countOdds(int low, int high) {
        int ans = 0;
        for (int i = low; i <= high; ++i) {
            if (i & 1) {
                ++ans;
            }
        }
        return ans;
    }
};
