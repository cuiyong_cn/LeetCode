/**
 * Count of Substrings Containing Every Vowel and K Consonants I
 *
 * You are given a string word and a non-negative integer k.
 *
 * Return the total number of substrings
 * of word that contain every vowel ('a', 'e', 'i', 'o', and 'u') at least once and exactly k consonants.
 *
 * Example 1:
 *
 * Input: word = "aeioqq", k = 1
 *
 * Output: 0
 *
 * Explanation:
 *
 * There is no substring with every vowel.
 *
 * Example 2:
 *
 * Input: word = "aeiou", k = 0
 *
 * Output: 1
 *
 * Explanation:
 *
 * The only substring with every vowel and zero consonants is word[0..4], which is "aeiou".
 *
 * Example 3:
 *
 * Input: word = "ieaouqqieaouqq", k = 1
 *
 * Output: 3
 *
 * Explanation:
 *
 * The substrings with every vowel and one consonant are:
 *     word[0..5], which is "ieaouq".
 *     word[6..11], which is "qieaou".
 *     word[7..12], which is "ieaouq".
 *
 * Constraints:
 *     5 <= word.length <= 250
 *     word consists only of lowercase English letters.
 *     0 <= k <= word.length - 5
 */
/**
 * Original solution.
 */
auto is_vowel(char c)
{
    return ('a' == c) || ('e' == c) || ('i' == c) || ('o' == c) || ('u' == c);
}

auto vowel_attr(string_view s)
{
    auto consonants = 0;
    auto vowel = bitset<32>{};

    for (auto c : s) {
        if (is_vowel(c)) {
            vowel[c - 'a'] = true;
        }
        else {
            ++consonants;
        }
    }

    return pair{vowel.count(), consonants};
}

class Solution {
public:
    int countOfSubstrings(string word, int k) {
        auto ans = 0;
        auto const size = word.length();

        for (auto i = 0uz; i < size; ++i) {
            for (auto j = i + 4 + k; j < size; ++j) {
                auto substr = string_view{word.data() + i, j - i + 1};
                auto [vowel, consonants] = vowel_attr(substr);
                if (consonants > k) {
                    break;
                }

                if (vowel > 4 && consonants == k) {
                    ++ans;
                }
            }
        }

        return ans;
    }
};
