/**
 * Count Good Meals
 *
 * A good meal is a meal that contains exactly two different food items with a sum of deliciousness
 * equal to a power of two.
 *
 * You can pick any two different foods to make a good meal.
 *
 * Given an array of integers deliciousness where deliciousness[i] is the deliciousness of the
 * ith item of food, return the number of different good meals you can make from this list modulo
 * 109 + 7.
 *
 * Note that items with different indices are considered different even if they have the same
 * deliciousness value.
 *
 * Example 1:
 *
 * Input: deliciousness = [1,3,5,7,9]
 * Output: 4
 * Explanation: The good meals are (1,3), (1,7), (3,5) and, (7,9).
 * Their respective sums are 4, 8, 8, and 16, all of which are powers of 2.
 * Example 2:
 *
 * Input: deliciousness = [1,1,1,3,3,3,7]
 * Output: 15
 * Explanation: The good meals are (1,1) with 3 ways, (1,3) with 9 ways, and (1,7) with 3 ways.
 *
 * Constraints:
 *
 * 1 <= deliciousness.length <= 105
 * 0 <= deliciousness[i] <= 220
 */
/**
 * work along the vector
 */
class Solution {
public:
    int countPairs(vector<int>& deliciousness) {
        unordered_set<int64_t> power_of_2_sets;
        for (int i = 0, p = 1; i < 22; ++i) {
            power_of_2_sets.insert(p);
            p *= 2;
        }

        unordered_map<int, int> num_cnt;
        int const mod = 1'000'000'007;
        int ans = 0;
        for (auto const& n : deliciousness) {
            for (auto const& p2 : power_of_2_sets) {
                auto other = p2 - n;
                if (num_cnt.count(other)) {
                    ans += num_cnt[other];
                }

                ans %= mod;
            }

            ++num_cnt[n];
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int countPairs(vector<int>& deliciousness) {
        unordered_set<int64_t> power_of_2_sets;
        for (int i = 0, p = 1; i < 22; ++i) {
            power_of_2_sets.insert(p);
            p *= 2;
        }

        unordered_map<int64_t, int64_t> num_cnt;
        for (auto const& n : deliciousness) {
            ++num_cnt[n];
        }

        int const mod = 1'000'000'007;
        int64_t ans = 0;
        for (auto const& [num, cnt] : num_cnt) {
            for (auto const& p2 : power_of_2_sets) {
                auto const other = p2 - num;
                if (num_cnt.count(other)) {
                    if (other == num) {
                        ans += cnt * (cnt - 1) / 2;
                    }
                    else if (num < other) {
                        ans += cnt * num_cnt[other];
                    }
                }

                ans %= mod;
            }
        }

        return ans;
    }
};
