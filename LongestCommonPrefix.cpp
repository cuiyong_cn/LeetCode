/**
 * Longest Common Prefix
 *
 * Write a function to find the longest common prefix string amongst an array of strings.
 *
 * If there is no common prefix, return an empty string ""
 * Example 1:
 *
 * Input: strs = ["flower","flow","flight"]
 * Output: "fl"
 *
 * Example 2:
 *
 * Input: strs = ["dog","racecar","car"]
 * Output: ""
 * Explanation: There is no common prefix among the input strings.
 *
 * Constraints:
 *
 *     1 <= strs.length <= 200
 *     0 <= strs[i].length <= 200
 *     strs[i] consists of only lower-case English letters.
 */
/**
 * Original solution.
 */
class Solution {
public:
    string longestCommonPrefix(vector<string>& strs) {
        auto it = min_element(begin(strs), end(strs),
                [](auto& a, auto& b) {
                return a.length() < b.length();
                });

        int const N = it->length();
        int L = 0;
        for (; L < N; ++L) {
            bool common = true;
            auto ch = strs.front()[L];
            for (auto s : strs) {
                if (ch != s[L]) {
                    common = false;
                    break;
                }
            }
            if (!common) {
                break;
            }
        }

        return strs.front().substr(0, L);
    }
};
