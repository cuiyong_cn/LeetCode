/**
 * Sort Colors
 *
 * Given an array nums with n objects colored red, white, or blue, sort them in-place so that objects of the same color
 * are adjacent, with the colors in the order red, white, and blue.
 *
 * Here, we will use the integers 0, 1, and 2 to represent the color red, white, and blue respectively.
 *
 * Follow up:
 *
 * Could you solve this problem without using the library's sort function?
 * Could you come up with a one-pass algorithm using only O(1) constant space?
 *
 * Example 1:
 *
 * Input: nums = [2,0,2,1,1,0]
 * Output: [0,0,1,1,2,2]
 * Example 2:
 *
 * Input: nums = [2,0,1]
 * Output: [0,1,2]
 * Example 3:
 *
 * Input: nums = [0]
 * Output: [0]
 * Example 4:
 *
 * Input: nums = [1]
 * Output: [1]
 *
 * Constraints:
 *
 * n == nums.length
 * 1 <= n <= 300
 * nums[i] is 0, 1, or 2.
 */
/**
 * A really intresting solutioin form discussion.
 */
class Solution {
public:
    void sortColors(vector<int>& nums) {
        int n0 = -1;
        int n1 = -1;
        int n2 = -1;

        for (int i = 0; i < nums.size(); ++i) {
            auto color = nums[i];
            nums[++n2] = 2;
            if (color < 2) {
                nums[++n1] = 1;
            }
            if (color < 1) {
                nums[++n0] = 0;
            }
        }
    }
};

/**
 * One pass.
 */
class Solution {
    public:
    void sortColors(vector<int>& nums)
    {
        int left = 0;
        int mid = 0;
        int right = nums.size() - 1;

        while (mid <= right) {
            switch (nums[mid]) {
            case 0:
                swap(nums[left], nums[mid]);
                ++left; // deliberately fallthrough
            case 1:
                ++mid;
                break;

            case 2:
                swap(nums[right], nums[mid]);
                --right;
                break;
            defaut:
                break;
            }
        }
    }
};

/**
 * Without sort. Two pass, using counting.
 */
class Solution {
public:
    void sortColors(vector<int>& nums) {
        int color_count[3] = { 0, };

        for (auto n : nums) {
            ++color_count[n];
        }

        int k = 0;
        auto it = nums.begin();
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < color_count[i]; ++j) {
                nums[k] = i;
                ++k;
            }
        }
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    void sortColors(vector<int>& nums) {
        sort(nums.begin(), nums.end());
    }
};
