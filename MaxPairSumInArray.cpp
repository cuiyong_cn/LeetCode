/**
 * Max Pair Sum in an Array
 *
 * You are given a 0-indexed integer array nums. You have to find the maximum
 * sum of a pair of numbers from nums such that the maximum digit in both
 * numbers are equal.
 *
 * Return the maximum sum or -1 if no such pair exists.
 *
 * Example 1:
 *
 * Input: nums = [51,71,17,24,42]
 * Output: 88
 * Explanation:
 * For i = 1 and j = 2, nums[i] and nums[j] have equal maximum digits with a pair sum of 71 + 17 = 88.
 * For i = 3 and j = 4, nums[i] and nums[j] have equal maximum digits with a pair sum of 24 + 42 = 66.
 * It can be shown that there are no other pairs with equal maximum digits, so the answer is 88.
 *
 * Example 2:
 *
 * Input: nums = [1,2,3,4]
 * Output: -1
 * Explanation: No pair exists in nums with equal maximum digits.
 *
 * Constraints:
 *     2 <= nums.length <= 100
 *     1 <= nums[i] <= 104
 */
/**
 * Original solution.
 */
int max_digit(int n)
{
    auto ans = 0;

    while (n > 9) {
        ans = max(ans, n % 10);
        n /= 10;
    }

    return max(ans, n);
}

class Solution {
public:
    int maxSum(vector<int>& nums) {
        auto group_by_max_digit = vector<vector<int>>(10);

        for (auto n : nums) {
            group_by_max_digit[max_digit(n)].push_back(n);
        }

        auto ans = -1;

        for (auto& g : group_by_max_digit) {
            if (g.size() > 1) {
                sort(g.begin(), g.end());
                int const n = g.size();
                ans = max(ans, g[n - 1] + g[n - 2]);
            }
        }

        return ans;
    }
};
