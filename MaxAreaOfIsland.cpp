/**
 * Max Area of Island
 *
 * Given a non-empty 2D array grid of 0's and 1's, an island is a group of 1's
 * (representing land) connected 4-directionally (horizontal or vertical.) You may assume
 * all four edges of the grid are surrounded by water.
 *
 * Find the maximum area of an island in the given 2D array. (If there is no island,
 * the maximum area is 0.)
 *
 * Example 1:
 *
 * [[0,0,1,0,0,0,0,1,0,0,0,0,0],
 *  [0,0,0,0,0,0,0,1,1,1,0,0,0],
 *  [0,1,1,0,1,0,0,0,0,0,0,0,0],
 *  [0,1,0,0,1,1,0,0,1,0,1,0,0],
 *  [0,1,0,0,1,1,0,0,1,1,1,0,0],
 *  [0,0,0,0,0,0,0,0,0,0,1,0,0],
 *  [0,0,0,0,0,0,0,1,1,1,0,0,0],
 *  [0,0,0,0,0,0,0,1,1,0,0,0,0]]
 *
 * Given the above grid, return 6. Note the answer is not 11, because the island must be
 * connected 4-directionally.
 *
 * Example 2:
 *
 * [[0,0,0,0,0,0,0,0]]
 *
 * Given the above grid, return 0.
 *
 * Note: The length of each dimension in the given grid does not exceed 50.
 */
/**
 * DFS method. We can also use the iterative method. Use a stack to store the land we
 * haven't seen. Then count the land connected.
 */
class Solution {
private:
    int walkAround(vector<vector<int>>& grid, int x, int y) {
        if (x < 0 || x >= m_rows || y < 0 || y >= m_cols) return 0;
        if (0 == grid[x][y] || 2 == grid[x][y]) return 0;
        int area = 1;
        grid[x][y] = 2;
        for (int i = 0; i < 4; ++i) {
            int dx = x + m_directions[i];
            int dy = y + m_directions[i + 1];
            area += walkAround(grid, dx, dy);
        }
        return area;
    }
public:
    int maxAreaOfIsland(vector<vector<int>>& grid) {
        m_rows = grid.size();
        m_cols = grid[0].size();
        int maxArea = 0;
        for (int i = 0; i < m_rows; ++i) {
            for (int j = 0; j < m_cols; ++j) {
                if (1 == grid[i][j]) {
                    maxArea = max(maxArea, walkAround(grid, i, j));
                }
            }
        }
        return maxArea;
    }
private:
    int m_rows;
    int m_cols;
    int m_directions[5] = {0, -1, 0, 1, 0};
};
