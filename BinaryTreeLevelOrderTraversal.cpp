/** Binary Tree Level Order Traversal
 *
 * Given a binary tree, return the level order traversal of its nodes' values. (ie, from left to right, level by level).
 *
 * For example: Given binary tree [3,9,20,null,null,15,7],
 *
 *     3 / \ 9  20 /  \ 15   7
 *
 * return its level order traversal as:
 *
 * [ [3], [9,20], [15,7] ]
 */
/**
 * Original solution.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> levelOrder(TreeNode* root) {
        vector<vector<int>> ans;
        if (nullptr == root) return ans;

        deque<TreeNode*> q;
        q.push_back(root);
        int lvl = 0;
        while (false == q.empty()) {
            ans.push_back({});
            int N = q.size();
            for (int i = 0; i < N; ++i) {
                TreeNode* node = q.front(); q.pop_front();
                ans[lvl].push_back(node->val);
                if (node->left) q.push_back(node->left);
                if (node->right) q.push_back(node->right);
            }
            ++lvl;
        }

        return ans;
    }
};

/**
 * Recursive solution.
 */
class Solution
{
public:
    vector<vector<int>> ret;

    void buildVector(TreeNode *root, int depth)
    {
        if(root == NULL) return;
        if(ret.size() == depth)
            ret.push_back(vector<int>());

        ret[depth].push_back(root->val);
        buildVector(root->left, depth + 1);
        buildVector(root->right, depth + 1);
    }

    vector<vector<int> > levelOrder(TreeNode *root) {
        buildVector(root, 0);
        return ret;
    }
};
