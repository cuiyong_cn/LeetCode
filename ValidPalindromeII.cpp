/**
 * Valid Palindrome II
 *
 * Given a string s, return true if the s can be palindrome after deleting at most one character from it.
 *
 * Example 1:
 *
 * Input: s = "aba"
 * Output: true
 * Example 2:
 *
 * Input: s = "abca"
 * Output: true
 * Explanation: You could delete the character 'c'.
 * Example 3:
 *
 * Input: s = "abc"
 * Output: false
 *
 * Constraints:
 *
 * 1 <= s.length <= 105
 * s consists of lowercase English letters.
 */
/**
 * Original solution.
 */
class Solution {
public:
    bool validPalindrome(string s) {
        for (int i = 0, j = s.length() - 1; i <= j; ++i, --j) {
            if (s[i] != s[j]) {
                return is_palindrome(s, i + 1, j) || is_palindrome(s, i, j - 1);
            }
        }

        return true;
    }

private:
    bool is_palindrome(string const& s, int start, int end)
    {
        for (int i = start, j = end; i <= j; ++i, --j) {
            if (s[i] != s[j]) {
                return false;
            }
        }

        return true;
    }
};
