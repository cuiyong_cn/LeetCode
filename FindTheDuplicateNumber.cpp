/**
 * Find the Duplicate Number
 *
 * Given an array nums containing n + 1 integers where each integer is between 1 and n (inclusive), prove that at least one duplicate number must exist. Assume that there is only one duplicate number, find the duplicate one.
 * 
 * Example 1:
 * 
 * Input: [1,3,4,2,2]
 * Output: 2
 * 
 * Example 2:
 * 
 * Input: [3,1,3,4,2]
 * Output: 3
 * 
 * Note:
 * 
 *     You must not modify the array (assume the array is read only).
 *     You must use only constant, O(1) extra space.
 *     Your runtime complexity should be less than O(n2).
 *     There is only one duplicate number in the array, but it could be repeated more than once.
 */
/**
 * Using cycle detection.
 */
class Solution {
public:
    int findDuplicate(vector<int>& nums) {
        int slow = nums[0];
        int fast = nums[0];
        do {
            slow = nums[slow];
            fast = nums[nums[fast]];
        } while (slow != fast);

        fast = nums[0];
        while (slow != fast) {
            slow = nums[slow];
            fast = nums[fast];
        }

        return slow;
    }
};

/**
 * There is another intresting solution using binary search.
 */
class Solution
{
public:
    int findDuplicate(vector<int>& nums) {
        int low = 1;
        int high = nums.size() - 1;

        while (low < high) {
            mid = (low + high) / 2;
            int count = 0;
            for (auto n : nums) {
                if (n <= mid) ++count;
            }

            if (count <= mid) low = mid + 1;
            else high = mid;
        }

        return low;
    }
};

/**
 * Original map solution. Also we can first sort the array, and then find the duplicate.
 * But these solution violates the constraints. (modify the array or O(1) space)
 */
class Solution {
public:
    int findDuplicate(vector<int>& nums) {
        map<int, int> numsCount;
        for (auto& n :nums) ++numsCount[n];

        auto it = find_if(numsCount.begin(), numsCount.end(),
                           [](const pair<int,int>& p) { return p.second > 1; });
        return it->first;
    }
};
