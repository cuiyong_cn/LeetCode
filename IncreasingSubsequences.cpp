/**
 * Increasing Subsequences
 *
 * Given an integer array, your task is to find all the different possible increasing subsequences of the given array,
 * and the length of an increasing subsequence should be at least 2.
 *
 * Example:
 *
 * Input: [4, 6, 7, 7]
 * Output: [[4, 6], [4, 7], [4, 6, 7], [4, 6, 7, 7], [6, 7], [6, 7, 7], [7,7], [4,7,7]]
 *
 * Constraints:
 *
 * The length of the given array will not exceed 15.
 * The range of integer in the given array is [-100,100].
 * The given array may contain duplicates, and two equal integers should also be considered as a special case of
 * increasing sequence.
 */
/**
 * Improve the below solution.
 */
class Solution {
public:
    vector<vector<int>> findSubsequences(vector<int>& nums) {
        vector<int> sub;
        dfs(nums, sub, 0);
        return ans;
    }

    void dfs(const vector<int>& nums, vector<int>& sub, int start) {
        if (sub.size() > 1) {
            ans.push_back(sub);
        }

        unordered_set<int> used;
        for (int i = start; i < nums.size(); ++i) {
            if ((sub.empty() || nums[i] >= sub.back()) && !used.count(nums[i])) {
                sub.push_back(nums[i]);
                dfs(nums, sub, i + 1);
                sub.pop_back();
                used.insert(nums[i]);
            }
        }
    }

private:
    vector<vector<int>> ans;
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<vector<int>> findSubsequences(vector<int>& nums) {
        if (nums.empty()) {
            return {};
        }

        vector<int> sub;
        dfs(nums, sub, "", 0);
        return ans;
    }

private:
    void dfs(const vector<int>& nums, vector<int>& sub, string sig, int start)
    {
        if (sub.size() > 1) {
            if (!seen.count(sig)) {
                seen.insert(sig);
                ans.push_back(sub);
            }
        }

        for (int i = start; i < nums.size(); ++i) {
            if (sub.empty()) {
                sub.push_back(nums[i]);
                dfs(nums, sub, sig + "#" + to_string(nums[i]), i + 1);
                sub.pop_back();
            }
            else {
                if (nums[i] >= sub.back()) {
                    sub.push_back(nums[i]);
                    dfs(nums, sub, sig + "#" + to_string(nums[i]), i + 1);
                    sub.pop_back();
                }
            }
        }
    }

    vector<vector<int>> ans;
    unordered_set<string> seen;
};
