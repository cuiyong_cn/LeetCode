/**
 * Smallest String With Swaps
 *
 * You are given a string s, and an array of pairs of indices in the string pairs where pairs[i] = [a, b] indicates 2
 * indices(0-indexed) of the string.
 *
 * You can swap the characters at any pair of indices in the given pairs any number of times.
 *
 * Return the lexicographically smallest string that s can be changed to after using the swaps.
 *
 * Example 1:
 *
 * Input: s = "dcab", pairs = [[0,3],[1,2]]
 * Output: "bacd"
 * Explaination:
 * Swap s[0] and s[3], s = "bcad"
 * Swap s[1] and s[2], s = "bacd"
 * Example 2:
 *
 * Input: s = "dcab", pairs = [[0,3],[1,2],[0,2]]
 * Output: "abcd"
 * Explaination:
 * Swap s[0] and s[3], s = "bcad"
 * Swap s[0] and s[2], s = "acbd"
 * Swap s[1] and s[2], s = "abcd"
 * Example 3:
 *
 * Input: s = "cba", pairs = [[0,1],[1,2]]
 * Output: "abc"
 * Explaination:
 * Swap s[0] and s[1], s = "bca"
 * Swap s[1] and s[2], s = "bac"
 * Swap s[0] and s[1], s = "abc"
 *
 * Constraints:
 *
 * 1 <= s.length <= 10^5
 * 0 <= pairs.length <= 10^5
 * 0 <= pairs[i][0], pairs[i][1] < s.length
 * s only contains lower case English letters.
 */
/**
 * Union find solution. Perfoms better the original.
 */
class Solution {
public:
    int find(vector<int>& ds, int i) {
        return ds[i] < 0 ? i : ds[i] = find(ds, ds[i]);
    }

    string smallestStringWithSwaps(string s, vector<vector<int>>& pairs) {
        vector<int> ds(s.size(), -1);
        vector<vector<int>> m(s.size());

        for (auto& p : pairs) {
            auto i = find(ds, p[0]);
            auto j = find(ds, p[1]);
            if (i != j) {
                ds[j] = i;
            }
        }

        for (auto i = 0; i < s.size(); ++i) {
            m[find(ds, i)].push_back(i);
        }

        for (auto &ids : m) {
            string ss = "";
            for (auto id : ids) {
                ss += s[id];
            }

            sort(begin(ss), end(ss));
            for (auto i = 0; i < ids.size(); ++i) {
                s[ids[i]] = ss[i];
            }
        }

        return s;
    }
};

/**
 * Original solution.
 * pick a path, sort the characters along the path.
 */
class Solution {
public:
    string smallestStringWithSwaps(string s, vector<vector<int>>& pairs) {
        unordered_map<int, vector<int>> G;

        for (const auto& pair : pairs) {
            G[pair[0]].push_back(pair[1]);
            G[pair[1]].push_back(pair[0]);
        }

        auto clone = s;
        vector<bool> visited(s.size(), false);

        for (int i = 0; i < s.size(); ++i) {
            if (visited[i]) {
                continue;
            }

            vector<int> path;
            deque<int> q{i};
            while (!q.empty()) {
                auto v = q.front();
                q.pop_front();

                if (!visited[v]) {
                    path.push_back(v);
                    visited[v] = true;
                    for (auto p : G[v]) {
                        if (!visited[p]) {
                            q.push_back(p);
                        }
                    }
                }
            }

            sort(std::begin(path), std::end(path));
            auto sub = path;

            sort(std::begin(path), std::end(path), [&s](auto a, auto b) {
                return s[a] < s[b];
            });

            for (int i = 0; i < sub.size(); ++i) {
                clone[sub[i]] = s[path[i]];
            }
        }

        return clone;
    }
};
