/**
 * Longest Substring with At Least K Repeating Characters
 *
 * Given a string s and an integer k, return the length of the longest substring of s such that the
 * frequency of each character in this substring is greater than or equal to k.
 *
 * Example 1:
 *
 * Input: s = "aaabb", k = 3
 * Output: 3
 * Explanation: The longest substring is "aaa", as 'a' is repeated 3 times.
 * Example 2:
 *
 * Input: s = "ababbc", k = 2
 * Output: 5
 * Explanation: The longest substring is "ababb", as 'a' is repeated 2 times and 'b' is repeated 3
 * times.
 *
 * Constraints:
 *
 * 1 <= s.length <= 104
 * s consists of only lowercase English letters.
 * 1 <= k <= 105
 */
/**
 * From leetcode. Sliding window.
 */
class Solution {
public:
    int longestSubstring(string s, int k) {
        int countMap[26];
        int maxUnique = getMaxUniqueLetters(s);
        int result = 0;
        for (int currUnique = 1; currUnique <= maxUnique; currUnique++) {
            // reset countMap
            memset(countMap, 0, sizeof(countMap));
            int windowStart = 0, windowEnd = 0, idx = 0, unique = 0, countAtLeastK = 0;
            while (windowEnd < s.size()) {
                // expand the sliding window
                if (unique <= currUnique) {
                    idx = s[windowEnd] - 'a';
                    if (countMap[idx] == 0) unique++;
                    countMap[idx]++;
                    if (countMap[idx] == k) countAtLeastK++;
                    windowEnd++;
                }
                // shrink the sliding window
                else {
                    idx = s[windowStart] - 'a';
                    if (countMap[idx] == k) countAtLeastK--;
                    countMap[idx]--;
                    if (countMap[idx] == 0) unique--;
                    windowStart++;
                }
                if (unique == currUnique && unique == countAtLeastK)
                    result = max(windowEnd - windowStart, result);
            }
        }

        return result;
    }

    // get the maximum number of unique letters in the string s
    int getMaxUniqueLetters(string s) {
        bool map[26] = {0};
        int maxUnique = 0;
        for (int i = 0; i < s.length(); i++) {
            if (!map[s[i] - 'a']) {
                maxUnique++;
                map[s[i] - 'a'] = true;
            }
        }
        return maxUnique;
    }
};

/**
 * Original solution. Divide and conquer.
 */
class Solution {
public:
    int longestSubstring(string s, int k) {
        vector<vector<int>> accumulate_char_count(26, vector<int>(s.length() + 1, 0));

        for (int i = 0; i < s.length(); ++i) {
            ++accumulate_char_count[s[i] - 'a'][i + 1];
        }

        for (auto& cc : accumulate_char_count) {
            partial_sum(begin(cc) + 1, end(cc), begin(cc) + 1);
        }

        int ans = 0;
        deque<pair<int, int>> q{{0, s.length()}};

        while (!q.empty()) {
            deque<pair<int, int>> nq;
            for (auto const& r : q) {
                int start = r.first, end = r.second;
                if ((end - start) <= ans) {
                    continue;
                }

                vector<int> count(26, 0);
                for (int i = 0; i < 26; ++i) {
                    count[i] = accumulate_char_count[i][end] - accumulate_char_count[i][start];
                }

                bool split = false;
                for (int i = start; i < end; ++i) {
                    if (count[s[i] - 'a'] < k) {
                        split = true;
                        nq.push_back({start, i});
                        nq.push_back({i + 1, end});
                        break;
                    }
                }

                if (!split) {
                    ans = max(ans, end - start);
                }
            }

            swap(q, nq);
        }

        return ans;
    }
};
