/**
 * Longest Subarray of 1's After Deleting One Element
 *
 * Given a binary array nums, you should delete one element from it.
 *
 * Return the size of the longest non-empty subarray containing only 1's in the resulting array.
 * Return 0 if there is no such subarray.
 *
 * Example 1:
 *
 * Input: nums = [1,1,0,1]
 * Output: 3
 * Explanation: After deleting the number in position 2, [1,1,1] contains 3 numbers with value of 1's.
 * Example 2:
 *
 * Input: nums = [0,1,1,1,0,1,1,0,1]
 * Output: 5
 * Explanation: After deleting the number in position 4, [0,1,1,1,1,1,0,1] longest subarray with value of 1's is [1,1,1,1,1].
 * Example 3:
 *
 * Input: nums = [1,1,1]
 * Output: 2
 * Explanation: You must delete one element.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 105
 * nums[i] is either 0 or 1.
 */
/**
 * Short below solution.
 */
class Solution {
public:
    int longestSubarray(vector<int>& A) {
        int i = 0, j, k = 1;
        for (j = 0; j < A.size(); ++j) {
            if (A[j] == 0) {
                k--;
            }

            if (k < 0 && A[i++] == 0) {
                k++;
            }
        }

        return j - i - 1;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int longestSubarray(vector<int>& nums) {
        int const N = nums.size();
        int zero_cnt = 0;
        int one_cnt = 0;

        for (int i = 0, j = 0; j < N; ++j) {
            if (0 == nums[j]) {
                ++zero_cnt;
            }
            else {
                ++one_cnt;
            }

            if (zero_cnt > 1) {
                if (0 == nums[i]) {
                    --zero_cnt;
                }
                else {
                    --one_cnt;
                }
                ++i;
            }
        }

        return one_cnt + zero_cnt - 1;
    }
};
