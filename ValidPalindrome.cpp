/**
 * Valid Palindrome
 *
 * Given a string s, determine if it is a palindrome, considering only alphanumeric characters and
 * ignoring cases.
 *
 * Example 1:
 *
 * Input: s = "A man, a plan, a canal: Panama"
 * Output: true
 * Explanation: "amanaplanacanalpanama" is a palindrome.
 * Example 2:
 *
 * Input: s = "race a car"
 * Output: false
 * Explanation: "raceacar" is not a palindrome.
 *
 * Constraints:
 *
 * 1 <= s.length <= 2 * 105
 * s consists only of printable ASCII characters.
 */
/**
 * Original solution.
 */
class Solution {
public:
    bool isPalindrome(string s) {
        s.erase(remove_if(begin(s), end(s),
                                   [](auto ch) {
                                       return false == isalnum(ch);
                                   }), end(s));

        transform(begin(s), end(s), begin(s), ::tolower);

        int front = 0, back = s.length() - 1;
        while (front < back) {
            if (s[front] != s[back]) {
                return false;
            }
            ++front;
            --back;
        }

        return true;
    }
};
