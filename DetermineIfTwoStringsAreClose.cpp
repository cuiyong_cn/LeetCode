/**
 * Determine if Two Strings Are Close
 *
 * Two strings are considered close if you can attain one from the other using the following operations:
 *
 * Operation 1: Swap any two existing characters.
 * For example, abcde -> aecdb
 * Operation 2: Transform every occurrence of one existing character into another existing character, and do the same with the other character.
 * For example, aacabb -> bbcbaa (all a's turn into b's, and all b's turn into a's)
 * You can use the operations on either string as many times as necessary.
 *
 * Given two strings, word1 and word2, return true if word1 and word2 are close, and false otherwise.
 *
 * Example 1:
 *
 * Input: word1 = "abc", word2 = "bca"
 * Output: true
 * Explanation: You can attain word2 from word1 in 2 operations.
 * Apply Operation 1: "abc" -> "acb"
 * Apply Operation 1: "acb" -> "bca"
 * Example 2:
 *
 * Input: word1 = "a", word2 = "aa"
 * Output: false
 * Explanation: It is impossible to attain word2 from word1, or vice versa, in any number of operations.
 * Example 3:
 *
 * Input: word1 = "cabbba", word2 = "abbccc"
 * Output: true
 * Explanation: You can attain word2 from word1 in 3 operations.
 * Apply Operation 1: "cabbba" -> "caabbb"
 * Apply Operation 2: "caabbb" -> "baaccc"
 * Apply Operation 2: "baaccc" -> "abbccc"
 *
 * Constraints:
 *
 * 1 <= word1.length, word2.length <= 105
 * word1 and word2 contain only lowercase English letters.
 */
/**
 * Original solution.
 */
struct Properties
{
    set<char> char_pattern;
    vector<int> freq_pattern;
};

bool operator==(Properties const& a, Properties const& b)
{
    return a.char_pattern == b.char_pattern && a.freq_pattern == b.freq_pattern;
}

class Solution {
public:
    bool closeStrings(string const& word1, string const& word2) {
        int const w1_len = word1.length();
        int const w2_len = word2.length();

        if (w1_len != w2_len) {
            return false;
        }

        return properties(word1) == properties(word2);
    }

private:
    Properties properties(string const& w)
    {
        array<int, 26> char_cnt = { 0, };
        for (auto c : w) {
            ++char_cnt[c - 'a'];
        }

        set<char> s;
        vector<int> freq;
        for (int i = 0; i < char_cnt.size(); ++i) {
            if (char_cnt[i] > 0) {
                s.insert('a' + i);
                freq.emplace_back(char_cnt[i]);
            }
        }

        sort(freq.begin(), freq.end());

        return {s, freq};
    }
};
