/**
 * Check If a Word Occurs As a Prefix of Any Word in a Sentence
 *
 * Given a sentence that consists of some words separated by a single space, and a searchWord, check
 * if searchWord is a prefix of any word in sentence.
 *
 * Return the index of the word in sentence (1-indexed) where searchWord is a prefix of this word.
 * If searchWord is a prefix of more than one word, return the index of the first word (minimum
 * index). If there is no such word return -1.
 *
 * A prefix of a string s is any leading contiguous substring of s.
 *
 * Example 1:
 *
 * Input: sentence = "i love eating burger", searchWord = "burg"
 * Output: 4
 * Explanation: "burg" is prefix of "burger" which is the 4th word in the sentence.
 * Example 2:
 *
 * Input: sentence = "this problem is an easy problem", searchWord = "pro"
 * Output: 2
 * Explanation: "pro" is prefix of "problem" which is the 2nd and the 6th word in the sentence, but we return 2 as it's the minimal index.
 * Example 3:
 *
 * Input: sentence = "i am tired", searchWord = "you"
 * Output: -1
 * Explanation: "you" is not a prefix of any word in the sentence.
 *
 * Constraints:
 *
 * 1 <= sentence.length <= 100
 * 1 <= searchWord.length <= 10
 * sentence consists of lowercase English letters and spaces.
 * searchWord consists of lowercase English letters.
 */
/**
 * Short and clear one.
 */
class Solution {
public:
    int isPrefixOfWord(string sentence, string searchWord) {
        int ans = 1;
        stringstream ss{sentence};
        for (; !ss.eof();) {
            string word; ss >> word;
            if (auto p = word.find(searchWord); 0 == p) {
                return ans;
            }
            ++ans;
        }

        return -1;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int isPrefixOfWord(string sentence, string searchWord) {
        size_t p = 0;
        for (size_t i = p; i < sentence.length(); i = p + 1) {
            p = sentence.find(searchWord, i);
            if (string::npos == p || 0 == p || ' ' == sentence[p - 1]) {
                break;
            }
        }

        if (string::npos == p) {
            return -1;
        }

        return count(sentence.begin(), sentence.begin() + p, ' ') + 1;
    }
};

/**
 * Old fashion way. It's not using KMP, so it may not be efficient as orignal one.
 */

class Solution {
public:
    int isPrefixOfWord(string sentence, string searchWord) {
        int ans = 1;
        for (size_t i = 0; i < sentence.length(); ++i) {
            int j = 0;
            for (; j < searchWord.length() && i < sentence.length(); ++i, ++j) {
                if (' ' == sentence[i] || sentence[i] != searchWord[j]) {
                    break;
                }
            }

            if (j == searchWord.length()) {
                return ans;
            }

            for (; i < sentence.length(); ++i) {
                if (' ' == sentence[i]) {
                    break;
                }
            }
            ++ans;
        }

        return -1;
    }
};
