/**
 * 01 Matrix
 *
 * Given a matrix consists of 0 and 1, find the distance of the nearest 0 for each cell.
 *
 * The distance between two adjacent cells is 1.
 *
 * Example 1:
 *
 * Input:
 * [[0,0,0],
 *  [0,1,0],
 *  [0,0,0]]
 *
 * Output:
 * [[0,0,0],
 *  [0,1,0],
 *  [0,0,0]]
 * Example 2:
 *
 * Input:
 * [[0,0,0],
 *  [0,1,0],
 *  [1,1,1]]
 *
 * Output:
 * [[0,0,0],
 *  [0,1,0],
 *  [1,2,1]]
 *
 * Note:
 *
 * The number of elements of the given matrix will not exceed 10,000.
 * There are at least one 0 in the given matrix.
 * The cells are adjacent in only four directions: up, down, left and right.
 */
/**
 * DP solution.
 */
class Solution {
public:
    vector<vector<int> > updateMatrix(vector<vector<int> >& matrix)
    {
        int rows = matrix.size();
        if (rows == 0)
            return matrix;
        int cols = matrix[0].size();
        vector<vector<int> > dist(rows, vector<int>(cols, INT_MAX - 100000));

        //First pass: check for left and top
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (matrix[i][j] == 0)
                    dist[i][j] = 0;
                else {
                    if (i > 0)
                        dist[i][j] = min(dist[i][j], dist[i - 1][j] + 1);
                    if (j > 0)
                        dist[i][j] = min(dist[i][j], dist[i][j - 1] + 1);
                }
            }
        }

        //Second pass: check for bottom and right
        for (int i = rows - 1; i >= 0; i--) {
            for (int j = cols - 1; j >= 0; j--) {
                if (i < rows - 1)
                    dist[i][j] = min(dist[i][j], dist[i + 1][j] + 1);
                if (j < cols - 1)
                    dist[i][j] = min(dist[i][j], dist[i][j + 1] + 1);
            }
        }

        return dist;
    }
}

/**
 * Original solution. BFS solution.
 */
class Solution {
public:
    vector<vector<int>> updateMatrix(vector<vector<int>>& matrix) {
        vector<pair<int, int>> q;
        int M = matrix.size();
        int N = M > 0 ? matrix[0].size() : 0;

        for (int i = 0; i < M; ++i) {
            for (int j = 0; j < N; ++j) {
                if (0 == matrix[i][j]) {
                    q.emplace_back(make_pair(i, j));
                }
                else {
                    matrix[i][j] = 10001;
                }
            }
        }

        int distance = 0;
        while (!q.empty()) {
            ++distance;
            vector<pair<int, int>> nq;
            for (auto&& cell : q) {
                int x = cell.first, y = cell.second;
                int left = y - 1, right = y + 1;
                int up = x - 1, down = x + 1;
                if (left >= 0) {
                    if (matrix[x][left] > distance) {
                        matrix[x][left] = distance;
                        nq.emplace_back(make_pair(x, left));
                    }
                }
                if (right < N) {
                    if (matrix[x][right] > distance) {
                        matrix[x][right] = distance;
                        nq.emplace_back(make_pair(x, right));
                    }
                }
                if (up >= 0) {
                    if (matrix[up][y] > distance) {
                        matrix[up][y] = distance;
                        nq.emplace_back(make_pair(up, y));
                    }
                }
                if (down < M) {
                    if (matrix[down][y] > distance) {
                        matrix[down][y] = distance;
                        nq.emplace_back(make_pair(down, y));
                    }
                }
            }

            swap(q, nq);
        }

        return matrix;
    }
};
