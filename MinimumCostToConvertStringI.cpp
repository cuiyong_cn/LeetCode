/**
 * Minimum Cost to Convert String I
 *
 * You are given two 0-indexed strings source and target, both of length n and consisting of
 * lowercase English letters. You are also given two 0-indexed character arrays original and
 * changed, and an integer array cost, where cost[i] represents the cost of changing the character
 * original[i] to the character changed[i].
 *
 * You start with the string source. In one operation, you can pick a character x from the string
 * and change it to the character y at a cost of z if there exists any index j such that cost[j] ==
 * z, original[j] == x, and changed[j] == y.
 *
 * Return the minimum cost to convert the string source to the string target using any number of
 * operations. If it is impossible to convert source to target, return -1.
 *
 * Note that there may exist indices i, j such that original[j] == original[i] and changed[j] ==
 * changed[i].
 *
 * Example 1:
 *
 * Input: source = "abcd", target = "acbe", original = ["a","b","c","c","e","d"], changed = ["b","c","b","e","b","e"], cost = [2,5,5,1,2,20]
 * Output: 28
 * Explanation: To convert the string "abcd" to string "acbe":
 * - Change value at index 1 from 'b' to 'c' at a cost of 5.
 * - Change value at index 2 from 'c' to 'e' at a cost of 1.
 * - Change value at index 2 from 'e' to 'b' at a cost of 2.
 * - Change value at index 3 from 'd' to 'e' at a cost of 20.
 * The total cost incurred is 5 + 1 + 2 + 20 = 28.
 * It can be shown that this is the minimum possible cost.
 *
 * Example 2:
 *
 * Input: source = "aaaa", target = "bbbb", original = ["a","c"], changed = ["c","b"], cost = [1,2]
 * Output: 12
 * Explanation: To change the character 'a' to 'b' change the character 'a' to 'c' at a cost of 1, followed by changing the character 'c' to 'b' at a cost of 2, for a total cost of 1 + 2 = 3. To change all occurrences of 'a' to 'b', a total cost of 3 * 4 = 12 is incurred.
 *
 * Example 3:
 *
 * Input: source = "abcd", target = "abce", original = ["a"], changed = ["e"], cost = [10000]
 * Output: -1
 * Explanation: It is impossible to convert source to target because the value at index 3 cannot be changed from 'd' to 'e'.
 *
 * Constraints:
 *     1 <= source.length == target.length <= 105
 *     source, target consist of lowercase English letters.
 *     1 <= cost.length == original.length == changed.length <= 2000
 *     original[i], changed[i] are lowercase English letters.
 *     1 <= cost[i] <= 106
 *     original[i] != changed[i]
 */
/**
 * Use Floyd-Warshall algorithm.
 */
template<typename T>
using Matrix2D = vector<vector<T>>;

static constexpr int int_max = numeric_limits<int>::max();

class Solution {
public:
    long long minimumCost(string source, string target, vector<char>& original, vector<char>& changed, vector<int>& c) {
        auto cost = Matrix2D<long long>(26, vector<long long>(26, int_max));

        int const n = original.size();

        for (auto i = 0; i < n; ++i) {
            auto from = original[i] - 'a';
            auto to = changed[i] - 'a';
            cost[from][to] = min<long long>(cost[from][to], c[i]);
        }

        for (auto b = 0; b < 26; ++b) {
            for (auto f = 0; f < 26; ++f) {
                for (auto t = 0; t < 26; ++t) {
                    cost[f][t] = f == t ? 0 : min(cost[f][t], cost[f][b] + cost[b][t]);
                }
            }
        }

        int const sn = source.size();
        auto ans = 0LL;

        for (auto i = 0; i < sn; ++i) {
            auto from = source[i] - 'a';
            auto to = target[i] - 'a';
            auto c = cost[from][to];

            if (c >= int_max) {
                return -1;
            }

            ans += c;
        }

        return ans;
    }
};

/**
 * Original solution. This should work but TLE.
 */
template<typename T>
using Matrix2D = vector<vector<T>>;

static constexpr int int_max = numeric_limits<int>::max();

auto transform_cost(Matrix2D<int> const& graph, Matrix2D<int> const& cache, int from, int to, vector<bool>& visited)
{
    if (from == to) {
        return 0;
    }

    if (-1 != cache[from][to]) {
        return cache[from][to];
    }

    if (visited[from]) {
        return int_max;
    }

    auto cost = int_max;

    visited[from] = true;

    for (auto t = 0; t < 26; ++t) {
        if (int_max == graph[from][t]) {
            continue;
        }

        auto c = transform_cost(graph, cache, t, to, visited);

        if (int_max != c) {
            cost = min(cost, c + graph[from][t]);
        }
    }

    visited[from] = false;

    return cost;
}

class Solution {
public:
    long long minimumCost(string source, string target, vector<char>& original, vector<char>& changed, vector<int>& cost) {
        auto graph = Matrix2D<int>(26, vector<int>(26, int_max));

        int const n = original.size();

        for (auto i = 0; i < n; ++i) {
            auto from = original[i] - 'a';
            auto to = changed[i] - 'a';
            graph[from][to] = min(graph[from][to], cost[i]);
        }

        auto visited = vector<bool>(26, false);
        auto to_cost = Matrix2D<int>(26, vector<int>(26, -1));

        for (auto f = 0; f < 26; ++f) {
            for (auto t = 0; t < 26; ++t) {
                to_cost[f][t] = transform_cost(graph, to_cost, f, t, visited);
            }
        }

        int const sn = source.size();
        auto ans = 0LL;

        for (auto i = 0; i < sn; ++i) {
            auto c = to_cost[source[i] - 'a'][target[i] - 'a'];

            if (int_max == c) {
                return -1;
            }

            ans += c;
        }

        return ans;
    }
};
