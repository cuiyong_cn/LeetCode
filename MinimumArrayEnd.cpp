/**
 * Minimum Array End
 *
 * You are given two integers n and x. You have to construct an array of positive integers nums of
 * size n where for every 0 <= i < n - 1, nums[i + 1] is greater than nums[i], and the result of the
 * bitwise AND operation between all elements of nums is x.
 *
 * Return the minimum possible value of nums[n - 1].
 *
 * Example 1:
 *
 * Input: n = 3, x = 4
 *
 * Output: 6
 *
 * Explanation:
 *
 * nums can be [4,5,6] and its last element is 6.
 *
 * Example 2:
 *
 * Input: n = 2, x = 7
 *
 * Output: 15
 *
 * Explanation:
 *
 * nums can be [7,15] and its last element is 15.
 *
 * Constraints:
 *     1 <= n, x <= 108
 */
/**
 * Original solution.
 */
class Solution {
public:
    long long minEnd(int n, int x) {
        auto constexpr bits_long_long = sizeof(long long) * 8;
        auto bits_x = bitset<bits_long_long>(x);
        auto bits_n = bitset<bits_long_long>(n - 1);

        for (auto i = 0, j = 0; i < bits_long_long; ++i) {
            if (!bits_x[i]) {
                bits_x[i] = bits_n[j];
                ++j;
            }
        }

        return bits_x.to_ullong();
    }
};
