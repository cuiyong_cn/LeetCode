/**
 * Super Ugly Number
 *
 * Write a program to find the nth super ugly number.
 *
 * Super ugly numbers are positive numbers whose all prime factors are in the given prime list primes of size k.
 *
 * Example:
 *
 * Input: n = 12, primes = [2,7,13,19]
 * Output: 32
 * Explanation: [1,2,4,7,8,13,14,16,19,26,28,32] is the sequence of the first 12
 *              super ugly numbers given primes = [2,7,13,19] of size 4.
 * Note:
 *
 * 1 is a super ugly number for any given primes.
 * The given numbers in primes are in ascending order.
 * 0 < k ≤ 100, 0 < n ≤ 106, 0 < primes[i] < 1000.
 * The nth super ugly number is guaranteed to fit in a 32-bit signed integer.
 */
/**
 * Passed solution. From the discussion.
 *
 * It is actually like how we merge k sorted list:
 *
 * ugly number                       k sorted list
 *     1                            2     7    13   19     1 * [2,7,13,19]
 *     |                            |     |    |    |
 *     2                            4     14   26   38     2 * [2,7,13,19]
 *     |                            |     |    |    |
 *     4                            8     28   52   76     4 * [2,7,13,19]
 *     |                            |     |    |    |
 *     7                            14    49   91   133    7 * [2,7,13,19]
 *     |                            |     |    |    |
 *     8                            16    56   ...   ...   8 * [2,7,13,19]
 *     |                            |     |    |     |
 *     .                            .     .     .    .
 *     .                            .     .     .    .
 *     .                            .     .     .    .
 * We can see that each prime number in primes[] form a sorted list, and now our job is to merge them and find the nth
 * minimum.  Here we don't have the next pointer for each node to trace the next potential candidate. But as we can see
 * in the graph, we can make use of the ugly number we have produced so far!
 *
 * Here, each entry has three parts: {num, prime, index}, num represents the value of the node, prime means which sorted
 * list this node is in, and index tells us how far we have gone in that list, it works like the next pointer in
 * linkedlist, help us find the next node in that sorted list.
 */
class Solution {
public:
    int nthSuperUglyNumber(int n, vector<int>& primes) {
        priority_queue<vector<int>> queue;
        for (int i = 0; i < primes.size(); ++i) {
            queue.emplace(vector<int>{-primes[i], primes[i], 0});
        }

        vector<int> nums(n + 1, 0);
        nums[0] = 1;

        int i = 1;
        while (i < n) {
            auto& entry = queue.top();
            int num = -entry[0], prime = entry[1], index = entry[2];
			// remove duplicate
            if (num != nums[i - 1]){
                nums[i] = num;
                ++i;
            }

            queue.emplace(vector<int>{-prime * nums[index + 1], prime, index + 1});
            queue.pop();
        }

        return nums[n-1];
    }
};


/**
 * Original solution. TLE one. But I think it works.
 */
class Solution {
public:
    int nthSuperUglyNumber(int n, vector<int>& primes) {
        vector<bool> path(1'000'000'001, false);
        path[1] = true;

        for (auto p : primes) {
            for (int i = p; i < path.size(); i += p) {
                path[i] = true;
            }
        }

        int count = 0;
        for (int i = 1; i < path.size(); ++i) {
            if (count == n) {
                return i;
            }

            if (path[i]) {
                ++count;
            }
        }

        return 1;
    }
};
