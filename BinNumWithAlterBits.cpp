/**
 * Binary Number with Alternating Bits
 *
 * Given a positive integer, check whether it has alternating bits: namely, if two
 * adjacent bits will always have different values.
 *
 * Example 1:
 *
 * Input: 5
 * Output: True
 * Explanation:
 * The binary representation of 5 is: 101
 *
 * Example 2:
 *
 * Input: 7
 * Output: False
 * Explanation:
 * The binary representation of 7 is: 111.
 *
 * Example 3:
 *
 * Input: 11
 * Output: False
 * Explanation:
 * The binary representation of 11 is: 1011.
 *
 * Example 4:
 *
 * Input: 10
 * Output: True
 * Explanation:
 * The binary representation of 10 is: 1010.
 */
/**
 * More efficient one.
 *          101010  n                        110010  n
 *          010101  n >> 1                   011001  n >> 1
 *         0111111  n ^ (n >> 1)  --> c      101011  n ^ (n >> 1) --> c
 *         1000000  c + 1         --> m      101100  c + 1        --> m
 *         0000000  c & m                    101000  c & m
 */
class Solution {
public:
    bool hasAlternatingBits(int n) {
        long c = n ^ (n >> 1);
        long m = c + 1;
        
        return 0 == (c & m);
    }
};
/**
 * Straight and simple. Also we can convert the number to string, and check if adjacent
 * digits are same.
 */
class Solution {
public:
    bool hasAlternatingBits(int n) {
        int prev = n & 1;
        n = n >> 1;
        while (n > 0) {
            int cur = n & 1;
            if (0 == (prev ^ cur)) return false;
            n = n >> 1;
            prev = cur;
        }
        return true;
    }
};
