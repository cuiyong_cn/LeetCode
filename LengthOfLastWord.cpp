/**
 * Length of Last Word
 *
 * Given a string s consists of some words separated by spaces, return the length of the last word
 * in the string. If the last word does not exist, return 0.
 *
 * A word is a maximal substring consisting of non-space characters only.
 *
 * Example 1:
 *
 * Input: s = "Hello World"
 * Output: 5
 * Example 2:
 *
 * Input: s = " "
 * Output: 0
 *
 * Constraints:
 *
 * 1 <= s.length <= 104
 * s consists of only English letters and spaces ' '.
 */
/**
 * Original solution.
 */
class Solution {
public:
    int lengthOfLastWord(string s) {
        auto non_space_back = s.find_last_not_of(' ');
        if (string::npos == non_space_back) {
            return 0;
        }

        auto space_back = s.find_last_of(' ', non_space_back);

        return string::npos == space_back ? (non_space_back + 1) : (non_space_back - space_back);
    }
};
