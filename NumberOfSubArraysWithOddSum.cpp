/**
 * Number of Sub-arrays With Odd Sum
 *
 * Given an array of integers arr. Return the number of sub-arrays with odd sum.
 *
 * As the answer may grow large, the answer must be computed modulo 10^9 + 7.
 *
 * Example 1:
 *
 * Input: arr = [1,3,5]
 * Output: 4
 * Explanation: All sub-arrays are [[1],[1,3],[1,3,5],[3],[3,5],[5]]
 * All sub-arrays sum are [1,4,9,3,8,5].
 * Odd sums are [1,9,3,5] so the answer is 4.
 * Example 2:
 *
 * Input: arr = [2,4,6]
 * Output: 0
 * Explanation: All sub-arrays are [[2],[2,4],[2,4,6],[4],[4,6],[6]]
 * All sub-arrays sum are [2,6,12,4,10,6].
 * All sub-arrays have even sum and the answer is 0.
 * Example 3:
 *
 * Input: arr = [1,2,3,4,5,6,7]
 * Output: 16
 * Example 4:
 *
 * Input: arr = [100,100,99,99]
 * Output: 4
 * Example 5:
 *
 * Input: arr = [7]
 * Output: 1
 *
 * Constraints:
 *
 * 1 <= arr.length <= 10^5
 * 1 <= arr[i] <= 100
 */
/**
 * We observe followings:
 * 1. odd + even = odd
 * 2. even + even = even;
 * 3. odd + odd = even;
 */
class Solution {
public:
    int numOfSubarrays(vector<int>& arr) {
        long const mod = 1'000'000'007;
        int odd = 0, even = 0;
        long ans = 0;
        for (auto n : arr) {
            if (n & 1) {
                ans += even + 1;
                swap(odd, even);
                ++odd;
            }
            else {
                ans += odd;
                ++even;
            }
            ans %= mod;
        }

        return ans;
    }
};

/**
 * Original solution. TLE.
 */
class Solution {
public:
    int numOfSubarrays(vector<int>& arr) {
        vector<int> prefix_sum(arr.size() + 1, 0);

        partial_sum(begin(arr), end(arr), begin(prefix_sum) + 1);
        int ans = 0;
        int len = arr.size();
        for (int L = 1; L <= len; ++L) {
            int end_start = len + 1 - L;
            for (int i = 0; i < end_start; ++i) {
                auto sum = prefix_sum[i + L] - prefix_sum[i];
                if (sum & 1) {
                    ++ans;
                }
            }
        }

        return ans;
    }
};
