/**
 * Network Delay Time
 *
 * There are N network nodes, labelled 1 to N.
 *
 * Given times, a list of travel times as directed edges times[i] = (u, v, w), where u is the source node, v is the
 * target node, and w is the time it takes for a signal to travel from source to target.
 *
 * Now, we send a signal from a certain node K. How long will it take for all nodes to receive the signal? If it is
 * impossible, return -1.
 *
 * Example 1:
 *
 * Input: times = [[2,1,1],[2,3,1],[3,4,1]], N = 4, K = 2
 * Output: 2
 *
 * Note:
 *
 * N will be in the range [1, 100].
 * K will be in the range [1, N].
 * The length of times will be in the range [1, 6000].
 * All edges times[i] = (u, v, w) will have 1 <= u, v <= N and 0 <= w <= 100.
 */
/**
 * Original solution.
 */
class Solution {
public:
    int networkDelayTime(vector<vector<int>>& times, int N, int K) {
        deque<pair<int, int>> q;
        map<int, set<pair<int, int>>> graph;
        vector<int> delays(N + 1, 0);

        for (auto& edge : times) {
            graph[edge[0]].insert({edge[1], edge[2]});
        }

        int ans = 0;
        q.push_back({K, 0});
        while (!q.empty()) {
            int size = q.size();
            for (int i = 0; i < size; ++i) {
                auto [s, d] = q.front(); q.pop_front();
                for (auto node : graph[s]) {
                    int delay = d + node.second;
                    if (0 == delays[node.first] || delay < delays[node.first]) {
                        delays[node.first] = delay;
                        q.push_back({node.first, delay});
                    }
                }
            }
        }

        for (int i = 1; i <= N; ++i) {
            if (i != K) {
                if (0 == delays[i]) {
                    return -1;
                }
                if (delays[i] > ans) {
                    ans = delays[i];
                }
            }
        }

        return ans;
    }
};
