/**
 * Find Kth Bit in Nth Binary String
 *
 * Given two positive integers n and k, the binary string Sn is formed as follows:
 *
 * S1 = "0" Si = Si - 1 + "1" + reverse(invert(Si - 1)) for i > 1 Where + denotes the concatenation
 * operation, reverse(x) returns the reversed string x, and invert(x) inverts all the bits in x (0
 * changes to 1 and 1 changes to 0).
 *
 * For example, the first four strings in the above sequence are:
 *
 * S1 = "0"
 * S2 = "011"
 * S3 = "0111001"
 * S4 = "011100110110001"
 * Return the kth bit in Sn. It is guaranteed that k is valid for the given n.
 *
 * Example 1:
 *
 * Input: n = 3, k = 1
 * Output: "0"
 * Explanation: S3 is "0111001".
 * The 1st bit is "0".
 * Example 2:
 *
 * Input: n = 4, k = 11
 * Output: "1"
 * Explanation: S4 is "011100110110001".
 * The 11th bit is "1".
 *
 * Constraints:
 *
 * 1 <= n <= 20
 * 1 <= k <= 2n - 1
 */
/**
 * Convert it to iterative.
 */
class Solution {
public:
    char findKthBit(int n, int k) {
        int flip = 0;
        int L = (1 << n) - 1;
        while (k > 1) {
            if (k == L / 2 + 1) {
                return '0' + (flip ^ 1);
            }

            if (k > L / 2) {
                k = L + 1 - k;
                flip ^= 1;
            }

            L /= 2;
        }
        return '0' + flip;
    }
};

/**
 * DFS solution.
 */
int sn_length(int n)
{
    int cnt = 1;
    for (int i = 1; i < n; ++i) {
        cnt = 2 * cnt + 1;
    }

    return cnt;
}

char dfs(int L, int k)
{
    if (1 == k) {
        return '0';
    }

    int half = L / 2;
    if (k == half) {
        return '1';
    }

    if (k > half) {
        int idx = half - (k - half - 1) + 1;
        return '0' == dfs(half, idx) ? '1' : '0';
    }

    return dfs(half, k);
}

class Solution {
public:
    char findKthBit(int n, int k) {
        return dfs(sn_length(n), k);
    }
};

/**
 * Original solution.
 */
string invert(string const& s)
{
    string copy = s;
    for (auto& c : copy) {
        c = '1' == c ? '0' : '1';
    }

    return copy;
}

string reverse(string s)
{
    reverse(s.begin(), s.end());
    return s;
}

class Solution {
public:
    char findKthBit(int n, int k) {
        string s = "0";
        for (int i = 0; i < n; ++i) {
            auto ir = reverse(invert(s));
            s = s + "1" + ir;
        }

        return s[k - 1];
    }
};
