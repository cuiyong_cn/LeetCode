/**
 * Out of Boundary Paths
 *
 * There is an m x n grid with a ball. The ball is initially at the position [startRow,
 * startColumn]. You are allowed to move the ball to one of the four adjacent cells in the grid
 * (possibly out of the grid crossing the grid boundary). You can apply at most maxMove moves to the
 * ball.
 *
 * Given the five integers m, n, maxMove, startRow, startColumn, return the number of paths to move
 * the ball out of the grid boundary. Since the answer can be very large, return it modulo 109 + 7.
 *
 * Example 1:
 *
 * Input: m = 2, n = 2, maxMove = 2, startRow = 0, startColumn = 0
 * Output: 6
 * Example 2:
 *
 * Input: m = 1, n = 3, maxMove = 3, startRow = 0, startColumn = 1
 * Output: 12
 *
 * Constraints:
 *
 * 1 <= m, n <= 50
 * 0 <= maxMove <= 50
 * 0 <= startRow < m
 * 0 <= startColumn < n
 */
/**
 * DP solution.
 */
class Solution {
public:
    int findPaths(int m, int n, int maxMove, int startRow, int startColumn) {
        int const mod = 1e9 + 7;
        auto dp = vector<vector<int>>(m, vector<int>(n, 0));
        dp[startRow][startColumn] = 1;

        int cnt = 0;
        for (int move = 1; move <= maxMove; ++move) {
            auto tmp = vector<vector<int>>(m, vector<int>(n, 0));
            for (int i = 0; i < m; ++i) {
                for (int j = 0; j < n; ++j) {
                    if (i == (m - 1)) {
                        cnt = (cnt + dp[i][j]) % mod;
                    }
                    if (j == (n - 1)) {
                        cnt = (cnt + dp[i][j]) % mod;
                    }
                    if (0 == i) {
                        cnt = (cnt + dp[i][j]) % mod;
                    }
                    if (0 == j) {
                        cnt = (cnt + dp[i][j]) % mod;
                    }

                    auto up = i > 0 ? dp[i - 1][j] : 0;
                    auto down = i < (m - 1) ? dp[i + 1][j] : 0;
                    auto left = j > 0 ? dp[i][j - 1] : 0;
                    auto right = j < (n - 1) ? dp[i][j + 1] : 0;
                    tmp[i][j] = ((up + down) % mod + (left + right) % mod) % mod;
                }
            }
            swap(dp, tmp);
        }

        return cnt;
    }
};

/**
 * Original solution. Memo solution.(actually variant dp)
 */
class Solution {
public:
    int findPaths(int m, int n, int maxMove, int startRow, int startColumn) {
        memo = vector<vector<vector<int>>>(m, vector<vector<int>>(n, vector<int>(maxMove + 1, -1)));

        return dfs(m, n, maxMove, startRow, startColumn);
    }
private:
    int dfs(int M, int N, int move_left, int r, int c)
    {
        if (r == M || c == N || r < 0 || c < 0) {
            return 1;
        }

        if (0 == move_left) {
            return 0;
        }

        if (memo[r][c][move_left] >= 0) {
            return memo[r][c][move_left];
        }

        memo[r][c][move_left] =
            ((dfs(M, N, move_left - 1, r - 1, c) + dfs(M, N, move_left - 1, r, c + 1)) % mod
            + (dfs(M, N, move_left - 1, r + 1, c) + dfs(M, N, move_left - 1, r, c - 1)) % mod) % mod;
        return memo[r][c][move_left];
    }

private:
    int const mod = 1e9 + 7;
    vector<vector<vector<int>>> memo;
};
