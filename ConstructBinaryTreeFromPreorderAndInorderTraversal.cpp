/**
 * Construct Binary Tree from Preorder and Inorder Traversal
 *
 * Given preorder and inorder traversal of a tree, construct the binary tree.
 *
 * Note:
 * You may assume that duplicates do not exist in the tree.
 *
 * For example, given
 *
 * preorder = [3,9,20,15,7]
 * inorder = [9,3,15,20,7]
 *
 * Return the following binary tree:
 *
 *     3
 *    / \
 *   9  20
 *     /  \
 *    15   7
 */
/**
 * Iterative version. More efficient.
 */
class Solution
{
public:
    TreeNode *buildTree(vector<int> &preorder, vector<int> &inorder)
    {
        if (preorder.empty()) {
            return nullptr;
        }

        stack<TreeNode*> stk;
        TreeNode *root = new TreeNode(preorder[0]);

        stk.push(root);
        int index = 0;

        for (int i = 1; i < preorder.size(); ++i) {
            TreeNode* cur = stk.top();

            if (cur->val != inorder[index]) {
                cur->left = new TreeNode(preorder[i]);
                stk.push(cur->left);
            } else {
                while (!stk.empty() && stk.top()->val == inorder[index]) {
                    cur = stk.top();
                    stk.pop();
                    ++index;
                }

                cur->right = new TreeNode(preorder[i]);
                stk.push(cur->right);
            }
        }

        return root;
    }
};

/**
 * Original solution. dfs solution.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution
{
public:
    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder)
    {
        if (preorder.empty()) return nullptr;

        int pre = 0;
        return helper(preorder, inorder, pre, 0, inorder.size());
    }

private:
    TreeNode* helper(const vector<int>& preorder, const vector<int>& inorder, int& pre, int ins, int ine)
    {
        if (ins >= ine) return nullptr;

        TreeNode* root = new TreeNode(preorder[pre]);
        int i = ins;
        for (; i < ine; ++i) {
            if (preorder[pre] == inorder[i]) break;
        }

        ++pre;
        root->left = helper(preorder, inorder, pre, ins, i);
        root->right = helper(preorder, inorder, pre, i + 1, ine);

        return root;
    }
};

