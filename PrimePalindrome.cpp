/**
 * Prime Palindrome
 *
 * Given an integer n, return the smallest prime palindrome greater than or equal to n.
 *
 * An integer is prime if it has exactly two divisors: 1 and itself. Note that 1 is not a prime number.
 *
 * For example, 2, 3, 5, 7, 11, and 13 are all primes.
 * An integer is a palindrome if it reads the same from left to right as it does from right to left.
 *
 * For example, 101 and 12321 are palindromes.
 * The test cases are generated so that the answer always exists and is in the range [2, 2 * 108].
 *
 * Example 1:
 *
 * Input: n = 6
 * Output: 7
 * Example 2:
 *
 * Input: n = 8
 * Output: 11
 * Example 3:
 *
 * Input: n = 13
 * Output: 101
 *
 * Constraints:
 *
 * 1 <= n <= 108
 */
/**
 * Solution from discussion. This is a little hard.
 *
 * panlindrome can be divided into two kinds:
 * 1. odd digits
 * 2. even digits (we can rule this out, because all even digits panlindrome divisible by 11)
 *
 * We prove point 2 by this:
 * abcddcba % 11 = (a * 10000001 + b * 100001 * 10 + c * 1001 * 100 + d * 11 * 1000) % 11
 *               = 0
 *
 * so questioin can became how can we generate a odd digits panlindrome number.
 */
class Solution {
public:
    bool is_prime(int n)
    {
        int sqrt_n = sqrt(n);
        for (int i = 2; i <= sqrt_n; ++i) {
            if (0 == (n % i)) {
                return false;
            }
        }

        return n > 1;
    }

    int primePalindrome(int n) {
        if (8 <= n && n <= 11) return 11;
        for (int x = 1; x < 100000; ++x) {
            string s = to_string(x), r(s.rbegin(), s.rend());
            int y = stoi(s + r.substr(1));
            if (y >= n && is_prime(y)) return y;
        }

        return -1;
    }
};

/**
 * Original solution. TLE.
 */
class Solution {
public:
    bool is_panlindrome(int n)
    {
        auto num = to_string(n);
        for (int i = 0, j = num.length() - 1; i <= j; ++i, --j) {
            if (num[i] != num[j]) {
                return false;
            }
        }

        return true;
    }

    bool is_prime(int n)
    {
        int sqrt_n = sqrt(n);
        for (int i = 2; i <= sqrt_n; ++i) {
            if (0 == (n % i)) {
                return false;
            }
        }

        return n > 1;
    }

    int primePalindrome(int n) {
        int const max_n = 2 * 1e8 + 1;
        for (int i = n; i < max_n; ++i) {
            if (is_panlindrome(i)) {
                if (is_prime(i)) {
                    return i;
                }
            }
        }

        return 2;
    }
};
