/**
 * N-ary Tree Level Order Traversal
 *
 * Given an n-ary tree, return the level order traversal of its nodes' values. (ie, from
 * left to right, level by level).
 *
 * For example, given a 3-ary tree:
 *                       1
 *                      /|\
 *                     / | \
 *                    3  2  4
 *                   / \
 *                  5   6
 *
 * We should return its level order traversal:
 *
 * [
 *      [1],
 *      [3,2,4],
 *      [5,6]
 * ]
 *
 * Note:
 *     The depth of the tree is at most 1000.
 *     The total number of nodes is at most 5000.
 */
/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/
/**
 * Intuitive BFS method.
 */
class Solution {
public:
    vector<vector<int>> levelOrder(Node* root) {
        vector<vector<int>> ans;
        if (root == nullptr) return ans;

        deque<Node*> queue;
        queue.push_back(root);
        while (!queue.empty()) {
            auto qsize = queue.size();
            vector<int> list(qsize);
            for (int i = 0; i < qsize; ++i) {
                Node* n = queue.front();
                queue.pop_front();
                list[i] = n->val;
                for (auto c : n->children) {
                    queue.push_back(c);
                }
            }
            ans.push_back(list);
        }
        return ans;
    }
};
