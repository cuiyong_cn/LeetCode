/**
 * Diagonal Traverse II
 *
 * Given a list of lists of integers, nums, return all elements of nums in diagonal order as shown in the below images.
 *
 *  Example 1:
 *
 *  Input: nums = [[1,2,3],[4,5,6],[7,8,9]]
 *  Output: [1,4,2,7,5,3,8,6,9]
 *  Example 2:
 *
 *  Input: nums = [[1,2,3,4,5],[6,7],[8],[9,10,11],[12,13,14,15,16]]
 *  Output: [1,6,2,8,7,3,9,4,12,10,5,13,11,14,15,16]
 *  Example 3:
 *
 *  Input: nums = [[1,2,3],[4],[5,6,7],[8],[9,10,11]]
 *  Output: [1,4,2,5,3,8,6,9,7,10,11]
 *  Example 4:
 *
 *  Input: nums = [[1,2,3,4,5,6]]
 *  Output: [1,2,3,4,5,6]
 *
 *   Constraints:
 *
 *   1 <= nums.length <= 10^5
 *   1 <= nums[i].length <= 10^5
 *   1 <= nums[i][j] <= 10^9
 *   There at most 10^5 elements in nums.
 */
/**
 * From discussion. Using BFS. Really smart one.
 */
class Solution {
public:
    vector<int> findDiagonalOrder(vector<vector<int>>& nums) {
        int m = nums.size();
		vector<int> ans;
        queue<pair<int, int>> q;
        q.push({0,0}); // first row, first cell

		// BFS
        while (!q.empty()) {
            auto [r, c] = q.front(); q.pop();
            ans.push_back(nums[r][c]);

			// insert the element below, if in first column
            if (0 == c && (r + 1) < m) {
                q.push({r + 1, c});
            }

			// insert the right neighbour, if exists
            if ((c +1) < nums[r].size()) {
                q.push({r, c + 1});
            }
        }

        return ans;
    }
};

/**
 * Same idea as below one.
 */
class Solution {
public:
    vector<int> findDiagonalOrder(vector<vector<int>>& nums) {
        vector<int> answer;
        unordered_map<int, vector<int>> m;
        int maxKey = 0; // maximum key inserted into the map i.e. max value of i+j indices.

        for (int i = 0; i < nums.size(); ++i) {
            for (int j = 0; j < nums[i].size(); ++j) {
                m[i + j].push_back(nums[i][j]); // insert nums[i][j] in bucket (i+j).
                if ((i + j) > maxKey) {
                    maxKey = i + j;
                }
            }
        }

        for (int i = 0; i <= maxKey; i++) { // Each diagonal starting with sum 0 to sum maxKey.
            const auto& diag = m[i];
            answer.insert(answer.end(), diag.rbegin(), diag.rend());
        }

        return answer;
    }
};

/**
 * Original solution.
 *
 * Observation:
 *  1. all the nums in the same diagonal, their sum of row and column indexes are the same.
 */
class Solution {
public:
    vector<int> findDiagonalOrder(vector<vector<int>>& nums) {
        vector<tuple<int, int, int>> sorted_nums;
        for (int i = 0; i < nums.size(); ++i) {
            for (int j = 0; j < nums[i].size(); ++j) {
                sorted_nums.emplace_back(std::make_tuple(i + j, i, nums[i][j]));
            }
        }

        sort(sorted_nums.begin(), sorted_nums.end(),
             [](const auto& a, const auto& b) {
                 int diff0 = std::get<0>(a) - std::get<0>(b);
                 if (diff0 < 0) {
                     return true;
                 }
                 if (diff0 > 0) {
                     return false;
                 }

                 return std::get<1>(a) > std::get<1>(b);
             });

        vector<int> ans(sorted_nums.size(), 0);
        transform(sorted_nums.begin(), sorted_nums.end(), ans.begin(),
                 [](const auto& a) {
                     return std::get<2>(a);
                 });
        return ans;
    }
};
