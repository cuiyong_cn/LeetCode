/**
 * Brace Expansion II
 *
 * Under a grammar given below, strings can represent a set of lowercase words.
 * Let's use R(expr) to denote the set of words the expression represents.
 *
 * Grammar can best be understood through simple examples:
 *
 *     Single letters represent a singleton set containing that word.
 *         R("a") = {"a"}
 *         R("w") = {"w"}
 *     When we take a comma delimited list of 2 or more expressions, we take the union of
 *     possibilities.
 *         R("{a,b,c}") = {"a","b","c"}
 *         R("{{a,b},{b,c}}") = {"a","b","c"} (notice the final set only contains each
 *                                             word at most once)
 *     When we concatenate two expressions, we take the set of possible concatenations
 *     between two words where the first word comes from the first expression and the
 *     second word comes from the second expression.
 *         R("{a,b}{c,d}") = {"ac","ad","bc","bd"}
 *         R("a{b,c}{d,e}f{g,h}") = {"abdfg", "abdfh", "abefg", "abefh", "acdfg", "acdfh",
 *                                   "acefg", "acefh"}
 *
 * Given an expression representing a set of words under the given grammar, return the
 * sorted list of words that the expression represents.
 *
 * Example 1:
 *
 * Input: "{a,b}{c,{d,e}}"
 * Output: ["ac","ad","ae","bc","bd","be"]
 *
 * Example 2:
 *
 * Input: "{{a,z},a{b,c},{ab,z}}"
 * Output: ["a","ab","ac","z"]
 * Explanation: Each distinct word is written only once in the final answer.
 *
 * Constraints:
 *
 *     1 <= expression.length <= 60
 *     expression[i] consists of '{', '}', ','or lowercase English letters.
 *     The given expression represents a set of words based on the grammar given in the description.
 */
/**
 * Directly copied from leetcode.
 * My version is really long and chatty, not wothy to post.
 */
class Solution {
public:
    string getWord(const string& expr, int& i) {
        string res;
        while (isalpha(expr[i])) {
            res += expr[i++];
        }

        return res;
    }

    unordered_set<string> dfs(const string& expr, int& i) {
        unordered_set<string> res;

        unordered_set<string> S;
        while (i < expr.size() && '}' != expr[i]) {
            if (expr[i] == ',') {
                for (auto& it : S)
                    res.insert(it);
                S.clear();
                ++i;
            }
            unordered_set<string> t;
            if (expr[i] == '{') {
                i++;
                t = dfs(expr, i);
                i++;
            } else {
                t.insert(getWord(expr, i));
            }

            if (true == S.empty()) {
                S = t;
            }
            else {
                unordered_set<string> S2;
                for (auto& s1 : S) {
                    for (auto& s2 : t)
                        S2.insert(s1 + s2);
                }
                S = S2;
            }
        }

        for (auto& s1 : S)
            res.insert(s1);

        return res;
    }

    vector<string> braceExpansionII(string expression) {
        int i = 0;
        unordered_set<string> S = dfs(expression, i);
        vector<string> res(S.begin(), S.end());
        sort(res.begin(), res.end());

        return res;
    }
};
