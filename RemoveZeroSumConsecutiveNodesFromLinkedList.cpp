/**
 * Remove Zero Sum Consecutive Nodes from Linked List
 *
 * Given the head of a linked list, we repeatedly delete consecutive sequences of nodes that sum to
 * 0 until there are no such sequences.
 *
 * After doing so, return the head of the final linked list.  You may return any such answer.
 *
 * (Note that in the examples below, all sequences are serializations of ListNode objects.)
 *
 * Example 1:
 *
 * Input: head = [1,2,-3,3,1]
 * Output: [3,1]
 * Note: The answer [1,2,1] would also be accepted.
 * Example 2:
 *
 * Input: head = [1,2,3,-3,4]
 * Output: [1,2,4]
 * Example 3:
 *
 * Input: head = [1,2,3,-3,-2]
 * Output: [1]
 *
 * Constraints:
 *
 * The given linked list will contain between 1 and 1000 nodes.
 * Each node in the linked list has -1000 <= node.val <= 1000
 */
/**
 * Original solution. Change to map seems faster than unordered_map
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* removeZeroSumSublists(ListNode* head) {
        vector<ListNode*> node_arr;
        unordered_map<int, int> sum_pos;
        sum_pos[0] = 0;
        int sum = 0;
        for (auto cur = head; cur; cur = cur->next) {
            node_arr.push_back(cur);
            sum += cur->val;
            auto it = sum_pos.find(sum);
            if (it != sum_pos.end()) {
                auto pos = it->second;
                node_arr.erase(begin(node_arr) + pos, end(node_arr));
                for (auto i = begin(sum_pos), last = end(sum_pos); i != last;) {
                    if (i->second > pos) {
                        i = sum_pos.erase(i);
                    }
                    else {
                        ++i;
                    }
                }
            }
            else {
                sum_pos[sum] = node_arr.size();
            }
        }

        ListNode dummy;
        auto cur = &dummy;
        for (auto n : node_arr) {
            cur->next = n;
            cur = n;
        }
        cur->next = nullptr;

        return dummy.next;
    }
};
