/** Map Sum Pairs
 *
 * Implement a MapSum class with insert, and sum methods.
 *
 * For the method insert, you'll be given a pair of (string, integer). The string represents the key and the integer
 * represents the value. If the key already existed, then the original key-value pair will be overridden to the new one.
 *
 * For the method sum, you'll be given a string representing the prefix, and you need to return the sum of all the
 * pairs' value whose key starts with the prefix.
 *
 * Example 1:
 *
 * Input: insert("apple", 3), Output: Null Input: sum("ap"), Output: 3 Input: insert("app", 2), Output: Null Input:
 * sum("ap"), Output: 5
 */
/**
 * Original solution. Store the string with the same first letter in a map. And do the linear search.
 * This solution depend on the distribution of the keys. Worst case is every search need to go through
 * all the pairs. Mean complexity is O(KN) (K is search times);
 *
 * Also we can using the following method.
 * 1. Every insert. We store or update every possible prefix key value pair.
 * 2. Using trie data structure.
 */
class MapSum {
public:
    unordered_map<char, unordered_map<string, int>> pairMap;
    /** Initialize your data structure here. */
    MapSum() {

    }

    void insert(string key, int val) {
        pairMap[key[0]][key] = val;
    }

    int sum(string prefix) {
        if (prefix.length() == 0) return 0;
        int ans = 0;
        auto& pairs = pairMap[prefix[0]];
        for (auto& p : pairs) {
            if (p.first.compare(0, prefix.length(), prefix) == 0) {
                ans += p.second;
            }
        }
        return ans;
    }
};

/**
 * Your MapSum object will be instantiated and called as such:
 * MapSum* obj = new MapSum();
 * obj->insert(key,val);
 * int param_2 = obj->sum(prefix);
 */
