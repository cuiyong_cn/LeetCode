/**
 * Word Search
 *
 * Given an m x n grid of characters board and a string word, return true if word exists in the
 * grid.
 *
 * The word can be constructed from letters of sequentially adjacent cells, where adjacent cells are
 * horizontally or vertically neighboring. The same letter cell may not be used more than once.
 *
 * Example 1:
 *
 * Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCCED"
 * Output: true
 * Example 2:
 *
 * Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "SEE"
 * Output: true
 * Example 3:
 *
 * Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCB"
 * Output: false
 *
 * Constraints:
 *
 * m == board.length
 * n = board[i].length
 * 1 <= m, n <= 6
 * 1 <= word.length <= 15
 * board and word consists of only lowercase and uppercase English letters.
 *
 * Follow up: Could you use search pruning to make your solution faster with a larger board?
 */
/**
 * More short one.
 */
class Solution {
public:
    bool exist(vector<vector<char>>& board, string word) {
        if (word.empty()) return true;
        if (board.empty() || board[0].empty()) return false;
        
        m = board.size();
        n = board[0].size();
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                if (dfsSearch(board, word, 0, i, j)) {
                    return true;
                }
            }
        }
        return false;
    }

private:
    int m;
    int n;
    bool dfsSearch(vector<vector<char>>& board, string& word, int k, int i, int j)
    {
        if (i < 0 || i >= m || j < 0 || j >= n || word[k] != board[i][j]) {
            return false;
        }

        if (k == word.length() - 1) {
            return true;
        }

        char cur = board[i][j];
        board[i][j] = '*';
        bool search_next = dfsSearch(board, word, k+1, i-1 ,j) 
                        || dfsSearch(board, word, k+1, i+1, j) 
                        || dfsSearch(board, word, k+1, i, j-1)
                        || dfsSearch(board, word, k+1, i, j+1);
        board[i][j] = cur;
        return search_next;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool exist(vector<vector<char>>& board, string word) {
        auto const M = board.size();
        auto const N = board[0].size();
        vector<vector<bool>> visited(M, vector<bool>(N, false));

        for (int i = 0; i < M; ++i) {
            for (int j = 0; j < N; ++j) {
                if (word.front() == board[i][j]) {
                    visited[i][j] = true;
                    if (dfs(board, visited, word, i, j, 1)) {
                        return true;
                    }
                    visited[i][j] = false;
                }
            }
        }

        return false;
    }

private:
    bool dfs(vector<vector<char>> const& board, vector<vector<bool>>& visited, string const& word, int r, int c, int pos)
    {
        if (pos == word.length()) {
            return true;
        }

        auto const M = board.size();
        auto const N = board[0].size();

        if (r > 0) {
            if (false == visited[r - 1][c] && word[pos] == board[r - 1][c]) {
                visited[r - 1][c] = true;
                if (dfs(board, visited, word, r - 1, c, pos + 1)) {
                    return true;
                }
                visited[r - 1][c] = false;
            }
        }

        if (c > 0) {
            if (false == visited[r][c - 1] && word[pos] == board[r][c - 1]) {
                visited[r][c - 1] = true;
                if (dfs(board, visited, word, r, c - 1, pos + 1)) {
                    return true;
                }
                visited[r][c - 1] = false;
            }
        }

        if (r < (M - 1)) {
            if (false == visited[r + 1][c] && word[pos] == board[r + 1][c]) {
                visited[r + 1][c] = true;
                if (dfs(board, visited, word, r + 1, c, pos + 1)) {
                    return true;
                }
                visited[r + 1][c] = false;
            }
        }

        if (c < (N - 1)) {
            if (false == visited[r][c + 1] && word[pos] == board[r][c + 1]) {
                visited[r][c + 1] = true;
                if (dfs(board, visited, word, r, c + 1, pos + 1)) {
                    return true;
                }
                visited[r][c + 1] = false;
            }
        }

        return false;
    }
};
