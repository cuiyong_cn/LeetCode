/**
 * Snapshot Array
 *
 * Implement a SnapshotArray that supports the following interface:
 *
 * SnapshotArray(int length) initializes an array-like data structure with the given length.
 * Initially, each element equals 0.  void set(index, val) sets the element at the given index to be
 * equal to val.  int snap() takes a snapshot of the array and returns the snap_id: the total number
 * of times we called snap() minus 1.  int get(index, snap_id) returns the value at the given index,
 * at the time we took the snapshot with the given snap_id
 *
 * Example 1:
 *
 * Input: ["SnapshotArray","set","snap","set","get"]
 * [[3],[0,5],[],[0,6],[0,0]]
 * Output: [null,null,0,null,5]
 * Explanation:
 * SnapshotArray snapshotArr = new SnapshotArray(3); // set the length to be 3
 * snapshotArr.set(0,5);  // Set array[0] = 5
 * snapshotArr.snap();  // Take a snapshot, return snap_id = 0
 * snapshotArr.set(0,6);
 * snapshotArr.get(0,0);  // Get the value of array[0] with snap_id = 0, return 5
 *
 * Constraints:
 *
 * 1 <= length <= 50000
 * At most 50000 calls will be made to set, snap, and get.
 * 0 <= index < length
 * 0 <= snap_id < (the total number of times we call snap())
 * 0 <= val <= 10^9
 */
/**
 * Using map + vector. Both time and space efficient.
 */
class SnapshotArray {
public:
    SnapshotArray(int length) { }

    void set(int index, int val)
    {
        if (0 == m.count(index) || m_snap_id > m[index].back().first) {
            m[index].push_back({m_snap_id, val});
        }
        else {
            m[index].back().second = val;
        }
    }

    int snap() { return m_snap_id++; }

    int get(int index, int snap_id)
    {
        if (0 == m.count(index)) {
            return 0;
        }

        auto const& snap_value = m[index];
        auto it = upper_bound(begin(snap_value), end(snap_value), pair<int, int>(snap_id, numeric_limits<int>::max()));

        return it == begin(snap_value) ? 0 : prev(it)->second;
    }

private:
    int m_snap_id = 0;
    unordered_map<int, vector<pair<int, int>>> m;
};

/**
 * Using map.
 */
class SnapshotArray {
private:
    map<int, map<int, int>> snaps;
    int snapId = 0;
public:
    SnapshotArray(int length) {
        for (int i = 0; i < length; i++) {
            map<int, int> mp; mp[0] = 0;
            snaps[i] = mp;
        }
    }

    void set(int index, int val) {
        snaps[index][snapId] = val;
    }

    int snap() {
        return snapId++;
    }

    int get(int index, int snap_id) {
        auto it = snaps[index].upper_bound(snap_id); it--;
        return it->second;
    }
};

/**
 * Original solution. Easy to understand but no very efficient maybe.
 */
class SnapshotArray {
public:
    SnapshotArray(int length) : m_snap_id{0}, values(length) {

    }

    void set(int index, int val) {
        auto& snap_value = values[index];
        if (snap_value.empty() || m_snap_id > snap_value.back().second) {
            snap_value.push_back({val, m_snap_id});
        }
        else {
            snap_value.back().first = val;
        }
    }

    int snap() {
        return m_snap_id++;
    }

    int get(int index, int snap_id) {
        int ans = 0;
        for (auto const& snap_value : values[index]) {
            if (snap_value.second > snap_id) {
                break;
            }
            ans = snap_value.first;
        }

        return ans;
    }

private:
    int m_snap_id;
    using SnapValue = pair<int, int>;
    vector<vector<SnapValue>> values;
};

/**
 * Your SnapshotArray object will be instantiated and called as such:
 * SnapshotArray* obj = new SnapshotArray(length);
 * obj->set(index,val);
 * int param_2 = obj->snap();
 * int param_3 = obj->get(index,snap_id);
 */
