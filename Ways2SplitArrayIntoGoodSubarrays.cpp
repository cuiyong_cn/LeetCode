/**
 * Ways to Split Array Into Good Subarrays
 *
 * You are given a binary array nums.
 *
 * A subarray of an array is good if it contains exactly one element with the value 1.
 *
 * Return an integer denoting the number of ways to split the array nums into good subarrays. As the number may be too large, return it modulo 109 + 7.
 *
 * A subarray is a contiguous non-empty sequence of elements within an array.
 *
 * Example 1:
 *
 * Input: nums = [0,1,0,0,1]
 * Output: 3
 * Explanation: There are 3 ways to split nums into good subarrays:
 * - [0,1] [0,0,1]
 * - [0,1,0] [0,1]
 * - [0,1,0,0] [1]
 * Example 2:
 *
 * Input: nums = [0,1,0]
 * Output: 1
 * Explanation: There is 1 way to split nums into good subarrays:
 * - [0,1,0]
 *
 * Constraints:
 *
 * 1 <= nums.length <= 105
 * 0 <= nums[i] <= 1
 */
/**
 * Original solution.
 */
class Solution {
public:
    int numberOfGoodSubarraySplits(vector<int>& nums) {
        auto first_one = find(begin(nums), end(nums), 1);
        if (end(nums) == first_one) {
            return 0;
        }

        auto ans = 1LL;
        int const size = nums.size();
        auto const mod = 1'000'000'007;
        auto zeros = 0;

        for (auto i = distance(begin(nums), first_one); i < size; ++i) {
            if (1 == nums[i]) {
                ans = (ans * (zeros + 1)) % mod;
                zeros = 0;
            }
            else {
                ++zeros;
            }
        }

        return ans;
    }
};
