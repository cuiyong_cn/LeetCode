/**
 * Best Time to Buy and Sell Stock with Cooldown
 *
 * Say you have an array for which the ith element is the price of a given stock on day i.
 *
 * Design an algorithm to find the maximum profit. You may complete as many transactions as you like (ie, buy one and sell one share of the stock multiple times) with the following restrictions:
 *
 * You may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).
 * After you sell your stock, you cannot buy stock on next day. (ie, cooldown 1 day)
 * Example:
 *
 * Input: [1,2,3,0,2]
 * Output: 3
 * Explanation: transactions = [buy, sell, cooldown, buy, sell]
 */
/**
 * Reduce the space to O(1)
 */
class Solution {
public:
    int maxProfit(vector<int> &prices) {
        int buy(INT_MIN), sell(0), prev_sell(0), prev_buy;
        for (int price : prices) {
            prev_buy = buy;
            buy = max(prev_sell - price, buy);
            prev_sell = sell;
            sell = max(prev_buy + price, sell);
        }
        return sell;
    }
};

/**
 * The solution from the discussion.
 *
 * refer to https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-cooldown/discuss/75928/Share-my-DP-solution-(By-State-Machine-Thinking)
 *
 * Basic idea is:
 *
 * s0[i] = max(s0[i - 1], s2[i - 1]); // Stay at s0, or rest from s2
 * s1[i] = max(s1[i - 1], s0[i - 1] - prices[i]); // Stay at s1, or buy from s0
 * s2[i] = s1[i - 1] + prices[i]; // Only one way from s1
 */
class Solution {
public:
	int maxProfit(vector<int>& prices){
        if (prices.size() <= 1) {
            return 0;
        }

		vector<int> s0(prices.size(), 0);
		vector<int> s1(prices.size(), 0);
		vector<int> s2(prices.size(), 0);

		s0[0] = 0;
		s1[0] = -prices[0];
		s2[0] = INT_MIN;

		for (int i = 1; i < prices.size(); i++) {
			s0[i] = max(s0[i - 1], s2[i - 1]);
			s1[i] = max(s1[i - 1], s0[i - 1] - prices[i]);
			s2[i] = s1[i - 1] + prices[i];
		}

		return max(s0[prices.size() - 1], s2[prices.size() - 1]);
	}
};

/**
 * I think this is the correct solution. But sadly TLE
 */
class Solution {
public:
    int maxProfit(vector<int>& prices) {
        return dfs(prices, 0);
    }

private:
    int dfs(const vector<int>& prices, int day)
    {
        if (day >= prices.size()) {
            return 0;
        }

        auto [day_buy, day_sell] = day_to_buy_and_sell(prices, day);
        if (-1 == day_buy || -1 == day_sell) {
            return 0;
        }

        int cool_down = 1;
        int ans = 0;

        for (int sell = day_sell - cool_down; sell <= day_sell; ++sell) {
            ans = max(ans, prices[sell] - prices[day_buy] + dfs(prices, sell + cool_down + 1));
            //cout << "buy: " << day_buy << ", sell: " << sell << ", profit: " << ans << "\n";
        }

        if ((day_sell + 1) < prices.size()) {
            ans = max(ans, prices[day_sell + 1] - prices[day_buy] + dfs(prices, day_sell + 1));
        }

        return ans;
    }

    std::pair<int, int> day_to_buy_and_sell(const vector<int>& prices, int day)
    {
        int buy = -1;
        int sell = -1;
        for (int d = day + 1; d < prices.size(); ++d) {
            if (prices[d - 1] < prices[d]) {
                buy = d - 1;
                break;
            }
        }

        if (buy >= day) {
            for (int d = buy + 1; d < prices.size(); ++d) {
                if (prices[d - 1] > prices[d]) {
                    break;
                }
                sell = d;
            }
        }

        return {buy, sell};
    }
};

/**
 * Note: this is not a correct solution. Just the first one I came up with and passed 80% of the test.
 */
class Solution {
public:
    int maxProfit(vector<int>& prices) {
        return dfs(prices, 0);
    }

private:
    int dfs(const vector<int>& prices, int day)
    {
        if (day >= prices.size()) {
            return 0;
        }

        auto [day_buy, day_sell] = day_to_buy_and_sell(prices, day);
        if (-1 == day_buy || -1 == day_sell) {
            return 0;
        }

        int cool_down = 1;
        int ans = 0;

        for (int sell = day_sell - cool_down; sell <= day_sell; ++sell) {
            ans = max(ans, prices[sell] - prices[day_buy] + dfs(prices, sell + cool_down + 1));
            cout << "buy: " << day_buy << ", sell: " << sell << ", profit: " << ans << "\n";
        }

        return ans;
    }

    std::pair<int, int> day_to_buy_and_sell(const vector<int>& prices, int day)
    {
        int buy = -1;
        int sell = -1;
        for (int d = day + 1; d < prices.size(); ++d) {
            if (prices[d - 1] < prices[d]) {
                buy = d - 1;
                break;
            }
        }

        if (buy >= day) {
            for (int d = buy + 1; d < prices.size(); ++d) {
                if (prices[d - 1] > prices[d]) {
                    break;
                }
                sell = d;
            }
        }

        return {buy, sell};
    }
};
