/**
 * You have a set of tiles, where each tile has one letter tiles[i] printed on it.
 * Return the number of possible non-empty sequences of letters you can make.
 * 
 * Example 1:
 * 
 * Input: "AAB"
 * Output: 8
 * Explanation: The possible sequences are
 *          "A", "B", "AA", "AB", "BA", "AAB", "ABA", "BAA".
 * 
 * Example 2:
 * 
 * Input: "AAABBC"
 * Output: 188
 * 
 * Note:
 * 
 *    1. 1 <= tiles.length <= 7
 *    2. tiles consists of uppercase English letters.
 */
class Solution {
public:
    int dfs(map<char, int>& ms)
    {
        int sum = 0;
        auto it = ms.begin();
        for (; it != ms.end(); ++it) {
            if (it->second == 0) continue;
            ++sum;
            --it->second;
            sum += dfs(ms, str);
            ++it->second;
        }

        return sum;
    }

    int numTilePossibilities(string tiles) {
        map<char, int> msets;
        for (auto c : tiles) {
            msets[c] += 1;
        }

        return dfs(msets);
    }
};
