/**
 * Number of Ways to Reach a Position After Exactly k Steps
 *
 * You are given two positive integers startPos and endPos. Initially, you are standing at position
 * startPos on an infinite number line. With one step, you can move either one position to the left,
 * or one position to the right.
 *
 * Given a positive integer k, return the number of different ways to reach the position endPos
 * starting from startPos, such that you perform exactly k steps. Since the answer may be very
 * large, return it modulo 109 + 7.
 *
 * Two ways are considered different if the order of the steps made is not exactly the same.
 *
 * Note that the number line includes negative integers.
 *
 * Example 1:
 *
 * Input: startPos = 1, endPos = 2, k = 3
 * Output: 3
 * Explanation: We can reach position 2 from 1 in exactly 3 steps in three ways:
 * - 1 -> 2 -> 3 -> 2.
 * - 1 -> 2 -> 1 -> 2.
 * - 1 -> 0 -> 1 -> 2.
 * It can be proven that no other way is possible, so we return 3.
 * Example 2:
 *
 * Input: startPos = 2, endPos = 5, k = 10
 * Output: 0
 * Explanation: It is impossible to reach position 5 from position 2 in exactly 10 steps.
 *
 * Constraints:
 *
 * 1 <= startPos, endPos, k <= 1000
 */
/**
 * DP solution.
 */
int odd_or_even(int val)
{
    return val & 1;
}

bool same_odd_or_even(int a, int b)
{
    return odd_or_even(a) == odd_or_even(b);
}

class Solution {
public:
    Solution() : dp(1001, vector<int>(1001, 0)), mod{1'000'000'007} {}

    int numberOfWays(int startPos, int endPos, int k) {
        auto distance = abs(startPos - endPos);
        if (!same_odd_or_even(distance, k)) {
            return 0;
        }

        return dfs(k, distance);
    }

private:
    int dfs(int k, int dist)
    {
        if (k <= dist) {
            return k == dist ? 1 : 0;
        }

        if (0 == dp[k][dist]) {
            dp[k][dist] = (1 + dfs(k - 1, dist + 1) + dfs(k - 1, abs(dist - 1))) % mod;
        }

        return dp[k][dist] - 1;
    }

    vector<vector<int>> dp;
    int const mod;
};

/**
 * I can't think of how to calc cNr and capping it within mod.
 */
int odd_or_even(int val)
{
    return val & 1;
}

bool same_odd_or_even(int a, int b)
{
    return odd_or_even(a) == odd_or_even(b);
}

long long inv(long long a, int mod)
{
    return 1 == a ? 1 : (mod - mod / a) * inv(mod % a, mod) % mod;
}

class Solution {
public:
    int numberOfWays(int startPos, int endPos, int k) {
        auto distance = abs(endPos - startPos);
        if (!same_odd_or_even(distance, k) || distance > k) {
            return 0;
        }

        auto ans = 1LL;
        auto const mod = 1'000'000'007;
        auto right_steps = (k + distance) / 2;

        for (auto i = 0; i < right_steps; ++i) {
            ans = ans * (k - i) % mod;
            ans = ans * inv(i + 1, mod) % mod;
        }

        return ans;
    }
};
