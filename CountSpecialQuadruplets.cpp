/**
 * Count Special Quadruplets
 *
 * Given a 0-indexed integer array nums, return the number of distinct quadruplets (a, b, c, d) such that:
 *
 * nums[a] + nums[b] + nums[c] == nums[d], and
 * a < b < c < d
 *
 * Example 1:
 *
 * Input: nums = [1,2,3,6]
 * Output: 1
 * Explanation: The only quadruplet that satisfies the requirement is (0, 1, 2, 3) because 1 + 2 + 3 == 6.
 * Example 2:
 *
 * Input: nums = [3,3,6,4,5]
 * Output: 0
 * Explanation: There are no such quadruplets in [3,3,6,4,5].
 * Example 3:
 *
 * Input: nums = [1,1,1,3,5]
 * Output: 4
 * Explanation: The 4 quadruplets that satisfy the requirement are:
 * - (0, 1, 2, 3): 1 + 1 + 1 == 3
 * - (0, 1, 3, 4): 1 + 1 + 3 == 5
 * - (0, 2, 3, 4): 1 + 1 + 3 == 5
 * - (1, 2, 3, 4): 1 + 1 + 3 == 5
 *
 * Constraints:
 *
 * 4 <= nums.length <= 50
 * 1 <= nums[i] <= 100
 */
/**
 * O(n2) solution.
 */
class Solution {
public:
    int countQuadruplets(vector<int>& nums) {
        int res = 0;
        int len = nums.size();

        unordered_map<int, int> count;
        count[nums[len-1] - nums[len-2]] = 1;

        for (int b = len - 3; b >= 1; b--) {
            for (int a = b - 1; a >= 0; a--) {
                res += count[nums[a] + nums[b]];
            }

            for (int x = len - 1; x > b; x--) {
                count[nums[x] - nums[b]]++;
            }
        }

        return res;
    }
};

/**
 * Original solution. o(n3)
 */
class Solution {
public:
    int countQuadruplets(vector<int>& nums) {
        int const N = nums.size();
        target_cnt = vector<vector<int>>(N, vector<int>(101, 0));
        target_cnt.back()[nums.back()] = 1;
        for (int i = N - 2; i >= 0; --i) {
            target_cnt[i] = target_cnt[i + 1];
            ++target_cnt[i][nums[i]];
        }

        dfs(nums, 0, 0, 0);

        return ans;
    }

private:
    void dfs(vector<int>& nums, int idx, int cnt, int sum)
    {
        if (sum > 100) {
            return;
        }

        int const N = nums.size();
        if (idx >= N) {
            return;
        }

        if (3 == cnt) {
            ans += target_cnt[idx][sum];
            return;
        }

        dfs(nums, idx + 1, cnt + 1, sum + nums[idx]);
        dfs(nums, idx + 1, cnt, sum);
    }

    vector<vector<int>> target_cnt;
    int ans = 0;
};
