/**
 * Minimum Moves to Equal Array Elements II
 *
 * Given a non-empty integer array, find the minimum number of moves required to make all array elements equal, where
 * a move is incrementing a selected element by 1 or decrementing a selected element by 1.
 *
 * You may assume the array's length is at most 10,000.
 *
 * Example:
 *
 * Input:
 * [1,2,3]
 *
 * Output:
 * 2
 *
 * Explanation:
 * Only two moves are needed (remember each move increments or decrements one element):
 *
 * [1,2,3]  =>  [2,2,3]  =>  [2,2,2]
 */
/**
 * Another view perspective. First we assume the array is sorted.
 * Consider two points.
 * a----------------b
 * the smallest moves is any point between a and b, and the number of moves is b-a
 * Now add two more:
 * a-----c-----d----b
 * what's the minimum moves to make sure c and d make the smallest number of moves?
 * same logic as a and b, which is ANY point between c and d.
 * add one more
 * a-----c----e-----d------b
 * To make the moves minimum, we just use e, and the moves is same as the above.
 *
 * So we know the median is the final equal value we are looking for.
 * Let's make this value C. we know that there are half of them < C, half of them > C. The minimum moves are:
 *
 * C - a1 + C - a2 + ... + C - a[N/2] + a[N/2 + 1] - C + a[N/2 + 2] - C + ... + aN - C
 * aN - a1 + a[N - 1] - a2 + ... + a[N/2 + 1] - a[N/2] (N is even)
 * aN - a1 + a[N - 1] - a2 + ... + a[N/2 + 1] - a[N/2 - 1] + a[N/2] - a[N/2] (N is odd)
 */
class Solution {
public:
    int minMoves2(int[] nums) {
        sort(nums.begin(), nums.end());
        int i = 0, j = nums.size() - 1;
        int ans = 0;
        while(i < j){
            ans += nums[j] - nums[i];
            i++;
            j--;
        }
        return ans;
    }
}

/**
 * In math, median minimize the sum of absolute deviations. So we just find the median and sum the deviations.
 */
class Solution {
public:
    int minMoves2(vector<int>& nums) {
        int n = nums.size();
        auto it = nums.begin() + n/2;
        nth_element(nums.begin(), it, nums.end());
        int median = *it;
        int total = 0;
        for (auto &i : nums)
            total += abs(i-median);
        return total;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int minMoves2(vector<int>& nums) {
        int count = nums.size();
        int sum = accumulate(nums.begin(), nums.end(), 0);
        int eq = sum / count;
        sort(nums.begin(), nums.end());
        int mid = nums[count / 2];

        eq = min(eq, mid);

        int ans = 0;
        for (auto& n : nums) {
            ans += abs(n - mid);
        }
        return ans;
    }
};
