/**
 * Corporate Flight Bookings
 *
 * There are n flights, and they are labeled from 1 to n.
 *
 * We have a list of flight bookings.  The i-th booking bookings[i] = [i, j, k] means that we booked k seats from
 * flights labeled i to j inclusive.
 *
 * Return an array answer of length n, representing the number of seats booked on each flight in order of their label.
 *
 * Example 1:
 *
 * Input: bookings = [[1,2,10],[2,3,20],[2,5,25]], n = 5
 * Output: [10,55,45,25,25]
 *
 * Constraints:
 *
 *     1 <= bookings.length <= 20000
 *     1 <= bookings[i][0] <= bookings[i][1] <= n <= 20000
 *     1 <= bookings[i][2] <= 10000
 */
/**
 * One pass solution. Explanation from user votrubac. Take [[1,2,10],[2,3,20],[3,5,25]] for example
 *
 * Flights#            1    2    3    4    5
 * Reservation #1     10        -10
 * Reservation #2          20        -20
 * Reservation #3                25
 *
 * All Reservation    10   20    15  -20
 *                         +10  +30  +45  +25
 * Result             10   30    45   25   25
 */
class Solution {
public:
    vector<int> corpFlightBookings(vector<vector<int>>& bookings, int n) {
        vector<int> bookedSeats(n, 0);

        for (auto& b : bookings) {
            int sf = b[0], ef = b[1], bs = b[2];
            bookedSeats[sf - 1] += bs;
            if (ef < n) bookedSeats[ef] += -bs;
        }

        for (int i = 1; i < n; ++i) {
            bookedSeats[i] += bookedSeats[i - 1];
        }

        return bookedSeats;
    }
};

/**
 * Original solution. TLE (I know this is not that easy - -!)
 */
class Solution {
public:
    vector<int> corpFlightBookings(vector<vector<int>>& bookings, int n) {
        vector<int> bookedSeats(n, 0);

        for (auto& b : bookings) {
            int sf = b[0];
            int ef = b[1];
            int bs = b[2];
            for (int i = sf; i <= ef; ++i) {
                bookedSeats[i - 1] += bs;
            }
        }

        return bookedSeats;
    }
};
