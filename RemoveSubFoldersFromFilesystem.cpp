/**
* Remove Sub-Folders from the Filesystem
*
* Given a list of folders, remove all sub-folders in those folders and return in any order the folders after removing.
*
* If a folder[i] is located within another folder[j], it is called a sub-folder of it.
*
* The format of a path is one or more concatenated strings of the form: / followed by one or more lowercase English
* letters. For example, /leetcode and /leetcode/problems are valid paths while an empty string and / are not.
*
* Example 1:
*
* Input: folder = ["/a","/a/b","/c/d","/c/d/e","/c/f"]
* Output: ["/a","/c/d","/c/f"]
* Explanation: Folders "/a/b/" is a subfolder of "/a" and "/c/d/e" is inside of folder "/c/d" in our filesystem.
*
* Example 2:
*
* Input: folder = ["/a","/a/b/c","/a/b/d"]
* Output: ["/a"]
* Explanation: Folders "/a/b/c" and "/a/b/d/" will be removed because they are subfolders of "/a".
*
* Example 3:
*
* Input: folder = ["/a/b/c","/a/b/ca","/a/b/d"]
* Output: ["/a/b/c","/a/b/ca","/a/b/d"]
*
* Constraints:
*
*     1 <= folder.length <= 4 * 10^4
*     2 <= folder[i].length <= 100
*     folder[i] contains only lowercase letters and '/'
*     folder[i] always starts with character '/'
*     Each folder name is unique.
*/
/**
 * More improved version.
 */
class Solution {
public:
    vector<string> removeSubfolders(vector<string>& folder) {
        vector<string> ans;
        sort(folder.begin(), folder.end());
        ans.push_back(folder[0]);

        string& mayBeParent = folder[0];
        for (int i = 1; i < folder.size(); ++i) {
            string& curFolder = folder[i];
            bool sub = true;
            if (curFolder.length() > mayBeParent.length()) {
                if (curFolder[mayBeParent.length()] != '/') {
                    sub = false;
                }
                else if (curFolder.compare(0, mayBeParent.length(), mayBeParent) != 0) {
                    sub = false;
                }
            }
            else {
                sub = false;
            }

            if (false == sub) {
                ans.push_back(curFolder);
                mayBeParent = curFolder;
            }
        }

        return ans;
    }
};

/**
 * Original solution. The idea is:
 *      1. sub folder's length must be greater than parent folder
 *      2. sub folder contains parent folder. This means the are alike. We can sort the folders, so that similar
 *         folder names aggregates.
 */
class Solution {
public:
    void parentMap(const string& s, vector<int>& map) {
        map.clear();
        for (int i = 0; i < s.length(); ++i) {
            if ('/' == s[i]) {
                map.push_back(i);
            }
        }
        map.push_back(s.length());
    }

    bool isSubDir(const string& p1, const string& cur, vector<int>& map) {
        bool sub = true;
        for (int j = 1; j < map.size(); ++j) {
            if (false == sub) break;

            int k = map[j - 1] + 1;
            int end = map[j];
            if (cur[end] != '/') {
                sub = false;
                break;
            }

            for (; k < end; ++k) {
                if (cur[k] != p1[k]) {
                    sub = false;
                    break;
                }
            }
        }
        return sub;
    }

    vector<string> removeSubfolders(vector<string>& folder) {
        vector<string> ans;
        sort(folder.begin(), folder.end());
        ans.push_back(folder[0]);
        vector<int> map;
        parentMap(folder[0], map);

        for (int i = 1, pi = 0; i < folder.size(); ++i) {
            string& curFolder = folder[i];
            string& mayBeParent = folder[pi];
            bool sub = true;
            if (curFolder.length() > mayBeParent.length()) {
                sub = isSubDir(mayBeParent, curFolder, map);
            }
            else {
                sub = false;
            }

            if (false == sub) {
                ans.push_back(curFolder);
                pi = i;
                parentMap(curFolder, map);
            }
        }

        return ans;
    }
};
