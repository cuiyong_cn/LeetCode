/**
 * Two Sum IV - Input is a BST
 *
 * Given a Binary Search Tree and a target number, return true if there exist two elements in the BST such that their
 * sum is equal to the given target.
 *
 * Example 1:
 *
 * Input:
 *     5
 *    / \
 *   3   6
 *  / \   \
 * 2   4   7
 *
 * Target = 9
 *
 * Output: True
 *
 * Example 2:
 *
 * Input:
 *     5
 *    / \
 *   3   6
 *  / \   \
 * 2   4   7
 *
 * Target = 28
 *
 * Output: False
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/**
 * Improved one. Using two pointer to track.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<int> nums;

    bool findTarget(TreeNode* root, int k) {
        stack<TreeNode*> q;
        TreeNode* t = root;
        while (1) {
            while (nullptr != t) {
                q.push(t);
                t = t->left;
            }

            if (true == q.empty()) break;

            t = q.top();
            nums.push_back(t->val);
            q.pop();
            t = t->right;
        }

        bool ans = false;
        int hi = nums.size() - 1, lo = 0;
        while (lo < hi) {
            int sum = nums[hi] + nums[lo];
            if (sum == k) {
                ans = true;
                break;
            }
            if (sum > k) {
                --hi;
            }
            else {
                ++lo;
            }
        }

        return ans;
    }
};

/**
 * original solution. It works, but TLE.
 */
class Solution {
public:
    vector<int> nums;
    int target;

    bool dfs(int start, int sum, int count) {
        if (count > 2) return false;
        if (count == 2) {
            if (sum == target) return true;
            else return false;
        }

        bool ans = false;
        for (int j = start; j < nums.size(); ++j) {
            if (true == dfs(j + 1, sum + nums[j], count + 1)) {
                ans = true;
                break;
            }
            if (true == dfs(j + 1, sum, count)) {
                ans = true;
                break;
            }
        }
        return ans;
    }

    bool findTarget(TreeNode* root, int k) {
        stack<TreeNode*> q;
        target = k;
        TreeNode* t = root;
        while (1) {
            while (nullptr != t) {
                q.push(t);
                t = t->left;
            }

            if (true == q.empty()) break;

            t = q.top();
            nums.push_back(t->val);
            q.pop();
            t = t->right;
        }

        return dfs(0, 0, 0);
    }
};

/**
 * withou flat the BST.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    set<int> s;

    bool find(TreeNode* root, int k) {
        if (nullptr == root) return false;
        if (s.count(k - root->val)) return true;
        s.insert(root->val);
        return find(root->left, k) || find(root->right, k);
    }

    bool findTarget(TreeNode* root, int k) {
        return find(root, k);
    }
};
