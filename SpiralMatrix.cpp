/**
 * Spiral Matrix
 *
 * Given an m x n matrix, return all elements of the matrix in spiral order.
 *
 * Example 1:
 *
 * Input: matrix = [[1,2,3],[4,5,6],[7,8,9]]
 * Output: [1,2,3,6,9,8,7,4,5]
 * Example 2:
 *
 * Input: matrix = [[1,2,3,4],[5,6,7,8],[9,10,11,12]]
 * Output: [1,2,3,4,8,12,11,10,9,5,6,7]
 *
 * Constraints:
 *
 * m == matrix.length
 * n == matrix[i].length
 * 1 <= m, n <= 10
 * -100 <= matrix[i][j] <= 100
 */
/**
 * Without using the extra two dimension array.
 */
class Solution {
public:
    vector<int> spiralOrder(vector<vector<int>>& matrix) {
        int R = matrix.size();
        int C = matrix[0].size();
        vector<int> ans;

        for (int tlr = 0, tlc = 0, brr = R - 1, brc = C - 1;
                tlr <= brr && tlc <= brc;
                ++tlr, ++tlc, --brr, --brc) {
            // move right
            for (int c = tlc; c <= brc; ++c) {
                ans.push_back(matrix[tlr][c]);
            }

            // move down
            for (int r = tlr + 1; r <= brr; ++r) {
                ans.push_back(matrix[r][brc]);
            }

            if (tlr < brr && tlc < brc) {
                // move left
                for (int c = brc - 1; c > tlc; --c) {
                    ans.push_back(matrix[brr][c]);
                }
                // move up
                for (int r = brr; r > tlr; --r) {
                    ans.push_back(matrix[r][tlc]);
                }
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<int> spiralOrder(vector<vector<int>>& matrix) {
        int R = matrix.size();
        int C = matrix[0].size();
        vector<int> ans;
        vector<vector<bool>> visited(R, vector<bool>(C, false));
        int end = min(R / 2, C / 2);
        for (int i = 0; i <= end; ++i) {
            // move right
            for (int j = i; j < (C - i); ++j) {
                if (!visited[i][j]) {
                    visited[i][j] = true;
                    ans.push_back(matrix[i][j]);
                }
            }

            // move down
            for (int j = i + 1; j < (R - i); ++j) {
                if (!visited[j][C - i - 1]) {
                    visited[j][C - i - 1] = true;
                    ans.push_back(matrix[j][C - i - 1]);
                }
            }

            // move left
            for (int j = C - i - 2; j > i; --j) {
                if (!visited[R - i - 1][j]) {
                    visited[R - i - 1][j] = true;
                    ans.push_back(matrix[R - i - 1][j]);
                }
            }

            // move up
            for (int j = R - i - 1; j > i; --j) {
                if (!visited[j][i]) {
                    visited[j][i] = true;
                    ans.push_back(matrix[j][i]);
                }
            }
        }

        return ans;
    }
};
