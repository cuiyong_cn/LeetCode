/**
 * Get Biggest Three Rhombus Sums in a Grid
 *
 * You are given an m x n integer matrix grid.
 *
 * A rhombus sum is the sum of the elements that form the border of a regular rhombus shape in
 * grid. The rhombus must have the shape of a square rotated 45 degrees with each
 * of the corners centered in a grid cell. Below is an image of four valid rhombus shapes with the
 * corresponding colored cells that should be included in each rhombus sum:
 *
 * Note that the rhombus can have an area of 0, which is depicted by the purple rhombus in the bottom right corner.
 *
 * Return the biggest three distinct rhombus sums in the grid in descending order. If there are less than three distinct values, return all of them.
 *
 * Example 1:
 *
 * Input: grid = [[3,4,5,1,3],[3,3,4,2,3],[20,30,200,40,10],[1,5,5,4,1],[4,3,2,2,5]]
 * Output: [228,216,211]
 * Explanation: The rhombus shapes for the three biggest distinct rhombus sums are depicted above.
 * - Blue: 20 + 3 + 200 + 5 = 228
 * - Red: 200 + 2 + 10 + 4 = 216
 * - Green: 5 + 200 + 4 + 2 = 211
 * Example 2:
 *
 * Input: grid = [[1,2,3],[4,5,6],[7,8,9]]
 * Output: [20,9,8]
 * Explanation: The rhombus shapes for the three biggest distinct rhombus sums are depicted above.
 * - Blue: 4 + 2 + 6 + 8 = 20
 * - Red: 9 (area 0 rhombus in the bottom right corner)
 * - Green: 8 (area 0 rhombus in the bottom middle)
 * Example 3:
 *
 * Input: grid = [[7,7,7]]
 * Output: [7]
 * Explanation: All three possible rhombus sums are the same, so return [7].
 *
 * Constraints:
 *
 * m == grid.length
 * n == grid[i].length
 * 1 <= m, n <= 50
 * 1 <= grid[i][j] <= 105
 */
/**
 * Original solution. Although we can calculate the sum using diagonal presum.
 */
class Solution {
public:
    vector<int> getBiggestThree(vector<vector<int>>& grid) {
        int const M = grid.size();
        int const N = grid[0].size();
        int const ML = min((M + 1) >> 1, (N + 1) >> 1); // max length of the Rhombus

        int m1 = -1;
        int m2 = -1;
        int m3 = -1;

        // Iterate through every Rhombus of length L
        for (int L = 1; L <= ML; ++L) {
            // The limit of the top coordinate
            int const top = M - 2 * (L - 1);
            int const left = L - 1;
            int const right = N - L;

            for (int i = 0; i < top; ++i) {
                for (int j = left; j <= right; ++j) {
                    auto sum = grid[i][j];
                    int k = 0;
                    int d = 2;
                    int left_x = i + 1;
                    int left_y = j - 1;
                    for (; k < left; ++k) {
                        sum += grid[left_x][left_y] + grid[left_x][left_y + d];
                        ++left_x;
                        --left_y;
                        d += 2;
                    }

                    left_y += 2;
                    d -= 4;

                    for (; k > 1; --k) {
                        sum += grid[left_x][left_y] + grid[left_x][left_y + d];
                        ++left_x;
                        ++left_y;
                        d -= 2;
                    }

                    if (L > 1) {
                        sum += grid[left_x][left_y];
                    }

                    if (sum > m1) {
                        m3 = m2;
                        m2 = m1;
                        m1 = sum;
                    }
                    else if (sum < m1 && sum > m2) {
                        m3 = m2;
                        m2 = sum;
                    }
                    else if (sum < m2 && sum > m3) {
                        m3 = sum;
                    }
                }
            }
        }

        vector<int> ans = {m1, m2, m3};
        while (ans.back() < 0) {
            ans.pop_back();
        }

        return ans;
    }
};
