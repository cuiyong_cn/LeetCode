/**
 * Palindromic Substrings
 *
 * Given a string, your task is to count how many palindromic substrings in this string.
 *
 * The substrings with different start indexes or end indexes are counted as different
 * substrings even they consist of same characters.
 *
 * Example 1:
 *
 * Input: "abc"
 * Output: 3
 * Explanation: Three palindromic strings: "a", "b", "c".
 *
 * Example 2:
 *
 * Input: "aaa"
 * Output: 6
 * Explanation: Six palindromic strings: "a", "a", "a", "aa", "aa", "aaa".
 *
 * Note:
 *
 *     The input string length won't exceed 1000.
 */
/**
 * Use the Manacher's Algorithm. Need to look at the paper.
 */
class Solution {
public:
    int countSubstrings(string s) {
        int ans = 0;
        string t("@#");

        for (auto c : s) {
            t += c;
            t += '#';
        }
        t += '$';

        vector<int> p(t.size(), 0);
        int center = 0, right = 0;
        for (int i = 1; i < p.size() - 1; ++i) {
            if (i < right) {
                p[i] = min(right - i, p[2 * center - i]);
            }
            while (t[i + p[i] + 1] == t[i - p[i] - 1]) {
                ++p[i];
            }
            if ((i + p[i]) > right) {
                center = i;
                right = i + p[i];
            }
        }

        for (auto n : p) {
            ans += (n + 1) / 2;
        }
        return ans;
    }
};
/**
 * Center expansion approach. The idea is:
 *           x x x x x x x x x x
 *           ^^^^^^^^^^^^^^^^^^^
 * The middle of the palindrome can be in one of the 2 * N - 1 positions above.
 *
 * This approach is more efficient because it does not need to extract the substring.
 */
class Solution {
public:
    int countSubstrings(string s) {
        int strSize = s.size();
        int ans = 0;
        for (int c = 0; c <= 2 * strSize - 1; ++c) {
            int left = c / 2;
            int right = left + c % 2;
            while (left >= 0 && right < strSize && s[left] == s[right]) {
                ++ans; --left; ++right;
            }
        }

        return ans;
    }
};
/**
 * Brute force approach. simple
 */
class Solution {
private:
    bool isPanlindrome(const string& s) {
        int i = 0;
        int j = s.size() - 1;
        bool panlindrome = true;
        while (i < j) {
            if (s[i] != s[j]) {
                panlindrome = false;
                break;
            }
            ++i; --j;
        }

        return panlindrome;
    }

public:
    int countSubstrings(string s) {
        int strSize = s.size();
        int ans = strSize;
        for (int i = 0; i < strSize; ++i) {
            string substr;
            substr += s[i];
            for (int j = i + 1; j < strSize; ++j) {
                substr += s[j];
                if (isPanlindrome(substr)) ++ans;
            }
        }

        return ans;
    }
};
