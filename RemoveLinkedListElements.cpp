/**
 * Remove Linked List Elements
 *
 * Given the head of a linked list and an integer val, remove all the nodes of the linked list that
 * has Node.val == val, and return the new head.
 *
 * Example 1:
 *
 * Input: head = [1,2,6,3,4,5,6], val = 6
 * Output: [1,2,3,4,5]
 * Example 2:
 *
 * Input: head = [], val = 1
 * Output: []
 * Example 3:
 *
 * Input: head = [7,7,7,7], val = 7
 * Output: []
 *
 * Constraints:
 *
 * The number of nodes in the list is in the range [0, 104].
 * 1 <= Node.val <= 50
 * 0 <= k <= 50
 */
/**
 * Non-recursive version.
 */
class Solution {
public:
    ListNode* removeElements(ListNode* head, int val) {
        ListNode dummy;
        auto cur = &dummy;
        while (head) {
            if (head->val != val) {
                cur->next = head;
                cur = head;
            }
            head = head->next;
        }
        cur->next = nullptr;

        return dummy.next;
    }
};

/**
 * Recursive version.
 */
class Solution {
public:
    ListNode* removeElements(ListNode* head, int val) {
        if (nullptr == head) {
            return nullptr;
        }

        if (head->val == val) {
            return removeElements(head->next, val);
        }

        head->next = removeElements(head->next, val);
        return head;
    }
};

/**
 * Original solution.
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* removeElements(ListNode* head, int val) {
        vector<ListNode*> nodes;
        while (head) {
            nodes.push_back(head);
            head = head->next;
        }

        nodes.erase(remove_if(begin(nodes), end(nodes),
                           [val](auto n1) {
                               return n1->val == val;
                           }), end(nodes));

        if (nodes.empty()) {
            return nullptr;
        }

        for (size_t i = 1; i <  nodes.size(); ++i) {
            nodes[i - 1]->next = nodes[i];
        }

        nodes.back()->next = nullptr;
        return nodes.front();
    }
};
