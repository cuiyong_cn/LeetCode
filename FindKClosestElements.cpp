/**
 * Find K Closest Elements
 *
 * Given a sorted integer array arr, two integers k and x, return the k closest integers to x in the
 * array. The result should also be sorted in ascending order.
 *
 * An integer a is closer to x than an integer b if:
 *
 * |a - x| < |b - x|, or
 * |a - x| == |b - x| and a < b
 *
 * Example 1:
 *
 * Input: arr = [1,2,3,4,5], k = 4, x = 3
 * Output: [1,2,3,4]
 * Example 2:
 *
 * Input: arr = [1,2,3,4,5], k = 4, x = -1
 * Output: [1,2,3,4]
 *
 * Constraints:
 *
 * 1 <= k <= arr.length
 * 1 <= arr.length <= 104
 * arr is sorted in ascending order.
 * -104 <= arr[i], x <= 104
 */
/**
 * Binary search + two pointer solution.
 */
class Solution {
public:
    vector<int> findClosestElements(vector<int>& arr, int k, int x) {
        if (x < arr.front()) {
            return {begin(arr), begin(arr) + k};
        }

        if (x > arr.back()) {
            return {end(arr) - k, end(arr)};
        }

        int n = arr.size();
        int idx = distance(begin(arr), lower_bound(begin(arr), end(arr), x));
        int low = max(0, idx - k - 1);
        int high = min(n - 1, idx + k - 1);
        while ((high - low + 1) > k) {
            if (low < 0 || (x - arr[low]) <= (arr[high] - x)) {
                --high;
            }
            else if (high > (n - 1) || (x - arr[low]) > (arr[high] - x)) {
                ++low;
            }
        }

        return {begin(arr) + low, begin(arr) + high + 1};
    }
};

/**
 * Improve below solution. Reduce space usage.
 */
class Solution {
public:
    vector<int> findClosestElements(vector<int>& arr, int k, int x) {
        sort(begin(arr), end(arr),
            [x](auto a, auto b) {
                auto diff = abs(a - x) - abs(b - x);
                if (diff < 0) {
                    return true;
                }

                if (0 == diff) {
                    return a < b;
                }

                return false;
            });

        sort(begin(arr), begin(arr) + k);

        return {begin(arr), begin(arr) + k};
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<int> findClosestElements(vector<int>& arr, int k, int x) {
        vector<pair<int, int>> diffset;

        for (auto n : arr) {
            diffset.push_back(make_pair(abs(n - x), n));
        }

        sort(begin(diffset), end(diffset),
            [](auto& a, auto& b) {
                if (a.first < b.first) {
                    return true;
                }

                if (a.first == b.first) {
                    return a.second < b.second;
                }

                return false;
            });

        vector<int> ans(k, 0);
        for (int i = 0; i < k; ++i) {
            ans[i] = diffset[i].second;
        }

        sort(begin(ans), end(ans));

        return ans;
    }
};
