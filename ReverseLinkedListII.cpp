/**
 * Reverse Linked List II
 *
 * Given the head of a singly linked list and two integers left and right where left <= right,
 * reverse the nodes of the list from position left to position right, and return the reversed list.
 *
 * Example 1:
 *
 * Input: head = [1,2,3,4,5], left = 2, right = 4
 * Output: [1,4,3,2,5]
 * Example 2:
 *
 * Input: head = [5], left = 1, right = 1
 * Output: [5]
 *
 * Constraints:
 *
 * The number of nodes in the list is n.
 * 1 <= n <= 500
 * -500 <= Node.val <= 500
 * 1 <= left <= right <= n
 */
/**
 * Iterative method. One pass.
 */
class Solution {
public:
    ListNode* reverseBetween(ListNode* head, int L, int R) {
        ListNode* cur = head;
        ListNode* prev = nullptr;

        while (L > 1) {
            prev = cur;
            cur = cur->next;
            --L;
            --R;
        }

        auto tail = cur, con = prev;
        while (R > 0) {
            auto next_node = cur->next;
            cur->next = prev;
            prev = cur;
            cur = next_node;
            --R;
        }

        if (nullptr != con) {
            con->next = prev;
        }
        else {
            head = prev;
        }

        tail->next = cur;

        return head;
    }
};

/**
 * Recursive solution. But I prefer non-recursive version.
 */
class Solution {
public:
    ListNode* reverseBetween(ListNode* head, int L, int R) {
        left = head;
        stop = false;

        recurseAndReverse(head, L, R);

        return head;
    }

private:
    void recurseAndReverse(ListNode* right, int m, int n)
    {
        if (1 == n) {
            return;
        }

        right = right->next;

        if (m > 1) {
            left = left->next;
        }

        recurseAndReverse(right, m - 1, n - 1);

        if (left == right || right->next == left) {
            stop = true;
        }

        if (!stop) {
            swap(left->val, right->val);
            left = left->next;
        }
    }

    ListNode* left;
    bool stop;
};

/**
 * Original solution.
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* reverseBetween(ListNode* head, int left, int right) {
        vector<ListNode*> nodes;
        while (head) {
            nodes.emplace_back(head);
            head = head->next;
        }

        reverse(begin(nodes) + (left - 1), begin(nodes) + right);

        ListNode dummy;
        auto prev = &dummy;
        for (auto n : nodes) {
            prev->next = n;
            prev = n;
        }

        prev->next = nullptr;

        return dummy.next;
    }
};
