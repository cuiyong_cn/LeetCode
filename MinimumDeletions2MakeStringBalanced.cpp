/**
 * Minimum Deletions to Make String Balanced
 *
 * You are given a string s consisting only of characters 'a' and 'b'​​​​.
 *
 * You can delete any number of characters in s to make s balanced. s is balanced if there is no pair of indices (i,j)
 * such that i < j and s[i] = 'b' and s[j]= 'a'.
 *
 * Return the minimum number of deletions needed to make s balanced.
 *
 * Example 1:
 *
 * Input: s = "aababbab"
 * Output: 2
 * Explanation: You can either:
 * Delete the characters at 0-indexed positions 2 and 6 ("aababbab" -> "aaabbb"), or
 * Delete the characters at 0-indexed positions 3 and 6 ("aababbab" -> "aabbbb").
 * Example 2:
 *
 * Input: s = "bbaaaaabb"
 * Output: 2
 * Explanation: The only solution is to delete the first two characters.
 *
 * Constraints:
 *
 * 1 <= s.length <= 105
 * s[i] is 'a' or 'b'​​.
 */
/**
 * Finally from discussion. Found a really simple one with easily understandable
 * explaination.
 */
/**
 * Intuition: the problem description says: no 'a' after any 'b'. A stack can help you to keep track of how many 'b's
 * have already come along --> that is the maximum cost you have to pay to make the string balanced. Concretely: you
 * will only have to delete all previous 'b's if at least as many 'a's come along.  However, a simple count of 'b's is
 * more space efficient (O(1) instead of O(n)):
 */
class Solution {
public:
    int minimumDeletions(string s) {
        int ans = 0;
        int b = 0;
        for (auto c : s) {
            if ('a' == c && b > 0) {
                ++ans;
                --b;
            }
            else if ('b' == c) {
                ++b;
            }
        }

        return ans;
    }
};

/**
 * A stack version.
 */
class Solution {
public:
    //Idea is to traverse from right to left & use stack to keep track if 'b' comes before 'a' in string
    //then we need to pop & need to delete that character
    int minimumDeletions(string s) {
        stack<char> st;
        int n =s.size();
        int res = 0;

        for(int i = n-1; i >=0; i--){
            char c = s[i];
            if(!st.empty() && st.top() < c){
                res++;
                st.pop();
            }else{
                st.push(c);
            }
        }

        return res;
    }
};

/**
 * From discussion. Found the below solution.
 * Frankly speaking, we dont't get the decision part. why chould smaller
 * direction?
 */
class Solution {
public:
    int minimumDeletions(string s) {
        int a = 0, b = 0, l = 0, r = s.size() - 1, res = 0;
        for (auto ch : s) {
            a += ch == 'a';
            b += ch == 'b';
        }
        while (l < r) {
            if (a && s[l] == 'a') {
                ++l;
                --a;
            }
            else if (b && s[r] == 'b') {
                --r;
                --b;
            }
            else {
                ++res;
                if (a < b) {
                    --a;
                    --r;
                }
                else {
                    --b;
                    ++l;
                }
            }
        }
        return res;
    }
};

/**
 * Improve the code.
 */
class Solution {
public:
    int minimumDeletions(string s) {
        int a = count(begin(s), end(s), 'a'), b = 0, res = a;
        for (auto ch : s) {
            b += ch == 'b';
            a -= ch == 'a';
            res = min(res, b + a);
        }
        return res;
    }
};

/**
 * From discussion. A not very efficient one. But more easyily understandable. Wrriten in python.
 * Idea is:
 * The intuition is that, let's say you have "aababbab"
 *
 * indices of all the 'a'
 * A = [0,1,3,6]
 * indices of all the 'b'
 * B = [2,4,5,7]
 * We want to delete minimum number of numbers from the tail of A or/and the head of B, to make all the numbers in B are
 * larger than A.
 * i.e. A = [0,1,3], B = [4,5,7]
 * Since the indices are sorted, we can loop over A and binary search a point where B[j] > A[i] in B, then you know what
 * to do :)
 */
# Runtime: 3816 ms
class Solution(object):
    def minimumDeletions(self, s):
        A = [-1]  # consider 'bbba', u want to delete the a instead of bbb
        B = []
        for i in range(len(s)):
            c = s[i]
            if c == 'a':
                A.append(i)
            else:
                B.append(i)
        res = 2**31 - 1
        for i in range(len(A)):
            a = A[i]
            j = self.upperBoundbsearch(B, a)
            toDeleteInA = len(A) - i - 1
            toDeleteInB = j
            charsToDelete = toDeleteInA + toDeleteInB
            res = min(res, charsToDelete)
        if res == 2**31 - 1:  # or sys.maxsize / 'inf', whatever
            return 0
        return res

    # or python bisect right, whatever
    def upperBoundbsearch(self, nums, target):
        left = 0
        right = len(nums)
        while left < right:
            mid = (left + right) // 2
            if target >= nums[mid]:
                left = mid + 1
            else:
                right = mid
        return left


/**
 * Original solution. The idea is to find the split point.
 */
class Solution {
public:
    int minimumDeletions(string s) {
        vector<pair<int, int>> info(s.length(), make_pair(0, 0));

        int prev = 0;
        for (int i = 0; i < s.length(); ++i) {
            info[i].first = 'b' == s[i] ? prev++ : prev;
        }

        prev = 0;
        for (int i = s.length() - 1; i >= 0; --i) {
            info[i].second = 'a' == s[i] ? prev++ : prev;
        }

        int ans = s.length();
        for (int i = 0; i < info.size(); ++i) {
            ans = std::min(ans, info[i].first + info[i].second);
        }

        return ans == s.length() ? 0 : ans;
    }
};
