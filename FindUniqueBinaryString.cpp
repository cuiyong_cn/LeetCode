/**
 * Find Unique Binary String
 *
 * Given an array of strings nums containing n unique binary strings each of length n, return a
 * binary string of length n that does not appear in nums. If there are multiple answers, you may
 * return any of them.
 *
 * Example 1:
 *
 * Input: nums = ["01","10"]
 * Output: "11"
 * Explanation: "11" does not appear in nums. "00" would also be correct.
 * Example 2:
 *
 * Input: nums = ["00","01"]
 * Output: "11"
 * Explanation: "11" does not appear in nums. "10" would also be correct.
 * Example 3:
 *
 * Input: nums = ["111","011","001"]
 * Output: "101"
 * Explanation: "101" does not appear in nums. "000", "010", "100", and "110" would also be correct.
 *
 * Constraints:
 *
 * n == nums.length
 * 1 <= n <= 16
 * nums[i].length == n
 * nums[i] is either '0' or '1'.
 * All the strings of nums are unique.
 */
/**
 * More efficient solution.
 */
class Solution {
public:
    string findDifferentBinaryString(vector<string>& nums) {
        for (int i = 0; i < nums.size(); ++i) {
            nums[0][i] = nums[i][i] == '0' ? '1' : '0';
        }

        return nums[0];
    }
};

/**
 * Original solution.
 */
void next(string& bin)
{
    auto const size = bin.length();
    for (int i = size - 1; i >= 0; --i) {
        if ('1' == bin[i]) {
            bin[i] = '0';
        }
        else {
            bin[i] = '1';
            break;
        }
    }
}

class Solution {
public:
    string findDifferentBinaryString(vector<string>& nums) {
        unordered_set<string> bin_sets(nums.begin(), nums.end());

        string bin(nums.size(), '0');
        while (true) {
            if (0 == bin_sets.count(bin)) {
                break;
            }
            next(bin);
        }

        return bin;
    }
};
