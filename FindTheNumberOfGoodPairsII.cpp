/**
 * Find the Number of Good Pairs II
 *
 * You are given 2 integer arrays nums1 and nums2 of lengths n and m respectively. You are also
 * given a positive integer k.
 *
 * A pair (i, j) is called good if nums1[i] is divisible by nums2[j] * k (0 <= i <= n - 1, 0 <= j <=
 * m - 1).
 *
 * Return the total number of good pairs.
 *
 * Example 1:
 *
 * Input: nums1 = [1,3,4], nums2 = [1,3,4], k = 1
 *
 * Output: 5
 *
 * Explanation:
 * The 5 good pairs are (0, 0), (1, 0), (1, 1), (2, 0), and (2, 2).
 *
 * Example 2:
 *
 * Input: nums1 = [1,2,4,12], nums2 = [2,4], k = 3
 *
 * Output: 2
 *
 * Explanation:
 *
 * The 2 good pairs are (3, 0) and (3, 1).
 *
 * Constraints:
 *     1 <= n, m <= 105
 *     1 <= nums1[i], nums2[j] <= 106
 *     1 <= k <= 103
 */
/**
 * Passed one.
 */
class Solution {
public:
    long long numberOfPairs(vector<int>& nums1, vector<int>& nums2, int k) {
        auto factor_cnt = unordered_map<int, int>{};

        for(auto n : nums1){
            auto end = sqrt(n);

            for (auto i = 1; i <= end; ++i) {
                if ((i * i) == n) {
                    ++factor_cnt[i];
                }
                else if (0 == (n % i)) {
                    ++factor_cnt[i];
                    ++factor_cnt[n / i];
                }
            }
        }

        auto ans = 0LL;

        for(auto n : nums2){
            if (factor_cnt.count(n * k)) {
                ans += factor_cnt[n * k];
            }
        }

        return ans;
    }
};

/**
 * Original solution. TLE.
 */
class Solution {
public:
    long long numberOfPairs(vector<int>& nums1, vector<int>& nums2, int k) {
        auto ans = 0LL;
        auto cnt = unordered_map<int, int>{};

        for (auto n : nums2) {
            ++cnt[n * k];
        }

        for (auto n : nums1) {
            for (auto const& [d, c] : cnt) {
                if (0 == (n % d)) {
                    ans += c;
                }
            }
        }

        return ans;
    }
};
