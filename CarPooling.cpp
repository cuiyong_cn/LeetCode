/**
 * Car Pooling
 *
 * You are driving a vehicle that has capacity empty seats initially available for passengers.  The vehicle only drives
 * east (ie. it cannot turn around and drive west.)
 *
 * Given a list of trips, trip[i] = [num_passengers, start_location, end_location] contains information about the i-th
 * trip: the number of passengers that must be picked up, and the locations to pick them up and drop them off.  The
 * locations are given as the number of kilometers due east from your vehicle's initial location.
 *
 * Return true if and only if it is possible to pick up and drop off all passengers for all the given trips.
 *
 * Example 1:
 *
 * Input: trips = [[2,1,5],[3,3,7]], capacity = 4
 * Output: false
 *
 * Example 2:
 *
 * Input: trips = [[2,1,5],[3,3,7]], capacity = 5
 * Output: true
 *
 * Example 3:
 *
 * Input: trips = [[2,1,5],[3,5,7]], capacity = 3
 * Output: true
 *
 * Example 4:
 *
 * Input: trips = [[3,2,7],[3,7,9],[8,3,9]], capacity = 11
 * Output: true
 *
 * Constraints:
 *
 *     trips.length <= 1000
 *     trips[i].length == 3
 *     1 <= trips[i][0] <= 100
 *     0 <= trips[i][1] < trips[i][2] <= 1000
 *     1 <= capacity <= 100000
 */
/**
 * Improved one, we don't need two map. Only one is sufficient.
 */
class Solution {
public:
    bool carPooling(vector<vector<int>>& trips, int capacity) {
        map<int, int> m;
        for (auto &t : trips) {
            m[t[1]] += t[0];
            m[t[2]] -= t[0];
        }

        bool finishTrip = true;
        for (auto &v : m) {
            if ((capacity -= v.second) < 0) {
                finishTrip = false;
                break;
            }
        }

        return finishTrip;
    }
};
/**
 * My original solution. Using two maps pickup and drop off.
 */
class Solution {
public:
    bool carPooling(vector<vector<int>>& trips, int capacity) {
        map<int, vector<int> > pickup;
        map<int, vector<int> > dropoff;
        for (auto& trip : trips) {
            pickup[trip[1]].push_back(trip[0]);
            dropoff[trip[2]].push_back(trip[0]);
        }

        bool finishTrip = true;
        int passengers = 0;
        auto drop = dropoff.begin();
        for (auto& it : pickup) {
            int pos = it.first;

            while (drop->first <= pos) {
                for (auto& d : drop->second) {
                    passengers -= min(passengers, d);
                }
                ++drop;
            }

            for (auto& p : it.second) {
                passengers += p;
            }

            if (passengers > capacity) {
                finishTrip = false;
                break;
            }
        }

        return finishTrip;
    }
};
