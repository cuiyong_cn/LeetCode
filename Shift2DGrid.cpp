/**
 * Shift 2D Grid
 *
 * Given a 2D grid of size n * m and an integer k. You need to shift the grid k times.
 *
 * In one shift operation:
 *
 *     Element at grid[i][j] becomes at grid[i][j + 1].
 *     Element at grid[i][m - 1] becomes at grid[i + 1][0].
 *     Element at grid[n - 1][m - 1] becomes at grid[0][0].
 *
 * Return the 2D grid after applying shift operation k times.
 *
 * Example 1:
 *
 * Input: grid = [[1,2,3],[4,5,6],[7,8,9]], k = 1
 * Output: [[9,1,2],[3,4,5],[6,7,8]]
 *
 * Example 2:
 *
 * Input: grid = [[3,8,1,9],[19,7,2,5],[4,6,11,10],[12,0,21,13]], k = 4
 * Output: [[12,0,21,13],[3,8,1,9],[19,7,2,5],[4,6,11,10]]
 *
 * Example 3:
 *
 * Input: grid = [[1,2,3],[4,5,6],[7,8,9]], k = 9
 * Output: [[1,2,3],[4,5,6],[7,8,9]]
 *
 * Constraints:
 *
 *     1 <= grid.length <= 50
 *     1 <= grid[i].length <= 50
 *     -1000 <= grid[i][j] <= 1000
 *     0 <= k <= 100
 */
/**
 * Improved version, only swap all cols once.
 */
class Solution {
private:
    void rowRotate(vector<vector<int>>& grid, int startCol, int dist) {
        if (0 == dist) return;

        int rows = grid.size();
        int cols = grid[0].size();
        bool divsible = (rows % dist) == 0 ? true : false;
        for (int i = startCol; i < cols; ++i) {
            int prev = 0;
            int fixed = 0;
            for (int j = 0; j < rows; ++j) {
                int sr = (prev + dist) % rows;
                if (sr == fixed) {
                    ++fixed;
                    prev = fixed;
                }
                else {
                    swap(grid[fixed][i], grid[sr][i]);
                    prev = sr;
                }
            }
        }
    }

    void swapCol(vector<vector<int>>& grid, int c1, int c2) {
        int rows = grid.size();
        for (int i = 0; i < rows; ++i) {
            swap(grid[i][c1], grid[i][c2]);
        }
    }
public:
    vector<vector<int>> shiftGrid(vector<vector<int>>& grid, int k) {
        int rows = grid.size();
        int cols = grid[0].size();
        int repeatTimes = k / cols;
        int col_dist = k % cols;
        int row_dist = repeatTimes % rows;
        rowRotate(grid, 0, row_dist);
        rowRotate(grid, cols - col_dist, 1);

        int prev = 0;
        int fixed = 0;
        for (int i = 0; i < cols; ++i) {
            int sc = (prev + col_dist) % cols;
            if (fixed == sc) {
                ++fixed;
                prev = fixed;
            }
            else {
                swapCol(grid, fixed, sc);
                prev = sc;
            }
        }
        return grid;
    }
};
/**
 * My original version.
 */
class Solution {
private:
    void botRow2FirstRow(vector<vector<int>>& grid, int startCol) {
        int rows = grid.size();
        int cols = grid[0].size();
        for (int i = startCol; i < cols; ++i) {
            for (int j = rows - 1; j > 0; --j) {
                swap(grid[j][i], grid[j - 1][i]);
            }
        }
    }

    void swapCol(vector<vector<int>>& grid, int c1, int c2) {
        int rows = grid.size();
        for (int i = 0; i < rows; ++i) {
            swap(grid[i][c1], grid[i][c2]);
        }
    }
public:
    vector<vector<int>> shiftGrid(vector<vector<int>>& grid, int k) {
        int rows = grid.size();
        int cols = grid[0].size();
        int repeatTimes = k / cols;
        int remain = k % cols;
        int rowRotateTimes = repeatTimes % rows;
        for (int i = 0; i < rowRotateTimes; ++i) {
            botRow2FirstRow(grid, 0);
        }

        botRow2FirstRow(grid, cols - remain);

        for (int i = remain; i > 0; --i) {
            for (int j = cols - 1; j > 0; --j) {
                swapCol(grid, j, j - 1);
            }
        }
        return grid;
    }
};
