/**
 * Maximum Product of the Length of Two Palindromic Subsequences
 *
 * Given a string s, find two disjoint palindromic subsequences of s such that the product of their
 * lengths is maximized. The two subsequences are disjoint if they do not both pick a character at
 * the same index.
 *
 * Return the maximum possible product of the lengths of the two palindromic subsequences.
 *
 * A subsequence is a string that can be derived from another string by deleting some or no
 * characters without changing the order of the remaining characters. A string is palindromic if it
 * reads the same forward and backward.
 *
 * Example 1:
 *
 * example-1
 * Input: s = "leetcodecom"
 * Output: 9
 * Explanation: An optimal solution is to choose "ete" for the 1st subsequence and "cdc" for the 2nd subsequence.
 * The product of their lengths is: 3 * 3 = 9.
 * Example 2:
 *
 * Input: s = "bb"
 * Output: 1
 * Explanation: An optimal solution is to choose "b" (the first character) for the 1st subsequence and "b" (the second character) for the 2nd subsequence.
 * The product of their lengths is: 1 * 1 = 1.
 * Example 3:
 *
 * Input: s = "accbcaxxcxx"
 * Output: 25
 * Explanation: An optimal solution is to choose "accca" for the 1st subsequence and "xxcxx" for the 2nd subsequence.
 * The product of their lengths is: 5 * 5 = 25.
 *
 * Constraints:
 *
 * 2 <= s.length <= 12
 * s consists of lowercase English letters only.
 */
/**
 * Pretty efficient one.
 */

class Solution {
public:
    int palindrome_length(string const& s, int mk)
    {
        bitset<12> mask(mk);
        int left = 0, right = 11;

        while (left < right) {
            while (left < 11 && !mask[left]) {
                ++left;
            }

            while (right >= 0 && !mask[right]) {
                --right;
            }

            if (s[left] != s[right]) {
                return 0;
            }
            ++left;
            --right;
        }

        return mask.count();
    }

    int maxProduct(string s) {
        int dp[4096] = {0, };
        int ans = 0;
        int total = (1 << s.length()) - 1;

        for (int mask = 1; mask <= total; ++mask) {
            dp[mask] = palindrome_length(s, mask);
        }

        for (int m1 = total; m1 > 0; --m1) {
            if (dp[m1] * (s.size() - dp[m1]) <= ans) {
                continue;
            }

            for (int m2 = total ^ m1; m2 > 0; m2 = (m2 - 1) & (total ^ m1)) {
                ans = max(ans, dp[m1] * dp[m2]);
            }
        }

        return ans;
    }
};

/**
 * I didn't come up with a decent and simple solution.
 * So here is the simple and intuitive solution from Discussion.
 */
class Solution {
public:
    bool is_panlindrome(string const& s)
    {
        return equal(s.begin(), s.begin() + s.length() / 2, s.rbegin());
    }

    void dfs(string const& s, int i)
    {
        if (i == s.length()) {
            if (is_panlindrome(left) && is_panlindrome(right)) {
                ans = max<int>(ans, left.length() * right.length());
            }
            return;
        }

        left.push_back(s[i]);
        dfs(s, i+1);
        left.pop_back();

        right.push_back(s[i]);
        dfs(s, i+1);
        right.pop_back();

        dfs(s, i+1);
    }

    int maxProduct(string s)
    {
        dfs(s, 0);

        return ans;
    }

private:
    int ans = 0;
    string left;
    string right;
};
