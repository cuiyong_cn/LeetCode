/**
 * Restore the Array From Adjacent Pairs
 *
 * There is an integer array nums that consists of n unique elements, but you have forgotten it.
 * However, you do remember every pair of adjacent elements in nums.
 *
 * You are given a 2D integer array adjacentPairs of size n - 1 where each adjacentPairs[i] = [ui,
 * vi] indicates that the elements ui and vi are adjacent in nums.
 *
 * It is guaranteed that every adjacent pair of elements nums[i] and nums[i+1] will exist in
 * adjacentPairs, either as [nums[i], nums[i+1]] or [nums[i+1], nums[i]]. The pairs can appear in
 * any order.
 *
 * Return the original array nums. If there are multiple solutions, return any of them.
 *
 * Example 1:
 *
 * Input: adjacentPairs = [[2,1],[3,4],[3,2]]
 * Output: [1,2,3,4]
 * Explanation: This array has all its adjacent pairs in adjacentPairs.
 * Notice that adjacentPairs[i] may not be in left-to-right order.
 * Example 2:
 *
 * Input: adjacentPairs = [[4,-2],[1,4],[-3,1]]
 * Output: [-2,4,1,-3]
 * Explanation: There can be negative numbers.
 * Another solution is [-3,1,4,-2], which would also be accepted.
 * Example 3:
 *
 * Input: adjacentPairs = [[100000,-100000]]
 * Output: [100000,-100000]
 *
 * Constraints:
 *
 * nums.length == n
 * adjacentPairs.length == n - 1
 * adjacentPairs[i].length == 2
 * 2 <= n <= 105
 * -105 <= nums[i], ui, vi <= 105
 * There exists some nums that has adjacentPairs as its pairs.
 */
/**
 * Without the extra hassle.
 */
class Solution {
public:
    vector<int> restoreArray(vector<vector<int>>& adjacentPairs) {
        unordered_map<int, vector<int>> edge;

        for (auto const& ap : adjacentPairs) {
            edge[ap[0]].emplace_back(ap[1]);
            edge[ap[1]].emplace_back(ap[0]);
        }

        vector<int> ans;
        for (auto const& e : edge) {
            if (1 == e.second.size()) {
                ans = { e.first, e.second.front() };
                break;
            }
        }

        auto const N = adjacentPairs.size() + 1;
        while (ans.size() < N) {
            auto tail = ans.back();
            auto prev = ans[ans.size() - 2];
            auto const& next = edge[tail];
            if (next.front() == prev) {
                ans.emplace_back(next.back());
            }
            else {
                ans.emplace_back(next.front());
            }
        }

        return ans;
    }
};

/**
 * Original solution. BFS solution.
 */
class Solution {
public:
    vector<int> restoreArray(vector<vector<int>>& adjacentPairs) {
        unordered_map<int, vector<int>> edge;

        for (auto const& ap : adjacentPairs) {
            edge[ap[0]].emplace_back(ap[1]);
            edge[ap[1]].emplace_back(ap[0]);
        }

        vector<int> ans;
        for (auto const& e : edge) {
            if (1 == e.second.size()) {
                ans = { e.first, e.second.front() };
                break;
            }
        }

        unordered_set<int> seen = {ans.front()};
        deque<int> q = {ans.back()};

        while (!q.empty()) {
            int const N = q.size();
            for (int i = 0; i < N; ++i) {
                auto const num = q.front();
                q.pop_front();
                if (seen.count(num)) {
                    continue;
                }

                seen.insert(num);
                for (auto const& e : edge[num]) {
                    if (0 == seen.count(e)) {
                        ans.emplace_back(e);
                        q.emplace_back(e);
                    }
                }
            }
        }

        return ans;
    }
};
