/**
 * Deepest Leaves Sum
 *
 * Given the root of a binary tree, return the sum of values of its deepest leaves.
 *
 * Example 1:
 *
 * Input: root = [1,2,3,4,5,null,6,7,null,null,null,null,8]
 * Output: 15
 * Example 2:
 *
 * Input: root = [6,7,8,2,7,1,3,9,null,1,4,null,null,null,5]
 * Output: 19
 *
 * Constraints:
 *
 * The number of nodes in the tree is in the range [1, 104].
 * 1 <= Node.val <= 100
 */
/**
 * BFS solution.
 */

class Solution {
public:
    int deepestLeavesSum(TreeNode* root) {
        int ans = 0;
        queue<TreeNode*> q{queue<TreeNode*>::container_type{root}};
        while (!q.empty()) {
            ans = 0;
            for (int i = q.size() - 1; i >= 0; --i) {
                auto node = q.front(); q.pop();
                ans += node->val;
                if (node->right) {
                    q.push(node->right);
                }
                if (node->left) {
                    q.push(node->left);
                }
            }
        }

        return ans;
    }
};

/**
 * Original DFS solution.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int deepestLeavesSum(TreeNode* root) {
        int const H = tree_height(root);
        return sum_at_height(root, H - 1);
    }

private:
    int tree_height(TreeNode* node)
    {
        if (nullptr == node) {
            return 0;
        }

        int left_height = tree_height(node->left);
        int right_height = tree_height(node->right);

        return max(left_height, right_height) + 1;
    }

    int sum_at_height(TreeNode* node, int height)
    {
        if (nullptr == node || 0 == height) {
            return nullptr == node ? 0 : node->val;
        }

        return sum_at_height(node->left, height - 1) + sum_at_height(node->right, height - 1);
    }
};
