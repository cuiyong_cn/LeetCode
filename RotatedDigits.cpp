/**
 * Rotated Digits
 *
 * X is a good number if after rotating each digit individually by 180 degrees, we get a valid number that is
 * different from X.  Each digit must be rotated - we cannot choose to leave it alone.
 *
 * A number is valid if each digit remains a digit after rotation. 0, 1, and 8 rotate to themselves; 2 and 5 rotate to
 * each other; 6 and 9 rotate to each other, and the rest of the numbers do not rotate to any other number and
 * become invalid.
 *
 * Now given a positive number N, how many numbers X from 1 to N are good?
 *
 * Example:
 * Input: 10
 * Output: 4
 * Explanation:
 * There are four good numbers in the range [1, 10] : 2, 5, 6, 9.
 * Note that 1 and 10 are not good numbers, since they remain unchanged after rotating.
 *
 * Note:
 *
 *     N  will be in range [1, 10000].
 */
/**
 * Mathematic solution.
 * see also: http://www.frankmadrid.com/ALudicFallacy/2018/02/28/rotated-digits-leet-code-788/
 */
class Solution {
public:
    Solution()
    {
        type = { 0,0,1,2,2,1,1,2,0,1 };
        differentRotations = { 0,0,1,1,1,2,3,3,3,4 };
        validRotations = { 1,2,3,3,3,4,5,5,6,7 };
        sameRotations = { 1,2,2,2,2,2,2,2,3,3 };
    }

    int rotatedDigits(int N) {
        return countRotations(to_string(N), false);
    }
private:
    int countRotations(const std::string& str, bool isNewNumber)
    {
        int digit = str[0] - '0';

        if (str.length() == 1) {
            return isNewNumber ? validRotations[digit] : differentRotations[digit];
        }

        int ret = 0;
        if (digit != 0) {
            ret += validRotations[digit - 1] * std::pow(7, str.length() - 1);

            if (false == isNewNumber) {
                ret -= sameRotations[digit - 1] * std::pow(3, str.length() - 1);
            }
        }

        if (type[digit] == 1) isNewNumber = true;

        if (type[digit] != 2) {
            ret += countRotations(str.substr(1, std::string::npos), isNewNumber);
        }

        return ret;
    }
    vector<int> type;
    vector<int> differentRotations;
    vector<int> validRotations;
    vector<int> sameRotations;
};

/**
 * Original solution.
 */
class Solution {
public:
    int rotatedDigits(int N) {
        int ans = 0;
                                //0  1  2  3  4  5  6  7  8  9
        vector<int> must_have = { 0, 0, 1, 0, 0, 1, 1, 0, 0, 1};
        vector<int> cant_have = { 0, 0, 0, 1, 1, 0, 0, 1, 0, 0};
        for (int i = 1; i <= N; ++i) {
            int m = 0, c = 0, num = i;
            while (num) {
                int digit = num % 10;
                if (1 == cant_have[digit]){
                    ++c;
                    break;
                }
                if (1 == must_have[digit]) ++m;
                num /= 10;
            }
            if (0 == c && m > 0) {
                ++ans;
            }
        }
        return ans;
    }
};
