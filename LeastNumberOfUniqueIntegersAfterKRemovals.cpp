/**
 * Least Number of Unique Integers after K Removals
 *
 * Given an array of integers arr and an integer k. Find the least number of unique integers after
 * removing exactly k elements.
 *
 * Example 1:
 *
 * Input: arr = [5,5,4], k = 1
 * Output: 1
 * Explanation: Remove the single 4, only 5 is left.
 * Example 2:
 * Input: arr = [4,3,1,1,3,3,2], k = 3
 * Output: 2
 * Explanation: Remove 4, 2 and either one of the two 1s or three 3s. 1 and 3 will be left.
 *
 * Constraints:
 *
 * 1 <= arr.length <= 10^5
 * 1 <= arr[i] <= 10^9
 * 0 <= k <= arr.length
 */
/**
 * Original solution.
 */
class Solution {
public:
    int findLeastNumOfUniqueInts(vector<int>& arr, int k) {
        unordered_map<int, int> int_count;
        for (auto n : arr) {
            ++int_count[n];
        }

        vector<pair<int, int>> freq_sorted{int_count.begin(), int_count.end()};
        sort(freq_sorted.begin(), freq_sorted.end(),
            [](auto const& a, auto const& b) {
                return a.second < b.second;
            });

        int ans = freq_sorted.size();
        for (auto const& f : freq_sorted) {
            if (0 == k) {
                break;
            }
            k = k >= f.second ? (--ans, k - f.second) : 0;
        }

        return ans;
    }
};
