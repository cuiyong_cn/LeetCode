/**
 * Add two numbet
 *
 * You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse
 * order, and each of their nodes contains a single digit. Add the two numbers and return the sum as a linked list.
 *
 * You may assume the two numbers do not contain any leading zero, except the number 0 itself.
 *
 * Example 1:
 *
 * Input: l1 = [2,4,3], l2 = [5,6,4]
 * Output: [7,0,8]
 * Explanation: 342 + 465 = 807.
 *
 * Example 2:
 *
 * Input: l1 = [0], l2 = [0]
 * Output: [0]
 *
 * Example 3:
 *
 * Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
 * Output: [8,9,9,9,0,0,0,1]
 *
 * Constraints:
 *
 *     The number of nodes in each linked list is in the range [1, 100].
 *     0 <= Node.val <= 9
 *     It is guaranteed that the list represents a number that does not have leading zeros.
 */
/**
 * More short one
 */
class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        int carry = 0;
        ListNode dummy;
        auto prev = &dummy;

        while (l1 || l2 || carry) {
            auto n1 = l1 ? l1->val : 0;
            auto n2 = l2 ? l2->val : 0;
            auto n = n1 + n2 + carry;
            if (n > 9) {
                carry = 1;
                n -= 10;
            }
            else {
                carry = 0;
            }

            auto node = new ListNode(n);
            prev->next = node;
            prev = node;
            l1 = l1 ? l1->next : nullptr;
            l2 = l2 ? l2->next : nullptr;
        }

        return dummy.next;
    }
};

/**
 * Original solution
 */
/**
 * Definition for singly-linked list.
 *
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        ListNode dummy;
        ListNode* prev = &dummy;
        auto carry = 0;
        while (l1 && l2) {
            auto n = l1->val + l2->val + carry;
            if (n > 9) {
                carry = 1;
                n -= 10;
            }
            else {
                carry = 0;
            }

            auto node = new ListNode(n);
            prev->next = node;
            prev = node;
            l1 = l1->next;
            l2 = l2->next;
        }

        while (l1) {
            auto n = carry + l1->val;
            if (n > 9) {
                carry = 1;
                n -= 10;
            }
            else {
                carry = 0;
            }
            auto node = new ListNode (n);
            prev->next = node;
            prev = node;
            l1 = l1->next;
        }

        while (l2) {
            auto n = carry + l2->val;
            if (n > 9) {
                carry = 1;
                n -= 10;
            }
            else {
                carry = 0;
            }
            auto node = new ListNode (n);
            prev->next = node;
            prev = node;
            l2 = l2->next;
        }

        if (carry) {
            prev->next = new ListNode(1);
        }

        return dummy.next;
    }
};
