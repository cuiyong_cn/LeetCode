/**
 * Number of Matching Subsequences
 *
 * Given string S and a dictionary of words words, find the number of words[i] that is a subsequence of S.
 *
 * Example :
 * Input:
 * S = "abcde"
 * words = ["a", "bb", "acd", "ace"]
 * Output: 3
 * Explanation: There are three words in words that are a subsequence of S: "a", "acd", "ace".
 * Note:
 *
 * All words in words and S will only consists of lowercase letters.
 * The length of S will be in the range of [1, 50000].
 * The length of words will be in the range of [1, 5000].
 * The length of words[i] will be in the range of [1, 50].
 */
/**
 * A intresting solution from discussion. But this is more like a C solution.
 */
class Solution {
public:
    int numMatchingSubseq(string S, vector<string>& words) {
        vector<const char*> waiting[128];

        for (auto &w : words) {
            waiting[w[0]].push_back(w.c_str());
        }

        for (char c : S) {
            auto advance = waiting[c];
            waiting[c].clear();
            for (auto it : advance) {
                waiting[*++it].push_back(it);
            }
        }

        return waiting[0].size();
    }
};

/**
 * Maybe more efficient one. using binary search.
 */
class Solution {
public:
    int numMatchingSubseq (string S, vector<string>& words) {
		vector<vector<int>> alpha (26);
        for (int i = 0; i < S.size (); ++i) {
            alpha[S[i] - 'a'].push_back(i);
        }

		int res = 0;
		for (const auto& word : words) {
			int x = -1;
			bool found = true;

			for (char c : word) {
				auto it = upper_bound (alpha[c - 'a'].begin (), alpha[c - 'a'].end (), x);
				if (it == alpha[c - 'a'].end ()) found = false;
				else x = *it;
			}

			if (found) res++;
		}

		return res;
	}
};

/**
 * To make code more readable
 */
class Solution {
public:
    int numMatchingSubseq(string S, vector<string>& words) {
        int ans = 0;

        for (auto& w : words) {
            if (StringWrapper(w).is_sub_of(S)) {
                ++ans;
            }
        }

        return ans;
    }

private:
    class StringWrapper
    {
    public:
        StringWrapper(const string& s) : str{s}
        {
            ;
        }

        bool is_sub_of(const string& s) const
        {
            if (str.length() > s.length()) {
                return false;
            }

            bool is_sub = true;
            int pos = 0;
            for (auto c : str) {
                pos = s.find(c, pos);
                if (string::npos == pos) {
                    is_sub = false;
                    break;
                }
                ++pos;
            }

            return is_sub;
        }

    private:
        const string& str;
    };
};

/**
 * Original solution.
 */
class Solution {
public:
    int numMatchingSubseq(string S, vector<string>& words) {
        int ans = 0;

        for (auto& w : words) {
            if (w.length() <= S.length()) {
                bool is_sub = true;
                int pos = 0;
                for (auto c : w) {
                    pos = S.find(c, pos);
                    if (string::npos == pos) {
                        is_sub = false;
                        break;
                    }
                    ++pos;
                }

                if (is_sub) {
                    ++ans;
                }
            }
        }

        return ans;
    }
};
