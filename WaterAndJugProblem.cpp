/**
 * Water and Jug Problem
 *
 * You are given two jugs with capacities jug1Capacity and jug2Capacity liters. There is an infinite
 * amount of water supply available. Determine whether it is possible to measure exactly
 * targetCapacity liters using these two jugs.
 *
 * If targetCapacity liters of water are measurable, you must have targetCapacity liters of water
 * contained within one or both buckets by the end.
 *
 * Operations allowed:
 *
 * Fill any of the jugs with water.
 * Empty any of the jugs.
 * Pour water from one jug into another till the other jug is completely full, or the first jug
 * itself is empty.
 *
 * Example 1:
 *
 * Input: jug1Capacity = 3, jug2Capacity = 5, targetCapacity = 4
 * Output: true
 * Explanation: The famous Die Hard example
 * Example 2:
 *
 * Input: jug1Capacity = 2, jug2Capacity = 6, targetCapacity = 5
 * Output: false
 * Example 3:
 *
 * Input: jug1Capacity = 1, jug2Capacity = 2, targetCapacity = 3
 * Output: true
 *
 * Constraints:
 *
 * 1 <= jug1Capacity, jug2Capacity, targetCapacity <= 106
 */
/**
 * Math solution from discussion.
 *
 * Quote from wiki:
 * Bézout's identity (also called Bézout's lemma) is a theorem in the elementary theory of numbers:
 *
 * let a and b be nonzero integers and let d be their greatest common divisor. Then there exist integers x
 * and y such that ax+by=d
 *
 * In addition, the greatest common divisor d is the smallest positive integer that can be written as ax + by
 * every integer of the form ax + by is a multiple of the greatest common divisor d.
 *
 * If a or b is negative this means we are emptying a jug of x or y gallons respectively.
 * Similarly if a or b is positive this means we are filling a jug of x or y gallons respectively.
 *
 * x = 4, y = 6, z = 8.
 * GCD(4, 6) = 2
 * 8 is multiple of 2
 * so this input is valid and we have:
 * -1 * 4 + 6 * 2 = 8
 *
 * In this case, there is a solution obtained by filling the 6 gallon jug twice and emptying the 4
 * gallon jug once. (Solution. Fill the 6 gallon jug and empty 4 gallons to the 4 gallon jug. Empty
 * the 4 gallon jug. Now empty the remaining two gallons from the 6 gallon jug to the 4 gallon jug.
 * Next refill the 6 gallon jug. This gives 8 gallons in the end)
 */
class Solution {
public:
    bool canMeasureWater(int jug1Capacity, int jug2Capacity, int targetCapacity) {
        auto total_capacity = jug1Capacity + jug2Capacity;
        if (total_capacity < targetCapacity) {
            return false;
        }

        if (total_capacity == targetCapacity
           || jug1Capacity == targetCapacity
           || jug2Capacity == targetCapacity) {
            return true;
        }

        return 0 == (targetCapacity % std::gcd(jug1Capacity, jug2Capacity));
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool canMeasureWater(int jug1Capacity, int jug2Capacity, int targetCapacity) {
        auto total_capacity = jug1Capacity + jug2Capacity;
        if (total_capacity < targetCapacity) {
            return false;
        }

        set<pair<int, int>> seen;
        deque<pair<int, int>> q = {{0, 0}};
        while (!q.empty()) {
            auto [jug1, jug2] = q.front(); q.pop_front();
            if (measurable(jug1, jug2, targetCapacity)) {
                return true;
            }

            if (!seen.count({jug1, jug2})) {
                seen.insert({jug1, jug2});
                // fill any of the jugs
                q.push_back({jug1Capacity, jug2});
                q.push_back({jug1, jug2Capacity});
                // empty any of the jugs
                q.push_back({0, jug2});
                q.push_back({jug1, 0});
                // pour water from one jug into another one
                // jug1 to jug2
                auto empty1 = min(jug1, jug2Capacity - jug2);
                q.push_back({jug1 - empty1, jug2 + empty1});
                auto empty2 = min(jug2, jug1Capacity - jug1);
                q.push_back({jug1 + empty2, jug2 - empty2});
            }
        }

        return false;
    }

private:
    bool measurable(int jug1, int jug2, int target) {
        auto total = jug1 + jug2;
        if (total == target || jug1 == target || jug2 == target) {
            return true;
        }

        return false;
    }
};
