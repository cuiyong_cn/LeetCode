/**
 * Domino and Tromino Tiling
 *
 * We have two types of tiles: a 2x1 domino shape, and an "L" tromino shape. These shapes may be
 * rotated.
 *
 * XX  <- domino
 *
 * XX  <- "L" tromino
 * X
 * Given N, how many ways are there to tile a 2 x N board? Return your answer modulo 10^9 + 7.
 *
 * (In a tiling, every square must be covered by a tile. Two tilings are different if and only if
 * there are two 4-directionally adjacent cells on the board such that exactly one of the tilings
 * has both squares occupied by a tile.)
 *
 * Example:
 * Input: 3
 * Output: 5
 * Explanation:
 * The five different ways are listed below, different letters indicates different tiles:
 * XYZ XXZ XYY XXY XYY
 * XYZ YYZ XZZ XYY XXY
 * Note:
 *
 * N  will be in range [1, 1000].
 */
/**
 * Original solution. Ok, so the idea is:
 *           xxx...xxx    xxxx...xxx
 * U(N) ->  xxxx...xxx or  xxx...xxx
 *           |-  N  -|     |-  N  -|
 *
 * g(N) ->  xxx...xxx
 *          xxx...xxx
 *          |-  N  -|
 *
 *                  y  xxx...xxxy
 *         |-  use  y  xxx...xxxy -> g(N - 1)
 *         |
 *         |           xx...xxxyy
 *         |-  use yy  xx...xxxyy -> g(N - 2)
 *         |
 * so g(N)-|       y   xxx...xxxy
 *         |-  use yy  xx...xxxyy -> U(N - 2)
 *         |
 *         |       yy  xx...xxxyy
 *         |-  use y   xxx...xxxy -> U(N - 2)
 *
 * so g(N) = g(N - 1) + g(N - 2) + 2 * U(N - 2)
 *
 *                     xxx...xxx
 *         |- use yy  yyxx...xxx -> U(N - 1)
 *    U(N)-|
 *         |       y   yxx...xxx
 *         |- use yy  yyxx...xxx -> g(N - 1)
 *
 * therefore U(N) = U(N - 1) + g(N - 1)
 *
 * g(N) = g(N - 1) + g(N - 2) + 2 * U(N - 2)
 *      = g(N - 1) + g(N - 2) + 2 * U(N - 3) + 2 * g(N - 3)
 *      = g(N - 1) + g(N - 3) + ( g(N - 2) + g(N - 3) + 2 * U(N - 3) )
 *      = g(N - 1) + g(N - 3) + g(N - 1)
 *      = 2 * g(N - 1) + g(N - 3)
 */
class Solution {
public:
    int numTilings(int N)
    {
        long long g[1001],u[1001];
        int mod=1000000007;
        g[0]=0; g[1]=1; g[2]=2;
        u[0]=0; u[1]=1; u[2]=2;

        for(int i=3;i<=N;i++)
        {
            u[i] = (u[i-1] + g[i-1]           )   %mod;
            g[i] = (g[i-1] + g[i-2] + 2*u[i-2])   %mod;
        }
        return g[N]%mod;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int numTilings(int N) {
        if (N < 3) {
            return N;
        }

        int const mod = 1000000007;
        vector<long> dp(N + 1, 0);
        dp[0] = 1;
        dp[1] = 1;
        dp[2] = 2;

        for (int i = 3; i <= N; ++i) {
            dp[i] = (2 * dp[i - 1] + dp[i - 3]) % mod;
        }

        return dp[N];
    }
};
