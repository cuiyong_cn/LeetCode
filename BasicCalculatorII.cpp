/**
 * Basic Calculator II
 *
 * Given a string s which represents an expression, evaluate this expression and return its value.
 *
 * The integer division should truncate toward zero.
 *
 * Note: You are not allowed to use any built-in function which evaluates strings as mathematical
 * expressions, such as eval().
 *
 * Example 1:
 *
 * Input: s = "3+2*2"
 * Output: 7
 * Example 2:
 *
 * Input: s = " 3/2 "
 * Output: 1
 * Example 3:
 *
 * Input: s = " 3+5 / 2 "
 * Output: 5
 *
 * Constraints:
 *
 * 1 <= s.length <= 3 * 105
 * s consists of integers and operators ('+', '-', '*', '/') separated by some number of spaces.
 * s represents a valid expression.
 * All the integers in the expression are non-negative integers in the range [0, 231 - 1].
 * The answer is guaranteed to fit in a 32-bit integer.
 */
/**
 * More compact solution.
 */
class Solution {
public:
    int calculate(string s) {
        stringstream ss("+" + s);
        char op;
        int n, last, ans = 0;
        while (ss >> op >> n) {
            if (op == '+' || op == '-') {
                n = op == '+' ? n : -n;
                ans += n;
            } else {
                n = op == '*' ? last * n : last / n;
                ans = ans - last + n; // simulate a stack by recovering last values
            }
            last = n;
        }

        return ans;
    }
};

/**
 * Stack solution.
 */
class Solution {
public:
    int calculate(string s) {
        while (pos < s.length()) {
            auto ch = s[pos++];
            if ('*' == ch) {
                auto val = operands.back(); operands.pop_back();
                operands.push_back(val * number(s));
            }
            else if ('/' == ch) {
                auto val = operands.back(); operands.pop_back();
                operands.push_back(val / number(s));
            }
            else if ('+' == ch || '-' == ch) {
                update_stack();
                operators.push_back(ch);
            }
            else {
                --pos;
                operands.push_back(number(s));
            }
        }

        update_stack();

        return operands.back();
    }

private:
    void update_stack()
    {
        if (!operators.empty()) {
            auto op1 = operands.back(); operands.pop_back();
            auto op2 = operands.back(); operands.pop_back();
            auto op = operators.back(); operators.pop_back();
            if ('-' == op) {
                operands.push_back(op2 - op1);
            }
            else {
                operands.push_back(op2 + op1);
            }
        }
    }

    int number(string const& s)
    {
        int start = pos;
        while (pos < s.length()) {
            auto ch = s[pos];
            if ('+' == ch || '-' == ch || '*' == ch || '/' == ch) {
                break;
            }
            ++pos;
        }

        return stoi(s.substr(start, pos - start));
    }

    vector<int> operands;
    vector<char> operators;
    int pos = 0;
};

/**
 * Original solution. Pretty much a mess.
 */
class Solution {
public:
    int calculate(string s) {
        return expr(s);
    }

private:
    int number(string const& s)
    {
        std::string const operators = "+-*/";
        int start = pos;
        while (pos < s.length()) {
            if (string::npos != operators.find(s[pos])) {
                break;
            }
            ++pos;
        }

        return stoi(s.substr(start, pos - start));
    }

    int expr(string const& s)
    {
        int val = 0;
        while (pos < s.length()) {
            auto ch = s[pos];
            if ('+' == ch) {
                ++pos;
                val += term(s);
            }
            else if ('-' == ch) {
                ++pos;
                val -= term(s);
            }
            else {
                val += term(s);
            }
        }

        return val;
    }

    int term(string const& s) {
        auto val = number(s);
        while (pos < s.length()) {
            auto ch = s[pos++];
            if ('+' == ch || '-' == ch) {
                --pos;
                break;
            }
            else if ('*' == ch) {
                val *= number(s);
            }
            else if ('/' == ch) {
                val /= number(s);
            }
        }

        return val;
    }

    int pos = 0;
};
