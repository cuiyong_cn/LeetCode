/**
 * Print Binary Tree
 *
 * Print a binary tree in an m*n 2D string array following these rules:
 *
 * The row number m should be equal to the height of the given binary tree.
 * The column number n should always be an odd number.
 * The root node's value (in string format) should be put in the exactly middle of the first row it can be put. The
 * column and the row where the root node belongs will separate the rest space into two parts (left-bottom part and
 * right-bottom part). You should print the left subtree in the left-bottom part and print the right subtree in the
 * right-bottom part. The left-bottom part and the right-bottom part should have the same size. Even if one subtree
 * is none while the other is not, you don't need to print anything for the none subtree but still need to leave the
 * space as large as that for the other subtree. However, if two subtrees are none, then you don't need to leave
 * space for both of them.
 * Each unused space should contain an empty string "".
 * Print the subtrees following the same rules.
 *
 * Example 1:
 * Input:
 *      1
 *     /
 *    2
 *
 * Output:
 * [["", "1", ""],
 *  ["2", "", ""]]
 *
 * Example 2:
 * Input:
 *      1
 *     / \
 *    2   3
 *     \
 *      4
 * Output:
 * [["", "", "", "1", "", "", ""],
 *  ["", "2", "", "", "", "3", ""],
 *  ["", "", "4", "", "", "", ""]]
 *
 * Example 3:
 * Input:
 *       1
 *      / \
 *     2   5
 *    /
 *   3
 *  /
 * 4
 * Output:
 *
 * [["",  "",  "", "",  "", "", "", "1", "",  "",  "",  "",  "", "", ""]
 *  ["",  "",  "", "2", "", "", "", "",  "",  "",  "",  "5", "", "", ""]
 *  ["",  "3", "", "",  "", "", "", "",  "",  "",  "",  "",  "", "", ""]
 *  ["4", "",  "", "",  "", "", "", "",  "",  "",  "",  "",  "", "", ""]]
 * Note: The height of binary tree is in the range of [1, 10].
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/**
 * Original solution. Idea is:
 * 1. Calculate the height of the tree so we know the m and n of the result array.
 * 2. fill the array recursively.
 */
class Solution {
public:
    vector<vector<string>> ans;

    int heightOfTree(TreeNode* root) {
        int ans = 0;
        if (nullptr != root) {
            ans = 1 + max(heightOfTree(root->left), heightOfTree(root->right));
        }

        return ans;
    }

    void fillArray(TreeNode* root, int row, int start, int end) {
        if (nullptr != root) {
            int mid = (start + end) / 2;
            ans[row][mid] = to_string(root->val);
            fillArray(root->left, row + 1, start, mid - 1);
            fillArray(root->right, row + 1, mid + 1, end);
        }
    }

    vector<vector<string>> printTree(TreeNode* root) {
        int height = heightOfTree(root);
        int width = pow(2, height) - 1;
        ans = vector<vector<string>>(height, vector<string>(width));
        fillArray(root, 0, 0, width);

        return ans;
    }
};
