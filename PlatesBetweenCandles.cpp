/**
 * Plates Between Candles
 *
 * There is a long table with a line of plates and candles arranged on top of it. You are given a
 * 0-indexed string s consisting of characters '*' and '|' only, where a '*' represents a plate and
 * a '|' represents a candle.
 *
 * You are also given a 0-indexed 2D integer array queries where queries[i] = [lefti, righti]
 * denotes the substring s[lefti...righti] (inclusive). For each query, you need to find the number
 * of plates between candles that are in the substring. A plate is considered between candles if
 * there is at least one candle to its left and at least one candle to its right in the substring.
 *
 * For example, s = "||**||**|*", and a query [3, 8] denotes the substring "*||**|". The number of
 * plates between candles in this substring is 2, as each of the two plates has at least one candle
 * in the substring to its left and right.  Return an integer array answer where answer[i] is the
 * answer to the ith query.
 *
 * Example 1:
 *
 * ex-1
 * Input: s = "**|**|***|", queries = [[2,5],[5,9]]
 * Output: [2,3]
 * Explanation:
 * - queries[0] has two plates between candles.
 * - queries[1] has three plates between candles.
 * Example 2:
 *
 * ex-2
 * Input: s = "***|**|*****|**||**|*", queries = [[1,17],[4,5],[14,17],[5,11],[15,16]]
 * Output: [9,0,0,0,0]
 * Explanation:
 * - queries[0] has nine plates between candles.
 * - The other queries have zero plates between candles.
 *
 * Constraints:
 *
 * 3 <= s.length <= 105
 * s consists of '*' and '|' characters.
 * 1 <= queries.length <= 105
 * queries[i].length == 2
 * 0 <= lefti <= righti < s.length
 */
/**
 * We don't need to count the plates.
 */
class Solution {
public:
    vector<int> platesBetweenCandles(string const& s, vector<vector<int>>& queries) {
        vector<int> candles;
        int const slen = s.length();

        for (int i = 0; i < slen; ++i) {
            if ('|' == s[i]) {
                candles.emplace_back(i);
            }
        }

        int const qsize = queries.size();
        vector<int> ans(qsize, 0);

        for (int i = 0; i < qsize; ++i) {
            auto s = lower_bound(candles.begin(), candles.end(), queries[i][0]) - candles.begin();
            auto e = upper_bound(candles.begin(), candles.end(), queries[i][1]) - candles.begin() - 1;
            ans[i] = s < e ? candles[e] - candles[s] - (e - s) : 0;
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<int> platesBetweenCandles(string const& s, vector<vector<int>>& queries) {
        vector<int> plates_to(s.length(), 0);
        vector<int> candles;
        int plates = 0;
        int total = 0;
        auto i = s.find_first_of('|');
        while (i < s.length()) {
            if ('*' == s[i]) {
                ++plates;
            }
            else {
                candles.emplace_back(i);
                total += plates;
                plates = 0;
            }
            plates_to[i] = total;
            ++i;
        }

        candles.emplace_back(s.length() - 1);

        int const qsize = queries.size();
        vector<int> ans(qsize, 0);

        for (int i = 0; i < qsize; ++i) {
            auto s = *lower_bound(candles.begin(), candles.end(), queries[i][0]);
            ans[i] = max(0, plates_to[queries[i][1]] - plates_to[s]);
        }

        return ans;
    }
};
