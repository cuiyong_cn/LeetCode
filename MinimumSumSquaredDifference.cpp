/**
 * Minimum Sum of Squared Difference
 *
 * You are given two positive 0-indexed integer arrays nums1 and nums2, both of length n.
 *
 * The sum of squared difference of arrays nums1 and nums2 is defined as the sum of (nums1[i] -
 * nums2[i])2 for each 0 <= i < n.
 *
 * You are also given two positive integers k1 and k2. You can modify any of the elements of nums1
 * by +1 or -1 at most k1 times. Similarly, you can modify any of the elements of nums2 by +1 or -1
 * at most k2 times.
 *
 * Return the minimum sum of squared difference after modifying array nums1 at most k1 times and
 * modifying array nums2 at most k2 times.
 *
 * Note: You are allowed to modify the array elements to become negative integers.
 *
 * Example 1:
 *
 * Input: nums1 = [1,2,3,4], nums2 = [2,10,20,19], k1 = 0, k2 = 0
 * Output: 579
 * Explanation: The elements in nums1 and nums2 cannot be modified because k1 = 0 and k2 = 0.
 * The sum of square difference will be: (1 - 2)2 + (2 - 10)2 + (3 - 20)2 + (4 - 19)2 = 579.
 * Example 2:
 *
 * Input: nums1 = [1,4,10,12], nums2 = [5,8,6,9], k1 = 1, k2 = 1
 * Output: 43
 * Explanation: One way to obtain the minimum sum of square difference is:
 * - Increase nums1[0] once.
 * - Increase nums2[2] once.
 * The minimum of the sum of square difference will be:
 * (2 - 5)2 + (4 - 8)2 + (10 - 7)2 + (12 - 9)2 = 43.
 * Note that, there are other ways to obtain the minimum of the sum of square difference, but there is no way to obtain a sum smaller than 43.
 *
 * Constraints:
 *
 * n == nums1.length == nums2.length
 * 1 <= n <= 105
 * 0 <= nums1[i], nums2[i] <= 105
 * 0 <= k1, k2 <= 109
 */
/**
 * Binary search solution.
 */
class Solution {
public:
    long long minSumSquareDiff(vector<int>& nums1, vector<int>& nums2, int k1, int k2) {
        auto nsize = nums1.size();
        auto diffs = vector<long long>(nsize, 0);
        for (auto i = 0; i < nsize; ++i) {
            diffs[i] = abs(nums1[i] - nums2[i]);
        }

        auto left = 0LL;
        auto right = *max_element(begin(diffs), end(diffs));
        auto target = 0LL;
        auto extra_ops = 0LL;
        while (left <= right) {
            auto ops = 0LL;
            auto mid = left + (right - left) / 2;
            for (auto d : diffs) {
                ops += max(0LL, d - mid);
            }

            if (ops > (k1 + k2)) {
                left = mid + 1;
            }
            else {
                target = mid;
                right = mid - 1;
                extra_ops = (k1 + k2) - ops;
            }
        }

        auto diff_evaluator = [&extra_ops, &target](auto d) {
                if (d < target) {
                    return d;
                }

                if (extra_ops > 0) {
                    --extra_ops;
                    return max(0LL, target - 1);
                }
                return target;
            };
        auto ans = 0LL;
        for (auto d : diffs) {
            auto ndiff = invoke(diff_evaluator, d);
            ans += ndiff * ndiff;
        }
        return ans;
    }
};

/**
 * Group operation instead of decrement one by one.
 */
class Solution {
public:
    long long minSumSquareDiff(vector<int>& nums1, vector<int>& nums2, int k1, int k2) {
        auto nsize = nums1.size();
        auto diffs = vector<long long>(nsize, 0);
        for (auto i = 0; i < nsize; ++i) {
            diffs[i] = abs(nums1[i] - nums2[i]);
        }

        auto diff_max = *max_element(begin(diffs), end(diffs));
        auto diff_bucket = vector<long long>(diff_max + 1, 0);
        for (auto d : diffs) {
            ++diff_bucket[d];
        }

        auto k = k1 + k2;
        for (auto i = diff_max; i > 0; --i) {
            auto cnt = min<long long>(diff_bucket[i], k);
            k -= cnt;
            diff_bucket[i] -= cnt;
            diff_bucket[i - 1] += cnt;
        }

        copy(begin(diff_bucket), end(diff_bucket), ostream_iterator<long long>(cout, ", "));
        auto ans = 0LL;
        for (auto i = 0; i <= diff_max; ++i) {
            ans += diff_bucket[i] * i * i;
        }
        return ans;
    }
};

/**
 * Original solution. Right direction but TLE.
 */
class Solution {
public:
    long long minSumSquareDiff(vector<int>& nums1, vector<int>& nums2, int k1, int k2) {
        auto pq = priority_queue<int>{};
        auto size = static_cast<int>(nums1.size());
        for (auto i = 0; i < size; ++i) {
            pq.push(abs(nums1[i] - nums2[i]));
        }

        auto cnt = k1 + k2;
        while (cnt > 0 && !pq.empty()) {
            --cnt;
            auto top = pq.top();
            pq.pop();
            if (top > 0) {
                pq.push(top - 1);
            }
        }

        auto ans = 0LL;
        while (!pq.empty()) {
            auto d = static_cast<long long>(pq.top());
            pq.pop();
            ans += d * d;
        }
        return ans;
    }
};
