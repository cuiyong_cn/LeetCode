/**
 * Binary String With Substrings Representing 1 To N
 *
 * Given a binary string S (a string consisting only of '0' and '1's) and a positive
 * integer N, return true if and only if for every integer X from 1 to N, the binary
 * representation of X is a substring of S.
 *
 * Example 1:
 *
 * Input: S = "0110", N = 3
 * Output: true
 *
 * Example 2:
 *
 * Input: S = "0110", N = 4
 * Output: false
 *
 * Note:
 *
 *     1 <= S.length <= 1000
 *     1 <= N <= 10^9
 */
/**
 * Very confusing explanation from leetcode. Spend a lot of time to figure that out.
 *
 * number 1001 ~ 2000 and their corresponding binary form, they contains the binary
 * form of (1 - 1000)
 *
 * 1001 0b1111101001
 * 1002 0b1111101010
 * 1003 0b1111101011
 * ...
 * 1023 0b1111111111
 * 1024 0b10000000000
 * ...
 * 1997 0b11111001101
 * 1998 0b11111001110
 * 1999 0b11111001111
 * 2000 0b11111010000
 *
 */
class Solution {
private:
    void getBinStr(int n, string& str) {
        while (n > 0) {
            int r = n & 1;
            str = str + ((1 == r) ? '1' : '0');
            n = n >> 1;
        }
    }

public:
    bool queryString(string S, int N) {
        string sub = S.substr(S.find('1'));
        if (N > sub.size()) return false;

        reverse(S.begin(), S.end());
        for (int i = N; i > N / 2; --i) {
            string binStr;
            getBinStr(i, binStr);
            if (S.find(binStr) == string::npos) {
                return false;
            }
        }

        return true;
    }
};

/**
 * Improved version. Explanation:
 *      Every i < N / 2, the binary string of 2 * i contains the binary string of i.
 */
class Solution {
private:
    void getBinStr(int n, string& str) {
        while (n > 0) {
            int r = n & 1;
            str = str + ((1 == r) ? '1' : '0');
            n = n >> 1;
        }
    }

public:
    bool queryString(string S, int N) {
        reverse(S.begin(), S.end());
        for (int i = N; i > N / 2; --i) {
            string binStr;
            getBinStr(i, binStr);
            if (S.find(binStr) == string::npos) {
                return false;
            }
        }

        return true;
    }
};
/**
 * Simple approach. But can be improved.
 */
class Solution {
private:
    void getBinStr(int n, string& str) {
        while (n > 0) {
            int r = n & 1;
            str = str + ((1 == r) ? '1' : '0');
            n = n >> 1;
        }
    }

public:
    bool queryString(string S, int N) {
        reverse(S.begin(), S.end());
        for (int i = 1; i <= N; ++i) {
            string binStr;
            getBinStr(i, binStr);
            if (S.find(binStr) == string::npos) {
                return false;
            }
        }

        return true;
    }
};
