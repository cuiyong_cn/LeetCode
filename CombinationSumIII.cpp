/**
 * Combination Sum III
 *
 * Find all possible combinations of k numbers that add up to a number n, given that only numbers from 1 to 9 can be
 * used and each combination should be a unique set of numbers.
 *
 * Note:
 *     All numbers will be positive integers.
 *     The solution set must not contain duplicate combinations.
 *
 * Example 1:
 *
 * Input: k = 3, n = 7
 * Output: [[1,2,4]]
 *
 * Example 2:
 *
 * Input: k = 3, n = 9
 * Output: [[1,2,6], [1,3,5], [2,3,4]]
 */
/**
 * Improved version. Backtrack
 */
class Solution {
public:
    vector<vector<int>> ans;
    int count;
    int target;

    void dfs(vector<int>& combo, int cur, int sum) {
        if (sum > target) return;

        if (combo.size() == count) {
            if (sum == target) {
                ans.push_back(combo);
            }
            return;
        }

        for (int j = cur; j < 10; ++j) {
            combo.push_back(j);
            dfs(combo, j + 1, sum + j);
            combo.pop_back();
        }
    }

    vector<vector<int>> combinationSum3(int k, int n) {
        count = k;
        target = n;
        vector<int> combo;
        dfs(combo, 1, 0);

        return ans;
    }
};

/**
 * Original solution. Check all permutation to see if fits the bill
 */
class Solution {
public:
    vector<int> combo;
    vector<vector<int>> ans;
    int count;
    int target;

    bool comboExists() {
        for (auto& c : ans) {
            bool same = true;
            for (int i = 0; i < combo.size(); ++i) {
                if (c[i] != combo[i]) {
                    same = false;
                    break;
                }
            }
            if (same) return true;
        }

        return false;
    }
    void dfs(int i, int cur, int sum) {
        if (sum > target) return;

        if (i == count) {
            if (sum == target) {
                if (false == comboExists())
                    ans.push_back(combo);
            }
            return;
        }

        for (int j = cur + 1; j < 10; ++j) {
            dfs(i, j, sum);
            combo[i] = j;
            dfs(i + 1, j, sum + j);
        }
    }

    vector<vector<int>> combinationSum3(int k, int n) {
        combo = vector<int>(k, 0);
        count = k;
        target = n;
        for (int i = 1; i < 10; ++i) {
            combo[0] = i;
            dfs(1, i, i);
        }
        return ans;
    }
};
