/**
 * Find First and Last Position of Element in Sorted Array
 *
 * Given an array of integers nums sorted in ascending order, find the starting and ending position
 * of a given target value.
 *
 * If target is not found in the array, return [-1, -1].
 *
 * You must write an algorithm with O(log n) runtime complexity.
 *
 * Example 1:
 *
 * Input: nums = [5,7,7,8,8,10], target = 8
 * Output: [3,4]
 * Example 2:
 *
 * Input: nums = [5,7,7,8,8,10], target = 6
 * Output: [-1,-1]
 * Example 3:
 *
 * Input: nums = [], target = 0
 * Output: [-1,-1]
 *
 * Constraints:
 *
 * 0 <= nums.length <= 105
 * -109 <= nums[i] <= 109
 * nums is a non-decreasing array.
 * -109 <= target <= 109
 **/
/**
 * And intresting one.
 */
class Solution
{
public:
    vector<int> searchRange(vector<int> const& nums, int target) {
        double left = target - 0.5, right = target + 0.5;
        int l = bs(nums, left), r = bs(nums, right);
        if (l == r) {
            return {-1, -1};
        }

        return {l, r-1};
    }

    int bs(vector<int> const& nums, double target) {
        int l = 0, h = nums.size() - 1;
        while(l <= h){
            int m = l + (h - l)/2;
            if(target > nums[m]) l = m+1;
            else h = m-1;
        }
        return l;
    }
};

/**
 * Simplify below code
 */
class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target) {
        auto sit = lower_bound(begin(nums), end(nums), target);
        if (sit == end(nums) || *sit != target) {
            return {-1, -1};
        }

        auto eit = upper_bound(sit, end(nums), target);
        int start = distance(begin(nums), sit);
        int end = distance(begin(nums), eit);
        return {start, end - 1};
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target) {
        auto it = lower_bound(begin(nums), end(nums), target);
        if (it == end(nums) || *it != target) {
            return {-1, -1};
        }

        int start = distance(begin(nums), it);
        int end = start + 1;
        for (; end < nums.size(); ++end) {
            if (nums[end] != target) {
                break;
            }
        }

        return {start, end - 1};
    }
};
