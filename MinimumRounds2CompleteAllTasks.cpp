/**
 * Minimum Rounds to Complete All Tasks
 *
 * You are given a 0-indexed integer array tasks, where tasks[i] represents the difficulty level of
 * a task. In each round, you can complete either 2 or 3 tasks of the same difficulty level.
 *
 * Return the minimum rounds required to complete all the tasks, or -1 if it is not possible to
 * complete all the tasks.
 *
 * Example 1:
 *
 * Input: tasks = [2,2,3,3,2,4,4,4,4,4]
 * Output: 4
 * Explanation: To complete all the tasks, a possible plan is:
 * - In the first round, you complete 3 tasks of difficulty level 2.
 * - In the second round, you complete 2 tasks of difficulty level 3.
 * - In the third round, you complete 3 tasks of difficulty level 4.
 * - In the fourth round, you complete 2 tasks of difficulty level 4.
 * It can be shown that all the tasks cannot be completed in fewer than 4 rounds, so the answer is 4.
 * Example 2:
 *
 * Input: tasks = [2,3,3]
 * Output: -1
 * Explanation: There is only 1 task of difficulty level 2, but in each round, you can only complete either 2 or 3 tasks of the same difficulty level. Hence, you cannot complete all the tasks, and the answer is -1.
 *
 * Constraints:
 *
 * 1 <= tasks.length <= 105
 * 1 <= tasks[i] <= 109
 */
/**
 * Math solution.
 *  say task_cnt is n
 *    if n = 3 * k, then needs k rounds
 *    if n = 3 * k + 1 = 3 * (k - 1) + 2 + 2, needs k + 1 rounds
 *    if n = 3 * k + 2, then needs k + 1 rounds;
 */
class Solution {
public:
    int minimumRounds(vector<int>& tasks) {
        auto task_cnt_level = unordered_map<int, int>{};
        for (auto t : tasks) {
            ++task_cnt_level[t];
        }

        auto ans = 0;
        for (auto [lv, cnt] : task_cnt_level) {
            if (1 == cnt) {
                ans = -1;
                break;
            }

            ans += (cnt + 2) / 3;
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int minimumRounds(vector<int>& tasks) {
        auto task_cnt_level = unordered_map<int, int>{};
        for (auto t : tasks) {
            ++task_cnt_level[t];
        }

        auto dp = vector<int>(tasks.size() + 1, 1);
        if (4 < dp.size()) {
            dp[4] = 2;
        }

        for (auto i = 5; i < dp.size(); ++i) {
            dp[i] = min(dp[i - 3] + 1, dp[i - 2] + 1);
        }

        auto ans = 0;
        for (auto [lv, cnt] : task_cnt_level) {
            if (1 == cnt) {
                ans = -1;
                break;
            }
            ans += dp[cnt];
        }

        return ans;
    }
};
