/**
 * Cheapest Flights Within K Stops
 *
 * There are n cities connected by some number of flights. You are given an array flights where
 * flights[i] = [fromi, toi, pricei] indicates that there is a flight from city fromi to city toi
 * with cost pricei.
 *
 * You are also given three integers src, dst, and k, return the cheapest price from src to dst with
 * at most k stops. If there is no such route, return -1.
 *
 * Example 1:
 *
 * Input: n = 3, flights = [[0,1,100],[1,2,100],[0,2,500]], src = 0, dst = 2, k = 1
 * Output: 200
 * Explanation: The graph is shown.
 * The cheapest price from city 0 to city 2 with at most 1 stop costs 200, as marked red in the picture.
 * Example 2:
 *
 * Input: n = 3, flights = [[0,1,100],[1,2,100],[0,2,500]], src = 0, dst = 2, k = 0
 * Output: 500
 * Explanation: The graph is shown.
 * The cheapest price from city 0 to city 2 with at most 0 stop costs 500, as marked blue in the picture.
 *
 * Constraints:
 *
 * 1 <= n <= 100
 * 0 <= flights.length <= (n * (n - 1) / 2)
 * flights[i].length == 3
 * 0 <= fromi, toi < n
 * fromi != toi
 * 1 <= pricei <= 104
 * There will not be any multiple flights between two cities.
 * 0 <= src, dst, k < n
 * src != dst
 */
/**
 * DP solution. Not very fast.
 */
class Solution {
public:
    int findCheapestPrice(int n, vector<vector<int>>& flights, int s, int d, int K) {
        const int INF = 1e9;
        vector<vector<int>> dp(K + 2, vector<int>(n, INF));
        dp[0][s] = 0;

        for (int i = 1; i <= K + 1; ++i) {
            dp[i][s] = 0;
            for (const auto& x : flights) {
                dp[i][x[1]] = min(dp[i][x[1]], dp[i-1][x[0]] + x[2]);
            }
        }
        return dp[K + 1][d] >= INF ? -1 : dp[K + 1][d];
    }
};

/**
 * Priority quque. Fastest one.
 */
typedef tuple<int,int,int> ti;
class Solution {
public:
    int findCheapestPrice(int n, vector<vector<int>>& flights, int src, int dst, int K) {
        vector<vector<pair<int,int>>> vp(n);

        for(const auto&f : flights) {
            vp[f[0]].emplace_back(f[1], f[2]);
        }

        vector<int> city_cost(n, numeric_limits<int>::max());
        priority_queue<ti> pq;
        pq.emplace(K + 1, 0, src);

        while (!pq.empty()) {
            auto const [moves, cost, from] = pq.top(); pq.pop();

            if (moves > 0) {
                for (auto const& [to, price] : vp[from]) {
                    auto new_cost = cost + price;
                    if (new_cost < city_cost[to]) {
                        city_cost[to] = new_cost;
                        pq.emplace(moves - 1, new_cost, to);
                    }
                }
            }
        }

        return numeric_limits<int>::max() == city_cost[dst] ? -1 : city_cost[dst];
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int findCheapestPrice(int n, vector<vector<int>>& flights, int src, int dst, int k) {
        unordered_map<int, vector<pair<int, int>>> city_flights;
        for (auto const& fi : flights) {
            city_flights[fi[0]].emplace_back(make_pair(fi[1], fi[2]));
        }

        vector<int> city_cost(n, numeric_limits<int>::max());
        vector<pair<int,int>> cities = { {src, 0} };
        int K = k + 1;

        for (int i = 0; i < K; ++i) {
            vector<pair<int,int>> next_cities;
            for (auto const& [city, cost] : cities) {
                for (auto const& [to, price] : city_flights[city]) {
                    auto new_cost = cost + price;
                    if (new_cost < city_cost[to]) {
                        city_cost[to] = new_cost;
                        next_cities.emplace_back(make_pair(to, new_cost));
                    }
                }
            }

            swap(cities, next_cities);
        }

        return numeric_limits<int>::max() == city_cost[dst] ? -1 : city_cost[dst];
    }
};
