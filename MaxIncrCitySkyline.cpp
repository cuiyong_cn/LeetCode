/**
 * In a 2 dimensional array grid, each value grid[i][j] represents the height of a
 * building located there. We are allowed to increase the height of any number of
 * buildings, by any amount (the amounts can be different for different buildings).
 * Height 0 is considered to be a building as well. 
 *
 * At the end, the "skyline" when viewed from all four directions of the grid, i.e. top,
 * bottom, left, and right, must be the same as the skyline of the original grid.
 * A city's skyline is the outer contour of the rectangles formed by all the buildings
 * when viewed from a distance. See the following example.
 *
 * What is the maximum total sum that the height of the buildings can be increased?
 *
 * Example:
 * Input: grid = [[3,0,8,4],[2,4,5,7],[9,2,6,3],[0,3,1,0]]
 * Output: 35
 * Explanation: 
 * The grid is:
 * [ [3, 0, 8, 4], 
 *   [2, 4, 5, 7],
 *   [9, 2, 6, 3],
 *   [0, 3, 1, 0] ]
 * 
 * The skyline viewed from top or bottom is: [9, 4, 8, 7]
 * The skyline viewed from left or right is: [8, 7, 9, 3]
 * 
 * The grid after increasing the height of buildings without affecting skylines is:
 * 
 * gridNew = [ [8, 4, 8, 7],
 *             [7, 4, 7, 7],
 *             [9, 4, 8, 7],
 *             [3, 3, 3, 3] ]
 * 
 * Notes:
 * 
 *     1 < grid.length = grid[0].length <= 50.
 *     All heights grid[i][j] are in the range [0, 100].
 *     All buildings in grid[i][j] occupy the entire grid cell: that is, they are
 *     a 1 x 1 x grid[i][j] rectangular prism.
 */ 

/**
 * This is solution works for grid M*N. But this solution is not
 * very efficient. Its time complexity is O(n^2)
 */
class Solution {
public:
    int maxIncreaseKeepingSkyline(vector<vector<int>>& grid) {
        int width = grid[0].size();
        int height = grid.size();
        
        std::vector<int> top2bottom;
        std::vector<int> left2right;
        
        for (int h = 0; h < height; h++) {
            int wmax = 0;
            for (int w = 0; w < width; w++) {
                wmax = wmax >= grid[h][w] ? wmax : grid[h][w];
            }
            left2right.push_back(wmax);
        }
        
        for (int w = 0; w < width; w++) {
            int hmax = 0;
            for (int h = 0; h < height; h++) {
                hmax = hmax >= grid[h][w] ? hmax : grid[h][w];
            }
            top2bottom.push_back(hmax);
        }
        
        int incrMax = 0;
        for (int h = 0; h < height; h++) {
            int min = left2right[h];
            for (int w = 0; w < width; w++) {
                int limit = top2bottom[w] >= min ? min : top2bottom[w];
                incrMax += limit - grid[h][w];
            }
        }
        
        return incrMax;
    }
};
