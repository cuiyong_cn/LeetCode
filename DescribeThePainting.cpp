/**
 * Describe the Painting
 *
 * There is a long and thin painting that can be represented by a number line. The painting was
 * painted with multiple overlapping segments where each segment was painted with a unique color.
 * You are given a 2D integer array segments, where segments[i] = [starti, endi, colori] represents
 * the half-closed segment [starti, endi) with colori as the color.
 *
 * The colors in the overlapping segments of the painting were mixed when it was painted. When two
 * or more colors mix, they form a new color that can be represented as a set of mixed colors.
 *
 * For example, if colors 2, 4, and 6 are mixed, then the resulting mixed color is {2,4,6}.  For the
 * sake of simplicity, you should only output the sum of the elements in the set rather than the
 * full set.
 *
 * You want to describe the painting with the minimum number of non-overlapping half-closed segments
 * of these mixed colors. These segments can be represented by the 2D array painting where
 * painting[j] = [leftj, rightj, mixj] describes a half-closed segment [leftj, rightj) with the
 * mixed color sum of mixj.
 *
 * For example, the painting created with segments = [[1,4,5],[1,7,7]] can be described by painting
 * = [[1,4,12],[4,7,7]] because: [1,4) is colored {5,7} (with a sum of 12) from both the first and
 * second segments.  [4,7) is colored {7} from only the second segment.  Return the 2D array
 * painting describing the finished painting (excluding any parts that are not painted). You may
 * return the segments in any order.
 *
 * A half-closed segment [a, b) is the section of the number line between points a and b including
 * point a and not including point b.
 *
 * Example 1:
 *
 * Input: segments = [[1,4,5],[4,7,7],[1,7,9]]
 * Output: [[1,4,14],[4,7,16]]
 * Explanation: The painting can be described as follows:
 * - [1,4) is colored {5,9} (with a sum of 14) from the first and third segments.
 * - [4,7) is colored {7,9} (with a sum of 16) from the second and third segments.
 * Example 2:
 *
 * Input: segments = [[1,7,9],[6,8,15],[8,10,7]]
 * Output: [[1,6,9],[6,7,24],[7,8,15],[8,10,7]]
 * Explanation: The painting can be described as follows:
 * - [1,6) is colored 9 from the first segment.
 * - [6,7) is colored {9,15} (with a sum of 24) from the first and second segments.
 * - [7,8) is colored 15 from the second segment.
 * - [8,10) is colored 7 from the third segment.
 * Example 3:
 *
 * Input: segments = [[1,4,5],[1,4,7],[4,7,1],[4,7,11]]
 * Output: [[1,4,12],[4,7,12]]
 * Explanation: The painting can be described as follows:
 * - [1,4) is colored {5,7} (with a sum of 12) from the first and second segments.
 * - [4,7) is colored {1,11} (with a sum of 12) from the third and fourth segments.
 * Note that returning a single segment [1,7) is incorrect because the mixed color sets are different.
 *
 * Constraints:
 *
 * 1 <= segments.length <= 2 * 104
 * segments[i].length == 3
 * 1 <= starti < endi <= 105
 * 1 <= colori <= 109
 * Each colori is distinct.*
 */
/**
 * Line sweep to optimize.
 */
class Solution {
public:
    vector<vector<long long>> splitPainting(vector<vector<int>>& segments) {
        map<int, long long> d;

        for (auto const& seg : segments) {
            d[seg[0]] += seg[2];
            d[seg[1]] -= seg[2];
        }

        vector<vector<long long>> ans;

        int prev = d.begin()->first;
        long long color = 0;

        for (auto const& it : d) {
            if (prev < it.first) {
                if (color > 0) {
                    ans.push_back({prev, it.first, color});
                }
            }

            prev = it.first;
            color += it.second;
        }

        return ans;
    }
};

/**
 * Original solution. TLE.
 */
template<typename T>
using MinHeap = priority_queue<T, vector<T>, std::greater<T>>;

template<typename T>
vector<T> make_vec(T a, T b, T c)
{
    return {a, b, c};
}

class Solution {
public:
    vector<vector<long long>> splitPainting(vector<vector<int>>& segments) {
        vector<vector<long long>> ans;

        MinHeap<vector<long long>> segs;

        for (auto const& seg : segments) {
            segs.emplace(vector<long long>{seg[0], seg[1], seg[2]});
        }

        while (segs.size() > 1) {
            auto seg1 = std::move(segs.top());
            segs.pop();
            if (seg1[1] <= segs.top()[0]) {
                ans.emplace_back(std::move(seg1));
            }
            else {
                auto seg2 = std::move(segs.top());
                segs.pop();
                if (seg1[0] < seg2[0]) {
                    segs.emplace(make_vec(seg1[0], seg2[0], seg1[2]));
                }

                auto mid = min(seg1[1], seg2[1]);
                segs.emplace(make_vec(seg2[0], mid, seg1[2] + seg2[2]));

                if (mid < seg1[1]) {
                    segs.emplace(make_vec(mid, seg1[1], seg1[2]));
                }

                if (mid < seg2[1]) {
                    segs.emplace(make_vec(mid, seg2[1], seg2[2]));
                }
            }
        }

        ans.emplace_back(segs.top());

        return ans;
    }
};
