/**
 * Intersection of Two Arrays
 *
 * Given two arrays, write a function to compute their intersection.
 *
 * Example 1:
 *
 * Input: nums1 = [1,2,2,1], nums2 = [2,2]
 * Output: [2]
 *
 * Example 2:
 *
 * Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
 * Output: [9,4]
 *
 * Note:
 *
 *     Each element in the result must be unique.
 *     The result can be in any order.
 */
/**
 * A little improvement. And may be put the smaller one in the set
 * is a good idea. And return ans if either vector is empty.
 */
class Solution {
public:
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        vector<int> ans;
        std::unordered_set<int> set(nums1.cbegin(), nums1.cend());
        for (auto& n: nums2){
            if (set.erase(n) > 0){ // if n exists in set, then 1 is returned and n is erased; otherwise, 0.
                ans.push_back(n);
            }
        }
        return ans;
    }
};
/**
 * Simple one
 */
class Solution {
public:
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        unordered_set<int> set1;
        unordered_set<int> set2;
        vector<int> ans;
        for (auto& n : nums1) {
            set1.insert(n);
        }
        for (auto& n : nums2) {
            set2.insert(n);
        }

        for (auto& s : set1) {
            if (1 == set2.count(s)) {
                ans.push_back(s);
            }
        }
        return ans;
    }
};
