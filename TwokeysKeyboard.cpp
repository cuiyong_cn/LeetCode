/**
 * 2 Keys Keyboard
 *
 * Initially on a notepad only one character 'A' is present. You can perform two operations on this notepad for each step:
 *     Copy All: You can copy all the characters present on the notepad (partial copy is not allowed).
 *     Paste: You can paste the characters which are copied last time.
 *
 * Given a number n. You have to get exactly n 'A' on the notepad by performing the minimum number of steps permitted.
 * Output the minimum number of steps to get n 'A'.
 *
 * Example 1:
 *
 * Input: 3
 * Output: 3
 * Explanation:
 * Intitally, we have one character 'A'.
 * In step 1, we use Copy All operation.
 * In step 2, we use Paste operation to get 'AA'.
 * In step 3, we use Paste operation to get 'AAA'.
 *
 * Note:
 *     The n will be in the range [1, 1000].
 */
/**
 * Although my original solution is correct. But it does not show its true logic.
 * This problem is also a math problem: prime factorization.
 *
 * We can break our moves into groups of (copy, paste, ..., paste). Let C denote copying and P denote pasting. Then for
 * example, in the sequence of moves CPPCPPPPCP, the groups would be [CPP][CPPPP][CP].
 *
 * Say these groups have lengths g_1, g_2, .... After parsing the first group, there are g_1 'A's. After parsing the
 * second group, there are g_1 * g_2 'A's, and so on. At the end, there are g_1 * g_2 * ... * g_n 'A's.
 *
 * We want exactly N = g_1 * g_2 * ... * g_n. If any of the g_i are composite, say g_i = p * q, then we can split this
 * group into two groups (the first of which has one copy followed by p-1 pastes, while the second group having one copy
 * and q-1 pastes).
 *
 * Such a split never uses more moves: we use p+q moves when splitting, and pq moves previously. As p+q <= pq is
 * equivalent to 1 <= (p-1)(q-1), which is true as long as p >= 2 and q >= 2.
 *
 * Algorithm By the above argument, we can suppose g_1, g_2, ... is the prime factorization of N, and the answer is
 * therefore the sum of these prime factors.
 */
class Solution
{
    int minSteps(int n)
    {
        int ans = 0, d = 2;
        while (n > 1) {
            while (n % d == 0) {
                ans += d;
                n /= d;
            }
            d++;
        }
        return ans;
    }
}

/**
 * original solution.
 */
bool is_prime(long N)
{
    if (N < 2) return false;
    if (N < 4) return true;
    if ((N & 1) == 0) return false;
    if (N % 3 == 0) return false;

    int curr = 5, s = sqrt(N);

    // Loop to check if any number number is divisible by any other number or not
    while (curr <= s) {
        if (N % curr == 0) return false;
        curr += 2;

        if (N % curr == 0) return false;
        curr += 4;
    }

    return true;
}

int max_denominator(int n)
{
    int i = n / 2;

    while (i > 0) {
        if (n % i == 0) break;
        --i;
    }

    return i;
}

class Solution
{
public:
    int minSteps(int n)
    {
        if (1 == n) return 0;

        if (is_prime(n)) return n;

        if ((n & 1) == 0) return minSteps(n >> 1) + 2;

        int d = max_denominator(n);

        return minSteps(d) + (n / d);
    }
};

