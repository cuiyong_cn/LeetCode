/**
 * Uncrossed Lines
 *
 * We write the integers of A and B (in the order they are given) on two separate horizontal lines.
 *
 * Now, we may draw connecting lines: a straight line connecting two numbers A[i] and B[j] such that:
 *
 * A[i] == B[j];
 * The line we draw does not intersect any other connecting (non-horizontal) line.
 * Note that a connecting lines cannot intersect even at the endpoints: each number can only belong to one connecting line.
 *
 * Return the maximum number of connecting lines we can draw in this way.
 *
 * Example 1:
 * 1   4   2
 * |     \
 * |      \
 * 1   2   4
 *
 * Input: A = [1,4,2], B = [1,2,4]
 * Output: 2
 * Explanation: We can draw 2 uncrossed lines as in the diagram.
 * We cannot draw 3 uncrossed lines, because the line from A[1]=4 to B[2]=4 will intersect the line from A[2]=2 to B[1]=2.
 * Example 2:
 *
 * Input: A = [2,5,1,2,5], B = [10,5,2,1,5,2]
 * Output: 3
 * Example 3:
 *
 * Input: A = [1,3,7,1,7,5], B = [1,9,2,5,1]
 * Output: 2
 *
 * Note:
 *
 * 1 <= A.length <= 500
 * 1 <= B.length <= 500
 * 1 <= A[i], B[i] <= 2000
 */
/**
 * Non-recursive dp solution. Longest common subseqence.
 */
class Solution {
public:
    vector<vector<int>> dp;

    int maxUncrossedLines(vector<int>& A, vector<int>& B) {
        int m = A.size(), n = B.size();
        dp = vector<vector<int>>(m + 1, vector<int>(n + 1, 0));
        for (int i = 1; i <= m; ++i) {
            for (int j = 1; j <= n; ++j) {
                dp[i][j] = (A[i - 1] == B [j - 1]) ? dp[i - 1][j - 1] + 1 : max(dp[i][j - 1], dp[i - 1][j]);
            }
        }

        return dp[m][n];
    }
};

/**
 * Improved version.
 */
class Solution {
public:
    vector<vector<int>> dp;

    int dps(vector<int>& A, int sa, vector<int>& B, int sb) {
        if (sa >= A.size() || sb >= B.size()) return 0;

        if (dp[sa][sb] > -1) return dp[sa][sb];

        int nj = sb;
        while (nj < B.size() && A[sa] != B[nj]) ++nj;

        dp[sa][sb] = max(dps(A, sa + 1, B, sb), (nj < B.size() ? 1 : 0) + dps(A, sa + 1, B, nj + 1));
        return dp[sa][sb];
    }

    int maxUncrossedLines(vector<int>& A, vector<int>& B) {
        dp = vector<vector<int>>(A.size(), vector<int>(B.size(), -1));
        return dps(A, 0, B, 0);
    }
};

/**
 * Original solution. Using dp method.
 */
class Solution {
public:
    vector<vector<int>> dp;

    int dps(vector<int>& A, int sa, vector<int>& B, int sb) {
        if (sa >= A.size() || sb >= B.size()) return 0;

        if (dp[sa][sb] > 0) return dp[sa][sb];

        int maxLines = 0;
        for (int i = sa; i < A.size(); ++i) {
            for (int j = sb; j < B.size(); ++j) {
                if (A[i] == B[j]) {
                    maxLines = max(maxLines, 1 + dps(A, i + 1, B, j + 1));
                }
            }
        }

        dp[sa][sb] = maxLines;
        return maxLines;
    }

    int maxUncrossedLines(vector<int>& A, vector<int>& B) {
        dp = vector<vector<int>>(A.size(), vector<int>(B.size(), -1));
        return dps(A, 0, B, 0);
    }
};
