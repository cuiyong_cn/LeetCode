/**
 * HTML Entity Parser
 *
 * HTML entity parser is the parser that takes HTML code as input and replace all the entities of
 * the special characters by the characters itself.
 *
 * The special characters and their entities for HTML are:
 *
 * Quotation Mark: the entity is &quot; and symbol character is ".
 * Single Quote Mark: the entity is &apos; and symbol character is '.
 * Ampersand: the entity is &amp; and symbol character is &.
 * Greater Than Sign: the entity is &gt; and symbol character is >.
 * Less Than Sign: the entity is &lt; and symbol character is <.
 * Slash: the entity is &frasl; and symbol character is /.
 * Given the input text string to the HTML parser, you have to implement the entity parser.
 *
 * Return the text after replacing the entities by the special characters.
 *
 * Example 1:
 *
 * Input: text = "&amp; is an HTML entity but &ambassador; is not."
 * Output: "& is an HTML entity but &ambassador; is not."
 * Explanation: The parser will replace the &amp; entity by &
 * Example 2:
 *
 * Input: text = "and I quote: &quot;...&quot;"
 * Output: "and I quote: \"...\""
 *
 * Constraints:
 *
 * 1 <= text.length <= 105
 * The string may contain any possible characters out of all the 256 ASCII characters.
 */
/**
 * Regex solution. Not that efficient.
 * But hey, it is really short and intuitive. Also note that we need to handle &amp; lastly
 */
vector<pair<regex, string>> entities =
{
    {regex{"&quot;"}, "\""},
    {regex{"&apos;"}, "'"},
    {regex{"&gt;"}, ">"},
    {regex{"&lt;"}, "<"},
    {regex{"&frasl;"}, "/"},
    {regex{"&amp;"}, "&"},
};

class Solution {
public:
    string entityParser(string text) {
        for (auto& [re, fmt] : entities) {
            text = regex_replace(text, re, fmt);
        }

        return text;
    }
};

/**
 * Original solution.
 */
unordered_map<string, char> entities =
{
    {"&quot;", '"'},
    {"&apos;", '\''},
    {"&amp;", '&'},
    {"&gt;", '>'},
    {"&lt;", '<'},
    {"&frasl;", '/'}
};

class Solution {
public:
    string entityParser(string text) {
        string ans;
        int const N = text.length();
        for (int i = 0; i < N; ++i) {
            if ('&' == text[i]) {
                int j = i + 1, cnt = 0;
                for (; j < N and cnt < 6; ++j, ++cnt) {
                    if (';' == text[j] or '&' == text[j]) {
                        break;
                    }
                }

                if (j < N and ';' == text[j]) {
                    auto entity = text.substr(i, cnt + 2);
                    if (entities.count(entity)) {
                        ans.push_back(entities[entity]);
                    }
                    else {
                        ans += entity;
                    }
                    i = j;
                }
                else {
                    ans.push_back(text[i]);
                }
            }
            else {
                ans.push_back(text[i]);
            }
        }

        return ans;
    }
};
