/**
 * House Robber
 *
 * You are a professional robber planning to rob houses along a street. Each house has a certain
 * amount of money stashed, the only constraint stopping you from robbing each of them is that
 * adjacent houses have security system connected and it will automatically contact the police if
 * two adjacent houses were broken into on the same night.
 *
 * Given a list of non-negative integers representing the amount of money of each house, determine
 * the maximum amount of money you can rob tonight without alerting the police.
 *
 * Example 1:
 *
 * Input: nums = [1,2,3,1]
 * Output: 4
 * Explanation: Rob house 1 (money = 1) and then rob house 3 (money = 3).
 *              Total amount you can rob = 1 + 3 = 4.
 * Example 2:
 *
 * Input: nums = [2,7,9,3,1]
 * Output: 12
 * Explanation: Rob house 1 (money = 2), rob house 3 (money = 9) and rob house 5 (money = 1).
 *              Total amount you can rob = 2 + 9 + 1 = 12.
 *
 * Constraints:
 *
 * 0 <= nums.length <= 100
 * 0 <= nums[i] <= 400
 */
/**
 * Improved one.
 */
class Solution {
public:
    int rob(vector<int>& nums) {
        if (nums.empty()) {
            return 0;
        }
        int prev1 = 0, prev2 = 0;
        for (auto n : nums) {
            int save = prev1;
            prev1 = std::max(prev2 + n, prev1);
            prev2 = save;
        }

        return std::max(prev1, prev2);
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int rob(vector<int>& nums) {
        int house_num = nums.size();
        if (0 == house_num) {
            return 0;
        }
        if (1 == house_num) {
            return nums.front();
        }
        if (2 == house_num) {
            return std::max(nums[0], nums[1]);
        }

        vector<vector<int>> dp(house_num, vector<int>(2, 0));
        dp[0][0] = 0; dp[0][1] = nums[0];
        dp[1][0] = nums[0]; dp[1][1] = nums[1];
        for (int i = 2; i < nums.size(); ++i) {
            dp[i][0] = std::max(dp[i - 1][1], dp[i - 1][0]);
            dp[i][1] = std::max(dp[i - 1][0], dp[i - 2][1]) + nums[i];
        }

        return std::max(dp[house_num - 1][0], dp[house_num - 1][1]);
    }
};
