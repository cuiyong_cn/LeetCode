/**
 * Single Number
 *
 * Given a non-empty array of integers, every element appears twice except for one. Find that single one.
 *
 * Note:
 *
 * Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?
 *
 * Example 1:
 *
 * Input: [2,2,1]
 * Output: 1
 *
 * Example 2:
 *
 * Input: [4,1,2,1,2]
 * Output: 4
 */
/**
 * First most efficient one. Use XOR
 *
 * a ^ 0 = a
 * a ^ a = 0
 * a ^ b ^ a = a ^ a ^ b = b
 */
class Solution {
public:
    int singleNumber(vector<int>& nums) {
        int ans = 0;
        for (auto n : nums) {
            ans ^= n;
        }

        return ans;
    }
};

/**
 * Using math.
 *
 * 2 * (a + b + c) - (a + a + b + b + c) = c
 */
class Solution {
public:
    int singleNumber(vector<int>& nums) {
        int sum = 0;
        unordered_set<int> iset;
        for (auto n : nums) {
            iset.insert(n);
            sum += n;
        }

        int setSum = 0;
        for (auto n : iset) {
            setSum += 2 * n;
        }

        return setSum - sum;
    }
};

/**
 * Using unordered_set method
 */
class Solution {
public:
    int singleNumber(vector<int>& nums) {
        unordered_set<int> iset;
        int ans = 0;
        for (auto n : nums) {
            if (iset.count(n) == 0) {
                iset.insert(n);
            }
            else {
                iset.erase(n);
            }
        }

        for (auto n : iset) {
            ans = n;
        }

        return ans;
    }
};

/**
 * Original version. Pretty shappy.
 */
class Solution {
public:
    int singleNumber(vector<int>& nums) {
        int ans;
        for (int i = 0; i < nums.size(); ++i) {
            bool foundDup = false;
            for (int j = i + 1; j < nums.size(); ++j) {
                if (nums[i] == nums[j]) {
                    foundDup = true;
                    swap(nums[i + 1], nums[j]);
                    ++i;
                    break;
                }
            }
            if (foundDup == false) {
                ans = nums[i];
                break;
            }
        }

        return ans;
    }
};
