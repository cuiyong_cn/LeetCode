/**
 * Palindrome Linked List
 *
 * Given the head of a singly linked list, return true if it is a palindrome.
 *
 * Example 1:
 *
 * Input: head = [1,2,2,1]
 * Output: true
 * Example 2:
 *
 * Input: head = [1,2]
 * Output: false
 *
 * Constraints:
 *
 * The number of nodes in the list is in the range [1, 105].
 * 0 <= Node.val <= 9
 *
 * Follow up: Could you do it in O(n) time and O(1) space?
 */
/**
 * For the follow up, I think we use a slow and fast pointer
 * to determine the middle node. The reverse the second half
 * list, then compare backwords. But modifying the input list
 * is not a good idea.
 */

/**
 * Reduce half the space.
 */
class Solution {
public:
    bool isPalindrome(ListNode* head) {
        vector<int> vec;
        auto slow = head;
        auto fast = head;
        while (fast->next && fast->next->next) {
            vec.push_back(slow->val);
            slow = slow->next;
            fast = fast->next->next;
        }

        if (fast->next) {
            vec.push_back(slow->val);
        }
        slow = slow->next;

        while (slow) {
            if (slow->val != vec.back()) {
                return false;
            }
            vec.pop_back();
            slow = slow->next;
        }

        return true;
    }
};

/**
 * Original solution.
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    bool isPalindrome(ListNode* head) {
        vector<int> vec;
        for (auto node = head; node; node = node->next) {
            vec.push_back(node->val);
        }

        int front = 0, back = vec.size() - 1;
        while (front < back) {
            if (vec[front] != vec[back]) {
                return false;
            }
            ++front;
            --back;
        }

        return true;
    }
};
