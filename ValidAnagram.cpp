/**
 * Valid Anagram
 *
 * Given two strings s and t , write a function to determine if t is an anagram of s.
 *
 * Example 1:
 *
 * Input: s = "anagram", t = "nagaram"
 * Output: true
 *
 * Example 2:
 *
 * Input: s = "rat", t = "car"
 * Output: false
 *
 * Note:
 * You may assume the string contains only lowercase alphabets.
 *
 * Follow up:
 * What if the inputs contain unicode characters? How would you adapt your solution to such case?
 *
 * Use a hash table instead of a fixed size counter. Imagine allocating a large size array to fit the entire range of
 * unicode characters, which could go up to more than 1 million. A hash table is a more generic solution and could
 * adapt to any range of characters.
 */
/**
 * More efficient version.
 */
class Solution {
public:
    bool isAnagram(string s, string t) {
        vector<int> alphabets(26, 0);

        bool ans = true;
        if (s.length() == t.length()) {
            for (int i = 0; i < s.length(); ++i) {
                ++alphabets[s[i] - 'a'];
                --alphabets[t[i] - 'a'];
            }

            for (auto& n : alphabets) {
                if (0 != n) {
                    ans = false;
                    break;
                }
            }
        }
        else {
            ans = false;
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool isAnagram(string s, string t) {
        bool ans = false;
        if (s.length() == t.length()) {
            sort(s.begin(), s.end());
            sort(t.begin(), t.end());
            ans = s == t;
        }

        return ans;
    }
};
