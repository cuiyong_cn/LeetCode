/**
 * Smallest Number in Infinite Set
 *
 * You have a set which contains all positive integers [1, 2, 3, 4, 5, ...].
 *
 * Implement the SmallestInfiniteSet class:
 *
 * SmallestInfiniteSet() Initializes the SmallestInfiniteSet object to contain all positive integers.
 * int popSmallest() Removes and returns the smallest integer contained in the infinite set.
 * void addBack(int num) Adds a positive integer num back into the infinite set, if it is not
 * already in the infinite set.
 *
 * Example 1:
 *
 * Input
 * ["SmallestInfiniteSet", "addBack", "popSmallest", "popSmallest", "popSmallest", "addBack", "popSmallest", "popSmallest", "popSmallest"]
 * [[], [2], [], [], [], [1], [], [], []]
 * Output
 * [null, null, 1, 2, 3, null, 1, 4, 5]
 *
 * Explanation
 * SmallestInfiniteSet smallestInfiniteSet = new SmallestInfiniteSet();
 * smallestInfiniteSet.addBack(2);    // 2 is already in the set, so no change is made.
 * smallestInfiniteSet.popSmallest(); // return 1, since 1 is the smallest number, and remove it from the set.
 * smallestInfiniteSet.popSmallest(); // return 2, and remove it from the set.
 * smallestInfiniteSet.popSmallest(); // return 3, and remove it from the set.
 * smallestInfiniteSet.addBack(1);    // 1 is added back to the set.
 * smallestInfiniteSet.popSmallest(); // return 1, since 1 was added back to the set and
 *                                    // is the smallest number, and remove it from the set.
 * smallestInfiniteSet.popSmallest(); // return 4, and remove it from the set.
 * smallestInfiniteSet.popSmallest(); // return 5, and remove it from the set.
 *
 * Constraints:
 *
 * 1 <= num <= 1000
 * At most 1000 calls will be made in total to popSmallest and addBack.*
 */
/**
 * Based on original. Different thinking.
 */

class SmallestInfiniteSet {
public:
    SmallestInfiniteSet() {

    }

    int popSmallest() {
        if (m_numers_addback.empty()) {
            return m_cur++;
        }

        auto iter_smallest = m_numers_addback.begin();
        auto smallest = *iter_smallest;
        m_numers_addback.erase(iter_smallest);
        return smallest;
    }

    void addBack(int num) {
        if (m_cur > num) {
            m_numers_addback.insert(num);
        }
    }

private:
    int m_cur = 1;
    set<int> m_numers_addback;
};

/**
 * Give the constaints. Little more efficient. Both speed and space.
 */
class SmallestInfiniteSet {
public:
    SmallestInfiniteSet() : m_number_popout(1001, false) {

    }

    int popSmallest() {
        for (auto i = 1; i < m_number_popout.size(); ++i) {
            auto out = m_number_popout[i];
            if (!out) {
                out = true;
                return i;
            }
        }
        return 0;
    }

    void addBack(int num) {
        if (num < m_number_popout.size()) {
            m_number_popout[num] = false;
        }
    }

private:
    vector<bool> m_number_popout;
};

/**
 * Original solution.
 */
class SmallestInfiniteSet {
public:
    SmallestInfiniteSet() {

    }

    int popSmallest() {
        for (auto i = 1; ; ++i) {
            if (0 == m_number_popout.count(i)) {
                m_number_popout.insert(i);
                return i;
            }
        }
        return 0;
    }

    void addBack(int num) {
        m_number_popout.erase(num);
    }

private:
    unordered_set<int> m_number_popout;
};

/**
 * Your SmallestInfiniteSet object will be instantiated and called as such:
 * SmallestInfiniteSet* obj = new SmallestInfiniteSet();
 * int param_1 = obj->popSmallest();
 * obj->addBack(num);
 */
