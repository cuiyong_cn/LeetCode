/**
 * Find Latest Group of Size M
 *
 * Given an array arr that represents a permutation of numbers from 1 to n. You have a binary string
 * of size n that initially has all its bits set to zero.
 *
 * At each step i (assuming both the binary string and arr are 1-indexed) from 1 to n, the bit at
 * position arr[i] is set to 1. You are given an integer m and you need to find the latest step at
 * which there exists a group of ones of length m. A group of ones is a contiguous substring of 1s
 * such that it cannot be extended in either direction.
 *
 * Return the latest step at which there exists a group of ones of length exactly m. If no such
 * group exists, return -1.
 *
 * Example 1:
 *
 * Input: arr = [3,5,1,2,4], m = 1
 * Output: 4
 * Explanation:
 * Step 1: "00100", groups: ["1"]
 * Step 2: "00101", groups: ["1", "1"]
 * Step 3: "10101", groups: ["1", "1", "1"]
 * Step 4: "11101", groups: ["111", "1"]
 * Step 5: "11111", groups: ["11111"]
 * The latest step at which there exists a group of size 1 is step 4.
 * Example 2:
 *
 * Input: arr = [3,1,5,4,2], m = 2
 * Output: -1
 * Explanation:
 * Step 1: "00100", groups: ["1"]
 * Step 2: "10100", groups: ["1", "1"]
 * Step 3: "10101", groups: ["1", "1", "1"]
 * Step 4: "10111", groups: ["1", "111"]
 * Step 5: "11111", groups: ["11111"]
 * No group of size 2 exists during any step.
 * Example 3:
 *
 * Input: arr = [1], m = 1
 * Output: 1
 * Example 4:
 *
 * Input: arr = [2,1], m = 2
 * Output: 2
 *
 * Constraints:
 *
 * n == arr.length
 * 1 <= n <= 10^5
 * 1 <= arr[i] <= n
 * All integers in arr are distinct.
 * 1 <= m <= arr.length
 */
/**
 * start from the front. Time and space efficient.
 */
class Solution {
public:
    int findLatestStep(vector<int>& A, int m) {
        if (A.size() == m) {
            return m;
        }

        int res = -1;
        int n = A.size();

        vector<int> length(n + 2);
        for (int i = 0; i < n; ++i) {
            int a = A[i];
            int left = length[a - 1];
            int right = length[a + 1];
            length[a - left] = length[a + right] = left + right + 1;

            if (left == m || right == m) {
                res = i;
            }
        }

        return res;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int findLatestStep(vector<int>& arr, int m) {
        if (m == arr.size()) {
            return m;
        }

        set<pair<int, int>> ones_groups{ {1, arr.size()} };

        int steps = arr.size() - 1;
        for (int i = arr.size() - 1; i >= 0; --i) {
            auto idx = arr[i];
            if (ones_groups.empty()) {
                return -1;
            }

            auto ogiter = find_if(begin(ones_groups), end(ones_groups),
                                 [idx](auto const& og) {
                                     return og.first <= idx && idx <= og.second;
                                 });
            if (ogiter != ones_groups.end()) {
                            auto group = *ogiter;
                ones_groups.erase(ogiter);
                if ((idx - group.first) == m || (group.second - idx) == m) {
                    break;
                }

                if ((idx - group.first) > m) {
                    ones_groups.insert({group.first, idx - 1});
                }

                if ((group.second - idx) > m) {
                    ones_groups.insert({idx + 1, group.second});
                }
            }

            --steps;
        }

        return steps;
    }
};
