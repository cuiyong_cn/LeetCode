/**
 * Maximum Erasure Value
 *
 * You are given an array of positive integers nums and want to erase a subarray containing unique
 * elements. The score you get by erasing the subarray is equal to the sum of its elements.
 *
 * Return the maximum score you can get by erasing exactly one subarray.
 *
 * An array b is called to be a subarray of a if it forms a contiguous subsequence of a, that is, if
 * it is equal to a[l],a[l+1],...,a[r] for some (l,r).
 *
 * Example 1:
 *
 * Input: nums = [4,2,4,5,6]
 * Output: 17
 * Explanation: The optimal subarray here is [2,4,5,6].
 * Example 2:
 *
 * Input: nums = [5,2,1,2,5,2,1,2,5]
 * Output: 8
 * Explanation: The optimal subarray here is [5,2,1] or [1,2,5].
 *
 * Constraints:
 *
 * 1 <= nums.length <= 105
 * 1 <= nums[i] <= 104
 */
/**
 * Original solution.
 */
class Solution {
public:
    int maximumUniqueSubarray(vector<int>& nums) {
        int ans = 0;
        int sum = 0;
        int const N = nums.size();
        unordered_map<int, int> num_idx;
        for (int L = 0, R = 0; R < N; ++R) {
            if (num_idx.count(nums[R])) {
                ans = max(ans, sum);

                int const idx = num_idlx[nums[R]] + 1;
                for (; L < idx; ++L) {
                    sum -= nums[L];
                    num_idx.erase(nums[L]);
                }
            }

            sum += nums[R];
            num_idx[nums[R]] = R;
        }

        return max(ans, sum);
    }
};
