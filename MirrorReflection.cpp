/**
 * There is a special square room with mirrors on each of the four walls.  Except for the southwest corner, there are
 * receptors on each of the remaining corners, numbered 0, 1, and 2.

 * The square room has walls of length p, and a laser ray from the southwest corner first meets the east wall at a
 * distance q from the 0th receptor.
 *
 * Return the number of the receptor that the ray meets first.  (It is guaranteed that the ray will meet a receptor eventually.)
 *
 * Example 1:
 *
 * Input: p = 2, q = 1
 * Output: 2
 * Explanation: The ray meets receptor 2 the first time it gets reflected back to the left wall.
 *
 * Note:
 *     1 <= p <= 1000
 *     0 <= q <= p
 */
/**
 * More compact one.
 */
class Solution {
public:
    int mirrorReflection(int p, int q) {
        int gcd = std::gcd(p, q); // std::gcd is availlable in c++17
        int lcm = p * q / gcd;
        int x = lcm / q; // determine the light hit left or right mirror
        int y = lcm / p; // determine the light hit top or bottom mirror

        if ((x & 1) == 0) return 2;
        else if (y & 1) return 1;

        return 0;
    }
};

/**
 * Original solution. Simulate the mirror effect. The idea is
 *
 *   ------0-----------0-----------0-----------0-----------0------
 *   |     |     |     |     |     |     |     |     |     |     |
 *   |     |     |     |     |     |     |     |     |     |     |
 *   2-----1-----2-----1-----2-----1-----2-----1-----2-----1-----2
 *   |     |     |     |     |     |     |     |     |     |     |
 *   |     |     |     |     |     |     |     |     |     |     |
 *   ------0-----------0-----------0-----------0-----------0------
 *   |     |     |     |     |     |     |     |     |     |     |
 *   |     |     |     |     |     |     |     |     |     |     |
 *   2-----1-----2-----1-----2-----1-----2-----1-----2-----1-----2
 *   |     |     |     |     |     |     |     |     |     |     |
 *   |     |     |     |     |     |     |     |     |     |     |
 *   ------0-----------0-----------0-----------0-----------0------
 */
class Solution {
public:
    int mirrorReflection(int p, int q) {
        int dp = p;
        int dq = q;
        int mirrorLR = 0;
        int mirrorTB = 0;
        vector<vector<int>> receptor{{2, 1}, {3, 0}};
        while (1) {
            if (p == q) break;
            if (p > q) {
                q += dq;
                ++mirrorLR;
            }
            else {
                p += dp;
                ++mirrorTB;
            }
        }

        mirrorLR = mirrorLR % 2;
        mirrorTB = mirrorTB % 2;
        if (mirrorLR) {
            swap(receptor[0][0], receptor[0][1]);
            swap(receptor[1][0], receptor[1][1]);
        }

        if (mirrorTB) {
            swap(receptor[0], receptor[1]);
        }

        return receptor[0][1];
    }
};
