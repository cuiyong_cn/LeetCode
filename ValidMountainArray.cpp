/**
 * Valid Mountain Array
 *
 * Given an array of integers arr, return true if and only if it is a valid mountain array.
 *
 * Recall that arr is a mountain array if and only if:
 *
 * arr.length >= 3
 * There exists some i with 0 < i < arr.length - 1 such that:
 * arr[0] < arr[1] < ... < arr[i - 1] < arr[i]
 * arr[i] > arr[i + 1] > ... > arr[arr.length - 1]
 *
 * Example 1:
 *
 * Input: arr = [2,1]
 * Output: false
 * Example 2:
 *
 * Input: arr = [3,5,5]
 * Output: false
 * Example 3:
 *
 * Input: arr = [0,3,2,1]
 * Output: true
 *
 * Constraints:
 *
 * 1 <= arr.length <= 104
 * 0 <= arr[i] <= 104
 */
/**
 * Make it shorter.
 */
class Solution {
public:
    bool validMountainArray(vector<int>& arr) {
        int const N = arr.size();
        int i = 0;
        while ((i + 1) < N && arr[i] < arr[i + 1]) {
            ++i;
        }

        if (0 == i || (N - 1) == i) {
            return false;
        }

        while ((i + 1) < N && arr[i] > arr[i + 1]) {
            ++i;
        }

        return (N - 1) == i;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool validMountainArray(vector<int>& arr) {
        if (arr.size() < 3 || arr[1] <= arr[0]) {
            return false;
        }

        // find the peak
        int i = 2;
        for (; i < arr.size(); ++i) {
            if (arr[i] == arr[i - 1]) {
                return false;
            }

            if (arr[i] < arr[i - 1]) {
                break;
            }
        }

        // if there is no peak
        if (arr.size() == i) {
            return false;
        }

        for (; i < arr.size(); ++i) {
            if (arr[i] >= arr[i - 1]) {
                return false;
            }
        }

        return true;
    }
};
