/**
* Backspace String Compare
*
* Given two strings S and T, return if they are equal when both are typed into empty text editors. # means a backspace
* character.
*
* Note that after backspacing an empty text, the text will continue empty.
*
* Example 1:
*
* Input: S = "ab#c", T = "ad#c"
* Output: true
* Explanation: Both S and T become "ac".
* Example 2:
*
* Input: S = "ab##", T = "c#d#"
* Output: true
* Explanation: Both S and T become "".
* Example 3:
*
* Input: S = "a##c", T = "#a#c"
* Output: true
* Explanation: Both S and T become "c".
* Example 4:
*
* Input: S = "a#c", T = "b"
* Output: false
* Explanation: S becomes "c" while T becomes "b".
* Note:
*
* 1 <= S.length <= 200
* 1 <= T.length <= 200
* S and T only contain lowercase letters and '#' characters.
* Follow up:
*
* Can you solve it in O(N) time and O(1) space?
*/
/**
 * Again we can reduce the space usage by using two pointer
 */
class Solution {
    public boolean backspaceCompare(String S, String T) {
        int i = S.length() - 1, j = T.length() - 1;
        int skipS = 0, skipT = 0;

        while (i >= 0 || j >= 0) { // While there may be chars in build(S) or build (T)
            while (i >= 0) { // Find position of next possible char in build(S)
                if (S.charAt(i) == '#') {skipS++; i--;}
                else if (skipS > 0) {skipS--; i--;}
                else break;
            }
            while (j >= 0) { // Find position of next possible char in build(T)
                if (T.charAt(j) == '#') {skipT++; j--;}
                else if (skipT > 0) {skipT--; j--;}
                else break;
            }
            // If two actual characters are different
            if (i >= 0 && j >= 0 && S.charAt(i) != T.charAt(j))
                return false;
            // If expecting to compare char vs nothing
            if ((i >= 0) != (j >= 0))
                return false;
            i--; j--;
        }
        return true;
    }
}

/**
 * Original solution.
 */
class Solution {
public:
    bool backspaceCompare(string S, string T) {
        return backspace(S) == backspace(T);
    }

private:
    string backspace(const string& str)
    {
        string ans;

        int bs_count = 0;
        for (auto it = str.rbegin(); it != str.rend(); ++it) {
            if ('#' == *it) {
                ++bs_count;
            }
            else {
                if (bs_count > 0) {
                    --bs_count;
                }
                else {
                    ans.push_back(*it);
                }
            }
        }

        return ans;
    }
};
