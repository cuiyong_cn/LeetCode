/**
 * Knight Dialer
 *
 * The chess knight has a unique movement, it may move two squares vertically and one square horizontally, or two
 * squares horizontally and one square vertically (with both forming the shape of an L). The possible movements of chess
 * knight are shown in this diagaram:
 *
 * A chess knight can move as indicated in the chess diagram below:
 *
 * We have a chess knight and a phone pad as shown below, the knight can only stand on a numeric cell (i.e. blue cell).
 *
 * Given an integer n, return how many distinct phone numbers of length n we can dial.
 *
 * You are allowed to place the knight on any numeric cell initially and then you should perform n - 1 jumps to dial a
 * number of length n. All jumps should be valid knight jumps.
 *
 * As the answer may be very large, return the answer modulo 109 + 7.
 *
 * Example 1:
 *
 * Input: n = 1
 * Output: 10
 * Explanation: We need to dial a number of length 1, so placing the knight over any numeric cell of the 10 cells is
 * sufficient.
 * Example 2:
 *
 * Input: n = 2
 * Output: 20
 * Explanation: All the valid number we can dial are [04, 06, 16, 18, 27, 29, 34, 38, 40, 43, 49, 60, 61, 67, 72, 76, 81, 83, 92, 94]
 * Example 3:
 *
 * Input: n = 3
 * Output: 46
 * Example 4:
 *
 * Input: n = 4
 * Output: 104
 * Example 5:
 *
 * Input: n = 3131
 * Output: 136006598
 * Explanation: Please take care of the mod.
 *
 * Constraints:
 *
 * 1 <= n <= 5000
 */
/**
 * weird. I first do the mod in the swap operation. But does not pass
 * the test???
 *
 * Also use the matrix multiplication can reduce time complexity to LOG(n).
 */
class Solution {
public:
    int knightDialer(int n) {
        if (0 == n) {
            return 0;
        }

        if (1 == n) {
            return 10;
        }

        int nums[10] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
        int mod = 1000000007;
        int i = 2;
        while (i <= n) {
            int next[10] = { 0, };

            next[0] += nums[4]; next[0] = next[0] % mod;
            next[0] += nums[6]; next[0] = next[0] % mod;

            next[1] += nums[6]; next[1] = next[1] % mod;
            next[1] += nums[8]; next[1] = next[1] % mod;

            next[2] += nums[7]; next[2] = next[2] % mod;
            next[2] += nums[9]; next[2] = next[2] % mod;

            next[3] += nums[4]; next[3] = next[3] % mod;
            next[3] += nums[8]; next[3] = next[3] % mod;

            next[4] += nums[0]; next[4] = next[4] % mod;
            next[4] += nums[3]; next[4] = next[4] % mod;
            next[4] += nums[9]; next[4] = next[4] % mod;

            next[6] += nums[0]; next[6] = next[6] % mod;
            next[6] += nums[1]; next[6] = next[6] % mod;
            next[6] += nums[7]; next[6] = next[6] % mod;

            next[7] += nums[2]; next[7] = next[7] % mod;
            next[7] += nums[6]; next[7] = next[7] % mod;

            next[8] += nums[1]; next[8] = next[8] % mod;
            next[8] += nums[3]; next[8] = next[8] % mod;

            next[9] += nums[2]; next[9] = next[9] % mod;
            next[9] += nums[4]; next[9] = next[9] % mod;

            for (int j = 0; j < 10; ++j) {
                nums[j] = next[j];
            }
            ++i;
        }

        int ans = 0;
        for (int i = 0; i < 10; ++i) {
            ans += nums[i];
            ans = ans % mod;
        }

        return ans;
    }
};

/**
 * Original solution. This is correct one. But I don't know how to handle the mod
 * further. My initial thought was using string and do the addition using string.
 * And finnaly manually mod it.
 */
class Solution {
public:
    int knightDialer(int n) {
        if (0 == n) {
            return 0;
        }

        if (1 == n) {
            return 10;
        }

        long long nums[10] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

        int i = 2;
        while (i <= n) {
            long long next[10] = { 0, };

            next[0] += nums[4] + nums[6];
            next[1] += nums[6] + nums[8];
            next[2] += nums[7] + nums[9];
            next[3] += nums[4] + nums[8];
            next[4] += nums[0] + nums[3] + nums[9];
            next[6] += nums[0] + nums[1] + nums[7];
            next[7] += nums[2] + nums[6];
            next[8] += nums[1] + nums[3];
            next[9] += nums[2] + nums[4];

            for (int j = 0; j < 10; ++j) {
                nums[j] = next[j];
            }
            ++i;
        }

        return accumulate(nums, nums + 10, 0);
    }
};
