/**
 * Restore IP Addresses
 *
 * Given a string s containing only digits, return all possible valid IP addresses that can be
 * obtained from s. You can return them in any order.
 *
 * A valid IP address consists of exactly four integers, each integer is between 0 and 255,
 * separated by single dots and cannot have leading zeros. For example, "0.1.2.201" and
 * "192.168.1.1" are valid IP addresses and "0.011.255.245", "192.168.1.312" and "192.168@1.1" are
 * invalid IP addresses.
 *
 * Example 1:
 *
 * Input: s = "25525511135"
 * Output: ["255.255.11.135","255.255.111.35"]
 * Example 2:
 *
 * Input: s = "0000"
 * Output: ["0.0.0.0"]
 * Example 3:
 *
 * Input: s = "1111"
 * Output: ["1.1.1.1"]
 * Example 4:
 *
 * Input: s = "010010"
 * Output: ["0.10.0.10","0.100.1.0"]
 * Example 5:
 *
 * Input: s = "101023"
 * Output: ["1.0.10.23","1.0.102.3","10.1.0.23","10.10.2.3","101.0.2.3"]
 *
 * Constraints:
 *
 * 0 <= s.length <= 3000
 * s consists of digits only.
 */
/**
 * very smart one.
 */
class Solution {
public:
    vector<string> restoreIpAddresses(string s) {
        vector<string> ret;

        if (s.length() > 12) {
            return {};
        }

        for (int a = 1; a <= 3; a++)
        for (int b = 1; b <= 3; b++)
        for (int c = 1; c <= 3; c++)
        for (int d = 1; d <= 3; d++) {
            if ((a + b + c + d) == s.length()) {
                int A = stoi(s.substr(0, a));
                int B = stoi(s.substr(a, b));
                int C = stoi(s.substr(a+b, c));
                int D = stoi(s.substr(a+b+c, d));
                if (A<=255 && B<=255 && C<=255 && D<=255) {
                    if (string ans=to_string(A)+"."+to_string(B)+"."+to_string(C)+"."+to_string(D);
                            ans.length() == s.length()+3) {
                        ret.push_back(ans);
                    }
                }
            }
        }

        return ret;
    }
};

/**
 * Original solution. Clumsy but works.
 */
class Solution {
public:
    vector<string> restoreIpAddresses(string s) {
        vector<string> ans;
        if (s.length() < 13) {
            dfs(ans, s, 0, 1, "");
        }

        return ans;
    }

private:
    void dfs(vector<string>& ans, string const& s, int pos, int part, string cur)
    {
        if (pos >= s.length()) {
            if (5 == part) {
                cur.pop_back();
                ans.emplace_back(std::move(cur));
            }

            return;
        }

        if ('0' == s[pos]) {
            dfs(ans, s, pos + 1, part + 1, cur + "0.");
        }
        else if ('1' == s[pos]) {
            dfs(ans, s, pos + 1, part + 1, cur + "1.");
            if ((pos + 1) < s.length()) {
                dfs(ans, s, pos + 2, part + 1, cur + s.substr(pos, 2) + ".");
            }
            if ((pos + 2) < s.length()) {
                dfs(ans, s, pos + 3, part + 1, cur + s.substr(pos, 3) + ".");
            }
        }
        else if ('2' == s[pos]) {
            dfs(ans, s, pos + 1, part + 1, cur + "2.");
            if ((pos + 1) < s.length()) {
                dfs(ans, s, pos + 2, part + 1, cur + s.substr(pos, 2) + ".");
            }
            if ((pos + 2) < s.length()) {
                if (s[pos + 1] < '5' || ('5' == s[pos + 1] && s[pos + 2] < '6')) {
                    dfs(ans, s, pos + 3, part + 1, cur + s.substr(pos, 3) + ".");
                }
            }
        }
        else {
            dfs(ans, s, pos + 1, part + 1, cur + s.substr(pos, 1) + ".");
            if ((pos + 1) < s.length()) {
                dfs(ans, s, pos + 2, part + 1, cur + s.substr(pos, 2) + ".");
            }
        }
    }
};
