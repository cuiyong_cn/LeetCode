/**
 * Difference Between Ones and Zeros in Row and Column
 *
 * You are given a 0-indexed m x n binary matrix grid.
 *
 * A 0-indexed m x n difference matrix diff is created with the following procedure:
 *
 * Let the number of ones in the ith row be onesRowi.
 * Let the number of ones in the jth column be onesColj.
 * Let the number of zeros in the ith row be zerosRowi.
 * Let the number of zeros in the jth column be zerosColj.
 * diff[i][j] = onesRowi + onesColj - zerosRowi - zerosColj
 * Return the difference matrix diff.
 *
 * Example 1:
 *
 * Input: grid = [[0,1,1],[1,0,1],[0,0,1]]
 * Output: [[0,0,4],[0,0,4],[-2,-2,2]]
 * Explanation:
 * - diff[0][0] = onesRow0 + onesCol0 - zerosRow0 - zerosCol0 = 2 + 1 - 1 - 2 = 0
 * - diff[0][1] = onesRow0 + onesCol1 - zerosRow0 - zerosCol1 = 2 + 1 - 1 - 2 = 0
 * - diff[0][2] = onesRow0 + onesCol2 - zerosRow0 - zerosCol2 = 2 + 3 - 1 - 0 = 4
 * - diff[1][0] = onesRow1 + onesCol0 - zerosRow1 - zerosCol0 = 2 + 1 - 1 - 2 = 0
 * - diff[1][1] = onesRow1 + onesCol1 - zerosRow1 - zerosCol1 = 2 + 1 - 1 - 2 = 0
 * - diff[1][2] = onesRow1 + onesCol2 - zerosRow1 - zerosCol2 = 2 + 3 - 1 - 0 = 4
 * - diff[2][0] = onesRow2 + onesCol0 - zerosRow2 - zerosCol0 = 1 + 1 - 2 - 2 = -2
 * - diff[2][1] = onesRow2 + onesCol1 - zerosRow2 - zerosCol1 = 1 + 1 - 2 - 2 = -2
 * - diff[2][2] = onesRow2 + onesCol2 - zerosRow2 - zerosCol2 = 1 + 3 - 2 - 0 = 2
 * Example 2:
 *
 * Input: grid = [[1,1,1],[1,1,1]]
 * Output: [[5,5,5],[5,5,5]]
 * Explanation:
 * - diff[0][0] = onesRow0 + onesCol0 - zerosRow0 - zerosCol0 = 3 + 2 - 0 - 0 = 5
 * - diff[0][1] = onesRow0 + onesCol1 - zerosRow0 - zerosCol1 = 3 + 2 - 0 - 0 = 5
 * - diff[0][2] = onesRow0 + onesCol2 - zerosRow0 - zerosCol2 = 3 + 2 - 0 - 0 = 5
 * - diff[1][0] = onesRow1 + onesCol0 - zerosRow1 - zerosCol0 = 3 + 2 - 0 - 0 = 5
 * - diff[1][1] = onesRow1 + onesCol1 - zerosRow1 - zerosCol1 = 3 + 2 - 0 - 0 = 5
 * - diff[1][2] = onesRow1 + onesCol2 - zerosRow1 - zerosCol2 = 3 + 2 - 0 - 0 = 5
 *
 * Constraints:
 *
 * m == grid.length
 * n == grid[i].length
 * 1 <= m, n <= 105
 * 1 <= m * n <= 105
 * grid[i][j] is either 0 or 1.
 */
/**
 * Simplified.
 */
class Solution {
public:
    vector<vector<int>> onesMinusZeros(vector<vector<int>>& grid) {
        auto const rows = grid.size();
        auto const cols = grid[0].size();
        auto ones_row = vector<int>(rows, 0);
        auto ones_col = vector<int>(cols, 0);

        for (auto r = rows * 0; r < rows; ++r) {
            for (auto c = cols * 0; c < cols; ++c) {
                ones_row[r] += grid[r][c];
                ones_col[c] += grid[r][c];
            }
        }

        auto ans = vector<vector<int>>(rows, vector<int>(cols, 0));
        for (auto r = rows * 0; r < rows; ++r) {
            for (auto c = cols * 0; c < cols; ++c) {
                ans[r][c] = 2 * ones_row[r] + 2 * ones_col[c] - rows - cols;
            }
        }
        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<vector<int>> onesMinusZeros(vector<vector<int>>& grid) {
        auto const rows = grid.size();
        auto const cols = grid[0].size();
        auto ones_row = vector<int>(rows, 0);
        auto zeros_row = ones_row;
        auto ones_col = vector<int>(cols, 0);
        auto zeros_col = ones_col;

        for (auto r = rows * 0; r < rows; ++r) {
            auto ones = 0;
            auto zeros = 0;
            for (auto n : grid[r]) {
                if (0 == n) {
                    ++zeros;
                }
                else {
                    ++ones;
                }
            }
            ones_row[r] = ones;
            zeros_row[r] = zeros;
        }

        for (auto c = cols * 0; c < cols; ++c) {
            auto ones = 0;
            auto zeros = 0;
            for (auto r = rows * 0; r < rows; ++r) {
                if (0 == grid[r][c]) {
                    ++zeros;
                }
                else {
                    ++ones;
                }
            }
            ones_col[c] = ones;
            zeros_col[c] = zeros;
        }

        auto ans = vector<vector<int>>(rows, vector<int>(cols, 0));
        for (auto r = rows * 0; r < rows; ++r) {
            for (auto c = cols * 0; c < cols; ++c) {
                ans[r][c] = ones_row[r] + ones_col[c] - zeros_row[r] - zeros_col[c];
            }
        }
        return ans;
    }
};
