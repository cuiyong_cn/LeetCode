/**
 * Design Add and Search Words Data Structure
 *
 * Design a data structure that supports adding new words and finding if a string matches any
 * previously added string.
 *
 * Implement the WordDictionary class:
 *
 * WordDictionary() Initializes the object.
 * void addWord(word) Adds word to the data structure, it can be matched later.
 * bool search(word) Returns true if there is any string in the data structure that matches word or
 * false otherwise. word may contain dots '.' where dots can be matched with any letter.
 *
 * Example:
 *
 * Input
 * ["WordDictionary","addWord","addWord","addWord","search","search","search","search"]
 * [[],["bad"],["dad"],["mad"],["pad"],["bad"],[".ad"],["b.."]]
 * Output
 * [null,null,null,null,false,true,true,true]
 *
 * Explanation
 * WordDictionary wordDictionary = new WordDictionary();
 * wordDictionary.addWord("bad");
 * wordDictionary.addWord("dad");
 * wordDictionary.addWord("mad");
 * wordDictionary.search("pad"); // return False
 * wordDictionary.search("bad"); // return True
 * wordDictionary.search(".ad"); // return True
 * wordDictionary.search("b.."); // return True
 *
 * Constraints:
 *
 * 1 <= word.length <= 500
 * word in addWord consists lower-case English letters.
 * word in search consist of  '.' or lower-case English letters.
 * At most 50000 calls will be made to addWord and search.
 */
/**
 * unordered_map solution.
 */
class WordDictionary {
public:
    WordDictionary() {}

    void addWord(string word) {
        words[word.size()].push_back(word);
    }

    bool search(string word) {
        for (auto&& s: words[word.length()]) {
            if (isEqual(s, word)) {
                return true;
            }
        }

        return false;
    }

private:
    unordered_map<int, vector<string>>words;

    bool isEqual(string const& a, string const& b){
        for (size_t i = 0; i < a.length(); i++){
            if (b[i] != '.') {
                if (a[i] != b[i]) {
                    return false;
                }
            }
        }

        return true;
    }
};

/**
 * Original solution using Trie data structure.
 */
class WordDictionary {
public:
    class Trie
    {
    public:
        Trie() : data(26), word{false}
        {

        }

        vector<unique_ptr<Trie>> data;
        bool word;
    };

    /** Initialize your data structure here. */
    WordDictionary() : m_trie{make_unique<Trie>()} {

    }

    void addWord(string word) {
        Trie* cur = m_trie.get();
        for (auto c : word) {
            if (nullptr == cur->data[c - 'a']) {
                cur->data[c - 'a'] = make_unique<Trie>();
            }
            cur = cur->data[c - 'a'].get();
        }

        cur->word = true;
    }

    bool search(string word) {
        vector<Trie*> q{m_trie.get()};

        for (auto c : word) {
            vector<Trie*> next;
            for (auto t : q) {
                if ('.' == c) {
                    for (auto&& tr : t->data) {
                        if (nullptr != tr) {
                            next.emplace_back(tr.get());
                        }
                    }
                }
                else {
                    if (nullptr != t->data[c - 'a']) {
                        next.emplace_back(t->data[c - 'a'].get());
                    }
                }
            }
            if (next.empty()) {
                return false;
            }
            swap(q, next);
        }

        for (auto&& tr : q) {
            if (tr->word) {
                return true;
            }
        }

        return false;
    }

private:
    unique_ptr<Trie> m_trie;
};

/**
 * Your WordDictionary object will be instantiated and called as such:
 * WordDictionary* obj = new WordDictionary();
 * obj->addWord(word);
 * bool param_2 = obj->search(word);
 */
