/**
 * Maximum XOR of Two Numbers in an Array
 *
 * Given a non-empty array of numbers, a0, a1, a2, … , an-1, where 0 ≤ ai < 231.
 *
 * Find the maximum result of ai XOR aj, where 0 ≤ i, j < n.
 *
 * Could you do this in O(n) runtime?
 *
 * Example:
 *
 * Input: [3, 10, 5, 25, 2, 8]
 *
 * Output: 28
 *
 * Explanation: The maximum result is 5 ^ 25 = 28.
 */
/**
 * Passed solution after look at other's solution.
 */
class Solution {
public:
    int findMaximumXOR(vector<int>& nums) {
        int max = 0, mask = 0;

        for (int i = 31; i >= 0; --i) {
            mask = mask | (1 << i);
            // get the left i part
            unordered_set<int> leftParts;
            for (auto& n : nums) leftParts.insert(n & mask);

            // maybeMax is what we expect to be the next max target.
            int maybeMax = max | (1 << i);
            for (auto& n : leftParts) {
                if (leftParts.count(n ^ maybeMax)) {
                    max = maybeMax;
                    break;
                }
            }
        }

        return max;
    }
};

/**
 * Original solution. Brute force, TLE of course.
 */
class Solution {
public:
    int findMaximumXOR(vector<int>& nums) {
        int ans = 0;
        for (int i = 0; i < nums.size(); ++i) {
            for (int j = i + 1; j < nums.size(); ++j) {
                ans = max(ans, nums[i] ^ nums[j]);
            }
        }

        return ans;
    }
};
