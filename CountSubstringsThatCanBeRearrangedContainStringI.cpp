/**
 * Count Substrings That Can Be Rearranged to Contain a String I
 *
 * You are given two strings word1 and word2.
 *
 * A string x is called valid if x can be rearranged to have word2 as a prefix.
 *
 * Return the total number of valid substrings of word1.
 *
 * Example 1:
 *
 * Input: word1 = "bcca", word2 = "abc"
 *
 * Output: 1
 *
 * Explanation:
 *
 * The only valid substring is "bcca" which can be rearranged to "abcc" having "abc" as a prefix.
 *
 * Example 2:
 *
 * Input: word1 = "abcabc", word2 = "abc"
 *
 * Output: 10
 *
 * Explanation:
 *
 * All the substrings except substrings of size 1 and size 2 are valid.
 *
 * Example 3:
 *
 * Input: word1 = "abcabc", word2 = "aaabc"
 *
 * Output: 0
 *
 * Constraints:
 *     1 <= word1.length <= 105
 *     1 <= word2.length <= 104
 *     word1 and word2 consist only of lowercase English letters.
 */
/**
 * Original solution. Sliding window.
 */
class Solution {
public:
    long long validSubstringCount(string word1, string word2) {
        auto cnt = array<int, 26>{ 0, };

        for (auto c : word2) {
            ++cnt[c - 'a'];
        }

        int const size = word1.size();
        auto ans = 0LL;
        auto match = std::ranges::count_if(cnt, [](auto n) { return n > 0; });

        for (auto [i, j] = tuple(0, 0); i < size; ++i) {
            auto n = --cnt[word1[i] - 'a'];
            if (0 == n) {
                --match;
            }

            for (; 0 == match; ++j) {
                ans += size - i;

                auto n = cnt[word1[j] - 'a']++;
                if (0 == n) {
                    ++match;
                }
            }
        }

        return ans;
    }
};
