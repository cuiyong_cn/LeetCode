/**
 * Minimum Number of People to Teach
 *
 * On a social network consisting of m users and some friendships between users, two users can
 * communicate with each other if they know a common language.
 *
 * You are given an integer n, an array languages, and an array friendships where:
 *
 * There are n languages numbered 1 through n,
 * languages[i] is the set of languages the
 * ith user knows, and friendships[i] = [ui, vi] denotes a friendship between the users
 * ui and vi.  You can choose one language and teach it to some users so that all friends can
 * communicate with each other. Return the minimum number of users you need to teach.
 *
 * Note that friendships are not transitive, meaning if x is a friend of y and y is a friend of z,
 * this doesn't guarantee that x is a friend of z.
 *
 * Example 1:
 *
 * Input: n = 2, languages = [[1],[2],[1,2]], friendships = [[1,2],[1,3],[2,3]]
 * Output: 1
 * Explanation: You can either teach user 1 the second language or user 2 the first language.
 * Example 2:
 *
 * Input: n = 3, languages = [[2],[1,3],[1,2],[3]], friendships = [[1,4],[1,2],[3,4],[2,3]]
 * Output: 2
 * Explanation: Teach the third language to users 1 and 3, yielding two users to teach.
 *
 * Constraints:
 *
 * 2 <= n <= 500
 * languages.length == m
 * 1 <= m <= 500
 * 1 <= languages[i].length <= n
 * 1 <= languages[i][j] <= n
 * 1 <= ui < vi <= languages.length
 * 1 <= friendships.length <= 500
 * All tuples (ui, vi) are unique
 * languages[i] contains only unique values
 */
/**
 * Original solution.
 */
class Solution {
public:
    int minimumTeachings(int n, vector<vector<int>>& languages, vector<vector<int>>& friendships) {
        int ans = numeric_limits<int>::max();
        int const not_checked = 0;
        int const checked = 1;
        int const M = languages.size();
        vector<int> users(M, not_checked);

        auto already_can_communicate = [&languages](auto const& fs) {
            auto const u = fs[0];
            auto const v = fs[1];
            for (auto L : languages[u - 1]) {
                if (languages[v - 1].end() != find(languages[v - 1].begin(), languages[v - 1].end(), L)) {
                    return true;
                }
            }
            return false;
        };

        auto it = remove_if(friendships.begin(), friendships.end(), already_can_communicate);
        friendships.erase(it, friendships.end());

        for (int i = 1; i <= n; ++i) {
            int people_to_teach = 0;
            fill(users.begin(), users.end(), not_checked);
            for (auto const& fs : friendships) {
                auto const u = fs[0];
                auto const v = fs[1];
                for (auto const& k : {u, v}) {
                    if (not_checked == users[k - 1]) {
                        users[k - 1] = checked;
                        auto const& lang = languages[k - 1];
                        if (lang.end() == find(lang.begin(), lang.end(), i)) {
                            ++people_to_teach;
                        }
                    }
                }
            }

            ans = min(ans, people_to_teach);
        }

        return ans;
    }
};
