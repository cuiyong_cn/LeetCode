/**
 * Maximum Side Length of a Square with Sum Less than or Equal to Threshold
 *
 * Given a m x n matrix mat and an integer threshold. Return the maximum side-length of a square with a sum less than or
 * equal to threshold or return 0 if there is no such square.
 * 
 * Example 1:
 * 
 * Input: mat = [[1,1,3,2,4,3,2],[1,1,3,2,4,3,2],[1,1,3,2,4,3,2]], threshold = 4
 * Output: 2
 * Explanation: The maximum side length of square with sum less than 4 is 2 as shown.
 * 
 * Example 2:
 * 
 * Input: mat = [[2,2,2,2,2],[2,2,2,2,2],[2,2,2,2,2],[2,2,2,2,2],[2,2,2,2,2]], threshold = 1
 * Output: 0
 * 
 * Example 3:
 * 
 * Input: mat = [[1,1,1,1],[1,0,0,0],[1,0,0,0],[1,0,0,0]], threshold = 6
 * Output: 3
 * 
 * Example 4:
 * 
 * Input: mat = [[18,70],[61,1],[25,85],[14,40],[11,96],[97,96],[63,45]], threshold = 40184
 * Output: 2
 * 
 * Constraints:
 *     1 <= m, n <= 300
 *     m == mat.length
 *     n == mat[i].length
 *     0 <= mat[i][j] <= 10000
 *     0 <= threshold <= 10^5
 */
/**
 * Improved one
 */
class Solution {
public:
    int maxSideLength(vector<vector<int>>& mat, int threshold) {
        int M = mat.size(), N = mat[0].size();

        vector<vector<int>> prefix_sum(M + 1, vector<int>(N + 1, 0));

        int len = 1;
        for (size_t i = 1; i <= M; ++i) {
            for (size_t j = 1; j <= N; ++j) {
                prefix_sum[i][j] = prefix_sum[i - 1][j] + prefix_sum[i][j - 1] - prefix_sum[i - 1][j - 1] + mat[i - 1][j - 1];
                if (i >= len && j >= len) {
                    if ((prefix_sum[i][j] - prefix_sum[i - len][j] - prefix_sum[i][j - len] + prefix_sum[i - len][j - len]) <= threshold) {
                        ++len;
                    }
                }
            }
        }
        
        return len - 1;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int maxSideLength(vector<vector<int>>& mat, int threshold) {
        int M = mat.size(), N = mat[0].size();

        vector<vector<int>> prefix_sum(M, vector<int>(N + 1, 0));
        for (size_t i = 0; i < M; ++i) {
            std::partial_sum(std::begin(mat[i]), std::end(mat[i]), ++std::begin(prefix_sum[i]));
        }
        
        int left = 1, right = std::min(M, N);
        int ans = 0;
        while (left <= right) {
            bool valid = false;
            int mid = (left + right) / 2;
            for (int i = 0; (i + mid - 1) < M; ++i) {
                for (int j = 0; (j + mid - 1) < N; ++j) {
                    int sum = 0;
                    for (int k = i, z = 0; z < mid; ++k, ++z) {
                        sum += prefix_sum[k][j + mid] - prefix_sum[k][j];
                    }

                    if (sum <= threshold) {
                        valid = true;
                        break;
                    }
                }

                if (valid) {
                    break;
                }
            }
            
            if (valid) {
                ans = mid;
                left = mid + 1;
            }
            else {
                right = mid - 1;
            }
        }
        
        return ans;
    }
};
