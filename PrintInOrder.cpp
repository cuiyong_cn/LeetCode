/**
 * Print in Order
 *
 * Suppose we have a class:
 *
 * public class Foo {
 *   public void first() { print("first"); }
 *   public void second() { print("second"); }
 *   public void third() { print("third"); }
 * }
 *
 * The same instance of Foo will be passed to three different threads. Thread A will
 * call first(), thread B will call second(), and thread C will call third(). Design a
 * mechanism and modify the program to ensure that second() is executed after first(),
 * and third() is executed after second().
 */
/**
 * Condvar version
 * Lots of method on leetcode using two mutex, but they use it in a wrong way.
 *
 * 1  Mutex are used for mutual exclusion i.e to safe gaurd the critical sections of a code.
 * 2  Semaphone/condition_variable are used for thread synchronisation(which is what we
 *    want to achieve here).
 * 3  Mutex have ownership assigned with them, that is to say, the thread that locks a
 *    mutex must only unlock it. Also, we must not unlock a mutex that has not been
 *    locked (This is what most programs have got wrong).
 * 4  If the mutex is not used as said above, the behavior is undefined, which however
 *    in our case produces the required result.
 */
class Foo
{
private:
    int count = 0;
    mutex mtx;
    condition_variable cv;
public:
    Foo() : count{1} {}

    void first(function<void()> printFirst)
    {
        unique_lock<mutex> lck(mtx);
        // No point of this wait as on start count will be 1, we need to make the other threads wait.
        // printFirst() outputs "first". Do not change or remove this line.

        printFirst();
        count = 2;
        cv.notify_all();
    }

    void second(function<void()> printSecond)
    {
        unique_lock<mutex> lck(mtx);
        while (count != 2) {
            cv.wait(lck);
        }
        // printSecond() outputs "second". Do not change or remove this line.
        printSecond();
        count = 3;
        cv.notify_all();
    }

    void third(function<void()> printThird)
    {
        unique_lock<mutex> lck(mtx);
        while (count != 3) {
            cv.wait(lck);
        }
        // printThird() outputs "third". Do not change or remove this line.
        printThird();
    }
};

/**
 * promise version
 */
class Foo
{
private:
    std::promise<void> p1;
    std::promise<void> p2;

public:
    void first(function<void()> printFirst)
    {
        printFirst();
        p1.set_value();
    }

    void second(function<void()> printSecond)
    {
        p1.get_future().wait();
        printSecond();
        p2.set_value();
    }

    void third(function<void()> printThird)
    {
        p2.get_future().wait();
        printThird();
    }
};

