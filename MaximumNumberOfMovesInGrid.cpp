/**
 * Maximum Number of Moves in a Grid
 *
 * You are given a 0-indexed m x n matrix grid consisting of positive integers.
 *
 * You can start at any cell in the first column of the matrix, and traverse the grid in the following way:
 *
 * From a cell (row, col), you can move to any of the cells: (row - 1, col + 1),
 * (row, col + 1) and (row + 1, col + 1) such that the value of the cell you
 * move to, should be strictly bigger than the value of the current cell.
 * Return the maximum number of moves that you can perform.
 *
 * Example 1:
 *
 * Input: grid = [[2,4,3,5],[5,4,9,3],[3,4,2,11],[10,9,13,15]]
 * Output: 3
 * Explanation: We can start at the cell (0, 0) and make the following moves:
 * - (0, 0) -> (0, 1).
 * - (0, 1) -> (1, 2).
 * - (1, 2) -> (2, 3).
 * It can be shown that it is the maximum number of moves that can be made.
 * Example 2:
 *
 * Input: grid = [[3,2,4],[2,1,9],[1,1,7]]
 * Output: 0
 * Explanation: Starting from any cell in the first column we cannot perform any moves.
 *
 * Constraints:
 *
 * m == grid.length
 * n == grid[i].length
 * 2 <= m, n <= 1000
 * 4 <= m * n <= 105
 * 1 <= grid[i][j] <= 106
 */
/**
 * Simplified.
 */
class Solution {
public:
    int maxMoves(vector<vector<int>>& grid) {
        int m = grid.size();
        int n = grid[0].size();
        auto dp = vector<vector<int>>(m, vector<int>(n, 0));

        for (auto c = n - 2; c >= 0; --c) {
            for (auto r = 0; r < m; ++r) {
                auto steps = -1;
                auto next_cells = array<pair<int, int>, 3>{ {{r - 1, c + 1}, {r, c + 1}, {r + 1, c + 1}} };

                for (auto [i, j] : next_cells) {
                    if (0 <= i && i < m && 0 <= j && j < n) {
                        if (grid[r][c] < grid[i][j]) {
                            steps = max(steps, dp[i][j]);
                        }
                    }
                }

                dp[r][c] = steps + 1;
            }
        }

        auto ans = 0;

        for (auto r = 0; r < m; ++r) {
            ans = max(ans, dp[r][0]);
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int maxMoves(vector<vector<int>>& grid) {
        int m = grid.size();
        int n = grid[0].size();
        auto dp = vector<vector<int>>(m, vector<int>(n, 0));
        for (auto c = n - 2; c >= 0; --c) {
            for (auto r = 0; r < m; ++r) {
                auto steps = -1;
                if ((r - 1) >= 0 && (c + 1) < n) {
                    if (grid[r][c] < grid[r - 1][c + 1]) {
                        steps = max(steps, dp[r - 1][c + 1]);
                    }
                }

                if ((c + 1) < n) {
                    if (grid[r][c] < grid[r][c + 1]) {
                        steps = max(steps, dp[r][c + 1]);
                    }
                }

                if ((r + 1) < m && (c + 1) < n) {
                    if (grid[r][c] < grid[r + 1][c + 1]) {
                        steps = max(steps, dp[r + 1][c + 1]);
                    }
                }

                dp[r][c] = steps + 1;
            }
        }

        auto ans = 0;

        for (auto r = 0; r < m; ++r) {
            ans = max(ans, dp[r][0]);
        }

        return ans;
    }
};
