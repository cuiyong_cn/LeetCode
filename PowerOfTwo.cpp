/**
 * Power of Two
 *
 * Given an integer n, return true if it is a power of two. Otherwise, return false.
 *
 * An integer n is a power of two, if there exists an integer x such that n == 2x.
 *
 * Example 1:
 *
 * Input: n = 1
 * Output: true
 * Explanation: 20 = 1
 * Example 2:
 *
 * Input: n = 16
 * Output: true
 * Explanation: 24 = 16
 * Example 3:
 *
 * Input: n = 3
 * Output: false
 * Example 4:
 *
 * Input: n = 4
 * Output: true
 * Example 5:
 *
 * Input: n = 5
 * Output: false
 *
 * Constraints:
 *
 * -231 <= n <= 231 - 1
 */
/**
 * Also there are two other solutions.
 * Using c++ std::bitset as power of two only has one '1'
 * Using loop up table. As int can only has 32 integers that are power of two.
 */

/**
 * From discussion. This one is pretty interesting. Using math.
 *
 * Because the range of an integer = -2147483648 (-2^31) ~ 2147483647 (2^31-1), the max possible
 * power of two = 2^30 = 1073741824.
 *
 * (1) If n is the power of two, let n = 2^k, where k is an integer.
 *
 * We have 2^30 = (2^k) * 2^(30-k), which means (2^30 % 2^k) == 0.
 *
 * (2) If n is not the power of two, let n = j*(2^k), where k is an integer and j is an odd number.
 *
 * We have (2^30 % j*(2^k)) == (2^(30-k) % j) != 0.
 */
class Solution {
public:
    bool isPowerOfTwo(int n) {
        if (n <= 0) {
            return false;
        }

        return 0 == (1073741824 % n);
    }
};

/**
 * From discussion. More efficient one.
 * The idea is:
 * Power of 2 means only one bit of n is '1', so use the trick n&(n-1)==0 to judge whether that is
 * the case
 */
class Solution {
public:
    bool isPowerOfTwo(int n) {
        if (n <= 0) {
            return false;
        }

        return 0 == (n & (n - 1));
    }
};

/**
 * We can write the below one in iterative version.
 */
class Solution {
public:
    bool isPowerOfTwo(int n) {
        if (n <= 0) {
            return false;
        }

        while (0 == (n & 1)) {
            n = n / 2;
        }

        return 1 == n;
    }
};


/**
 * Original solution. Recursive one.
 */
class Solution {
public:
    bool isPowerOfTwo(int n) {
        if (n <= 0) {
            return false;
        }

        if (1 == n) {
            return true;
        }

        if (n & 1) {
            return false;
        }

        return isPowerOfTwo(n / 2);
    }
};
