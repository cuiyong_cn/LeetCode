/**
 * Maximum Product of Splitted Binary Tree
 *
 * Given the root of a binary tree, split the binary tree into two subtrees by removing one edge
 * such that the product of the sums of the subtrees is maximized.
 *
 * Return the maximum product of the sums of the two subtrees. Since the answer may be too large,
 * return it modulo 109 + 7.
 *
 * Note that you need to maximize the answer before taking the mod and not after taking it.
 *
 * Example 1:
 *
 * Input: root = [1,2,3,4,5,6]
 * Output: 110
 * Explanation: Remove the red edge and get 2 binary trees with sum 11 and 10. Their product is 110 (11*10)
 * Example 2:
 *
 * Input: root = [1,null,2,3,4,null,null,5,6]
 * Output: 90
 * Explanation: Remove the red edge and get 2 binary trees with sum 15 and 6.Their product is 90 (15*6)
 * Example 3:
 *
 * Input: root = [2,3,9,10,7,8,6,5,4,11,1]
 * Output: 1025
 * Example 4:
 *
 * Input: root = [1,1]
 * Output: 1
 *
 * Constraints:
 *
 * The number of nodes in the tree is in the range [2, 5 * 104].
 * 1 <= Node.val <= 104
 */
/**
 * Without the use of vector. Traverse the tree twice.
 */
class Solution {
public:
    int maxProduct(TreeNode* root) {
        sum_of_tree = sum(root);
        max_prod_of_split_binary_tree(root);
        return ans % mod;
    }

private:
    int64_t sum(TreeNode* node)
    {
        if (nullptr == node) {
            return 0;
        }

        auto vl = sum(node->left);
        auto vr = sum(node->right);
        return vl + vr + node->val;
    }

    int64_t max_prod_of_split_binary_tree(TreeNode* node)
    {
        if (nullptr == node) {
            return 0;
        }

        auto vl = max_prod_of_split_binary_tree(node->left);
        auto vr = max_prod_of_split_binary_tree(node->right);
        auto total = vl + vr + node->val;
        ans = max(ans, total * (sum_of_tree - total));
        return total;
    }
    
private:
    int64_t ans = 0;
    int const mod = 1e9 + 7;
    int64_t sum_of_tree = 0;
};

/**
 * Original solution.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int maxProduct(TreeNode* root) {
        vector<int64_t> sum;
        collect_sum(root, sum);

        auto sum_of_tree = sum.back();

        int const mod = 1e9 + 7;
        int64_t ans = 0;
        int const N = sum.size();
        for (int i = 0; i < N; ++i) {
            ans = max(ans, sum[i] * (sum_of_tree - sum[i]));
        }

        return ans % mod;
    }

private:
    int64_t collect_sum(TreeNode* node, vector<int64_t>& sum)
    {
        if (nullptr == node) {
            return 0;
        }

        auto vl = collect_sum(node->left, sum);
        auto vr = collect_sum(node->right, sum);
        auto total = vl + vr + node->val;
        sum.emplace_back(total);
        return total;
    }
};
