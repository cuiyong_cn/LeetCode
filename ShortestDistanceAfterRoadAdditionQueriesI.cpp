/**
 * Shortest Distance After Road Addition Queries I
 *
 * You are given an integer n and a 2D integer array queries.
 *
 * There are n cities numbered from 0 to n - 1. Initially, there is a unidirectional road from city
 * i to city i + 1 for all 0 <= i < n - 1.
 *
 * queries[i] = [ui, vi] represents the addition of a new unidirectional road from city ui to city
 * vi. After each query, you need to find the length of the shortest path from city 0 to city n - 1.
 *
 * Return an array answer where for each i in the range [0, queries.length - 1], answer[i] is the
 * length of the shortest path from city 0 to city n - 1 after processing the first i + 1 queries.
 *
 * Example 1:
 *
 * Input: n = 5, queries = [[2,4],[0,2],[0,4]]
 *
 * Output: [3,2,1]
 *
 * Explanation:
 *
 * After the addition of the road from 2 to 4, the length of the shortest path from 0 to 4 is 3.
 *
 * After the addition of the road from 0 to 2, the length of the shortest path from 0 to 4 is 2.
 *
 * After the addition of the road from 0 to 4, the length of the shortest path from 0 to 4 is 1.
 *
 * Example 2:
 *
 * Input: n = 4, queries = [[0,3],[0,2]]
 *
 * Output: [1,1]
 *
 * Explanation:
 *
 * After the addition of the road from 0 to 3, the length of the shortest path from 0 to 3 is 1.
 *
 * After the addition of the road from 0 to 2, the length of the shortest path remains 1.
 *
 * Constraints:
 *     3 <= n <= 500
 *     1 <= queries.length <= 500
 *     queries[i].length == 2
 *     0 <= queries[i][0] < queries[i][1] < n
 *     1 < queries[i][1] - queries[i][0]
 *     There are no repeated roads among the queries.
 */
/**
 * Optimize.
 */
class Solution {
public:
    vector<int> shortestDistanceAfterQueries(int n, vector<vector<int>>& queries) {
        m_to_city = vector<vector<int>>(n);
        auto ans = vector<int>{};

        for (auto i = 1; i < n; ++i) {
            m_to_city[i - 1].push_back(i);
        }

        for (auto& q : queries) {
            auto from = q[0];
            auto to = q[1];
            m_to_city[from].push_back(to);

            ans.push_back(bfs());
        }

        return ans;
    }
private:
    auto bfs() -> int
    {
        auto distance = 0;
        auto q = deque<int>{ 0 };
        auto visited = vector<bool>(m_to_city.size(), false);

        while (!q.empty()) {
            int const size = q.size();
            for (auto i = 0; i < size; ++i) {
                auto from = q.front();
                q.pop_front();

                if (m_to_city[from].empty()) {
                    return distance;
                }

                if (visited[from]) {
                    continue;
                }

                visited[from] = true;

                for (auto to : m_to_city[from]) {
                    q.push_back(to);
                }
            }

            ++distance;
        }

        return -1;
    }

private:
    vector<vector<int>> m_to_city;
};

/**
 * Original solution. TLE.
 */
class Solution {
public:
    vector<int> shortestDistanceAfterQueries(int n, vector<vector<int>>& queries) {
        m_to_city = vector<vector<int>>(n);
        auto ans = vector<int>{};

        for (auto i = 1; i < n; ++i) {
            m_to_city[i - 1].push_back(i);
        }

        for (auto& q : queries) {
            auto from = q[0];
            auto to = q[1];
            m_to_city[from].push_back(to);

            ans.push_back(bfs());
        }

        return ans;
    }
private:
    auto bfs() -> int
    {
        auto distance = numeric_limits<int>::max();
        auto q = deque<pair<int, int>>{ {0, 0} };

        while (!q.empty()) {
            int const size = q.size();
            for (auto i = 0; i < size; ++i) {
                auto [from, dist] = q.front();
                q.pop_front();

                if (m_to_city[from].empty()) {
                    distance = min(distance, dist);
                }

                for (auto to : m_to_city[from]) {
                    q.push_back({to, dist + 1});
                }
            }
        }

        return distance;
    }

private:
    vector<vector<int>> m_to_city;
};
