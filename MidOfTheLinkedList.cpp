/**
 * Middle of the Linked List
 *
 * Given a non-empty, singly linked list with head node head, return a middle
 * node of linked list.
 *
 * If there are two middle nodes, return the second middle node.
 *
 * Example 1:
 *
 * Input: [1,2,3,4,5]
 * Output: Node 3 from this list (Serialization: [3,4,5])
 * The returned node has value 3.  (The judge's serialization of this node is [3,4,5]).
 * Note that we returned a ListNode object ans, such that:
 * ans.val = 3, ans.next.val = 4, ans.next.next.val = 5, and ans.next.next.next = NULL.
 *
 * Example 2:
 *
 * Input: [1,2,3,4,5,6]
 * Output: Node 4 from this list (Serialization: [4,5,6])
 * Since the list has two middle nodes with values 3 and 4, we return the second one.
 *
 * Note:
 *
 *     The number of nodes in the given list will be between 1 and 100.
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
/**
 * More efficient one
 */
class Solution {
public:
    ListNode* middleNode(ListNode* head) {
        ListNode *slow = head, *fast = head;
        while (fast && fast->next) {
            slow = slow->next;
            /* Here I don't think is safe, what if fast->next is nullptr */
            fast = fast->next->next;
        }
        return slow;
    }
};
/**
 * Mine original version
 */
class Solution {
public:
    ListNode* middleNode(ListNode* head) {
        int nodeNum = 0;
        ListNode* p = head;
        while (p) {
            ++nodeNum;
            p = p->next;
        }
        p = head;
        int mid = nodeNum / 2;
        for (int i = 0; i < mid; ++i) {
            p = p->next;
        }
        return p;
    }
};
