/**
 * Construct String With Repeat Limit
 *
 * You are given a string s and an integer repeatLimit. Construct a new string repeatLimitedString
 * using the characters of s such that no letter appears more than repeatLimit times in a row. You
 * do not have to use all characters from s.
 *
 * Return the lexicographically largest repeatLimitedString possible.
 *
 * A string a is lexicographically larger than a string b if in the first position where a and b
 * differ, string a has a letter that appears later in the alphabet than the corresponding letter in
 * b. If the first min(a.length, b.length) characters do not differ, then the longer string is the
 * lexicographically larger one.
 *
 * Example 1:
 *
 * Input: s = "cczazcc", repeatLimit = 3
 * Output: "zzcccac"
 * Explanation: We use all of the characters from s to construct the repeatLimitedString "zzcccac".
 * The letter 'a' appears at most 1 time in a row.
 * The letter 'c' appears at most 3 times in a row.
 * The letter 'z' appears at most 2 times in a row.
 * Hence, no letter appears more than repeatLimit times in a row and the string is a valid repeatLimitedString.
 * The string is the lexicographically largest repeatLimitedString possible so we return "zzcccac".
 * Note that the string "zzcccca" is lexicographically larger but the letter 'c' appears more than 3 times in a row, so it is not a valid repeatLimitedString.
 * Example 2:
 *
 * Input: s = "aababab", repeatLimit = 2
 * Output: "bbabaa"
 * Explanation: We use only some of the characters from s to construct the repeatLimitedString "bbabaa".
 * The letter 'a' appears at most 2 times in a row.
 * The letter 'b' appears at most 2 times in a row.
 * Hence, no letter appears more than repeatLimit times in a row and the string is a valid repeatLimitedString.
 * The string is the lexicographically largest repeatLimitedString possible so we return "bbabaa".
 * Note that the string "bbabaaa" is lexicographically larger but the letter 'a' appears more than 2 times in a row, so it is not a valid repeatLimitedString.
 *
 * Constraints:
 *
 * 1 <= repeatLimit <= s.length <= 105
 * s consists of lowercase English letters.
 */
/**
 * Without pq
 */
class Solution {
public:
    string repeatLimitedString(string const& s, int repeatLimit) {
        auto char_cnt = array<int, 26>{0,};
        for (auto c : s) {
            ++char_cnt[c - 'a'];
        }

        auto waiting = pair('z', 0);
        auto ans = string{};
        for (int i = 25; i >= 0; --i) {
            if (0 == char_cnt[i]) {
                continue;
            }

            if (waiting.second > 0) {
                if (waiting.first == ans.back()) {
                    ans += 'a' + i;
                    --char_cnt[i];
                }
                else {
                    auto cnt = min(waiting.second, repeatLimit);
                    ans += string(cnt, waiting.first);
                    waiting.second -= cnt;
                }
            }
            else {
                if (!ans.empty() && ('a' + i) == ans.back()) {
                    break;
                }
                auto cnt = min(char_cnt[i], repeatLimit);
                ans += string(cnt, 'a' + i);
                waiting = pair('a' + i, char_cnt[i] - cnt);
                char_cnt[i] = 0;
            }
            ++i;
        }

        if (waiting.second > 0 && waiting.first != ans.back()) {
            auto cnt = min(waiting.second, repeatLimit);
            ans += string(cnt, waiting.first);
        }

        return ans;
    }
};
/**
 * Original solution.
 */
class Solution {
public:
    string repeatLimitedString(string s, int repeatLimit) {
        auto char_cnt = array<int, 26>{0,};
        for (auto c : s) {
            ++char_cnt[c - 'a'];
        }

        auto pq = priority_queue<pair<char, int>>{};
        for (int i = 0; i < 26; ++i) {
            if (char_cnt[i] > 0) {
                pq.emplace('a' + i, char_cnt[i]);
            }
        }

        auto ans = string{};
        while (!pq.empty()) {
            auto [ch, cnt] = pq.top();
            pq.pop();
            auto len = min(cnt, repeatLimit);
            ans += string(len, ch);
            if (cnt > len) {
                if (pq.empty()) {
                    break;
                }

                auto [nch, ncnt] = pq.top();
                pq.pop();
                ans += nch;
                --ncnt;
                if (ncnt > 0) {
                    pq.emplace(nch, ncnt);
                }
                pq.emplace(ch, cnt - len);
            }
        }

        return ans;
    }
};
