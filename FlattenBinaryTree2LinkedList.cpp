/**
 * Flatten Binary Tree to Linked List
 *
 * Given a binary tree, flatten it to a linked list in-place.
 *
 * For example, given the following tree:
 *
 *     1
 *    / \
 *   2   5
 *  / \   \
 * 3   4   6
 *
 * The flattened tree should look like:
 *
 * 1
 *  \
 *   2
 *    \
 *     3
 *      \
 *       4
 *        \
 *         5
 *          \
 *           6
 */
/**
 * Really simple one from discussion.
 */
class Solution {
public:
    void flatten(TreeNode *root) {
        while (root) {
            if (root->left) {
                TreeNode* t = root->left;
                while (t->right)
                    t = t->right;
                t->right = root->right;
                root->right = root->left;
                root->left = nullptr;
            }

            root = root->right;
        }
    }
};

/**
 * Original solution. Basic idea is:
 * 1. use pre-order traversal to get the list
 * 2. correct the relation
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    void flatten(TreeNode* root) {
        vector<TreeNode*> nodes;
        helper(root, nodes);

        for (size_t i = 1; i < nodes.size(); ++i) {
            nodes[i - 1]->left = nullptr;
            nodes[i - 1]->right = nodes[i];
        }

        if (!nodes.empty()) {
            nodes[nodes.size() - 1]->left = nullptr;
            nodes[nodes.size() - 1]->right = nullptr;
        }
    }

private:
    void helper(TreeNode* root, vector<TreeNode*>& nodes)
    {
        if (!root) return;

        nodes.push_back(root);

        helper(root->left, nodes);
        helper(root->right, nodes);
    }
};
