/**
 * Permutation in String
 *
 * Given two strings s1 and s2, write a function to return true if s2 contains the permutation of s1. In other words,
 * one of the first string's permutations is the substring of the second string.
 *
 * Example 1:
 *
 * Input: s1 = "ab" s2 = "eidbaooo"
 * Output: True
 * Explanation: s2 contains one permutation of s1 ("ba").
 * Example 2:
 *
 * Input:s1= "ab" s2 = "eidboaoo"
 * Output: False
 *
 * Constraints:
 *
 * The input strings only contain lower case letters.
 * The length of both given strings is in range [1, 10,000].
 */
/**
 * Original solution. same as Find All Anagrams in a String
 */
class Solution {
public:
    bool checkInclusion(string s1, string s2) {
        if (s1.length() > s2.length()) {
            return false;
        }

        int s1_len = s1.length();
        vector<int> trait_of_s1(26, 0);
        vector<int> trait(26, 0);
        for (int i = 0; i < s1_len; ++i) {
            ++trait_of_s1[s1[i] - 'a'];
            ++trait[s2[i] - 'a'];
        }

        if (trait == trait_of_s1) {
            return true;
        }

        for (int i = s1_len; i < s2.length(); ++i) {
            ++trait[s2[i] - 'a'];
            --trait[s2[i - s1_len] - 'a'];
            if (trait == trait_of_s1) {
                return true;
            }
        }

        return false;
    }
};
