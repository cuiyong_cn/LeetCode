/**
 * Largest Number
 *
 * Given a list of non-negative integers nums, arrange them such that they form the largest number.
 *
 * Note: The result may be very large, so you need to return a string instead of an integer.
 *
 * Example 1:
 *
 * Input: nums = [10,2]
 * Output: "210"
 * Example 2:
 *
 * Input: nums = [3,30,34,5,9]
 * Output: "9534330"
 * Example 3:
 *
 * Input: nums = [1]
 * Output: "1"
 * Example 4:
 *
 * Input: nums = [10]
 * Output: "10"
 *
 * Constraints:
 *
 * 1 <= nums.length <= 100
 * 0 <= nums[i] <= 109
 */
/**
 * Original solution. Actually I was stuck by this one for quite a while and realize
 * to determine the order, we just consider the combo of a and b, which is larger if
 * combined.
 */
class Solution {
public:
    string largestNumber(vector<int>& nums) {
        vector<string> num_strs;
        for (auto n : nums) {
            num_strs.emplace_back(to_string(n));
        }

        sort(begin(num_strs), end(num_strs),
            [](auto const& a, auto const& b) {
                auto ab = a + b;
                auto ba = b + a;
                return ab > ba;
            });

        string ans;
        for (auto&& s : num_strs) {
            ans += s;
        }

        return  '0' == ans[0] ? "0" : ans;
    }
};
