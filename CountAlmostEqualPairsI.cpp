/**
 * Count Almost Equal Pairs I
 *
 * You are given an array nums consisting of positive integers.
 *
 * We call two integers x and y in this problem almost equal if both integers can become equal after
 * performing the following operation at most once:
 *     Choose either x or y and swap any two digits within the chosen number.
 *
 * Return the number of indices i and j in nums where i < j such that nums[i] and nums[j] are almost equal.
 *
 * Note that it is allowed for an integer to have leading zeros after performing an operation.
 *
 * Example 1:
 *
 * Input: nums = [3,12,30,17,21]
 *
 * Output: 2
 *
 * Explanation:
 *
 * The almost equal pairs of elements are:
 *     3 and 30. By swapping 3 and 0 in 30, you get 3.
 *     12 and 21. By swapping 1 and 2 in 12, you get 21.
 *
 * Example 2:
 *
 * Input: nums = [1,1,1,1,1]
 *
 * Output: 10
 *
 * Explanation:
 *
 * Every two elements in the array are almost equal.
 *
 * Example 3:
 *
 * Input: nums = [123,231]
 *
 * Output: 0
 *
 * Explanation:
 *
 * We cannot swap any two digits of 123 or 231 to reach the other.
 *
 * Constraints:
 *     2 <= nums.length <= 100
 *     1 <= nums[i] <= 106
 */
/**
 * Original solution.
 */
auto almost_equal(int a, int b)
{
    if (a == b) {
        return true;
    }

    auto str_a = to_string(a);
    auto str_b = to_string(b);

    if (str_a.length() < str_b.length()) {
        str_a = string(str_b.length() - str_a.length(), '0') + str_a;
    }
    else {
        str_b = string(str_a.length() - str_b.length(), '0') + str_b;
    }

    auto const size = str_a.length();
    auto digits_a = array<int, 10>{ 0, };
    auto digits_b = array<int, 10>{ 0, };
    auto diffs = 0;

    for (auto i = 0uz; i < size; ++i) {
        if (str_a[i] != str_b[i]) {
            ++diffs;
        }
        ++digits_a[str_a[i] - '0'];
        ++digits_b[str_b[i] - '0'];
    }

    return (2 == diffs) && (digits_a == digits_b);
}

class Solution {
public:
    int countPairs(vector<int>& nums) {
        auto ans = 0;
        auto const size = nums.size();

        for (auto i = 0uz; i < size; ++i) {
            for (auto j = i + 1; j < size; ++j) {
                if (almost_equal(nums[i], nums[j])) {
                    ++ans;
                }
            }
        }

        return ans;
    }
};
