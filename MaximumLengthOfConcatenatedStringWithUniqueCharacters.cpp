/**
 * Maximum Length of a Concatenated String with Unique Characters
 *
 * Given an array of strings arr. String s is a concatenation of a sub-sequence of arr which have unique characters.
 *
 * Return the maximum possible length of s.
 *
 * Example 1:
 *
 * Input: arr = ["un","iq","ue"]
 * Output: 4
 * Explanation: All possible concatenations are "","un","iq","ue","uniq" and "ique".
 * Maximum length is 4.
 * Example 2:
 *
 * Input: arr = ["cha","r","act","ers"]
 * Output: 6
 * Explanation: Possible solutions are "chaers" and "acters".
 * Example 3:
 *
 * Input: arr = ["abcdefghijklmnopqrstuvwxyz"]
 * Output: 26
 *
 * Constraints:
 *
 * 1 <= arr.length <= 16
 * 1 <= arr[i].length <= 26
 * arr[i] contains only lower case English letters.
 */
/**
 * original solution. DFS solution.
 */
class Solution {
public:
    int maxLength(vector<string>& arr) {
        return dfs(arr, 0, 0);
    }

private:
    int dfs(const vector<string>& arr, int start, int mask)
    {
        size_t ans = 0;
        for (int i = start; i < arr.size(); ++i) {
            auto [str_mask, valid] = string_mask(arr[i]);
            if (valid && !(str_mask & mask)) {
                ans = std::max(ans, arr[i].length() + dfs(arr, i + 1, str_mask | mask));
            }
        }

        return ans;
    }

    std::pair<int, bool> string_mask(const string& str)
    {
        bool meet_chars[26] = { false, };
        int mask = 0;
        for (auto c : str) {
            if (!meet_chars[c - 'a']) {
                mask |= 1 << (c - 'a');
                meet_chars[c - 'a'] = true;
            }
            else {
                return {mask, false};
            }
        }

        return {mask, true};
    }
};
