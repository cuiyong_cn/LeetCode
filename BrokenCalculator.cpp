/**
 * Broken Calculator
 *
 * On a broken calculator that has a number showing on its display, we can perform two operations:
 *
 * Double: Multiply the number on the display by 2, or;
 * Decrement: Subtract 1 from the number on the display.
 * Initially, the calculator is displaying the number X.
 *
 * Return the minimum number of operations needed to display the number Y.
 *
 * Example 1:
 *
 * Input: X = 2, Y = 3
 * Output: 2
 * Explanation: Use double operation and then decrement operation {2 -> 4 -> 3}.
 * Example 2:
 *
 * Input: X = 5, Y = 8
 * Output: 2
 * Explanation: Use decrement and then double {5 -> 4 -> 8}.
 * Example 3:
 *
 * Input: X = 3, Y = 10
 * Output: 3
 * Explanation:  Use double, decrement and double {3 -> 6 -> 5 -> 10}.
 * Example 4:
 *
 * Input: X = 1024, Y = 1
 * Output: 1023
 * Explanation: Use decrement operations 1023 times.
 *
 * Note:
 *
 * 1 <= X <= 10^9
 * 1 <= Y <= 10^9
 */
/**
 * same idea. Iterative, work backwards.
 */
class Solution {
public:
    int brokenCalc(int X, int Y) {
        int ans = 0;
        while (Y > X) {
            ans++;
            Y = Y % 2 ? ++Y : Y / 2;
        }

        return ans + X - Y;
    }
}

/**
 * Original solution.
 */
class Solution {
public:
    int brokenCalc(int X, int Y) {
        if (X >= Y) {
            return X - Y;
        }

        const int half_y = Y / 2 + Y % 2;
        int step = Y % 2;
        if (X < half_y) {
            step += brokenCalc(X, half_y) + 1;
        }
        else {
            step += X - half_y + 1;
        }

        return step;
    }
};
