/**
 * Maximum Twin Sum of a Linked List
 *
 * In a linked list of size n, where n is even, the ith node (0-indexed) of the linked list is known
 * as the twin of the (n-1-i)th node, if 0 <= i <= (n / 2) - 1.
 *
 * For example, if n = 4, then node 0 is the twin of node 3, and node 1 is the twin of node 2. These
 * are the only nodes with twins for n = 4.
 * The twin sum is defined as the sum of a node and its twin.
 *
 * Given the head of a linked list with even length, return the maximum twin sum of the linked list.
 *
 * Example 1:
 *
 * Input: head = [5,4,2,1]
 * Output: 6
 * Explanation:
 * Nodes 0 and 1 are the twins of nodes 3 and 2, respectively. All have twin sum = 6.
 * There are no other nodes with twins in the linked list.
 * Thus, the maximum twin sum of the linked list is 6.
 * Example 2:
 *
 * Input: head = [4,2,2,3]
 * Output: 7
 * Explanation:
 * The nodes with twins present in this linked list are:
 * - Node 0 is the twin of node 3 having a twin sum of 4 + 3 = 7.
 * - Node 1 is the twin of node 2 having a twin sum of 2 + 2 = 4.
 * Thus, the maximum twin sum of the linked list is max(7, 4) = 7.
 * Example 3:
 *
 * Input: head = [1,100000]
 * Output: 100001
 * Explanation:
 * There is only one node with a twin in the linked list having twin sum of 1 + 100000 = 100001.
 *
 * Constraints:
 *
 * The number of nodes in the list is an even integer in the range [2, 105].
 * 1 <= Node.val <= 105*
 */
/**
 * Without extra space.
 */
int len(ListNode* n)
{
    auto L = int{0};
    while (n) {
        ++L;
        n = n->next;
    }
    return L;
}

ListNode* advance(ListNode* node, int n)
{
    while (node && n > 0) {
        --n;
        node = node->next;
    }

    return node;
}

ListNode* reverse(ListNode* node)
{
    ListNode* prev = nullptr;
    auto head = node;
    while (head) {
        auto next = head->next;
        head->next = prev;
        prev = head;
        head = next;
    }

    return prev;
}

ListNode* split(ListNode* node, int n)
{
    auto split_node = advance(node, n);
    auto new_head = split_node->next;
    split_node->next = nullptr;
    return new_head;
}

class Solution {
public:
    int pairSum(ListNode* head) {
        auto size = len(head);
        auto middle = split(head, size / 2 - 1);
        auto rev_head = reverse(head);

        auto ans = int{0};
        auto left = rev_head;
        auto right = middle;
        while (right) {
            ans = max(ans, left->val + right->val);
            right = right->next;
            left = left->next;
        }

        rev_head = reverse(rev_head);
        rev_head->next = middle;

        return ans;
    }
};

/**
 * Original solution.
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
int len(ListNode* n)
{
    auto L = int{0};
    while (n) {
        ++L;
        n = n->next;
    }
    return L;
}

class Solution {
public:
    int pairSum(ListNode* head) {
        auto size = len(head);
        auto values = vector<int>(size, 0);
        for (auto i = int{0}; i < size; ++i) {
            values[i] = head->val;
            head = head->next;
        }

        auto ans = 0;
        for (auto i = int{0}, j = size - 1; i < j; ++i, --j) {
            ans = max(ans, values[i] + values[j]);
        }

        return ans;
    }
};
