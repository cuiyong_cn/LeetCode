/** Reordered Power of 2
 *
 * Starting with a positive integer N, we reorder the digits in any order (including the original order) such that the
 * leading digit is not zero.
 *
 * Return true if and only if we can do this in a way such that the resulting number is a power of 2.
 *
 * Example 1:
 *
 * Input: 1 Output: true
 *
 * Example 2:
 *
 * Input: 10 Output: false
 *
 * Example 3:
 *
 * Input: 16 Output: true
 *
 * Example 4:
 *
 * Input: 24 Output: false
 *
 * Example 5:
 *
 * Input: 46 Output: true
 *
 * Note: 1 <= N <= 10^9
 */
/**
 * Improved version.
 */
class Solution
{
public:
    bool reorderedPowerOf2(int N) {
        long c = counter(N);
        for (int i = 0; i < 32; i++)
            if (counter(1 << i) == c) return true;
        return false;
    }

    long counter(int N) {
        long res = 0;
        for (; N; N /= 10) res += pow(10, N % 10);
        return res;
    }
};

/**
 * Original solution. First I thought permutation method, but 32 bit integer will have tons of permutations. Passed.
 * Then I though there will only 32 power of 2 inegers. Then ...
 */
class Solution {
public:
    bool reorderedPowerOf2(int N) {
        unordered_map<long, vector<string>> power2Map;
        for (long i = 0; i < 37; ++i) {
            string s = to_string(1L << i);
            power2Map[s.length()].push_back(s);
        }

        string numStr = to_string(N);
        vector<int> numCount(10, 0);
        for (auto c : numStr) ++numCount[c - '0'];

        bool powerof2 = true;
        for (auto& s : power2Map[numStr.length()]) {
            powerof2 = true;
            vector<int> count = numCount;
            for (auto c : s) {
                --count[c - '0'];
            }

            for (auto n : count) {
                if (0 != n) powerof2 = false;
            }

            if (powerof2) break;
        }

        return powerof2;
    }
};
