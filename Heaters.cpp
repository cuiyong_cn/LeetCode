/**
 * Heaters
 *
 * Winter is coming! During the contest, your first job is to design a standard heater with a fixed
 * warm radius to warm all the houses.
 *
 * Every house can be warmed, as long as the house is within the heater's warm radius range.
 *
 * Given the positions of houses and heaters on a horizontal line, return the minimum radius
 * standard of heaters so that those heaters could cover all houses.
 *
 * Notice that all the heaters follow your radius standard, and the warm radius will the same.
 *
 * Example 1:
 *
 * Input: houses = [1,2,3], heaters = [2]
 * Output: 1
 * Explanation: The only heater was placed in the position 2, and if we use the radius 1 standard,
 * then all the houses can be warmed.
 * Example 2:
 *
 * Input: houses = [1,2,3,4], heaters = [1,4]
 * Output: 1
 * Explanation: The two heater was placed in the position 1 and 4. We need to use radius 1 standard,
 * then all the houses can be warmed.
 * Example 3:
 *
 * Input: houses = [1,5], heaters = [2]
 * Output: 3
 *
 * Constraints:
 *
 * 1 <= houses.length, heaters.length <= 3 * 104
 * 1 <= houses[i], heaters[i] <= 109
 */
/**
 * Optimize further. We don't need the deque and stack.
 */
class Solution {
public:
    int findRadius(vector<int>& houses, vector<int>& heaters) {
        sort(houses.begin(), houses.end());
        sort(heaters.begin(), heaters.end());

        int radius = 0;
        size_t i = 0;
        for (auto h : houses) {
            while (i < heaters.size() && h > heaters[i]) {
                ++i;
            }

            int r = heaters.size() == i ? h - heaters[i - 1] : heaters[i] - h;
            if (i > 0) {
                r = min(r, h - heaters[i - 1]);
            }

            radius = max(radius, r);
        }

        return radius;
    }
};

/**
 * Optimize. Step 1 at a time is too slow for this problem.
 */
class Solution {
public:
    int findRadius(vector<int>& houses, vector<int>& heaters) {
        sort(houses.begin(), houses.end());
        sort(heaters.begin(), heaters.end());

        deque<int> hts(heaters.begin(), heaters.end());
        vector<int> stk;

        int radius = 0;
        for (auto h : houses) {
            while (!hts.empty() && h > hts.front()) {
                stk.push_back(hts.front());
                hts.pop_front();
            }

            int r = hts.empty() ? h - stk.back() : hts.front() - h;
            if (!stk.empty()) {
                r = min(r, h - stk.back());
            }

            radius = max(radius, r);
        }

        return radius;
    }
};

/**
 * Original solution. TLE.
 * First thing come to mind is this one just like adjacent island. BFS to expand the head radius.
 */
class Solution {
public:
    int findRadius(vector<int>& houses, vector<int>& heaters) {
        unordered_set hs(houses.begin(), houses.end());

        int r = 0;
        deque<int> q(heaters.begin(), heaters.end());
        while (!hs.empty()) {
            auto const N = q.size();
            for (int i = 0; i < N; ++i) {
                auto n = q.front(); q.pop_front();
                if (hs.count(n)) {
                    hs.erase(n);
                }

                q.push_back(n + 1);
                q.push_back(n - 1);
            }
            ++r;
        }

        return r - 1;
    }
};
