/**
 * Maximum Prime Difference
 *
 * You are given an integer array nums.
 *
 * Return an integer that is the maximum distance between the indices of two (not necessarily
 * different) prime numbers in nums.
 *
 * Example 1:
 *
 * Input: nums = [4,2,9,5,3]
 *
 * Output: 3
 *
 * Explanation: nums[1], nums[3], and nums[4] are prime. So the answer is |4 - 1| = 3.
 *
 * Example 2:
 *
 * Input: nums = [4,8,2,8]
 *
 * Output: 0
 *
 * Explanation: nums[2] is prime. Because there is just one prime number, the answer is |2 - 2| = 0.
 *
 * Constraints:
 *     1 <= nums.length <= 3 * 105
 *     1 <= nums[i] <= 100
 *     The input is generated such that the number of prime numbers in the nums is at least one.
 */
/**
 * Original solution.
 */
class Solution {
public:
    int maximumPrimeDifference(vector<int>& nums) {
        auto is_prime = vector<bool>(101, true);

        is_prime[1] = false;

        for (auto i = 2; i < 101; ++i) {
            if (is_prime[i]) {
                for (auto k = i + i; k < 101; k = k + i) {
                    is_prime[k] = false;
                }
            }
        }
        auto prime_number = [&is_prime](auto n) { return is_prime[n]; };
        auto left = find_if(nums.begin(), nums.end(), prime_number);
        auto right = find_if(nums.rbegin(), nums.rend(), prime_number);

        return right.base() - left - 1;
    }
};
