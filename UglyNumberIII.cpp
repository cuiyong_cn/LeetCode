/**
 * Ugly Number III
 *
 * An ugly number is a positive integer that is divisible by a, b, or c.
 *
 * Given four integers n, a, b, and c, return the nth ugly number.
 *
 * Example 1:
 *
 * Input: n = 3, a = 2, b = 3, c = 5
 * Output: 4
 * Explanation: The ugly numbers are 2, 3, 4, 5, 6, 8, 9, 10... The 3rd is 4.
 * Example 2:
 *
 * Input: n = 4, a = 2, b = 3, c = 4
 * Output: 6
 * Explanation: The ugly numbers are 2, 3, 4, 6, 8, 9, 10, 12... The 4th is 6.
 * Example 3:
 *
 * Input: n = 5, a = 2, b = 11, c = 13
 * Output: 10
 * Explanation: The ugly numbers are 2, 4, 6, 8, 10, 11, 12, 13... The 5th is 10.
 * Example 4:
 *
 * Input: n = 1000000000, a = 2, b = 217983653, c = 336916467
 * Output: 1999999984
 *
 * Constraints:
 *
 * 1 <= n, a, b, c <= 109
 * 1 <= a * b * c <= 1018
 * It is guaranteed that the result will be in range [1, 2 * 109].
 */
/**
 * Original solution. Idea:
 *        a a a a  b b b b
 *     a        b  a        b
 *    a        b    a        b
 *    a        b    a        b
 *    a        c c c c       b
 *    a     c  b    a   c    b
 *     a   c    b  a     c  b
 *        aca a a  b b b c
 *         c             c
 *         c             c
 *          c           c
 *             c c c c
 *
 * F(N) = (total number of positive integers <= N which are divisible by a or b or c.).
 * F(N) = a + b + c - ab - bc - ac + abc
 *      = N/a + N/b + N/c - N/lcm(a,b) - N/lcm(b,c) - N/lcm(a,c) - N/lcm(a,b,c)
 *
 * lcm(a,b) = (a*b)/gcd(a,b)
 */
class Solution {
public:
    int nthUglyNumber(int k, int A, int B, int C) {
        int lo = 1, hi = 2 * 1e9;
        long a = A, b = B, c = C;
        long ab = a * b / gcd(a, b);
        long bc = b * c / gcd(b, c);
        long ac = a * c / gcd(a, c);
        long abc = a * bc / gcd(a, bc);
        while (lo < hi) {
            int mid = lo + (hi - lo)/2;
            int cnt = mid/a + mid/b + mid/c - mid/ab - mid/bc - mid/ac + mid/abc;
            if (cnt < k) {
                lo = mid + 1;
            }
            else {
                hi = mid;
            }
        }

        return lo;
    }
};
