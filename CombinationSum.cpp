/**
 * Combination Sum
 *
 * Given a set of candidate numbers (candidates) (without duplicates) and a target number (target), find all unique
 * combinations in candidates where the candidate numbers sums to target.
 *
 * The same repeated number may be chosen from candidates unlimited number of times.
 *
 * Note:
 *     All numbers (including target) will be positive integers.
 *     The solution set must not contain duplicate combinations.
 *
 * Example 1:
 *
 * Input: candidates = [2,3,6,7], target = 7,
 * A solution set is:
 * [
 *   [7],
 *   [2,2,3]
 * ]
 *
 * Example 2:
 *
 * Input: candidates = [2,3,5], target = 8,
 * A solution set is:
 * [
 *   [2,2,2,2],
 *   [2,3,3],
 *   [3,5]
 * ]
 */
/**
 * Original recursive solution. Other users's solution is similar.
 */
class Solution {
public:
    vector<vector<int>> ans;
    void helper(vector<int>& candidates, const vector<int>& combo, int start, int target) {
        if (0 == target) {
            if (false == combo.empty()) {
                ans.push_back(combo);
            }
            return;
        }

        if (start >= candidates.size()) return;

        int num = candidates[start];
        if (num > target) return;

        int count = target / num;
        for (int i = count; i >= 0; --i) {
            vector<int> newcombo = combo;
            for (int j = i; j > 0; --j) {
                newcombo.push_back(num);
            }
            helper(candidates, newcombo, start + 1, target - num * i);
        }
    }

    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
        sort(candidates.begin(), candidates.end());
        helper(candidates, {}, 0, target);

        return ans;
    }
};
