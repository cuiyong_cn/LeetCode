/**
 * Number of Good Ways to Split a String
 *
 * You are given a string s.
 *
 * A split is called good if you can split s into two non-empty strings sleft and sright where their
 * concatenation is equal to s (i.e., sleft + sright = s) and the number of distinct letters in
 * sleft and sright is the same.
 *
 * Return the number of good splits you can make in s.
 *
 * Example 1:
 *
 * Input: s = "aacaba"
 * Output: 2
 * Explanation: There are 5 ways to split "aacaba" and 2 of them are good.
 * ("a", "acaba") Left string and right string contains 1 and 3 different letters respectively.
 * ("aa", "caba") Left string and right string contains 1 and 3 different letters respectively.
 * ("aac", "aba") Left string and right string contains 2 and 2 different letters respectively (good split).
 * ("aaca", "ba") Left string and right string contains 2 and 2 different letters respectively (good split).
 * ("aacab", "a") Left string and right string contains 3 and 1 different letters respectively.
 * Example 2:
 *
 * Input: s = "abcd"
 * Output: 1
 * Explanation: Split the string as follows ("ab", "cd").
 *
 * Constraints:
 *
 * 1 <= s.length <= 105
 * s consists of only lowercase English letters.
 */
/**
 * less memory usage.
 */
class Solution {
public:
    int numSplits(string s) {
        int l[26] = {}, r[26] = {}, d_l = 0, d_r = 0, res = 0;
        for (auto ch : s) {
            d_r += ++r[ch - 'a'] == 1;
        }

        for (int i = 0; i < s.size(); ++i) {
            d_l += ++l[s[i] - 'a'] == 1;
            d_r -= --r[s[i] - 'a'] == 0;
            res += d_l == d_r;
        }

        return res;
    }
};

/**
 * Original solution.
 */
template<typename Iter>
vector<int> partial_distinct(Iter b, Iter e)
{
    int const N = distance(b, e);
    if (N <= 0) {
        return {};
    }

    vector<int> ans(N, 0);
    int dist = 0;
    unordered_set<typename Iter::value_type> seen;

    for (int i = 0; i < N; ++i) {
        auto el = *b; ++b;
        if (0 == seen.count(el)) {
            ++dist;
            seen.insert(el);
        }
        ans[i] = dist;
    }

    return ans;
}

class Solution {
public:
    int numSplits(string s) {
        int const N = s.length();
        auto const left = partial_distinct(s.cbegin(), s.cend());
        auto const right = partial_distinct(s.crbegin(), s.crend());

        int ans = 0;
        for (int i = 0; i < (N - 1); ++i) {
            if (left[i] == right[N - i - 2]) {
                ++ans;
            }
        }

        return ans;
    }
};
