/**
 * Count Number of Texts
 *
 * Alice is texting Bob using her phone. The mapping of digits to letters is shown in the figure below.
 *
 * In order to add a letter, Alice has to press the key of the corresponding digit i times, where i
 * is the position of the letter in the key.
 *
 * For example, to add the letter 's', Alice has to press '7' four times. Similarly, to add the
 * letter 'k', Alice has to press '5' twice.
 * Note that the digits '0' and '1' do not map to any letters, so Alice does not use them.
 * However, due to an error in transmission, Bob did not receive Alice's text message but received a
 * string of pressed keys instead.
 *
 * For example, when Alice sent the message "bob", Bob received the string "2266622".
 * Given a string pressedKeys representing the string received by Bob, return the total number of
 * possible text messages Alice could have sent.
 *
 * Since the answer may be very large, return it modulo 109 + 7.
 *
 * Example 1:
 *
 * Input: pressedKeys = "22233"
 * Output: 8
 * Explanation:
 * The possible text messages Alice could have sent are:
 * "aaadd", "abdd", "badd", "cdd", "aaae", "abe", "bae", and "ce".
 * Since there are 8 possible messages, we return 8.
 * Example 2:
 *
 * Input: pressedKeys = "222222222222222222222222222222222222"
 * Output: 82876089
 * Explanation:
 * There are 2082876103 possible text messages Alice could have sent.
 * Since we need to return the answer modulo 109 + 7, we return 2082876103 % (109 + 7) = 82876089.
 *
 * Constraints:
 *
 * 1 <= pressedKeys.length <= 105
 * pressedKeys only consists of digits from '2' - '9'.
 */
/**
 * Original solution.
 *
 * For every number other than 7 and 9, we can consider at most 3 consecutive digits whereas for 7
 * and 9 we can consider atmost 4 consecutive digits.
 *
 * Logic:
 *
 * Make an empty dp array.
 * Iterate through every key pressed in pressedKeys and for every i we can add dp[i-1],dp[i-2],
 * dp[i-3] to dp[i] and if pressedKeys[i] is either 7 or 9 then we can add dp[i-4] also.
 */
class Solution {
public:
    int countTexts(string k) {
        int dp[5] = {1, 1, 1, 1, 1}, n = k.size();
        for (int i = n - 1; i >= 0; --i) {
            dp[i % 5] = 0;
            int max_j = min(n, i + (k[i] == '7' || k[i] == '9' ? 4 : 3));
            for (int j = i; j < max_j && k[i] == k[j]; ++j)
                dp[i % 5] = (dp[i % 5] + dp[(j + 1) % 5]) % 1000000007;
        }
        return dp[0];
    }
};
