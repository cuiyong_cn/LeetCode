/**
 * Count Square Submatrices with All Ones
 *
 * Given a m * n matrix of ones and zeros, return how many square submatrices have all ones.
 *
 * Example 1:
 *
 * Input: matrix =
 * [
 *   [0,1,1,1],
 *   [1,1,1,1],
 *   [0,1,1,1]
 * ]
 * Output: 15
 * Explanation:
 * There are 10 squares of side 1.
 * There are 4 squares of side 2.
 * There is  1 square of side 3.
 * Total number of squares = 10 + 4 + 1 = 15.
 * Example 2:
 *
 * Input: matrix =
 * [
 *   [1,0,1],
 *   [1,1,0],
 *   [1,1,0]
 * ]
 * Output: 7
 * Explanation:
 * There are 6 squares of side 1.
 * There is 1 square of side 2.
 * Total number of squares = 6 + 1 = 7.
 *
 * Constraints:
 *
 * 1 <= arr.length <= 300
 * 1 <= arr[0].length <= 300
 * 0 <= arr[i][j] <= 1
 */
/**
 * DP solution. dp[i][j] represent the biggest square with matrix[i][j] as bottom right corner.
 * dp[i][j] also represent number of squares with matrix[i][j] as bottom right corner
 */
class Solution {
public:
    int countSquares(vector<vector<int>>& matrix) {
        int const M = matrix.size();
        int const N = matrix[0].size();
        int ans = 0;
        for (int i = 0; i < M; ++i) {
            for (int j = 0; j < N; ++j) {
                auto& val = matrix[i][j];
                if (1 == val && i > 0 && j > 0) {
                    val += min(matrix[i - 1][j - 1], min(matrix[i - 1][j], matrix[i][j - 1]));
                }
                ans += val;
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int countSquares(vector<vector<int>>& matrix) {
        int const M = matrix.size();
        int const N = matrix[0].size();
        vector<vector<int>> add_table(M + 1, vector<int>(N + 1, 0));
        for (int i = 0; i < M; ++i) {
            for (int j = 0; j < N; ++j) {
                add_table[i + 1][j + 1] = add_table[i][j + 1] + add_table[i + 1][j] - add_table[i][j] + matrix[i][j];
            }
        }

        int ans = add_table[M][N];
        int const max_len = min(M, N);
        for (int L = 2; L <= max_len; ++L) {
            int const expected = L * L;
            for (int i = 0; i <= (M - L); ++i) {
                for (int j = 0; j <= (N - L); ++j) {
                    int ri = i + L, rj = j + L;
                    int const sum = add_table[ri][rj] - add_table[ri][j] - add_table[i][rj] + add_table[i][j];
                    if (sum == expected) {
                        ++ans;
                    }
                }
            }
        }

        return ans;
    }
};
