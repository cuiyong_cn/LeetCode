/**
 * Minimum Cost Tree From Leaf Values
 *
 * Given an array arr of positive integers, consider all binary trees such that:
 *
 *  1  Each node has either 0 or 2 children;
 *  2  The values of arr correspond to the values of each leaf in an in-order traversal of
 *     the tree.  (Recall that a node is a leaf if and only if it has 0 children.)
 *  3  The value of each non-leaf node is equal to the product of the largest leaf value
 *     in its left and right subtree respectively.
 *
 * Among all possible binary trees considered, return the smallest possible sum of the
 * values of each non-leaf node.  It is guaranteed this sum fits into a 32-bit integer.
 *
 * Example 1:
 *
 * Input: arr = [6,2,4]
 * Output: 32
 * Explanation:
 * There are two possible trees.  The first has non-leaf node sum 36, and the second has
 * non-leaf node sum 32.
 *
 *     24            24
 *    /  \          /  \
 *   12   4        6    8
 *  /  \               / \
 * 6    2             2   4
 *
 * Constraints:
 *
 *  1  2 <= arr.length <= 40
 *  2  1 <= arr[i] <= 15
 *  3  It is guaranteed that the answer fits into a 32-bit signed integer (ie. it is less
 *     than 2^31).
 */
/**
 * Most efficient one. One pass
 */
class Solution
{
public:
    int mctFromLeafValues(vector<int>& A)
    {
        int res = 0, n = A.size();
        vector<int> stack = {INT_MAX};
        for (int a : A) {
            while (stack.back() <= a) {
                int mid = stack.back();
                stack.pop_back();
                res += mid * min(stack.back(), a);
            }
            stack.push_back(a);
        }
        for (int i = 2; i < stack.size(); ++i) {
            res += stack[i] * stack[i - 1];
        }
        return res;
    }
};

/**
 * Same as above, but a little bit slower
 * Until the size of the array is more than 1;
 * 1 Find the least a[i] * a[i+1] product and accumulate it;
 * 2 Remove the least element which is min of the multipliers
 */
class Solution
{
public:
    int mctFromLeafValues(vector<int>& a)
    {
        int sum = 0;
        while (a.size() > 1) {
            int min_product = INT_MAX;   // min(a[0]*a[1], ... a[N-1]*a[N])
            int me_index = -1;           // min(a[i], a[i+1])

            for (int i = 1; i < a.size(); ++i) {
                if ( min_product > a[i - 1]*a[i]) {
                    me_index = a[i - 1] < a[i] ? i - 1 : i;
                    min_product = a[i - 1] * a[i];
                }
            }
            sum += min_product;
            a.erase(a.begin() + me_index);
        }

        return sum;
    }
};

/**
 * Using dp method. But not efficient.
 */
class Solution
{
public:
    int memo[41][41];
    int maxi[41][41];

    int dp(int left, int right)
    {
        if (left == right)return 0; //leaf node
        if (memo[left][right] != -1)return memo[left][right];

        int ans = 1 << 30;

        for (int i = left; i < right; i++)
            ans = min(ans, maxi[left][i] * maxi[i + 1][right] + dp(left, i) + dp(i + 1, right) );

        memo[left][right] = ans;
        return ans;
    }

    int mctFromLeafValues(vector<int>& arr)
    {
        memset(memo, -1, sizeof(memo));
        for (int i = 0; i < arr.size(); i++) {
            maxi[i][i] = arr[i];
            for (int j = i + 1; j < arr.size(); j++)
                maxi[i][j] = max(maxi[i][j - 1], arr[j]);
        }

        return dp(0, arr.size() - 1);
    }
};

