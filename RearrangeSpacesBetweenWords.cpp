/**
 * Rearrange Spaces Between Words
 *
 * You are given a string text of words that are placed among some number of spaces. Each word
 * consists of one or more lowercase English letters and are separated by at least one space. It's
 * guaranteed that text contains at least one word.
 *
 * Rearrange the spaces so that there is an equal number of spaces between every pair of adjacent
 * words and that number is maximized. If you cannot redistribute all the spaces equally, place the
 * extra spaces at the end, meaning the returned string should be the same length as text.
 *
 * Return the string after rearranging the spaces.
 *
 * Example 1:
 *
 * Input: text = "  this   is  a sentence "
 * Output: "this   is   a   sentence"
 * Explanation: There are a total of 9 spaces and 4 words. We can evenly divide the 9 spaces between
 * the words: 9 / (4-1) = 3 spaces.
 * Example 2:
 *
 * Input: text = " practice   makes   perfect"
 * Output: "practice   makes   perfect "
 * Explanation: There are a total of 7 spaces and 3 words. 7 / (3-1) = 3 spaces plus 1 extra space.
 * We place this extra space at the end of the string.
 * Example 3:
 *
 * Input: text = "hello   world"
 * Output: "hello   world"
 * Example 4:
 *
 * Input: text = "  walks  udp package   into  bar a"
 * Output: "walks  udp  package  into  bar  a "
 * Example 5:
 *
 * Input: text = "a"
 * Output: "a"
 *
 * Constraints:
 *
 * 1 <= text.length <= 100
 * text consists of lowercase English letters and ' '.
 * text contains at least one word.
 */
/**
 * Shorten below code a step further.
 */
class Solution {
public:
    string reorderSpaces(string text) {
        auto space_count = std::count(text.begin(), text.end(), ' ');
        std::stringstream ss{text};
        std::vector<std::string> words{std::istream_iterator<std::string>(ss),
                                       std::istream_iterator<std::string>()};

        int gap = words.size() > 1 ? space_count / (words.size() - 1) : 0;
        auto ans = std::accumulate(words.begin(), words.end(), std::string{}, [gap](auto i, auto w) {
                                       return i += w + string(gap, ' ');
                                   });
        return ans.substr(0, ans.length() - gap) + std::string(space_count - (words.size() - 1) * gap, ' ');;
    }
};

/**
 * Improve the below code.
 */
class Solution {
public:
    string reorderSpaces(string text) {
        auto space_count = std::count(text.begin(), text.end(), ' ');
        std::stringstream ss{text};
        std::vector<std::string> words{std::istream_iterator<std::string>(ss),
                                       std::istream_iterator<std::string>()};

        int interval_spaces = words.size() > 1 ? space_count / (words.size() - 1) : space_count;
        int extra_spaces = words.size() > 1 ? space_count % (words.size() - 1) : space_count;
        std::string ans;
        for (int i = 0; i < words.size() - 1; ++i) {
            ans += words[i] + std::string(interval_spaces, ' ');
        }
        ans += words.back();
        ans += std::string(extra_spaces, ' ');

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    string reorderSpaces(string text) {
        auto space_count = std::count(text.begin(), text.end(), ' ');
        std::vector<std::string> words;
        std::stringstream ss{text};
        while (false == ss.eof()) {
            std::string word;
            ss >> word;
            if (false == word.empty()) {
                words.emplace_back(std::move(word));
            }
        }

        int interval_spaces = words.size() > 1 ? space_count / (words.size() - 1) : space_count;
        int extra_spaces = words.size() > 1 ? space_count % (words.size() - 1) : space_count;
        std::string ans;
        for (int i = 0; i < words.size() - 1; ++i) {
            ans += words[i] + std::string(interval_spaces, ' ');
        }
        ans += words.back();
        if (extra_spaces > 0) {
            ans += std::string(extra_spaces, ' ');
        }

        return ans;
    }
};
