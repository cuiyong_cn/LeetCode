/**
 * Recover a Tree From Preorder Traversal
 *
 * We run a preorder depth first search on the root of a binary tree.
 *
 * At each node in this traversal, we output D dashes (where D is the depth of this node),
 * then we output the value of this node.  (If the depth of a node is D,
 * the depth of its immediate child is D+1.  The depth of the root node is 0.)
 *
 * If a node has only one child, that child is guaranteed to be the left child.
 *
 * Given the output S of this traversal, recover the tree and return its root.
 *
 * Example 1:
 *                       1
 *                      / \
 *                     /   \
 *                    2     5
 *                   / \   / \
 *                  3   4 6   7
 *
 * Input: "1-2--3--4-5--6--7"
 * Output: [1,2,5,3,4,6,7]
 *
 * Example 2:
 *                       1
 *                      / \
 *                     2   5
 *                    /   /
 *                   3   6
 *                  /   /
 *                 4   7
 *
 * Input: "1-2--3---4-5--6---7"
 * Output: [1,2,5,3,null,6,null,4,null,7]
 *
 * Example 3:
 *                      1
 *                     /
 *                   401
 *                   /  \
 *                 349  88
 *                 /
 *                90
 *
 * Input: "1-401--349---90--88"
 * Output: [1,401,null,349,88,90]
 *
 * Note:
 *
 *  1. The number of nodes in the original tree is between 1 and 1000.
 *  2. Each node will have a value between 1 and 10^9.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/**
 * more short one, but the thinking is the same
 *
 *  TreeNode* recoverFromPreorder(string S) {
 *      vector<TreeNode*> stack;
 *      for (int i = 0, level, val; i < S.length();) {
 *          for (level = 0; S[i] == '-'; i++)
 *              level++;
 *          for (val = 0; i < S.length() && S[i] != '-'; i++)
 *              val = val * 10 + S[i] - '0';
 *          TreeNode* node = new TreeNode(val);
 *          while (stack.size() > level) stack.pop_back();
 *          if (!stack.empty())
 *              if (!stack.back()->left) stack.back()->left = node;
 *              else stack.back()->right = node;
 *          stack.push_back(node);
 *      }
 *      return stack[0];
 *  }
 */
class Solution {
public:
    TreeNode* recoverFromPreorder(string S) {
        int preLevel = 0;
        int level = 0;

        stringstream in(S);
        vector<TreeNode*> stack;
        int val = 0;
        in >> val;
        stack.push_back(new TreeNode(val));

        while (!in.eof()) {
            if (in.peek() == '-') {
                ++level;
                in.get();
            }
            else {
                in >> val;
                TreeNode* node = new TreeNode(val);
                if (level <= preLevel) {
                    int disLevel = preLevel - level + 1;

                    for (int i = 0; i < disLevel; ++i) {
                        stack.pop_back();
                    }
                    stack.back()->right = node;
                }
                else {
                    stack.back()->left = node;
                }

                preLevel = level;
                level = 0;
                stack.push_back(node);
            }
        }

        return stack[0];
    }
};
