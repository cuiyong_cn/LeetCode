/**
 * Reverse Substrings Between Each Pair of Parentheses
 *
 * You are given a string s that consists of lower case English letters and brackets.
 *
 * Reverse the strings in each pair of matching parentheses, starting from the innermost one.
 *
 * Your result should not contain any brackets.
 *
 * Example 1:
 *
 * Input: s = "(abcd)"
 * Output: "dcba"
 *
 * Example 2:
 *
 * Input: s = "(u(love)i)"
 * Output: "iloveu"
 * Explanation: The substring "love" is reversed first, then the whole string is reversed.
 *
 * Example 3:
 *
 * Input: s = "(ed(et(oc))el)"
 * Output: "leetcode"
 * Explanation: First, we reverse the substring "oc", then "etco", and finally, the whole string.
 *
 * Example 4:
 *
 * Input: s = "a(bcdefghijkl(mno)p)q"
 * Output: "apmnolkjihgfedcbq"
 *
 * Constraints:
 *
 *     0 <= s.length <= 2000
 *     s only contains lower case English characters and parentheses.
 *     It's guaranteed that all parentheses are balanced.
 */
/**
 * Very impressive one. The explanation as below:
 *
 *       --------------
 *       |   ------   |
 *       |   |    |   |
 *       |   ∨    |   |
 * ----->(<--(--->)<--)------>
 *       |   ^    |   ^
 *       |   |    |   |
 *       |   ------   |
 *       --------------
 */
class Solution {
    string reverseParentheses(string s) {
        int n = s.length();
        vector<int> opened, pair(n);
        for (int i = 0; i < n; ++i) {
            if (s[i] == '(')
                opened.push_back(i);
            if (s[i] == ')') {
                int j = opened.back();
                opened.pop_back();
                pair[i] = j;
                pair[j] = i;
            }
        }
        string res;
        for (int i = 0, d = 1; i < n; i += d) {
            if (s[i] == '(' || s[i] == ')')
                i = pair[i], d = -d;
            else
                res += s[i];
        }
        return res;
    }
};
/**
 * Improved version.
 */
class Solution {
    string reverseParentheses(string s) {
        stack<int> opened;
        string res;
        for (int i = 0; i < s.length(); ++i) {
            if (s[i] == '(')
                opened.push(res.length());
            else if (s[i] == ')') {
                int j = opened.top();
                opened.pop();
                reverse(res.begin() + j, res.end());
            } else {
                res += s[i];
            }
        }
        return res;
    }
};
/**
 * My original ugly solution.
 */
class Solution {
public:
    string reverseParentheses(string s) {
        int depth = 0;
        for (int i = 0; i < s.size(); ++i) {
            if ('(' == s[i]) {
                ++depth;
                for (int j = i + 1; j < s.size(); ++j) {
                    if ('(' == s[j]) {
                        s[j] = ')';
                        ++depth;
                    }
                    else if (')' == s[j]) {
                        s[j] = '(';
                        --depth;
                        if (0 == depth) {
                            s[j] = ')';
                            reverse(s.begin() + i + 1, s.begin() + j);
                            break;
                        }
                    }
                }
                depth = 0;
            }
        }
        string ans;
        for (auto& c : s) {
            if ('(' != c && ')' != c) {
                ans += c;
            }
        }
        return ans;
    }
};
