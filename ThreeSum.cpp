/**
 * Three sum
 *
 * Given an integer array nums, return all the triplets [nums[i], nums[j], nums[k]] such that i !=
 * j, i != k, and j != k, and nums[i] + nums[j] + nums[k] == 0.
 *
 * Notice that the solution set must not contain duplicate triplets.
 *
 * Example 1:
 *
 * Input: nums = [-1,0,1,2,-1,-4]
 * Output: [[-1,-1,2],[-1,0,1]]
 * Example 2:
 *
 * Input: nums = []
 * Output: []
 * Example 3:
 *
 * Input: nums = [0]
 * Output: []
 *
 * Constraints:
 *
 * 0 <= nums.length <= 3000
 * -105 <= nums[i] <= 105
 */
/**
 * Same idea, but sort the nums.
 */
class Solution {
public:
    vector<vector<int> > threeSum(vector<int> &num) {
        vector<vector<int> > res;

        std::sort(num.begin(), num.end());

        for (int i = 0; i < num.size(); i++) {
            int target = -num[i];
            int front = i + 1;
            int back = num.size() - 1;

            while (front < back) {
                int sum = num[front] + num[back];
                if (sum < target) {
                    ++front;
                }
                else if (sum > target) {
                    --back;
                }
                else {
                    vector<int> triplet = {num[i], num[front], num[back]};
                    res.push_back(triplet);

                    // Processing duplicates of Number 2
                    // Rolling the front pointer to the next different number forwards
                    while (front < back && num[front] == triplet[1]) {
                        front++;
                    }

                    // Processing duplicates of Number 3
                    // Rolling the back pointer to the next different number backwards
                    while (front < back && num[back] == triplet[2]) {
                        back--;
                    }
                }
            }

            // Processing duplicates of Number 1
            while ((i + 1) < num.size() && num[i + 1] == num[i]) {
                i++;
            }
        }

        return res;

    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums) {
        map<int, int> int_count;
        for (auto n : nums) {
            ++int_count[n];
        }

        vector<vector<int>> ans;
        for (auto it = begin(int_count); it != end(int_count); ++it) {
            if (it->first > 0) {
                break;
            }

            --it->second;
            for (auto in = it; in != end(int_count); ++in) {
                if (in->second > 0) {
                    auto sum = it->first + in->first;
                    if ((0 - sum) >= in->first) {
                        --in->second;
                        auto third = int_count.find(0 - sum);
                        if (third != end(int_count) && third->second > 0) {
                            ans.push_back({it->first, in->first, third->first});
                        }
                    }

                    ++in->second;
                }
            }
            ++it->second;
        }

        return ans;
    }
};
