/**
 * N-ary Tree Preorder Traversal
 *
 * Given an n-ary tree, return the preorder traversal of its nodes' values.
 *
 * For example, given a 3-ary tree:
 *                        1
 *                       /|\
 *                      / | \
 *                     3  2  4
 *                    / \
 *                   5   6
 *
 * Return its preorder traversal as: [1,3,5,6,2,4].
 *
 * Note:
 *
 * Recursive solution is trivial, could you do it iteratively?
 */
/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/
/**
 * Use the stack one.
 *
 * class Solution {
 * public:
 *     vector<int> preorder(Node* root) {
 *         vector<int> result;
 *         if (root == nullptr) {
 *             return result;
 *         }
 *
 *         stack<Node*> stk;
 *         stk.push(root);
 *         while (!stk.empty()) {
 *             Node* cur = stk.top();
 *             stk.pop();
 *             result.push_back(cur -> val);
 *             for (auto it = cur->children.rbegin(); it != cur->children.rend(); ++it) {
 *                 stk.push(*it);
 *             }
 *         }
 *         return result;
 *     }
 * };
*/
/**
 * Recursive one
 *
 * class Solution {
 * private:
 *     void travel(Node* root, vector<int>& result) {
 *         if (root == nullptr) {
 *             return;
 *         }
 *
 *         result.push_back(root -> val);
 *         for (Node* child : root -> children) {
 *             travel(child, result);
 *         }
 *     }
 * public:
 *     vector<int> preorder(Node* root) {
 *         vector<int> result;
 *         travel(root, result);
 *         return result;
 *     }
 * };
 */
/**
 * My solution use list. But seems like list is not efficient.
 */
class Solution {
public:
    vector<int> preorder(Node* root) {
        vector<int> ans;

        if (!root) return ans;
        list<Node*> li;
        li.push_back(root);
        do {
            Node* tn = li.front();
            li.pop_front();
            ans.push_back(tn->val);
            li.insert(li.begin(), tn->children.begin(), tn->children.end());
        } while (!li.empty());

        return ans;
    }
};
