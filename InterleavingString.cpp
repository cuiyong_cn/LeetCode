/**
 * Interleaving String
 *
 * Given strings s1, s2, and s3, find whether s3 is formed by an interleaving of s1 and s2.
 *
 * An interleaving of two strings s and t is a configuration where they are divided into non-empty
 * substrings such that:
 *
 * s = s1 + s2 + ... + sn
 * t = t1 + t2 + ... + tm
 * |n - m| <= 1
 * The interleaving is s1 + t1 + s2 + t2 + s3 + t3 + ... or t1 + s1 + t2 + s2 + t3 + s3 + ...
 * Note: a + b is the concatenation of strings a and b.
 *
 * Example 1:
 *
 * Input: s1 = "aabcc", s2 = "dbbca", s3 = "aadbbcbcac"
 * Output: true
 * Example 2:
 *
 * Input: s1 = "aabcc", s2 = "dbbca", s3 = "aadbbbaccc"
 * Output: false
 * Example 3:
 *
 * Input: s1 = "", s2 = "", s3 = ""
 * Output: true
 *
 * Constraints:
 *
 * 0 <= s1.length, s2.length <= 100
 * 0 <= s3.length <= 200
 * s1, s2, and s3 consist of lowercase English letters.
 *
 * Follow up: Could you solve it using only O(s2.length) additional memory space?
 */
/**
 * As always, DP can be reduce to one less dimension space usage.
 */
public class Solution {
    public boolean isInterleave(String s1, String s2, String s3) {
        if (s3.length() != s1.length() + s2.length()) {
            return false;
        }
        boolean dp[] = new boolean[s2.length() + 1];
        for (int i = 0; i <= s1.length(); i++) {
            for (int j = 0; j <= s2.length(); j++) {
                if (i == 0 && j == 0) {
                    dp[j] = true;
                } else if (i == 0) {
                    dp[j] = dp[j - 1] && s2.charAt(j - 1) == s3.charAt(i + j - 1);
                } else if (j == 0) {
                    dp[j] = dp[j] && s1.charAt(i - 1) == s3.charAt(i + j - 1);
                } else {
                    dp[j] = (dp[j] && s1.charAt(i - 1) == s3.charAt(i + j - 1)) || (dp[j - 1] && s2.charAt(j - 1) == s3.charAt(i + j - 1));
                }
            }
        }
        return dp[s2.length()];
    }
}

/**
 * DP solution.
 */
public class Solution {
    public boolean isInterleave(String s1, String s2, String s3) {
        if (s3.length() != s1.length() + s2.length()) {
            return false;
        }

        boolean dp[][] = new boolean[s1.length() + 1][s2.length() + 1];
        for (int i = 0; i <= s1.length(); i++) {
            for (int j = 0; j <= s2.length(); j++) {
                if (i == 0 && j == 0) {
                    dp[i][j] = true;
                } else if (i == 0) {
                    dp[i][j] = dp[i][j - 1] && s2.charAt(j - 1) == s3.charAt(i + j - 1);
                } else if (j == 0) {
                    dp[i][j] = dp[i - 1][j] && s1.charAt(i - 1) == s3.charAt(i + j - 1);
                } else {
                    dp[i][j] = (dp[i - 1][j] && s1.charAt(i - 1) == s3.charAt(i + j - 1)) || (dp[i][j - 1] && s2.charAt(j - 1) == s3.charAt(i + j - 1));
                }
            }
        }

        return dp[s1.length()][s2.length()];
    }
}

/**
 * Using memo to speed up.
 */
class Solution {
public:
    struct State
    {
        string const& s1;
        int32_t p1;
        string const& s2;
        int32_t p2;
    };

    bool isInterleave(string s1, string s2, string s3) {
        if ((s1.length() + s2.length()) != s3.length()) {
            return false;
        }

        array<int, 26> ch_cnt = { 0, };
        for (auto const& ch : s1) {
            ++ch_cnt[ch - 'a'];
        }

        for (auto const& ch : s2) {
            ++ch_cnt[ch - 'a'];
        }

        for (auto const& ch : s3) {
            --ch_cnt[ch - 'a'];
        }

        for (auto n : ch_cnt) {
            if (n != 0) {
                return false;
            }
        }

        vector<vector<bool>> memo(s1.length() + 1, vector<bool>(s2.length() + 1, false));

        return dfs({s1, 0, s2, 0}, s3, 0, memo);
    }

private:
    bool dfs(State const& s, string const& t, int pos, vector<vector<bool>>& memo)
    {
        if (pos >= t.length()) {
            return true;
        }

        if (memo[s.p1][s.p2]) {
            return false;
        }

        if (s.p1 < s.s1.length() && s.s1[s.p1] == t[pos]) {
            if (dfs({s.s1, s.p1 + 1, s.s2, s.p2}, t, pos + 1, memo)) {
                return true;
            }
        }

        if (s.p2 < s.s2.length() && s.s2[s.p2] == t[pos]) {
            if (dfs({s.s1, s.p1, s.s2, s.p2 + 1}, t, pos + 1, memo)) {
                return true;
            }
        }

        memo[s.p1][s.p2] = true;

        return false;
    }
};

/**
 * Brute force solution. TLE as expected.
 */
class Solution {
public:
    struct State
    {
        string const& s1;
        int32_t p1;
        string const& s2;
        int32_t p2;
    };

    bool isInterleave(string s1, string s2, string s3) {
        if ((s1.length() + s2.length()) != s3.length()) {
            return false;
        }

        array<int, 26> ch_cnt = { 0, };
        for (auto const& ch : s1) {
            ++ch_cnt[ch - 'a'];
        }

        for (auto const& ch : s2) {
            ++ch_cnt[ch - 'a'];
        }

        for (auto const& ch : s3) {
            --ch_cnt[ch - 'a'];
        }

        for (auto n : ch_cnt) {
            if (n != 0) {
                return false;
            }
        }

        if (s1_part({s1, 0, s2, 0}, s3, 0)) {
            return true;
        }

        return s2_part({s1, 0, s2, 0}, s3, 0);
    }

private:
    bool s1_part(State const& s, string const& t, int pos)
    {
        if (pos >= t.length()) {
            return true;
        }

        int longest = 0;
        auto const& s1 = s.s1;
        auto p1 = s.p1;

        for (int i = pos, j = p1; i < t.length() && j < s1.length(); ++i, ++j) {
            if (t[i] != s1[j]) {
                break;
            }
            ++longest;
        }

        for (int k = 1; k <= longest; ++k) {
            if (s2_part({s1, p1 + k, s.s2, s.p2}, t, pos + k)) {
                return true;
            }
        }

        return false;
    }

    bool s2_part(State const& s, string const& t, int pos)
    {
        if (pos >= t.length()) {
            return true;
        }

        int longest = 0;
        auto const& s2 = s.s2;
        auto p2 = s.p2;

        for (int i = pos, j = p2; i < t.length() && j < s2.length(); ++i, ++j) {
            if (t[i] != s2[j]) {
                break;
            }
            ++longest;
        }

        for (int k = 1; k <= longest; ++k) {
            if (s1_part({s.s1, s.p1, s2, p2 + k}, t, pos + k)) {
                return true;
            }
        }

        return false;
    }
};
