/**
 * Find a Peak Element II
 *
 * A peak element in a 2D grid is an element that is strictly greater than all of its adjacent
 * neighbors to the left, right, top, and bottom.
 *
 * Given a 0-indexed m x n matrix mat where no two adjacent cells are equal, find any peak element
 * mat[i][j] and return the length 2 array [i,j].
 *
 * You may assume that the entire matrix is surrounded by an outer perimeter with the value -1 in
 * each cell.
 *
 * You must write an algorithm that runs in O(m log(n)) or O(n log(m)) time.
 *
 * Example 1:
 *
 * Input: mat = [[1,4],[3,2]]
 * Output: [0,1]
 * Explanation: Both 3 and 4 are peak elements so [1,0] and [0,1] are both acceptable answers.
 * Example 2:
 *
 * Input: mat = [[10,20,15],[21,30,14],[7,16,32]]
 * Output: [1,1]
 * Explanation: Both 30 and 32 are peak elements so [1,1] and [2,2] are both acceptable answers.
 *
 * Constraints:
 *
 * m == mat.length
 * n == mat[i].length
 * 1 <= m, n <= 500
 * 1 <= mat[i][j] <= 105
 * No two adjacent cells are equal.
 */
/**
 * More Efficient one.
 */
class Solution {
public:
    vector<int> findPeakGrid(vector<vector<int>>& mat) {
        int const rows = mat.size();
        int startCol = 0, endCol = mat[0].size()-1;

        while (startCol <= endCol) {
            int maxRow = 0;
            int midCol = (startCol + endCol) / 2;

            for (int row=0; row < rows; ++row) {
                maxRow = mat[row][midCol] >= mat[maxRow][midCol] ? row : maxRow;
            }

            bool leftIsBig = midCol - 1 >= startCol && mat[maxRow][midCol-1] > mat[maxRow][midCol];
            bool rightIsBig = midCol+1 <= endCol && mat[maxRow][midCol+1] > mat[maxRow][midCol];

            if (!leftIsBig && !rightIsBig) { // we have found the peak element
                return {maxRow, midCol};
            }
            else if (rightIsBig) {
                // if rightIsBig, then there is an element in 'right' that is bigger than all the elements in the 'midCol',
                startCol = midCol + 1;    //so 'midCol' cannot have a 'peakPlane'
            }
            else {
                endCol = midCol - 1;
            }
        }

        return {-1, -1};
    }
};

/**
 * Original solution. Flooding solution. Worst case O(M*N)
 */
bool outside_matrix(vector<vector<int>> const& mat, pair<int, int> const& coord)
{
    int const rows = mat.size();
    int const cols = mat[0].size();
    return coord.first < 0 || coord.first >= rows || coord.second < 0 || coord.second >= cols;
}

bool higher(vector<vector<int>> const& mat, pair<int, int> const& coord1, pair<int, int> const& coord2)
{
    if (outside_matrix(mat, coord2)) {
        return true;
    }

    return mat[coord1.first][coord1.second] > mat[coord2.first][coord2.second];
}

bool is_peak(vector<vector<int>> const& mat, pair<int, int> const& coord)
{
    return higher(mat, coord, {coord.first, coord.second - 1})
        && higher(mat, coord, {coord.first, coord.second + 1})
        && higher(mat, coord, {coord.first - 1, coord.second})
        && higher(mat, coord, {coord.first + 1, coord.second});
}

void find_candidate(deque<pair<int, int>>& dq, vector<vector<int>> const& mat, pair<int, int> const& coord1, pair<int, int> const& coord2)
{
    if (outside_matrix(mat, coord2)) {
        return;
    }

    if (higher(mat, coord2, coord1)) {
        dq.emplace_back(coord2);
    }
}

class Solution {
public:
    vector<int> findPeakGrid(vector<vector<int>>& mat) {
        int const rows = mat.size();
        int const cols = mat[0].size();

        vector<vector<int>> seen(rows, vector<int>(cols, 0));

        deque<pair<int, int>> dq{{0, 0}};

        while (!dq.empty()) {
            auto coord = dq.front();
            dq.pop_front();

            if (seen[coord.first][coord.second]) {
                continue;
            }

            seen[coord.first][coord.second] = 1;

            if (is_peak(mat, coord)) {
                return {coord.first, coord.second};
            }
            else {
                find_candidate(dq, mat, coord, {coord.first, coord.second - 1});
                find_candidate(dq, mat, coord, {coord.first, coord.second + 1});
                find_candidate(dq, mat, coord, {coord.first - 1, coord.second});
                find_candidate(dq, mat, coord, {coord.first + 1, coord.second});
            }
        }

        return {0, 0};
    }
};
