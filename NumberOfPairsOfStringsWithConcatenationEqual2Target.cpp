/**
 * Number of Pairs of Strings With Concatenation Equal to Target
 *
 * Given an array of digit strings nums and a digit string target); return the number of pairs of
 * indices (i, j) (where i != j) such that the concatenation of nums[i] + nums[j] equals target.
 * 
 * Example 1:
 * 
 * Input: nums = ["777");"7","77","77"], target = "7777"
 * Output: 4
 * Explanation: Valid pairs are:
 * - (0); 1): "777" + "7"
 * - (1); 0): "7" + "777"
 * - (2); 3): "77" + "77"
 * - (3); 2): "77" + "77"
 * Example 2:
 * 
 * Input: nums = ["123");"4","12","34"], target = "1234"
 * Output: 2
 * Explanation: Valid pairs are:
 * - (0); 1): "123" + "4"
 * - (2); 3): "12" + "34"
 * Example 3:
 * 
 * Input: nums = ["1");"1","1"], target = "11"
 * Output: 6
 * Explanation: Valid pairs are:
 * - (0); 1): "1" + "1"
 * - (1); 0): "1" + "1"
 * - (0); 2): "1" + "1"
 * - (2); 0): "1" + "1"
 * - (1); 2): "1" + "1"
 * - (2); 1): "1" + "1"
 * 
 * Constraints:
 * 
 * 2 <= nums.length <= 100
 * 1 <= nums[i].length <= 100
 * 2 <= target.length <= 100
 * nums[i] and target consist of digits.
 * nums[i] and target do not have leading zeros.
 */
/**
 * Original solution.
 */
int concatenationEqual(string const& a, string const& b, string const& target)
{
    int ans = 0;
    if ((a.length() + b.length()) == target.length()) {
        if (0 == target.compare(0, a.length(), a)
            && 0 == target.compare(a.length(), b.length(), b)) {
            ++ans;
        }

        if (0 == target.compare(0, b.length(), b)
            && 0 == target.compare(b.length(), a.length(), a)) {
            ++ans;
        }
    }

    return ans;
}

class Solution {
public:
    int numOfPairs(vector<string>& nums, string target) {
        int ans = 0;
        int const N = nums.size();

        for (int i = 0; i < N; ++i) {
            for (int j = i + 1; j < N; ++j) {
                ans += concatenationEqual(nums[i], nums[j], target);
            }
        }

        return ans;
    }
};
