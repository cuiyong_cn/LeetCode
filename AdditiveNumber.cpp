/**
 * Additive Number
 *
 * Additive number is a string whose digits can form additive sequence.
 *
 * A valid additive sequence should contain at least three numbers. Except for the first two
 * numbers, each subsequent number in the sequence must be the sum of the preceding two.
 *
 * Given a string containing only digits '0'-'9', write a function to determine if it's an additive number.
 *
 * Note: Numbers in the additive sequence cannot have leading zeros, so sequence 1, 2, 03 or 1, 02,
 * 3 is invalid.
 *
 * Example 1:
 *
 * Input: "112358"
 * Output: true
 * Explanation: The digits can form an additive sequence: 1, 1, 2, 3, 5, 8.
 *              1 + 1 = 2, 1 + 2 = 3, 2 + 3 = 5, 3 + 5 = 8
 * Example 2:
 *
 * Input: "199100199"
 * Output: true
 * Explanation: The additive sequence is: 1, 99, 100, 199.
 *              1 + 99 = 100, 99 + 100 = 199
 *
 * Constraints:
 *
 * num consists only of digits '0'-'9'.
 * 1 <= num.length <= 35
 * Follow up:
 * How would you handle overflow for very large input integers?
 */
/**
 * Origianal solution. Actually we can improve this further as a's length can not be exceed the
 * num.length() / 2;
 */
class Solution {
public:
    bool isAdditiveNumber(string num) {
        int const N = num.length();
        if (N < 3) {
            return false;
        }

        int const ML = N - 1;
        for (int L = 2; L <= ML; ++L) {
            for (int a = 1; a < L; ++a) {
                if ('0' == num[0] && a > 1) {
                    return false;
                }

                if ('0' == num[a] && (L - a) > 1) {
                    continue;
                }

                if (match(num, num.substr(0, a), num.substr(a, L - a), L)) {
                    return true;
                }
            }
        }

        return false;
    }

private:
    bool match(string const& num, string const& a, string const& b, int pos)
    {
        if (pos >= num.length()) {
            return true;
        }

        auto sum = add(a, b);
        if (0 == num.compare(pos, sum.length(), sum)) {
            return match(num, b, sum, pos + sum.length());
        }

        return false;
    }

    string add(string const& a, string const& b)
    {
        string ans;
        int carry = 0;
        while (int i = a.length() - 1, j = b.length() - 1; i >= 0 || j >= 0; --i, --j) {
            auto sum = (i >= 0 ? a[i] - '0' : 0) + (j >= 0 ? b[j] - '0' : 0) + carry;
            carry = sum > 9;
            sum -= carry > 0 ? 10 : 0;
            ans.push_back('0' + sum);
        }

        if (carry > 0) {
            ans.push_back('1');
        }

        reverse(begin(ans), end(ans));
        return ans;
    }
};
