/**
 * House Robber II
 *
 * You are a professional robber planning to rob houses along a street. Each house has a certain
 * amount of money stashed. All houses at this place are arranged in a circle. That means the first
 * house is the neighbor of the last one. Meanwhile, adjacent houses have a security system
 * connected, and it will automatically contact the police if two adjacent houses were broken into
 * on the same night.
 *
 * Given an integer array nums representing the amount of money of each house, return the maximum
 * amount of money you can rob tonight without alerting the police.
 *
 * Example 1:
 *
 * Input: nums = [2,3,2]
 * Output: 3
 * Explanation: You cannot rob house 1 (money = 2) and then rob house 3 (money = 2), because they
 * are adjacent houses.
 * Example 2:
 *
 * Input: nums = [1,2,3,1]
 * Output: 4
 * Explanation: Rob house 1 (money = 1) and then rob house 3 (money = 3).
 * Total amount you can rob = 1 + 3 = 4.
 * Example 3:
 *
 * Input: nums = [0]
 * Output: 0
 *
 * Constraints:
 *
 * 1 <= nums.length <= 100
 * 0 <= nums[i] <= 1000
 */
/**
 * DP solution.
 */
class Solution {
public:
    int rob(vector<int>& nums) {
        int n = nums.size();
        if (n < 2) {
            return n > 0 ? nums[0] : 0;
        }

        return max(robber(nums, 0, n - 2), robber(nums, 1, n - 1));
    }

private:
    int robber(vector<int>& nums, int l, int r)
    {
        int prepre = 0, pre = 0;
        for (int i = l; i <= r; i++) {
            auto cur = max(prepre + nums[i], pre);
            prepre = pre;
            pre = cur;
        }

        return pre;
    }
};

/**
 * Original solution. DFS and memo.
 */
class Solution {
public:
    int rob(vector<int>& nums) {
        if (1 == nums.size()) {
            return nums[0];
        }

        auto m1 = rob(nums, 0, nums.size() - 1);
        visited.clear();
        auto m2 = rob(nums, 1, nums.size());
        return max(m1, m2);
    }

private:
    int rob(vector<int> const& nums, int start, int end)
    {
        if (start >= end) {
            return 0;
        }

        if (visited.count(start)) {
            return visited[start];
        }

        // we rob this house, skip the next one
        auto m1 = rob(nums, start + 2, end) + nums[start];

        // we don't rob this one
        auto m2 = rob(nums, start + 1, end);

        auto max_money = max(m1, m2);
        visited[start] = max_money;
        return max_money;
    }

private:
    unordered_map<int, int> visited;
};
