/**
 * Make Number of Distinct Characters Equal
 *
 * You are given two 0-indexed strings word1 and word2.
 *
 * A move consists of choosing two indices i and j such that 0 <= i < word1.length and 0 <= j <
 * word2.length and swapping word1[i] with word2[j].
 *
 * Return true if it is possible to get the number of distinct characters in word1 and word2 to be
 * equal with exactly one move. Return false otherwise.
 *
 * Example 1:
 *
 * Input: word1 = "ac", word2 = "b"
 * Output: false
 * Explanation: Any pair of swaps would yield two distinct characters in the first string, and one in the second string.
 * Example 2:
 *
 * Input: word1 = "abcc", word2 = "aab"
 * Output: true
 * Explanation: We swap index 2 of the first string with index 0 of the second string. The resulting strings are word1 = "abac" and word2 = "cab", which both have 3 distinct characters.
 * Example 3:
 *
 * Input: word1 = "abcde", word2 = "fghij"
 * Output: true
 * Explanation: Both resulting strings will have 5 distinct characters, regardless of which indices we swap.
 *
 * Constraints:
 *
 * 1 <= word1.length, word2.length <= 105
 * word1 and word2 consist of only lowercase English letters.
 */
/**
 * Original solution.
 */
class Solution {
public:
    bool isItPossible(string word1, string word2) {
        auto char_cnt1 = array<int, 26>{ 0, };
        auto char_cnt2 = char_cnt1;
        for (auto c : word1) {
            ++char_cnt1[c - 'a'];
        }

        for (auto c : word2) {
            ++char_cnt2[c - 'a'];
        }

        auto positive = [](auto n) { return n > 0; };
        for (auto i = 0; i < 26; ++i) {
            if (0 == char_cnt1[i]) {
                continue;
            }
            for (auto j = 0; j < 26; ++j) {
                if (0 == char_cnt2[j]) {
                    continue;
                }
                --char_cnt1[i];
                ++char_cnt1[j];
                --char_cnt2[j];
                ++char_cnt2[i];
                auto dis_cnt1 = count_if(begin(char_cnt1), end(char_cnt1), positive);
                auto dis_cnt2 = count_if(begin(char_cnt2), end(char_cnt2), positive);
                if (dis_cnt1 == dis_cnt2) {
                    return true;
                }
                ++char_cnt1[i];
                --char_cnt1[j];
                ++char_cnt2[j];
                --char_cnt2[i];
            }
        }

        return false;
    }
};
