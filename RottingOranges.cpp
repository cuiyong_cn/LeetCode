/**
 * Rotting Oranges
 *
 * In a given grid, each cell can have one of three values:
 *     the value 0 representing an empty cell;
 *     the value 1 representing a fresh orange;
 *     the value 2 representing a rotten orange.
 *
 * Every minute, any fresh orange that is adjacent (4-directionally) to a rotten orange becomes rotten.
 *
 * Return the minimum number of minutes that must elapse until no cell has a fresh orange.  If this is impossible,
 * return -1 instead.
 *
 * Example 1:
 *
 * Input: [[2,1,1],[1,1,0],[0,1,1]]
 * Output: 4
 *
 * Example 2:
 *
 * Input: [[2,1,1],[0,1,1],[1,0,1]]
 * Output: -1
 * Explanation:  The orange in the bottom left corner (row 2, column 0) is never rotten, because rotting only happens 4-directionally.
 *
 * Example 3:
 *
 * Input: [[0,2]]
 * Output: 0
 * Explanation:  Since there are already no fresh oranges at minute 0, the answer is just 0.
 *
 * Note:
 *     1 <= grid.length <= 10
 *     1 <= grid[0].length <= 10
 *     grid[i][j] is only 0, 1, or 2.
 */
/**
 * Another solution from discussion. BFS solution.
 */
class Solution {
    public int orangesRotting(int[][] grid) {
        if(grid == null || grid.length == 0) return 0;
        int rows = grid.length;
        int cols = grid[0].length;
        Queue<int[]> queue = new LinkedList<>();
        int count_fresh = 0;
        //Put the position of all rotten oranges in queue
        //count the number of fresh oranges
        for(int i = 0 ; i < rows ; i++) {
            for(int j = 0 ; j < cols ; j++) {
                if(grid[i][j] == 2) {
                    queue.offer(new int[]{i , j});
                }
                else if(grid[i][j] == 1) {
                    count_fresh++;
                }
            }
        }
        //if count of fresh oranges is zero --> return 0
        if(count_fresh == 0) return 0;
        int count = 0;
        int[][] dirs = {{1,0},{-1,0},{0,1},{0,-1}};
        //bfs starting from initially rotten oranges
        while(!queue.isEmpty()) {
            ++count;
            int size = queue.size();
            for(int i = 0 ; i < size ; i++) {
                int[] point = queue.poll();
                for(int dir[] : dirs) {
                    int x = point[0] + dir[0];
                    int y = point[1] + dir[1];
                    //if x or y is out of bound
                    //or the orange at (x , y) is already rotten
                    //or the cell at (x , y) is empty
                        //we do nothing
                    if(x < 0 || y < 0 || x >= rows || y >= cols || grid[x][y] == 0 || grid[x][y] == 2) continue;
                    //mark the orange at (x , y) as rotten
                    grid[x][y] = 2;
                    //put the new rotten orange at (x , y) in queue
                    queue.offer(new int[]{x , y});
                    //decrease the count of fresh oranges by 1
                    count_fresh--;
                }
            }
        }
        return count_fresh == 0 ? count-1 : -1;
    }
}

/**
 * Original solution.
 */
struct Direction
{
    int dx;
    int dy;
};

class Solution {
public:
    int orangesRotting(vector<vector<int>>& grid) {
        int minutes = -1;
        std::vector<Direction> dir{ {-1, 0}, {0, -1}, {0, 1}, {1, 0} };
        int round = 2;
        while (1) {
            int inffected = 0;
            int fresh = 0;
            for (size_t i = 0; i < grid.size(); ++i) {
                for (size_t j = 0; j < grid[0].size(); ++j) {
                    if (grid[i][j] == round) {
                        // inffect around
                        for (auto d : dir) {
                            int nx = i + d.dx;
                            int ny = j + d.dy;
                            if (0 <= nx && nx < grid.size() && 0 <= ny && ny < grid[0].size()) {
                                if (1 == grid[nx][ny]) {
                                    grid[nx][ny] = round + 1;
                                    ++inffected;
                                }
                            }
                        }
                    }
                    else if (1 == grid[i][j]) {
                        ++fresh;
                    }
                }
            }

            if (0 == inffected) {
                if (-1 == minutes) {
                    minutes = 0;
                }
                if (fresh) {
                    minutes = -1;
                }
                break;
            }

            minutes += minutes == -1 ? 2 : 1;
            round += 1;
        }

        return minutes;
    }
};
