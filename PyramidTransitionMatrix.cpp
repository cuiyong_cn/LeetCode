/**
 * Pyramid Transition Matrix
 *
 * We are stacking blocks to form a pyramid. Each block has a color which is a one letter string.
 *
 * We are allowed to place any color block C on top of two adjacent blocks of colors A and B, if and only if ABC is
 * an allowed triple.
 *
 * We start with a bottom row of bottom, represented as a single string. We also start with a list of allowed triples
 * allowed. Each allowed triple is represented as a string of length 3.
 *
 * Return true if we can build the pyramid all the way to the top, otherwise false.
 *
 * Example 1:
 *
 * Input: bottom = "BCD", allowed = ["BCG", "CDE", "GEA", "FFF"]
 * Output: true
 * Explanation:
 * We can stack the pyramid like this:
 *     A
 *    / \
 *   G   E
 *  / \ / \
 * B   C   D
 *
 * We are allowed to place G on top of B and C because BCG is an allowed triple.  Similarly, we can place E on top of
 * C and D, then A on top of G and E.
 *
 * Example 2:
 *
 * Input: bottom = "AABA", allowed = ["AAA", "AAB", "ABA", "ABB", "BAC"]
 * Output: false
 * Explanation:
 * We can't stack the pyramid to the top.
 * Note that there could be allowed triples (A, B, C) and (A, B, D) with C != D.
 *
 * Note:
 *
 * bottom will be a string with length in range [2, 8].
 * allowed will have length in range [0, 200].
 * Letters in all strings will be chosen from the set {'A', 'B', 'C', 'D', 'E', 'F', 'G'}.
 */
/**
 * Original solution. The idea is simple.
 * 1. We check if the bottom has any unvalid block.
 * 2. If bottom does not contain any unvalid block, then we form all the possible upper bottoms and check for each
 *    of them recursively
 */
class Solution {
public:
    map<string, vector<char>> allowedMap;
    void buildMap(vector<string>& allowed) {
        for (auto& s : allowed) {
            allowedMap[s.substr(0, 2)].push_back(s[2]);
        }
    }

    bool dfs(string& bottom) {
        if (1 == bottom.length()) return true;

        vector<vector<char>> bottomChars(bottom.length() - 1);
        for (int i = 0; i < bottom.length() - 1; ++i) {
            if (0 == allowedMap.count(bottom.substr(i, 2))) {
                return false;
            }
            bottomChars[i] = allowedMap[bottom.substr(i, 2)];
        }

        deque<string> q;
        for (int i = 0; i < bottomChars.size(); ++i) {
            auto& chars = bottomChars[i];
            if (q.empty()) {
                for (auto& c : chars) {
                    q.push_back(string{c});
                }
            }
            else {
                int len = q.size();
                for (int i = 0; i < len; ++i) {
                    string s = q.front();
                    q.pop_front();
                    for (auto& c : chars) {
                        q.push_back(s + c);
                    }
                }
            }
        }

        for (auto& b : q) {
            if (dfs(b)) {
                return true;
            }
        }

        return false;
    }

    bool pyramidTransition(string bottom, vector<string>& allowed) {
        buildMap(allowed);

        return dfs(bottom);
    }
};

/**
 * From user wxd_sjtu, shorter but with poor readability I think.
 * Using dfs and backtracking.
 */
class Solution {
public:
  bool pyramidTransition(string bottom, vector<string>& allowed) {
    unordered_map<string, vector<char>> m;
    for(auto& s:allowed) m[s.substr(0, 2)].push_back(s.back());
    return helper(bottom, m, 0, "");
  }
  bool helper(string bottom, unordered_map<string, vector<char>>& m, int start, string next){
    if(bottom.size() == 1) return true;
    if(start == (int)bottom.size() - 1) return helper(next, m, 0, "");
    for(char c : m[bottom.substr(start, 2)])
      if(helper(bottom, m, start+1, next+c)) return true;
    return false;
  }
};
