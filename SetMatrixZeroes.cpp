/**
 * Set Matrix Zeroes
 *
 * Given an m x n matrix. If an element is 0, set its entire row and column to 0. Do it in-place.
 *
 * Follow up:
 *
 * A straight forward solution using O(mn) space is probably a bad idea.
 * A simple improvement uses O(m + n) space, but still not the best solution.
 * Could you devise a constant space solution?
 *
 * Example 1:
 *
 * Input: matrix = [[1,1,1],[1,0,1],[1,1,1]]
 * Output: [[1,0,1],[0,0,0],[1,0,1]]
 * Example 2:
 *
 * Input: matrix = [[0,1,2,0],[3,4,5,2],[1,3,1,5]]
 * Output: [[0,0,0,0],[0,4,5,0],[0,3,1,0]]
 *
 * Constraints:
 *
 * m == matrix.length
 * n == matrix[0].length
 * 1 <= m, n <= 200
 * -231 <= matrix[i][j] <= 231 - 1
 */
/**
 * Without using set. O(1) space solution.
 */
class Solution {
public:
    void setZeroes(vector<vector<int>>& matrix) {
        bool col0 = false;
        int m = matrix.size();
        int n = matrix.front().size();

        for (int r = 0; r < m; ++r) {
            if (0 == matrix[r][0]) {
                col0 = true;
            }

            for (int c = 1; c < n; ++c) {
                if (0 == matrix[r][c]) {
                    matrix[r][0] = 0;
                    matrix[0][c] = 0;
                }
            }
        }

        for (int r = 1; r < m; ++r) {
            for (int c = 1; c < n; ++c) {
                if (0 == matrix[r][0] || 0 == matrix[0][c]) {
                    matrix[r][c] = 0;
                }
            }
        }

        if (0 == matrix[0][0]) {
            for (auto& cell : matrix[0]) {
                cell = 0;
            }
        }

        if (col0) {
            for (int r = 0; r < m; ++r) {
                matrix[r][0] = 0;
            }
        }
    }
};

/**
 * Original solution. Using set.
 */
class Solution {
public:
    void setZeroes(vector<vector<int>>& matrix) {
        std::set<std::pair<int, int>> zero_rows_cols;
        int m = matrix.size();
        int n = matrix.front().size();

        for (int r = 0; r < m; ++r) {
            for (int c = 0; c < n; ++c) {
                if (0 == matrix[r][c]) {
                    zero_rows_cols.insert({1, r});
                    zero_rows_cols.insert({2, c});
                }
            }
        }

        for (auto zrc : zero_rows_cols) {
            if (1 == zrc.first) {
                zero_row(matrix, zrc.second);
            }
            else {
                zero_col(matrix, zrc.second);
            }
        }
    }

private:
    void zero_row(vector<vector<int>>& matrix, int row)
    {
        for (auto& cell : matrix[row]) {
            cell = 0;
        }
    }

    void zero_col(vector<vector<int>>& matrix, int col)
    {
        int rows = matrix.size();
        for (int r = 0; r < rows; ++r) {
            matrix[r][col] = 0;
        }
    }
};
