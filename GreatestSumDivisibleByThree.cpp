/**
 * Greatest Sum Divisible by Three
 *
 * Given an array nums of integers, we need to find the maximum possible sum of elements of the array such that it is
 * divisible by three.
 *
 * Example 1:
 *
 * Input: nums = [3,6,5,1,8]
 * Output: 18
 * Explanation: Pick numbers 3, 6, 1 and 8 their sum is 18 (maximum sum divisible by 3).
 * Example 2:
 *
 * Input: nums = [4]
 * Output: 0
 * Explanation: Since 4 is not divisible by 3, do not pick any number.
 * Example 3:
 *
 * Input: nums = [1,2,3,4,4]
 * Output: 12
 * Explanation: Pick numbers 1, 3, 4 and 4 their sum is 12 (maximum sum divisible by 3).
 *
 * Constraints:
 *
 * 1 <= nums.length <= 4 * 10^4
 * 1 <= nums[i] <= 10^4
 */
/**
 * Continue to simplify
 */
class Solution {
public:
    int maxSumDivThree(vector<int>& nums) {
        vector<int> cur(3, 0);
        vector<int> pre(3, 0);

        for (auto n : nums) {
            pre = cur;
            for (auto m : pre) {
                int mod = (n + m) % 3;
                cur[mod] = std::max(cur[mod], n + m);
            }
        }

        return cur[0];
    }
};

/**
 * And as always, we can reduce the space usgae.
 */
class Solution {
public:
    int maxSumDivThree(vector<int>& nums) {
        if (1 == nums.size()) {
            return nums.front() % 3 ? 0 : nums.front();
        }

        vector<int> cur(3, 0);
        vector<int> pre(3, 0);

        pre[nums.back() % 3] = nums.back();

        for (int i = nums.size() - 2; i >= 0; --i) {
            int mod = nums[i] % 3;
            cur[mod] = std::max(pre[mod], pre[0] + nums[i]);
            for (int j = 1; j < 3; ++j) {
                int new_mod = (j + mod) % 3;
                cur[new_mod] = std::max(pre[new_mod], pre[j] > 0 ? (pre[j] + nums[i]) : 0);
            }
            swap(cur, pre);
        }

        return pre[0];
    }
};

/**
 * More compact one.
 */
class Solution {
public:
    int maxSumDivThree(vector<int>& nums) {
        if (1 == nums.size()) {
            return nums.front() % 3 ? 0 : nums.front();
        }

        vector<vector<int>> dp(nums.size(), vector<int>(3, 0));

        dp.back()[nums.back() % 3] = nums.back();

        for (int i = nums.size() - 2; i >= 0; --i) {
            int mod = nums[i] % 3;
            for (int j = 0; j < 3; ++j) {
                int new_mod = (j + mod) % 3;
                if (0 == j) {
                    dp[i][new_mod] = std::max(dp[i + 1][new_mod], dp[i + 1][j] + nums[i]);
                }
                else {
                    dp[i][new_mod] = std::max(dp[i + 1][new_mod], dp[i + 1][j] > 0 ? (dp[i + 1][j] + nums[i]) : 0);
                }
            }
        }

        return dp[0][0];
    }
};

/**
 * Original solution. DP starting from the back.
 */
class Solution {
public:
    int maxSumDivThree(vector<int>& nums) {
        if (1 == nums.size()) {
            return nums.front() % 3 ? 0 : nums.front();
        }

        vector<vector<int>> dp(nums.size(), vector<int>(3, 0));

        dp.back()[nums.back() % 3] = nums.back();

        for (int i = nums.size() - 2; i >= 0; --i) {
            int mod = nums[i] % 3;
            if (0 == mod) {
                dp[i][0] = dp[i + 1][0] + nums[i];
                dp[i][1] = dp[i + 1][1] > 0 ? (dp[i + 1][1] + nums[i]) : 0;
                dp[i][2] = dp[i + 1][2] > 0 ? (dp[i + 1][2] + nums[i]) : 0;
            }
            else if (1 == mod) {
                dp[i][0] = std::max(dp[i + 1][0], dp[i + 1][2] > 0 ? (dp[i + 1][2] + nums[i]) : 0);
                dp[i][1] = std::max(dp[i + 1][1], dp[i + 1][0] + nums[i]);
                dp[i][2] = std::max(dp[i + 1][2], dp[i + 1][1] > 0 ? (dp[i + 1][1] + nums[i]) : 0);
            }
            else if (2 == mod) {
                dp[i][0] = std::max(dp[i + 1][0], dp[i + 1][1] > 0 ? (dp[i + 1][1] + nums[i]) : 0);
                dp[i][1] = std::max(dp[i + 1][1], dp[i + 1][2] > 0 ? (dp[i + 1][2] + nums[i]) : 0);
                dp[i][2] = std::max(dp[i + 1][2], dp[i + 1][0] + nums[i]);
            }
        }

        return dp[0][0];
    }
};
