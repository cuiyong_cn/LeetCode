/**
 * Magic Squares In Grid
 *
 * A 3 x 3 magic square is a 3 x 3 grid filled with distinct numbers from 1 to 9 such that each row,
 * column, and both diagonals all have the same sum.
 *
 * Given a row x col grid of integers, how many 3 x 3 "magic square" subgrids are there?  (Each
 * subgrid is contiguous).
 *
 * Example 1:
 *
 * Input: grid = [[4,3,8,4],[9,5,1,9],[2,7,6,2]]
 * Output: 1
 * Explanation:
 * The following subgrid is a 3 x 3 magic square:
 *
 * while this one is not:
 *
 * In total, there is only one magic square inside the given grid.
 * Example 2:
 *
 * Input: grid = [[8]]
 * Output: 0
 * Example 3:
 *
 * Input: grid = [[4,4],[3,3]]
 * Output: 0
 * Example 4:
 *
 * Input: grid = [[4,7,8],[9,5,1],[2,3,6]]
 * Output: 0
 *
 * Constraints:
 *
 * row == grid.length
 * col == grid[i].length
 * 1 <= row, col <= 10
 * 0 <= grid[i][j] <= 15
 */
/**
 * Original solution.
 * consider square:
 *
 * a1 a2 a3   a1 + a2 + a3 = X  |
 * a4 a5 a6   a4 + a5 + a6 = X  |-> Sum(s) = 3X
 * a7 a8 a9   a7 + a8 + a9 = X  |
 *
 *            a1 + a5 + a9 = X  |
 *            a2 + a5 + a8 = X  | - Sum(s) + 3 * a5 = 4X
 *            a3 + a5 + a7 = X  | - a5 = X / 3
 *            a4 + a5 + a6 = X  |
 *
 * Clearly X should be greater than 11 (minmum is 9 + 1 + 2)
 * So a5 could be 4, 5, 6, 7, 8, 9
 * But a5 must only be equal to 5, and
 * Even number should be at corners,
 * Odd number at the edge
 */
class Solution {
public:
    int numMagicSquaresInside(vector<vector<int>>& grid) {
        int const R = grid.size();
        int const C = grid[0].size();
        if (R < 3 || C < 3) {
            return 0;
        }

        int ans = 0;
        int const RE = R - 3;
        int const CE = C - 3;
        for (int i = 0; i <= RE; ++i) {
            for (int j = 0; j <= CE; ++j) {
                // i, j is top left corner of the square, check if square is the magic
                if (is_magic_square(grid, i, j)) {
                    ++ans;
                }
            }
        }

        return ans;
    }

private:
    bool is_magic_square(vector<vector<int>> const& g, int tlx, int tly)
    {
        if (5 != g[tlx + 1][tly + 1]) {
            return false;
        }

        int M = 0;
        int const sum = g[tlx][tly] + g[tlx + 1][tly + 1] + g[tlx + 2][tly + 2];
        // check diagonal sum is the same
        if (sum != (g[tlx][tly + 2] + g[tlx + 1][tly + 1] + g[tlx + 2][tly])) {
            return false;
        }

        // check each row
        for (int L = 0; L < 3; ++L) {
            int const rsum = g[tlx + L][tly] + g[tlx + L][tly + 1] + g[tlx + L][tly + 2];
            if (rsum != sum) {
                return false;
            }
            M |= 1 << g[tlx + L][tly];
            M |= 1 << g[tlx + L][tly + 1];
            M |= 1 << g[tlx + L][tly + 2];
        }

        int const magic = 0b1'111'111'110;
        if (M != magic) {
            return false;
        }

        // check each column
        for (int L = 0; L < 3; ++L) {
            int const csum = g[tlx][tly + L] + g[tlx + 1][tly + L] + g[tlx + 2][tly + L];
            if (csum != sum) {
                return false;
            }
        }

        return true;
    }
};
