/**
 * Beautiful Arrangement II
 *
 * Given two integers n and k, you need to construct a list which contains n different positive integers ranging from 1
 * to n and obeys the following requirement:
 *
 * Suppose this list is [a1, a2, a3, ... , an], then the list [|a1 - a2|, |a2 - a3|, |a3 - a4|, ... , |an-1 - an|] has
 * exactly k distinct integers.
 *
 * If there are multiple answers, print any of them.
 *
 * Example 1:
 * Input: n = 3, k = 1
 * Output: [1, 2, 3]
 * Explanation: The [1, 2, 3] has three different positive integers ranging from 1 to 3, and the [1, 1] has exactly 1
 * distinct integer: 1.
 *
 * Example 2:
 * Input: n = 3, k = 2
 * Output: [1, 3, 2]
 * Explanation: The [1, 3, 2] has three different positive integers ranging from 1 to 3, and the [2, 1] has exactly 2
 * distinct integers: 1 and 2.
 *
 * Note:
 * The n and k are in the range 1 <= k < n <= 10^4.
 */
/**
 * And the following solution I think is most intuitive.
 *
 * if you have n number, the maximum k can be n - 1;
 * if n is 9, max k is 8.
 * This can be done by picking numbers interleavingly from head and tail,
 *
 * // start from i = 1, j = n;
 * // i++, j--, i++, j--, i++, j--
 *
 * i: 1   2   3   4   5
 * j:   9   8   7   6
 * out: 1 9 2 8 3 7 4 6 5
 * dif:  8 7 6 5 4 3 2 1
 *
 * Above is a case where k is exactly n - 1
 * When k is less than that, simply lay out the rest (i, j) in incremental
 * order(all diff is 1). Say if k is 5:
 *
 *      i++ j-- i++ j--  i++ i++ i++ ...
 * out: 1   9   2   8    3   4   5   6   7
 * dif:   8   7   6   5    1   1   1   1
 */
class Solution {
public:
    vector<int> constructArray(int n, int k) {
        vector<int> ans(n);
        int head = 1, tail = n;
        int i = 0;
        while (i < n) {
            if (k > 1) {
                if (i % 2 == 1) {
                    ans[i] = head; ++head;
                }
                else {
                    ans[i] = tail; --tail;
                }
                --k;
            }
            else {
                ans[i] = head; ++head;
            }
            ++i;
        }

        return ans;
    }
};

/**
 * Original solution.
 * We just need to adjust the first k number.
 * For example 8 -- 5
 * 1   6  2   5  3   4  7  8
 *   5  -4  3  -2  1
 */
class Solution {
public:
    vector<int> constructArray(int n, int k) {
        vector<int> ans(n, 0);
        for (int i = 0; i < n; ++i) {
            ans[i] = i + 1;
        }
        int s = k;
        for (int i = 1; i <= k; ++i) {
            ans[i] = ans[i - 1] + s;
            s = s > 0 ? -(s - 1) : -(s + 1);
        }

        return ans;
    }
};

/**
 * Same idea. With different thought.
 */
class Solution {
public:
    vector<int> constructArray(int n, int k) {
        vector<int> ans(n, 0);
        int c = 0;
        for (int v = 1; v < (n - k); ++v) {
            ans[c] = v; ++c;
        }

        for (int i = 0; i <= k; ++i) {
            ans[c] = (i % 2 == 0) ? (n - k + i / 2) : (n - i / 2);
            c++;
        }
        return ans;
    }
};
