/**
 * Power of Three
 *
 * Given an integer n, return true if it is a power of three. Otherwise, return false.
 *
 * An integer n is a power of three, if there exists an integer x such that n == 3x.
 *
 * Example 1:
 *
 * Input: n = 27
 * Output: true
 * Example 2:
 *
 * Input: n = 0
 * Output: false
 * Example 3:
 *
 * Input: n = 9
 * Output: true
 * Example 4:
 *
 * Input: n = 45
 * Output: false
 *
 * Constraints:
 *
 * -231 <= n <= 231 - 1
 *
 * Follow up: Could you solve it without loops/recursion?
 */
/**
 * Math solution. There is another logarithmic solution. But c++ is not java.
 *
 * Since 3 is a prime number, the only divisors of 3^19 are 3^1, 3^2, 3^3, ...,  3^19,
 * therefore all we need to do is divide 3^19 by n. A remainder of 0 means n is a divisor
 * of 3^19 and therefore a power of three
 */
class Solution {
public:
    bool isPowerOfThree(int n) {
        if (n <= 0) {
            return false;
        }

        int const max_num_of_power_three = 1162261467;
        return 0 == (max_num_of_power_three % n);
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool isPowerOfThree(int n) {
        if (n <= 0) {
            return false;
        }

        while (0 == (n % 3)) {
            n /= 3;
        }

        return 1 == n;
    }
};
