/**
 * Complete Binary Tree Inserter
 *
 * A complete binary tree is a binary tree in which every level, except possibly the last, is completely filled, and
 * all nodes are as far left as possible.
 *
 * Write a data structure CBTInserter that is initialized with a complete binary tree and supports the following operations:
 *
 *     CBTInserter(TreeNode root) initializes the data structure on a given tree with head node root;
 *     CBTInserter.insert(int v) will insert a TreeNode into the tree with value node.val = v so that the tree remains
 *     complete, and returns the value of the parent of the inserted TreeNode;
 *     CBTInserter.get_root() will return the head node of the tree.
 *
 * Example 1:
 *
 * Input: inputs = ["CBTInserter","insert","get_root"], inputs = [[[1]],[2],[]]
 * Output: [null,1,[1,2]]
 *
 * Example 2:
 *
 * Input: inputs = ["CBTInserter","insert","insert","get_root"], inputs = [[[1,2,3,4,5,6]],[7],[8],[]]
 * Output: [null,3,4,[1,2,3,4,5,6,7,8]]
 *
 * Note:
 *
 *     The initial given tree is complete and contains between 1 and 1000 nodes.
 *     CBTInserter.insert is called at most 10000 times per test case.
 *     Every value of a given or inserted node is between 0 and 5000.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/**
 * Improved version.
 */
class CBTInserter {
public:
    CBTInserter(TreeNode* root) {
        deque<TreeNode*> tq;
        r = root;
        if (nullptr != r) {
            tq.push_back(r);
        }
        while (false == tq.empty()) {
            TreeNode* t = tq.front();
            if (nullptr == t->left || nullptr == t->right) {
                q.push_back(t);
            }
            if (nullptr != t->left) {
                tq.push_back(t->left);
            }
            if (nullptr != t->right) {
                tq.push_back(t->right);
            }
            tq.pop_front();
        }
    }

    int insert(int v) {
        TreeNode* node = new TreeNode(v);
        TreeNode* t = q.front();

        if (nullptr == t->left) {
            t->left = node;
        }
        else {
            t->right = node;
            q.pop_front();
        }

        q.push_back(node);
        return t->val;
    }

    TreeNode* get_root() {
        return r;
    }
private:
    TreeNode* r;
    deque<TreeNode*> q;
};

/**
 * Original solution. Actually, we don't need the level and amount of nodes in level "level" info.
 */
class CBTInserter {
public:
    CBTInserter(TreeNode* root) {
        lvl = 0;
        lvlCount = 0;

        r = root;
        if (nullptr != r) {
            stk.push_back(r);
            ++lvlCount;
        }

        while (false == stk.empty()) {
            int shouldHave = pow(2, lvl);
            if (lvlCount != shouldHave) {
                break;
            }

            if (nullptr == stk.front()->left) {
                break;
            }

            ++lvl;
            lvlCount = 0;
            for (int i = 0; i < shouldHave; ++i) {
                TreeNode* t = stk.front();
                if (nullptr != t->left) {
                    stk.push_back(t->left);
                    ++lvlCount;
                }
                if (nullptr != t->right) {
                    stk.push_back(t->right);
                    ++lvlCount;
                }
                if (nullptr == t->left || nullptr == t->right) {
                    break;
                }
                else {
                    stk.pop_front();
                }
            }
        }
    }

    int insert(int v) {
        TreeNode* node = new TreeNode(v);
        TreeNode* t = stk.front();
        int shouldHave = 0 == lvl ? 1 : lvl << 1;
        if (lvlCount != shouldHave) {
            if (nullptr == t->left) {
                t->left = node;
            }
            else {
                t->right = node;
                stk.pop_front();
            }
            ++lvlCount;
        }
        else {
            ++lvl;
            lvlCount = 1;
            t->left = node;
        }
        stk.push_back(node);
        return t->val;
    }

    TreeNode* get_root() {
        return r;
    }
private:
    int lvl;
    int lvlCount;
    TreeNode* r;
    deque<TreeNode*> stk;
};

/**
 * Your CBTInserter object will be instantiated and called as such:
 * CBTInserter* obj = new CBTInserter(root);
 * int param_1 = obj->insert(v);
 * TreeNode* param_2 = obj->get_root();
 */
