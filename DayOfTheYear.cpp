/**
 * Day of the Year
 *
 * Given a string date representing a Gregorian calendar date formatted as YYYY-MM-DD, return the day number of the year.
 *
 * Example 1:
 *
 * Input: date = "2019-01-09"
 * Output: 9
 * Explanation: Given date is the 9th day of the year in 2019.
 *
 * Example 2:
 *
 * Input: date = "2019-02-10"
 * Output: 41
 *
 * Example 3:
 *
 * Input: date = "2003-03-01"
 * Output: 60
 *
 * Example 4:
 *
 * Input: date = "2004-03-01"
 * Output: 61
 *
 * Constraints:
 *     date.length == 10
 *     date[4] == date[7] == '-', and all other date[i]'s are digits
 *     date represents a calendar date between Jan 1st, 1900 and Dec 31, 2019.
 */
/**
 * Original solution.
 */
int is_leap_year(int year)
{
    if (year % 4) return 0;

    if (year % 100) return 1;

    if (year % 400) return 0;

    return 1;
}

class Solution {
public:
    int dayOfYear(string date) {
        int year = stoi(date.substr(0, 4));
        int month = stoi(date.substr(5, 2));
        int day = stoi(date.substr(8, 2));

        std::array<int, 12> days{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        for (int m = 1; m < month; ++m) {
            if (2 == m) {
                if (is_leap_year(year)) day += 1;
            }
            day += days[m - 1];
        }

        return day;
    }
};
