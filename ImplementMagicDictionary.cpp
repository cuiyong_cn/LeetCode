/**
 * Implement Magic Dictionary
 *
 * Implement a magic directory with buildDict, and search methods.
 *
 * For the method buildDict, you'll be given a list of non-repetitive words to build a dictionary.
 *
 * For the method search, you'll be given a word, and judge whether if you modify exactly one character into another
 * character in this word, the modified word is in the dictionary you just built.
 *
 * Example 1:
 *
 * Input: buildDict(["hello", "leetcode"]), Output: Null Input: search("hello"), Output: False Input: search("hhllo"),
 * Output: True Input: search("hell"), Output: False Input: search("leetcoded"), Output: False
 *
 * Note:
 *
 *    You may assume that all the inputs are consist of lowercase letters a-z.  For contest purpose, the test data is
 *    rather small by now. You could think about highly efficient algorithm after the contest.  Please remember to RESET
 *    your class variables declared in class MagicDictionary, as static/class variables are persisted across multiple test
 *    cases. Please see here for more details.
 */
/**
 * Neighbor solution.
 */
class MagicDictionary {
public:
    set<string> words;
    map<string, int> neighbors;
    /** Initialize your data structure here. */
    MagicDictionary() {

    }

    /** Build a dictionary through a list of words */
    void buildDict(vector<string> dict) {
        words.insert(dict.begin(), dict.end());
        for (string w : words) {
            for (int i = 0; i < w.length(); ++i) {
                char letter = w[i];
                w[i] = '*';
                ++neighbors[w];
                w[i] = letter;
            }
        }
    }

    /** Returns if there is any word in the trie that equals to the given word after modifying exactly one character */
    bool search(string word) {
        for (int i = 0; i < word.length(); ++i) {
            char letter = word[i];
            word[i] = '*';
            auto it = neighbors.find(word);
            if (it != neighbors.end()) {
                word[i] = letter;
                if (it->second > 1 || words.count(word) == 0) return true;
            }
            word[i] = letter;
        }
        return false;
    }
};

/**
 * More simple one.
 */
class MagicDictionary {
public:
    vector<string> words;
    /** Initialize your data structure here. */
    MagicDictionary() {

    }

    /** Build a dictionary through a list of words */
    void buildDict(vector<string> dict) {
        words = dict;
    }

    /** Returns if there is any word in the trie that equals to the given word after modifying exactly one character */
    bool search(string word) {
        bool ans = false;
        for (auto& w : words) {
            ans = diffByOneChar(w, word);
            if (ans) break;
        }
        return ans;
    }

    bool diffByOneChar(const string& s1, const string& s2) {
        if (s1.length() != s2.length()) return false;
        int diff = 0;
        for (int i = 0; i < s1.length(); ++i) {
            if (s1[i] != s2[i]) ++diff;
        }
        return 1 == diff ? true : false;
    }
};

/**
 * Original solution .Using Trie data structure. Not complete yet. But efficient enough.
 */
class MagicDictionary {
public:
    struct Trie {
        Trie() : data(26), isWord{false} {}
        vector<unique_ptr<Trie>> data;
        bool isWord;
    };

    unique_ptr<Trie> mdict;
    /** Initialize your data structure here. */
    MagicDictionary() : mdict{new Trie{}} {

    }

    /** Build a dictionary through a list of words */
    void buildDict(vector<string> dict) {
        for (auto& word : dict) {
            Trie* t = mdict.get();
            for (auto& c : word) {
                if (nullptr == t->data[c - 'a']) {
                    t->data[c - 'a'] = make_unique<Trie>();
                }
                t = t->data[c - 'a'].get();
            }
            t->isWord = true;
        }
    }

    /** Returns if there is any word in the trie that equals to the given word after modifying exactly one character */
    bool search(string word) {
        Trie* t = mdict.get();
        int i = 0;
        for (; i < word.length(); ++i) {
            char c = word[i];
            if (nullptr == t->data[c - 'a']) {
                break;
            }
            t = t->data[c - 'a'].get();
        }

        if (i == word.length()) {
            return false;
        }

        bool areRestSame = false;
        char diffChar = word[i];
        for (int k = 0; k < 26; ++k) {
            if (t->data[k]) {
                Trie* t1 = t->data[k].get();
                bool same = true;
                for (int j = i + 1; j < word.size(); ++j) {
                    if (nullptr == t1->data[word[j] - 'a'].get()) {
                        same = false;
                        break;
                    }
                    t1 = t1->data[word[j] - 'a'].get();
                }
                if (same && t1->isWord) {
                    areRestSame = true;
                    break;
                }
            }
        }

        return areRestSame;
    }
};

/**
 * Your MagicDictionary object will be instantiated and called as such:
 * MagicDictionary* obj = new MagicDictionary();
 * obj->buildDict(dict);
 * bool param_2 = obj->search(word);
 */
