/**
 * Convert Sorted List to Binary Search Tree
 *
 * Given the head of a singly linked list where elements are sorted in ascending order, convert it to a height balanced
 * BST.
 *
 * For this problem, a height-balanced binary tree is defined as a binary tree in which the depth of the two subtrees of
 * every node never differ by more than 1.
 *
 * Example 1:
 *
 * Input: head = [-10,-3,0,5,9]
 * Output: [0,-3,9,-10,null,5]
 * Explanation: One possible answer is [0,-3,9,-10,null,5], which represents the shown height balanced BST.
 * Example 2:
 *
 * Input: head = []
 * Output: []
 * Example 3:
 *
 * Input: head = [0]
 * Output: [0]
 * Example 4:
 *
 * Input: head = [1,3]
 * Output: [3,1]
 *
 * Constraints:
 *
 * The number of nodes in head is in the range [0, 2 * 104].
 * -10^5 <= Node.val <= 10^5
 */
/**
 * Solution using inorder traversal
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* sortedListToBST(ListNode* head) {
        int size = list_size(head);
        cur = head;

        return dfs(0, size - 1);
    }
private:
    int list_size(ListNode* head)
    {
        int c = 0;

        while (head) {
            ++c;
            head = head->next;
        }

        return c;
    }

    TreeNode* dfs(int l, int r)
    {
        if (l > r) {
            return nullptr;
        }

        int mid = (l + r) / 2;
        TreeNode* left = dfs(l, mid - 1);

        TreeNode* root = new TreeNode(cur->val);
        root->left = left;

        cur = cur->next;

        root->right = dfs(mid + 1, r);
        return root;
    }

    ListNode* cur;
};

/**
 * Solution without the array help
 */
class Solution {
public:
    TreeNode* sortedListToBST(ListNode* head) {
        if (!head) {
            return nullptr;
        }

        auto mid = find_middle(head);
        TreeNode* root = new TreeNode(mid->val);

        if (head == mid) {
            return root;
        }

        root->left = sortedListToBST(head);
        root->right = sortedListToBST(mid->next);

        return root;
    }
private:
    ListNode* find_middle(ListNode* head)
    {
        ListNode* prev = nullptr;
        ListNode* slow = head;
        ListNode* fast = head;

        while (fast && fast->next) {
            prev = slow;
            slow = slow->next;
            fast = fast->next->next;
        }

        if (prev) {
            prev->next = nullptr;
        }

        return slow;
    }
};

/**
 * Original solution.
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* sortedListToBST(ListNode* head) {
        vector<int> sortArr;
        ListNode* cur = head;
        while (cur) {
            sortArr.push_back(cur->val);
            cur = cur->next;
        }

        return dfs(sortArr, 0, sortArr.size() - 1);
    }

private:
    TreeNode* dfs(const vector<int>& tree, int left, int right)
    {
        if (left <= right) {
            int mid = (left + right) / 2;
            TreeNode* node = new TreeNode(tree[mid]);
            node->left = dfs(tree, left, mid - 1);
            node->right = dfs(tree, mid + 1, right);
            return node;
        }

        return nullptr;
    }
};
