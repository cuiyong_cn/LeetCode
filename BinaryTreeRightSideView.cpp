/**
 * Binary Tree Right Side View
 *
 * Given a binary tree, imagine yourself standing on the right side of it, return the values of the nodes you can see
 * ordered from top to bottom.
 *
 * Example:
 *
 * Input: [1,2,3,null,5,null,4]
 * Output: [1, 3, 4]
 * Explanation:
 *
 *    1            <---
 *  /   \
 * 2     3         <---
 *  \     \
 *   5     4       <---
 */
/**
 * Ok, here is non-recursive version. This non-recursive version basically is exactly like the recursive version.
 *
 * After thinking about it, I can use the BFS to expand tree at every level, and take the last one as the rightmost one.
 * BFS's solution is much cleaner I think.
 */
class Solution {
public:
    vector<int> rightSideView(TreeNode* root) {
        vector<int> ans;
        stack<pair<TreeNode*, int>> stk;
        TreeNode* cur = root;
        int height = 0;
        int maxHeight = 0;
        while (false == stk.empty() || nullptr != cur) {
            while (nullptr != cur) {
                ++height;
                if (height > maxHeight) {
                    ans.push_back(cur->val);
                    maxHeight = height;
                }
                stk.push({cur, height});
                cur = cur->right;
            }

            while (false == stk.empty()) {
                if (stk.top().first->left) {
                    cur = stk.top().first->left;
                    height = stk.top().second;
                    stk.pop();
                    break;
                }
                stk.pop();
            }
        }

        return ans;
    }
};

/**
 * First, I tried the non-recursive version but failed. (a lot)
 * So I wrote the recursive version first.
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int maxHeight;
    vector<int> ans;

    void dfs(TreeNode* node, int height) {
        if (nullptr == node) return;

        int newHeight = height + 1;
        if (newHeight > maxHeight) {
            maxHeight = newHeight;
            ans.push_back(node->val);
        }

        dfs(node->right, newHeight);
        dfs(node->left, newHeight);
    }

    vector<int> rightSideView(TreeNode* root) {
        maxHeight = 0;
        dfs(root, 0);
        return ans;
    }
};
