/**
 * Matrix Cells in Distance Order
 *
 * We are given a matrix with R rows and C columns has cells with integer
 * coordinates (r, c), where 0 <= r < R and 0 <= c < C.
 *
 * Additionally, we are given a cell in that matrix with coordinates (r0, c0).
 *
 * Return the coordinates of all cells in the matrix, sorted by their distance
 * from (r0, c0) from smallest distance to largest distance.  Here, the distance
 * between two cells (r1, c1) and (r2, c2) is the Manhattan distance,
 * |r1 - r2| + |c1 - c2|.  (You may return the answer in any order that
 * satisfies this condition.)
 *
 * Example 1:
 *
 * Input: R = 1, C = 2, r0 = 0, c0 = 0
 * Output: [[0,0],[0,1]]
 * Explanation: The distances from (r0, c0) to other cells are: [0,1]
 *
 * Example 2:
 *
 * Input: R = 2, C = 2, r0 = 0, c0 = 1
 * Output: [[0,1],[0,0],[1,1],[1,0]]
 * Explanation: The distances from (r0, c0) to other cells are: [0,1,1,2]
 * The answer [[0,1],[1,1],[0,0],[1,0]] would also be accepted as correct.
 *
 * Example 3:
 *
 * Input: R = 2, C = 3, r0 = 1, c0 = 2
 * Output: [[1,2],[0,2],[1,1],[0,1],[1,0],[0,0]]
 * Explanation: The distances from (r0, c0) to other cells are: [0,1,1,2,2,3]
 * There are other answers that would also be accepted as correct, such as [[1,2],[1,1],[0,2],[1,0],[0,1],[0,0]].
 *
 * Note:
 *
 *     1 <= R <= 100
 *     1 <= C <= 100
 *     0 <= r0 < R
 *     0 <= c0 < C
 */
/**
 * My original version.
 */
class Solution
{
public:
    vector<vector<int>> allCellsDistOrder(int R, int C, int r0, int c0)
    {
        map<int, vector<vector<int>> > mDistCells;
        for (int i = 0; i < R; ++i) {
            for (int j = 0; j < C; ++j) {
                int dist = abs(i - r0) + abs(j - c0);
                mDistCells[dist].push_back({i, j});
            }
        }
        vector<vector<int>> ans;
        for (auto& it : mDistCells) {
            ans.insert(ans.end(), it.second.begin(), it.second.end());
        }
        return ans;
    }
};
/**
 * Consider another one.
 */
class Solution
{
public:
    vector<vector<int>> allCellsDistOrder(int R, int C, int r0, int c0)
    {
        vector<vector<int>> res = { { r0, c0 } };
        auto max_d = max({ r0 + c0, c0 + R - r0, r0 + C - c0, R - r0 + C - c0 });
        for (auto d = 1; d <= max_d; ++d) {
            /* here we can replace it with, clockwise/counterclockwise walkthrough */
            for (int x = d; x >= -d; --x) {
                auto r1 = r0 + x, c1_a = c0 + d - abs(x), c1_b = c0 + abs(x) - d;
                if (r1 >= 0 && r1 < R) {
                    if (c1_a >= 0 && c1_a < C) res.push_back({ r1, c1_a });
                    if (c1_a != c1_b && c1_b >= 0 && c1_b < C) res.push_back({ r1, c1_b });
                }
            }
        }
        return res;
    }
}

/**
 * Same as previous one. But using clockwise walkthrough, this one is more efficient
 */
class Solution
{
public:
    vector<vector<int>> allCellsDistOrder(int R, int C, int r0, int c0)
    {
        vector<vector<int>> res = { { r0, c0 } };
        auto max_d = max({ r0 + c0, c0 + R - 1 - r0, r0 + C - 1 - c0, R - r0 + C - c0 - 2});
        const vector<pair<int, int>> dir{{1, 1}, {1, -1}, {-1, -1}, {-1, 1}};
        for (auto d = 1; d <= max_d; ++d) {
            int y = r0 - d;
            int x = c0;
            for (int i = 0; i < dir.size(); ++i) {
                int dy = dir[i].first, dx = dir[i].second;
                for (int j = 0; j < d; ++j) {
                    x += dx, y += dy;
                    if (x >= 0 && x < C && y >= 0 && y < R) {
                        res.push_back({y, x});
                    }
                }
            }
        }

        return res;
    }
};
