/**
 * Next Greater Node In Linked List
 *
 * We are given a linked list with head as the first node.  Let's number the nodes in the list: node_1, node_2, node_3,
 * ... etc.
 *
 * Each node may have a next larger value: for node_i, next_larger(node_i) is the node_j.val such that j > i,
 * node_j.val > node_i.val, and j is the smallest possible choice.
 * If such a j does not exist, the next larger value is 0.
 *
 * Return an array of integers answer, where answer[i] = next_larger(node_{i+1}).
 *
 * Note that in the example inputs (not outputs) below, arrays such as [2,1,5] represent the serialization of a linked
 * list with a head node value of 2, second node value of 1, and third node value of 5.
 *
 * Example 1:
 *
 * Input: [2,1,5]
 * Output: [5,5,0]
 *
 * Example 2:
 *
 * Input: [2,7,4,3,5]
 * Output: [7,0,5,5,0]
 *
 * Example 3:
 *
 * Input: [1,7,5,1,9,2,5,1]
 * Output: [7,9,9,9,0,5,0,0]
 *
 * Note:
 *     1 <= node.val <= 10^9 for each node in the linked list.
 *     The given list has length in the range [0, 10000].
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
/**
 * Same idea using stack, but processing from right to left. More intuitive.
 */
class Solution {
public:
    vector<int> nextLargerNodes(ListNode* h) {
        vector<int> res, stack;
        for (auto p = h; p != nullptr; p = p->next) res.push_back(p->val);
        for (int i = res.size() - 1; i >= 0; --i) {
            auto val = res[i];
            while (!stack.empty() && stack.back() <= res[i]) stack.pop_back();
            res[i] = stack.empty() ? 0 : stack.back();
            stack.push_back(val);
        }
        return res;
    }
};
/**
 * More efficient version.
 */
class Solution {
public:
    vector<int> nextLargerNodes(ListNode* head) {
        vector<int> res;
        stack<int> stack;
        for (ListNode* node = head; node; node = node->next) {
            while (stack.size() && res[stack.top()] < node->val) {
                res[stack.top()] = node->val;
                stack.pop();
            }
            stack.push(res.size());
            res.push_back(node->val);
        }

        while (false == stack.empty()) {
            res[stack.top()] = 0;
            stack.pop();
        }

        return res;
    }
};

/**
 * Original solution. brute force.
 */
class Solution {
public:
    vector<int> nextLargerNodes(ListNode* head) {
        ListNode* next = head;
        vector<int> ans;
        while (next != nullptr) {
            ListNode* tmp = next->next;
            ans.push_back(nextLarger(next));
            next = tmp;
        }
        return ans;
    }
private:
    int nextLarger(ListNode* i) {
        ListNode* j = i->next;
        int val = 0;
        while (j != nullptr) {
            if (j->val > i->val) {
                val = j->val;
                break;
            }
            j = j->next;
        }
        return val;
    }
};
