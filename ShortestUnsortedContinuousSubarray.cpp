/**
 * Shortest Unsorted Continuous Subarray
 *
 * Given an integer array nums, you need to find one continuous subarray that if you only sort this
 * subarray in ascending order, then the whole array will be sorted in ascending order.
 *
 * Return the shortest such subarray and output its length.
 *
 * Example 1:
 *
 * Input: nums = [2,6,4,8,10,9,15]
 * Output: 5
 * Explanation: You need to sort [6, 4, 8, 10, 9] in ascending order to make the whole array sorted
 * in ascending order.
 * Example 2:
 *
 * Input: nums = [1,2,3,4]
 * Output: 0
 * Example 3:
 *
 * Input: nums = [1]
 * Output: 0
 *
 * Constraints:
 *
 * 1 <= nums.length <= 104
 * -105 <= nums[i] <= 105
 *
 * Follow up: Can you solve it in O(n) time complexity?
 */
/**
 * Stack solution. This one comes up to mind after the original solution.
 * Optimization:
 *      We can do this without the stack.
 *
 *      1. Find first unsorted element from left. Then continue to traversal the array and find the
 *      minimum element value.
 *      2. Find first unsorted element from right. Then continue to traversal the array and find
 *      the maximum element value.
 *      3. scan the array from left to the right, find first element that greater than minimum.
 *      4. scan the array from right to left, find the first element that less than maximum.
 */
class Solution {
public:
    int findUnsortedSubarray(vector<int>& nums) {
        vector<int> stk;
        int left = nums.size();
        int right = 0;
        for (int i = 0; i < nums.size(); ++i) {
            while (!stk.empty() && nums[i] < nums[stk.back()]) {
                left = min(left, stk.back());
                stk.pop_back();
            }
            stk.push_back(i);
        }

        stk.clear();
        for (int i = nums.size() - 1; i >= 0; --i) {
            while (!stk.empty() && nums[i] > nums[stk.back()]) {
                right = max(right, stk.back());
                stk.pop_back();
            }
            stk.push_back(i);
        }

        return (right - left) > 0 ? right - left + 1 : 0;
    }
};

/**
 * Sorting solution. Pretty straightforward.
 */
class Solution {
public:
    int findUnsortedSubarray(vector<int>& nums) {
        auto clone = nums;
        sort(clone.begin(), clone.end());
        int left = nums.size();
        int right = 0;
        for (int i = 0; i < nums.size(); ++i) {
            if (clone[i] != nums[i]) {
                left = min(left, i);
                right = max(right, i);
            }
        }

        return (right - left) >= 0 ? right - left + 1 : 0;
    }
};

/**
 * Original solution. Idea is:
 * Find the first unsorted element from left, and first unsorted element from right.
 * Sort the elements between left and right.
 * Then repeat this process. If all elements are sorted.
 * Answer will be right - left.
 *
 * Thought this answer passed the test, but it sucks. As it handles serveral edge cases, which
 * I test it serveral time before finanlly got accepted.
 */
class Solution {
public:
    int findUnsortedSubarray(vector<int>& nums) {
        int left = nums.size();
        int right = -1;

        while (true) {
            int i = 1;
            for (; i < nums.size(); ++i) {
                if (nums[i] < nums[i - 1]) {
                    left = i <= left ? i - 1 : left;
                    break;
                }
            }

            if (i == nums.size()) {
                if (nums.size() == left) {
                    left = -1;
                }
                break;
            }

            int j = nums.size() - 2;
            for (; j >= 0; --j) {
                if (nums[j] > nums[j + 1]) {
                    right = j >= (right - 2) ? j + 2 : right;
                    break;
                }
            }

            if (j < 0) {
                break;
            }

            if (left < right) {
                sort(nums.begin() + left, nums.begin() + right);
            }
        }

        return right - left;
    }
};
