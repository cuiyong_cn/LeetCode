/**
 * Missing Number
 *
 * Given an array containing n distinct numbers taken from 0, 1, 2, ..., n, find the one that is missing from the array.
 *
 * Example 1:
 *
 * Input: [3,0,1]
 * Output: 2
 *
 * Example 2:
 *
 * Input: [9,6,4,2,3,5,7,0,1]
 * Output: 8
 *
 * Note:
 * Your algorithm should run in linear runtime complexity. Could you implement it using only constant extra space
 * complexity?
 */
/**
 * Also we can use other approaches. like sorting (bucket sort or fast sort), hash map.
 * But there is another interesting solution.
 */
class Solution {
public:
    int missingNumber(vector<int>& nums) {
        int num = nums.size();
        for (size_t i = 0; i < nums.size(); ++i) {
            num ^= i ^ nums[i];
        }

        return num;
    }
};

/**
 * To avoid overflow.
 */
class Solution {
public:
    int missingNumber(vector<int>& nums) {
        int num = 0;
        for (size_t i = 0; i < nums.size(); ++i) {
            num += i + 1 - nums[i];
        }

        return num;
    }
};

/**
 * Original solution. Though this solution survived the test, it has a minor flaw.
 * What if the sum of the numbers overflow?
 */
class Solution {
public:
    int missingNumber(vector<int>& nums) {
        int total = nums.size() * (nums.size() + 1) / 2;
        int sum = std::accumulate(nums.begin(), nums.end(), 0);

        return total - sum;
    }
};
