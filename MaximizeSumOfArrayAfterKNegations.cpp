/**
 * Maximize Sum Of Array After K Negations
 *
 * Given an array A of integers, we must modify the array in the following way: we choose an i and replace A[i] with
 * -A[i], and we repeat this process K times in total.  (We may choose the same index i multiple times.)
 *
 * Return the largest possible sum of the array after modifying it in this way.
 *
 * Example 1:
 *
 * Input: A = [4,2,3], K = 1
 * Output: 5
 * Explanation: Choose indices (1,) and A becomes [4,-2,3].
 *
 * Example 2:
 *
 * Input: A = [3,-1,0,2], K = 3
 * Output: 6
 * Explanation: Choose indices (1, 2, 2) and A becomes [3,1,0,2].
 *
 * Example 3:
 *
 * Input: A = [2,-3,-1,5,-4], K = 2
 * Output: 13
 * Explanation: Choose indices (1, 4) and A becomes [2,3,-1,5,4].
 *
 * Note:
 *     1 <= A.length <= 10000
 *     1 <= K <= 10000
 *     -100 <= A[i] <= 100
 */
/**
 * Improved one.
 */
class Solution {
public:
    int largestSumAfterKNegations(vector<int>& A, int K) {
        sort(A.begin(), A.end());

        auto it = find_if(A.begin(), A.end(), [](auto n) { return n > 0; });
        int negNums = distance(A.begin(), it);
        auto flipNums = min(K, negNums);
        if (flipNums > 0) {
            transform(A.begin(), next(A.begin(), flipNums), A.begin(), [](auto n) { return -n; });
            K -= flipNums;
        }

        int sum = accumulate(A.begin(), A.end(), 0);
        if (K % 2) {
            int minNum = *min_element(A.begin(), A.end());
            sum += -2 * minNum;
        }

        return sum;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int largestSumAfterKNegations(vector<int>& A, int K) {
        sort(A.begin(), A.end());

        for (auto& n : A) {
            if (n < 0) {
                if (K > 0) {
                    n = -n;
                    --K;
                }
                else {
                    break;
                }
            }
            else {
                break;
            }
        }

        if (K > 0) {
            sort(A.begin(), A.end());
        }

        int sum = accumulate(A.begin(), A.end(), 0);
        if (K % 2) {
            sum += -A[0] * 2;
        }

        return sum;
    }
};
