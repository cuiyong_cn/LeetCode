/**
 * Count Submatrices With All Ones
 *
 * Given an m x n binary matrix mat, return the number of submatrices that have all ones.
 *
 * Example 1:
 *
 * Input: mat = [[1,0,1],[1,1,0],[1,1,0]]
 * Output: 13
 * Explanation:
 * There are 6 rectangles of side 1x1.
 * There are 2 rectangles of side 1x2.
 * There are 3 rectangles of side 2x1.
 * There is 1 rectangle of side 2x2.
 * There is 1 rectangle of side 3x1.
 * Total number of rectangles = 6 + 2 + 3 + 1 + 1 = 13.
 * Example 2:
 *
 * Input: mat = [[0,1,1,0],[0,1,1,1],[1,1,1,0]]
 * Output: 24
 * Explanation:
 * There are 8 rectangles of side 1x1.
 * There are 5 rectangles of side 1x2.
 * There are 2 rectangles of side 1x3.
 * There are 4 rectangles of side 2x1.
 * There are 2 rectangles of side 2x2.
 * There are 2 rectangles of side 3x1.
 * There is 1 rectangle of side 3x2.
 * Total number of rectangles = 8 + 5 + 2 + 4 + 2 + 2 + 1 = 24.
 *
 * Constraints:
 *
 * 1 <= m, n <= 150
 * mat[i][j] is either 0 or 1.
 */
/**
 * Further optimize below solution.
 */
class Solution {
public:
    int numSubmat(vector<vector<int>>& mat)
    {
        int const M = mat.size();
        int const N = mat[0].size();
        sum = vector<int>(N, 0);
        int res = 0;

        vector<int> h(N, 0);
        for (int i = 0; i < M; ++i) {
            for (int j = 0; j < N; ++j) {
                h[j] = mat[i][j] == 0 ? 0 : h[j] + 1;
            }
            res += helper(h);
        }

        return res;
    }

    int helper(vector<int> const& A)
    {
        int const N = A.size();
        fill(sum.begin(), sum.end(), 0);
        stack<int> stack;

        for (int i = 0; i < N; ++i) {
            while (!stack.empty() && A[stack.top()] >= A[i]) {
                stack.pop();
            }

            if (!stack.empty()) {
                auto preIndex = stack.top();
                sum[i] = sum[preIndex];
                sum[i] += A[i] * (i - preIndex);
            } else {
                sum[i] = A[i] * (i + 1);
            }

            stack.push(i);
        }

        return accumulate(sum.begin(), sum.end(), 0);
    }

private:
    vector<int> sum;
};

/**
 * More efficient one. Idea behind this is:
 *
 * Imagine you have an one-dimension array, how to count number of all 1 submatrices (size is 1 *
 * X). It's pretty simple right?
 * 
 * int res = 0, length = 0;
 * for (int i = 0; i < A.length; ++i) {
 * 	   length = (A[i] == 0 ? 0 : length + 1);
 * 	   res += length;
 * }
 * return res;
 *
 * Now, Let's solve 2D matrix by finding all 1 submatrices from row "up" to row "down". And apply
 * above 1D helper function. Note: the array h[k] == 1 means all values in column k from row "up" to
 * "down" are 1 (that's why we use &). So overall, the idea is to "compress" the 2D array to the 1D
 * array, and apply 1D array method on it, while trying all heights up to down.
 */
class Solution {
public:
    int countOneRow(vector<int> const& v)
    {
        int length = 0;
        int ans = 0;
        for (auto n : v) {
            length = 0 == n ? 0 : length + 1;
            ans += length;
        }

        return ans;
    }

    int numSubmat(vector<vector<int>>& mat) {
        int const M = mat.size();
        int const N = mat[0].size();
        int sum = 0;

        vector<int> v(N,1);
        for (int i = 0; i < M; ++i) {
            fill(v.begin(), v.end(), 1);
            for (int j = i; j < M; ++j) {
                for(int k = 0; k < N; ++k) {
                    v[k] &= mat[j][k];
                }

                sum += countOneRow(v);
            }
        }

        return sum;
    }
};

/**
 * Brute solution from discussion.
 */
class Solution {
public:
    // count all submatrices with top-left corner at mat[a][b]
    int helper(vector<vector<int>> const& mat, int a, int b)
    {
        int const M = mat.size();
        int const N = mat[0].size();
        int count = 0;
        int bound = N;

        for (int i = a; i < M; ++i) {
            for (int j = b; j < bound; ++j) {
                if (mat[i][j]) {
                    ++count;
                }
                else {
                    bound = j;
                }
            }
        }
        
        return count;
    }

    int numSubmat(vector<vector<int>>& mat) {
        int const M = mat.size();
        int const N = mat[0].size();
        int count = 0;
        
        for (int i = 0; i < M; ++i) {
            for (int j = 0; j < N; ++j) {
                count += helper(mat, i, j);
            }
        }
        
        return count;
    }
};
