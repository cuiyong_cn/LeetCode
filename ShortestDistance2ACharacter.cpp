/**
 * Shortest Distance to a Character
 *
 * Given a string S and a character C, return an array of integers representing
 * the shortest distance from the character C in the string.
 *
 * Example 1:
 *
 * Input: S = "loveleetcode", C = 'e'
 * Output: [3, 2, 1, 0, 1, 0, 0, 1, 2, 2, 1, 0]
 *
 * Note:
 *
 *     S string length is in [1, 10000].
 *     C is a single character, and guaranteed to be in string S.
 *     All letters in S and C are lowercase.
 */
/**
 * More clear one.
 */
class Solution {
    vector<int> shortestToChar(string S, char C) {
        int n = S.size();
        vector<int> res(n, n);
        int pos = -n;
        for (int i = 0; i < n; ++i) {
            if (S[i] == C) pos = i;
            res[i] = min(res[i], abs(i - pos));
        }
        for (int i = n - 1; i >= 0; --i) {
            if (S[i] == C)  pos = i;
            res[i] = min(res[i], abs(i - pos));
        }
        return res;
    }
};
/**
 * My original version, quite chatty. not a good one
 */
class Solution {
public:
    vector<int> shortestToChar(string S, char C) {
        int start = 0;
        int end;
        end = S.find(C, start);
        vector<int> ans(S.size(), 0);
        if (end == 0) {
            end = S.find(C, 1);
        }
        else {
            for (int i = 0; i < end; ++i) {
                ans[i] = end - i;
            }
            start = end;
            end = S.find(C, start + 1);
        }
        while (end != string::npos) {
            for (int i = start; i <= end; ++i) {
                ans[i] = min(i - start, end - i);
            }
            start = end;
            end = S.find(C, start + 1);
        }
        for (int i = start; i < S.size(); ++i) {
            ans[i] = i - start;
        }
        return ans;
    }
};
