/**
 * Longest Palindromic Subsequence
 *
 * Given a string s, find the longest palindromic subsequence's length in s. You may assume that the maximum length of s
 * is 1000.
 *
 * Example 1:
 * Input: "bbbab"
 *
 * Output: 4
 *
 * One possible longest palindromic subsequence is "bbbb".
 *
 * Example 2:
 * Input: "cbbd"
 * Output: 2
 *
 * One possible longest palindromic subsequence is "bb".
 */
/**
 * DP solution.
 */
class Solution {
public:
    int longestPalindromeSubseq(string s) {
        if (s.empty()) return 0;

        vector<vector<int>> dp(s.length(), vector<int>(s.length(), 0));

        for (int i = s.length() - 1; i >= 0; i--) {
            dp[i][i] = 1;
            for (int j = i+1; j < s.length(); j++) {
                if (s[i] == s[j]) {
                    dp[i][j] = dp[i+1][j-1] + 2;
                } else {
                    dp[i][j] = max(dp[i+1][j], dp[i][j-1]);
                }
            }
        }
        return dp[0][s.length()-1];
    }
};

/**
 * original solution.
 */
class Solution {
public:
    int longestPalindromeSubseq(string s) {
        if (s.size() > 0) {
            memo = vector<vector<int>>(s.size(), vector<int>(s.size(), 0));
        }

        return longest(s, 0, s.size() - 1);
    }

private:
    int longest(string& s, int start, int end) {
        if (start > end) return 0;
        if (start == end) return 1;

        if (memo[start][end] > 0) return memo[start][end];

        int len = 0;
        if (s[start] == s[end]) {
            len = 2 + longest(s, start + 1, end - 1);
        }
        else {
            len = max(longest(s, start, end - 1), longest(s, start + 1, end));
        }
        memo[start][end] = len;

        return len;
    }

    vector<vector<int>> memo;
};
