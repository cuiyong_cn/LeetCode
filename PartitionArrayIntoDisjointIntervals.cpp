/**
 * Partition Array into Disjoint Intervals
 *
 * Given an array A, partition it into two (contiguous) subarrays left and right so that:
 *
 * Every element in left is less than or equal to every element in right.  left and right are non-empty.  left has the
 * smallest possible size.  Return the length of left after such a partitioning.  It is guaranteed that such a
 * partitioning exists.
 *
 * Example 1:
 *
 * Input: [5,0,3,8,6]
 * Output: 3
 * Explanation: left = [5,0,3], right = [8,6]
 * Example 2:
 *
 * Input: [1,1,1,0,6,12]
 * Output: 4
 * Explanation: left = [1,1,1,0], right = [6,12]
 *
 * Note:
 *
 * 2 <= A.length <= 30000
 * 0 <= A[i] <= 10^6
 * It is guaranteed there is at least one way to partition A as described.
 */
/**
 * Improve the code.
 */
class Solution {
public:
    int partitionDisjoint(vector<int>& A) {
        int max_in_left = A[0];
        int max_so_far = A[0];
        int partition_point = 0;

        for (int i = 1; i < A.size(); ++i) {
            if (A[i] < max_in_left) {
                max_in_left = max_so_far;
                partition_point = i;
            }
            else {
                max_so_far = std::max(max_so_far, A[i]);
            }
        }

        return partition_point + 1;
    }
};

/**
 * Original solution. The idea is:
 *  The max number in left should be less than or equal to min number in right
 */
class Solution {
public:
    int partitionDisjoint(vector<int>& A) {
        vector<vector<int>> dp(2, vector<int>(A.size(), 0));

        int cur_max = -1;
        for (int i = 0; i < A.size(); ++i) {
            cur_max = std::max(A[i], cur_max);
            dp[0][i] = cur_max;
        }

        int cur_min = numeric_limits<int>::max();
        for (int i = A.size() - 1; i >= 0; --i) {
            dp[1][i] = cur_min;
            cur_min = std::min(A[i], cur_min);
        }

        for (int i = 0; i < A.size(); ++i) {
            if (dp[0][i] <= dp[1][i]) {
                return i + 1;
            }
        }

        return A.size();
    }
};
