/**
 * Find the Maximum Length of Valid Subsequence I
 *
 * You are given an integer array nums.
 *
 * A subsequence sub of nums with length x is called valid if it satisfies:
 *     (sub[0] + sub[1]) % 2 == (sub[1] + sub[2]) % 2 == ... == (sub[x - 2] + sub[x - 1]) % 2.
 *
 * Return the length of the longest valid subsequence of nums.
 *
 * A subsequence is an array that can be derived from another array by deleting some or no elements
 * without changing the order of the remaining elements.
 *
 * Example 1:
 *
 * Input: nums = [1,2,3,4]
 *
 * Output: 4
 *
 * Explanation:
 *
 * The longest valid subsequence is [1, 2, 3, 4].
 *
 * Example 2:
 *
 * Input: nums = [1,2,1,1,2,1,2]
 *
 * Output: 6
 *
 * Explanation:
 *
 * The longest valid subsequence is [1, 2, 1, 2, 1, 2].
 *
 * Example 3:
 *
 * Input: nums = [1,3]
 *
 * Output: 2
 *
 * Explanation:
 *
 * The longest valid subsequence is [1, 3].
 *
 * Constraints:
 *     2 <= nums.length <= 2 * 105
 *     1 <= nums[i] <= 107
 */
/**
 * One Pass.
 */
class Solution {
public:
    int maximumLength(vector<int>& nums) {
        auto parity_cnt = array<int, 2>{0, 0};
        auto alt_cnt = array<int, 2>{0, 0};

        for (auto n : nums) {
            auto p = n & 1;

            ++parity_cnt[p];
            alt_cnt[p] = alt_cnt[1 - p] + 1;
        }

        return max({parity_cnt[0], parity_cnt[1], alt_cnt[0], alt_cnt[1]});
    }
};

/**
 * Original solution.
 */
bool odd(int n)
{
    return n & 1;
}

class Solution {
public:
    int maximumLength(vector<int>& nums) {
        auto p1 = std::ranges::count_if(nums, odd);
        auto p2 = static_cast<long>(nums.size() - p1);
        auto alternate_start_with = [&nums](bool expected) {
            auto p = 0L;

            for (auto n : nums) {
                if (expected == odd(n)) {
                    p = p + 1;
                    expected = !expected;
                }
            }

            return p;
        };

        auto p3 = alternate_start_with(true);
        auto p4 = alternate_start_with(false);

        return max({p1, p2, p3, p4});
    }
};
