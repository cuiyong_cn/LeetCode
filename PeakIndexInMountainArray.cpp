/**
 * Peak Index in a Mountain Array
 *
 * Let's call an array A a mountain if the following properties hold:
 *
 *     A.length >= 3
 *     There exists some 0 < i < A.length - 1 such that
 *     A[0] < A[1] < ... A[i-1] < A[i] > A[i+1] > ... > A[A.length - 1]
 *
 * Given an array that is definitely a mountain, return any i such that
 * A[0] < A[1] < ... A[i-1] < A[i] > A[i+1] > ... > A[A.length - 1].
 *
 * Example 1:
 *
 * Input: [0,1,0]
 * Output: 1
 *
 * Example 2:
 *
 * Input: [0,2,1,0]
 * Output: 1
 *
 * Note:
 *
 *  1.  3 <= A.length <= 10000
 *  2.  0 <= A[i] <= 10^6
 *  3.  A is a mountain, as defined above.
 *
 */
/**
 * Binary search version, there also exists a golden-section search algorithm, but
 * I don't think it is better than BS. Cause the average is about the same.
 *
 * class Solution {
 * public:
 *     int peakIndexInMountainArray(vector<int>& A) {
 *         int l = 0, r = A.size() - 1, mid;
 *         while (l < r) {
 *             mid = (l + r) / 2;
 *             if (A[mid] < A[mid + 1])
 *                 l = mid + 1;
 *             else
 *                 r = mid;
 *         }
 *         return l;
 *     }
 * };
 */
/**
 * one line version.
 */
class Solution {
public:
    int peakIndexInMountainArray(vector<int>& A) {
       return max_element(A.begin(), A.end()) - A.begin();
    }

    /* same as the above. *//*
    int peakIndexInMountainArray2(vector<int> A) {
        for (int i = 1; i + 1 < A.size(); ++i) if (A[i] > A[i + 1]) return i;
    }
     */
};
