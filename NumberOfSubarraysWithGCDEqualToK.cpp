/**
 * Number of Subarrays With GCD Equal to K
 *
 * Given an integer array nums and an integer k, return the number of subarrays of nums where the
 * greatest common divisor of the subarray's elements is k.
 *
 * A subarray is a contiguous non-empty sequence of elements within an array.
 *
 * The greatest common divisor of an array is the largest integer that evenly divides all the array elements.
 *
 * Example 1:
 *
 * Input: nums = [9,3,1,2,6,3], k = 3
 * Output: 4
 * Explanation: The subarrays of nums where 3 is the greatest common divisor of all the subarray's elements are:
 * - [9,3,1,2,6,3]
 * - [9,3,1,2,6,3]
 * - [9,3,1,2,6,3]
 * - [9,3,1,2,6,3]
 * Example 2:
 *
 * Input: nums = [4], k = 7
 * Output: 0
 * Explanation: There are no subarrays of nums where 7 is the greatest common divisor of all the
 * subarray's elements.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 1000
 * 1 <= nums[i], k <= 109
 */
/**
 * Passed one.
 */
class Solution {
public:
    int subarrayGCD(vector<int>& nums, int k) {
        auto ans = 0;
        auto nsize = static_cast<int>(nums.size());
        for (auto i = 0; i < nsize; ++i) {
            for (auto j = i; j < nsize && 0 == (nums[j] % k); ++j) {
                nums[i] = gcd(nums[i], nums[j]);
                ans += k == nums[i];
            }
        }
        return ans;
    }
};

/**
 * Original solution. TLE.
 */
int gcd(vector<int>& nums, int s, int e)
{
    auto g = nums[s];
    for (auto i = s; i < e; ++i) {
        for (auto j = i + 1; j < e; ++j) {
            g = min(g, gcd(nums[i], nums[j]));
        }
    }
    return g;
}

class Solution {
public:
    int subarrayGCD(vector<int>& nums, int k) {
        auto ans = 0;
        auto nsize = static_cast<int>(nums.size());
        for (auto i = 0; i < nsize; ++i) {
            if (0 != (nums[i] % k)) {
                continue;
            }
            ans += nums[i] == k;
            for (auto L = 2; (i + L) <= nsize; ++L) {
                auto is_sat = gcd(nums, i, i + L) == k;
                ans += is_sat;
            }
        }
        return ans;
    }
};
