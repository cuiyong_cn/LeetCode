/**
 * Find the Minimum and Maximum Number of Nodes Between Critical Points
 *
 * A critical point in a linked list is defined as either a local maxima or a local minima.
 *
 * A node is a local maxima if the current node has a value strictly greater than the previous node
 * and the next node.
 *
 * A node is a local minima if the current node has a value strictly smaller than the previous node
 * and the next node.
 *
 * Note that a node can only be a local maxima/minima if there exists both a previous node and a next node.
 *
 * Given a linked list head, return an array of length 2 containing [minDistance, maxDistance] where
 * minDistance is the minimum distance between any two distinct critical points and maxDistance is
 * the maximum distance between any two distinct critical points. If there are fewer than two
 * critical points, return [-1, -1].
 *
 * Example 1:
 *
 * Input: head = [3,1]
 * Output: [-1,-1]
 * Explanation: There are no critical points in [3,1].
 * Example 2:
 *
 * Input: head = [5,3,1,2,5,1,2]
 * Output: [1,3]
 * Explanation: There are three critical points:
 * - [5,3,1,2,5,1,2]: The third node is a local minima because 1 is less than 3 and 2.
 * - [5,3,1,2,5,1,2]: The fifth node is a local maxima because 5 is greater than 2 and 1.
 * - [5,3,1,2,5,1,2]: The sixth node is a local minima because 1 is less than 5 and 2.
 * The minimum distance is between the fifth and the sixth node. minDistance = 6 - 5 = 1.
 * The maximum distance is between the third and the sixth node. maxDistance = 6 - 3 = 3.
 * Example 3:
 *
 * Input: head = [1,3,2,2,3,2,2,2,7]
 * Output: [3,3]
 * Explanation: There are two critical points:
 * - [1,3,2,2,3,2,2,2,7]: The second node is a local maxima because 3 is greater than 1 and 2.
 * - [1,3,2,2,3,2,2,2,7]: The fifth node is a local maxima because 3 is greater than 2 and 2.
 * Both the minimum and maximum distances are between the second and the fifth node.
 * Thus, minDistance and maxDistance is 5 - 2 = 3.
 * Note that the last node is not considered a local maxima because it does not have a next node.
 *
 * Constraints:
 *
 * The number of nodes in the list is in the range [2, 105].
 * 1 <= Node.val <= 105
 */
/**
 * Original solution.
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
bool is_critical(ListNode* prev, ListNode* cur, ListNode* next)
{
    if (nullptr == next) {
        return false;
    }

    return (cur->val > prev->val && cur->val > next->val)
        || (cur->val < prev->val && cur->val < next->val);
}

class Solution {
public:
    vector<int> nodesBetweenCriticalPoints(ListNode* head) {
        int const value_not_set = -1;
        int min_distance = numeric_limits<int>::max();
        int max_distance = value_not_set;
        int index = 1;
        int left_most_index = value_not_set;
        int prev_index = left_most_index;

        ListNode* prev = head;
        ListNode* cur = head->next;
        while (nullptr != cur) {
            auto next = cur->next;
            if (is_critical(prev, cur, next)) {
                if (value_not_set == left_most_index) {
                    left_most_index = index;
                }

                if (value_not_set != prev_index) {
                    min_distance = min(min_distance, index - prev_index);
                    max_distance = max(max_distance, index - left_most_index);
                }

                prev_index = index;
            }

            prev = cur;
            cur = next;
            ++index;
        }

        min_distance = numeric_limits<int>::max() == min_distance ? value_not_set : min_distance;

        return {min_distance, max_distance};
    }
};
