/**
 * Binary Watch
 *
 * A binary watch has 4 LEDs on the top which represent the hours (0-11), and the 6 LEDs on the bottom represent the
 * minutes (0-59).
 *
 * Each LED represents a zero or one, with the least significant bit on the right.
 *
 * For example, the above binary watch reads "3:25".
 *
 * Given a non-negative integer n which represents the number of LEDs that are currently on, return all possible times
 * the watch could represent.
 *
 * Example:
 *
 * Input: n = 1
 * Return: ["1:00", "2:00", "4:00", "8:00", "0:01", "0:02", "0:04", "0:08", "0:16", "0:32"]
 * Note:
 * The order of output does not matter.
 * The hour must not contain a leading zero, for example "01:00" is not valid, it should be "1:00".
 * The minute must be consist of two digits and may contain a leading zero, for example "10:2" is not valid, it should be "10:02".
 */
/**
 * Err.., really intresting solution from discussion.
 * Also there is a O(1) solution that list all the possible answers. Brute force.
 */
class Solution {
public:
    vector<string> readBinaryWatch(int num) {
        vector<string> rs;
        for (int h = 0; h < 12; h++) {
            for (int m = 0; m < 60; m++) {
                if (bitset<10>(h << 6 | m).count() == num) {
                    rs.emplace_back(to_string(h) + (m < 10 ? ":0" : ":") + to_string(m));
                }
            }
        }
        return rs;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    vector<string> readBinaryWatch(int num) {
        if (num < 0 || num > 8) {
            return {};
        }

        vector<string> ans;
        for (size_t i = 0; i <= num; ++i) {
            auto hours = combos<4>(i);
            if (hours.empty()) {
                continue;
            }
            auto minutes = combos<6>(num - i);
            if (minutes.empty()) {
                continue;
            }

            for (auto& h : hours) {
                for (auto& m : minutes) {
                    ans.push_back(h + ":" + m);
                }
            }
        }

        return ans;
    }

private:
    template<size_t N, bool hour = (N <= 4)>
    vector<string> combos(int led_num)
    {
        if (led_num < 0 || led_num > (N - 1)) {
            return {};
        }

        std::bitset<N> led_bits;
        vector<string> ans;

        combo_helper<N>(ans, led_bits, 0, led_num);
        return ans;
    }

    template<size_t N, bool hour = (N <= 4)>
    void combo_helper(vector<string>& combos, bitset<N>& led_bits, int bp, int num)
    {
        if (!num) {
            if constexpr (hour) {
                int h = led_bits.to_ulong();
                if (0 <= h && h < 12) {
                    combos.push_back(to_string(h));
                }
            }
            else {
                int minutes = led_bits.to_ulong();
                if (0 <= minutes && minutes < 60) {
                    stringstream ss;
                    ss << setw(2) << setfill('0') << minutes;
                    combos.push_back(ss.str());
                }
            }
            return;
        }

        for (size_t i = bp; i < N; ++i) {
            led_bits.set(i);
            combo_helper<N>(combos, led_bits, i + 1, num - 1);
            led_bits.set(i, false);
        }
    }
};
