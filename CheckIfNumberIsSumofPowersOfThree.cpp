/**
 * Check if Number is a Sum of Powers of Three
 *
 * Given an integer n, return true if it is possible to represent n as the sum of distinct powers of
 * three. Otherwise, return false.
 *
 * An integer y is a power of three if there exists an integer x such that y == 3x.
 *
 * Example 1:
 *
 * Input: n = 12
 * Output: true
 * Explanation: 12 = 31 + 32
 * Example 2:
 *
 * Input: n = 91
 * Output: true
 * Explanation: 91 = 30 + 32 + 34
 * Example 3:
 *
 * Input: n = 21
 * Output: false
 *
 * Constraints:
 *
 * 1 <= n <= 107
 */
/**
 * Original solution.
 * Think how we represent a number in ternary system.
 *
 *  9 8 7 6 5 4 3    2    1    0
 * 3^9   ......     3^2  3^1  3^0
 */
class Solution {
public:
    bool checkPowersOfThree(int n) {
        while (n > 0) {
            auto rem = n % 3;
            if (2 == rem) {
                return false;
            }

            n /= 3;
        }

        return true;
    }
};
