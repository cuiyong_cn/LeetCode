/**
 * Fizz Buzz
 *
 * Write a program that outputs the string representation of numbers from 1 to n.
 *
 * But for multiples of three it should output “Fizz” instead of the number and for the
 * multiples of five output “Buzz”. For numbers which are multiples of both three and
 * five output “FizzBuzz”.
 *
 * Example:
 *
 * n = 15,
 *
 * Return:
 * [
 *     "1",
 *     "2",
 *     "Fizz",
 *     "4",
 *     "Buzz",
 *     "Fizz",
 *     "7",
 *     "8",
 *     "Fizz",
 *     "Buzz",
 *     "11",
 *     "Fizz",
 *     "13",
 *     "14",
 *     "FizzBuzz"
 * ]
 */
/**
 * Intuitive but naive approach. But not extensible.
 */
class Solution {
public:
    vector<string> fizzBuzz(int n) {
        vector<string> ans(n);
        for (int i = 1; i <= n; ++i) {
            if (i % 3 == 0) {
                if (i % 5 == 0) {
                    ans[i - 1] = "FizzBuzz";
                }
                else {
                    ans[i - 1] = "Fizz";
                }
            }
            else if (i % 5 == 0) {
                ans[i - 1] = "Buzz";
            }
            else {
                ans[i - 1] = to_string(i);
            }
        }
        return ans;
    }
};

/**
 * Extensible but a little chatty.
 */
class Solution {
public:
    vector<string> fizzBuzz(int n) {
        vector<string> ans(n);
        for (int i = 1; i <= n; ++i) {
            string str;

            if (i % 3 == 0) {
                str += "Fizz";
            }
            if (i % 5 == 0) {
                str += "Buzz";
            }
            if (str == "") {
                str += to_string(i);
            }
            ans[i - 1] = str;
        }
        return ans;
    }
};

/**
 * Map approach
 */
class Solution {
public:
    vector<string> fizzBuzz(int n) {
        map<int, string> fbMap = { {3, "Fizz"}, {5, "Buzz"} };
        vector<string> ans(n);
        for (int i = 1; i <= n; ++i) {
            string str;
            for (auto& m : fbMap) {
                if (i % m.first == 0) {
                    str += m.second;
                }
            }
            if (str == "") {
                str += to_string(i);
            }
            ans[i - 1] = str;
        }
        return ans;
    }
};

/**
 * Like map but different
 */
class Solution {
public:
    vector<string> fizzBuzz(int n) {
        map<pair<int, int>, string> fbMap = { {{3, 3}, "Fizz"}, {{5, 5}, "Buzz"}, {{15, 15}, "FizzBuzz"} };
        vector<string> ans(n);
        for (int i = 1; i <= n; ++i) {
            ans[i - 1] = to_string(i);
        }

        for (auto& m : fbMap) {
            int start = m.first.first;
            int step = m.first.second;
            string str = m.second;
            for (int i = start; i <= n; i += step) {
                ans[i - 1] = str;
            }
        }
        return ans;
    }
};
