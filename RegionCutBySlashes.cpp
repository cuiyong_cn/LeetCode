/**
 * Regions Cut By Slashes
 *
 * In a N x N grid composed of 1 x 1 squares, each 1 x 1 square consists of a /, \,
 * or blank space.  These characters divide the square into contiguous regions.
 *
 * (Note that backslash characters are escaped, so a \ is represented as "\\".)
 *
 * Return the number of regions.
 *
 * Example 1:
 *
 * Input:
 * [
 *   " /",
 *   "/ "
 * ]
 * Output: 2
 * Explanation: The 2x2 grid is as follows:
 *
 * Example 2:
 *
 * Input:
 * [
 *   " /",
 *   "  "
 * ]
 * Output: 1
 * Explanation: The 2x2 grid is as follows:
 *
 * Example 3:
 *
 * Input:
 * [
 *   "\\/",
 *   "/\\"
 * ]
 * Output: 4
 * Explanation: (Recall that because \ characters are escaped, "\\/" refers to \/, and "/\\" refers to /\.)
 * The 2x2 grid is as follows:
 *
 * Example 4:
 *
 * Input:
 * [
 *   "/\\",
 *   "\\/"
 * ]
 * Output: 5
 * Explanation: (Recall that because \ characters are escaped, "/\\" refers to /\, and "\\/" refers to \/.)
 * The 2x2 grid is as follows:
 *
 * Example 5:
 *
 * Input:
 * [
 *   "//",
 *   "/ "
 * ]
 * Output: 3
 * Explanation: The 2x2 grid is as follows:
 *
 * Note:
 *
 *     1 <= grid.length == grid[0].length <= 30
 *     grid[i][j] is either '/', '\', or ' '.
 */
class Solution
{
public:
    void dfs(vector<vector<int>>& g, int i, int j)
    {
        if (i >= 0 && j >= 0 && i < g.size() && j < g.size() && g[i][j] == 0) {
            g[i][j] = 1;
            // color the empty region
            dfs(g, i - 1, j);
            dfs(g, i + 1, j);
            dfs(g, i, j - 1);
            dfs(g, i, j + 1);
        }
    }

    int regionsBySlashes(vector<string>& grid, int regions = 0)
    {
        vector<vector<int>> g(grid.size() * 3, vector<int>(grid.size() * 3, 0));
        // first we upscale the pattern
        for (auto i = 0; i < grid.size(); ++i) {
            for (auto j = 0; j < grid.size(); ++j) {
                if (grid[i][j] == '/') {
                    g[i * 3][j * 3 + 2] = 1;
                    g[i * 3 + 1][j * 3 + 1] = 1;
                    g[i * 3 + 2][j * 3] = 1;
                }

                if (grid[i][j] == '\\') {
                    g[i * 3][j * 3] = 1;
                    g[i * 3 + 1][j * 3 + 1] = 1;
                    g[i * 3 + 2][j * 3 + 2] = 1;
                }
            }
        }

        for (auto i = 0; i < g.size(); ++i) {
            for (auto j = 0; j < g.size(); ++j) {
                if (g[i][j] == 0) {
                    dfs(g, i, j);
                    ++regions;
                }
            }
        }

        return regions;
    }
};

class Solution
{
public:
    int count, n;
    vector<int> f;
    int regionsBySlashes(vector<string>& grid)
    {
        n = grid.size();
        count = n * n * 4;
        for (int i = 0; i < n * n * 4; ++i)
            f.push_back(i);
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                if (i > 0) uni(g(i - 1, j, 2), g(i, j, 0));
                if (j > 0) uni(g(i, j - 1, 1), g(i, j, 3));
                if (grid[i][j] != '/') {
                    uni(g(i, j, 0), g(i, j,  1));
                    uni(g(i, j, 2), g(i, j,  3));
                }
                if (grid[i][j] != '\\') {
                    uni(g(i, j, 0), g(i, j,  3));
                    uni(g(i, j, 2), g(i, j,  1));
                }
            }
        }
        return count;
    }

    int find(int x)
    {
        if (x != f[x]) {
            f[x] = find(f[x]);
        }
        return f[x];
    }
    void uni(int x, int y)
    {
        x = find(x);
        y = find(y);
        if (x != y) {
            f[x] = y;
            count--;
        }
    }
    int g(int i, int j, int k)
    {
        return (i * n + j) * 4 + k;
    }
};

