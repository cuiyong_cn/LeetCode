/**
 * Consecutive Numbers
 * Table: Logs
 *
 * +-------------+---------+
 * | Column Name | Type    |
 * +-------------+---------+
 * | id          | int     |
 * | num         | varchar |
 * +-------------+---------+
 * id is the primary key for this table.
 *
 * Write an SQL query to find all numbers that appear at least three times consecutively.
 *
 * Return the result table in any order.
 *
 * The query result format is in the following example:
 *
 * Logs table:
 * +----+-----+
 * | Id | Num |
 * +----+-----+
 * | 1  | 1   |
 * | 2  | 1   |
 * | 3  | 1   |
 * | 4  | 2   |
 * | 5  | 1   |
 * | 6  | 2   |
 * | 7  | 2   |
 * +----+-----+
 *
 * Result table:
 * +-----------------+
 * | ConsecutiveNums |
 * +-----------------+
 * | 1               |
 * +-----------------+
 * 1 is the only number that appears consecutively for at least three times.
 */
# Write your MySQL query statement below
select distinct L1.num as ConsecutiveNums
  from Logs L1, Logs L2, Logs L3
 where L1.id = L2.id - 1
   and L2.id = L3.id - 1
   and L1.num = L2.num
   and L2.num = L3.num;
