/**
 * Maximum Subarray Sum with One Deletion
 *
 * Given an array of integers, return the maximum sum for a non-empty subarray (contiguous elements)
 * with at most one element deletion. In other words, you want to choose a subarray and optionally
 * delete one element from it so that there is still at least one element left and the sum of the
 * remaining elements is maximum possible.
 *
 * Note that the subarray needs to be non-empty after deleting one element.
 *
 * Example 1:
 *
 * Input: arr = [1,-2,0,3]
 * Output: 4
 * Explanation: Because we can choose [1, -2, 0, 3] and drop -2, thus the subarray [1, 0, 3] becomes
 * the maximum value.
 * Example 2:
 *
 * Input: arr = [1,-2,-2,3]
 * Output: 3
 * Explanation: We just choose [3] and it's the maximum sum.
 * Example 3:
 *
 * Input: arr = [-1,-1,-1,-1]
 * Output: -1
 * Explanation: The final subarray needs to be non-empty. You can't choose [-1] and delete -1 from
 * it, then get an empty subarray to make the sum equals to 0.
 *
 * Constraints:
 *
 * 1 <= arr.length <= 105
 * -104 <= arr[i] <= 104
 */
/**
 * As always, dp's space complexity can be reduced.
 */
class Solution
{
public:
    int maximumSum(vector<int>& arr) {
        int n = arr.size(), res = arr[0], keep = arr[0], del = arr[0];
        for (int i = 1; i < n; i++) {
            del = max(keep, del + arr[i]);
            keep = max(keep, 0) + arr[i];
            res = max({ res, keep, del });
        }
        return res;
    }
};

/**
 * DP solution.
 */
class Solution {
public:
    int maximumSum(vector<int>& arr) {
        int n = arr.size(), res = arr[0];
        vector<int> keep (n), del (n);

        keep[0] = del[0] = arr[0];

        for (int i = 1; i < n; i++) {
            // The max sum of full subarray without deleting any element is the larger one between
            // the current element and (current + previous full subarray). This part is the same as
            // the Kadane algorithm.
            keep[i] = max(keep[i - 1], 0) + arr[i];

            // The max sum of "deleted" subarray can be evolved from the previous full subarray by
            // dropping the current element, or by having dropped some element previously and
            // include the current element.
            del[i] = max(keep[i - 1], del[i - 1] + arr[i]);

            // Keep track of the global maximum
            res = max({ res, keep[i], del[i] });
        }

        return res;
    }
};

/**
 * Original solution. Please note that, the deletion of element is optional.
 */
class Solution {
public:
    int maximumSum(vector<int>& arr) {
        if (1U == arr.size()) {
            return arr[0];
        }

        vector<int> max_to(arr.size() + 1, -10001);
        for (int i = 0; i < arr.size(); ++i) {
            if (max_to[i] >= 0) {
                max_to[i + 1] = max_to[i] + arr[i];
            }
            else {
                max_to[i + 1] = arr[i];
            }
        }

        vector<int> max_from(arr.size() + 1, -10001);
        for (int i = arr.size() - 1; i >= 0; --i) {
            if (max_from[i + 1] >= 0) {
                max_from[i] = max_from[i + 1] + arr[i];
            }
            else {
                max_from[i] = arr[i];
            }
        }

        int ans = -10001;
        for (int i = 0; i < arr.size(); ++i) {
            if (arr[i] > 0) {
                ans = max(ans, max_to[i] + arr[i]);
                ans = max(ans, max_from[i + 1] + arr[i]);
                ans = max(ans, max_to[i] + max_from[i + 1] + arr[i]);
            }
            else {
                ans = max(ans, max_to[i]);
                ans = max(ans, max_from[i + 1]);
                ans = max(ans, max_to[i] + max_from[i + 1]);
            }
        }

        return ans;
    }
};
