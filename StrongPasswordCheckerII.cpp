/**
 * Strong Password Checker II
 *
 * A password is said to be strong if it satisfies all the following criteria:
 *
 * It has at least 8 characters.
 * It contains at least one lowercase letter.
 * It contains at least one uppercase letter.
 * It contains at least one digit.
 * It contains at least one special character. The special characters are the characters in the
 * following string: "!@#$%^&*()-+".  It does not contain 2 of the same character in adjacent
 * positions (i.e., "aab" violates this condition, but "aba" does not).
 * Given a string password, return true if it is a strong password. Otherwise, return false.
 *
 * Example 1:
 *
 * Input: password = "IloveLe3tcode!"
 * Output: true
 * Explanation: The password meets all the requirements. Therefore, we return true.
 * Example 2:
 *
 * Input: password = "Me+You--IsMyDream"
 * Output: false
 * Explanation: The password does not contain a digit and also contains 2 of the same character in adjacent positions. Therefore, we return false.
 * Example 3:
 *
 * Input: password = "1aB!"
 * Output: false
 * Explanation: The password does not meet the length requirement. Therefore, we return false.
 *
 * Constraints:
 *
 * 1 <= password.length <= 100
 * password consists of letters, digits, and special characters: "!@#$%^&*()-+".
 */
/**
 * Shorter one.
 */
bool has_lower_case_letter(string const& pass)
{
    return any_of(begin(pass), end(pass), ::islower);
}

bool has_upper_case_letter(string const& pass)
{
    return any_of(begin(pass), end(pass), ::isupper);
}

bool has_digit(string const& pass)
{
    return any_of(begin(pass), end(pass), ::isdigit);
}

bool has_same_adjacent_letter(string const& pass)
{
    for (auto i = 1; i < pass.length(); ++i) {
        if (pass[i - 1] == pass[i]) {
            return true;
        }
    }
    return false;
}

bool has_special_letter(string const& pass)
{
    auto special = string{"!@#$%^&*()-+"};
    auto is_special = [&special](auto c) { return string::npos != special.find(c); };
    return any_of(begin(pass), end(pass), is_special);
}

class Solution {
public:
    bool strongPasswordCheckerII(string password) {
        return password.length() >= 8
        && has_lower_case_letter(password)
        && has_upper_case_letter(password)
        && has_digit(password)
        && has_special_letter(password)
        && !has_same_adjacent_letter(password);
    }
};

/**
 * Original solution.
 */
bool has_lower_case_letter(string const& pass)
{
    for (auto c : pass) {
        if (islower(c)) {
            return true;
        }
    }
    return false;
}

bool has_upper_case_letter(string const& pass)
{
    for (auto c : pass) {
        if (isupper(c)) {
            return true;
        }
    }

    return false;
}

bool has_digit(string const& pass)
{
    for (auto c : pass) {
        if (isdigit(c)) {
            return true;
        }
    }
    return false;
}

bool has_same_adjacent_letter(string const& pass)
{
    for (auto i = 1; i < pass.length(); ++i) {
        if (pass[i - 1] == pass[i]) {
            return true;
        }
    }
    return false;
}

bool has_special_letter(string const& pass)
{
    auto special = string{"!@#$%^&*()-+"};
    for (auto c : pass) {
        if (string::npos != special.find(c)) {
            return true;
        }
    }
    return false;
}

class Solution {
public:
    bool strongPasswordCheckerII(string password) {
        return password.length() >= 8
        && has_lower_case_letter(password)
        && has_upper_case_letter(password)
        && has_digit(password)
        && has_special_letter(password)
        && !has_same_adjacent_letter(password);
    }
};
