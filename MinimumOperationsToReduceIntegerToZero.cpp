/**
 * Minimum Operations to Reduce an Integer to 0
 *
 * You are given a positive integer n, you can do the following operation any number of times:
 *
 * Add or subtract a power of 2 from n.
 * Return the minimum number of operations to make n equal to 0.
 *
 * A number x is power of 2 if x == 2i where i >= 0.
 *
 * Example 1:
 *
 * Input: n = 39
 * Output: 3
 * Explanation: We can do the following operations:
 * - Add 20 = 1 to n, so now n = 40.
 * - Subtract 23 = 8 from n, so now n = 32.
 * - Subtract 25 = 32 from n, so now n = 0.
 * It can be shown that 3 is the minimum number of operations we need to make n equal to 0.
 * Example 2:
 *
 * Input: n = 54
 * Output: 3
 * Explanation: We can do the following operations:
 * - Add 21 = 2 to n, so now n = 56.
 * - Add 23 = 8 to n, so now n = 64.
 * - Subtract 26 = 64 from n, so now n = 0.
 * So the minimum number of operations is 3.
 *
 * Constraints:
 *
 * 1 <= n <= 105
 */
/**
 * One line solution.
 */
class Solution {
public:
    int minOperations(int n) {
        return __builtin_popcount(n ^ (n * 3));
    }
};

/**
 * Same idea. Diff implementation.
 */
class Solution {
public:
    int minOperations(int n) {
        auto ans = 0;
        while (n > 0) {
            if (3 == (n & 3)) {
                ++n;
                ++ans;
            }
            else {
                ans += n & 1;
                n >>= 1;
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int minOperations(int n) {
        auto const bits = 8 * sizeof(int);
        auto ones = 0;
        auto ans = 0;
        for (auto i = 0; i < bits; ++i) {
            if ((n >> i) & 1) {
                ++ones;
            }
            else {
                ans += ones > 0 ? 1 : 0;
                ones = ones > 1 ? 1 : 0;
            }
        }
        return ans;
    }
};
