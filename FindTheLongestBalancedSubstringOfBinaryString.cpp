/**
 * Find the Longest Balanced Substring of a Binary String
 *
 * You are given a binary string s consisting only of zeroes and ones.
 *
 * A substring of s is considered balanced if all zeroes are before ones and the
 * number of zeroes is equal to the number of ones inside the substring. Notice
 * that the empty substring is considered a balanced substring.
 *
 * Return the length of the longest balanced substring of s.
 *
 * A substring is a contiguous sequence of characters within a string.
 *
 * Example 1:
 *
 * Input: s = "01000111"
 * Output: 6
 * Explanation: The longest balanced substring is "000111", which has length 6.
 * Example 2:
 *
 * Input: s = "00111"
 * Output: 4
 * Explanation: The longest balanced substring is "0011", which has length 4.
 * Example 3:
 *
 * Input: s = "111"
 * Output: 0
 * Explanation: There is no balanced substring except the empty substring, so the answer is 0.
 *
 * Constraints:
 *
 * 1 <= s.length <= 50
 * '0' <= s[i] <= '1'
 */
/**
 * Simplify.
 */
class Solution {
public:
    int findTheLongestBalancedSubstring(string s) {
        auto ans = 0;
        auto zero_cnt = 0;
        auto one_cnt = 0;
        for (auto&& c : s) {
            if ('0' == c) {
                zero_cnt = one_cnt > 0 ? 1 : zero_cnt + 1;
            }
            one_cnt = '0' == c ? 0 : one_cnt + 1;
            ans = max(ans, min(zero_cnt, one_cnt) * 2);
        }
        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int findTheLongestBalancedSubstring(string s) {
        auto ans = 0;
        auto i = 0;
        auto size = s.length();
        while (i < size) {
            if ('0' == s[i]) {
                auto zero_cnt = 0;
                for (; i < size; ++i) {
                    if ('1' == s[i]) {
                        break;
                    }
                    ++zero_cnt;
                }
                auto one_cnt = 0;
                for (; i < size; ++i) {
                    if ('0' == s[i]) {
                        break;
                    }
                    ++one_cnt;
                }
                ans = max(ans, min(zero_cnt, one_cnt) * 2);
            }
            else {
                ++i;
            }
        }
        return ans;
    }
};
