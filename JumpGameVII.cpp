/**
 * Jump Game VII
 *
 * You are given a 0-indexed binary string s and two integers minJump and maxJump. In the beginning,
 * you are standing at index 0, which is equal to '0'. You can move from index i to index j if the
 * following conditions are fulfilled:
 *
 * i + minJump <= j <= min(i + maxJump, s.length - 1), and
 * s[j] == '0'.
 * Return true if you can reach index s.length - 1 in s, or false otherwise.
 *
 * Example 1:
 *
 * Input: s = "011010", minJump = 2, maxJump = 3
 * Output: true
 * Explanation:
 * In the first step, move from index 0 to index 3.
 * In the second step, move from index 3 to index 5.
 * Example 2:
 *
 * Input: s = "01101110", minJump = 2, maxJump = 3
 * Output: false
 *
 * Constraints:
 *
 * 2 <= s.length <= 105
 * s[i] is either '0' or '1'.
 * s[0] == '0'
 * 1 <= minJump <= maxJump < s.length
 */
/**
 * Sliding + dp solution.
 */
class Solution {
public:
    bool canReach(string s, int minJump, int maxJump) {
        int const SL = s.length();
        int pre = 0;
        vector<int> dp(SL, 0);
        dp[0] = 1;

        for (int i = 1; i < SL; ++i) {
            if (i >= minJump) {
                pre += dp[i - minJump];
            }

            if (i > maxJump) {
                pre -= dp[i - maxJump - 1];
            }

            dp[i] = pre > 0 && '0' == s[i] ? 1 : 0;
        }

        return 1 == dp.back();
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    bool canReach(string s, int minJump, int maxJump) {
        if ('1' == s.back()) {
            return false;
        }

        int SL = s.length();
        deque<pair<int, int>> reachable{{minJump, min(maxJump, SL - 1)}};

        int i = 0;
        while (!reachable.empty()) {
            auto [L, R] = reachable.front();
            reachable.pop_front();

            if (L >= SL) {
                break;
            }

            i = max(i, L);
            for (; i <= R; ++i) {
                if ('0' == s[i]) {
                    reachable.emplace_back(i + minJump, min(i + maxJump, SL - 1));
                }
            }
        }

        return SL == i;
    }
};
