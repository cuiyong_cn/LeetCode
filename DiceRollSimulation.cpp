/**
 * Dice Roll Simulation
 *
 * A die simulator generates a random number from 1 to 6 for each roll. You introduced a constraint to the generator
 * such that it cannot roll the number i more than rollMax[i] (1-indexed) consecutive times.
 *
 * Given an array of integers rollMax and an integer n, return the number of distinct sequences that can be obtained
 * with exact n rolls.
 *
 * Two sequences are considered different if at least one element differs from each other. Since the answer may be too
 * large, return it modulo 10^9 + 7.
 *
 * Example 1:
 *
 * Input: n = 2, rollMax = [1,1,2,2,2,3]
 * Output: 34
 * Explanation: There will be 2 rolls of die, if there are no constraints on the die, there are 6 * 6 = 36 possible
 * combinations. In this case, looking at rollMax array, the numbers 1 and 2 appear at most once consecutively,
 * therefore sequences (1,1) and (2,2) cannot occur, so the final answer is 36-2 = 34.
 * Example 2:
 *
 * Input: n = 2, rollMax = [1,1,1,1,1,1]
 * Output: 30
 * Example 3:
 *
 * Input: n = 3, rollMax = [1,1,1,2,2,3]
 * Output: 181
 *
 * Constraints:
 *
 * 1 <= n <= 5000
 * rollMax.length == 6
 * 1 <= rollMax[i] <= 15
 */
/**
 * Original solution.
 * dp[i][j] at step i, number of combinations that end with j (0..5)
 * sum[i] total combinations at step i
 *
 * note:
 *      sum[i], sum[j] when i < j, sum[j] < sum[i] is possible because of modulo
 */
class Solution {
public:
    int dieSimulator(int n, vector<int>& rollMax) {
        int p = 1e9 + 7;
        vector<vector<long>> dp(n+1, vector<long>(6, 0));
        vector<long> sum(n+1, 0); // sum[i] total combos at step i
        sum[0] = 1;

        for (int i = 1; i <= n; ++i) {
            for (int j = 0; j < 6; ++j)  {
                /*
                for (int k = 1; k <= rollMax[j] && i - k >= 0; ++k)
                    dp[i][j] = (dp[i][j] + sum[i - k] - dp[i-k][j] + p) % p;
                */
                /**
                 * replace above with the following.
                 * Thinking. when i > rollMax[j], mean we over count the combinations.
                 * but dp[i - 1][j] does not break the constraints. we actually make
                 * dp[i - 1 - rollMax[j]][j] break the constraints, so we have to rule
                 * that out.
                 */
                dp[i][j] = sum[i - 1];
                if (i > rollMax[j]) {
                    dp[i][j] = (dp[i][j] - sum[i - 1 - rollMax[j]] + dp[i - 1 - rollMax[j]][j] + p) % p;
                }

                sum[i] = (sum[i] + dp[i][j]) % p;
            }
        }

        return sum[n];
    }
};
