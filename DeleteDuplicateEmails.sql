/**
 * Delete Duplicate Emails
 */
DELETE FROM Person WHERE Id NOT IN 
(SELECT * FROM (
    SELECT MIN(Id) FROM Person GROUP BY Email) as p);

# Write your MySQL query statement below
delete a from Person a, Person b
 where a.id > b.id and a.email = b.email;
