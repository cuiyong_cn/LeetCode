/**
 * Longest Consecutive Sequence
 *
 * Given an unsorted array of integers nums, return the length of the longest consecutive elements
 * sequence.
 *
 * You must write an algorithm that runs in O(n) time.
 *
 * Example 1:
 *
 * Input: nums = [100,4,200,1,3,2]
 * Output: 4
 * Explanation: The longest consecutive elements sequence is [1, 2, 3, 4]. Therefore its length is 4.
 * Example 2:
 *
 * Input: nums = [0,3,7,2,5,8,4,6,0,1]
 * Output: 9
 *
 * Constraints:
 *
 * 0 <= nums.length <= 105
 * -109 <= nums[i] <= 109*
 */
/**
 * O(n) solution.
 */
class Solution {
public:
    int longestConsecutive(vector<int>& nums) {
        unordered_set<int> nsets(begin(nums), end(nums));

        int ans = 0;

        for (auto n : nsets) {
            if (0 == nsets.count(n - 1)) {
                int num = n, len = 1;

                while (nsets.count(num + 1)) {
                    ++len;
                    ++num;
                }

                ans = max(ans, len);
            }
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int longestConsecutive(vector<int>& nums) {
        sort(begin(nums), end(nums));

        int ans = nums.empty() ? 0 : 1;
        int len = ans;
        for (int i = 1; i < nums.size(); ++i) {
            if (nums[i] == (nums[i - 1] + 1)) {
                ++len;
            }
            else if (nums[i] == nums[i - 1] ) {
                ;
            }
            else {
                ans = max(ans, len);
                len = 1;
            }
        }

        return max(ans, len);
    }
};
