/**
 * Split Array into Consecutive Subsequences
 *
 * Given an array nums sorted in ascending order, return true if and only if you can split it into 1 or more
 * subsequences such that each subsequence consists of consecutive integers and has length at least 3.
 *
 *
 *
 * Example 1:
 *
 * Input: [1,2,3,3,4,5]
 * Output: True
 * Explanation:
 * You can split them into two consecutive subsequences :
 * 1, 2, 3
 * 3, 4, 5
 * Example 2:
 *
 * Input: [1,2,3,3,4,4,5,5]
 * Output: True
 * Explanation:
 * You can split them into two consecutive subsequences :
 * 1, 2, 3, 4, 5
 * 3, 4, 5
 * Example 3:
 *
 * Input: [1,2,3,4,4,5]
 * Output: False
 *
 *
 * Constraints:
 *
 * 1 <= nums.length <= 10000
 */
/**
 * Efficient and general one.
 */
class Solution {
public:
    bool isPossible(vector<int>& nums) {
        deque<int> starts;
        int prev_count = 0;
        bool has_prev = false;
        int prev = 0;
        int anchor = 0;

        for (int i = 0; i < nums.size(); ++i) {
            int t = nums[i];
            if (i == (nums.size() - 1) || nums[i + 1] != t) {
                int count = i - anchor + 1;
                if (has_prev && (t - prev) != 1) {
                    while (prev_count-- > 0) {
                        if (prev < (starts.front() + 2)) {
                            return false;
                        }
                        starts.pop_front();
                    }
                    has_prev = false;
                }

                if (!has_prev || (t - prev) == 1) {
                    while (prev_count > count) {
                        --prev_count;
                        if ((t - 1) < (starts.front() + 2)) {
                            return false;
                        }
                        starts.pop_front();
                    }

                    while (prev_count++ < count) {
                        starts.push_back(t);
                    }
                }

                prev = t;
                has_prev = true;
                prev_count = count;
                anchor = i + 1;
            }
        }

        while (prev_count-- > 0) {
            if (nums.back() < (starts.front() + 2)) {
                return false;
            }
            starts.pop_front();
        }

        return true;
    }
};

/**
 * Similar greedy solution. But more general.
 */
class Solution {
public:
	bool isPossible(vector<int>& nums)
	{
		unordered_map<int, priority_queue<int, vector<int>, std::greater<int>>> backs;

		// Keep track of the number of sequences with size < 3
		int need_more = 0;
		for (int num : nums) {
			if (!backs[num - 1].empty()) {
                // There exists a sequence that ends in num-1
				// Append 'num' to this sequence
				// Remove the existing sequence
				// Add a new sequence ending in 'num' with size incremented by 1
				int count = backs[num - 1].top();
				backs[num - 1].pop();
				backs[num].push(++count);

                if (count == 3) {
					need_more--;
                }
			}
			else {
                // There is no sequence that ends in num-1
				// Create a new sequence with size 1 that ends with 'num'
				backs[num].push(1);
				need_more++;
			}
		}
		return need_more == 0;
	}
};

/**
 * I did not come up with a solution.
 * Greedy solution.
 *
 * left[i] counts the number of i that I haven't placed yet.
 * end[i] counts the number of consecutive subsequences that ends at number i
 */
class Solution {
public:
    bool isPossible(vector<int>& nums) {
        unordered_map<int, int> left, end;
        for (auto n : nums) {
            ++left[n];
        }

        for (auto n : nums) {
            if (!left[n]) {
                continue;
            }

            --left[n];
            if (end[n - 1] > 0) {
                --end[n - 1];
                ++end[n];
            }
            else if (left[n + 1] > 0 && left[n + 2] > 0) {
                --left[n + 1];
                --left[n + 2];
                ++end[n + 2];
            }
            else {
                return false;
            }
        }

        return true;
    }
};
