/**
 * 3Sum Closest
 *
 * Given an array nums of n integers and an integer target, find three integers in nums such that the sum is closest to
 * target. Return the sum of the three integers. You may assume that each input would have exactly one solution.
 *
 * Example 1:
 *
 * Input: nums = [-1,2,1,-4], target = 1
 * Output: 2
 * Explanation: The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
 *
 * Constraints:
 *
 * 3 <= nums.length <= 10^3
 * -10^3 <= nums[i] <= 10^3
 * -10^4 <= target <= 10^4
 */
/**
 * more efficient one.
 */
class Solution {
public:
    int threeSumClosest(vector<int>& nums, int target) {
        auto diff = std::numeric_limits<int>::max();
        int sz = nums.size();

        sort(begin(nums), end(nums));

        for (int i = 0; i < sz && diff != 0; ++i) {
            int lo = i + 1, hi = sz - 1;
            while (lo < hi) {
                int sum = nums[i] + nums[lo] + nums[hi];
                if (abs(target - sum) < abs(diff)) {
                    diff = target - sum;
                }
                if (sum < target) {
                    ++lo;
                }
                else {
                    --hi;
                }
            }
        }
        return target - diff;
    }
};

/**
 * Original solution. This actually passed!?
 */
class Solution {
public:
    int threeSumClosest(vector<int>& nums, int target) {
        int ans = nums[0] + nums[1] + nums[2];

        for (int i = 0; i < nums.size() - 2; ++i) {
            if (ans == target) {
                break;
            }
            for (int j = i + 1; j < nums.size() - 1; ++j) {
                for (int k = j + 1; k < nums.size(); ++k) {
                    auto sum = nums[i] + nums[j] + nums[k];
                    if (std::abs(sum - target) < std::abs(ans - target)) {
                        ans = sum;
                    }
                }
            }
        }

        return ans;
    }
};
