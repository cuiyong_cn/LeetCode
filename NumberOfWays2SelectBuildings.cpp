/**
 * Number of Ways to Select Buildings
 *
 * You are given a 0-indexed binary string s which represents the types of buildings along a street where:
 *
 * s[i] = '0' denotes that the ith building is an office and
 * s[i] = '1' denotes that the ith building is a restaurant.
 * As a city official, you would like to select 3 buildings for random inspection. However, to
 * ensure variety, no two consecutive buildings out of the selected buildings can be of the same
 * type.
 *
 * For example, given s = "001101", we cannot select the 1st, 3rd, and 5th buildings as that would
 * form "011" which is not allowed due to having two consecutive buildings of the same type.
 * Return the number of valid ways to select 3 buildings.
 *
 * Example 1:
 *
 * Input: s = "001101"
 * Output: 6
 * Explanation:
 * The following sets of indices selected are valid:
 * - [0,2,4] from "001101" forms "010"
 * - [0,3,4] from "001101" forms "010"
 * - [1,2,4] from "001101" forms "010"
 * - [1,3,4] from "001101" forms "010"
 * - [2,4,5] from "001101" forms "101"
 * - [3,4,5] from "001101" forms "101"
 * No other selection is valid. Thus, there are 6 total ways.
 * Example 2:
 *
 * Input: s = "11100"
 * Output: 0
 * Explanation: It can be shown that there are no valid selections.
 *
 * Constraints:
 *
 * 3 <= s.length <= 105
 * s[i] is either '0' or '1'.
 */
/**
 * WTF solution from discussion.
 */
class Solution {
public:
    long long numberOfWays(string s) {
        auto a0 = 0LL; // occurances of '0' after current char.
        auto a1 = 0LL; // occurances of '1' after current char.
                       //
        for (auto c : s){
            if ('1' == c) {
                ++a1;
            }
            else {
                ++a0;
            }
        }

        auto ans = 0LL;
        auto b0 = 0LL; // occurances of '0' before current char.
        auto b1 = 0LL; // occurances of '1' before current char.
        for (auto c : s) {
            if ('1' == c) {
                ans += b0 * a0;
                ++b1;
                --a1;
            }
            else {
                ans += b1 * a1;
                ++b0;
                --a0;
            }
        }

        return ans;
    }
};

/**
 * Improve dp.
 */
class Solution {
public:
    long long numberOfWays(string s) {
        long long dp[4][2] = {};
        dp[0][0] = dp[0][1] = 1;
        for (int i = 0; i < s.size(); ++i) {
            for (int len = 1; len <= 3; ++len) {
                dp[len][s[i] - '0'] += dp[len - 1][1 - (s[i] - '0')];
            }
        }

        return dp[3][0] + dp[3][1];
    }
};

/**
 * Original solution.
 */
auto prefix(string const& s, char c)
{
    auto n = vector<long long>(s.size() + 1, 0);
    for (auto i = 0; i < s.size(); ++i) {
        n[i + 1] = n[i] + (c == s[i] ? 1 : 0);
    }
    return n;
}

auto prefix_n0(string const& s)
{
    return prefix(s, '0');
}

auto prefix_n1(string const& s)
{
    return prefix(s, '1');
}

auto prefix(string const& s, vector<long long> const& p, char c)
{
    auto n = vector<long long>(s.size() + 1, 0);
    for (auto i = 0; i < s.size(); ++i) {
        n[i + 1] = n[i] + (c == s[i] ? p[i] : 0);
    }
    return n;
}

auto prefix_n01(string const& s, vector<long long> const& p)
{
    return prefix(s, p, '1');
}

auto prefix_n10(string const& s, vector<long long> const& p)
{
    return prefix(s, p, '0');
}

auto prefix_n010(string const& s, vector<long long> const& p)
{
    return prefix(s, p, '0');
}

auto prefix_n101(string const& s, vector<long long> const& p)
{
    return prefix(s, p, '1');
}

class Solution {
public:
    long long numberOfWays(string s) {
        auto n0 = prefix_n0(s);
        auto n1 = prefix_n1(s);
        auto n01 = prefix_n01(s, n0);
        auto n10 = prefix_n10(s, n1);
        auto n010 = prefix_n010(s, n01);
        auto n101 = prefix_n101(s, n10);

        return n010.back() * 1LL + n101.back();
    }
};
