/**
 * Maximum Absolute Sum of Any Subarray
 *
 * You are given an integer array nums. The absolute sum of a subarray [numsl, numsl+1, ...,
 * numsr-1, numsr] is abs(numsl + numsl+1 + ... + numsr-1 + numsr).
 *
 * Return the maximum absolute sum of any (possibly empty) subarray of nums.
 *
 * Note that abs(x) is defined as follows:
 *
 * If x is a negative integer, then abs(x) = -x.
 * If x is a non-negative integer, then abs(x) = x.
 *
 * Example 1:
 *
 * Input: nums = [1,-3,2,3,-4]
 * Output: 5
 * Explanation: The subarray [2,3] has absolute sum = abs(2+3) = abs(5) = 5.
 * Example 2:
 *
 * Input: nums = [2,-5,1,-4,3,-2]
 * Output: 8
 * Explanation: The subarray [-5,1,-4] has absolute sum = abs(-5+1-4) = abs(-8) = 8.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 105
 * -104 <= nums[i] <= 104
 */
/**
 * Simplify below logic. Idea is:
 * draw the prefix_sum on the 2D plane, and you will find out why it works.
 */
class Solution {
public:
    int maxAbsoluteSum(vector<int>& n) {
        partial_sum(begin(n), end(n), begin(n));
        return max(0, *max_element(begin(n), end(n))) - min(0, *min_element(begin(n), end(n)));
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    int maxAbsoluteSum(vector<int>& nums) {
        partial_sum(nums.begin(), nums.end(), nums.begin());

        int ans = abs(*max_element(nums.begin(), nums.end(),
                    [](auto const& a, auto const& b) { return abs(a) < abs(b); }));
        int max_sum = numeric_limits<int>::min();
        int min_sum = numeric_limits<int>::max();

        for (auto const& n : nums) {
            max_sum = max(max_sum, n);
            min_sum = min(min_sum, n);
            ans = max(ans,
                     max(abs(n - max_sum), abs(n - min_sum)));
        }

        return ans;
    }
};
