/** Merge Two Sorted Lists
 *
 * Merge two sorted linked lists and return it as a new list. The new list should be made by splicing together the nodes
 * of the first two lists.
 *
 * Example:
 *
 * Input: 1->2->4, 1->3->4 Output: 1->1->2->3->4->4
 */
/**
 * use a dummy node to eliminate the clutter. We can also solve this recusively. But recusion sometimes is bad choice.
 */
class Solution
{
public:
    ListNode *mergeTwoLists(ListNode *l1, ListNode *l2)
    {
        ListNode dummy(0);
        ListNode *tail = &dummy;

        while (l1 && l2) {
            if (l1->val < l2->val) {
                tail->next = l1;
                l1 = l1->next;
            } else {
                tail->next = l2;
                l2 = l2->next;
            }
            tail = tail->next;
        }

        tail->next = l1 ? l1 : l2;
        return dummy.next;
    }
};

/**
 * original solution.
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution
{
public:
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2)
    {
        if (nullptr == l1) return l2;
        if (nullptr == l2) return l1;

        ListNode* p1 = l1, * p2 = l2;
        ListNode* tail = l1->val <= l2->val ? l2 : l1;
        while (nullptr != p1 && nullptr != p2) {
            if ((p1->val < p2->val) || (p1->val == p2->val && p2 == tail)) {
                tail = p1->next;
                if ((tail && tail->val >= p2->val) || (nullptr == tail)) {
                    p1->next = p2;
                }
                p1 = tail;
            } else {
                tail = p2->next;
                if ((tail && tail->val >= p1->val) || (nullptr == tail)) {
                    p2->next = p1;
                }
                p2 = tail;
            }
        }

        return l1->val <= l2->val ? l1 : l2;
    }
};

