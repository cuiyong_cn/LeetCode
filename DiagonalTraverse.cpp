/**
 * Diagonal Traverse
 *
 * Given a matrix of M x N elements (M rows, N columns), return all elements of the matrix in diagonal order as shown in
 * the below image.
 *
 * Example:
 *
 * Input:
 * [
 *  [ 1, 2, 3 ],
 *  [ 4, 5, 6 ],
 *  [ 7, 8, 9 ]
 * ]
 *
 * Output:  [1,2,4,7,5,3,6,8,9]
 *
 * Explanation:
 *
 * Note:
 *
 * The total number of elements of the given matrix will not exceed 10,000.
 */
/**
 * Though less efficient thant the original one. But easier to understand.
 * The idea is:
 *      the sum of the index of the element in the same diagonal are the same.
 *      so first we can group the elements according to this rule.
 *
 *      Then twist a bit to build the answer
 */
class Solution
{
public:
    vector<int> findDiagonalOrder(vector<vector<int>>& matrix)
    {
        if (matrix.empty() || matrix[0].empty()) {
            return {};
        }

        int M = matrix.size();
        int N = matrix[0].size();

        vector<vector<int>> indice_sub(M + N - 1);
        for (size_t r = 0; r < M; ++r) {
            for (size_t c = 0; c < N; ++c) {
                indice_sub[r + c].push_back(matrix[r][c]);
            }
        }

        vector<int> ans;
        for (size_t i = 0; i < indice_sub.size(); ++i) {
            if (i % 2 == 0) {
                std::reverse(indice_sub[i].begin(), indice_sub[i].end());
            }

            ans.insert(ans.end(), indice_sub[i].begin(), indice_sub[i].end());
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution
{
public:
    vector<int> findDiagonalOrder(vector<vector<int>>& matrix)
    {
        if (matrix.empty() || matrix[0].empty()) {
            return {};
        }

        int M = matrix.size();
        int N = matrix[0].size();
        int x_speed = -1;
        int y_speed = 1;
        int x = 0;
        int y = 0;

        vector<int> ans;

        while (1) {
            if ((M - 1) == x && (N - 1) == y) {
                ans.push_back(matrix[x][y]);
                break;
            }

            ans.push_back(matrix[x][y]);
            int nx = x + x_speed;
            int ny = y + y_speed;
            if (nx < 0 || ny >= N) {
                x_speed = 1;
                y_speed = -1;
            }

            if (ny < 0 || nx >= M) {
                x_speed = -1;
                y_speed = 1;
            }

            if (nx < 0) {
                nx = 0;
            }

            if (ny < 0) {
                ny = 0;
            }

            if (nx >= M) {
                nx = M - 1;
                ny = y + 1;
            }

            if (ny >= N) {
                ny = y;
                nx = x + 1;
            }

            x = nx;
            y = ny;
        }

        return ans;
    }
};

/**
 * Same idea with the grouping one. But different implementation written in java.
 */
class Solution
{
    public int[] findDiagonalOrder(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return new int[0];
        }

        int N = matrix.length;
        int M = matrix[0].length;

        int[] result = new int[N * M];
        int k = 0;
        ArrayList<Integer> intermediate = new ArrayList<Integer>();

        // We have to go over all the elements in the first
        // row and the last column to cover all possible diagonals
        for (int d = 0; d < N + M - 1; d++) {
            intermediate.clear();

            // Figure out the "head" of this diagonal
            int r = d < M ? 0 : d - M + 1;
            int c = d < M ? d : M - 1;

            while (r < N && c > -1) {
                intermediate.add(matrix[r][c]);
                ++r;
                --c;
            }

            if (d % 2 == 0) {
                Collections.reverse(intermediate);
            }

            for (int i = 0; i < intermediate.size(); i++) {
                result[k++] = intermediate.get(i);
            }
        }

        return result;
    }
}

/**
 * Another simulating method from discussion written in java.
 */
class Solution
{
    public int[] findDiagonalOrder(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            return new int[0];
        }

        int M = matrix.length;
        int N = matrix[0].length;

        int[] result = new int[M * N];
        result[0] = matrix[0][0]; //Initialization start position

        int i = 0, j = 0, k = 1;
        while (k < N * M) {
            //move up-right first
            while (i >= 1 && j < N - 1) {
                i--;
                j++;
                result[k++] = matrix[i][j];
            }
            //when we can't move up-right ,then move right one step
            if (j < N - 1) {
                j++;
                result[k++] = matrix[i][j];
            }
            //if we can't move right,just move down one step
            else if (i < M - 1) {
                i++;
                result[k++] = matrix[i][j];
            }
            //After that,we will move down-left until it can't move
            while (i < M - 1 && j >= 1) {
                i++;
                j--;
                result[k++] = matrix[i][j];
            }
            //if we can't move down-left,then move down
            if (i < M - 1) {
                i++;
                result[k++] = matrix[i][j];
            }
            //if we can't move down,just move right
            else if (j < N - 1) {
                j++;
                result[k++] = matrix[i][j];
            }
        }

        return result;
    }
}



