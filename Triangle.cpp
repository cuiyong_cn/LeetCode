/**
 * Triangle
 *
 * Given a triangle, find the minimum path sum from top to bottom. Each step you may move to adjacent numbers on the row
 * below.
 *
 * For example, given the following triangle
 *
 * [ [2], [3,4], [6,5,7], [4,1,8,3] ] The minimum path sum from top to bottom is 11 (i.e., 2 + 3 + 5 + 1 = 11).
 *
 * Note:
 *
 * Bonus point if you are able to do this using only O(n) extra space, where n is the total number of rows in the
 * triangle.
 */
/**
 * dp, bottom up.
 */
class Solution {
public:
    int minimumTotal(vector<vector<int> > &triangle) {
        vector<int> mini = triangle.back();
        for (int i = triangle.size() - 2; i>= 0 ; --i) {
            for (int j = 0; j < triangle[i].size() ; ++ j) {
                mini[j] = triangle[i][j] + min(mini[j], mini[j+1]);
            }
        }
        return mini[0];
    }
};

/**
 * passed, bottom up, using memo
 */
class Solution {
public:
    int minimumTotal(vector<vector<int>>& triangle) {
        return dfs(triangle, 0, 0);
    }

private:
    int dfs(const vector<vector<int>>& triangle, int row, int col)
    {
        if (row >= triangle.size()) {
            return 0;
        }

        auto point = std::make_pair(row, col);
        if (memo.count(point)) {
            return memo[point];
        }

        int left = dfs(triangle, row + 1, col);
        int right = dfs(triangle, row + 1, col + 1);

        return memo[point] = triangle[row][col] + std::min(left, right);
    }

    map<pair<int, int>, int> memo;
};

/**
 * Original solution. Simple but TLE.
 */
class Solution {
public:
    int minimumTotal(vector<vector<int>>& triangle) {
        int ans = numeric_limits<int>::max();
        dfs(triangle, 0, 0, 0, ans);
        return ans;
    }

private:
    void dfs(const vector<vector<int>>& triangle, int row, int col, int sum, int& min_sum)
    {
        if (row >= triangle.size()) {
            min_sum = std::min(sum, min_sum);
            return;
        }

        dfs(triangle, row + 1, col, sum + triangle[row][col], min_sum);
        dfs(triangle, row + 1, col + 1, sum + triangle[row][col], min_sum);
    }
};
