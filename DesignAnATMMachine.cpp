/**
 * Design an ATM Machine
 *
 * There is an ATM machine that stores banknotes of 5 denominations: 20, 50, 100, 200, and 500
 * dollars. Initially the ATM is empty. The user can use the machine to deposit or withdraw any
 * amount of money.
 *
 * When withdrawing, the machine prioritizes using banknotes of larger values.
 *
 * For example, if you want to withdraw $300 and there are 2 $50 banknotes, 1 $100 banknote, and 1
 * $200 banknote, then the machine will use the $100 and $200 banknotes.  However, if you try to
 * withdraw $600 and there are 3 $200 banknotes and 1 $500 banknote, then the withdraw request will
 * be rejected because the machine will first try to use the $500 banknote and then be unable to use
 * banknotes to complete the remaining $100. Note that the machine is not allowed to use the $200
 * banknotes instead of the $500 banknote.
 * Implement the ATM class:
 *
 * ATM() Initializes the ATM object.
 * void deposit(int[] banknotesCount) Deposits new banknotes in the order $20, $50, $100, $200, and $500.
 * int[] withdraw(int amount) Returns an array of length 5 of the number of banknotes that will be
 * handed to the user in the order $20, $50, $100, $200, and $500, and update the number of
 * banknotes in the ATM after withdrawing. Returns [-1] if it is not possible (do not withdraw any
 * banknotes in this case).
 *
 * Example 1:
 *
 * Input
 * ["ATM", "deposit", "withdraw", "deposit", "withdraw", "withdraw"]
 * [[], [[0,0,1,2,1]], [600], [[0,1,0,1,1]], [600], [550]]
 * Output
 * [null, null, [0,0,1,0,1], null, [-1], [0,1,0,0,1]]
 *
 * Explanation
 * ATM atm = new ATM();
 * atm.deposit([0,0,1,2,1]); // Deposits 1 $100 banknote, 2 $200 banknotes,
 *                           // and 1 $500 banknote.
 * atm.withdraw(600);        // Returns [0,0,1,0,1]. The machine uses 1 $100 banknote
 *                           // and 1 $500 banknote. The banknotes left over in the
 *                           // machine are [0,0,0,2,0].
 * atm.deposit([0,1,0,1,1]); // Deposits 1 $50, $200, and $500 banknote.
 *                           // The banknotes in the machine are now [0,1,0,3,1].
 * atm.withdraw(600);        // Returns [-1]. The machine will try to use a $500 banknote
 *                           // and then be unable to complete the remaining $100,
 *                           // so the withdraw request will be rejected.
 *                           // Since the request is rejected, the number of banknotes
 *                           // in the machine is not modified.
 * atm.withdraw(550);        // Returns [0,1,0,0,1]. The machine uses 1 $50 banknote
 *                           // and 1 $500 banknote.
 *
 * Constraints:
 *
 * banknotesCount.length == 5
 * 0 <= banknotesCount[i] <= 109
 * 1 <= amount <= 109
 * At most 5000 calls in total will be made to withdraw and deposit.
 * At least one call will be made to each function withdraw and deposit.
 */
/**
 * Original solution.
 */
int banknote(int i) {
    switch (i) {
    case 0:
        return 20;
    case 1:
        return 50;
    case 2:
        return 100;
    case 3:
        return 200;
    case 4:
        return 500;
    }

    return 1;
}

class ATM {
public:
    ATM() {

    }

    void deposit(vector<int> banknotesCount) {
        for (auto i = 0; i < 5; ++i) {
            banknotes[i] += banknotesCount[i];
        }
    }

    vector<int> withdraw(int amount) {
        auto ans = vector{0, 0, 0, 0, 0};
        for (auto i = 4; i >= 0; --i) {
            if (banknotes[i] > 0) {
                auto bn = banknote(i);
                auto cnt = min<long long>(banknotes[i], amount / bn);
                amount -= cnt * bn;
                ans[i] = cnt;
                banknotes[i] -= cnt;
            }
        }

        if (0 != amount) {
            deposit(ans);
            return {-1};
        }

        return ans;
    }

private:
    vector<long long> banknotes{0, 0, 0, 0, 0};
};

/**
 * Your ATM object will be instantiated and called as such:
 * ATM* obj = new ATM();
 * obj->deposit(banknotesCount);
 * vector<int> param_2 = obj->withdraw(amount);
 */
