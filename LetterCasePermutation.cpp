/**
 * Letter Case Permutation
 *
 * Given a string S, we can transform every letter individually to be lowercase or
 * uppercase to create another string.  Return a list of all possible strings we could
 * create.
 *
 * Examples:
 * Input: S = "a1b2"
 * Output: ["a1b2", "a1B2", "A1b2", "A1B2"]
 *
 * Input: S = "3z4"
 * Output: ["3z4", "3Z4"]
 *
 * Input: S = "12345"
 * Output: ["12345"]
 *
 * Note:
 *
 *     S will be a string with length between 1 and 12.
 *     S will consist only of letters or digits.
 */
/**
 * My original version.
 * Recursive approach. Not quite like backtrack version. straight and intuitive.
 */
class Solution {
private:
    void permute(string& str, int i, vector<string>& pa)
    {
        pa.push_back(str);
        while (i < str.size()) {
            locale loc;
            char c = str[i];
            if (true == isalpha(c, loc)) {
                str[i] = true == islower(c, loc) ? toupper(c, loc) : tolower(c, loc);
                permute(str, i + 1, pa);
                str[i] = c;
            }
            ++i;
        }
    }

public:
    vector<string> letterCasePermutation(string S) {
        vector<string> ans;
        permute(S, 0, ans);
        return ans;
    }
};

/**
 * Backtrack approach
 */
class Solution {
    void backtrack(string &s, int i, vector<string> &res) {
        if (i == s.size()) {
            res.push_back(s);
            return;
        }
        backtrack(s, i + 1, res);
        if (isalpha(s[i])) {
            // toggle case trick
            s[i] ^= (1 << 5);
            backtrack(s, i + 1, res);
        }
    }
public:
    vector<string> letterCasePermutation(string S) {
        vector<string> res;
        backtrack(S, 0, res);
        return res;
    }
};
