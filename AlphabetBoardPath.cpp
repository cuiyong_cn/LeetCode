/**
 * Alphabet Board Path
 *
 * On an alphabet board, we start at position (0, 0), corresponding to character board[0][0].
 * Here, board = ["abcde", "fghij", "klmno", "pqrst", "uvwxy", "z"], as shown in the diagram below.
 *
 * We may make the following moves:
 *     'U' moves our position up one row, if the position exists on the board;
 *     'D' moves our position down one row, if the position exists on the board;
 *     'L' moves our position left one column, if the position exists on the board;
 *     'R' moves our position right one column, if the position exists on the board;
 *     '!' adds the character board[r][c] at our current position (r, c) to the answer.
 *
 * (Here, the only positions that exist on the board are positions with letters on them.)
 * Return a sequence of moves that makes our answer equal to target in the minimum number of moves.  You may return any
 * path that does so.
 *
 * Example 1:
 *
 * Input: target = "leet"
 * Output: "DDR!UURRR!!DDD!"
 *
 * Example 2:
 *
 * Input: target = "code"
 * Output: "RR!DDRR!UUL!R!"
 *
 * Constraints:
 *     1 <= target.length <= 100
 *     target consists only of English lowercase letters.
 */
/**
 * Simplified one.
 */
class Solution {
public:
    string alphabetBoardPath(string target) {
        string ans;
        int x = 0;
        int y = 0;

        for (auto ch : target) {
            int x1 = (ch - 'a') % 5;
            int y1 = (ch - 'a') / 5;
            ans += string(max(0, y - y1), 'U')
                + string(max(0, x1 - x), 'R')
                + string(max(0, x - x1), 'L')
                + string(max(0, y1 - y), 'D') + "!";

            x = x1;
            y = y1;
        }

        return ans;
    }
};

/**
 * Original solution.
 */
class Solution {
public:
    string alphabetBoardPath(string target) {
        std::vector<Pos> alpha_board{
            {0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4},
            {1, 0}, {1, 1}, {1, 2}, {1, 3}, {1, 4},
            {2, 0}, {2, 1}, {2, 2}, {2, 3}, {2, 4},
            {3, 0}, {3, 1}, {3, 2}, {3, 3}, {3, 4},
            {4, 0}, {4, 1}, {4, 2}, {4, 3}, {4, 4},
            {5, 0}
        };

        char prev = 'a';
        string ans;
        for (auto ch : target) {
            auto prev_pos = alpha_board[prev - 'a'];
            auto cur_pos = alpha_board[ch - 'a'];
            int dr = cur_pos.r - prev_pos.r;
            int dc = cur_pos.c - prev_pos.c;

            string walk_path = string(std::abs(dc), dc > 0 ? 'R' : 'L')
                + string(std::abs(dr), dr > 0 ? 'D' : 'U');

            if (dc > 0 && 5 == prev_pos.r) {
                ans += string(walk_path.rbegin(), walk_path.rend());
            }
            else {
                ans += walk_path;
            }

            ans += '!';

            prev = ch;
        }

        return ans;
    }

private:
    struct Pos
    {
        int r;
        int c;
    };
};
